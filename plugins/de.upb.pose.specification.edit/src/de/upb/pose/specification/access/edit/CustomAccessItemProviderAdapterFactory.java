package de.upb.pose.specification.access.edit;

import org.eclipse.emf.common.notify.Adapter;

public class CustomAccessItemProviderAdapterFactory extends AccessItemProviderAdapterFactory {
	@Override
	public Adapter createPatternEnvironmentAdapter() {
		if (patternEnvironmentItemProvider == null) {
			patternEnvironmentItemProvider = new CustomPatternEnvironmentItemProvider(this);
		}
		return patternEnvironmentItemProvider;
	}

	@Override
	public Adapter createAccessRuleAdapter() {
		if (accessRuleItemProvider == null) {
			accessRuleItemProvider = new CustomAccessRuleItemProvider(this);
		}
		return accessRuleItemProvider;
	}

	@Override
	public Adapter createAccessAccessTypeAdapter() {
		if (accessAccessTypeItemProvider == null) {
			accessAccessTypeItemProvider = new CustomAccessAccessTypeItemProvider(this);
		}

		return accessAccessTypeItemProvider;
	}

	@Override
	public Adapter createAnyAccessTypeAdapter() {
		if (anyAccessTypeItemProvider == null) {
			anyAccessTypeItemProvider = new CustomAnyAccessTypeItemProvider(this);
		}

		return anyAccessTypeItemProvider;
	}

	@Override
	public Adapter createCallAccessTypeAdapter() {
		if (callAccessTypeItemProvider == null) {
			callAccessTypeItemProvider = new CustomCallAccessTypeItemProvider(this);
		}

		return callAccessTypeItemProvider;
	}

	@Override
	public Adapter createInstantiateAccessTypeAdapter() {
		if (instantiateAccessTypeItemProvider == null) {
			instantiateAccessTypeItemProvider = new CustomInstantiateAccessTypeItemProvider(this);
		}

		return instantiateAccessTypeItemProvider;
	}

	@Override
	public Adapter createReadAccessTypeAdapter() {
		if (readAccessTypeItemProvider == null) {
			readAccessTypeItemProvider = new CustomReadAccessTypeItemProvider(this);
		}

		return readAccessTypeItemProvider;
	}

	@Override
	public Adapter createReferAccessTypeAdapter() {
		if (referAccessTypeItemProvider == null) {
			referAccessTypeItemProvider = new CustomReferAccessTypeItemProvider(this);
		}

		return referAccessTypeItemProvider;
	}

	@Override
	public Adapter createSpecializeAccessTypeAdapter() {
		if (specializeAccessTypeItemProvider == null) {
			specializeAccessTypeItemProvider = new CustomSpecializeAccessTypeItemProvider(this);
		}

		return specializeAccessTypeItemProvider;
	}

	@Override
	public Adapter createWriteAccessTypeAdapter() {
		if (writeAccessTypeItemProvider == null) {
			writeAccessTypeItemProvider = new CustomWriteAccessTypeItemProvider(this);
		}

		return writeAccessTypeItemProvider;
	}
}
