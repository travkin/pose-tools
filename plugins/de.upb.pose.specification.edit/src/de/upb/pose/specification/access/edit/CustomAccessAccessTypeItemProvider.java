package de.upb.pose.specification.access.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.util.SpecificationUtil;

public class CustomAccessAccessTypeItemProvider extends AccessAccessTypeItemProvider {
	public CustomAccessAccessTypeItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
	
	@Override
	public String getText(Object object)
	{
		return SpecificationUtil.getAccessTypeLabel((AccessType) object);
	}
}
