package de.upb.pose.specification.access.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;

public class CustomPatternEnvironmentItemProvider extends PatternEnvironmentItemProvider {
	public CustomPatternEnvironmentItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
