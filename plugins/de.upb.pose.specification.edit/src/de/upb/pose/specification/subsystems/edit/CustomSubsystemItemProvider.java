package de.upb.pose.specification.subsystems.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;

public class CustomSubsystemItemProvider extends SubsystemItemProvider {
	public CustomSubsystemItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
