package de.upb.pose.specification.subsystems.edit;

import org.eclipse.emf.common.notify.Adapter;

public class CustomSubsystemsItemProviderAdapterFactory extends SubsystemsItemProviderAdapterFactory {
	@Override
	public Adapter createSubsystemAdapter() {
		if (subsystemItemProvider == null) {
			subsystemItemProvider = new CustomSubsystemItemProvider(this);
		}
		return subsystemItemProvider;
	}
}
