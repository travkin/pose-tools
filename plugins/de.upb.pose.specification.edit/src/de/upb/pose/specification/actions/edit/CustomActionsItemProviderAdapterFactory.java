package de.upb.pose.specification.actions.edit;

import org.eclipse.emf.common.notify.Adapter;

public class CustomActionsItemProviderAdapterFactory extends ActionsItemProviderAdapterFactory {
	@Override
	public Adapter createCreateActionAdapter() {
		if (createActionItemProvider == null) {
			createActionItemProvider = new CustomCreateActionItemProvider(this);
		}
		return createActionItemProvider;
	}

	@Override
	public Adapter createCallActionAdapter() {
		if (callActionItemProvider == null) {
			callActionItemProvider = new CustomCallActionItemProvider(this);
		}
		return callActionItemProvider;
	}

	@Override
	public Adapter createDelegateActionAdapter() {
		if (delegateActionItemProvider == null) {
			delegateActionItemProvider = new CustomDelegateActionItemProvider(this);
		}
		return delegateActionItemProvider;
	}

	@Override
	public Adapter createWriteActionAdapter() {
		if (writeActionItemProvider == null) {
			writeActionItemProvider = new CustomWriteActionItemProvider(this);
		}
		return writeActionItemProvider;
	}

	@Override
	public Adapter createDeleteActionAdapter() {
		if (deleteActionItemProvider == null) {
			deleteActionItemProvider = new CustomDeleteActionItemProvider(this);
		}
		return deleteActionItemProvider;
	}

	@Override
	public Adapter createReadActionAdapter() {
		if (readActionItemProvider == null) {
			readActionItemProvider = new CustomReadActionItemProvider(this);
		}
		return readActionItemProvider;
	}
	
	@Override
	public Adapter createRedirectActionAdapter()
	{
		if (redirectActionItemProvider == null)	{
			redirectActionItemProvider = new CustomRedirectActionItemProvider(this);
		}
		return redirectActionItemProvider;
	}

	@Override
	public Adapter createReturnActionAdapter() {
		if (returnActionItemProvider == null) {
			returnActionItemProvider = new CustomReturnActionItemProvider(this);
		}
		return returnActionItemProvider;
	}

	@Override
	public Adapter createParameterAssignmentAdapter() {
		if (parameterAssignmentItemProvider == null) {
			parameterAssignmentItemProvider = new CustomParameterAssignmentItemProvider(this);
		}
		return parameterAssignmentItemProvider;
	}

	@Override
	public Adapter createSelfVariableAdapter() {
		if (selfVariableItemProvider == null) {
			selfVariableItemProvider = new CustomSelfVariableItemProvider(this);
		}
		return selfVariableItemProvider;
	}

	@Override
	public Adapter createResultVariableAdapter() {
		if (resultVariableItemProvider == null) {
			resultVariableItemProvider = new CustomResultVariableItemProvider(this);
		}
		return resultVariableItemProvider;
	}

	@Override
	public Adapter createNullVariableAdapter() {
		if (nullVariableItemProvider == null) {
			nullVariableItemProvider = new CustomNullVariableItemProvider(this);
		}
		return nullVariableItemProvider;
	}
}
