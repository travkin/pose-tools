package de.upb.pose.specification.actions.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;

public class CustomCreateActionItemProvider extends CreateActionItemProvider {
	public CustomCreateActionItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
