package de.upb.pose.specification.edit;

import org.eclipse.emf.common.notify.Adapter;

public class CustomSpecificationItemProviderAdapterFactory extends SpecificationItemProviderAdapterFactory {
	@Override
	public Adapter createDesignPatternCatalogAdapter() {
		if (designPatternCatalogItemProvider == null) {
			designPatternCatalogItemProvider = new CustomDesignPatternCatalogItemProvider(this);
		}
		return designPatternCatalogItemProvider;
	}

	@Override
	public Adapter createCategoryAdapter() {
		if (categoryItemProvider == null) {
			categoryItemProvider = new CustomCategoryItemProvider(this);
		}
		return categoryItemProvider;
	}

	@Override
	public Adapter createDesignPatternAdapter() {
		if (designPatternItemProvider == null) {
			designPatternItemProvider = new CustomDesignPatternItemProvider(this);
		}
		return designPatternItemProvider;
	}

	@Override
	public Adapter createPatternSpecificationAdapter() {
		if (patternSpecificationItemProvider == null) {
			patternSpecificationItemProvider = new CustomPatternSpecificationItemProvider(this);
		}
		return patternSpecificationItemProvider;
	}

	@Override
	public Adapter createTaskDescriptionAdapter() {
		if (taskDescriptionItemProvider == null) {
			taskDescriptionItemProvider = new CustomTaskDescriptionItemProvider(this);
		}
		return taskDescriptionItemProvider;
	}

	@Override
	public Adapter createSetFragmentAdapter() {
		if (setFragmentItemProvider == null) {
			setFragmentItemProvider = new CustomSetFragmentItemProvider(this);
		}
		return setFragmentItemProvider;
	}

	@Override
	public Adapter createDesignModelAdapter() {
		if (designModelItemProvider == null) {
			designModelItemProvider = new CustomDesignModelItemProvider(this);
		}
		return designModelItemProvider;
	}
}
