package de.upb.pose.specification.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;

public class CustomDesignPatternCatalogItemProvider extends DesignPatternCatalogItemProvider {
	public CustomDesignPatternCatalogItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
