package de.upb.pose.specification.types.edit;

import org.eclipse.emf.common.notify.Adapter;

public class CustomTypesItemProviderAdapterFactory extends TypesItemProviderAdapterFactory {
	@Override
	public Adapter createTypeAdapter() {
		if (typeItemProvider == null) {
			typeItemProvider = new CustomTypeItemProvider(this);
		}
		return typeItemProvider;
	}

	@Override
	public Adapter createOperationAdapter() {
		if (operationItemProvider == null) {
			operationItemProvider = new CustomOperationItemProvider(this);
		}
		return operationItemProvider;
	}

	@Override
	public Adapter createPrimitiveTypeAdapter() {
		if (primitiveTypeItemProvider == null) {
			primitiveTypeItemProvider = new CustomPrimitiveTypeItemProvider(this);
		}
		return primitiveTypeItemProvider;
	}

	@Override
	public Adapter createParameterAdapter() {
		if (parameterItemProvider == null) {
			parameterItemProvider = new CustomParameterItemProvider(this);
		}
		return parameterItemProvider;
	}

	@Override
	public Adapter createAttributeAdapter() {
		if (attributeItemProvider == null) {
			attributeItemProvider = new CustomAttributeItemProvider(this);
		}
		return attributeItemProvider;
	}

	@Override
	public Adapter createReferenceAdapter() {
		if (referenceItemProvider == null) {
			referenceItemProvider = new CustomReferenceItemProvider(this);
		}
		return referenceItemProvider;
	}
}
