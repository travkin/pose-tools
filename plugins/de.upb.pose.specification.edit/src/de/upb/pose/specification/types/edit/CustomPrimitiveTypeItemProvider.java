package de.upb.pose.specification.types.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.PrimitiveType;
import de.upb.pose.specification.util.SpecificationUtil;

public class CustomPrimitiveTypeItemProvider extends PrimitiveTypeItemProvider {
	public CustomPrimitiveTypeItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
	
	@Override
	public String getText(Object object)
	{
		return SpecificationUtil.getPrimitiveTypeLabel((PrimitiveType) object);
	}
}
