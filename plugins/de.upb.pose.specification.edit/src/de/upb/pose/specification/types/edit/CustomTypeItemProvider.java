package de.upb.pose.specification.types.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.Type;

public class CustomTypeItemProvider extends TypeItemProvider {
	public CustomTypeItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public String getText(Object object) {
		Type bo = (Type) object;

		StringBuilder builder = new StringBuilder();

		builder.append(bo.eClass().getName());
		builder.append(' ');
		builder.append(bo.getName());

		return builder.toString();
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
