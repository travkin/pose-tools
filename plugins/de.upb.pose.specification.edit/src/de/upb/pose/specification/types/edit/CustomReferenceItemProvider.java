package de.upb.pose.specification.types.edit;

import org.eclipse.emf.common.notify.AdapterFactory;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.Reference;

public class CustomReferenceItemProvider extends ReferenceItemProvider {
	public CustomReferenceItemProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public String getText(Object object) {
		Reference element = (Reference) object;

		StringBuilder builder = new StringBuilder();

		builder.append(element.eClass().getName());
		builder.append(' ');
		builder.append(element.getName());
		switch (element.getCardinality()) {
		case MULTIPLE:
			builder.append('*');
			break;

		default:
			break;
		}

		return builder.toString();
	}

	@Override
	public Object getImage(Object element) {
		return SpecificationImages.get(element);
	}
}
