/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.edit;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import de.upb.pose.specification.access.util.AccessAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AccessItemProviderAdapterFactory extends AccessAdapterFactory implements ComposeableAdapterFactory,
		IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.PatternEnvironment} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternEnvironmentItemProvider patternEnvironmentItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.PatternEnvironment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPatternEnvironmentAdapter() {
		if (patternEnvironmentItemProvider == null) {
			patternEnvironmentItemProvider = new PatternEnvironmentItemProvider(this);
		}

		return patternEnvironmentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.AccessRule} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessRuleItemProvider accessRuleItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.AccessRule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAccessRuleAdapter() {
		if (accessRuleItemProvider == null) {
			accessRuleItemProvider = new AccessRuleItemProvider(this);
		}

		return accessRuleItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.AnyAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnyAccessTypeItemProvider anyAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.AnyAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAnyAccessTypeAdapter() {
		if (anyAccessTypeItemProvider == null) {
			anyAccessTypeItemProvider = new AnyAccessTypeItemProvider(this);
		}

		return anyAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.ReferAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferAccessTypeItemProvider referAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.ReferAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createReferAccessTypeAdapter() {
		if (referAccessTypeItemProvider == null) {
			referAccessTypeItemProvider = new ReferAccessTypeItemProvider(this);
		}

		return referAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.AccessAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessAccessTypeItemProvider accessAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.AccessAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAccessAccessTypeAdapter() {
		if (accessAccessTypeItemProvider == null) {
			accessAccessTypeItemProvider = new AccessAccessTypeItemProvider(this);
		}

		return accessAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.SpecializeAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SpecializeAccessTypeItemProvider specializeAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.SpecializeAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSpecializeAccessTypeAdapter() {
		if (specializeAccessTypeItemProvider == null) {
			specializeAccessTypeItemProvider = new SpecializeAccessTypeItemProvider(this);
		}

		return specializeAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.InstantiateAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstantiateAccessTypeItemProvider instantiateAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.InstantiateAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createInstantiateAccessTypeAdapter() {
		if (instantiateAccessTypeItemProvider == null) {
			instantiateAccessTypeItemProvider = new InstantiateAccessTypeItemProvider(this);
		}

		return instantiateAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.ReadAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReadAccessTypeItemProvider readAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.ReadAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createReadAccessTypeAdapter() {
		if (readAccessTypeItemProvider == null) {
			readAccessTypeItemProvider = new ReadAccessTypeItemProvider(this);
		}

		return readAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.WriteAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WriteAccessTypeItemProvider writeAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.WriteAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWriteAccessTypeAdapter() {
		if (writeAccessTypeItemProvider == null) {
			writeAccessTypeItemProvider = new WriteAccessTypeItemProvider(this);
		}

		return writeAccessTypeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link de.upb.pose.specification.access.CallAccessType} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallAccessTypeItemProvider callAccessTypeItemProvider;

	/**
	 * This creates an adapter for a {@link de.upb.pose.specification.access.CallAccessType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCallAccessTypeAdapter() {
		if (callAccessTypeItemProvider == null) {
			callAccessTypeItemProvider = new CallAccessTypeItemProvider(this);
		}

		return callAccessTypeItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>) type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (patternEnvironmentItemProvider != null)
			patternEnvironmentItemProvider.dispose();
		if (accessRuleItemProvider != null)
			accessRuleItemProvider.dispose();
		if (anyAccessTypeItemProvider != null)
			anyAccessTypeItemProvider.dispose();
		if (referAccessTypeItemProvider != null)
			referAccessTypeItemProvider.dispose();
		if (accessAccessTypeItemProvider != null)
			accessAccessTypeItemProvider.dispose();
		if (specializeAccessTypeItemProvider != null)
			specializeAccessTypeItemProvider.dispose();
		if (instantiateAccessTypeItemProvider != null)
			instantiateAccessTypeItemProvider.dispose();
		if (readAccessTypeItemProvider != null)
			readAccessTypeItemProvider.dispose();
		if (writeAccessTypeItemProvider != null)
			writeAccessTypeItemProvider.dispose();
		if (callAccessTypeItemProvider != null)
			callAccessTypeItemProvider.dispose();
	}

}
