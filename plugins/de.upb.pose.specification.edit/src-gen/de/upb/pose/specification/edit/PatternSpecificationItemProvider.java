/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.edit;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import de.upb.pose.core.CorePackage;
import de.upb.pose.core.edit.IdentifierItemProvider;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.subsystems.SubsystemsFactory;
import de.upb.pose.specification.types.TypesFactory;

/**
 * This is the item provider adapter for a {@link de.upb.pose.specification.PatternSpecification} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PatternSpecificationItemProvider extends IdentifierItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternSpecificationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addCommentPropertyDescriptor(object);
			addDesignElementsPropertyDescriptor(object);
			addSetFragmentsPropertyDescriptor(object);
			addAccessRulesPropertyDescriptor(object);
			addEnvironmentPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_Named_name_feature"), //$NON-NLS-1$
				getString("_UI_PropertyDescriptor_description", "_UI_Named_name_feature", "_UI_Named_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				CorePackage.Literals.NAMED__NAME, true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				null));
	}

	/**
	 * This adds a property descriptor for the Comment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCommentPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_Commentable_comment_feature"), //$NON-NLS-1$
						getString(
								"_UI_PropertyDescriptor_description", "_UI_Commentable_comment_feature", "_UI_Commentable_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						CorePackage.Literals.COMMENTABLE__COMMENT, true, true, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Design Elements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDesignElementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_PatternSpecification_designElements_feature"), //$NON-NLS-1$
						getString(
								"_UI_PropertyDescriptor_description", "_UI_PatternSpecification_designElements_feature", "_UI_PatternSpecification_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						SpecificationPackage.Literals.PATTERN_SPECIFICATION__DESIGN_ELEMENTS, true, false, true, null,
						null, null));
	}

	/**
	 * This adds a property descriptor for the Set Fragments feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSetFragmentsPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_PatternSpecification_setFragments_feature"), //$NON-NLS-1$
						getString(
								"_UI_PropertyDescriptor_description", "_UI_PatternSpecification_setFragments_feature", "_UI_PatternSpecification_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						SpecificationPackage.Literals.PATTERN_SPECIFICATION__SET_FRAGMENTS, true, false, true, null,
						null, null));
	}

	/**
	 * This adds a property descriptor for the Access Rules feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAccessRulesPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_PatternSpecification_accessRules_feature"), //$NON-NLS-1$
						getString(
								"_UI_PropertyDescriptor_description", "_UI_PatternSpecification_accessRules_feature", "_UI_PatternSpecification_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						SpecificationPackage.Literals.PATTERN_SPECIFICATION__ACCESS_RULES, true, false, true, null,
						null, null));
	}

	/**
	 * This adds a property descriptor for the Environment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnvironmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_PatternSpecification_environment_feature"), //$NON-NLS-1$
						getString(
								"_UI_PropertyDescriptor_description", "_UI_PatternSpecification_environment_feature", "_UI_PatternSpecification_type"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						SpecificationPackage.Literals.PATTERN_SPECIFICATION__ENVIRONMENT, true, false, true, null,
						null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean shouldComposeCreationImage() {
		return true;
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((PatternSpecification) object).getName();
		return label == null || label.length() == 0 ? getString("_UI_PatternSpecification_type") : //$NON-NLS-1$
				getString("_UI_PatternSpecification_type") + " " + label; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(PatternSpecification.class)) {
		case SpecificationPackage.PATTERN_SPECIFICATION__NAME:
		case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				SpecificationFactory.eINSTANCE.createTaskDescription()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				SpecificationFactory.eINSTANCE.createSetFragment()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				SpecificationFactory.eINSTANCE.createDesignModel()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				AccessFactory.eINSTANCE.createPatternEnvironment()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				AccessFactory.eINSTANCE.createAccessRule()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createCreateAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createRedirectAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createDelegateAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createWriteAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createDeleteAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createReadAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createReturnAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createParameterAssignment()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createSelfVariable()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createResultVariable()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createNullVariable()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				ActionsFactory.eINSTANCE.createProduceAction()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				SubsystemsFactory.eINSTANCE.createSubsystem()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createType()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createPrimitiveType()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createParameter()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createAttribute()));

		newChildDescriptors.add(createChildParameter(
				SpecificationPackage.Literals.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
				TypesFactory.eINSTANCE.createReference()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return Activator.INSTANCE;
	}

}
