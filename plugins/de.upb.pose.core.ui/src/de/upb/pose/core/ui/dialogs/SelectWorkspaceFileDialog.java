package de.upb.pose.core.ui.dialogs;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

public class SelectWorkspaceFileDialog extends AbstractTreeSelectionDialog<IFile> {
	public SelectWorkspaceFileDialog(String title, String description) {
		super(title, description);
	}

	public SelectWorkspaceFileDialog() {
		this("Select Workspace Resource", "Select the resource from the workspace.");
	}

	@Override
	protected String getErrorMessage(Object element) {
		if (element instanceof IFile) {
			return null;
		}
		return "Select a file to load.";
	}

	@Override
	protected ViewerComparator getViewerComparator() {
		return new ResourceComparator(ResourceComparator.TYPE);
	}

	@Override
	protected ViewerFilter getViewerFilter() {
		return new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parent, Object element) {
				if (element instanceof IFile) {
					if (((IFile) element).getName().charAt(0) == '.') {
						return false;
					}
				}

				if (element instanceof IResource) {
					return ((IResource) element).isAccessible();
				}
				return false;
			}
		};
	}

	@Override
	protected boolean isMulti() {
		return false;
	}

	@Override
	protected Object getInput() {
		return ResourcesPlugin.getWorkspace().getRoot();
	}

	@Override
	protected ITreeContentProvider getContentProvider() {
		return new WorkbenchContentProvider();
	}

	@Override
	protected ILabelProvider getLabelProvider() {
		return new WorkbenchLabelProvider();
	}
}
