package de.upb.pose.core.ui.editor;

import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;

public class InnerDiagramEditorUpdateBehavior extends DefaultUpdateBehavior {
	public InnerDiagramEditorUpdateBehavior(DiagramEditor editor) {
		super(editor);
	}

	@Override
	protected void disposeEditingDomain() {
		// let the main editor dispose it
	}

	@Override
	protected void createEditingDomain() {
		initializeEditingDomain(diagramEditor.getEditingDomain());
	}
}
