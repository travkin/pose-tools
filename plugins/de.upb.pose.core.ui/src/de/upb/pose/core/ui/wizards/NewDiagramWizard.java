package de.upb.pose.core.ui.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

public abstract class NewDiagramWizard extends Wizard implements INewWizard {
	protected String modelExtension;
	private String diagramExtension;

	protected String diagramTypeId;
	protected String editorId;

	private EClass type;

	protected ContainerShape diagramContainer;

	protected ResourceSet resourceSet;

	private ComposedAdapterFactory adapterFactory;

	private boolean shouldCreateModel;

	protected EObject model;
	protected final Map<OLDCreateResourcePage, IPath> paths;

	protected IWorkbench workbench;
	private IStructuredSelection selection;

	protected SelectModelSourcePage modePage;
	protected OLDCreateResourcePage modelPage;
	protected OLDCreateResourcePage diagramPage;

	public NewDiagramWizard(String modelExtension, String diagramExtension, String diagramTypeId, EClass type) {
		paths = new LinkedHashMap<OLDCreateResourcePage, IPath>();

		setHelpAvailable(false);
		setNeedsProgressMonitor(true);
		configureWizard();

		this.modelExtension = modelExtension;
		this.diagramExtension = diagramExtension;
		this.diagramTypeId = diagramTypeId;
		this.type = type;
	}

	public NewDiagramWizard() {
		paths = new LinkedHashMap<OLDCreateResourcePage, IPath>();

		setHelpAvailable(false);
		setNeedsProgressMonitor(true);

		configureWizard();
	}

	protected void configureWizard() {
	}

	@Override
	public void addPages() {
		// create pages
		modePage = new SelectModelSourcePage();
		configureModePage(modePage);

		modelPage = new OLDCreateResourcePage(modelExtension);
		configureModelPage(modelPage);

		diagramPage = new OLDCreateResourcePage(getDiagramExtension());
		configureDiagramPage(diagramPage);

		addPage(modePage);
		addPage(modelPage);
		addPage(diagramPage);
	}

	protected void configureModePage(SelectModelSourcePage page) {
		page.setTitle("Create Diagram");
		page.setDescription("Select the model for which the diagram should be created.");
	}

	protected void configureModelPage(OLDCreateResourcePage page) {
		page.setDefaultName("catalog");
		page.setTitle("Select Catalog Resource");
		page.setDescription("Select the location for the pattern catalog resource.");
	}

	protected String getDiagramExtension() {
		if (diagramExtension == null) {
			return modelExtension + "_diagrams";
		}
		return diagramExtension;
	}

	protected void configureDiagramPage(OLDCreateResourcePage page) {
		page.setDefaultName("catalog");
		page.setTitle("Select Catalog Diagram Resource");
		page.setDescription("Select the location for the pattern catalog diagram resource.");
	}

	@Override
	public boolean performFinish() {
		IRunnableWithProgress runnable = new IRunnableWithProgress() {
			@Override
			public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				// create diagram
				diagramContainer = PictogramsFactory.eINSTANCE.createContainerShape();

				// create diagram resource
				URI diagramUri = URI.createPlatformResourceURI(paths.get(diagramPage).toString(), true);
				Resource diagramResource = getResourceSet().createResource(diagramUri);
				diagramResource.getContents().add(diagramContainer);

				// save diagram
				try {
					if (diagramResource instanceof XMIResource) {
						((XMIResource) diagramResource).setEncoding("UTF-8");
					}
					diagramResource.save(Collections.emptyMap());
				} catch (IOException e) {
					throw new InvocationTargetException(e);
				}

				// look for model
				EObject bo = model;
				if (shouldCreateModel) {
					bo = createModel();

					// create model resource
					URI modelUri = URI.createPlatformResourceURI(paths.get(modelPage).toString(), true);
					Resource modelResource = getResourceSet().createResource(modelUri);
					modelResource.getContents().add(bo);

					if (modelResource instanceof XMIResource) {
						((XMIResource) modelResource).setEncoding("UTF-8");
					}

					// save model
					try {
						modelResource.save(Collections.emptyMap());
					} catch (IOException e) {
						throw new InvocationTargetException(e);
					}
				}

				// create link
				PictogramLink link = PictogramsFactory.eINSTANCE.createPictogramLink();
				diagramContainer.setLink(link);
				link.getBusinessObjects().add(bo);

				// save diagram, again
				try {
					if (diagramResource instanceof XMIResource) {
						((XMIResource) diagramResource).setEncoding("UTF-8");
					}
					diagramResource.save(Collections.emptyMap());
				} catch (IOException e) {
					throw new InvocationTargetException(e);
				}

				postExecute();
			}
		};

		try {
			getContainer().run(true, false, runnable);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}

		// open the editor
		if (editorId != null) {
			if (workbench == null) {
				workbench = PlatformUI.getWorkbench();
			}
			IPath diagramPath = paths.get(diagramPage);
			IFile file = (IFile) ResourcesPlugin.getWorkspace().getRoot().findMember(diagramPath);

			try {
				workbench.getActiveWorkbenchWindow().getActivePage().openEditor(new FileEditorInput(file), editorId);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	public void setModelExtension(String modelExtension) {
		this.modelExtension = modelExtension;
	}

	public void setDiagramExtension(String diagramExtension) {
		this.diagramExtension = diagramExtension;
	}

	public void setDiagramTypeId(String diagramTypeId) {
		this.diagramTypeId = diagramTypeId;
	}

	public void setType(EClass type) {
		this.type = type;
	}

	public EClass getType() {
		return type;
	}

	@Override
	public boolean canFinish() {
		if (shouldCreateModel) {
			return paths.get(modelPage) != null && paths.get(diagramPage) != null;
		} else {
			return model != null && paths.get(diagramPage) != null;
		}
	}

	public void setPath(OLDCreateResourcePage page, IPath path) {
		paths.put(page, path);
	}

	public IPath getPath(OLDCreateResourcePage page) {
		return paths.get(page);
	}

	public void setModel(EObject model) {
		this.model = model;
	}

	public void setShouldCreateModel(boolean shouldCreateModel) {
		this.shouldCreateModel = shouldCreateModel;
	}

	public IStructuredSelection getSelection() {
		return selection;
	}

	public AdapterFactory getAdapterFactory() {
		if (adapterFactory == null) {
			adapterFactory = new ComposedAdapterFactory(Registry.INSTANCE);
		}
		return adapterFactory;
	}

	@Override
	public void dispose() {
		if (adapterFactory != null) {
			adapterFactory.dispose();
		}

		super.dispose();
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
	}

	public ResourceSet getResourceSet() {
		if (resourceSet == null) {
			resourceSet = new ResourceSetImpl();
		}
		return resourceSet;
	}

	public boolean shouldCreateModel() {
		return shouldCreateModel;
	}

	public EObject getModel() {
		return model;
	}

	protected void postExecute() {
		// nothing by default
	}

	protected EObject getDiagramContainer() {
		return diagramContainer;
	}

	protected String getEditorId() {
		return editorId;
	}

	protected String getModelExtension() {
		return modelExtension;
	}

	protected void setEditorId(String editorId) {
		this.editorId = editorId;
	}

	protected EObject createModel() {
		return EcoreUtil.create(type);
	}
}
