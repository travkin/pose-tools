/**
 * 
 */
package de.upb.pose.core.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * @author Dietrich Travkin
 */
public class FileViewerFiler extends ViewerFilter {

	private List<String> fileExtensions;
	
	public FileViewerFiler(String... visibleFileExtensions) {
		if (visibleFileExtensions == null || visibleFileExtensions.length == 0) {
			throw new IllegalArgumentException("No file extensions defined to be visible.");
		}
		
		this.fileExtensions = new ArrayList<String>(visibleFileExtensions.length);
		for (String extension: visibleFileExtensions) {
			this.fileExtensions.add(extension);
		}
	}
	
	/**
	 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean select(Viewer viewer, Object parent, Object element) {
		// check primary type
		if (!(element instanceof IResource)) {
			return false;
		}

		// check accessibility
		if (!((IResource) element).isAccessible()) {
			return false;
		}

		// check for 'hidden' files
		if (((IResource) element).getName().charAt(0) == '.') {
			return false;
		}

		// check for container
		if (element instanceof IContainer) {
			return true;
		}

		// check file extension
		if (element instanceof IFile) {
			String extension = ((IFile) element).getFileExtension();
			for (String visibleExtension: this.fileExtensions) {
				if (visibleExtension.equals(extension)) {
					return true;
				}
			}
		}

		return false;
	}

}
