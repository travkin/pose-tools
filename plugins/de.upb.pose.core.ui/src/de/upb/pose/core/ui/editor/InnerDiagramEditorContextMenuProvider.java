package de.upb.pose.core.ui.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.ui.editor.DiagramEditorContextMenuProvider;
import org.eclipse.jface.action.IContributionItem;

public class InnerDiagramEditorContextMenuProvider extends DiagramEditorContextMenuProvider {
	private static Collection<String> hidden;

	public InnerDiagramEditorContextMenuProvider(EditPartViewer viewer, ActionRegistry registry,
			IDiagramTypeProvider dtp) {
		super(viewer, registry, dtp);
	}

	@Override
	public IContributionItem[] getItems() {
		IContributionItem[] oldItems = super.getItems();
		Collection<IContributionItem> items = new ArrayList<IContributionItem>();

		// dirty hack to hide some unwanted context menu entries
		for (IContributionItem item : oldItems) {
			String id = item.getId();
			if (!getHidden().contains(id)) {
				items.add(item);
			}
		}

		return items.toArray(new IContributionItem[items.size()]);
	}

	private static Collection<String> getHidden() {
		if (hidden == null) {
			hidden = new LinkedHashSet<String>();

			hidden.add("team.main");
			hidden.add("compareWithMenu");
			hidden.add("replaceWithMenu");
			hidden.add("org.eclipse.ui.projectConfigure");
			hidden.add("org.eclipse.debug.ui.actions.WatchCommand");
		}
		return hidden;
	}
}
