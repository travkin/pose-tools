package de.upb.pose.core.ui.outline;

import org.eclipse.emf.common.notify.AdapterFactory;

public interface IOutlineInputProvider {
	AdapterFactory getAdapterFactory();

	Object getOutlinePageInput();
}
