package de.upb.pose.core.ui.properties;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

public abstract class StringMultiTextSection extends FeaturePropertySectionBase<String> {
	private Group group;
	private Text text;

	@Override
	public void refresh() {
		if (isReady() && hasChanged()) {
			text.setText(getValue());
		}
	}

	private boolean isReady() {
		return text != null && !text.isDisposed();
	}

	private boolean hasChanged() {
		String oldText = text.getText();
		String newText = getValue();

		if (oldText.equals(newText)) {
			return false;
		}

		if (newText != null && newText.equals(oldText)) {
			return false;
		}

		if (newText == null && EMPTY.equals(oldText)) {
			return false;
		}

		return true;
	}

	@Override
	public boolean shouldUseExtraSpace() {
		return true;
	}

	@Override
	protected void createWidgets(Composite parent, TabbedPropertySheetWidgetFactory factory) {
		group = factory.createGroup(parent, getLabelText());

		text = factory.createText(group, EMPTY, SWT.BORDER | SWT.MULTI | SWT.WRAP);
	}

	protected abstract String getLabelText();
	@Override
	protected void hookWidgetListeners() {
		// select text on label click
		group.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				text.setFocus();
				text.selectAll();
			}
		});

		// execute command when focus lost
		text.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				doExecute();
			}
		});
	}

	@Override
	protected void layoutWidgets() {
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(group);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(text);

		FormData data = new FormData();
		data.left = new FormAttachment(0);
		data.right = new FormAttachment(100);
		data.top = new FormAttachment(0);
		data.bottom = new FormAttachment(100);
		group.setLayoutData(data);
	}

	private void doExecute() {
		String value = getValue(text.getText());

		boolean abort = false;
		Object oldValue = getValue();
		if (oldValue != null) {
			abort = oldValue.equals(value);
		}
		if (!abort && value != null) {
			abort = value.equals(oldValue);
		}

		if (!abort) {
			Point selection = text.getSelection();
			set(value);
			text.setSelection(selection);
		}
	}

	protected String getValue(String value) {
		return value;
	}
}
