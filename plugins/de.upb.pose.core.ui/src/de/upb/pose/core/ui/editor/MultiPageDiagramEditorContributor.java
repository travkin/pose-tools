package de.upb.pose.core.ui.editor;

import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.ui.actions.ActionBarContributor;
import org.eclipse.gef.ui.actions.AlignmentRetargetAction;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.MatchHeightRetargetAction;
import org.eclipse.gef.ui.actions.MatchWidthRetargetAction;
import org.eclipse.gef.ui.actions.ZoomComboContributionItem;
import org.eclipse.gef.ui.actions.ZoomInRetargetAction;
import org.eclipse.gef.ui.actions.ZoomOutRetargetAction;
import org.eclipse.graphiti.platform.IPlatformImageConstants;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.RetargetAction;

public class MultiPageDiagramEditorContributor extends ActionBarContributor {
	private IEditorPart activeEditor;

	@Override
	public void setActiveEditor(IEditorPart editor) {
		IEditorPart page = null;
		if (editor instanceof MultiPageDiagramEditor) {
			page = ((MultiPageDiagramEditor) editor).getActiveEditor();
		}
		setActivePage(page);
	}

	private void setActivePage(IEditorPart editor) {
		if (activeEditor == editor) {
			return;
		}

		activeEditor = editor;

		if (editor instanceof DiagramEditor) {
			super.setActiveEditor(activeEditor);
		}
	}

	@Override
	public void contributeToMenu(IMenuManager menubar) {
		super.contributeToMenu(menubar);
		IMenuManager editMenu = menubar.findMenuUsingPath(IWorkbenchActionConstants.M_EDIT);

		if (editMenu != null) {
			MenuManager alignments = new MenuManager("Align");
			alignments.add(getAction(GEFActionConstants.ALIGN_LEFT));
			alignments.add(getAction(GEFActionConstants.ALIGN_CENTER));
			alignments.add(getAction(GEFActionConstants.ALIGN_RIGHT));
			alignments.add(new Separator());
			alignments.add(getAction(GEFActionConstants.ALIGN_TOP));
			alignments.add(getAction(GEFActionConstants.ALIGN_MIDDLE));
			alignments.add(getAction(GEFActionConstants.ALIGN_BOTTOM));
			alignments.add(new Separator());
			alignments.add(getAction(GEFActionConstants.MATCH_WIDTH));
			alignments.add(getAction(GEFActionConstants.MATCH_HEIGHT));
			editMenu.insertAfter(ActionFactory.SELECT_ALL.getId(), alignments);

			editMenu.insertAfter(ActionFactory.DELETE.getId(), getAction("predefined remove action"));
			editMenu.insertAfter("predefined remove action", getAction("predefined update action"));
		}

		// Create view menu ...
		MenuManager viewMenu = new MenuManager("View");
		viewMenu.add(getAction(GEFActionConstants.ZOOM_IN));
		viewMenu.add(getAction(GEFActionConstants.ZOOM_OUT));
		viewMenu.add(getAction(GEFActionConstants.TOGGLE_GRID_VISIBILITY));
		viewMenu.add(getAction(GEFActionConstants.TOGGLE_SNAP_TO_GEOMETRY));

		// ... and add it. The position of the view menu differs depending on
		// which menus exist (see Bugzilla
		// https://bugs.eclipse.org/bugs/show_bug.cgi?id=381437)
		if (editMenu != null) {
			// Edit menu exists --> place view menu directly in front of it
			menubar.insertAfter(IWorkbenchActionConstants.M_EDIT, viewMenu);
		} else if (menubar.findMenuUsingPath(IWorkbenchActionConstants.M_FILE) != null) {
			// File menu exists --> place view menu behind it
			menubar.insertAfter(IWorkbenchActionConstants.M_FILE, viewMenu);
		} else {
			// Add view menu as first entry
			IContributionItem[] contributionItems = menubar.getItems();
			if (contributionItems != null && contributionItems.length > 0) {
				// Any menu exists --> place view menu in front of it it
				menubar.insertBefore(contributionItems[0].getId(), viewMenu);
			} else {
				// No item exists --> simply add view menu
				menubar.add(viewMenu);
			}
		}

		IMenuManager fileMenu = menubar.findMenuUsingPath(IWorkbenchActionConstants.M_FILE);
		if (fileMenu != null) {
			// Might not be available in RCP case
			fileMenu.insertAfter(ActionFactory.EXPORT.getId(), getAction("export_diagram_action"));
		}
	}

	public void contributeToToolBar(IToolBarManager tbm) {
		tbm.add(getAction(ActionFactory.UNDO.getId()));
		tbm.add(getAction(ActionFactory.REDO.getId()));

		tbm.add(new Separator());
		tbm.add(getAction(ActionFactory.COPY.getId()));
		tbm.add(getAction(ActionFactory.PASTE.getId()));

		tbm.add(new Separator());
		tbm.add(getAction(GEFActionConstants.ALIGN_LEFT));
		tbm.add(getAction(GEFActionConstants.ALIGN_CENTER));
		tbm.add(getAction(GEFActionConstants.ALIGN_RIGHT));
		tbm.add(new Separator());
		tbm.add(getAction(GEFActionConstants.ALIGN_TOP));
		tbm.add(getAction(GEFActionConstants.ALIGN_MIDDLE));
		tbm.add(getAction(GEFActionConstants.ALIGN_BOTTOM));
		tbm.add(new Separator());
		tbm.add(getAction(GEFActionConstants.MATCH_WIDTH));
		tbm.add(getAction(GEFActionConstants.MATCH_HEIGHT));

		// Bug 323351: Add button to toggle a flag if the context pad buttons
		// shall be shown or not
		tbm.add(new Separator());
		tbm.add(getAction("toggle_context_button_pad"));
		// End bug 323351

		tbm.add(new Separator());
		tbm.add(getAction(GEFActionConstants.ZOOM_OUT));
		tbm.add(getAction(GEFActionConstants.ZOOM_IN));
		ZoomComboContributionItem zoomCombo = new ZoomComboContributionItem(getPage());
		tbm.add(zoomCombo);

		tbm.add(new Separator());
	}

	@Override
	protected void buildActions() {
		addRetargetAction((RetargetAction) ActionFactory.UNDO.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.REDO.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.DELETE.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.COPY.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.PASTE.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.PRINT.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));
		addRetargetAction((RetargetAction) ActionFactory.SELECT_ALL.create(PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow()));

		addRetargetAction(new AlignmentRetargetAction(PositionConstants.LEFT));
		addRetargetAction(new AlignmentRetargetAction(PositionConstants.CENTER));
		addRetargetAction(new AlignmentRetargetAction(PositionConstants.RIGHT));
		addRetargetAction(new AlignmentRetargetAction(PositionConstants.TOP));
		addRetargetAction(new AlignmentRetargetAction(PositionConstants.MIDDLE));
		addRetargetAction(new AlignmentRetargetAction(PositionConstants.BOTTOM));
		addRetargetAction(new MatchWidthRetargetAction());
		addRetargetAction(new MatchHeightRetargetAction());

		addRetargetAction(new ZoomInRetargetAction());
		addRetargetAction(new ZoomOutRetargetAction());
		addRetargetAction(new RetargetAction(GEFActionConstants.TOGGLE_GRID_VISIBILITY, "&Grid", IAction.AS_CHECK_BOX));

		addRetargetAction(new RetargetAction(GEFActionConstants.TOGGLE_SNAP_TO_GEOMETRY, "Snap to Geo&metry",
				IAction.AS_CHECK_BOX));

		// Bug 323351: Add button to toggle a flag if the context pad buttons
		// shall be shown or not
		RetargetAction toggleContextPadAction = new RetargetAction("toggle_context_button_pad", "Hide Context Buttons",
				IAction.AS_CHECK_BOX);
		toggleContextPadAction.setImageDescriptor(GraphitiUi.getImageService().getImageDescriptorForId(
				IPlatformImageConstants.IMG_TOGGLE_PAD));
		addRetargetAction(toggleContextPadAction);
		// End bug 323351

		RetargetAction removeRetargetAction = new RetargetAction("predefined remove action", "Re&move");
		removeRetargetAction.setImageDescriptor(GraphitiUi.getImageService().getImageDescriptorForId(
				IPlatformImageConstants.IMG_EDIT_REMOVE));
		removeRetargetAction.setActionDefinitionId("org.eclipse.graphiti.ui.internal.action.RemoveAction");
		addRetargetAction(removeRetargetAction);
		RetargetAction updateRetargetAction = new RetargetAction("predefined update action", "Upda&te");
		updateRetargetAction.setImageDescriptor(GraphitiUi.getImageService().getImageDescriptorForId(
				IPlatformImageConstants.IMG_EDIT_REFRESH));
		updateRetargetAction.setActionDefinitionId("org.eclipse.graphiti.ui.internal.action.UpdateAction");
		addRetargetAction(updateRetargetAction);
		RetargetAction saveImageRetargetAction = new RetargetAction("export_diagram_action", "&Export Diagram...");
		saveImageRetargetAction.setActionDefinitionId("org.eclipse.graphiti.ui.internal.action.SaveImageAction");
		addRetargetAction(saveImageRetargetAction);
	}

	@Override
	protected void declareGlobalActionKeys() {
		// nothing
	}
}
