package de.upb.pose.core.ui.editor;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

public class InnerDiagramEditor extends DiagramEditor {
	private TransactionalEditingDomain editingDomain;

	public InnerDiagramEditor(TransactionalEditingDomain editingDomain) {
		this.editingDomain = editingDomain;
	}

	@Override
	public TransactionalEditingDomain getEditingDomain() {
		return editingDomain;
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// FIXME check why the viewer's control is sometimes null!
		if (this.isAlive()
				&& this.getGraphicalViewer() != null
				&& this.getGraphicalViewer().getControl() != null) {
			super.selectionChanged(part, selection);
			
			updateActions(getSelectionActions());
		}
	}

	@Override
	public void refreshTitle() {
		// nothing to do
	}

	@Override
	protected ContextMenuProvider createContextMenuProvider() {
		EditPartViewer viewer = getGraphicalViewer();
		ActionRegistry registry = getActionRegistry();
		IDiagramTypeProvider dtp = getDiagramTypeProvider();

		return new InnerDiagramEditorContextMenuProvider(viewer, registry, dtp);
	}

	@Override
	protected DefaultUpdateBehavior createUpdateBehavior() {
		return new InnerDiagramEditorUpdateBehavior(this);
	}
}
