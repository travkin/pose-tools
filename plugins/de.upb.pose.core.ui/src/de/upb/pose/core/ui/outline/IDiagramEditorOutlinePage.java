package de.upb.pose.core.ui.outline;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

public interface IDiagramEditorOutlinePage extends IContentOutlinePage, ISelectionListener, IPostSelectionProvider,
		ISelectionChangedListener {
	void setInput(Object input);

	void setViewer(GraphicalViewer viewer);

	void refresh();

	boolean isLinkingEnabled();
}
