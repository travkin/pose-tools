package de.upb.pose.core.ui.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import de.upb.pose.core.commands.AbstractAddElementsCommand;
import de.upb.pose.core.ui.Activator;

public abstract class InitializeDiagramWizardBase extends Wizard {
	
	private TransactionalEditingDomain editingDomain;

	private SelectDiagramResourcePage page;

	private IFile boFile;

	public InitializeDiagramWizardBase(IFile boFile) {
		this.boFile = boFile;

		setWindowTitle("Initialize Diagram");

		setHelpAvailable(false);
		setNeedsProgressMonitor(true);
	}

	@Override
	public boolean performFinish() {
		// get paths
		final String boPath = boFile.getFullPath().toString();
		final String pePath = page.getPath();

		// create runnable
		IRunnableWithProgress runnable = new IRunnableWithProgress() {
			@Override
			public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				monitor.beginTask("Creating diagram...", IProgressMonitor.UNKNOWN);

				// create uris
				URI boUri = URI.createPlatformResourceURI(boPath, true);
				URI peUri = URI.createPlatformResourceURI(pePath, true);

				Resource boResource = getResourceSet().getResource(boUri, true);
				EObject bo = getElement(boResource);

				if (bo != null) {
					// execute diagram creation command
					AbstractAddElementsCommand command = createCommand(bo, peUri);
					getEditingDomain().getCommandStack().execute(command);

					Resource peResource = command.getDiagramResource();
					try {
						peResource.save(Collections.emptyMap());
					} catch (IOException e) {
						throw new InvocationTargetException(e);
					}
				}

				monitor.done();
			}
		};

		// execute the initialization
		try {
			getContainer().run(true, false, runnable);
		} catch (InvocationTargetException e) {
			Activator.get().error(e);
			return false;
		} catch (InterruptedException e) {
			Activator.get().error(e);
			return false;
		}

		// dispose editing domain
		getEditingDomain().dispose();
		editingDomain = null;

		// open the editor
		IResource peFile = ResourcesPlugin.getWorkspace().getRoot().findMember(pePath);
		if (peFile instanceof IFile) {
			IFileEditorInput input = new FileEditorInput((IFile) peFile);
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(input, getEditorId());
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	public ResourceSet getResourceSet() {
		return getEditingDomain().getResourceSet();
	}

	public TransactionalEditingDomain getEditingDomain() {
		if (editingDomain == null) {
			editingDomain = TransactionalEditingDomain.Factory.INSTANCE.createEditingDomain();
		}
		return editingDomain;
	}

	protected abstract String getEditorId();

	@Override
	public void addPages() {
		page = new SelectDiagramResourcePage();
		addPage(page);
	}

	public abstract String getDiagramExtension();

	public IFile getModelFile() {
		return boFile;
	}

	protected abstract AbstractAddElementsCommand createCommand(EObject bo, URI peUri);

	protected abstract EObject getElement(Resource resource);
}
