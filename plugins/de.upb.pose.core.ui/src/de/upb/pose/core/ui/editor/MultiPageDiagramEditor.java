package de.upb.pose.core.ui.editor;

import java.io.IOException;
import java.util.Collections;
import java.util.EventObject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStackListener;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.MultiPageSelectionProvider;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.upb.pose.core.ui.outline.DiagramEditorOutlinePage;
import de.upb.pose.core.ui.outline.IDiagramEditorOutlinePage;
import de.upb.pose.core.ui.outline.IOutlineInputProvider;

public abstract class MultiPageDiagramEditor extends MultiPageEditorPart implements
		ITabbedPropertySheetPageContributor, IOutlineInputProvider {
	/**
	 * The overview page of the editor page. Created during a successful start-up.
	 */
	private MultiPageDiagramEditorOverviewPage overviewPage;

	/**
	 * The currently opened diagram editors.
	 */
	private final Map<Diagram, IEditorPart> editors;

	/**
	 * The editing domain for the editor itself and all diagram editors inside.
	 */
	private TransactionalEditingDomain editingDomain;

	/**
	 * The last command on the undo-stack when the editor is saved. Used to calculate the dirty state and allow
	 * undo/redo functionality although the editor is saved.
	 */
	private Command savedCommand;

	/**
	 * The container of the contained/available diagrams.
	 */
	private ContainerShape rootElement;

	/**
	 * The outline page of the editor.
	 */
	private IDiagramEditorOutlinePage contentOutlinePage;

	/**
	 * The action registry which is used for the overview page.
	 */
	private ActionRegistry actionRegistry;

	private AdapterFactory adapterFactory;

	public MultiPageDiagramEditor() {
		editors = new LinkedHashMap<Diagram, IEditorPart>();
	}

	public void openDiagram(Diagram diagram) {
		Assert.isNotNull(diagram);

		int index = getEditor(diagram);

		if (index == -1) {
			InnerDiagramEditor editor = createDiagramEditor();

			URI uri = EcoreUtil.getURI(diagram);
			String diagramTypeID = diagram.getDiagramTypeId();
			String diagramTypeProviderID = GraphitiUi.getExtensionManager().getDiagramTypeProviderId(diagramTypeID);
			
			IEditorInput input = new DiagramEditorInput(uri, diagramTypeProviderID);

			try {
				index = addPage(editor, input);
				editors.put(diagram, editor);
			} catch (PartInitException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}

			setPageImage(index, getEditorImage(diagram));
			setPageText(index, getEditorName(diagram));
			setPageCloseable(index);
		}

		setActivePage(index);
	}

	private int getEditor(Diagram diagram) {
		IEditorPart editor = editors.get(diagram);

		// search for existing
		if (editor != null) {
			// check all pages
			for (int index = 0; index < getPageCount(); index++) {
				IEditorPart existing = getEditor(index);
				if (editor.equals(existing)) {
					return index;
				}
			}
		}
		return -1;
	}

	protected InnerDiagramEditor createDiagramEditor() {
		return new InnerDiagramEditor(getEditingDomain());
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		// check input
		Assert.isTrue(input instanceof IFileEditorInput);
		
		// check file
		IFile file = ((IFileEditorInput) input).getFile();
		Assert.isNotNull(file);

		// check resource
		URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		Resource resource = getResourceSet().getResource(uri, true);
		Assert.isNotNull(resource);

		// check resource contents
		List<EObject> contents = resource.getContents();
		Assert.isTrue(contents.size() == 1);

		// check resource
		EObject element = contents.get(0);
		Assert.isTrue(element instanceof ContainerShape);
		rootElement = (ContainerShape) element;

		super.init(site, input);

		setPartName(file.getName());

		getCommandStack().addCommandStackListener(new CommandStackListener() {
			@Override
			public void commandStackChanged(EventObject event) {
				handleCommandStackChanged();
			}
		});
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		ResourceSet resourceSet = getResourceSet();

		Assert.isNotNull(resourceSet);

		try {
			for (Resource resource : resourceSet.getResources()) {
				resource.save(Collections.emptyMap());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		savedCommand = getCommandStack().getUndoCommand();
		firePropertyChange(PROP_DIRTY);
	}

	private ResourceSet getResourceSet() {
		return getEditingDomain().getResourceSet();
	}

	public void execute(Command command) {
		getCommandStack().execute(command);
	}

	@Override
	public boolean isDirty() {
		return savedCommand != getCommandStack().getUndoCommand();
	}

	protected BasicCommandStack getCommandStack() {
		return (BasicCommandStack) getEditingDomain().getCommandStack();
	}

	public AdapterFactory getAdapterFactory() {
		if (adapterFactory == null) {
			TransactionalEditingDomain editingDomain = getEditingDomain();
			if (editingDomain instanceof AdapterFactoryEditingDomain) {
				adapterFactory = ((AdapterFactoryEditingDomain) getEditingDomain()).getAdapterFactory();
			} else {
				adapterFactory = new ComposedAdapterFactory(Registry.INSTANCE);
			}
		}
		return adapterFactory;
	}

	@Override
	public void dispose() {
		super.dispose();

		if (adapterFactory instanceof ComposedAdapterFactory) {
			((ComposedAdapterFactory) adapterFactory).dispose();
		}
		getEditingDomain().dispose();
	}

	protected TransactionalEditingDomain getEditingDomain() {
		if (editingDomain == null) {
			editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
		}
		return editingDomain;
	}

	protected Image getEditorImage(Diagram diagram) {
		return null;
	}

	protected String getEditorName(Diagram pe) {
		EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

		if (bo != null) {
			String name = getEditorName(bo);
			if (name != null) {
				return name;
			}
		}

		return pe.getName();
	}

	protected String getEditorName(EObject element) {
		return null;
	}

	private void setPageCloseable(final int index) {
		Composite container = getContainer();
		if (container instanceof CTabFolder) {
			CTabItem tabItem = ((CTabFolder) container).getItem(index);
			tabItem.setShowClose(true);
			// // TODO: close tab/enhance page handling!
			// tabItem.addDisposeListener(new DisposeListener() {
			// @Override
			// public void widgetDisposed(DisposeEvent e) {
			// // System.out.println("closed tab " + index);
			// // System.out.println(getPageCount());
			// // removePage(index);
			// }
			// });
		}
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class required) {
		if (ActionRegistry.class.equals(required)) {
			return getActionRegistry();
		}
		if (IContentOutlinePage.class.equals(required)) {
			return getContentOutlinePage();
		}
		if (IPropertySheetPage.class.equals(required)) {
			return new TabbedPropertySheetPage(this);
		}
		return super.getAdapter(required);
	}

	private ActionRegistry getActionRegistry() {
		if (actionRegistry == null) {
			actionRegistry = createActionRegistry();
		}
		return actionRegistry;
	}

	private ActionRegistry createActionRegistry() {
		return new ActionRegistry();
	}

	public IDiagramEditorOutlinePage getContentOutlinePage() {
		if (contentOutlinePage == null) {
			contentOutlinePage = createOutlinePage();
		}
		return contentOutlinePage;
	}

	protected IDiagramEditorOutlinePage createOutlinePage() {
		return new DiagramEditorOutlinePage(this);
	}

	@Override
	public void removePage(int index) {
		// // TODO: delete editor/enhance page handling!
		System.out.println("remove page " + index);
		// for (EObject element : getEditors().keySet()) {
		// if (getEditors().get(element).intValue() == index) {
		// // System.out.println("delete this: " + index);
		// }
		// }

		super.removePage(index);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void doSaveAs() {
		// not supported
	}

	public Diagram getDiagram(EObject element) {
		Assert.isNotNull(element);

		// search resource tree
		for (Shape child : rootElement.getChildren()) {
			if (child instanceof Diagram) {
				EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(child);
				if (element.equals(bo)) {
					return (Diagram) child;
				}
			}
		}

		throw new RuntimeException();
	}

	@Override
	public Object getOutlinePageInput() {
		return rootElement;
	}

	public ContainerShape getRootElement() {
		return rootElement;
	}

	@Override
	protected void pageChange(int index) {
		super.pageChange(index);

		IEditorPart editor = getActiveEditor();

		// outline page
		if (contentOutlinePage != null) {
			if (editor instanceof DiagramEditor) {
				IDiagramTypeProvider diagramTypeProvider = ((DiagramEditor) editor).getDiagramTypeProvider();
				Diagram diagram = diagramTypeProvider != null ? diagramTypeProvider.getDiagram() : null;
				GraphicalViewer viewer = ((DiagramEditor) editor).getGraphicalViewer();
				
				if (diagram != null && viewer != null) {
					contentOutlinePage.setInput(diagram);
					contentOutlinePage.setViewer(viewer);
				} else {
					System.err.println("Could not initialize content outline page due to editor initialization problems.");
					contentOutlinePage.setInput(rootElement);
					contentOutlinePage.setViewer(null);
				}
			} else {
				contentOutlinePage.setInput(rootElement);
				contentOutlinePage.setViewer(null);
			}
		}

		if (editor == null) {
			fireSelectionChanged(overviewPage.getSelectionProvider());
		}
	}

	@Override
	public IEditorPart getActiveEditor() {
		return super.getActiveEditor();
	}

	public void fireSelectionChanged(ISelectionProvider provider) {
		ISelectionProvider outerProvider = getSite().getSelectionProvider();
		if (outerProvider instanceof MultiPageSelectionProvider) {
			SelectionChangedEvent event = new SelectionChangedEvent(provider, provider.getSelection());

			MultiPageSelectionProvider siteProvider = (MultiPageSelectionProvider) outerProvider;
			siteProvider.fireSelectionChanged(event);
			siteProvider.firePostSelectionChanged(event);
		}
	}

	@Override
	protected void createPages() {
		overviewPage = createOverviewPage();
		overviewPage.createControl(getContainer());

		int index = addPage(overviewPage.getControl());
		setPageImage(index, overviewPage.getPageImage());
		setPageText(index, overviewPage.getPageText());
	}

	protected abstract MultiPageDiagramEditorOverviewPage createOverviewPage();

	protected void handleCommandStackChanged() {
		// refresh overview page
		overviewPage.refresh();

		// refresh content outline page
		if (contentOutlinePage != null) {
			contentOutlinePage.refresh();
		}
		firePropertyChange(PROP_DIRTY);
	}
}
