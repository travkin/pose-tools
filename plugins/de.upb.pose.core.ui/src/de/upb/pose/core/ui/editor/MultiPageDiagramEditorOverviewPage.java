package de.upb.pose.core.ui.editor;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.part.Page;

import de.upb.pose.core.ui.CoreUiImages;

public abstract class MultiPageDiagramEditorOverviewPage extends Page implements ISelectionChangedListener,
		ISelectionListener {
	private final MultiPageDiagramEditor editor;

	private FormToolkit toolkit;

	private ScrolledForm form;

	private ISelectionProvider provider;

	public MultiPageDiagramEditorOverviewPage(MultiPageDiagramEditor editor) {
		this.editor = editor;
	}

	@Override
	public void createControl(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());

		// create form
		form = toolkit.createScrolledForm(parent);
		toolkit.decorateFormHeading(form.getForm());
		form.setImage(getHeaderImage());
		form.setText(getHeaderText());

		// create toolbar
		IToolBarManager toolBarManager = form.getToolBarManager();
		createToolBar(toolBarManager);
		toolBarManager.update(true);

		// create body
		form.getBody().setLayout(new FormLayout());

		createWidgets(toolkit, form.getBody());

		editor.getEditorSite().getPage().addSelectionListener(this);
	}

	public Image getPageImage() {
		return getHeaderImage();
	}

	protected Image getHeaderImage() {
		return CoreUiImages.get(CoreUiImages.OUTLINE_THUMBNAIL);
	}

	protected String getHeaderText() {
		return "Multi Diagram Editor";
	}

	protected void createToolBar(IToolBarManager tbm) {
		// nothing added by default
	}

	protected abstract void createWidgets(FormToolkit toolkit, Composite parent);

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// only proceed if coming from an other part
		if (editor.equals(part)) {
			return;
		}

		// only proceed if we are the active page
		if (editor.getActiveEditor() != null) {
			return;
		}

		// only proceed when coming from navigator or outline and linking is enabled
		if (part instanceof CommonNavigator && !((CommonNavigator) part).isLinkingEnabled()) {
			return;
		}

		// only proceed when coming from outline and linking is enabled
		if (IPageLayout.ID_OUTLINE.equals(part.getSite().getId()) && !editor.getContentOutlinePage().isLinkingEnabled()) {
			return;
		}

		// deliver it
		selectionChanged(selection);
	}

	protected void selectionChanged(ISelection selection) {
		// nothing by default
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		editor.fireSelectionChanged(event.getSelectionProvider());
	}

	@Override
	public void dispose() {
		editor.getEditorSite().getPage().removeSelectionListener(this);

		if (toolkit != null) {
			toolkit.dispose();
		}

		super.dispose();
	}

	@Override
	public Control getControl() {
		return form;
	}

	@Override
	public void setFocus() {
		form.setFocus();
	}

	public ISelectionProvider getSelectionProvider() {
		return provider;
	}

	public String getPageText() {
		return "Overview";
	}

	public void refresh() {
		// nothing by default
	}

	protected void openElement(EObject element) {
		openDiagram(editor.getDiagram(element));
	}

	protected void openDiagram(Diagram diagram) {
		editor.openDiagram(diagram);
	}

	protected void setSelectionProvider(ISelectionProvider provider) {
		this.provider = provider;
	}

	protected Diagram getDiagram(EObject element) {
		return editor.getDiagram(element);
	}

	protected TransactionalEditingDomain getEditingDomain() {
		return editor.getEditingDomain();
	}

	protected void execute(Command command) {
		editor.execute(command);
	}

	protected void updateToolBar() {
		if (form != null) {
			form.getToolBarManager().update(true);
		}
	}

	protected AdapterFactory getAdapterFactory() {
		return editor.getAdapterFactory();
	}

	protected ContainerShape getRootElement() {
		return editor.getRootElement();
	}
}
