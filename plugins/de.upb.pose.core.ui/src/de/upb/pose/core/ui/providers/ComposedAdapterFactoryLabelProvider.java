package de.upb.pose.core.ui.providers;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class ComposedAdapterFactoryLabelProvider extends LabelProvider {
	private final AdapterFactoryLabelProvider aflf;

	public ComposedAdapterFactoryLabelProvider() {
		AdapterFactory af = new ComposedAdapterFactory(Registry.INSTANCE);
		aflf = new AdapterFactoryLabelProvider(af);
	}

	@Override
	public void dispose() {
		aflf.dispose();
		super.dispose();
	}

	@Override
	public Image getImage(Object element) {
		return aflf.getImage(element);
	}

	@Override
	public String getText(Object element) {
		return aflf.getText(element);
	}
}
