package de.upb.pose.core.ui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.model.WorkbenchViewerComparator;

public class SelectDiagramResourcePage extends WizardPage {
	private IContainer container;
	private TreeViewer containerViewer;

	private String name;
	private Text nameText;

	public SelectDiagramResourcePage() {
		super(SelectDiagramResourcePage.class.getSimpleName());

		setTitle("Initialize Story Diagram");
		setDescription("Select the location for the story diagram resource.");
	}

	@Override
	public void createControl(Composite parent) {
		Composite page = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(page);
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(page);

		// container
		Group group = new Group(page, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(group);
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(group);
		group.setText("Container");

		Tree tree = new Tree(group, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(tree);

		containerViewer = new TreeViewer(tree);
		containerViewer.setContentProvider(new WorkbenchContentProvider());
		containerViewer.setLabelProvider(new WorkbenchLabelProvider());
		containerViewer.setComparator(new WorkbenchViewerComparator());
		containerViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
		containerViewer.addFilter(new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parent, Object element) {
				return element instanceof IContainer;
			}
		});
		containerViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) containerViewer.getSelection();
				container = (IContainer) selection.getFirstElement();
				setPageComplete(isValid());
			}
		});

		// name
		Composite nameComposite = new Composite(page, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(nameComposite);
		GridLayoutFactory.fillDefaults().margins(6, 6).numColumns(2).applyTo(nameComposite);

		Label nameLabel = new Label(nameComposite, SWT.TRAIL);
		nameLabel.setText("File Name:");
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).applyTo(nameLabel);

		nameText = new Text(nameComposite, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(nameText);
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				name = nameText.getText();
				setPageComplete(isValid());
			}
		});

		// set initial values
		containerViewer.setSelection(new StructuredSelection(container));
		nameText.setText(name);

		setPageComplete(isValid());

		setControl(page);
	}

	private boolean isValid() {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		if (root.findMember(getIPath()) instanceof IFile) {
			setMessage("The file already exists.", WARNING);
		} else {
			setMessage(null);
		}

		return true;
	}

	public String getPath() {
		return getIPath().toString();
	}

	private IPath getIPath() {
		IPath path = container.getFullPath().append(name);
		if (!getWizard().getDiagramExtension().equals(path.getFileExtension())) {
			path = path.addFileExtension(getWizard().getDiagramExtension());
		}
		return path;
	}

	@Override
	public void setWizard(IWizard wizard) {
		super.setWizard(wizard);

		IFile file = getWizard().getModelFile();
		container = file.getParent();
		name = file.getFullPath().removeFileExtension().addFileExtension(getWizard().getDiagramExtension())
				.lastSegment();
	}

	@Override
	public InitializeDiagramWizardBase getWizard() {
		return (InitializeDiagramWizardBase) super.getWizard();
	}
}
