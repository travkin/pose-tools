package de.upb.pose.core.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.upb.pose.core.ui.Activator;
import de.upb.pose.core.ui.CoreUiImages;
import de.upb.pose.core.ui.dialogs.SelectWorkspaceFileDialog;

public class SelectModelSourcePage extends WizardPageBase {
	private Resource resource;

	private Button newResourceButton;

	private Button existingResourceButton;
	private Text existingResourceText;
	private Button existingFindButton;
	private TreeViewer existingTreeViewer;

	public SelectModelSourcePage() {
		super(SelectModelSourcePage.class.getName());
	}

	@Override
	protected void createWidgets(Composite parent) {
		createNewResourceControl(parent);
		createExistingResourceControl(parent);

		newResourceButton.setSelection(true);
		handleExistingResourceSelected(false);
	}

	private void createNewResourceControl(Composite parent) {
		newResourceButton = new Button(parent, SWT.RADIO);
		newResourceButton.setText("Create New Model");
	}

	private void createExistingResourceControl(Composite parent) {
		existingResourceButton = new Button(parent, SWT.RADIO);
		existingResourceButton.setText("Use Existing Model");

		Group existingResourceGroup = new Group(parent, SWT.NONE);
		existingResourceGroup.setText("Select Model Element");
		existingResourceGroup.setLayout(new GridLayout(1, false));
		existingResourceGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Composite existingResourceComposite = new Composite(existingResourceGroup, SWT.NONE);
		existingResourceComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		existingResourceComposite.setBounds(0, 0, 64, 64);
		existingResourceComposite.setLayout(new GridLayout(3, false));

		Label lblResource = new Label(existingResourceComposite, SWT.NONE);
		lblResource.setText("Resource:");

		existingResourceText = new Text(existingResourceComposite, SWT.BORDER);
		existingResourceText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		existingResourceText.setBounds(0, 0, 76, 21);

		existingFindButton = new Button(existingResourceComposite, SWT.NONE);
		existingFindButton.setText("Find...");
		existingFindButton.setImage(CoreUiImages.get(CoreUiImages.FIND));

		Composite composite = new Composite(existingResourceGroup, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		existingTreeViewer = new TreeViewer(composite, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(existingTreeViewer.getControl());

		final ITreeContentProvider contentProvider = new AdapterFactoryContentProvider(getAdapterFactory());
		existingTreeViewer.setContentProvider(contentProvider);
		existingTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(getAdapterFactory()));
		existingTreeViewer.addFilter(new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parent, Object element) {
				if (element instanceof EObject) {
					if (getWizard().getType().isSuperTypeOf(((EObject) element).eClass())) {
						return true;
					}
				}

				for (Object child : contentProvider.getChildren(element)) {
					if (select(viewer, element, child)) {
						return true;
					}
				}

				return false;
			}
		});
	}

	private AdapterFactory getAdapterFactory() {
		return getWizard().getAdapterFactory();
	}

	protected void handleExistingResourceSelected(boolean existing) {
		getWizard().setShouldCreateModel(!existing);

		existingResourceText.setEnabled(existing);
		existingFindButton.setEnabled(existing);
		existingTreeViewer.getControl().setEnabled(existing);
	}

	@Override
	protected MultiStatus doValidate(MultiStatus status) {
		String id = Activator.get().getSymbolicName();

		if (!getWizard().shouldCreateModel()) {
			// check model
			if (getWizard().getModel() == null) {
				status.add(new Status(IStatus.ERROR, id, "Select a model element for the diagram!"));
			}
		}

		return status;
	}

	private ResourceSet getResourceSet() {
		return ((NewDiagramWizard) getWizard()).getResourceSet();
	}

	@Override
	public NewDiagramWizard getWizard() {
		return (NewDiagramWizard) super.getWizard();
	}

	protected void loadResource(final String path) {
		if (resource != null) {
			String oldPath = resource.getURI().toPlatformString(true);
			if (oldPath.equals(path)) {
				return;
			}
		}

		resource = null;

		if (ResourcesPlugin.getWorkspace().getRoot().findMember(path) instanceof IFile) {
			// create runnable
			IRunnableWithProgress runnable = new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
					URI uri = URI.createPlatformResourceURI(path, true);
					resource = getResourceSet().getResource(uri, true);
				}
			};

			try {
				getContainer().run(true, false, runnable);
				setErrorMessage(null);
			} catch (InvocationTargetException e) {
				setErrorMessage("The resource could not be loaded!");
			} catch (InterruptedException e) {
				setErrorMessage("The resource could not be loaded!");
			}
		} else {
			setErrorMessage("The resource does not exist!");
		}

		existingTreeViewer.setInput(resource);
		validate();
	}

	@Override
	protected void hookListeners() {
		// add mode listener
		SelectionAdapter modeAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleExistingResourceSelected(e.getSource().equals(existingResourceButton));
				validate();
			}
		};
		existingResourceButton.addSelectionListener(modeAdapter);
		newResourceButton.addSelectionListener(modeAdapter);

		// load resource on [ENTER] key
		existingResourceText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.character == SWT.CR) {
					loadResource(existingResourceText.getText());
				}
			}
		});

		// load resource on focus lost
		existingResourceText.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				loadResource(existingResourceText.getText());
			}
		});

		// add find button listener
		existingFindButton.addSelectionListener(new SelectionAdapter() {
			private SelectWorkspaceFileDialog dialog = new SelectWorkspaceFileDialog();

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (dialog.open() == Window.OK) {
					String path = dialog.getElement().getFullPath().toString();

					existingResourceText.setText(path);
					loadResource(path);
				}
			}
		});

		// select model element on tree selection
		existingTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object selected = ((IStructuredSelection) existingTreeViewer.getSelection()).getFirstElement();
				if (selected instanceof EObject) {
					if (getWizard().getType().isSuperTypeOf(((EObject) selected).eClass())) {
						getWizard().setModel((EObject) selected);
					} else {
						getWizard().setModel(null);
					}
				} else {
					getWizard().setModel(null);
				}
				validate();
			}
		});
	}
}
