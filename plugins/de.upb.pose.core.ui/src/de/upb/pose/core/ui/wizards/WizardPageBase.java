package de.upb.pose.core.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import de.upb.pose.core.ui.Activator;

public abstract class WizardPageBase extends WizardPage {
	
	private List<WizardPageCompletionListener> pageCompleteListeners = new ArrayList<WizardPageCompletionListener>(2);
	private boolean pageCompleted = false;
	
	protected WizardPageBase(String name) {
		super(name);
	}

	@Override
	public final void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(composite);
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(composite);

		createWidgets(composite);

		hookListeners();

		validate();

		setControl(composite);
	}

	protected abstract void createWidgets(Composite parent);

	protected void hookListeners() {
		// nothing by default
	}

	public final void validate() {
		IStatus status = doValidate(new MultiStatus(Activator.get().getSymbolicName(), IStatus.OK, null, null));

		// page is complete
		if (status == null || status.isOK()) {
			setMessage(null);
			setErrorMessage(null);
			setPageComplete(true);
		} else {
			// there are messages
			StringBuilder builder = new StringBuilder();

			// collect those with highest severity
			for (IStatus child : status.getChildren()) {
				if (child.getSeverity() == status.getSeverity()) {
					if (builder.length() > 0) {
						builder.append('\n');
						builder.append(' ');
					}
					builder.append(child.getMessage());
				}
			}
			
			// show message
			switch (status.getSeverity()) {
				case IStatus.INFO:
					setErrorMessage(null);
					setMessage(builder.toString(), INFORMATION);
					setPageComplete(true);
					break;
				case IStatus.WARNING:
					setErrorMessage(null);
					setMessage(builder.toString(), WARNING);
					setPageComplete(true);
					break;
				case IStatus.ERROR:
				case IStatus.CANCEL:
					setErrorMessage(builder.toString());
					setMessage(null);
					setPageComplete(false);
					break;
				default:
			}
		}

		this.pageCompleted = isPageComplete();
		notifyPageCompletionChanged(this.pageCompleted);
	}
	
	private void notifyPageCompletionChanged(boolean pageComplete) {
		for (WizardPageCompletionListener listener: pageCompleteListeners) {
			if (pageComplete) {
				listener.pageCompleted(this);
			} else {
				listener.pageIncomplete(this);
			}
		}
	}
	
	public void addPageCompleteListener(WizardPageCompletionListener listener) {
		this.pageCompleteListeners.add(listener);
	}
	
	public void removePageCompleteListener(WizardPageCompletionListener listener) {
		this.pageCompleteListeners.remove(listener);
	}

	protected MultiStatus doValidate(MultiStatus status) {
		return status;
	}
}
