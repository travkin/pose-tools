package de.upb.pose.core.ui.providers;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.util.PropertiesUtil;

public class PropertiesLabelProvider extends LabelProvider {
	private AdapterFactoryLabelProvider aflp;

	public PropertiesLabelProvider() {
		AdapterFactory af = new ComposedAdapterFactory(Registry.INSTANCE);
		aflp = new AdapterFactoryLabelProvider(af);
	}

	@Override
	public final Image getImage(Object element) {
		if (element instanceof IStructuredSelection && ((IStructuredSelection) element).size() > 1) {
			return null;
		}
		return getImage(PropertiesUtil.getAdapted(element));
	}

	protected Image getImage(EObject element) {
		return aflp.getImage(element);
	}

	@Override
	public final String getText(Object element) {
		if (element instanceof IStructuredSelection && ((IStructuredSelection) element).size() > 1) {
			int count = ((IStructuredSelection) element).size();

			return String.format("%1s items selected", count);
		}
		return getText(PropertiesUtil.getAdapted(element));
	}

	protected String getText(EObject element) {
		return aflp.getText(element);
	}

	@Override
	public void dispose() {
		aflp.dispose();

		super.dispose();
	}
}
