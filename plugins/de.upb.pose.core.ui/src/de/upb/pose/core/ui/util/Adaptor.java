package de.upb.pose.core.ui.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

public final class Adaptor {
	private Adaptor() {
		// hide constructor
	}

	public static List<EObject> getAll(ISelection selection) {
		List<EObject> elements = new ArrayList<EObject>();

		if (selection instanceof IStructuredSelection) {
			for (Object part : ((IStructuredSelection) selection).toArray()) {
				EObject bo = get(part);
				if (bo != null) {
					elements.add(bo);
				}
			}
		}

		return elements;
	}

	public static EObject get(Object element) {
		if (element instanceof EditPart) {
			return get((EditPart) element);
		}
		if (element instanceof PictogramElement) {
			return get((PictogramElement) element);
		}
		if (element instanceof EObject) {
			return (EObject) element;
		}
		return null;
	}

	public static EObject get(EditPart element) {
		return get(element.getModel());
	}

	public static EObject get(PictogramElement element) {
		return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(element);
	}
}
