package de.upb.pose.core.ui.providers;

public interface Binder<E, V> {
	V getBinding(E element);

	void setBinding(E element, V value);
}
