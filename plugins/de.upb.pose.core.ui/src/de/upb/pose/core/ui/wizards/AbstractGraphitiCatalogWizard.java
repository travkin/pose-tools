package de.upb.pose.core.ui.wizards;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.FileEditorInput;

import de.upb.pose.core.ui.Activator;

public abstract class AbstractGraphitiCatalogWizard<T extends EObject> extends Wizard implements INewWizard {
	
	private IWorkbench workbench;
	private boolean tryOpeningCreatedDiagramFileInEditor;

	private CreateResourcePage modelPage;
	private CreateResourcePage diagramsPage;
	
	private URI modelResourceUri = null;
	private URI diagramsResourceURI = null;

	public AbstractGraphitiCatalogWizard() {
		this(true);
	}
	
	public AbstractGraphitiCatalogWizard(boolean tryOpeningCreatedDiagramFileInEditor) {
		super();

		this.tryOpeningCreatedDiagramFileInEditor = tryOpeningCreatedDiagramFileInEditor;
		
		setHelpAvailable(false);
		setNeedsProgressMonitor(true);
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
	}
	
	@Override
	public void addPages() {
		modelPage = new CreateResourcePage(getModelFileExtension());
		modelPage.setDescription("Select the resource for the model.");
		addPage(modelPage);

		diagramsPage = new CreateResourcePage(getDiagramFileExtension());
		diagramsPage.setDescription("Select the resource for the diagrams.");
		addPage(diagramsPage);
	}
	
	protected CreateResourcePage getModelPage() {
		return this.modelPage;
	}
	
	protected CreateResourcePage getDiagramPage() {
		return this.diagramsPage;
	}
	
	public URI getModelResourceURI() {
		return this.modelResourceUri;
	}
	
	public URI getDiagramsResourceURI() {
		return this.diagramsResourceURI;
	}
	
	/**
	 * Should create the model element which represents the single root of the model resource.
	 * 
	 * @return Returns the model root element.
	 */
	protected abstract T createModelRoot();
	
	protected abstract ContainerShape createDiagramsRoot(T modelRoot);
	
	protected abstract String getModelFileExtension();
	protected abstract String getDiagramFileExtension();
	
	protected abstract String getEditorId();

	protected void postCreate(EObject model, ContainerShape diagrams) {
		// nothing by default
	}

	@Override
	public boolean performFinish() {
		ResourceSet resourceSet = new ResourceSetImpl();

		// model resource
		String modelPath = modelPage.getSelectedFileContainer().getFullPath().append(modelPage.getFileNameWithExtension()).toString();
		this.modelResourceUri = URI.createPlatformResourceURI(modelPath, true);
		Resource modelResource = resourceSet.createResource(this.modelResourceUri);
		
		// model element
		T model = createModelRoot();
		modelResource.getContents().add(model);
		
		
		// diagrams resource
		String diagramsPath = diagramsPage.getSelectedFileContainer().getFullPath().append(diagramsPage.getFileNameWithExtension()).toString();
		this.diagramsResourceURI = URI.createPlatformResourceURI(diagramsPath, true);
		Resource diagramsResource = resourceSet.createResource(this.diagramsResourceURI);

		// diagrams element
		ContainerShape diagrams = createDiagramsRoot(model);
		diagramsResource.getContents().add(diagrams);

		postCreate(model, diagrams);

		// save
		try {
			modelResource.save(Collections.emptyMap());
			diagramsResource.save(Collections.emptyMap());
		} catch (IOException e) {
			e.printStackTrace();
			Activator.get().error("Could not save model and diagram resources.", e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(),
					"Creating files failed", "Model and diagrams files couldn't be created.");
			return false;
		}

		// open editor
		if (this.tryOpeningCreatedDiagramFileInEditor
				&& getEditorId() != null
				&& this.workbench != null) {
			IFile file = (IFile) ResourcesPlugin.getWorkspace().getRoot().findMember(diagramsPath);
			IEditorInput input = new FileEditorInput(file);
			try {
				this.workbench.getActiveWorkbenchWindow().getActivePage().openEditor(input, getEditorId());
			} catch (PartInitException e) {
				e.printStackTrace();
				Activator.get().error("Could not open editor with id " + getEditorId() + " and file " + file.getFullPath() + ".", e);
				MessageDialog.openError(Display.getCurrent().getActiveShell(),
						"Opening editor failed", "Created diagram file couldn't be opened in editor.");
				return false;
			}
		}

		return true;
	}

}
