package de.upb.pose.core.ui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

import de.upb.pose.core.ui.Activator;

public class OLDCreateResourcePage extends WizardPageBase {
	private static final String DEFAULT_RESOURCE_NAME = "default";

	private final String resourceExtension;

	private IContainer resourceContainer;
	private String resourceName;

	private TreeViewer containerViewer;
	private Text nameText;

	public OLDCreateResourcePage(String extension) {
		super(OLDCreateResourcePage.class.getName() + extension);

		this.resourceContainer = ResourcesPlugin.getWorkspace().getRoot();
		this.resourceName = DEFAULT_RESOURCE_NAME;
		this.resourceExtension = extension;
	}

	public void setDefaultName(String value) {
		this.resourceName = value;
	}

	public String getPath() {
		return resourceContainer.getFullPath().append(resourceName).toString();
	}

	@Override
	protected void createWidgets(Composite parent) {
		// initialize selection
		IStructuredSelection selection = ((NewDiagramWizard) getWizard()).getSelection();
		if (selection != null && !selection.isEmpty()) {
			for (Object selected : selection.toArray()) {
				if (selected instanceof IContainer) {
					resourceContainer = (IContainer) selected;
				} else if (selected instanceof IFile) {
					resourceContainer = ((IFile) selected).getParent();
					resourceName = ((IFile) selected).getName();
				}
			}
		}
		if (resourceContainer != null) {
			IPath path = resourceContainer.getFullPath().append(resourceName);
			getWizard().setPath(this, path);
		}

		// default
		Composite areaComposite = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(areaComposite);
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(areaComposite);

		// container tree
		Tree containerTree = new Tree(areaComposite, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(containerTree);

		containerViewer = new TreeViewer(containerTree);
		containerViewer.setContentProvider(new WorkbenchContentProvider());
		containerViewer.setLabelProvider(new WorkbenchLabelProvider());
		containerViewer.setComparator(new ResourceComparator(ResourceComparator.TYPE));
		containerViewer.addFilter(new FileViewerFiler(resourceExtension));

		Composite nameComposite = new Composite(areaComposite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(nameComposite);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(nameComposite);

		Label nameLabel = new Label(nameComposite, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.TRAIL, SWT.CENTER).applyTo(nameLabel);

		nameText = new Text(nameComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(nameText);

		// initialize values
		containerViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
		if (resourceContainer != null) {
			containerViewer.setSelection(new StructuredSelection(resourceContainer));
		}
		nameLabel.setText("Name:");
		nameText.setText(getName(resourceName));
	}

	@Override
	public NewDiagramWizard getWizard() {
		return (NewDiagramWizard) super.getWizard();
	}

	private String getName(String name) {
		if (name == null) {
			return this.resourceName + '.' + resourceExtension;
		}

		return name + '.' + resourceExtension;
	}

	protected void hookListeners() {
		// validate when viewer selection changes
		containerViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object element = ((IStructuredSelection) containerViewer.getSelection()).getFirstElement();

				IPath path = null;

				IContainer container = null;
				String name = nameText.getText();

				if (element instanceof IContainer) {
					container = (IContainer) element;
				} else if (element instanceof IFile) {
					container = ((IFile) element).getParent();
					name = ((IFile) element).getName();
				}

				if (container != null) {
					path = container.getFullPath().append(name);

					getWizard().setPath(OLDCreateResourcePage.this, path);
				}

				validate();
			}
		});

		// validate when name text changes
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				resourceName = nameText.getText();

				validate();
			}
		});
	}

	protected MultiStatus doValidate(MultiStatus status) {
		String id = Activator.get().getSymbolicName();

		// check container
		if (resourceContainer == null) {
			status.add(new Status(IStatus.ERROR, id, "You have to select a container for the resource!"));
		}

		// check name
		if (resourceName == null || resourceName.trim().isEmpty()) {
			status.add(new Status(IStatus.ERROR, id, "You have to select a name for the resource!"));
		}

		// show status
		if (status.isOK()) {
			setMessage(null);
			setErrorMessage(null);
			setPageComplete(true);
		} else {
			for (IStatus child : status.getChildren()) {
				switch (child.getSeverity()) {
				case IStatus.WARNING:
					setMessage(child.getMessage(), WARNING);
					break;
				case IStatus.ERROR:
					setErrorMessage(child.getMessage());
					setPageComplete(false);
					break;
				default:
					break;
				}
			}
		}

		return status;
	}
}
