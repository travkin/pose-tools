package de.upb.pose.core.ui.providers;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;

public class ContainmentContentProvider extends ArrayContentProvider implements ITreeContentProvider {
	@Override
	public Object[] getElements(Object element) {
		return getChildren(element);
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	@Override
	public Object[] getChildren(Object element) {
		if (element instanceof Resource) {
			Collection<EObject> contents = ((Resource) element).getContents();
			return contents.toArray(new EObject[contents.size()]);
		} else if (element instanceof EObject) {
			Collection<EObject> contents = ((EObject) element).eContents();
			return contents.toArray(new EObject[contents.size()]);
		} else if (element instanceof Collection<?>) {
			Collection<?> contents = (Collection<?>) element;
			return contents.toArray(new Object[contents.size()]);
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof EObject) {
			return ((EObject) element).eContainer();
		}
		return null;
	}
}
