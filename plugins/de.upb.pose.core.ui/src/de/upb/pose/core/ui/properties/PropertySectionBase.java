package de.upb.pose.core.ui.properties;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.EditPart;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.platform.IDiagramEditor;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.ISection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import de.upb.pose.core.ui.Activator;
import de.upb.pose.core.ui.CoreUiColors;
import de.upb.pose.core.ui.CoreUiImages;
import de.upb.pose.core.util.PropertiesUtil;
import de.upb.pose.core.util.State;

public abstract class PropertySectionBase implements ISection {
	
	protected static final String EMPTY = ""; //$NON-NLS-1$
	protected static final int LABEL_WIDTH = 120;
	protected static final int MARGIN = 4;

	private TabbedPropertySheetPage page;

	private IDiagramEditor editor;
	private TransactionalEditingDomain editingDomain;
	private EObject element;

	private Adapter listener;
	private ISelection selection;
	private PictogramElement pe;
	
	/**
	 * Getter of the currently selected element.
	 * 
	 * @return Returns the currently selected element.
	 */
	protected final EObject getElement() {
		return element;
	}
	
	protected final PictogramElement getPE() {
		return pe;
	}

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage page) {
		this.page = page;

		FormLayout layout = new FormLayout();
		layout.marginTop = 6;
		layout.marginRight = 6;
		layout.marginLeft = 6;

		Composite section = page.getWidgetFactory().createFlatFormComposite(parent);
		section.setLayout(layout);

		createWidgets(section, page.getWidgetFactory());
		layoutWidgets();
		hookWidgetListeners();
	}

	protected abstract void createWidgets(Composite parent, TabbedPropertySheetWidgetFactory factory);

	protected void layoutWidgets() {
		// nothing here
	}

	protected void hookWidgetListeners() {
		// nothing here
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		if (selection.equals(this.selection)) {
			return;
		}
		
		this.selection = selection;
		
		// TODO this class does not properly work with the specification editor and its main page (no editing domain)

		removeAdapter();
		saveSelectedModelElementAndPE(selection);
		addAdapter();

		editingDomain = element != null ? TransactionUtil.getEditingDomain(element) : null;
		editor = editingDomain != null ? PropertiesUtil.getEditor(part) : null;
	}
	
	private Object getFirstSelectedElement(ISelection selection) {
		if (selection instanceof IStructuredSelection && !((IStructuredSelection) selection).isEmpty()) {
			return ((IStructuredSelection) selection).getFirstElement();
		}
		return null;
	}
	
	private Object getModelElementFromEditPart(EditPart editPart) {
		if (editPart != null) {
			return editPart.getModel();
		}
		return null;
	}
	
	protected EObject getModelElementFromPictogramElement(PictogramElement pe) {
		if (pe != null) {
			return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
		}
		return null;
	}
	
	private void saveSelectedModelElementAndPE(ISelection selection) {
		Object selected = getFirstSelectedElement(selection);
		
		if (selected instanceof EditPart) {
			selected = getModelElementFromEditPart((EditPart) selected);
		}
		
		if (selected instanceof PictogramElement) {
			this.pe = (PictogramElement) selected;
		} else {
			this.pe = null;
		}
		
		this.element = getModelElementFromPictogramElement(this.pe);
	}


	@Override
	public final void aboutToBeHidden() {
		removeAdapter();
	}

	/**
	 * Removes the adapter that calls {@link #refresh()} when {@link #shouldRefresh(Notification)} returns
	 * <code>true</code> on after a model change from the current {@link #getElement() element}.
	 */
	protected void removeAdapter() {
		// remove adapter
		EObject element = getElement();
		if (element != null && listener != null) {
			element.eAdapters().remove(listener);
		}
	}

	@Override
	public final void aboutToBeShown() {
		addAdapter();
	}

	/**
	 * Adds the adapter that calls {@link #refresh()} when {@link #shouldRefresh(Notification)} returns
	 * <code>true</code> on after a model change to the current {@link #getElement() element}.
	 */
	protected void addAdapter() {
		// add listener
		EObject element = getElement();
		if (element != null) {
			element.eAdapters().add(getListener());
		}
	}

	protected void add(EStructuralFeature feature, Collection<Object> values) {
		// FIXME check, why element and / or editing domain are sometimes null!
		if (getElement() != null && getEditingDomain() != null) {
			execute(AddCommand.create(getEditingDomain(), getElement(), feature, values));
		}
	}

	protected void add(EStructuralFeature feature, Object value) {
		if (getElement() != null && getEditingDomain() != null) {
			execute(AddCommand.create(getEditingDomain(), getElement(), feature, value));
		}
	}

	protected void remove(EStructuralFeature feature, Collection<Object> values) {
		if (getElement() != null && getEditingDomain() != null) {
			execute(RemoveCommand.create(getEditingDomain(), getElement(), feature, values));
		}
	}

	protected void remove(EStructuralFeature feature, Object value) {
		if (getElement() != null && getEditingDomain() != null) {
			execute(RemoveCommand.create(getEditingDomain(), getElement(), feature, value));
		}
	}

	protected void set(EStructuralFeature feature, Object value) {
		if (getElement() != null && getEditingDomain() != null) {
			execute(SetCommand.create(getEditingDomain(), getElement(), feature, value));
		}
	}

	protected Object getValue(EStructuralFeature feature) {
		if (getElement() != null && feature != null) {
			return getElement().eGet(feature);
		}
		return null;
	}

	/**
	 * Decides whether to refresh the controls after a model change.
	 * 
	 * @param msg The received notification.
	 * @return Returns <code>true</code> when {@link #refresh()} should be called.
	 */
	protected boolean shouldRefresh(Notification msg) {
		return msg.getEventType() != Notification.REMOVING_ADAPTER && getElement() != null;
	}

	private Adapter getListener() {
		if (listener == null) {
			listener = new AdapterImpl() {
				@Override
				public void notifyChanged(Notification msg) {
					if (shouldRefresh(msg)) {
						refresh();
					}
				}
			};
		}
		return listener;
	}

	@Override
	public void dispose() {
		// nothing here
	}

	@Override
	public int getMinimumHeight() {
		return SWT.DEFAULT;
	}

	@Override
	public void refresh() {
		// nothing here
	}

	@Override
	public boolean shouldUseExtraSpace() {
		return false;
	}

	/**
	 * Executes the command on the command stack of the editing domain.
	 * 
	 * @param command The command to execute.
	 */
	protected void execute(Command command) {
		if (editingDomain != null) {
			try {
				editingDomain.getCommandStack().execute(command);
				postExecute();
			} catch (IllegalStateException e) {
				Activator.get().error("Could not execute modification.", e);
			}
		} else {
			Activator.get().error("Could not find an editing domain.");
		}
	}

	/**
	 * Method that will be called after a command has successfully been executed.
	 * 
	 * @see #execute(Command)
	 */
	protected void postExecute() {
		// nothing additional by default
	}

	protected final TransactionalEditingDomain getEditingDomain() {
		return editingDomain;
	}

	protected final void decorate(IStatus state, Control control, Label info) {
		Image image = null;
		String text = null;
		Color color = null;
		if (state != null) {
			text = state.getMessage();
			switch (state.getSeverity()) {
			case IStatus.ERROR:
				color = CoreUiColors.get(CoreUiColors.ERROR);
				image = CoreUiImages.get(CoreUiImages.ERROR);
				break;
			case IStatus.WARNING:
				color = CoreUiColors.get(CoreUiColors.WARNING);
				image = CoreUiImages.get(CoreUiImages.WARNING);
				break;
			case IStatus.INFO:
				image = CoreUiImages.get(CoreUiImages.QUESTION);
			default:
				break;
			}
		}

		if (control != null && !control.isDisposed()) {
			control.setBackground(color);
		}

		if (info != null && !info.isDisposed()) {
			info.setImage(image);
			info.setToolTipText(text);
		}
	}

	protected final void decorate(State state, Label info) {
		Image image = null;
		String text = null;
		if (state != null) {
			text = state.getMessage();
			switch (state.getType()) {
			case ERROR:
				image = CoreUiImages.get(CoreUiImages.ERROR);
				break;
			case WARNING:
				image = CoreUiImages.get(CoreUiImages.WARNING);
				break;
			case INFO:
				image = CoreUiImages.get(CoreUiImages.QUESTION);
			default:
				break;
			}
		}

		if (info != null && !info.isDisposed()) {
			info.setImage(image);
			info.setToolTipText(text);
		}
	}

	protected final TabbedPropertySheetPage getPage() {
		return page;
	}

	/**
	 * Refreshes the label of the property sheet and the title of the diagram editor.
	 */
	protected final void refreshTitle() {
		if (page != null) {
			page.labelProviderChanged(null);
		}

		if (editor != null) {
			editor.refreshTitle();
			editor.refreshTitleToolTip();
		}
	}

	protected final IReason update(EObject bo) {
		IFeatureProvider fp = editor.getDiagramTypeProvider().getFeatureProvider();
		PictogramElement pe = fp.getPictogramElementForBusinessObject(bo);

		return fp.updateIfPossible(new UpdateContext(pe));
	}
}
