/**
 * 
 */
package de.upb.pose.core.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;

/**
 * @author Dietrich Travkin
 */
public interface WizardPageCompletionListener {

	void pageCompleted(WizardPage page);
	
	void pageIncomplete(WizardPage page);
	
}
