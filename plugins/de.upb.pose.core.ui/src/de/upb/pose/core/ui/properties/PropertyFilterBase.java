package de.upb.pose.core.ui.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IFilter;

import de.upb.pose.core.util.PropertiesUtil;

public abstract class PropertyFilterBase implements IFilter {
	@Override
	public final boolean select(Object element) {
		EObject adapted = PropertiesUtil.getAdapted(element);
		if (adapted instanceof EObject) {
			return show(adapted);
		}

		return false;
	}

	protected abstract boolean show(EObject element);
}
