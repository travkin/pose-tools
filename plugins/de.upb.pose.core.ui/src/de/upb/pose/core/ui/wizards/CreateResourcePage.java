package de.upb.pose.core.ui.wizards;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.views.navigator.ResourceComparator;

import de.upb.pose.core.ui.Activator;

public class CreateResourcePage extends WizardPageBase {
	
	private static final String DEFAULT_FILE_NAME = "default";
	
	private final String extension;
	private String fileNameWithoutExtenion = DEFAULT_FILE_NAME;
	private IContainer fileParent = null;

	private TreeViewer containerViewer;
	private Text nameText;

	public CreateResourcePage(String extension) {
		super(CreateResourcePage.class.getName() + extension);

		this.extension = extension;
	}

	@Override
	protected void createWidgets(Composite parent) {
		// container tree
		Tree containerTree = new Tree(parent, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(containerTree);

		containerViewer = new TreeViewer(containerTree);
		containerViewer.setContentProvider(new WorkbenchContentProvider());
		containerViewer.setLabelProvider(new WorkbenchLabelProvider());
		containerViewer.setComparator(new ResourceComparator(ResourceComparator.TYPE));
		containerViewer.addFilter(new FileViewerFiler(extension));

		Composite nameComposite = new Composite(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(nameComposite);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(nameComposite);

		Label nameLabel = new Label(nameComposite, SWT.NONE);
		GridDataFactory.fillDefaults().align(SWT.TRAIL, SWT.CENTER).applyTo(nameLabel);

		nameText = new Text(nameComposite, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(nameText);

		// initialize values
		containerViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
		nameLabel.setText("Name:");
		this.nameText.setText(getFileNameWithExtension());
	}
	
	protected String getFileNameWithExtension() {
		return this.fileNameWithoutExtenion + '.' + this.extension;
	}
	
	public void setFileNameWithoutExtension(String fileNameWithoutExtension) {
		if (fileNameWithoutExtension == null || fileNameWithoutExtension.isEmpty()) {
			this.fileNameWithoutExtenion = DEFAULT_FILE_NAME;
		} else {
			this.fileNameWithoutExtenion = fileNameWithoutExtension;
		}
		this.nameText.setText(getFileNameWithExtension());
	}
	
	protected IContainer getSelectedFileContainer() {
		return this.fileParent;
	}
	
	public void setFileContainer(IContainer fileParent) {
		if (fileParent != null && fileParent.isAccessible()) {
			this.fileParent = fileParent;
			this.containerViewer.setSelection(new StructuredSelection(fileParent));
		}
	}

	protected MultiStatus doValidate(MultiStatus status) {
		String id = Activator.get().getSymbolicName();

		// check container
		if (this.fileParent == null
				|| this.fileParent.equals(ResourcesPlugin.getWorkspace().getRoot())) {
			status.add(new Status(IStatus.ERROR, id, "You have to select a valid container for the resource!"));
		}

		// check name
		String currentFileName = this.nameText.getText();
		if (currentFileName == null || currentFileName.trim().isEmpty()) {
			status.add(new Status(IStatus.ERROR, id, "You have to select a name for the resource!"));
		} else if (!currentFileName.endsWith('.' + this.extension)) {
			status.add(new Status(IStatus.ERROR, id,
					"You have to select valid name for the resource! The file extension has to be ." + this.extension));
		}

		// show status
		if (status.isOK()) {
			setMessage(null);
			setErrorMessage(null);
			setPageComplete(true);
		} else {
			for (IStatus child : status.getChildren()) {
				switch (child.getSeverity()) {
				case IStatus.WARNING:
					setMessage(child.getMessage(), WARNING);
					break;
				case IStatus.ERROR:
					setErrorMessage(child.getMessage());
					setPageComplete(false);
					break;
				default:
					break;
				}
			}
		}

		return status;
	}

	@Override
	protected void hookListeners() {
		// validate when viewer selection changes
		containerViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object element = ((IStructuredSelection) containerViewer.getSelection()).getFirstElement();

				if (element instanceof IContainer) {
					fileParent = (IContainer) element;
					validate();
				} else if (element instanceof IFile) {
					IFile selectedFile = (IFile) element;
					if (extension.equals(selectedFile.getFileExtension())) {
						fileParent = selectedFile.getParent();
						setFileNameWithoutExtension(selectedFile.getFullPath().removeFileExtension().lastSegment());
					}
				}
			}
		});

		// validate when name text changes
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				validate();
			}
		});
	}
}
