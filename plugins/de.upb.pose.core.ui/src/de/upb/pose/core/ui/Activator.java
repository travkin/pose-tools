/**
 * <copyright>
 *  Copyright 2012 by Aljoschability and others. All rights reserved. This program and its materials are made available
 *  under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Aljoscha Hark <aljoschability@gmail.com> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core.ui;

import de.upb.pose.core.debug.DebugStream;
import de.upb.pose.core.ui.runtime.ActivatorImpl;
import de.upb.pose.core.ui.runtime.IActivator;

/**
 * This controls the life-cycle of the plug-in and delivers useful methods.
 * 
 * @author Aljoscha Hark <aljoschability@gmail.com>
 */
public class Activator extends ActivatorImpl {
	private static IActivator instance;

	public static IActivator get() {
		return Activator.instance;
	}

	@Override
	protected void dispose() {
		DebugStream.deactivate();
		Activator.instance = null;
	}

	@Override
	protected void initialize() {
		DebugStream.activate();

		Activator.instance = this;

		// images
		for (String key : CoreUiImages.getPaths()) {
			addImage(key);
		}

		// colors
		for (String key : CoreUiColors.getMap().keySet()) {
			addColor(key, CoreUiColors.getMap().get(key));
		}
	}
}
