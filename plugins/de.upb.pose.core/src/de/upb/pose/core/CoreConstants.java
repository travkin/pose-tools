package de.upb.pose.core;

public final class CoreConstants {

	/**
	 * An {@link org.eclipse.emf.ecore.EAnnotation} with this source key references
	 * the root element (container) of all diagrams associated with a model resource.
	 */
	public static final String ANNOTATION_SRC_DIAGRAMS_REF = "http://www.uni-paderborn.de/pose/diagrams";

	private CoreConstants() {
		// hide constructor
	}
}
