package de.upb.pose.core.helpers;

import org.eclipse.graphiti.mm.algorithms.styles.AdaptedGradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredArea;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredLocation;
import org.eclipse.graphiti.mm.algorithms.styles.LocationType;
import org.eclipse.graphiti.mm.algorithms.styles.StylesFactory;
import org.eclipse.graphiti.mm.algorithms.styles.StylesPackage;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.util.IGradientType;
import org.eclipse.graphiti.util.IPredefinedRenderingStyle;

public abstract class AbstractHelper {
	protected static final String SPLIT = "/";

	protected static final String DEFAULT_FONT_NAME = "Segoe UI"; //$NON-NLS-1$
	protected static final int DEFAULT_FONT_SIZE = 9;

	protected static final int VERTICAL = IGradientType.VERTICAL;
	protected static final int HORIZONTAL = IGradientType.HORIZONTAL;

	protected static final LocationType START = LocationType.LOCATION_TYPE_ABSOLUTE_START;
	protected static final LocationType END = LocationType.LOCATION_TYPE_ABSOLUTE_END;
	protected static final LocationType RELATIVE = LocationType.LOCATION_TYPE_RELATIVE;

	protected static final int DEFAULT = IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT;
	protected static final int PRIMARY = IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED;
	protected static final int SECONDARY = IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED;
	protected static final int ALLOWED = IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_ALLOWED;
	protected static final int FORBIDDEN = IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_FORBIDDEN;

	protected static final IGaService GA = Graphiti.getGaService();

	protected static GradientColoredArea createArea(GradientColoredLocation start, GradientColoredLocation end) {
		GradientColoredArea area = StylesFactory.eINSTANCE.createGradientColoredArea();

		area.setStart(start);
		area.setEnd(end);

		return area;
	}

	protected static AdaptedGradientColoredAreas createAreas(int type) {
		AdaptedGradientColoredAreas areas = StylesFactory.eINSTANCE.createAdaptedGradientColoredAreas();

		areas.setGradientType(type);

		return areas;
	}

	protected static Color createColor(int red, int green, int blue) {
		Color color = StylesFactory.eINSTANCE.createColor();

		color.eSet(StylesPackage.Literals.COLOR__RED, red);
		color.eSet(StylesPackage.Literals.COLOR__GREEN, green);
		color.eSet(StylesPackage.Literals.COLOR__BLUE, blue);

		return color;
	}

	protected static GradientColoredAreas createEmpty() {
		return StylesFactory.eINSTANCE.createGradientColoredAreas();
	}

	protected static GradientColoredLocation createLocation(Color color, LocationType type, int value) {
		GradientColoredLocation location = StylesFactory.eINSTANCE.createGradientColoredLocation();

		location.setColor(color);
		location.setLocationType(type);
		location.setLocationValue(value);

		return location;
	}

	protected static Font getFont(Diagram diagram, int size, boolean isBold, boolean isItalic) {
		return GA.manageFont(diagram, DEFAULT_FONT_NAME, size, isItalic, isBold);
	}
}
