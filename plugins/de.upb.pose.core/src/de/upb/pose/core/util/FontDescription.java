package de.upb.pose.core.util;

import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;

public class FontDescription {
	private final String name;
	private final int size;
	private final boolean isBold;
	private final boolean isItalic;

	public FontDescription(String name, int size) {
		this(name, size, false, false);
	}

	public FontDescription(String name, int size, boolean isBold, boolean isItalic) {
		this.name = name;
		this.size = size;
		this.isBold = isBold;
		this.isItalic = isItalic;
	}

	public Font manage(Diagram diagram) {
		return Graphiti.getGaService().manageFont(diagram, name, size, isItalic, isBold);
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}

	public boolean isBold() {
		return isBold;
	}

	public boolean isItalic() {
		return isItalic;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(getClass().getSimpleName());
		builder.append(' ');
		builder.append('(');
		builder.append(name);
		builder.append(',');
		builder.append(' ');
		builder.append(size);
		if (isBold) {
			builder.append(',');
			builder.append(' ');
			builder.append("BOLD");
		}
		if (isItalic) {
			builder.append(',');
			builder.append(' ');
			builder.append("ITALIC");
		}
		builder.append(')');

		return builder.toString();
	}
}
