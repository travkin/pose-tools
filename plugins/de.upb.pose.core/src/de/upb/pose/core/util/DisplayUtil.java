package de.upb.pose.core.util;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

public class DisplayUtil {
	public static void async(Runnable runnable) {
		get().asyncExec(runnable);
	}

	public static void sync(Runnable runnable) {
		get().syncExec(runnable);
	}

	public static Shell getShell() {
		return get().getActiveShell();
	}

	private static Display get() {
		return PlatformUI.getWorkbench().getDisplay();
	}
}
