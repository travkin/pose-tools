/**
 * 
 */
package de.upb.pose.core.util;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

/**
 * @author Dietrich Travkin
 */
public class GraphitiUtil
{
	/**
	 * Returns all pictogram elements linked to the given model element.
	 * 
	 * @param diagram the parent diagram
	 * @param element the model element
	 * @return all pictogram elements linked to the given model element
	 */
	public static List<PictogramElement> getPictogramElements(Diagram diagram, EObject element)
	{
		return Graphiti.getLinkService().getPictogramElements(diagram, element);
	}
	
	/**
	 * Returns the first of all pictogram elements linked to the given model element.
	 * 
	 * @param diagram the parent diagram
	 * @param element the model element
	 * @return the first of all pictogram elements linked to the given model element
	 */
	public static PictogramElement getFirstPictogramElement(Diagram diagram, EObject element)
	{
		return getPictogramElements(diagram, element).get(0);
	}
	
	/**
	 * Returns the first business object for the given pictorgram element of the given type.
	 * 
	 * @param pe the pictogram element
	 * @param businessObjectClass the type of the desired business object
	 * @return the first business object for the given pictorgram element of the given type
	 */
	public static <T> T getFirstBusinessObjectOfType(PictogramElement pe,  Class<T> businessObjectClass)
	{
		return getFirstObjectOfType(pe.getLink().getBusinessObjects(), businessObjectClass);
	}
	
	/**
	 * Returns the first object in the given list that has the given type or a subtype.
	 *  
	 * @param listOfObjects the object list
	 * @param type the required type
	 * @return the first object in the given list that has the required type
	 */
	@SuppressWarnings("unchecked")
	public static <S,T> T getFirstObjectOfType(List<S> listOfObjects, Class<T> type)
	{
		for (S object: listOfObjects)
		{
			if (type.isAssignableFrom(object.getClass()))
			{
				return (T) object;
			}
		}
		return null;
	}
}
