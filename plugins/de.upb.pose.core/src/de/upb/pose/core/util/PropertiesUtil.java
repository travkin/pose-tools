package de.upb.pose.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.platform.IDiagramEditor;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.IContributedContentsView;
import org.eclipse.ui.part.MultiPageEditorPart;

public final class PropertiesUtil {
	
	private PropertiesUtil() {
		// hide constructor
	}

	/**
	 * Returns all elements that can be adapted to {@link org.eclipse.emf.ecore.EObject} of a
	 * {@link org.eclipse.jface.viewers.IStructuredSelection structured selection}.
	 * 
	 * @param object The {@link IStructuredSelection structured selection}.
	 * @return Returns a {@link java.util.List list} of all adapted elements of the selection or an empty list.
	 * @see #getAdapted(Object)
	 */
	public static Collection<EObject> getAllAdapted(Object object) {
		if (object instanceof IStructuredSelection) {
			Collection<EObject> list = new ArrayList<EObject>();
			for (Object selected : ((IStructuredSelection) object).toArray()) {
				EObject adapted = getAdapted(selected);
				if (adapted != null) {
					list.add(adapted);
				}
			}
			return list;
		}
		return Collections.emptyList();
	}

	/**
	 * Returns the given object adapted to {@link org.eclipse.emf.ecore.EObject} for the first element of a
	 * {@link org.eclipse.jface.viewers.IStructuredSelection structured selection}, an {@link org.eclipse.gef.EditPart
	 * edit part} or a {@link org.eclipse.graphiti.mm.pictograms.PictogramElement pictogram element}.
	 * 
	 * @param object The object to adapt.
	 * @return Returns the adapted {@link org.eclipse.emf.ecore.EObject} or <code>null</code>.
	 */
	public static EObject getAdapted(Object object) {
		
		if (object instanceof IStructuredSelection) {
			return getAdapted(((IStructuredSelection) object).getFirstElement());
		}

		if (object instanceof EditPart) {
			return getAdapted(((EditPart) object).getModel());
		}

		if (object instanceof PictogramElement) {
			return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement((PictogramElement) object);
		}

		if (object instanceof EObject) {
			return (EObject) object;
		}

		return null;
	}

	public static PictogramElement getPictogramElement(Object object) {
		
		if (object instanceof IStructuredSelection) {
			return getPictogramElement(((IStructuredSelection) object).getFirstElement());
		}

		if (object instanceof EditPart) {
			return getPictogramElement(((EditPart) object).getModel());
		}

		if (object instanceof PictogramElement) {
			return (PictogramElement) object;
		}

		return null;
	}

	/**
	 * Tries to get the {@link org.eclipse.graphiti.platform.IDiagramEditor diagram editor} for a {@link IWorkbenchPart
	 * workbench part}.
	 * 
	 * @param part The workbench part.
	 * @return Returns the {@link org.eclipse.graphiti.platform.IDiagramEditor diagram editor} for the workbench part or
	 *         <code>null</code>.
	 */
	public static IDiagramEditor getEditor(IWorkbenchPart part) {
		if (part instanceof IDiagramEditor) {
			return (IDiagramEditor) part;
		}

		// contributed contents view
		IContributedContentsView view = (IContributedContentsView) part.getAdapter(IContributedContentsView.class);
		if (view != null) {
			part = view.getContributingPart();

			if (part instanceof IDiagramEditor) {
				return (DiagramEditor) part;
			}
		}

		// catalog editor
		if (part instanceof MultiPageEditorPart) {
			Object page = ((MultiPageEditorPart) part).getSelectedPage();
			if (page instanceof IWorkbenchPart) {
				return getEditor((IWorkbenchPart) page);
			}
		}
		return null;
	}
}
