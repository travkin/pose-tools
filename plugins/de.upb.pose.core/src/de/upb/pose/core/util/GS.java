package de.upb.pose.core.util;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeService;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.internal.core.util.CalculatorRunnable;

public final class GS {
	public static final IPeService PE = Graphiti.getPeService();
	public static final IGaService GA = Graphiti.getGaService();

	private GS() {
		// hide constructor
	}

	public static PictogramElement getPE(Diagram diagram, EObject bo) {
		for (PictogramElement pe : getAllPEs(diagram, bo)) {
			return pe;
		}

		return null;
	}

	public static boolean differ(IColorConstant boColor, Color peColor) {
		if (peColor != null) {
			return !(boColor.getRed() == peColor.getRed() && boColor.getGreen() == peColor.getGreen() && boColor
					.getBlue() == peColor.getBlue());
		}
		return true;
	}

	public static boolean differ(String bo, String pe) {
		if (bo != null) {
			return !bo.equals(pe);
		}

		if (pe != null) {
			return !pe.equals(bo);
		}

		if (pe == null && bo == null) {
			return false;
		}

		return true;
	}

	public static boolean unequals(String bo, Text pe) {
		if (pe != null) {
			return differ(bo, pe.getValue());
		}
		else if (bo == null) {
			return false;
		}
		else
		{
			return !bo.equals(pe);		
		}
	}

	public static boolean differ(String bo, MultiText pe) {
		return differ(bo, pe.getValue());
	}

	public static boolean differ(FontDescription boFont, Font peFont) {
		if (boFont == null && peFont == null) {
			return false;
		}
		if (boFont != null && peFont == null) {
			return true;
		}
		if (boFont == null && peFont != null) {
			return true;
		}

		boolean equalName = boFont.getName().equalsIgnoreCase(peFont.getName());
		boolean equalSize = boFont.getSize() == peFont.getSize();
		boolean equalBold = boFont.isBold() == peFont.isBold();
		boolean equalItalic = boFont.isItalic() == peFont.isItalic();

		return !(equalName && equalSize && equalBold && equalItalic);
	}

	public static List<PictogramElement> getAllPEs(Diagram diagram, EObject bo) {
		return Graphiti.getLinkService().getPictogramElements(diagram, bo);
	}

	public static int getHeight(Font font, String text) {
		return getSize(font, text).getHeight();
	}

	public static int getHeight(Text text) {
		return getSize(text).getHeight();
	}

	public static int getWidth(Text text) {
		return getSize(text).getWidth();
	}

	public static Size getSize(Text text) {
		Font font = text.getFont();
		if (font == null && text.getStyle() != null) {
			font = text.getStyle().getFont();
		}

		return getSize(font, text.getValue());
	}

	public static int getWidth(Font font, String text) {
		return getSize(font, text).getWidth();
	}

	public static Size getSize(Font font, String value) {
		return getSize(font, value, 0, 0);
	}

	public static Size getSize(Font font, String value, int paddingWidth, int paddingHeight) {
		if (font == null || value == null || value.isEmpty()) {
			return new Size(0, 0);
		}

		CalculatorRunnable runnable = new CalculatorRunnable(value, font);
		DisplayUtil.sync(runnable);

		return new Size(runnable.getWidth() + paddingWidth * 2, runnable.getHeight() + paddingHeight * 2);
	}

	public static EObject getBO(PictogramElement pe) {
		return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
	}
}
