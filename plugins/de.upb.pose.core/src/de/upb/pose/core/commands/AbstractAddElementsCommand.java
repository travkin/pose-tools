package de.upb.pose.core.commands;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.services.GraphitiUi;

import de.upb.pose.core.features.LayoutDiagramFeature;

public abstract class AbstractAddElementsCommand extends RecordingCommand {
	
	private EObject bo;
	private Resource peResource;
	private URI uri;

	public AbstractAddElementsCommand(EObject bo, URI uri) {
		super(TransactionUtil.getEditingDomain(bo));
		this.bo = bo;
		this.uri = uri;
	}

	public Resource getDiagramResource() {
		return peResource;
	}

	@Override
	protected void doExecute() {
		// create the diagram and its file
		Diagram pe = Graphiti.getPeCreateService().createDiagram(getDiagramId(), getDiagramName(), true);

		// create link
		PictogramLink link = PictogramsFactory.eINSTANCE.createPictogramLink();
		link.getBusinessObjects().add(bo);
		link.setPictogramElement(pe);

		peResource = getEditingDomain().getResourceSet().createResource(uri);
		peResource.getContents().add(pe);

		// start dark feature processing
		IDiagramTypeProvider dtp = GraphitiUi.getExtensionManager().createDiagramTypeProvider(pe,
				getDiagramTypeProviderId());
		IFeatureProvider fp = dtp.getFeatureProvider();

		execute(fp, pe);

		// layout diagram
		LayoutDiagramFeature layoutFeature = new LayoutDiagramFeature(fp);
		layoutFeature.execute(null);
	}

	protected abstract String getDiagramId();

	protected abstract String getDiagramName();

	protected TransactionalEditingDomain getEditingDomain() {
		return TransactionUtil.getEditingDomain(bo);
	}

	protected abstract String getDiagramTypeProviderId();

	protected abstract void execute(IFeatureProvider fp, Diagram pe);

	protected EObject getElement() {
		return bo;
	}
}
