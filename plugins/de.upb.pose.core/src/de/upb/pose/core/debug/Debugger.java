package de.upb.pose.core.debug;

import java.text.MessageFormat;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;

public final class Debugger {
	public static void debug(Notification msg) {
		// event
		String event;
		switch (msg.getEventType()) {
		case Notification.ADD:
			event = "ADD";
			break;
		case Notification.ADD_MANY:
			event = "ADD_MANY";
			break;
		case Notification.MOVE:
			event = "MOVE";
			break;
		case Notification.NO_FEATURE_ID:
			event = "NO_FEATURE_ID";
			break;
		case Notification.REMOVE:
			event = "REMOVE";
			break;
		case Notification.REMOVE_MANY:
			event = "REMOVE_MANY";
			break;
		case Notification.REMOVING_ADAPTER:
			event = "REMOVING_ADAPTER";
			break;
		case Notification.RESOLVE:
			event = "RESOLVE";
			break;
		case Notification.SET:
			event = "SET";
			break;
		case Notification.UNSET:
			event = "UNSET";
			break;
		default:
			event = "? (" + msg.getEventType() + ")";
			break;
		}

		Resource resource = null;
		TransactionalEditingDomain editingDomain = null;
		Object notifier = msg.getNotifier();
		if (notifier instanceof EObject) {
			resource = ((EObject) notifier).eResource();
			if (resource != null) {
				editingDomain = TransactionUtil.getEditingDomain(resource);
			}
		} else if (notifier instanceof Resource) {
			resource = (Resource) notifier;
			editingDomain = TransactionUtil.getEditingDomain(resource);
		}
		else if (notifier instanceof ResourceSet) {
			editingDomain = TransactionUtil.getEditingDomain((ResourceSet) notifier);
		}

		System.out.println(MessageFormat.format("  DOMAIN: {0}", editingDomain));
		System.out.println(MessageFormat.format("RESOURCE: {0}", resource));

		System.out.println(MessageFormat.format("NOTIFIER: {0}", msg.getNotifier()));
		System.out.println(MessageFormat.format(" FEATURE: {0}", msg.getFeature()));
		System.out.println(MessageFormat.format("   EVENT: {0}", event));
		System.out.println(MessageFormat.format("     OLD: {0}", msg.getOldValue()));
		System.out.println(MessageFormat.format("     NEW: {0}", msg.getNewValue()));

		System.out.println();
	}
}
