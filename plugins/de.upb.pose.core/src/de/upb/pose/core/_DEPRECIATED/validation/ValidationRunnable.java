/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Display;

/**
 * This {@link IRunnableWithProgress} performs a validation with the given {@link IValidator}. The result of the
 * validation described by a {@link Diagnostic} is handled by the registered {@link IDiagnosticHandler}s.
 */
public class ValidationRunnable implements IRunnableWithProgress {
	public static final String DIAGNOSTIC_SOURCE = "de.upb.pose.commons.emf.validation.ValidationRunnable";

	private final IValidationObjectProvider validationObjectProvider;

	private final List<IValidator> validators;

	private final List<IDiagnosticHandler> diagnosticHandlers;

	public ValidationRunnable(IValidationObjectProvider validationObjectProvider) {
		this.validationObjectProvider = validationObjectProvider;

		validators = new LinkedList<IValidator>();
		diagnosticHandlers = new LinkedList<IDiagnosticHandler>();
	}

	@Override
	public void run(final IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		try {
			/** get validationObject **/
			List<? extends EObject> validationObjects = validationObjectProvider.getValidationObjects();

			/** validate **/
			final BasicDiagnostic diagnostic = createDefaultDiagnostic();
			for (IValidator validator : validators) {
				for (EObject validationObject : validationObjects) {
					Diagnostic subDiagnostic = validator.validate(validationObject, monitor);
					diagnostic.addAll(subDiagnostic);
				}
			}

			/** handle diagnostic **/
			// Runs async in case of UI modifications
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					if (monitor.isCanceled()) {
						for (IDiagnosticHandler diagnosticHandler : diagnosticHandlers) {
							diagnosticHandler.handle(Diagnostic.CANCEL_INSTANCE);
						}
					} else {
						for (IDiagnosticHandler diagnosticHandler : diagnosticHandlers) {
							diagnosticHandler.handle(diagnostic);
						}
					}
				}
			});
		} finally {
			monitor.done();
		}
	}

	private BasicDiagnostic createDefaultDiagnostic() {
		return new BasicDiagnostic(DIAGNOSTIC_SOURCE, 0, "Diagnosis", new Object[] {});
	}

	/**
	 * Adds an {@link IDiagnosticHandler} which is used to handle the result of the validation described by a
	 * {@link Diagnostic}.
	 * 
	 * @param handler
	 */
	public void addDiagnosticHandler(IDiagnosticHandler handler) {
		diagnosticHandlers.add(handler);
	}

	/**
	 * Adds an {@link IValidator} which is used to perform the validation.
	 * 
	 * @param handler
	 */
	public void addValidator(IValidator validator) {
		validators.add(validator);
	}
}
