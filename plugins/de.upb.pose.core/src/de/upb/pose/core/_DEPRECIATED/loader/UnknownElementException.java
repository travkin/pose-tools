/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import org.eclipse.emf.ecore.EObject;

public class UnknownElementException extends Exception {
	private static final long serialVersionUID = 4049132768829748669L;

	private final Class<? extends EObject> clazz;

	public UnknownElementException(Class<? extends EObject> clazz) {
		super();
		this.clazz = clazz;
	}

	public Class<? extends EObject> getCausingClass() {
		return clazz;
	}
}
