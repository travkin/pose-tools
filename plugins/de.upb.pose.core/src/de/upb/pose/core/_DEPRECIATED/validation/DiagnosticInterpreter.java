/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import org.eclipse.emf.common.util.Diagnostic;

public class DiagnosticInterpreter {
	private DiagnosticInterpreter() {
		// utility class
	}

	/**
	 * Interprets the given {@link Diagnostic} and returns a short message text.
	 * 
	 * @param diagnostic
	 * @return
	 */
	public static String getMessageText(Diagnostic diagnostic) {
		int severity = diagnostic.getSeverity();

		if (severity == Diagnostic.ERROR || severity == Diagnostic.WARNING) {
			return "Problems encountered during validation";
		} else if (severity == Diagnostic.OK) {
			return "Validation completed successfully";
		} else {
			return "Information encountered during validation";
		}
	}

	/**
	 * Interprets the given {@link Diagnostic} and returns a short title text.
	 * 
	 * @param diagnostic
	 * @return
	 */
	public static String getTitleText(Diagnostic diagnostic) {
		int severity = diagnostic.getSeverity();

		if (severity == Diagnostic.ERROR || severity == Diagnostic.WARNING) {
			return "Validation Problems";
		} else {
			return "Validation Information";
		}
	}

	/**
	 * Interprets the given {@link Diagnostic} and returns true if everything is OK.
	 * 
	 * @param diagnostic
	 * @return
	 */
	public static boolean isOK(Diagnostic diagnostic) {
		return diagnostic.getSeverity() == Diagnostic.OK;
	}
}
