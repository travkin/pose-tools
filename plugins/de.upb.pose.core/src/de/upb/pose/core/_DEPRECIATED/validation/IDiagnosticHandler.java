/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import org.eclipse.emf.common.util.Diagnostic;

/**
 * Implementing classes are able to handle {@link Diagnostic}s.
 */
public interface IDiagnosticHandler {
	/**
	 * Handles the given {@link Diagnostic}
	 * 
	 * @param diagnostic
	 */
	void handle(Diagnostic diagnostic);
}
