/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class ModelToXmlResourceStrategy extends AbstractModelToResourceStrategy {
	private final String fileExtension;

	private final IInitablePackage initablePackage;

	public ModelToXmlResourceStrategy(String fileExtension, IInitablePackage initablePackage) {
		super();
		this.fileExtension = fileExtension;
		this.initablePackage = initablePackage;
	}

	@Override
	public EList<EObject> modelFromResource(IResource resource) throws LoadException {
		if (!resource.exists() || !resource.isAccessible()) {
			// throw an exception if the file does not exist or isn't accessible
			throw new LoadException(resource, "Resource not exists or is not accessible.", null);
		} else {
			try {
				// Try to load and return the diagram
				initablePackage.init();

				ResourceSet rSet = new ResourceSetImpl();

				rSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
						.put(fileExtension, new XMIResourceFactoryImpl());

				Resource modelResource = rSet.getResource(
						URI.createPlatformResourceURI(resource.getFullPath().toString(), true), true);

				return modelResource.getContents();

			} catch (Exception e) {
				throw new LoadException(resource, "Resource is not a valid Model resource.", e);
			}
		}
	}

	@Override
	public void modelToResource(Collection<? extends EObject> objects, IResource resource) throws LoadException {
		ResourceSet rSet = new ResourceSetImpl();
		rSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(fileExtension, new XMIResourceFactoryImpl());

		if (resource.exists() && resource.getResourceAttributes().isReadOnly()) {
			// throw an exception if the file exists, but is not writeable
			throw new LoadException(resource, "Resource is read-only.", null);

		} else {
			// Create resource. If it exists it is opened otherwise it is
			// created
			Resource modelResource = rSet.createResource(URI.createPlatformResourceURI(resource.getFullPath()
					.toString(), true));
			modelResource.getContents().addAll(objects);

			// try to save the diagram into the file
			try {
				modelResource.save(Collections.EMPTY_MAP);
			} catch (IOException e) {
				throw new LoadException(resource, "Model can not be saved to Resource.", e);

			}
		}
	}
}
