/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.edit.provider.IItemLabelProvider;

/**
 * A basic implementation of the {@link IValidator} interface. Validates an {@link EObject} provided by the given
 * {@link IValidationObjectProvider}.
 */
public class DefaultValidator implements IValidator {
	private final AdapterFactory adapterFactory;

	public DefaultValidator(AdapterFactory adapterFactory) {
		super();
		this.adapterFactory = adapterFactory;
	}

	@Override
	public Diagnostic validate(EObject validationObject, IProgressMonitor progressMonitor) {
		progressMonitor.beginTask("Validating", 1);

		// create diagnostics
		Diagnostician diagnostician = createDiagnostician(adapterFactory, progressMonitor);
		BasicDiagnostic diagnostic = diagnostician.createDefaultDiagnostic(validationObject);

		// set TaskName
		progressMonitor.setTaskName("Validating " + diagnostician.getObjectLabel(validationObject));

		// validate
		Map<Object, Object> context = diagnostician.createDefaultContext();
		diagnostician.validate(validationObject, diagnostic, context);

		return diagnostic;
	}

	private Diagnostician createDiagnostician(final AdapterFactory adapterFactory,
			final IProgressMonitor progressMonitor) {
		return new Diagnostician() {
			@Override
			public String getObjectLabel(EObject eObject) {
				// try to get the name from adapterFactory
				if (adapterFactory != null && !eObject.eIsProxy()) {
					IItemLabelProvider itemLabelProvider = (IItemLabelProvider) adapterFactory.adapt(eObject,
							IItemLabelProvider.class);
					if (itemLabelProvider != null) {
						return itemLabelProvider.getText(eObject);
					}
				}

				return super.getObjectLabel(eObject);
			}

			@Override
			public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics,
					Map<Object, Object> context) {
				// worked one step
				progressMonitor.worked(1);

				return super.validate(eClass, eObject, diagnostics, context);
			}
		};
	}
}
