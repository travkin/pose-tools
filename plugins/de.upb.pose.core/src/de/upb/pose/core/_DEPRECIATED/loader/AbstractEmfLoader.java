/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.EObject;

public abstract class AbstractEmfLoader {
	private AbstractModelToResourceStrategy strategy;

	public AbstractEmfLoader(AbstractModelToResourceStrategy strategy) {
		super();
		this.strategy = strategy;
	}

	public Collection<EObject> loadAll(IResource resource) throws LoadException {
		return strategy.modelFromResource(resource);
	}

	public void saveAll(Collection<EObject> objects, IResource resource) throws LoadException {
		strategy.modelToResource(objects, resource);
	}
}
