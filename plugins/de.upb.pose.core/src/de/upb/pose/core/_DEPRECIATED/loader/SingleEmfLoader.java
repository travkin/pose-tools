/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

public class SingleEmfLoader<T extends EObject> implements ISaveProvider {
	private final CombinedEmfLoader loader;

	private T element;

	private Collection<T> elements;

	public SingleEmfLoader(AbstractModelToResourceStrategy strategy) {
		loader = new CombinedEmfLoader(strategy);
		loader.addSaveProvider(this);
	}

	@Override
	public EObject getElement() {
		return element;
	}

	@Override
	public Collection<? extends EObject> getElements() {
		return elements;
	}

	public T load(IResource resource, Class<T> clazz) throws LoadException, UnknownElementException {
		Collection<EObject> objects = loader.loadAll(resource);

		return ListUtil.getObjectFromList(clazz, objects);
	}

	public Collection<EObject> loadAll(IResource resource) throws LoadException {
		return loader.loadAll(resource);
	}

	public void save(T object, IResource resource) throws LoadException {
		this.element = object;

		loader.save(resource);

		// Garbage collection
		this.element = null;
	}

	public void saveAll(EList<T> objects, IResource resource) throws LoadException {
		this.elements = objects;

		loader.save(resource);

		// Garbage collection
		this.elements = null;
	}
}
