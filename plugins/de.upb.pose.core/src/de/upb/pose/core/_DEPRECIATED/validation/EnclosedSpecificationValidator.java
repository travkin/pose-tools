/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.IItemLabelProvider;

public class EnclosedSpecificationValidator implements IValidator {
	public static final String DIAGNOSTIC_SOURCE = "de.upb.pose.pamela.ui.editor.validation.EnclosedSpecificationValidator";

	private final AdapterFactory adapterFactory;

	public EnclosedSpecificationValidator(AdapterFactory adapterFactory) {
		super();
		this.adapterFactory = adapterFactory;
	}

	@Override
	public Diagnostic validate(EObject validationObject, IProgressMonitor progressMonitor) {
		progressMonitor.beginTask("Validating", 1);

		// set TaskName
		progressMonitor.setTaskName("Validating " + getObjectLabel(validationObject));

		BasicDiagnostic diagnostic = createDefaultDiagnostic(validationObject);
		Map<EObject, Collection<Setting>> map = EcoreUtil.ExternalCrossReferencer.find(validationObject);

		for (Entry<EObject, Collection<Setting>> entry : map.entrySet()) {
			for (Setting setting : entry.getValue()) {
				Diagnostic newDiagnostic = createDiagnostic(setting.getEObject(), setting.getEStructuralFeature(),
						entry.getKey());
				diagnostic.add(newDiagnostic);
			}
		}

		return diagnostic;
	}

	private BasicDiagnostic createDefaultDiagnostic(EObject eObject) {
		return new BasicDiagnostic(DIAGNOSTIC_SOURCE, 0, "Diagnosis of " + getObjectLabel(eObject),
				new Object[] { eObject });
	}

	private Diagnostic createDiagnostic(EObject sourceObject, EStructuralFeature structuralFeature, EObject targetObject) {
		// an indicator of the severity of the problem.
		int severity = Diagnostic.ERROR;
		// the unique identifier of the source.
		String source = DIAGNOSTIC_SOURCE;
		// the source-specific identity code.
		int code = 0;
		// a string describing the problem
		String message = "The feature '" + structuralFeature.getName() + "' of '" + getObjectLabel(sourceObject)
				+ "' refers to the object '" + getObjectLabel(targetObject)
				+ "' that is not child of the specification";
		// the data associated with the diagnostic
		Object[] data = new Object[] { sourceObject };

		return new BasicDiagnostic(severity, source, code, message, data);
	}

	public String getObjectLabel(EObject eObject) {
		// try to get the name from adapterFactory
		if (adapterFactory != null && !eObject.eIsProxy()) {
			IItemLabelProvider itemLabelProvider = (IItemLabelProvider) adapterFactory.adapt(eObject,
					IItemLabelProvider.class);
			if (itemLabelProvider != null) {
				return itemLabelProvider.getText(eObject);
			}
		}

		return eObject.toString();
	}
}
