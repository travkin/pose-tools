/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.EObject;

public class CombinedEmfLoader extends AbstractEmfLoader {
	private final List<ISaveProvider> saveProviders;

	private final EventListenerList loadListeners;

	public CombinedEmfLoader(AbstractModelToResourceStrategy strategy) {
		super(strategy);
		saveProviders = new LinkedList<ISaveProvider>();
		loadListeners = new EventListenerList();
	}

	@Override
	public Collection<EObject> loadAll(IResource resource) throws LoadException {

		Collection<EObject> objects = super.loadAll(resource);

		notifyLoadListeners(new LoadEvent(objects));

		return objects;
	}

	private synchronized void notifyLoadListeners(LoadEvent event) {
		for (ILoadListener l : loadListeners.getListeners(ILoadListener.class)) {
			l.contentLoaded(event);
		}
	}

	public void save(IResource resource) throws LoadException {
		save(resource, new LinkedList<EObject>());
	}

	public void save(IResource resource, EObject object) throws LoadException {
		Collection<EObject> objects = new LinkedList<EObject>();
		objects.add(object);

		save(resource, objects);
	}

	private void save(IResource resource, Collection<EObject> objects) throws LoadException {
		Iterator<ISaveProvider> it = saveProviders.iterator();
		ISaveProvider saveProvider;
		EObject element;
		Collection<? extends EObject> elements;
		while (it.hasNext()) {
			saveProvider = it.next();

			element = saveProvider.getElement();
			if (element != null) {
				objects.add(element);
			}

			elements = saveProvider.getElements();
			if (elements != null) {
				objects.addAll(elements);
			}
		}

		saveAll(objects, resource);
	}

	public void addLoadListener(ILoadListener listener) {
		loadListeners.add(ILoadListener.class, listener);
	}

	public void addSaveProvider(ISaveProvider provider) {
		saveProviders.add(provider);
	}

	public void removeLoadListener(ILoadListener listener) {
		loadListeners.remove(ILoadListener.class, listener);
	}

	public void removeSaveProvider(ISaveProvider provider) {
		saveProviders.remove(provider);
	}
}
