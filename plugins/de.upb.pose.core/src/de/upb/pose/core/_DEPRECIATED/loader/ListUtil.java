/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;

public class ListUtil {
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getObjectFromList(Class<T> clazz, Collection<EObject> elements)
			throws UnknownElementException {
		for (EObject element : elements) {
			if (clazz.isAssignableFrom(element.getClass())) {
				return (T) element;
			}
		}

		throw new UnknownElementException(clazz);
	}

	@SuppressWarnings("unchecked")
	public static <T extends EObject> Collection<T> getObjectsFromList(Class<T> clazz, Collection<EObject> elements) {
		Collection<T> result = new ArrayList<T>();
		for (EObject element : elements) {
			if (clazz.isAssignableFrom(element.getClass())) {
				result.add((T) element);
			}
		}

		return result;
	}
}
