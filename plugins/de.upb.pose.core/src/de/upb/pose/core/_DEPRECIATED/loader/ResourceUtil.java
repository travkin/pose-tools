/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter;

public class ResourceUtil {
	public static IResource createResource(String pathString) {
		// Search file in workspace
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IPath path = new Path(pathString);
		IFile specificationFile = root.getFile(path);

		return specificationFile;
	}

	public static IResource getIResourceFromEResource(Resource resource) {
		if (resource == null) {
			return null;
		}
		URI uri = resource.getURI();
		if (uri != null && resource.getResourceSet() != null) {
			URIConverter converter = resource.getResourceSet().getURIConverter();
			if (converter != null) {
				uri = converter.normalize(uri);
			}
		}
		if (uri != null && uri.isPlatformResource()) {
			return ResourcesPlugin.getWorkspace().getRoot().findMember(uri.toPlatformString(true));
		}
		return null;
	}
}
