/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;

public class LoadEvent {
	private final Collection<EObject> objects;

	public LoadEvent(Collection<EObject> objects) {
		super();
		this.objects = objects;
	}

	public <T extends EObject> T getObject(Class<T> clazz) throws UnknownElementException {
		return ListUtil.getObjectFromList(clazz, objects);
	}

	public <T extends EObject> Collection<T> getObjects(Class<T> clazz) {
		return ListUtil.getObjectsFromList(clazz, objects);
	}
}
