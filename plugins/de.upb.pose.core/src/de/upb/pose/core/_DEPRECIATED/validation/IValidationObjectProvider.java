/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * Implementing classes are able to return an object used for a validation.
 */
public interface IValidationObjectProvider {
	/**
	 * Returns the objects used for a validation.
	 * 
	 * @return
	 */
	List<? extends EObject> getValidationObjects();
}
