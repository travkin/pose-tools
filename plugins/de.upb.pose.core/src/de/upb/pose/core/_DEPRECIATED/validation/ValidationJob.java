/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.Diagnostic;

/**
 * This {@link Job} performs a validation with the given {@link IValidator}. The result of the validation described by a
 * {@link Diagnostic} is handled by the registered {@link IDiagnosticHandler}s.
 */
public class ValidationJob extends Job {
	/** Delegates everything to this {@link ValidationRunnable} **/
	private final ValidationRunnable validationRunnable;

	public ValidationJob(IValidationObjectProvider validationObjectProvider) {
		super("Validation");

		validationRunnable = new ValidationRunnable(validationObjectProvider);
	}

	/**
	 * Adds an {@link IDiagnosticHandler} which is used to handle the result of the validation described by a
	 * {@link Diagnostic}.
	 * 
	 * @param handler
	 */
	public void addDiagnosticHandler(IDiagnosticHandler handler) {
		validationRunnable.addDiagnosticHandler(handler);
	}

	/**
	 * Adds an {@link IValidator} which is used to perform the validation.
	 * 
	 * @param handler
	 */
	public void addValidator(IValidator validator) {
		validationRunnable.addValidator(validator);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Validation", IProgressMonitor.UNKNOWN);

			validationRunnable.run(monitor);

			// Must return OK to avoid any dialog
			return Status.OK_STATUS;
		} catch (Exception e) {
			e.printStackTrace();

			return new Status(IStatus.ERROR, "de.upb.pose.pamela.ui.editor.PamelaEditor", "Error during validation", e);
		}
	}
}
