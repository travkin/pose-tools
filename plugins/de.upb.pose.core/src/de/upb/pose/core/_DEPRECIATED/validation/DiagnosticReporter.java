/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

/**
 * An implementation of the {@link IDiagnosticHandler} interface. Reports the given {@link Diagnostic} to the user using
 * a dialog.
 */
public class DiagnosticReporter implements IDiagnosticHandler {
	private final boolean showDetailedDialog;

	/**
	 * 
	 * @param showDetailedDialog if true, a detailed diagnostic dialog will be used in case of any problems
	 */
	public DiagnosticReporter(boolean showDetailedDialog) {
		super();
		this.showDetailedDialog = showDetailedDialog;
	}

	@Override
	public void handle(Diagnostic diagnostic) {
		String title = DiagnosticInterpreter.getTitleText(diagnostic);
		String message = DiagnosticInterpreter.getMessageText(diagnostic);

		// open dialog
		if (diagnostic.getSeverity() == Diagnostic.OK) {
			MessageDialog.openInformation(Display.getDefault().getActiveShell(), title, message);
		} else if (showDetailedDialog) {
			DiagnosticDialog.open(Display.getDefault().getActiveShell(), title, message, diagnostic);
		} else {
			MessageDialog.openError(Display.getDefault().getActiveShell(), title, message);
		}
	}
}
