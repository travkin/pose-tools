/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.loader;

import org.eclipse.core.resources.IResource;

public class LoadException extends Exception {
	private static final long serialVersionUID = 7352720027474445115L;

	private final IResource resource;

	private final String message;

	private final Exception e;

	public LoadException(IResource resource, String message, Exception e) {
		super();
		this.resource = resource;
		this.message = message;
		this.e = e;
	}

	public Exception getE() {
		return e;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public IResource getResource() {
		return resource;
	}
}
