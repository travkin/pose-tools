/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Andre Backofen <andreb@mail.uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.core._DEPRECIATED.validation;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;

/**
 * A simple validator interface. Implementing classes are able to validate something.
 */
public interface IValidator {
	/**
	 * Validates something and returns the result as {@link Diagnostic}. Shows progress on the given
	 * {@link IProgressMonitor}.
	 * 
	 * @param progressMonitor
	 * @return
	 */
	Diagnostic validate(EObject validationObject, IProgressMonitor progressMonitor);
}
