package de.upb.pose.core.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

public abstract class CreateConnectionFeature extends AbstractCreateConnectionFeature {
	
	public CreateConnectionFeature(IFeatureProvider fp, String name) {
		this(fp, name, "Create " + name);
	}

	public CreateConnectionFeature(IFeatureProvider fp, String name, String description) {
		super(fp, name, description);
	}

	@Override
	public boolean canStartConnection(ICreateConnectionContext context) {
		PictogramElement spe = context.getSourcePictogramElement();
		Object sbo = getBusinessObjectForPictogramElement(spe);
		if (sbo instanceof EObject) {
			return canStart((EObject) sbo);
		}
		return false;
	}

	protected abstract boolean canStart(EObject source);

	@Override
	public boolean canCreate(ICreateConnectionContext context) {
		PictogramElement spe = context.getSourcePictogramElement();
		Object sbo = getBusinessObjectForPictogramElement(spe);

		PictogramElement tpe = context.getTargetPictogramElement();
		Object tbo = getBusinessObjectForPictogramElement(tpe);

		if (sbo instanceof EObject && tbo instanceof EObject) {
			return canCreate((EObject) sbo, (EObject) tbo);
		}
		return false;
	}

	protected abstract boolean canCreate(EObject source, EObject target);
}
