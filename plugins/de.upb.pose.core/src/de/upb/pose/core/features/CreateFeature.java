package de.upb.pose.core.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;

/**
 * Default implementation of a create feature. Only the {@link #doCreate(ICreateContext)} method
 * has to be implemented. This method is expected to create the model element.
 * Creation of the visual representation is triggered by trying to find
 * and execute the corresponding add feature.
 * 
 * @author Dietrich Travkin
 */
public abstract class CreateFeature extends AbstractCreateFeature {
	private final boolean isDirectEditingActive;

	public CreateFeature(IFeatureProvider fp, String name) {
		this(fp, name, true);
	}

	public CreateFeature(IFeatureProvider fp, String name, boolean isDirectEditingActive) {
		super(fp, name, "Create " + name);

		this.isDirectEditingActive = isDirectEditingActive;
	}

	@Override
	public Object[] create(ICreateContext context) {
		EObject element = doCreate(context);

		addGraphicalRepresentation(context, element);

		getFeatureProvider().getDirectEditingInfo().setActive(isDirectEditingActive);

		return new Object[] { element };
	}

	protected abstract EObject doCreate(ICreateContext context);
}
