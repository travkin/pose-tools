package de.upb.pose.core.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;

/**
 * Default implementation of an add feature. Only the {@link #add(IAddContext)} method
 * has to be implemented. This method is expected to create the model element's
 * visual representation. Layouting the visual (graphical) representation
 * should be triggered by calling {@link #layoutPictogramElement(org.eclipse.graphiti.mm.pictograms.PictogramElement)}.
 * This call tries to find and execute the corresponding layout feature.
 * 
 * @author Dietrich Travkin
 */
public abstract class AddFeature extends AbstractAddFeature {
	public AddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object bo = context.getNewObject();
		if (bo instanceof EObject) {
			return canAdd((EObject) bo);
		}
		return false;
	}

	protected abstract boolean canAdd(EObject element);
}
