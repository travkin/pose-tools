package de.upb.pose.core.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

public abstract class AddConnectionFeature extends AddFeature {
	
	public AddConnectionFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public PictogramElement add(IAddContext context) {
		return add((IAddConnectionContext) context);
	}

	protected abstract Connection add(IAddConnectionContext context);

	@Override
	public boolean canAdd(IAddContext context) {
		if (context instanceof IAddConnectionContext) {
			return super.canAdd(context);
		}
		return false;
	}
}
