package de.upb.pose.modeling.stories.util;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;

public class TextUtil {
	public static String getText(EOperation operation) {
		return append(new StringBuilder(), operation).toString();
	}

	private static StringBuilder append(StringBuilder builder, EOperation operation) {
		// container
		EClass eClass = operation.getEContainingClass();
		if (eClass != null) {
			builder.append(eClass.getName());
			builder.append('#');
		}

		// name
		builder.append(operation.getName());

		// parameters
		builder.append('(');
		for (EParameter parameter : operation.getEParameters()) {
			EClassifier eType = parameter.getEType();
			if (eType != null) {
				builder.append(eType.getName());
			} else {
				builder.append(eType);
			}
			builder.append(',');
			builder.append(' ');
		}
		if (!operation.getEParameters().isEmpty()) {
			builder.delete(builder.length() - 2, builder.length());
		}

		builder.append(')');
		builder.append(':');
		builder.append(' ');

		// type
		if (operation.getEType() != null) {
			builder.append(operation.getEType().getName());
		}

		return builder;
	}
}
