package de.upb.pose.modeling.classes.editor.edit.policies;

import org.eclipse.emf.ecoretools.diagram.edit.policies.EPackageItemSemanticEditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.upb.pose.modeling.classes.commands.PatternApplicationCreateCommand;
import de.upb.pose.modeling.classes.editor.edit.PoseEcoreConstants;

public class CustomEPackageItemSemanticEditPolicy extends EPackageItemSemanticEditPolicy {
	
	@Override
	protected Command getCreateCommand(CreateElementRequest req) {
		if (PoseEcoreConstants.TYPE_PATTERN_APPLICATION.equals(req.getElementType())) {
			return getGEFWrapper(new PatternApplicationCreateCommand(req));
		}

		return super.getCreateCommand(req);
	}
}
