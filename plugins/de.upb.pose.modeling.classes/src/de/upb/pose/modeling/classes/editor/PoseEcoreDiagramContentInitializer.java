/**
 * 
 */
package de.upb.pose.modeling.classes.editor;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EReferenceEditPart;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramContentInitializerCopy;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditorPlugin;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramUpdater;
import org.eclipse.emf.ecoretools.diagram.part.EcoreLinkDescriptor;
import org.eclipse.emf.ecoretools.diagram.part.EcoreNodeDescriptor;
import org.eclipse.emf.ecoretools.diagram.part.EcoreVisualIDRegistry;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;

/**
 * @author Dietrich Travkin
 */
public class PoseEcoreDiagramContentInitializer extends EcoreDiagramContentInitializerCopy {
	
	private boolean onlyAddViewElementsIfTheyAreMissing = true;
	
	public PoseEcoreDiagramContentInitializer(boolean onlyAddMissingViewElements) {
		this.onlyAddViewElementsIfTheyAreMissing = onlyAddMissingViewElements;
	}

	@Override
	protected void createNode(View parentView, EcoreNodeDescriptor nodeDescriptor) {
		Node node =  (Node) findViewForDescriptor(parentView, nodeDescriptor);
		
		if (node == null || !this.onlyAddViewElementsIfTheyAreMissing) {
			node = ViewService.createNode(
							parentView,
							nodeDescriptor.getModelElement(),
							EcoreVisualIDRegistry.getType(nodeDescriptor.getVisualID()),
							EcoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		}
		
		// also add children if they are missing
		if (node != null) {
			switch (nodeDescriptor.getVisualID()) {
			case AppliedPatternEditPart.VISUAL_ID:
				createAppliedPattern_1999Children(node);
				return;
			default:
				super.createNodeChildren(node, nodeDescriptor);
			}
		}
	}
	
	private Edge createLink(Diagram diagram, EcoreLinkDescriptor linkDescriptor) {
		if (this.onlyAddViewElementsIfTheyAreMissing
				&& isEdgeAlreadyThere(diagram, linkDescriptor)) {
			return null;
		}
		
		Edge edge = (Edge) ViewService.getInstance()
				.createEdge(
						linkDescriptor.getSemanticAdapter(),
						diagram,
						EcoreVisualIDRegistry.getType(linkDescriptor.getVisualID()),
						ViewUtil.APPEND,
						EcoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		return edge;
	}
	
	@Override
	protected void createLinks(Diagram diagram) {
		for (boolean continueLinkCreation = true; continueLinkCreation;) {
			continueLinkCreation = false;
			Collection<EcoreLinkDescriptor> additionalDescriptors = new LinkedList<EcoreLinkDescriptor>();
			for (Iterator<EcoreLinkDescriptor> it = getLinkDescriptorsIterator(); it.hasNext();) {
				EcoreLinkDescriptor nextLinkDescriptor = (EcoreLinkDescriptor) it.next();
				if (!containsKeyInDomain2NotationMap(nextLinkDescriptor.getSource()) || !containsKeyInDomain2NotationMap(nextLinkDescriptor.getDestination())) {
					continue;
				}
				Edge edge = createLink(diagram, nextLinkDescriptor);
				if (edge != null) {
					edge.setSource(getFromDomain2NotationMap(nextLinkDescriptor.getSource()));
					edge.setTarget(getFromDomain2NotationMap(nextLinkDescriptor.getDestination()));
					it.remove();
					if (nextLinkDescriptor.getModelElement() != null) {
						addToDomain2NotationMap(nextLinkDescriptor.getModelElement(), edge);
					}
					continueLinkCreation = true;
					switch (nextLinkDescriptor.getVisualID()) {
					case EReferenceEditPart.VISUAL_ID:
						additionalDescriptors.addAll(EcoreDiagramUpdater.getEReference_3002OutgoingLinks(edge));
						break;
					}
				}
			}
			addAllToLinkDescriptors(additionalDescriptors);
		}
	}
	
	private boolean equalVisualIds(View view, EcoreNodeDescriptor nodeDescriptor) {
		String type = view.getType();
		String desiredType = String.valueOf(nodeDescriptor.getVisualID());
		if (desiredType.equals(type)) {
			return true;
		}
		return false;
	}
	
	private boolean equalModels(View view, EcoreNodeDescriptor nodeDescriptor) {
		if ( (view.getElement() == null && nodeDescriptor.getModelElement() != null)
				|| (view.getElement() != null && nodeDescriptor.getModelElement() == null) ) {
			return false;
		}
		
		if (view.getElement() != null && !view.getElement().equals(nodeDescriptor.getModelElement())) {
			return false;
		}
		
		// also check source and target in case of an edge
		if (view instanceof Edge) {
			Edge edge = (Edge) view;
			EcoreLinkDescriptor linkDescriptor = (EcoreLinkDescriptor) nodeDescriptor;
			
			EObject edgeSource = (edge.getSource() != null ? edge.getSource().getElement() : null);
			EObject edgeTarget = (edge.getTarget() != null ? edge.getTarget().getElement() : null);
			
			EObject linkSource = linkDescriptor.getSource();
			EObject linkTarget = linkDescriptor.getDestination();
			
			return ( (edgeSource == linkSource) && (edgeTarget == linkTarget) );
		}
		
		return true;
	}
	
	private boolean isEdgeAlreadyThere(Diagram parentDiagram, EcoreLinkDescriptor linkDescriptor) {
		for (Object childEdge: parentDiagram.getEdges()) {
			if (childEdge instanceof Edge) {
				Edge edge = (Edge) childEdge;
				if (equalVisualIds(edge, linkDescriptor)
						&& equalModels(edge, linkDescriptor)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private View findViewForDescriptor(View parentView, EcoreNodeDescriptor nodeDescriptor) {
		for (Object child: parentView.getChildren()) {
			if (child instanceof View) {
				View childView = (View) child;
				if (equalVisualIds(childView, nodeDescriptor)
						&& equalModels(childView, nodeDescriptor)) {
					return childView;
				}
			}
		}
		return null;
	}
	
	private void createAppliedPattern_1999Children(Node node) {
		addToDomain2NotationMap(node.getElement(), node);
		addAllToLinkDescriptors(EcoreDiagramUpdaterExtended.getAppliedPattern_1999OutgoingLinks(node));
	}
	
	@Override
	protected void createEPackageContents_5003Children(View view) {
		Collection childNodeDescriptors = EcoreDiagramUpdaterExtended.getEPackageContents_5003SemanticChildren(view);
		for (Iterator it = childNodeDescriptors.iterator(); it.hasNext();) {
			createNode(view, (EcoreNodeDescriptor) it.next());
		}
	}
	
	@Override
	protected void createEPackage_79Children(View view) {
		Collection childNodeDescriptors = EcoreDiagramUpdaterExtended.getEPackage_79SemanticChildren(view);
		for (Iterator it = childNodeDescriptors.iterator(); it.hasNext();) {
			createNode(view, (EcoreNodeDescriptor) it.next());
		}
	}
	
}
