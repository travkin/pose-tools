/**
 * 
 */
package de.upb.pose.modeling.classes.commands;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecoretools.diagram.providers.EcoreElementTypes;
import org.eclipse.gmf.runtime.emf.core.util.PackageUtil;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;

import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplicationDiagrams2ModelConnector;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.ui.dialogs.SelectPatternSpecificationDialog;
import de.upb.pose.mapping.ui.editor.multipage.MultiPagePatternRoleMappingDiagramEditor;
import de.upb.pose.mapping.ui.mapping.MappingDiagramCreator;
import de.upb.pose.mapping.ui.wizards.NewPatternApplicationsModelWizard;
import de.upb.pose.modeling.classes.Activator;
import de.upb.pose.patternapplication.PatternApplicationCreator;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelCreator;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.ui.PatternSpecificationDiagrams2ModelConnector;

/**
 * Command for the creation of a new design pattern application. Should do the following:
 * <ul>
 *   <li>determine mapping model and mapping diagram files (maybe let the user create them) (editing domain: <b>mapping</b>)</li>
 *   <li>if necessary, create the root node for the mapping model and mapping diagram model (editing domain: <b>mapping</b>)</li>
 *   <li>create the pattern application in the mapping model (editing domain: <b>mapping</b>)</li>
 *   <li>link the created pattern application to the ecore model (editing domain: <b>mapping</b>)</li>
 *   <li>link the ecore model to the created pattern application (editing domain: <b>ecore</b>)</li>
 *   <li>create the graphical representation of the pattern application in the ecore diagram (editing domain: <b>ecore or mapping?</b>)</li>
 *   <li>determine the pattern specification to be used (maybe let the user choose it) (editing domain: <b>mapping</b>)</li>
 *   <li>link the pattern application to the chosen pattern specification (editing domain: <b>mapping</b>)</li>
 *   <li>create the so called pattern application model in the mapping model (editing domain: <b>mapping</b>)</li>
 *   <li>create the diagram for the created pattern application model in the mapping diagram model (editing domain: <b>mapping</b>)</li>
 * </ul>
 * 
 * @author Dietrich Travkin
 */
public class PatternApplicationCreateCommand extends CreateElementCommand {

	private PatternSpecification patternSpecification = null;
	
	public PatternApplicationCreateCommand(CreateElementRequest request) {
		super(request);
	}
	
	public PatternApplicationCreateCommand(CreateElementRequest request, PatternSpecification patternSpecification) {
		super(request);
		this.patternSpecification = patternSpecification;
	}
	
	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand#getRequest()
	 */
	@Override
	protected CreateElementRequest getRequest() {
		return (CreateElementRequest) super.getRequest();
	}

	@Override
	public boolean canExecute() {
		return (findContainerPackage() != null);
	}
	
	protected EReference getContainmentFeature() {
		if (super.getContainmentFeature() == null) {
			EClass classToEdit = getEClassToEdit();
			
			if (classToEdit != null) {
				IElementType type = EcoreElementTypes.EAnnotation_1003;
				EReference containmentFeature = PackageUtil.findFeature(classToEdit, type.getEClass());
				if (containmentFeature != null) {
					this.setContainmentFeature(containmentFeature);
				}
			}
		}
		
		return super.getContainmentFeature();
	}
	
	@Override
	protected EObject doDefaultElementCreation() {
		AppliedPattern patternApplication = null;
		String errorMessage = null;
		
		EPackage containerPackage = findContainerPackage();
		
		// ensure, there is a mapping model
		PatternApplications patternApplicationsRoot = PatternApplications2EcoreConnector.getLinkedPatternApplications(containerPackage);
		if (patternApplicationsRoot == null) {
			// create mapping model and diagram files and link them to the package
			URI diagramsUri = openNewMappingWizardDialog(containerPackage);
			
			// load the created files and created pattern applications root
			if (diagramsUri != null) {
				ResourceSet resourceSet = containerPackage.eResource().getResourceSet();
				Resource diagramsResource = resourceSet.getResource(diagramsUri, true);
				if (diagramsResource != null) {
					EList<EObject> diagramResourceContents = diagramsResource.getContents();
					if (diagramResourceContents != null
							&& !diagramResourceContents.isEmpty()
							&& diagramResourceContents.size() == 1
							&& diagramResourceContents.get(0) instanceof ContainerShape) {
						ContainerShape diagramsContainer = (ContainerShape) diagramResourceContents.get(0);
						patternApplicationsRoot = PatternApplicationDiagrams2ModelConnector.getPatternApplicationsModel(diagramsContainer);
					}
				}
			}
			
			// cancel if creation failed
			if (patternApplicationsRoot == null && errorMessage == null) {
				errorMessage = "Couldn't create mapping model and pattern applications.";
			}
			
			// create a pointer to the pattern applications root
			EAnnotation pointerToPatternApplicationsModel = PatternApplications2EcoreConnector.getPointerToPatternApplications(containerPackage);
			if (pointerToPatternApplicationsModel == null) {
				pointerToPatternApplicationsModel = PatternApplications2EcoreConnector.createPointerToPatternApplications(containerPackage);
			}
			
			// link the EPackage to the created pattern applications root
			PatternApplications2EcoreConnector.linkEPackageToPatternApplicationsRoot(containerPackage, patternApplicationsRoot);
		}
		
		// determine the pattern specification
		if (this.patternSpecification == null) {
			this.patternSpecification = letUserChoosePatternSpecification(containerPackage);
			if (this.patternSpecification == null && errorMessage == null) {
				errorMessage = "No pattern specification selected to be applied.";
			}
		}

		// create the new pattern application, its diagram, and configure the pattern application representation in ecore class diagram
		Diagram mappingDiagram = null;
		if (patternApplicationsRoot != null && this.patternSpecification != null) {
			patternApplication = PatternApplicationCreator.createPatternApplicationFor(this.patternSpecification, patternApplicationsRoot);

			// TODO check if this is really necessary
			Diagram specificationDiagram = PatternSpecificationDiagrams2ModelConnector.findPatternSpecificationDiagram(this.patternSpecification);
			if (specificationDiagram == null) {
				errorMessage = "Could not find pattern specification diagram for the specification " + this.patternSpecification.getName();
			} else {
				mappingDiagram = MappingDiagramCreator.createMappingDiagramFor(patternApplication, specificationDiagram);
			}
			
			if (mappingDiagram == null) {
				errorMessage = "Could not create a mapping diagram.";
			}
		}
		
		if (patternApplication == null && errorMessage == null) {
			errorMessage = "Could not create a new pattern application."; 
		}
		
		// create an initial pattern application model for the created pattern application
		if (errorMessage == null && patternApplication != null) {
			ApplicationModel applicationModel = ApplicationModelCreator.createInitialApplicationModelFromMapping(patternApplication);
			
			if (applicationModel == null) {
				errorMessage = "Could not create and initialize an application model for the pattern application.";
			}
		}
		
		if (errorMessage == null) {
			Assert.isNotNull(patternApplication);
			Assert.isNotNull(patternApplication.getPatternSpecification());
			Assert.isNotNull(patternApplication.getApplicationModel());
			Assert.isNotNull(mappingDiagram);
		}
		
		IStatus status = (errorMessage == null) ? Status.OK_STATUS : new Status(Status.ERROR, Activator.PLUGIN_ID, errorMessage);
		setDefaultElementCreationStatus(status);
		
//		if (mappingDiagram != null) {
//			openMappingDiagramInEditor(mappingDiagram);
//		}
				
		return patternApplication;
	}
	
	private EPackage findContainerPackage() {
		EObject containerObject = this.getRequest().getContainer();
		if (containerObject == null) {
			containerObject = this.getRequest().createContainer();
		}
		
		// take the business object container
		if (containerObject instanceof View) {
			containerObject = ((View) containerObject).getElement();
		}
		
		if (containerObject instanceof EPackage) {
			return (EPackage) containerObject;
		}
		
		return null;
	}
	
	private URI openNewMappingWizardDialog(EPackage containerPackage) {
		NewPatternApplicationsModelWizard newMappingWizard = new NewPatternApplicationsModelWizard(false);
		newMappingWizard.init(PlatformUI.getWorkbench(), new StructuredSelection(containerPackage));
		
		Shell parent = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		WizardDialog dialog = new WizardDialog(parent, newMappingWizard);
		dialog.create();
		int success = dialog.open();
		
		if (success == Window.OK) {
			URI diagramsUri = newMappingWizard.getDiagramsResourceURI();
			return diagramsUri;
		}
		return null;
	}
	
	private void openMappingDiagramInEditor(Diagram graphitiDiagram) {
//		DiagramEditorInput diagramInput = DiagramEditorInput.createEditorInput(
//				graphitiDiagram,
//				RoleMappingDiagramTypeProvider.DIAGRAM_TYPE_PROVIDER_ID);
		
		String filePath = graphitiDiagram.eResource().getURI().toPlatformString(true);
		IFile file = (IFile) ResourcesPlugin.getWorkspace().getRoot().findMember(filePath);
		IEditorInput fileInput = new FileEditorInput(file);
		
		try {
			MultiPagePatternRoleMappingDiagramEditor editor = (MultiPagePatternRoleMappingDiagramEditor) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.openEditor(
						fileInput,
						MultiPagePatternRoleMappingDiagramEditor.ID);
			
			// TODO save and then open the diagram?
			//editor.openDiagram(graphitiDiagram);
		} catch (PartInitException e) {
			e.printStackTrace();
			MessageDialog.openError(Display.getCurrent().getActiveShell(),
					"Opening editor failed", "Created diagram couldn't be opened in editor.\n" +
							"Editor id: " + MultiPagePatternRoleMappingDiagramEditor.ID + "\n" +
							"diagram URI: " + graphitiDiagram.eResource().getURI().toString() + ".");
		}
		
	}
	
	private PatternSpecification letUserChoosePatternSpecification(EPackage containerPackage) {
		Resource ecoreModelResource = containerPackage.eResource();
		ResourceSet resourceSet = ecoreModelResource.getResourceSet();
		
		SelectPatternSpecificationDialog specificationDialog = new SelectPatternSpecificationDialog(resourceSet, ecoreModelResource);
		
		if (specificationDialog.open() == Window.OK) {
			return specificationDialog.getSpecification();
		}
		
		return null;
	}
	
}
