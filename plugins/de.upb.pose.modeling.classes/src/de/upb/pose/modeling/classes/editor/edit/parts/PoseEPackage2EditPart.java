/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackage2EditPart;
import org.eclipse.emf.ecoretools.diagram.edit.policies.OpenDiagramEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @author Dietrich Travkin
 */
public class PoseEPackage2EditPart extends EPackage2EditPart {

	public PoseEPackage2EditPart(View view) {
		super(view);
	}

	/**
	 * @generated NOT
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		
		removeEditPolicy(EditPolicyRoles.OPEN_ROLE);
		installEditPolicy(EditPolicyRoles.OPEN_ROLE, new OpenDiagramEditPolicy());
	}
	
}
