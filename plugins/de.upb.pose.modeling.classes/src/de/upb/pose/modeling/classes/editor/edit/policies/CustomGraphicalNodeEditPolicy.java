/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.GraphicalNodeEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest.ConnectionViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;

/**
 * @author Dietrich Travkin
 */
public class CustomGraphicalNodeEditPolicy extends GraphicalNodeEditPolicy {

	public static CreateConnectionViewRequest createCreateConnectionViewRequest(RoleBinding semanticConnection, PreferencesHint preferencesHint) {
		String semanticHint = String.valueOf(RoleBindingEditPart.VISUAL_ID);
		CreateConnectionViewRequest createConnectionViewRequest = new CreateConnectionViewRequest(
				new ConnectionViewDescriptor(new EObjectAdapter(semanticConnection), semanticHint, true, preferencesHint));
		return createConnectionViewRequest;
	}
	
	public static Command getCreateConnectionViewCommand(CreateConnectionViewRequest request, AppliedPatternEditPart sourceEditPart, EditPart targetEditPart) {
		return CreateConnectionViewRequest.getCreateCommand(request, sourceEditPart, targetEditPart);
	}
	
	public static Command getCreateConnectionViewCommand(RoleBinding semanticConnection, PreferencesHint preferencesHint, AppliedPatternEditPart sourceEditPart, EditPart targetEditPart) {
		if (semanticConnection != null
				&& sourceEditPart != null
				&& targetEditPart != null
				&& sourceEditPart.isActive()
				&& targetEditPart.isActive()) {
			
			CreateConnectionViewRequest createConnectionViewRequest = CustomGraphicalNodeEditPolicy
					.createCreateConnectionViewRequest(
							semanticConnection,
							preferencesHint);
			Command createConnectionViewCommand = CustomGraphicalNodeEditPolicy
					.getCreateConnectionViewCommand(
							createConnectionViewRequest,
							sourceEditPart,
							targetEditPart);
			return createConnectionViewCommand;
		}
		return null;
	}
	
	@Override
	public Command getCommand(Request request) {
		
		if (request instanceof CreateConnectionViewRequest) {
			CreateConnectionViewRequest createConnectionViewRequest = (CreateConnectionViewRequest) request;
			EditPart sourceEP = createConnectionViewRequest.getSourceEditPart();
			EditPart targetEP = createConnectionViewRequest.getTargetEditPart();
			
			if (RequestConstants.REQ_CONNECTION_START.equals(request.getType())) {
				if (sourceEP instanceof AppliedPatternEditPart) {
					return getConnectionCreateCommand(createConnectionViewRequest);
				}
			} else if (RequestConstants.REQ_CONNECTION_END.equals(request.getType())) {
				
				if (sourceEP != null && targetEP != null && sourceEP instanceof AppliedPatternEditPart) {
					return getConnectionCompleteCommand(createConnectionViewRequest);
				}
			}
		}
		
		return super.getCommand(request);
	}

}
