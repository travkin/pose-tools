/**
 * 
 */
package de.upb.pose.modeling.classes.outline;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.upb.pose.specification.DesignPatternCatalog;

/**
 * @author Dietrich Travkin
 */
public class DesignPatternResources extends org.eclipse.emf.ecoretools.diagram.ui.outline.AdditionalResources
{
	/**
	 * Constructor
	 * 
	 * @param rSet the ResourceSet to be used to load these Additional Resources
	 */
	public DesignPatternResources(ResourceSet rSet)
	{
		super(rSet);
	}
	
	public List<Resource> getResources()
	{
		List<Resource> additionnalResources = super.getResources();
		List<Resource> filteredResources = new ArrayList<Resource>();;
		for (Resource resource : additionnalResources) {
			// only add resources with a design pattern catalog
			if (resource.getContents().size() == 1)
			{
				EObject root = resource.getContents().get(0);
				if (root instanceof DesignPatternCatalog)
				{
					filteredResources.add(resource);;
				}
			}
		}

		return filteredResources;
	}
	
	protected ResourceSet getResourceSet()
	{
		List<Resource> resources = super.getResources();
		if (!resources.isEmpty())
		{
			return resources.get(0).getResourceSet();
		}
		return null;
	}
}
