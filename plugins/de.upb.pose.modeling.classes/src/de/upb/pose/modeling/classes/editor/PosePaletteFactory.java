package de.upb.pose.modeling.classes.editor;

import java.util.Collections;

import org.eclipse.gef.Tool;
import org.eclipse.gmf.runtime.diagram.ui.services.palette.PaletteFactory.Adapter;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

import de.upb.pose.modeling.classes.editor.edit.PoseEcoreConstants;

public class PosePaletteFactory extends Adapter {
	
	@Override
	public Tool createTool(String id) {
		if (PoseEcoreConstants.ID_PATTERN.equals(id)) {
			return new UnspecifiedTypeCreationTool(Collections.singletonList(PoseEcoreConstants.TYPE_PATTERN_APPLICATION));
		}
		return super.createTool(id);
	}
}
