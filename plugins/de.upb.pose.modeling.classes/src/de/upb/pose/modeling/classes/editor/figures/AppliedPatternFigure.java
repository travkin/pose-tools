package de.upb.pose.modeling.classes.editor.figures;

import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.gmf.runtime.gef.ui.internal.figures.CircleFigure;

public class AppliedPatternFigure extends CircleFigure {
	
	private final Label label;

	private boolean isComplete;

	public AppliedPatternFigure(String name, boolean isComplete, int width, int height) {
		super(width, height);

		// layout
		this.setLayoutManager(new BorderLayout());

		// name label
		this.label = new Label(name);

		this.label.setBorder(new MarginBorder(4, 4, 4, 4));
		if (!isComplete) {
			// this.label.setIcon(Images.getImageDescriptor(Images.WARNING_ICON).createImage());
		}
		add(this.label, BorderLayout.CENTER);
	}

	public boolean isComplete() {
		return this.isComplete;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;

		if (!isComplete) {
			// this.label.setIcon(Images.getImageDescriptor(Images.WARNING_ICON).createImage());
		}
	}

	public void setName(String name) {
		this.label.setText(name);
	}

	@Override
	protected void paintFigure(Graphics g) {
		g.setLineStyle(getLineStyle());
		g.setLineWidth(getLineWidth());

		super.paintFigure(g);
	}
}
