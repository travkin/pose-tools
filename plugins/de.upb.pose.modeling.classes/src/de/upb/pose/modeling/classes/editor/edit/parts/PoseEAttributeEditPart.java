package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EAttributeEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.modeling.classes.PoseEcoreImages;
import de.upb.pose.modeling.classes.editor.FilteredEContentListener;

public class PoseEAttributeEditPart extends EAttributeEditPart {
	
	public PoseEAttributeEditPart(View view) {
		super(view);
	}
	
	@Override
	protected void addSemanticListeners() {
		super.addSemanticListeners();
		
		// also listen to changes in child role bindings to update role binding connections
		FilteredEContentListener.addSemanticElementListenerToModelsResourceSet(this, new FilteredEContentListener() {
			
			@Override
			protected void roleBindingModelElementsFeatureChanged(Notification notification) {
				refreshLabel();
			}
			
		});
    }

	@Override
	protected Image getLabelIcon() {
		if (RoleMapper.isMapped(resolveSemanticElement())) {
			return PoseEcoreImages.get(PoseEcoreImages.MAPPED_EATTRIBUTE);
		}
		return super.getLabelIcon();
	}
}
