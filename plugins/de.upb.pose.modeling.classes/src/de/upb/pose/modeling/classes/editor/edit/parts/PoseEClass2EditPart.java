/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EClass2EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.policies.CustomGraphicalNodeEditPolicy;

/**
 * @author Dietrich Travkin
 *
 */
public class PoseEClass2EditPart extends EClass2EditPart {

	public PoseEClass2EditPart(View view) {
		super(view);
	}
	
	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		
		removeEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE);
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new CustomGraphicalNodeEditPolicy());
	}

}
