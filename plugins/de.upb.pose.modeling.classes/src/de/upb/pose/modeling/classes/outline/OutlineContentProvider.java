package de.upb.pose.modeling.classes.outline;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;

import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.Category;

public class OutlineContentProvider extends AdapterFactoryContentProvider {
	
	public OutlineContentProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public Object[] getChildren(Object element)
	{
		// TODO adapt this implementation to use PatternApplicationsContentProvider from mapping.ui plug-in 
		
		// list referenced resources
		if (element instanceof AdditionalResources)
		{
			return ((AdditionalResources) element).getResources().toArray();
		}
		
		// list design pattern catalog resources
		if (element instanceof DesignPatternResources)
		{
			return ((DesignPatternResources) element).getResources().toArray();
		}

		// show all applied patterns
		if (element instanceof PatternApplications)
		{
			Collection<?> children = ((PatternApplications) element).getApplications();
			return children.toArray(new Object[children.size()]);
		}

		// set bindings, (non-set) role bindings and app. model
		if (element instanceof AppliedPattern)
		{
			Collection<Object> children = new ArrayList<Object>();

			// add set bindings
			children.addAll(((AppliedPattern) element).getSetFragmentBindings());

			// add (non-set) role bindings
			for (RoleBinding roleBinding : ((AppliedPattern) element).getRoleBindings())
			{
				if (roleBinding.getContainingSetFragmentInstances().isEmpty())
				{
					children.add(roleBinding);
				}
			}

			// add application model
			ApplicationModel model = ((AppliedPattern) element).getApplicationModel();
			if (model != null)
			{
				children.add(model);
			}

			return children.toArray(new Object[children.size()]);
		}

		// for set bindings
		if (element instanceof SetFragmentBinding) {
			Collection<?> children = ((SetFragmentBinding) element).getSetFragmentInstances();

			return children.toArray(new Object[children.size()]);
		}

		// for set element bindings
		if (element instanceof SetFragmentInstance) {
			Collection<?> children = ((SetFragmentInstance) element).getContainedRoleBindings();

			return children.toArray(new Object[children.size()]);
		}

		// for role bindings
		if (element instanceof RoleBinding) {
			Collection<Object> children = new ArrayList<Object>();

			children.add(((RoleBinding) element).getRole());
			
			children.addAll(((RoleBinding) element).getModelElements());

			return children.toArray(new Object[children.size()]);
		}
		
		if (element instanceof Category)
		{
			List<Object> children = new ArrayList<Object>(((Category) element).getChildren());
			children.addAll(((Category) element).getPatterns());
			return children.toArray();
		}

		// work-around to remove redundant child elements calculated by the super class
		Object[] children = super.getChildren(element);
		children = removeRedundantEntries(children);
		return children;
	}
	
	private Object[] removeRedundantEntries(Object[] elements) {
		if (elements == null || elements.length == 0) {
			return elements;
		}
		
		ArrayList<Object> selectedElements = new ArrayList<Object>(elements.length);
		for (Object element: elements) {
			if (!selectedElements.contains(element)) {
				selectedElements.add(element);
			}
		}
		return selectedElements.toArray();
	}

	@Override
	public boolean hasChildren(Object object) {
		return getChildren(object).length > 0;
	}

	@Override
	public Object[] getElements(Object element)
	{
		// getting resource as input
		if (element instanceof Resource)
		{
			// resource contents
			EList<EObject> contents = ((Resource) element).getContents();
			List<Object> children = new ArrayList<Object>(contents);
			
			// pattern applications
			for (EObject rootElement: contents)
			{
				if (rootElement instanceof EModelElement)
				{
					PatternApplications applications = getApplications((EModelElement) rootElement);
					if (applications != null)
					{
						children.add(applications);
					}
				}
			}
			
			// loaded pattern catalogs
			children.add(new DesignPatternResources(((Resource) element).getResourceSet()));
			
			// additional resources
			children.add(new AdditionalResources(((Resource) element).getResourceSet()));

			return children.toArray();
		}
		return super.getElements(element);
	}

	private static PatternApplications getApplications(EModelElement element)
	{
		// This should not be necessary on new diagrams, the constant from 'core' is used now
		// EAnnotation annotation =  element.getEAnnotation(CoreConstants.SOURCE_MAPPINGS);
		for (EAnnotation annotation : element.getEAnnotations())
		{
			if (annotation != null)
			{
				for (EObject reference : annotation.getReferences())
				{
					if (reference instanceof PatternApplications)
					{
						return (PatternApplications) reference;
					}
				}
			}
		}

		return null;
	}
}
