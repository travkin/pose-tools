package de.upb.pose.modeling.classes.editor;

import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramActionBarContributor;


public class PoseEcoreDiagramActionBarContributor extends EcoreDiagramActionBarContributor
{
	@Override
	protected String getEditorId()
	{
		return PoseEcoreEditor.ID;
	}

	@Override
	protected Class<?> getEditorClass()
	{
		return PoseEcoreEditor.class;
	}
}
