/**
 * 
 */
package de.upb.pose.modeling.classes.editor;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramUpdater;
import org.eclipse.emf.ecoretools.diagram.part.EcoreLinkDescriptor;
import org.eclipse.emf.ecoretools.diagram.part.EcoreNodeDescriptor;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.modeling.classes.editor.edit.PoseEcoreConstants;
import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;

/**
 * @author Dietrich Travkin
 */
public class EcoreDiagramUpdaterExtended {
	
	public static Collection<EcoreLinkDescriptor> getAppliedPattern_1999OutgoingLinks(Node view) {
		AppliedPattern source = (AppliedPattern) view.getElement();
		List<EcoreLinkDescriptor> result = new LinkedList<EcoreLinkDescriptor>();
		for (RoleBinding roleBinding: source.getRoleBindings()) {
			for (EObject mappedModelElement: roleBinding.getModelElements()) {
					result.add(new EcoreLinkDescriptor(
							source, mappedModelElement, roleBinding,
							PoseEcoreConstants.TYPE_ROLE_BINDING,
							RoleBindingEditPart.VISUAL_ID));
			}
		}
		return result;
	}
	
	private static List addPatternApplicationViewChildren(View view, EPackage packageModel, List childrenList) {
		PatternApplications patternApplications = PatternApplications2EcoreConnector.getLinkedPatternApplications(packageModel);
		if (patternApplications != null && !patternApplications.getApplications().isEmpty()) {
			for (AppliedPattern patternApplication: patternApplications.getApplications()) {
				//int visualID = EcoreVisualIDRegistry.getNodeVisualID(view, patternApplication); // returns -1
				int visualID = AppliedPatternEditPart.VISUAL_ID;
				childrenList.add(new EcoreNodeDescriptor(patternApplication, visualID));
			} 
		}
		return childrenList;
	}
	
	public static List getEPackageContents_5003SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		EPackage modelElement = (EPackage) containerView.getElement();
		List result = EcoreDiagramUpdater.getEPackageContents_5003SemanticChildren(view);
		result = addPatternApplicationViewChildren(view, modelElement, result);
		return result;
	}
	
	public static List getEPackage_79SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		EPackage modelElement = (EPackage) view.getElement();
		List result = EcoreDiagramUpdater.getEPackage_79SemanticChildren(view);
		result = addPatternApplicationViewChildren(view, modelElement, result);
		return result;
	}

}
