package de.upb.pose.modeling.classes;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.swt.graphics.Image;

public final class PoseEcoreImages
{
	public static final String MAPPED_ECLASS = "icons/EClass_mapped.png"; //$NON-NLS-1$
	public static final String MAPPED_EATTRIBUTE = "icons/EAttribute_mapped.png"; //$NON-NLS-1$
	public static final String MAPPED_EOPERATION = "icons/EOperation_mapped.png"; //$NON-NLS-1$

	private static Collection<String> paths;

	public static Image get(String key)
	{
		return Activator.get().getImage(key);
	}

	public static Collection<String> getPaths()
	{
		if (paths == null)
		{
			paths = new ArrayList<String>();

			paths.add(MAPPED_ECLASS);
			paths.add(MAPPED_EATTRIBUTE);
			paths.add(MAPPED_EOPERATION);
		}
		return paths;
	}
}
