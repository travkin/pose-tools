package de.upb.pose.modeling.classes.editor.edit.policies;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.SemanticEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;

@Deprecated
public class AppliedPatternItemSemanticEditPolicy extends SemanticEditPolicy {

	/**
	 * @see org.eclipse.gmf.runtime.diagram.ui.editpolicies.SemanticEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(Request request)
	{
		if (request instanceof CreateConnectionViewRequest) {
			CreateConnectionViewRequest createConnectionViewRequest = (CreateConnectionViewRequest) request;
			EditPart sourceEP = createConnectionViewRequest.getSourceEditPart();
			EditPart targetEP = createConnectionViewRequest.getTargetEditPart();
			
			// TODO create command(s) for the creation of view elements (link from applied pattern to pattern role, i.e. a role binding connection)
			if (RequestConstants.REQ_CONNECTION_START.equals(request.getType())) {
				// ignore
			} else if (RequestConstants.REQ_CONNECTION_END.equals(request.getType())) {
				if (sourceEP != null && targetEP != null) {
					Object sourceView = sourceEP.getModel();
					Object targetView = targetEP.getModel();
				}
			}
		}
		
		return super.getCommand(request);
	}
	
	//
	//
	// @Override
	// protected Command getSemanticCommand(IEditCommandRequest request) {
	// IEditCommandRequest completedRequest = completeRequest(request);
	// return getSemanticCommandSwitch(completedRequest);
	// }
	//
	// private Command getSemanticCommandSwitch(IEditCommandRequest req) {
	// if (req instanceof CreateRelationshipRequest) {
	// return getCreateRelationshipCommand((CreateRelationshipRequest) req);
	// }
	// return null;
	// }
	//
	// private Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
	// if (PoseSemanticType.TYPE_BINDING.equals(req.getElementType())) {
	// return getGEFWrapper(new RoleBindingCreateCommand(req, req.getSource(), req.getTarget()));
	// }
	// return null;
	// }
	//
	// private final Command getGEFWrapper(ICommand cmd) {
	// return new ICommandProxy(cmd);
	// }
}
