package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageContentsEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.policies.CustomEPackageContentsItemSemanticEditPolicy;

public class PoseEPackageContentsEditPart extends EPackageContentsEditPart {
	
	public PoseEPackageContentsEditPart(View view) {
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();

		removeEditPolicy(EditPolicyRoles.SEMANTIC_ROLE);
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CustomEPackageContentsItemSemanticEditPolicy());
	}
}
