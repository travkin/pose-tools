package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.emf.ecoretools.diagram.providers.EcoreEditPartProvider;


public class PoseEcoreEditPartProvider extends EcoreEditPartProvider {
	
	public PoseEcoreEditPartProvider() {
		setFactory(new PoseEcoreEditPartFactory());
		setAllowCaching(true);
	}
}
