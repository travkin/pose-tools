package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.policies.EcoretoolsEditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.policies.CustomDiagramDragDropEditPolicy;
import de.upb.pose.modeling.classes.editor.edit.policies.CustomEPackageItemSemanticEditPolicy;
import de.upb.pose.modeling.classes.editor.edit.policies.PoseEPackageCanonicalEditPolicy;

public class PoseEPackageEditPart extends EPackageEditPart {
	
	public PoseEPackageEditPart(View view) {
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();

		removeEditPolicy(EcoretoolsEditPolicyRoles.PSEUDO_CANONICAL_ROLE);
		installEditPolicy(EcoretoolsEditPolicyRoles.PSEUDO_CANONICAL_ROLE, new PoseEPackageCanonicalEditPolicy());
		
		removeEditPolicy(EditPolicyRoles.SEMANTIC_ROLE);
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new CustomEPackageItemSemanticEditPolicy());

		removeEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE);
		installEditPolicy(EditPolicyRoles.DRAG_DROP_ROLE, new CustomDiagramDragDropEditPolicy());
	}
}
