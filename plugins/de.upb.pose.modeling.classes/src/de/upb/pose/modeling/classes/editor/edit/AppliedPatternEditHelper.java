package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelper;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import de.upb.pose.modeling.classes.commands.PatternApplicationCreateCommand;

public class AppliedPatternEditHelper extends AbstractEditHelper {
	
	@Override
	protected ICommand getCreateCommand(CreateElementRequest req) {
		if (PoseEcoreConstants.TYPE_PATTERN_APPLICATION.equals(req.getElementType())) {
			return new PatternApplicationCreateCommand(req);
		}
		return super.getCreateCommand(req);
	}
}
