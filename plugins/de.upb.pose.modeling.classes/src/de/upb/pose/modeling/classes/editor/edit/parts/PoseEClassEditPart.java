/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EClassEditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.policies.CustomGraphicalNodeEditPolicy;

/**
 * @author Dietrich Travkin
 *
 */
public class PoseEClassEditPart extends EClassEditPart {

	public PoseEClassEditPart(View view) {
		super(view);
	}

	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		
		removeEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE);
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE,
				new CustomGraphicalNodeEditPolicy());
	}
}
