package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.core.providers.AbstractViewProvider;
import org.eclipse.gmf.runtime.diagram.ui.view.factories.ConnectionViewFactory;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;


public class PoseEcoreViewProvider extends AbstractViewProvider {
	
	@Override
	protected Class<?> getNodeViewClass(IAdaptable semanticAdapter, View containerView, String semanticHint) {
		if (containerView != null
				&& PoseEcoreConstants.TYPE_PATTERN_APPLICATION.equals(semanticAdapter.getAdapter(IElementType.class))) {
			return PoseNodeViewFactory.class;
		}

		return super.getNodeViewClass(semanticAdapter, containerView, semanticHint);
	}

	@Override
	protected Class<?> getEdgeViewClass(IAdaptable semanticAdapter, View containerView, String semanticHint) {
		String semHint = String.valueOf(RoleBindingEditPart.VISUAL_ID);
		if (containerView != null
				//&& PoseEcoreConstants.TYPE_ROLE_BINDING.equals(adapter)) {
				&& semanticHint != null && semanticHint.equals(semHint)) {
			return ConnectionViewFactory.class;
		}
		
		return super.getEdgeViewClass(semanticAdapter, containerView, semanticHint);
	}
}
