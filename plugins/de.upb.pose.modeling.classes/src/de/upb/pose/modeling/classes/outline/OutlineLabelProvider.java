package de.upb.pose.modeling.classes.outline;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AdditionalResources;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentInstance;

public class OutlineLabelProvider extends AdapterFactoryLabelProvider
{
	public OutlineLabelProvider(AdapterFactory adapterFactory)
	{
		super(adapterFactory);
	}

	@Override
	public String getText(Object element)
	{
//		if (element instanceof AppliedPattern)
//		{
//			return getText((AppliedPattern) element);
//		}
//
//		if (element instanceof RoleBinding)
//		{
//			return getText((RoleBinding) element);
//		}
//
//		if (element instanceof SetElementBinding)
//		{
//			return getText((SetElementBinding) element);
//		}

		if (element instanceof DesignPatternResources)
		{
			return "Loaded Design Patterns";
		}

		if (element instanceof AdditionalResources)
		{
			return "Additional Resources";
		}

		return super.getText(element);
	}

	private static String getText(SetFragmentInstance element)
	{
		return MappingNameCreator.getNameFor(element);
//		StringBuilder builder = new StringBuilder();
//
//		append(builder, element.eClass());
//		builder.append(' ');
//		builder.append('(');
//		builder.append(element.getSetBinding().getSetElements().indexOf(element) + 1);
//		builder.append(')');
//
//		return builder.toString();
	}

	private static String getText(RoleBinding element)
	{
		return MappingNameCreator.getNameFor(element);
//		StringBuilder builder = new StringBuilder();
//
//		append(builder, element.eClass());
//		builder.append(' ');
//		builder.append('(');
//
//		// binding text
//		if (!element.getModelElements().isEmpty()) {
//			for (EObject model : element.getModelElements()) {
//				if (model instanceof ENamedElement) {
//					builder.append(((ENamedElement) model).getName());
//				} else {
//					builder.append(model);
//				}
//				builder.append(',');
//				builder.append(' ');
//			}
//			int length = builder.length();
//			builder.delete(length - 2, length);
//		} else if (!element.getName().isEmpty()) {
//			builder.append('*');
//			builder.append(element.getName());
//			builder.append('*');
//		} else {
//			builder.append('*');
//			builder.append(element.getRole().getName());
//			builder.append('*');
//		}
//
//		builder.append(')');
//
//		return builder.toString();
	}

	private static String getText(AppliedPattern element)
	{
		return MappingNameCreator.getNameFor(element);
//		StringBuilder builder = new StringBuilder();
//
//		append(builder, element.eClass());
//		builder.append(' ');
//		builder.append('"');
//		append(builder, element.getPatternSpecification());
//		builder.append('"');
//		
//		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object object)
	{
		if (object instanceof DesignPatternResources)
		{
			ResourceSet resourceSet = ((DesignPatternResources) object).getResourceSet();
			if (resourceSet != null)
			{
				return super.getImage(resourceSet);
			}
		}
		return super.getImage(object);
	}

//	private static StringBuilder append(StringBuilder builder, PatternSpecification specification) {
//		append(builder, specification.getPattern());
//		if (specification.getPattern().getSpecifications().size() > 1) {
//			builder.append('/');
//			builder.append(specification.getName());
//		}
//		return builder;
//	}
//
//	private static StringBuilder append(StringBuilder builder, DesignPattern pattern) {
//		return builder.append(pattern.getName());
//	}
//
//	private static StringBuilder append(StringBuilder builder, EClass eClass) {
//		return builder.append(eClass.getName().replaceAll(
//				String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
//						"(?<=[A-Za-z])(?=[^A-Za-z])"), " "));
//	}
}
