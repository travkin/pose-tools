package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EAttributeEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClass2EditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClassEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClassName2EditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClassNameEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EOperationEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackage2EditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageContentsEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageNameEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EcoreEditPartFactory;
import org.eclipse.emf.ecoretools.diagram.part.EcoreVisualIDRegistry;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEAttributeEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEClass2EditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEClassEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEClassName2EditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEClassNameEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEOperationEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEPackage2EditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEPackageContentsEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEPackageEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.PoseEPackageNameEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;

public class PoseEcoreEditPartFactory extends EcoreEditPartFactory {
	
	@Override
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			int visualId = EcoreVisualIDRegistry.getVisualID(view);
					
			// adapt the look of some elements visible in the ecore class diagram editor
			switch (visualId) {
			case EPackageEditPart.VISUAL_ID:
				return new PoseEPackageEditPart(view);
			case EPackageContentsEditPart.VISUAL_ID:
				return new PoseEPackageContentsEditPart(view);
			case EClassNameEditPart.VISUAL_ID:
				return new PoseEClassNameEditPart(view);
			case EClassName2EditPart.VISUAL_ID:
				return new PoseEClassName2EditPart(view);
			case EAttributeEditPart.VISUAL_ID:
				return new PoseEAttributeEditPart(view);
			case EOperationEditPart.VISUAL_ID:
				return new PoseEOperationEditPart(view);
			
			// adapt some edit policies
			case EClassEditPart.VISUAL_ID:
				return new PoseEClassEditPart(view);
			case EClass2EditPart.VISUAL_ID:
				return new PoseEClass2EditPart(view);
			case EPackage2EditPart.VISUAL_ID:
				return new PoseEPackage2EditPart(view);
			case EPackageNameEditPart.VISUAL_ID:
				return new PoseEPackageNameEditPart(view);
			
			// handle elements added to the ecore class diagram editor
			case AppliedPatternEditPart.VISUAL_ID:
				return new AppliedPatternEditPart(view);
			case RoleBindingEditPart.VISUAL_ID:
				return new RoleBindingEditPart(view);
				
			default:
				break;
			}
		}

		return super.createEditPart(context, model);
	}
}
