package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;
import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;

public final class PoseEcoreConstants
{
	// see plugin.xml
	public static final String ID_PATTERN = "de.upb.pose.mapping.AppliedPattern_"
			+ AppliedPatternEditPart.VISUAL_ID; //$NON-NLS-1$
	public static final String ID_ROLE_BINDING = "de.upb.pose.mapping.RoleBinding_"
			+ RoleBindingEditPart.VISUAL_ID; //$NON-NLS-1$

	public static final IElementType TYPE_PATTERN_APPLICATION = ElementTypeRegistry.getInstance().getType(ID_PATTERN); //$NON-NLS-1$
	public static final IElementType TYPE_ROLE_BINDING = ElementTypeRegistry.getInstance().getType(ID_ROLE_BINDING); //$NON-NLS-1$
}
