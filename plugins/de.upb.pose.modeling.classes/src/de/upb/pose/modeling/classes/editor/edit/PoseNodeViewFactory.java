package de.upb.pose.modeling.classes.editor.edit;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gmf.runtime.diagram.ui.view.factories.AbstractShapeViewFactory;
import org.eclipse.gmf.runtime.draw2d.ui.figures.FigureUtilities;
import org.eclipse.gmf.runtime.notation.LineType;
import org.eclipse.gmf.runtime.notation.LineTypeStyle;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.ShapeStyle;
import org.eclipse.gmf.runtime.notation.Style;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.upb.pose.modeling.classes.editor.edit.parts.AppliedPatternEditPart;

public class PoseNodeViewFactory extends AbstractShapeViewFactory {
	
	private static final Color THIS_FONT = ColorConstants.darkGray;
	private static final Color THIS_FORE = ColorConstants.gray;
	private static final Color THIS_BACK = ColorConstants.white;

	private static final int fontHeight = 10;

	@Override
	protected void initializeFromPreferences(View view) {
		super.initializeFromPreferences(view);
		ShapeStyle style = (ShapeStyle) view.getStyle(NotationPackage.Literals.SHAPE_STYLE);
		style.setFillColor(FigureUtilities.colorToInteger(THIS_BACK));
		style.setLineColor(FigureUtilities.colorToInteger(THIS_FORE));
		style.setFontColor(FigureUtilities.colorToInteger(THIS_FONT));
		style.setFontHeight(fontHeight);

		LineTypeStyle lts = (LineTypeStyle) view.getStyle(NotationPackage.Literals.LINE_TYPE_STYLE);
		lts.setLineType(LineType.DASH_LITERAL);
	}

	@Override
	protected List<Style> createStyles(View view) {
		List<Style> styles = new ArrayList<Style>();
		styles.add(NotationFactory.eINSTANCE.createShapeStyle());
		styles.add(NotationFactory.eINSTANCE.createLineTypeStyle());
		return styles;
	}

	@Override
	protected void decorateView(View containerView, View view, IAdaptable semanticAdapter, String semanticHint,
			int index, boolean persisted)
	{
		if (semanticHint == null)
		{
			System.out.println("Something seems to go wrong."); // TODO remove this code?
			semanticHint = String.valueOf(AppliedPatternEditPart.VISUAL_ID);
			view.setType(semanticHint);
		}
		super.decorateView(containerView, view, semanticAdapter, semanticHint, index, persisted);
	}
}
