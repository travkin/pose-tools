package de.upb.pose.modeling.classes.editor.edit.policies;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecoretools.diagram.edit.policies.PackageDiagramDragDropEditPolicy;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.commands.PatternApplicationCreateCommand;
import de.upb.pose.modeling.classes.editor.edit.PoseEcoreConstants;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.PatternSpecification;

public class CustomDiagramDragDropEditPolicy extends PackageDiagramDragDropEditPolicy {
	
	@Override
	public Command getDropObjectsCommand(DropObjectsRequest request) {
		// List<?> elements = request.getObjects();
		// if (elements.size() == 1) {
		// PatternSpecification specification = getSpecification(elements.get(0));
		//
		// if (specification != null) {
		//
		// return new ICommandProxy(new PatternDropCommand(getEditingDomain(), getPackage(), specification));
		// }
		// }

		// get specification
		List<?> elements = request.getObjects();
		if (elements.size() == 1) {
			Object element = elements.get(0);
			PatternSpecification specification = null;
			if (element instanceof PatternSpecification) {
				specification = (PatternSpecification) element;
			} else if (element instanceof DesignPattern) {
				List<PatternSpecification> specifications = ((DesignPattern) element).getSpecifications();
				if (specifications.size() == 1) {
					specification = specifications.get(0);
				}
			}

			if (specification != null) {
				EObject container = getHostObject();
				CreateElementRequest createRequest = new CreateElementRequest(container,
						PoseEcoreConstants.TYPE_PATTERN_APPLICATION);
				return new ICommandProxy(new PatternApplicationCreateCommand(createRequest, specification));
			}
		}

		return super.getDropObjectsCommand(request);
	}

	@Override
	protected Command getDropElementCommand(EObject element, DropObjectsRequest request) {
		// get specification
		PatternSpecification specification = null;
		if (element instanceof PatternSpecification) {
			specification = (PatternSpecification) element;
		} else if (element instanceof DesignPattern) {
			List<PatternSpecification> specifications = ((DesignPattern) element).getSpecifications();
			if (specifications.size() == 1) {
				specification = specifications.get(0);
			}
		}

		if (specification != null) {
			CreateElementRequest createRequest = new CreateElementRequest(PoseEcoreConstants.TYPE_PATTERN_APPLICATION);
			return new ICommandProxy(new PatternApplicationCreateCommand(createRequest, specification));
		}

		return super.getDropElementCommand(element, request);
	}

	private PatternSpecification getSpecification(Object element) {
		if (element instanceof PatternSpecification) {
			return (PatternSpecification) element;
		}

		if (element instanceof DesignPattern) {
			List<PatternSpecification> specifications = ((DesignPattern) element).getSpecifications();
			if (specifications.size() == 1) {
				return specifications.get(0);
			}
		}
		return null;
	}

	private TransactionalEditingDomain getEditingDomain() {
		if (getHost() instanceof IGraphicalEditPart) {
			return ((IGraphicalEditPart) getHost()).getEditingDomain();
		}
		return null;
	}

	private EPackage getPackage() {
		Object model = getHost().getModel();
		if (model instanceof View) {
			EObject element = ((View) model).getElement();

			if (element instanceof EPackage) {
				return (EPackage) element;
			}
		}
		return null;
	}
}
