package de.upb.pose.modeling.classes.editor;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecoretools.diagram.part.EcoreDiagramEditor;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramDropTargetListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

import de.upb.pose.modeling.classes.outline.PoseEcoreDiagramOutlinePage;

public class PoseEcoreEditor extends EcoreDiagramEditor {
	
	public static final String ID = "de.upb.pose.modeling.classes.editor";

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class type)
	{
		if (IContentOutlinePage.class.equals(type))
		{
			return new PoseEcoreDiagramOutlinePage(this);
		}
		return super.getAdapter(type);
	}

	@Override
	protected void initializeGraphicalViewer()
	{
		super.initializeGraphicalViewer();

		// add drop listener (for new pattern specification mappings)
		getDiagramGraphicalViewer().addDropTargetListener(
				new DiagramDropTargetListener(getGraphicalViewer(), LocalTransfer.getInstance())
				{
					@Override
					protected List<?> getObjectsBeingDropped()
					{
						TransferData data = getCurrentEvent().currentDataType;

						Object java = LocalTransfer.getInstance().nativeToJava(data);

						if (java instanceof IStructuredSelection)
						{
							return ((IStructuredSelection) java).toList();
						}

						return null;
					}
				});
	}

	
	@Override
	protected void performSaveAs(IProgressMonitor progressMonitor) {
		super.performSaveAs(progressMonitor);
	}

	@Override
	protected void performSave(boolean overwrite, IProgressMonitor progressMonitor) {
		super.performSave(overwrite, progressMonitor);
	}

	private EcoreDiagramRefresher diagramUpdater = null;
	
	@Override
	public void setInput(IEditorInput input) {
		super.setInput(input);
		
		this.diagramUpdater = new EcoreDiagramRefresher(this.getDiagram());
		this.getEditingDomain().getResourceSet().eAdapters().add(this.diagramUpdater);
	}

	private void removeDiagramUpdater() {
		if (this.diagramUpdater != null) {
			this.getEditingDomain().getResourceSet().eAdapters().remove(this.diagramUpdater);
		}
		this.diagramUpdater = null;
	}
	
	@Override
	protected void releaseInput() {
		removeDiagramUpdater();
		super.releaseInput();
	}
	
	@Override
	public void dispose() {
		removeDiagramUpdater();
		super.dispose();
	}
	
}
