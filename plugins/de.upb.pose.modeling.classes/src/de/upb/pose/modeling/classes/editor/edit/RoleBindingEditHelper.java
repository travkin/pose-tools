/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit;

import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelper;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;

@Deprecated
public class RoleBindingEditHelper extends AbstractEditHelper
{
	@Override
	protected ICommand getCreateCommand(CreateElementRequest req) {
		if (PoseEcoreConstants.TYPE_ROLE_BINDING.equals(req.getElementType())) {
			//return new AddPatternApplicationCommand(req);
			// TODO create a command
		}
		return super.getCreateCommand(req);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelper#getEditCommand(org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest)
	 */
	@Override
	public ICommand getEditCommand(IEditCommandRequest req)
	{
		return super.getEditCommand(req);
	}
}
