/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.policies;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageNameEditPart;
import org.eclipse.emf.ecoretools.diagram.edit.policies.OpenDiagramEditPolicy;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;

import de.upb.pose.modeling.classes.commands.PoseOpenDiagramCommand;

/**
 * @author Dietrich Travkin
 */
public class PoseOpenDiagramEditPolicy extends OpenDiagramEditPolicy {

	// copy of the original method with a replacement of the OpenDiagramCommand
	/**
	 * @see org.eclipse.emf.ecoretools.diagram.edit.policies.OpenDiagramEditPolicy#getOpenCommand(org.eclipse.gef.Request)
	 */
	@Override
	protected Command getOpenCommand(Request request) {
		EditPart targetEditPart = getTargetEditPart(request);
		if (targetEditPart instanceof EPackageNameEditPart && targetEditPart.getParent() != null) {
			targetEditPart = targetEditPart.getParent();
		}
		if (false == targetEditPart instanceof GraphicalEditPart) {
			return UnexecutableCommand.INSTANCE;
		}

		return new ICommandProxy(
				new PoseOpenDiagramCommand(
						((GraphicalEditPart) targetEditPart).resolveSemanticElement(),
						((EObject) targetEditPart.getModel()).eResource()));
	}
}
