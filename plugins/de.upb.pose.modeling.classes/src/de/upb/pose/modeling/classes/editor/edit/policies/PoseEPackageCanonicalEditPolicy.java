/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.policies;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecoretools.diagram.edit.policies.EPackageCanonicalEditPolicy;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.modeling.classes.editor.edit.parts.RoleBindingEditPart;

/**
 * @author Dietrich Travkin
 */
public class PoseEPackageCanonicalEditPolicy extends EPackageCanonicalEditPolicy {

	// TODO unfortunately, the class EPackageCanonicalEditPolicy is not customizable as desired / needed, find another way to create connections
	
//	@SuppressWarnings("rawtypes")
//	@Override
//	protected List getSemanticConnectionsList() {
//		View viewModel = (View) getHost().getModel();
//		EPackage realModel = (EPackage) viewModel.getElement();
//		PatternApplications applicationsRoot = PatternApplications2EcoreConnector.getLinkedPatternApplications(realModel);
//		if (applicationsRoot != null) {
//			List<RoleBinding> semanticConnections = new LinkedList<RoleBinding>();
//			for (AppliedPattern patternApplication: applicationsRoot.getApplications()) {
//				for (RoleBinding roleBinding: patternApplication.getRoleBindings()) {
//					for (EObject mappedEcoreElement: roleBinding.getModelElements()) {
//						if (mappedEcoreElement instanceof EObject) {
//							semanticConnections.add(roleBinding);
//							break;
//						}
//					}
//				}
//			}
//			if (!semanticConnections.isEmpty()) {
//				return semanticConnections;
//			}
//		}
//		
//		return Collections.EMPTY_LIST;
//	}
//	
//	protected EObject getSourceElement(EObject relationship) {
//		if (relationship instanceof RoleBinding) {
//			return ((RoleBinding) relationship).getAppliedPattern();
//		}
//		return null;
//	}
//
//	protected EObject getTargetElement(EObject relationship) {
//		if (relationship instanceof RoleBinding) {
//			RoleBinding roleBinding = (RoleBinding) relationship;
//			for (EObject mappedModelElement: roleBinding.getModelElements()) {
//				if (mappedModelElement instanceof EClass) {
//					return mappedModelElement;
//				}
//			}
//		}
//		return null;
//	}
//	
//	@Override
//	protected boolean canCreateConnection(EditPart sep, EditPart tep,
//			EObject connection) {
//		if (connection instanceof RoleBinding
//				&& sep != null && sep.isActive()
//				&& tep != null && tep.isActive()) {
//
//			View src = (View) sep.getAdapter(View.class);
//			View tgt = (View) tep.getAdapter(View.class);
//			if (src != null && tgt != null) {
//                return true;
//			}
//		}
//		return super.canCreateConnection(sep, tep, connection);
//	}
//	
//	protected String getDefaultFactoryHint() {
//		return String.valueOf(RoleBindingEditPart.VISUAL_ID);
//	}
	
}
