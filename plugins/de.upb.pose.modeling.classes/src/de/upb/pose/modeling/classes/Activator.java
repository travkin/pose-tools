package de.upb.pose.modeling.classes;

import de.upb.pose.core.ui.runtime.ActivatorImpl;
import de.upb.pose.core.ui.runtime.IActivator;

public final class Activator extends ActivatorImpl {
	
	public static final String PLUGIN_ID = "de.upb.pose.modeling.classes";
			
	private static IActivator instance;

	public static IActivator get() {
		return Activator.instance;
	}

	@Override
	protected void dispose() {
		Activator.instance = null;
	}

	@Override
	protected void initialize() {
		Activator.instance = this;

		for (String path : PoseEcoreImages.getPaths()) {
			addImage(path);
		}
	}
}
