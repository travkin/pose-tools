package de.upb.pose.modeling.classes.editor.edit.parts;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClass2EditPart;
import org.eclipse.emf.ecoretools.diagram.edit.parts.EClassEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.modeling.classes.editor.FilteredEContentListener;
import de.upb.pose.modeling.classes.editor.edit.policies.AppliedPatternItemSemanticEditPolicy;
import de.upb.pose.modeling.classes.editor.edit.policies.CustomGraphicalNodeEditPolicy;
import de.upb.pose.modeling.classes.editor.figures.AppliedPatternFigure;
import de.upb.pose.specification.util.SpecificationTextUtil;

public class AppliedPatternEditPart extends ShapeNodeEditPart {
	
	public static final int VISUAL_ID = 1999;

	public AppliedPatternEditPart(View view) {
		super(view);
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();

		refreshLineType();
		refreshLineWidth();

		getNodeFigure().setName(getPatternSpecificationName());
		
		refreshSourceConnections();
		refreshTargetConnections();
	}

	@Override
	protected void setForegroundColor(Color color) {
		if (getNodeFigure() != null) {
			getNodeFigure().setForegroundColor(color);
		}
	}

	@Override
	protected void setBackgroundColor(Color color) {
		if (getNodeFigure() != null) {
			getNodeFigure().setBackgroundColor(color);
		}
	}

	@Override
	protected void setLineWidth(int width) {
		if (getNodeFigure() != null) {
			getNodeFigure().setLineWidth(getMapMode().DPtoLP(width));
		}
	}

	@Override
	protected void setLineType(int style) {
		if (getNodeFigure() != null) {
			getNodeFigure().setLineStyle(style);
		}
	}

	@Override
	protected AppliedPatternFigure getNodeFigure() {
		return (AppliedPatternFigure) super.getNodeFigure();
	}

	@Override
	protected NodeFigure createNodeFigure() {
		String value = getPatternSpecificationName();
		return new AppliedPatternFigure(value, true, getMapMode().DPtoLP(40), getMapMode().DPtoLP(40));
	}

	private AppliedPattern getPatternApplication() {
		return (AppliedPattern) ((View) getModel()).getElement();
	}
	
	private String getPatternSpecificationName() {
		return SpecificationTextUtil.get(getPatternApplication().getPatternSpecification());
	}

	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();

		removeEditPolicy(EditPolicyRoles.CONNECTION_HANDLES_ROLE);

		removeEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE);
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new CustomGraphicalNodeEditPolicy());
		
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new AppliedPatternItemSemanticEditPolicy());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart#getModelSourceConnections()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected List getModelSourceConnections()
	{
		return super.getModelSourceConnections();
	}

	@Override
	protected void addSemanticListeners() {
		super.addSemanticListeners();
		
		// also listen to changes in child role bindings to update role binding connections
		AppliedPattern patternApplication = getPatternApplication();
		patternApplication.eAdapters().add(new FilteredEContentListener() {
			protected void roleBindingModelElementsFeatureChanged(Notification notification) {
				RoleBinding roleBinding = (RoleBinding) notification.getNotifier();
				addOrRemoveViewConnection(roleBinding, notification.getNewValue(), notification.getOldValue());
			}
		});
    }
	
	private void addOrRemoveViewConnection(RoleBinding roleBinding, Object newValue, Object oldValue) {
		if (newValue != null && oldValue == null && newValue instanceof EObject) {
			EditPart editPart = findEditPartFor((EObject) newValue);
			if (editPart != null) {
				AppliedPatternEditPart sourceEditPart = this;
				EditPart targetEditPart = editPart;
				EObject sourceModel = sourceEditPart.resolveSemanticElement();
				EObject targetModel = null;
				
				if (targetEditPart instanceof GraphicalEditPart) {
					GraphicalEditPart gTargetEditPart = (GraphicalEditPart) targetEditPart;
					targetModel = gTargetEditPart.resolveSemanticElement();
				} else {
					Object model = targetEditPart.getModel();
					if (model instanceof View) {
						model = ((View) model).getElement();
					}
					targetModel = (EObject) model;
				}
				
				
				// only add a view connection if there is no such connection
				boolean connectionExists = false;
				for (Object existingEdge: getDiagramView().getEdges()) {
					Edge edge = (Edge) existingEdge;
					if (edge.getSource() != null && edge.getTarget() != null) {
						EObject edgeSource = edge.getSource().getElement();
						EObject edgeTarget = edge.getTarget().getElement();
						
						if (sourceModel.equals(edgeSource) && targetModel.equals(edgeTarget)) {
							connectionExists = true;
							break;
						}
					}
				}
				
				if (!connectionExists) {
					Command createConnectionViewCommand = CustomGraphicalNodeEditPolicy
							.getCreateConnectionViewCommand(
									roleBinding, getDiagramPreferencesHint(),
									sourceEditPart, targetEditPart);
					executeCommand(createConnectionViewCommand);
				}
			}
		} else if (newValue == null && oldValue != null && oldValue instanceof EObject) {
			EditPart editPart = findEditPartFor(roleBinding);
			if (editPart != null && editPart instanceof RoleBindingEditPart) {
				GroupRequest deleteViewRequest = new GroupRequest(RequestConstants.REQ_DELETE);
				deleteViewRequest.setEditParts(editPart);
				
				Command deleteCommand = editPart.getCommand(deleteViewRequest);
				executeCommand(deleteCommand);
			}
		}
	}
	
	private EditPart findEditPartFor(EObject ecoreElement) {
		// TODO use {@link #findEditPart(EditPart,EObject)} instead?
		
		if (ecoreElement != null) {
			// try finding the target edit part
			EditPart editPart = null; //this.findEditPart(this, ecoreElement);
			EditPartViewer viewer = getViewer();
			for (Object obj: viewer.getEditPartRegistry().keySet()) {
				if (obj instanceof View && ((View) obj).getElement() != null) {
					EObject semanticElement = ((View) obj).getElement();
					if (ecoreElement.equals(semanticElement)) {
						
						if (semanticElement instanceof EClass) {
							String type = ((View) obj).getType();
							if (!String.valueOf(EClassEditPart.VISUAL_ID).equals(type)
									&& !String.valueOf(EClass2EditPart.VISUAL_ID).equals(type)) {
								continue; // wrong edit part
							}
						}
						
						editPart = (EditPart) viewer.getEditPartRegistry().get(obj);
						if (editPart != null) {
							return editPart;
						}
					}
				}
			}
		}
		
		return null;
	}

}
