package de.upb.pose.modeling.classes.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.LayoutService;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.LayoutType;
import org.eclipse.gmf.runtime.notation.Bounds;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.LayoutConstraint;
import org.eclipse.gmf.runtime.notation.Node;

import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;


public class EcoreDiagramRefresher extends FilteredEContentListener {
	
	private Diagram diagram;
	private PoseEcoreDiagramContentInitializer viewElementCreator = new PoseEcoreDiagramContentInitializer(true);
	
	public EcoreDiagramRefresher(Diagram diagram) {
		this.diagram = diagram;
	}
	
	/**
	 * @see de.upb.pose.modeling.classes.editor.FilteredEContentListener#contentChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void contentChanged(Notification notification) {
		if (this.diagram == null || notification.getNotifier() == null) {
			return;
		}
		
		if (notification.getNotifier() instanceof EObject) {
			EObject notifier = (EObject) notification.getNotifier();
			int event = notification.getEventType();
			
			if (!isResourceOfInterest(notifier.eResource())) {
				return;
			}
				
			if ( (event == Notification.ADD)
					|| (notifier instanceof RoleBinding
						&& MappingPackage.eINSTANCE.getRoleBinding_ModelElements().equals(notification.getFeature())) ) {
				// add missing view elements
				viewElementCreator.initDiagramContent(this.diagram);
				
				this.layout(this.diagram);
			}
		}
	}
	
	private void layout(Diagram diagram) {
		Point max = determineMaxXandMaxY(diagram);

		// Layout diagram content if necessary
		if (!diagram.getChildren().isEmpty()) {
			List<Node> nodes = new ArrayList<Node>();
			for (Object view : diagram.getChildren()) {
				if (view instanceof Node) {
					Node node = (Node) view;
					
					// add nodes without no layout constraints or with (x,y)=(0,0)
					if (node.getLayoutConstraint() == null) {
						nodes.add(node);
					} else {
						LayoutConstraint constraint = node.getLayoutConstraint();
						if (constraint instanceof Bounds) {
							Bounds bounds = (Bounds) constraint;
							if (bounds.getX() == 0 && bounds.getY() == 0) {
								nodes.add(node);
							}
						}
					}
				}
			}
			
			if (!nodes.isEmpty()) {
				LayoutService.getInstance().layoutNodes(nodes, true, LayoutType.DEFAULT);
			}
			
			// move all nodes
			for (Node node : nodes) {
				LayoutConstraint constraint = node.getLayoutConstraint();
				if (constraint instanceof Bounds) {
					Bounds bounds = (Bounds) constraint;
					if (max.x < max.y) {
						bounds.setX(bounds.getX() + max.x);
					} else {
						bounds.setY(bounds.getY() + max.y);
					}
				}
			}
		}
	}
	
	private Point determineMaxXandMaxY(Diagram diagram) {
		// search maximum
		int maxX = 0;
		int maxY = 0;
		for (Object child : diagram.getChildren()) {
			if (child instanceof Node) {
				LayoutConstraint constraint = ((Node) child).getLayoutConstraint();
				if (constraint instanceof Bounds) {
					Bounds bounds = (Bounds) constraint;
					
					int x = bounds.getX();
					if (bounds.getWidth() > 0) {
						x += bounds.getWidth();
					}
					
					int y = bounds.getY() + bounds.getHeight();
					if (bounds.getHeight() > 0) {
						y += bounds.getHeight();
					}
					
					if (x > maxX) {
						maxX = x;
					}
					
					if (y > maxY) {
						maxY = y;
					}
				}
			}
		}
		return new Point(maxX, maxY);
	}
	
}
