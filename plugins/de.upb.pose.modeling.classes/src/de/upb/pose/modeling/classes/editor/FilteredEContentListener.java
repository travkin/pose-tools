/**
 * 
 */
package de.upb.pose.modeling.classes.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;

import de.upb.pose.mapping.MappingConstants;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;

/**
 * @author Dietrich Travkin
 */
public abstract class FilteredEContentListener extends EContentAdapter {

	public static void addSemanticElementListenerToModelsResourceSet(GraphicalEditPart editPart, FilteredEContentListener listener) {
		EObject semanticModelElement = editPart.resolveSemanticElement();
		if (semanticModelElement != null
				&& semanticModelElement.eResource() != null
				&& semanticModelElement.eResource().getResourceSet() != null) {
			
			ResourceSet resourceSet = semanticModelElement.eResource().getResourceSet();
			resourceSet.eAdapters().add(listener);
		}
	}
	
	@Override
	public final void notifyChanged(Notification notification) {
		super.notifyChanged(notification);
		
		if ( //(notification.getEventType() == Notification.ADD || notification.getEventType() == Notification.REMOVE) &&
				notification.getNotifier() instanceof RoleBinding
				&& MappingPackage.eINSTANCE.getRoleBinding_ModelElements().equals(notification.getFeature())) {
			
			this.roleBindingModelElementsFeatureChanged(notification);
		}
		
		this.contentChanged(notification);
	}
	
	protected void roleBindingModelElementsFeatureChanged(Notification notification) {
		// do nothing per default, subclasses may override this behavior
	}
	
	protected void contentChanged(Notification notification) {
		// do nothing per default, subclasses may override this behavior
	}
	
	@Override
	protected void addAdapter(Notifier notifier) {
		if (!(notifier instanceof Resource)
				|| isResourceOfInterest((Resource) notifier)) {
			super.addAdapter(notifier);
		}
	}
	
	protected boolean isResourceOfInterest(Resource modifiedResource) {
		if (modifiedResource != null) {
			if (modifiedResource.getURI() != null) {
				String fileExtension = modifiedResource.getURI().fileExtension();
				if ("ecore".equals(fileExtension)
						|| MappingConstants.MODEL_FILE_EXTENSION.equals(fileExtension)) {
					return true;
				}
			}
		}
		return false;
	}
}
