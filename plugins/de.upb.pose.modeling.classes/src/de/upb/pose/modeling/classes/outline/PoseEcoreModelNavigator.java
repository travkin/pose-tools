package de.upb.pose.modeling.classes.outline;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecoretools.diagram.outline.EcoreModelNavigator;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.gmf.runtime.diagram.ui.parts.IDiagramWorkbenchPart;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.IPageSite;

public class PoseEcoreModelNavigator extends EcoreModelNavigator {
	
	private ComposedAdapterFactory af;

	public PoseEcoreModelNavigator(Composite parent, IDiagramWorkbenchPart part, IPageSite site) {
		super(parent, part, site);
	}
	
	@Override
	protected AdapterFactory getAdapterFactory() {
		if (af == null) {
			af = new ComposedAdapterFactory(Registry.INSTANCE);
		}
		return af;
	}

	@Override
	public void dispose() {
		if (af != null) {
			af.dispose();
		}
		super.dispose();
	}

	@Override
	protected void initProviders() {
		TreeViewer viewer = getTreeViewer();

		viewer.setContentProvider(new OutlineContentProvider(getAdapterFactory()));
		viewer.setLabelProvider(new OutlineLabelProvider(getAdapterFactory()));
	}
	
}
