/**
 * 
 */
package de.upb.pose.modeling.classes.editor.edit.parts;

import org.eclipse.emf.ecoretools.diagram.edit.parts.EPackageNameEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import de.upb.pose.modeling.classes.editor.edit.policies.PoseOpenDiagramEditPolicy;

/**
 * @author Dietrich Travkin
 */
public class PoseEPackageNameEditPart extends EPackageNameEditPart {

	public PoseEPackageNameEditPart(View view) {
		super(view);
	}
	
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		
		removeEditPolicy(EditPolicyRoles.OPEN_ROLE);
		installEditPolicy(EditPolicyRoles.OPEN_ROLE, new PoseOpenDiagramEditPolicy());
	}

}
