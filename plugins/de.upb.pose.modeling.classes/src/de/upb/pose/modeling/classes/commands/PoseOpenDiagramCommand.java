/**
 * 
 */
package de.upb.pose.modeling.classes.commands;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.diagram.edit.policies.OpenDiagramEditPolicy;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.LayoutService;
import org.eclipse.gmf.runtime.diagram.ui.services.layout.LayoutType;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.osgi.util.NLS;

import de.upb.pose.modeling.classes.editor.PoseEcoreDiagramContentInitializer;


/**
 * @author Dietrich Travkin
 */
public class PoseOpenDiagramCommand extends OpenDiagramEditPolicy.OpenDiagramCommand {

	private Resource diagramResource;
	private List<Diagram> allDiagrams;
	
	public PoseOpenDiagramCommand(EObject domainElement, Resource diagramResource) {
		super(domainElement, diagramResource);
		this.diagramResource = diagramResource;
		this.allDiagrams = findAllDiagrams(domainElement, diagramResource);
	}

	// copy of the original method with a replacement for the EcoreDiagramContentInitializer
	protected Diagram intializeNewDiagram(boolean initializeContent) throws ExecutionException {
		Diagram diagram = ViewService.createDiagram(getDiagramDomainElement(), getDiagramKind(), getPreferencesHint());
		if (diagram == null) {
			throw new ExecutionException(NLS.bind("Can't create diagram of '{0}' kind", getDiagramKind()));
		}
		setDefaultNameForDiagram(diagram);

		// multiDiagramFacet.getDiagramLinks().add(diagram);
		assert diagramResource != null;
		diagramResource.getContents().add(diagram);

		if (initializeContent) {
			// Initialize diagram content
			
			// ########## start of the actual change #################
			PoseEcoreDiagramContentInitializer initializer = new PoseEcoreDiagramContentInitializer(false);
			// ########### end of the actual change ##################
			
			initializer.setInitEPackageContent(false);
			initializer.initDiagramContent(diagram);

			// Layout diagram content if necessary
			if (false == diagram.getChildren().isEmpty()) {
				List<Node> nodes = new ArrayList<Node>();
				for (Object view : diagram.getChildren()) {
					if (view instanceof Node) {
						nodes.add((Node) view);
					}
				}
				LayoutService.getInstance().layoutNodes(nodes, true, LayoutType.DEFAULT);
			}
		}
		return diagram;
	}
	
	// copy of the original method, since it is private
	private void setDefaultNameForDiagram(Diagram elementToConfigure) {
		EPackage pseudoContainer = (EPackage) elementToConfigure.getElement();
		String baseString = pseudoContainer.getName() + "_Diagram"; //$NON-NLS-1$
		int count = 0;
		for (Diagram diagram : allDiagrams) {
			if (diagram.getName().equals(baseString + count)) {
				count++;
			}
		}
		elementToConfigure.setName(baseString + count);
	}
	
	// imitation of the original behavior
	private List<Diagram> findAllDiagrams(EObject domainElement, Resource diagramResource) {
		List<Diagram> allDiagrams = new ArrayList<Diagram>();
		if (domainElement instanceof EPackage) {
			for (EObject currentDiag : diagramResource.getContents()) {
				if (currentDiag instanceof Diagram && domainElement.equals(((Diagram) currentDiag).getElement())) {
					allDiagrams.add((Diagram) currentDiag);
				}
			}
		}
		return allDiagrams;
	}
	
}
