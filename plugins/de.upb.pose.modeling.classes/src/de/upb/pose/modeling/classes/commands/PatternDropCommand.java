package de.upb.pose.modeling.classes.commands;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingConstants;
import de.upb.pose.mapping.MappingFactory;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.patternapplication.PatternApplicationCreator;
import de.upb.pose.specification.PatternSpecification;

public class PatternDropCommand extends AbstractTransactionalCommand {
	
	private EPackage ePackage;
	private PatternSpecification specification;

	// TODO check this command and adapt it to the implementation of {@link PatternApplicationCreateCommand}
	
	public PatternDropCommand(TransactionalEditingDomain domain, EPackage ePackage, PatternSpecification specification) {
		super(domain, "Create DesignPattern", null);

		this.ePackage = ePackage;
		this.specification = specification;
	}

	@Override
	public boolean canExecute() {
		return ePackage != null && specification != null;
	}

	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		// get application container
		PatternApplications boContainer = getApplications();

		AppliedPattern pattern = PatternApplicationCreator.createPatternApplicationFor(this.specification, boContainer);

		return CommandResult.newOKCommandResult(pattern);
	}

	private PatternApplications getApplications() {
		URI uri = ePackage.eResource().getURI().trimFileExtension().appendFileExtension(MappingConstants.MODEL_FILE_EXTENSION);

		PatternApplications applications = null;
		Resource resource = getResource(uri);

		if (resource != null) {
			// get it
			applications = (PatternApplications) resource.getContents().get(0);
		} else {
			// create
			applications = MappingFactory.eINSTANCE.createPatternApplications();
			resource = createResource(uri, applications);
		}

		return applications;
	}

	private Resource getResource(URI uri) {
		ResourceSet resourceSet = ePackage.eResource().getResourceSet();
		try {
			return resourceSet.getResource(uri, true);
		} catch (Exception e) {
			return null;
		}
	}

	private Resource createResource(URI uri, EObject content) {
		ResourceSet resourceSet = ePackage.eResource().getResourceSet();
		Resource resource = resourceSet.createResource(uri);

		resource.getContents().add(content);

		try {
			resource.save(Collections.emptyMap());
			return resource;
		} catch (IOException e) {
			return null;
		}
	}
}
