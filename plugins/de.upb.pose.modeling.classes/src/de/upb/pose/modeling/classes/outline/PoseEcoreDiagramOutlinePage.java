package de.upb.pose.modeling.classes.outline;

import org.eclipse.emf.ecoretools.diagram.outline.EcoreDiagramOutlinePage;
import org.eclipse.emf.ecoretools.diagram.ui.outline.AbstractModelNavigator;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.IPageSite;

public class PoseEcoreDiagramOutlinePage extends EcoreDiagramOutlinePage {
	
	public PoseEcoreDiagramOutlinePage(DiagramEditor editor) {
		super(editor);
	}

	@Override
	protected AbstractModelNavigator createNavigator(Composite parent, IPageSite pageSite) {
		return new PoseEcoreModelNavigator(parent, getEditor(), pageSite);
	}
}
