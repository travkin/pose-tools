package de.upb.pose.mapping.ui.editor.features.custom;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.marker.MarkerUtil;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.service.IBatchValidator;
import org.eclipse.emf.validation.service.ModelValidationService;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.ui.PlatformUI;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.MappingUIPlugin;

public class ValidateMappingFeature extends AbstractCustomFeature {
	public ValidateMappingFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void execute(ICustomContext context) {
		final AppliedPattern appliedPattern = (AppliedPattern) getDiagram().getLink().getBusinessObjects().get(1);

		// create a validator.  We know it is a batch validator because we are
		// asking for batch mode
		final IBatchValidator validator = (IBatchValidator) ModelValidationService.getInstance().newValidator(EvaluationMode.BATCH);

		validator.setIncludeLiveConstraints(true);

		try {
			PlatformUI.getWorkbench().getProgressService().run(true, true, new IRunnableWithProgress() {
			    public void run(IProgressMonitor monitor) {
			        IStatus results = validator.validate(appliedPattern, monitor);

			        if (!results.isOK()) {
			        	try {
							MarkerUtil.createMarkers(results);
							MarkerUtil.updateMarkers(results);
						} catch (CoreException e) {
							e.printStackTrace();
							MappingUIPlugin.getDefault().getLog().log(e.getStatus());
						}
			        }
			    }});
		} catch (InvocationTargetException e1) {
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public String getImageId() {
		return MappingImageProvider.VALIDATE_MAPPING;
	}

	@Override
	public String getName() {
		return "Validate Mapping";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		return true;
	}
}
