package de.upb.pose.mapping.ui.editor.features.custom;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.mapping.SetFragmentPEMapping;
import de.upb.pose.specification.SetFragment;

public class NavigateSetInstancesFeature extends AbstractCustomFeature {
	private boolean isNext;

	public NavigateSetInstancesFeature(IFeatureProvider fp, boolean isNext) {
		super(fp);

		this.isNext = isNext;
	}

	@Override
	public void execute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];

		int value;
		int current = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstanceIndex(pe);
		int total = SetFragmentPEMapping.get().getNumberOfSetFragmentInstances(pe);

		if (isNext) {
			// next
			if (current == total) {
				// go to start
				value = 1;
			} else {
				// go to next
				value = current + 1;
			}
		} else {
			// previous
			if (current == 1) {
				// go to end
				value = total;
			} else {
				// go to previous
				value = current - 1;
			}
		}

		SetFragmentPEMapping.get().setCurrentlyVisibleSetFragmentInstanceIndex(pe, value);

		updatePictogramElement(pe);
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		Object bo = getBusinessObjectForPictogramElement(pe);

		// ensure set fragment
		if (bo instanceof SetFragment) {
			// only allow navigation when not 'empty'
			if (SetFragmentPEMapping.get().getNumberOfSetFragmentInstances(pe) > 1) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String getImageId() {
		if (isNext) {
			return MappingImageProvider.NEXT;
		} else {
			return MappingImageProvider.PREVIOUS;
		}
	}

	@Override
	public String getName() {
		if (isNext) {
			return "Show Next Set Instance";
		} else {
			return "Show Previous Set Instance";
		}
	}
}
