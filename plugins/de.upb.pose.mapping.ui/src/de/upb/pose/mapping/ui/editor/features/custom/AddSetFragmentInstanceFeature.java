package de.upb.pose.mapping.ui.editor.features.custom;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.services.GraphitiUi;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.mapping.SetFragmentPEMapping;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelCreator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;

public class AddSetFragmentInstanceFeature extends AbstractCustomFeature {
	
	public AddSetFragmentInstanceFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void execute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		
		int currentInstanceIndex = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstanceIndex(pe);
		
		SetFragmentInstance currentSetFragmentInstance = SetFragmentPEMapping.get().getSetFragmentInstanceWithIndex(pe, currentInstanceIndex);

		SetFragmentInstance newSetFragmentInstance = ApplicationModelCreator.addSetFragmentInstance(currentSetFragmentInstance);
		pe.getLink().getBusinessObjects().add(newSetFragmentInstance);

		// collect all (new) role bindings for all pattern roles in the new set fragment instance
		List<RoleBinding> newRoleBindings = SetFragmentBindingCreator.collectAllRoleBindingInSetFragmentInstance(newSetFragmentInstance);
		
		// link all new role bindings to their pictogram elements
		for (RoleBinding roleBinding: newRoleBindings) {
			DesignElement patternRole = roleBinding.getRole();
			List<PictogramElement> pes = GraphitiUi.getLinkService().getPictogramElements(getDiagram(), patternRole);
			for (PictogramElement pictogramElement: pes) {
				
				boolean linkedToARoleBinding = false;
				for (EObject bo: pictogramElement.getLink().getBusinessObjects()) {
					if (bo instanceof RoleBinding && !bo.equals(roleBinding)) {
						linkedToARoleBinding = true;
						break;
					}
				}
				
				if (linkedToARoleBinding) {
					pictogramElement.getLink().getBusinessObjects().add(roleBinding);
				}
			}
		}
		
		
		// change index to the new set element
		SetFragmentPEMapping.get().setCurrentlyVisibleSetFragmentInstanceIndex(pe, currentInstanceIndex + 1);

		updatePictogramElement(pe);
	}

	@Override
	public String getImageId() {
		return MappingImageProvider.ADD;
	}

	@Override
	public String getName() {
		return "Add New Set Instance";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof SetFragment;
	}
}
