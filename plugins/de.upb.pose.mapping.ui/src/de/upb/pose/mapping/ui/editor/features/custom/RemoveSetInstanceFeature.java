package de.upb.pose.mapping.ui.editor.features.custom;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.mapping.SetFragmentPEMapping;
import de.upb.pose.specification.SetFragment;

public class RemoveSetInstanceFeature extends AbstractCustomFeature {
	public RemoveSetInstanceFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void execute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];

		int current = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstanceIndex(pe);
		int total = SetFragmentPEMapping.get().getNumberOfSetFragmentInstances(pe);

		// remove the set element
		// TODO consider that sets could contain other sets, too
		SetFragmentPEMapping.get().removeSetFragmentInstanceWithIndex(pe, current);

		// change index when necessary
		if (current == total) {
			SetFragmentPEMapping.get().setCurrentlyVisibleSetFragmentInstanceIndex(pe, total - 1);
		}

		updatePictogramElement(pe);
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof SetFragment) {
			return SetFragmentPEMapping.get().getNumberOfSetFragmentInstances(pe) > 1;
		}

		return false;
	}

	@Override
	public String getImageId() {
		return MappingImageProvider.REMOVE;
	}

	@Override
	public String getName() {
		return "Remove Current Set Instance";
	}
}
