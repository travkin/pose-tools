/**
 * 
 */
package de.upb.pose.mapping.ui.mapping;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.util.GraphitiUtil;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;

/**
 * @author Dietrich Travkin
 */
public class RolePEMapping {

	private static RolePEMapping instance = null;
	
	private RolePEMapping() {}
	
	public static RolePEMapping get() {
		if (instance == null) {
			instance = new RolePEMapping();
		}
		return instance;
	}
	
	/**
	 * Returns the role binding for the mapping node represented by the given pictrogram element.
	 * Potentially surrounding set fragments are taken into account and the role binding of the currently
	 * visible set element binding is returned.
	 * 
	 * @param pe the pictogram element to find the role binding for
	 * @return The role binding for the mapping node represented by the given pictrogram element
	 */
	public RoleBinding getRoleBinding(PictogramElement pe)
	{
		RoleBinding roleBinding = null;
		EObject bo = null;
		if (pe.getLink() != null && !pe.getLink().getBusinessObjects().isEmpty())
		{
			bo = pe.getLink().getBusinessObjects().get(0);
		}
		if (bo != null && bo instanceof DesignElement)
		{
			DesignElement patternElement = (DesignElement) bo;
			if (!patternElement.getContainingSets().isEmpty())
			{
				// determine one (of possibly two) visible set element binding(s)
				SetFragment setFragment = patternElement.getContainingSets().get(0);
				Diagram diagram = Graphiti.getPeService().getDiagramForPictogramElement(pe);
				List<PictogramElement> setFragmentPEs = Graphiti.getLinkService().getPictogramElements(diagram, setFragment);
				if (setFragmentPEs.isEmpty()) {
					return roleBinding;
				}
				
				PictogramElement setFragmentPE = Graphiti.getLinkService().getPictogramElements(diagram, setFragment).get(0);
				SetFragmentInstance setElementBinding = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstance(setFragmentPE);
				List<RoleBinding> roleBindings = SetFragmentBindingCreator.findAllRoleBindingsFor(patternElement, setElementBinding);
				if (roleBindings != null && roleBindings.size() == 1)
				{
					roleBinding = roleBindings.get(0);
				}
				else // there must be another set fragment
				{
					// find the currently visible role binding
					assert patternElement.getContainingSets().size() == 2;
					
					SetFragment otherSetFragment = patternElement.getContainingSets().get(1);
					PictogramElement otherSetFragmentPE = Graphiti.getLinkService().getPictogramElements(diagram, otherSetFragment).get(0);
					SetFragmentInstance otherSetElementBinding = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstance(otherSetFragmentPE);
					for (RoleBinding currentRoleBinding: roleBindings)
					{
						if (currentRoleBinding.getContainingSetFragmentInstances().contains(otherSetElementBinding))
						{
							roleBinding = currentRoleBinding;
							break;
						}
					}
				}
			}
			else
			{
				roleBinding = GraphitiUtil.getFirstBusinessObjectOfType(pe, RoleBinding.class);
			}
		}
		return roleBinding;
	}
	
}
