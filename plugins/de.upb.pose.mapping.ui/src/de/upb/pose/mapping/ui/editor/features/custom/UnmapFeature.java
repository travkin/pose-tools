package de.upb.pose.mapping.ui.editor.features.custom;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelElementCreator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.subsystems.Subsystem;

public class UnmapFeature extends AbstractMapUnmapElementFeature {
	
	public UnmapFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void execute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);

		EObject[] selectedElements = this.getSelectedEcoreModelElements(context);
		
		// remove the selected model elements from the mapping
		if (selectedElements != null && selectedElements.length > 0) {
			for (EObject selectedElement: selectedElements) {
				roleBinding.getModelElements().remove(selectedElement);
			}
		} else {
			roleBinding.getModelElements().clear(); // remove all
		}
		
		// update the role binding name
		roleBinding.setName(MappingNameCreator.getNameFor(roleBinding));
		
		// update the application model element name
		String mappedElementName = ApplicationModelElementCreator.getApplicationModelElementName(roleBinding);
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement != null) {
			applicationModelElement.setName(mappedElementName);
		}

		updatePictogramElement(pe);
	}

	@Override
	public String getImageId() {
		return MappingImageProvider.UNMAP;
	}

	@Override
	public String getName() {
		return "Remove Binding";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		DesignElement selectedPatternRole = this.getSelectedPatternRole(context);
		EObject[] selectedElements = this.getSelectedEcoreModelElements(context);
		if (selectedPatternRole != null) {
			PictogramElement pe = context.getPictogramElements()[0];
			RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);

			if (!(selectedPatternRole instanceof Subsystem)) {
				return roleBinding != null
						&& roleBinding.getModelElements().size() > 0;
			}
			else if (roleBinding != null
					&& selectedElements != null
					&& selectedElements.length > 0) {
				boolean selectedElementsAreMapped = true;
				for (EObject selectedElement: selectedElements) {
					selectedElementsAreMapped = selectedElementsAreMapped
							&& roleBinding.getModelElements().contains(selectedElement);
				}
				return selectedElementsAreMapped;
			}
			
		}
		return false;
	}
}
