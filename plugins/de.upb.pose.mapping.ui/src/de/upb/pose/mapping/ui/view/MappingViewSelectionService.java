/**
 * 
 */
package de.upb.pose.mapping.ui.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PlatformUI;


/**
 * @author Dietrich Travkin
 */
public class MappingViewSelectionService {

	private static final EmptySelection emptySelection = new EmptySelection();
	
	static class EmptySelection implements ISelection {
		public boolean isEmpty() {
			return true;
		}
	}
	
	private static List<IEditorPart> getOpenEcoreEditors() {
		ArrayList<IEditorPart> editors = new ArrayList<IEditorPart>();
		IEditorReference[] editorReferences = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
		for (IEditorReference editorReference: editorReferences) {
			if (MappingView.isValidEcoreModelEditorId(editorReference.getId())) {
				IEditorPart editor = editorReference.getEditor(false);
				if (editor != null && editor.getSite() != null) {
					editors.add(editor);
				}
			}
		}
		return editors;
	}
	
	public static ISelection getCurrentSelection(IEditorPart editor) {
		if (editor != null && editor.getSite() != null) {
			if (editor.getSite().getSelectionProvider() != null) {
				ISelectionProvider selectionProvider = editor.getSite().getSelectionProvider();
				return selectionProvider.getSelection();
			}
		}
		
		return emptySelection;
	}
	
	public static EObject adaptSelectionToEObject(ISelection selection) {
		if (selection != null && !selection.isEmpty() && selection instanceof IStructuredSelection) {
			IStructuredSelection theSelection = (IStructuredSelection) selection;
			if (theSelection.size() == 1) {
				Object seletedObject = theSelection.getFirstElement();
				return adaptObjectToEObject(seletedObject);
			}
		}
		return null;
	}
	
	public static EObject adaptObjectToEObject(Object element) {
		if (element instanceof EditPart) {
			return adaptObjectToEObject(((EditPart) element).getModel());
		}

		if (element instanceof View) {
			return adaptObjectToEObject(((View) element).getElement());
		}

		if (element instanceof EObject) {
			return (EObject) element;
		}

		return null;
	}
	
}
