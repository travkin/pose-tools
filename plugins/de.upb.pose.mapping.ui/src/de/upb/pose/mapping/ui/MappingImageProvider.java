package de.upb.pose.mapping.ui;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

public class MappingImageProvider extends AbstractImageProvider {
	
	public static final String MAP = "icons/map.png"; //$NON-NLS-1$
	public static final String UNMAP = "icons/unmap.png"; //$NON-NLS-1$

	public static final String PREVIOUS = "icons/previous.png"; //$NON-NLS-1$
	public static final String NEXT = "icons/next.png"; //$NON-NLS-1$
	public static final String REMOVE = "icons/remove.png"; //$NON-NLS-1$
	public static final String ADD = "icons/add.png"; //$NON-NLS-1$

	public static final String VALIDATE_MAPPING = "icons/validate.png"; //$NON-NLS-1$
	public static final String APPLY_PATTERN = "icons/apply.png"; //$NON-NLS-1$

	@Override
	protected void addAvailableImages() {
		add(MAP);
		add(UNMAP);

		add(VALIDATE_MAPPING);
		add(APPLY_PATTERN);

		add(PREVIOUS);
		add(NEXT);
		add(REMOVE);
		add(ADD);
	}

	private void add(String path) {
		addImageFilePath(path, path);
	}
	
}
