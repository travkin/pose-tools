/**
 * 
 */
package de.upb.pose.mapping.ui;

import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;

/**
 * @author Dietrich Travkin
 */
public class MappingColors {

	private static final IColorConstant COLOR_TYPE_UNMAPPED_GEN = new ColorConstant(200, 200, 255);
	private static final IColorConstant COLOR_TYPE_UNMAPPED_NOT_GEN = new ColorConstant(130, 150, 207);
	private static final IColorConstant COLOR_TYPE_MAPPED_GEN = new ColorConstant(255, 255, 230);
	private static final IColorConstant COLOR_TYPE_MAPPED_NOT_GEN = new ColorConstant(228, 224, 165);
	
	public static IColorConstant getMappingColor(PictogramElement element) {
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(element);
		boolean isMapped = !roleBinding.getModelElements().isEmpty();
		boolean canBeGenerated = roleBinding.getRole().isCanBeGenerated();
		
		IColorConstant color;
		
		if (isMapped) {
			if (canBeGenerated) {
				color = COLOR_TYPE_MAPPED_GEN;
			} else {
				color = COLOR_TYPE_MAPPED_NOT_GEN;
			}
		} else {
			if (canBeGenerated) {
				color = COLOR_TYPE_UNMAPPED_GEN;
			} else {
				color = COLOR_TYPE_UNMAPPED_NOT_GEN;
			}
		}
		
		return color; 
	}
}
