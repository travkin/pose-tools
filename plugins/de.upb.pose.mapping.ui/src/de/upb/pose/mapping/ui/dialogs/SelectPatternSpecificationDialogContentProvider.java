package de.upb.pose.mapping.ui.dialogs;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.ui.providers.SpecificationContentProvider;

public class SelectPatternSpecificationDialogContentProvider extends SpecificationContentProvider
{
	@Override
	public Object[] getElements(Object element)
	{
		if (element instanceof ResourceSet)
		{
			Collection<Object> elements = new ArrayList<Object>();
			for (Resource resource : ((ResourceSet) element).getResources())
			{
				if (hasChildren(resource))
				{
					elements.add(resource);
				}
			}
			return elements.toArray(new Object[elements.size()]);
		}
		return new Object[0];
	}

	@Override
	public Object[] getChildren(Object element)
	{
		if (element instanceof Resource || element instanceof EObject)
		{
			Collection<Object> children = new ArrayList<Object>();
			for (Object child : super.getChildren(element))
			{
				if (isValid(child) || hasChildren(child))
				{
					children.add(child);
				}
			}
			return children.toArray(new Object[children.size()]);
		}

		return new Object[0];
	}

	private boolean isValid(Object element) {
		return element instanceof PatternSpecification;
	}
}
