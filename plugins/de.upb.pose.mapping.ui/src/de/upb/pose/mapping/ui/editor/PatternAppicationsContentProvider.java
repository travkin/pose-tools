/**
 * 
 */
package de.upb.pose.mapping.ui.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;

import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;


/**
 * @author Dietrich Travkin
 */
public class PatternAppicationsContentProvider extends AdapterFactoryContentProvider {

	public PatternAppicationsContentProvider(AdapterFactory af) {
		super(af);
	}

	/**
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object object) {
		return getChildren(object).length > 0;
	}
	
	/**
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object element) {
		if (element instanceof Resource) {
			return ((Resource) element).getContents().toArray();
		}
		
		return getChildren(element);
	}
	
	/**
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object element) {
		// PatternApplications (root) --> all pattern applications
		if (element instanceof PatternApplications) {
			PatternApplications patternApplications = (PatternApplications) element;
			return patternApplications.getApplications().toArray();
		}
		
		// a pattern application --> set bindings, (non-set) role bindings and application model
		if (element instanceof AppliedPattern) {
			AppliedPattern patternApplication = (AppliedPattern) element;
			List<Object> children = new ArrayList<Object>();

			// add set bindings
			children.addAll(patternApplication.getSetFragmentBindings());

			// add role bindings which are not in a set binding
			for (RoleBinding roleBinding : patternApplication.getRoleBindings()) {
				if (roleBinding.getContainingSetFragmentInstances().isEmpty()) {
					children.add(roleBinding);
				}
			}

			// add application model
			ApplicationModel model = patternApplication.getApplicationModel();
			if (model != null) {
				children.add(model);
			}

			return children.toArray();
		}
		
		// a set binding --> contained set element bindings
		if (element instanceof SetFragmentBinding) {
			return ((SetFragmentBinding) element).getSetFragmentInstances().toArray();
		}

		// a set element binding --> contained role bindings
		if (element instanceof SetFragmentInstance) {
			return ((SetFragmentInstance) element).getContainedRoleBindings().toArray();
		}

		// a role binding --> pattern role & role playing model elements
		if (element instanceof RoleBinding) {
			RoleBinding roleBinding = (RoleBinding) element;
			List<Object> children = new ArrayList<Object>();

			children.add(roleBinding.getRole());
			children.addAll(roleBinding.getModelElements());

			return children.toArray();
		}
		
		return super.getChildren(element);
	}
	
	/**
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(Object object) {
		if (object instanceof PatternApplications) {
			return ((PatternApplications) object).eResource();
		}
		
		if (object instanceof AppliedPattern) {
			return ((AppliedPattern) object).getParent();
		}
		
		if (object instanceof SetFragmentBinding) {
			return ((SetFragmentBinding) object).getAppliedPattern();
		}
		
		if (object instanceof SetFragmentInstance) {
			return ((SetFragmentInstance) object).getParentSetFragmentBinding();
		}
		
		if (object instanceof ApplicationModel) {
			return ((ApplicationModel) object).getAppliedPattern();
		}
		
		if (object instanceof RoleBinding) {
			RoleBinding binding = (RoleBinding) object;
			if (binding.getContainingSetFragmentInstances().size() == 1) {
				return binding.getContainingSetFragmentInstances().get(0);
			} else if (binding.getContainingSetFragmentInstances().isEmpty()) {
				return binding.getAppliedPattern();
			} else {
				// there are 2 parent set element bindings, this is ambiguous!
				return null; // TODO check if this is a problem
			}
		}
		
		// TODO compute the parent of DesignElements and mapped model elements, too?
		
		return super.getParent(object);
	}
}
