package de.upb.pose.mapping.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.upb.pose.mapping.ui.view.MappingView;

public class PosePerspective implements IPerspectiveFactory {

	private static final String BOTTOM = "bottom"; //$NON-NLS-1$

	@Override
	public void createInitialLayout(IPageLayout layout) {
		addFastViews(layout);
		addViewShortcuts(layout);
		addPerspectiveShortcuts(layout);

		layout.addView(IPageLayout.ID_PROJECT_EXPLORER, IPageLayout.LEFT, 0.3f, IPageLayout.ID_EDITOR_AREA);

		IFolderLayout bottomFolder = layout.createFolder(BOTTOM, IPageLayout.BOTTOM, 0.67f, IPageLayout.ID_EDITOR_AREA);
		bottomFolder.addView(IPageLayout.ID_PROP_SHEET);
		bottomFolder.addView(IPageLayout.ID_PROBLEM_VIEW);

		layout.addView(MappingView.ID, IPageLayout.RIGHT, 0.6f, IPageLayout.ID_EDITOR_AREA);
		layout.addView(IPageLayout.ID_OUTLINE, IPageLayout.BOTTOM, 0.7f, IPageLayout.ID_PROJECT_EXPLORER);
	}

	/**
	 * Add fast views to the perspective.
	 */
	private void addFastViews(IPageLayout layout) {
	}

	/**
	 * Add view shortcuts to the perspective.
	 */
	private void addViewShortcuts(IPageLayout layout) {
	}

	/**
	 * Add perspective shortcuts to the perspective.
	 */
	private void addPerspectiveShortcuts(IPageLayout layout) {
		
	}
}
