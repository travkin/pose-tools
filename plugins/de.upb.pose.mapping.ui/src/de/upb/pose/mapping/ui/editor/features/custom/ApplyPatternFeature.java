package de.upb.pose.mapping.ui.editor.features.custom;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.editor.RoleMappingEditor;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;

public class ApplyPatternFeature extends AbstractCustomFeature {
	
	public ApplyPatternFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void execute(ICustomContext context) {	
		AppliedPattern pattern = (AppliedPattern) getDiagram().getLink().getBusinessObjects().get(1);
		Assert.isNotNull(pattern);

		Resource resource = findEcoreResource();
		Assert.isNotNull(resource);

		// apply the pattern by translating the application model into an Ecore model and completing the Ecore model
		ApplicationModel2EcoreAndSDsTranslator.performPatternApplication(pattern, getDiagramEditor().getResourceSet());
	}
	
	private Resource findEcoreResource()
	{
		for (Resource resource: getDiagramEditor().getResourceSet().getResources())
		{
			if (resource.getURI().lastSegment().endsWith(".ecore"))
			{
				return resource;
			}
		}
		return null;
	}

	@Override
	protected RoleMappingEditor getDiagramEditor() {
		return (RoleMappingEditor) super.getDiagramEditor();
	}

	@Override
	public String getImageId() {
		return MappingImageProvider.APPLY_PATTERN;
	}

	@Override
	public String getName() {
		return "Apply DesignPattern";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		// TODO check if the application is really possible
		return true;
	}
}
