/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 *
 */
public class DelegatingLayoutFeature implements ILayoutFeature
{
	private ILayoutFeature wrappedFeature;
	private IFeatureProvider featureProvider;
	
	public DelegatingLayoutFeature(ILayoutFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		this.wrappedFeature = featureToBeWrapped;
		this.featureProvider = fp;
	}
	
	protected ILayoutFeature getWrappedFeature()
	{
		return wrappedFeature;
	}
	
	protected Object getBusinessObjectForPictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().getBusinessObjectForPictogramElement(pe);
	}
	
	protected Color manageColor(IColorConstant colorConstant)
	{
		return Graphiti.getGaService().manageColor(getDiagram(), colorConstant);
	}
	
	protected Diagram getDiagram()
	{
		return getFeatureProvider().getDiagramTypeProvider().getDiagram();
	}
	
	protected IReason layoutPictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().layoutIfPossible(new LayoutContext(pe));
	}
	
	protected IReason updatePictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().updateIfPossible(new UpdateContext(pe));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context)
	{
		return wrappedFeature.canLayout(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.ILayout#layout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layout(ILayoutContext context)
	{
		return wrappedFeature.layout(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#isAvailable(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean isAvailable(IContext context)
	{
		return wrappedFeature.isAvailable(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#canExecute(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean canExecute(IContext context)
	{
		boolean ret = false;
		if (context instanceof ILayoutContext)
		{
			ILayoutContext layoutSemanticsContext = (ILayoutContext) context;
			ret = canLayout(layoutSemanticsContext);
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#execute(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public void execute(IContext context)
	{
		if (context instanceof ILayoutContext)
		{
			layout((ILayoutContext) context);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#canUndo(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean canUndo(IContext context)
	{
		return wrappedFeature.canUndo(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#hasDoneChanges()
	 */
	@Override
	public boolean hasDoneChanges()
	{
		return wrappedFeature.hasDoneChanges();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.IName#getName()
	 */
	@Override
	public String getName()
	{
		return wrappedFeature.getName();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.IDescription#getDescription()
	 */
	@Override
	public String getDescription()
	{
		return wrappedFeature.getDescription();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeatureProviderHolder#getFeatureProvider()
	 */
	@Override
	public IFeatureProvider getFeatureProvider()
	{
		return this.featureProvider;
	}
}
