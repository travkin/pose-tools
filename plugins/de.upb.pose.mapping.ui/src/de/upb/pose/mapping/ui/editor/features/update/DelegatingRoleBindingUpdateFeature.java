/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.update;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.MappingColors;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.mapping.ui.editor.features.layout.ClassMappingLabelConstants;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.features.layout.LabelConstants;
import de.upb.pose.specification.ui.editor.features.update.IShapeUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.UpdateFeatureConstants;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

/**
 * @author Dietrich Travkin
 */
public class DelegatingRoleBindingUpdateFeature extends DelegatingShapeUpdateFeature
{	
	public DelegatingRoleBindingUpdateFeature(IShapeUpdateFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	protected Text getLabel(IUpdateContext context)
	{
		return getNameLabel(context.getPictogramElement());
	}
	
	protected Text getBindingLabel(IUpdateContext context)
	{
		return getBindingLabel(context.getPictogramElement());
	}
	
	public Text getNameLabel(PictogramElement pe) {
		return getLabel(LabelConstants.LABEL_ID_NAME, pe);
	}
	
	public Text getBindingLabel(PictogramElement pe) {
		return getLabel(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME, pe);
	}
	
	private Text getLabel(String labelKey, PictogramElement pe) {
		return this.getFeatureProvider().getLabelService().getLabel(labelKey, pe);
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.features.DelegatingUpdateFeature#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context)
	{
		Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
		return super.canUpdate(context)
				&& (bo instanceof DesignElement)
				&& (RolePEMapping.get().getRoleBinding(context.getPictogramElement()) != null)
				&& (context.getPictogramElement() instanceof ContainerShape
						|| context.getPictogramElement() instanceof Connection);
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context)
	{
		IReason updateNeeded = super.updateNeeded(context);
		if (updateNeeded.toBoolean())
		{
			// ignore the background color determined by the wrapped update feature
			if (!updateNeeded.getText().equals(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR))
			{
				return updateNeeded;
			}
		}
		
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);
		
		if (roleBinding == null)
		{
			throw new IllegalStateException("No Role binding found for " + pe.toString() + " " + bo);
		}

		if (pe instanceof ContainerShape)
		{
			// binding text
			String bindingText = MappingNameCreator.getMappedToText(roleBinding);
			String currentBindingText = getBindingLabelText(context);
			if (roleBinding.getModelElements().isEmpty()
					&& bindingText != null && bindingText.equals(roleBinding.getRole().getName()))
			{
				// ignore binding text
				bindingText = null;
			}
			if (GS.differ(bindingText, currentBindingText))
			{
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
	
			// binding color
			IColorConstant boColor = MappingColors.getMappingColor(context.getPictogramElement());
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor))
			{
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		}

		return Reason.createFalseReason();
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context)
	{
		boolean successful = super.update(context);
		
		PictogramElement pe = context.getPictogramElement();
		if (pe instanceof ContainerShape)
		{
			RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);
	
			// binding text
			String bindingText = MappingNameCreator.getMappedToText(roleBinding);
			String currentBindingText = getBindingLabelText(context);
			if (GS.differ(bindingText, currentBindingText))
			{
				DesignElement patternRole = (DesignElement) getBusinessObjectForPictogramElement(pe);
				Text bindingLabel = getBindingLabel(context);
				if (roleBinding.getModelElements().isEmpty()
						&& bindingText != null && bindingText.equals(patternRole.getName()))
				{
					// pattern role's name and binding text are equal
					if (bindingLabel != null)
					{
						// remove the label
						bindingLabel.setParentGraphicsAlgorithm(null);
					}
				}
				else
				{
					if (bindingLabel == null)
					{
						bindingLabel = this.createTextLabel(pe.getGraphicsAlgorithm());
					}
					bindingLabel.setValue(bindingText);
				}
			}
	
			// binding color
			IColorConstant boColor = MappingColors.getMappingColor(context.getPictogramElement());
			setBackgroundColor(context, boColor);
			
			ContainerShape peContainer = (ContainerShape) pe;
			for (PictogramElement childPE: peContainer.getChildren()) {
				updatePictogramElement(childPE);
			}
			
			IReason success = null;
			// TODO remove this dirty hack
			if (pe.getLink() != null
					&& pe.getLink().getBusinessObjects().size() > 0
					&& pe.getLink().getBusinessObjects().get(0) instanceof Parameter) {
				success = layoutPictogramElement(((ContainerShape) pe).getContainer());
			} else {
				success = layoutPictogramElement(pe);
			}
			
			successful = successful && success.toBoolean();
		}

		return successful;
	}
	
	private String getBindingLabelText(IUpdateContext context) {
		Text label = this.getBindingLabel(context);
		if (label != null) {
			return label.getValue();
		}
		return null;
	}
	
	private Text createTextLabel(GraphicsAlgorithm parent)
	{
		String fontName = FontConstants.FONT_NAME_DEFAULT;
		Font font = Graphiti.getGaService().manageFont(getDiagram(), fontName, 9, false, false);

		Text text = Graphiti.getGaService().createText(parent);
		text.setX(4);
		text.setFilled(false);
		text.setForeground(Graphiti.getGaService().manageColor(getDiagram(), IColorConstant.BLACK));
		text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		text.setFont(font);

		return text;
	}
	
}
