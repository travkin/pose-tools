/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.update;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.MappingColors;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.mapping.ui.editor.features.layout.BindingLabelLayout;
import de.upb.pose.mapping.ui.editor.features.layout.ClassMappingLabelConstants;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.ui.editor.features.layout.LabelConstants;
import de.upb.pose.specification.ui.editor.features.update.IShapeUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.UpdateFeatureConstants;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

/**
 * @author Dietrich Travkin
 */
public class MappingSubsystemUpdateFeature extends DelegatingShapeUpdateFeature {

	public MappingSubsystemUpdateFeature(IShapeUpdateFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	protected Text getLabel(IUpdateContext context)
	{
		return getNameLabel(context.getPictogramElement());
	}
	
	public Text getNameLabel(PictogramElement pe) {
		return getLabel(LabelConstants.LABEL_ID_NAME, pe);
	}
	
	public Text getBindingLabel(PictogramElement pe, int labelIndex) {
		return getLabel(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME + labelIndex, pe);
	}
	
	private String getBindingLabelText(PictogramElement pe, int labelIndex) {
		Text label = this.getBindingLabel(pe, labelIndex);
		if (label != null) {
			return label.getValue();
		}
		return null;
	}
	
	public int getNumberOfBindingLabels(PictogramElement pe) {
		//BindingLabelLayout.getLabelAtIndex((ContainerShape) pictogramElement, 1);
		int currentIndex = 0;
		while (getBindingLabel(pe, currentIndex) != null) {
			currentIndex++;
		}
		return currentIndex;
	}
	
	private Text getLabel(String labelKey, PictogramElement pe) {
		if (labelKey != null && labelKey.startsWith(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME)) {
			int index = Integer.parseInt(labelKey.substring(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.length()));
			return BindingLabelLayout.getLabelAtIndex(((ContainerShape) pe), index + 1);
		}
		return this.getFeatureProvider().getLabelService().getLabel(labelKey, pe);
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.features.DelegatingUpdateFeature#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context)
	{
		Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
		return super.canUpdate(context)
				&& (bo instanceof Subsystem)
				&& (RolePEMapping.get().getRoleBinding(context.getPictogramElement()) != null)
				&& (context.getPictogramElement() instanceof ContainerShape);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context)
	{
		IReason updateNeeded = super.updateNeeded(context);
		if (updateNeeded.toBoolean())
		{
			// ignore the background color determined by the wrapped update feature
			if (!updateNeeded.getText().equals(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR))
			{
				return updateNeeded;
			}
		}
		
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);
		
		if (roleBinding == null)
		{
			throw new IllegalStateException("No Role binding found for " + pe.toString() + " " + bo);
		}

		if (pe instanceof ContainerShape)
		{
			// number of binding labels
			int numberOfMappedModelElements = roleBinding.getModelElements().size();
			int numberOfBindingLabels = getNumberOfBindingLabels(pe);
			if (numberOfBindingLabels != numberOfMappedModelElements) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// binding text
			List<String> bindingTexts = getBindingTexts(roleBinding);
			for (int i = 0; i < bindingTexts.size(); i++) {
				String bindingText = bindingTexts.get(i);
				String currentBindingText = getBindingLabelText(pe, i);
				
				if (GS.differ(bindingText, currentBindingText))	{
					return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
				}
			}
			
			// binding color
			IColorConstant boColor = MappingColors.getMappingColor(context.getPictogramElement());
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor))
			{
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		}

		return Reason.createFalseReason();
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context)
	{
		boolean successful = super.update(context);
		
		PictogramElement pe = context.getPictogramElement();
		if (pe instanceof ContainerShape)
		{
			RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);
			
			int numberOfMappedModelElements = roleBinding.getModelElements().size();
			int numberOfBindingLabels = getNumberOfBindingLabels(pe);
			List<String> bindingTexts = getBindingTexts(roleBinding);
			
			Assert.isTrue(bindingTexts.size() == numberOfMappedModelElements);
			
			// update binding text
			for (int i = 0; i < bindingTexts.size(); i++) {
				String bindingText = bindingTexts.get(i);
				Text currentBindingLabel = getBindingLabel(pe, i);
				
				if (currentBindingLabel == null) {
					// add missing label
					currentBindingLabel = this.createTextLabel(pe.getGraphicsAlgorithm());
				}
				currentBindingLabel.setValue(bindingText);
			}
			
			// remove superfluous labels
			if (numberOfBindingLabels > numberOfMappedModelElements) {
				for (int i = numberOfMappedModelElements; i < numberOfBindingLabels; i++) {
					Text currentBindingLabel = getBindingLabel(pe, i);
					currentBindingLabel.setParentGraphicsAlgorithm(null);
				}
			}
	
			// binding color
			IColorConstant boColor = MappingColors.getMappingColor(context.getPictogramElement());
			setBackgroundColor(context, boColor);
			
			ContainerShape peContainer = (ContainerShape) pe;
			for (PictogramElement childPE: peContainer.getChildren()) {
				updatePictogramElement(childPE);
			}
			
			IReason success = layoutPictogramElement(pe);
			
			successful = successful && success.toBoolean();
		}

		return successful;
	}
	
	private List<String> getBindingTexts(RoleBinding roleBinding) {
		List<String> bindingTexts = new ArrayList<String>(roleBinding.getModelElements().size());
		for (EObject eObject: roleBinding.getModelElements()) {
			String text = MappingNameCreator.retrieveEObjectName(eObject);
			bindingTexts.add(text);
		}
		Collections.sort(bindingTexts);
		return bindingTexts;
	}
	
	private Text createTextLabel(GraphicsAlgorithm parent)
	{
		String fontName = FontConstants.FONT_NAME_DEFAULT;
		Font font = Graphiti.getGaService().manageFont(getDiagram(), fontName, 9, false, false);

		Text text = Graphiti.getGaService().createText(parent);
		text.setFilled(false);
		text.setForeground(Graphiti.getGaService().manageColor(getDiagram(), IColorConstant.BLACK));
		text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		text.setFont(font);

		return text;
	}
	
}
