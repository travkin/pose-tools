/**
 * 
 */
package de.upb.pose.mapping.ui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;

import de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard;
import de.upb.pose.core.ui.wizards.WizardPageCompletionListener;
import de.upb.pose.mapping.MappingConstants;
import de.upb.pose.mapping.PatternApplicationDiagrams2ModelConnector;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.ui.editor.multipage.MultiPagePatternRoleMappingDiagramEditor;

/**
 * @author Dietrich Travkin
 */
public class NewPatternApplicationsModelWizard extends AbstractGraphitiCatalogWizard<PatternApplications> {

	private SelectEcoreModelPage selectEcoreModelFilePage; 
	private IResource selectedResource;
	
	public NewPatternApplicationsModelWizard() {
		super();
	}
	
	public NewPatternApplicationsModelWizard(boolean tryOpeningEditor) {
		super(tryOpeningEditor);
	}
	
	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getEditorId()
	 */
	@Override
	protected String getEditorId() {
		return MultiPagePatternRoleMappingDiagramEditor.ID;
	}
	
	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getModelFileExtension()
	 */
	@Override
	protected String getModelFileExtension() {
		return MappingConstants.MODEL_FILE_EXTENSION;
	}

	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getDiagramFileExtension()
	 */
	@Override
	protected String getDiagramFileExtension() {
		return MappingConstants.DIAGRAMS_FILE_EXTENSION;
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		
		setWindowTitle("New Pattern Applications Model");

		if (!selection.isEmpty() && selection.size() == 1) {
			Object selected = selection.getFirstElement();
			if (selected instanceof IResource) {
				this.selectedResource = (IResource) selected;
			} else if (selected instanceof EObject) {
				Resource resource = ((EObject) selected).eResource();
				if (resource != null) {
					String path = resource.getURI().toPlatformString(true);
					IResource file = ResourcesPlugin.getWorkspace().getRoot().findMember(path);
					this.selectedResource = file;
				}
			}
		}
	}

	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#createModelRoot()
	 */
	@Override
	protected PatternApplications createModelRoot() {
		PatternApplications patternApplicationsModel = null;
		
		IFile ecoreFile = this.getSelectedEcoreFile();
		
		if (ecoreFile != null) {
			String path = ecoreFile.getFullPath().toPortableString();
			URI fileUri = URI.createPlatformResourceURI(path, true);
			
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource fileResource = resourceSet.getResource(fileUri, true);
			
			patternApplicationsModel = PatternApplications2EcoreConnector.createPatternApplicationsForEcoreFile(fileResource);
		}
		
		return patternApplicationsModel;
	}

	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#createDiagramsRoot(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected ContainerShape createDiagramsRoot(PatternApplications modelRoot) {
		// create pattern application diagrams container and connect it to the pattern applications model
		ContainerShape patternApplicationDiagramsContainer = PatternApplicationDiagrams2ModelConnector
				.createPatternApplicationDiagramsContainer();
		PatternApplicationDiagrams2ModelConnector.createPointerToPatternApplicationDiagramsContainer(modelRoot);
		PatternApplicationDiagrams2ModelConnector.linkPatternApplicationsToPatternApplicationDiagramsContainer(
				modelRoot, patternApplicationDiagramsContainer);
		return patternApplicationDiagramsContainer;
	}

	private IFile getSelectedEcoreFile() {
		return this.selectEcoreModelFilePage.getSelectedFile();
	}
	
	@Override
	public void addPages() {
		this.selectEcoreModelFilePage = new SelectEcoreModelPage(this.selectedResource);
		this.selectEcoreModelFilePage.setDescription("Select an ecore model file.");
		addPage(this.selectEcoreModelFilePage);
		super.addPages();
	}
	
	@Override
	public void createPageControls(Composite pageContainer) {
		super.createPageControls(pageContainer);
		
		this.selectEcoreModelFilePage.addPageCompleteListener(new WizardPageCompletionListener() {
			
			@Override
			public void pageIncomplete(WizardPage page) {
				getModelPage().setFileNameWithoutExtension(null);
				getDiagramPage().setFileNameWithoutExtension(null);
			}
			
			@Override
			public void pageCompleted(WizardPage page) {
				IFile selectedEcoreFile = getSelectedEcoreFile();
				if (selectedEcoreFile != null && !selectedEcoreFile.getName().isEmpty()) {
					String newName = selectedEcoreFile.getFullPath().removeFileExtension().lastSegment().toString();
					getModelPage().setFileNameWithoutExtension(newName);
					getModelPage().setFileContainer(selectedEcoreFile.getParent());
					getDiagramPage().setFileNameWithoutExtension(newName);
					getDiagramPage().setFileContainer(selectedEcoreFile.getParent());
				}
			}
			
		});
		
		this.selectEcoreModelFilePage.validate();
	}

}