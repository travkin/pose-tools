package de.upb.pose.mapping.ui.properties;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.CorePackage;
import de.upb.pose.core.ui.properties.StringTextSectionBase;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelElementCreator;

public class RoleBindingNewNameSection extends StringTextSectionBase {
	
	@Override
	protected String getLabelText() {
		return "New Element Name";
	}

	@Override
	protected EStructuralFeature getFeature() {
		return MappingPackage.Literals.ROLE_BINDING__NEW_ELEMENT_NAME;
	}

	@Override
	protected RoleBinding getModelElementFromPictogramElement(PictogramElement pe) {
		if (pe != null) {
			RoleBinding binding = RolePEMapping.get().getRoleBinding(getPE());
			if (binding != null) {
				return binding;
			}
			
			EObject[] bos = Graphiti.getLinkService().getAllBusinessObjectsForLinkedPictogramElement(pe);
			for (EObject bo : bos) {
				if (bo instanceof RoleBinding) {
					return (RoleBinding) bo;
				}
			}
			
		}
		return null;
	}
	
	/**
	 * @see de.upb.pose.core.ui.properties.PropertySectionBase#set(org.eclipse.emf.ecore.EStructuralFeature, java.lang.Object)
	 */
	protected void set(EStructuralFeature feature, Object value) {
		super.set(feature, value);
		
		RoleBinding roleBinding = getModelElementFromPictogramElement(getPE());
		if (roleBinding != null && getEditingDomain() != null) {
			String newName = MappingNameCreator.getNameFor(roleBinding);
			if (roleBinding.getName() == null || !roleBinding.getName().equals(newName)) {
				execute(SetCommand.create(getEditingDomain(), roleBinding, CorePackage.eINSTANCE.getNamed_Name(), newName));
			}
			
			if (roleBinding.getApplicationModelElement() != null) {
				String mappedElementName = ApplicationModelElementCreator.getApplicationModelElementName(roleBinding);
				if (roleBinding.getApplicationModelElement().getName() == null
						|| !roleBinding.getApplicationModelElement().getName().equals(mappedElementName)) {
					execute(SetCommand.create(getEditingDomain(), roleBinding.getApplicationModelElement(),
							CorePackage.eINSTANCE.getNamed_Name(), mappedElementName));
				}
			}
		}
	}
	
}
