package de.upb.pose.mapping.ui.view;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingImages;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.ui.editor.RoleMappingEditor;
import de.upb.pose.mapping.ui.editor.features.custom.ApplyPatternFeature;
import de.upb.pose.mapping.ui.editor.features.custom.ValidateMappingFeature;

public class MappingView extends ViewPart implements ISelectionListener, ITabbedPropertySheetPageContributor {
	
	public static final String ID = "de.upb.pose.mapping.ui.view"; //$NON-NLS-1$
	public static final String TABBED_PROPERTY_CONTRIBUTOR_ID = "de.upb.pose.mapping.ui.propertycontributor";

	// TODO find a better way than to add this implicit dependency to the de.upb.pose.modeling.classes plug-in 
	/*package*/ static final String[] ECORE_EDITOR_IDS = {"de.upb.pose.modeling.classes.editor"};
	
	private static final String TITLE_NO_PATTERN = "No Applied Design Pattern Selected";
	private static final String DESC_NO_PATTERN = "Select an applied pattern in a POSE class diagram editor.";
	
	private PageBook book;
	private Form form;
	private FormToolkit toolkit;
	private Composite emptyComposite;
	private IEditorSite editorSite;
	
	private IEditorPart currentlyOpenEcoreEditor = null;

	private final Map<IEditorPart, MappingViewEditorGroup> editorGroups = new HashMap<IEditorPart, MappingViewEditorGroup>();
	
	private IAction validateAction;
	private IAction applyAction;
	
	private TabbedPropertySheetPage propertySheetPage;

	public static MappingView findMappingView() {
		IViewPart mappingViewPart = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage().findView(MappingView.ID);
		if (mappingViewPart != null) {
			return (MappingView) mappingViewPart;
		}
		return null;
	}
	
	@Override
	public void createPartControl(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());

		// main form
		form = toolkit.createForm(parent);
		form.setText(TITLE_NO_PATTERN);
		toolkit.paintBordersFor(form);
		toolkit.decorateFormHeading(form);

		createToolBar();

		GridLayoutFactory.fillDefaults().applyTo(form.getBody());

		// page book for the body
		book = new PageBook(form.getBody(), SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(book);
		toolkit.adapt(book);
		toolkit.paintBordersFor(book);

		// composite for situation when no pattern is shown
		emptyComposite = toolkit.createComposite(book);
		emptyComposite.setLayout(new FillLayout());

		toolkit.createLabel(emptyComposite, DESC_NO_PATTERN);
	}

	private void createToolBar() {
		IToolBarManager manager = form.getToolBarManager();

		ImageDescriptor icon = MappingImages.getDescriptor(MappingImages.APPLY);
		applyAction = new Action("Apply the pattern with the current settings", icon) {
			@Override
			public void run() {
				applyPattern();
			}
		};
		applyAction.setEnabled(false);
		manager.add(applyAction);

		icon = MappingImages.getDescriptor(MappingImages.VALIDATE);
		validateAction = new Action("Validate the pattern application", icon) {
			@Override
			public void run() {
				validateMapping();
			}
		};
		validateAction.setEnabled(false);
		manager.add(validateAction);

		manager.update(true);
	}
	
	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);

		site.getPage().addPostSelectionListener(this);
		site.getPage().getWorkbenchWindow().getPartService().addPartListener(new PoseEcoreEditorDisposePartListener());
	}
	
	public static boolean isValidEcoreModelEditor(IEditorPart editor) {
		if (editor != null
				&& editor.getSite() != null
				&& isValidEcoreModelEditorId(editor.getSite().getId())) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isValidEcoreModelEditorId(String editorId) {
		if (editorId != null && !editorId.isEmpty()) {
			for (String validEditorId: ECORE_EDITOR_IDS) {
				if (validEditorId.equals(editorId)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private class PoseEcoreEditorDisposePartListener implements IPartListener {

		/**
		 * @see org.eclipse.ui.IPartListener#partActivated(org.eclipse.ui.IWorkbenchPart)
		 */
		@Override
		public void partActivated(IWorkbenchPart part) {
		}

		/**
		 * @see org.eclipse.ui.IPartListener#partBroughtToTop(org.eclipse.ui.IWorkbenchPart)
		 */
		@Override
		public void partBroughtToTop(IWorkbenchPart part) {
		}

		/**
		 * @see org.eclipse.ui.IPartListener#partClosed(org.eclipse.ui.IWorkbenchPart)
		 */
		@Override
		public void partClosed(IWorkbenchPart part) {
			if (part instanceof IEditorPart && isValidEcoreModelEditor((IEditorPart) part)) {
				MappingViewEditorGroup editorGroup = editorGroups.get(part);
				if (editorGroup != null) {
					editorGroup.dispose();
					editorGroups.remove(part);
				}
				if (part.equals(currentlyOpenEcoreEditor)) {
					currentlyOpenEcoreEditor = null;
					showEmptyPage();
				}
			}
		}

		/**
		 * @see org.eclipse.ui.IPartListener#partDeactivated(org.eclipse.ui.IWorkbenchPart)
		 */
		@Override
		public void partDeactivated(IWorkbenchPart part) {
		}

		/**
		 * @see org.eclipse.ui.IPartListener#partOpened(org.eclipse.ui.IWorkbenchPart)
		 */
		@Override
		public void partOpened(IWorkbenchPart part) {
		}
	}

	@Override
	public void dispose() {
		// dispose form toolkit
		if (toolkit != null) {
			toolkit.dispose();
		}

		// remove post selection listener
		IWorkbenchPartSite site = getSite();
		if (site != null) {
			IWorkbenchPage page = site.getPage();
			if (page != null) {
				page.removePostSelectionListener(this);
				page.removePostSelectionListener(propertySheetPage);
			}
		}
		
		if (propertySheetPage != null) {
			propertySheetPage.dispose();
			propertySheetPage = null;
		}
		
		for (MappingViewEditorGroup group: this.editorGroups.values()) {
			group.dispose();
		}
		this.editorGroups.clear();
		
		emptyComposite.dispose();
		book.dispose();
		form.dispose();
		toolkit.dispose();
		
		validateAction = null;
		applyAction = null;
		editorSite = null;
		currentlyOpenEcoreEditor = null;
		
		super.dispose();
	}

	@Override
	public void setFocus() {
		form.setFocus();
	}
	
	@Override
	public String getContributorId() {
		return TABBED_PROPERTY_CONTRIBUTOR_ID;
	}
	
	private TabbedPropertySheetPage getPropertySheetPage() {
		if (propertySheetPage == null) {
			propertySheetPage = new TabbedPropertySheetPage(this);
		}
		return propertySheetPage;
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class required) {
		if (IPropertySheetPage.class.equals(required)) {
			return getPropertySheetPage();
		}

		return super.getAdapter(required);
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		String partId = part.getSite().getId();
		
		if (isValidEcoreModelEditorId(partId)) {
			AppliedPattern selectedPatternApplication = this.getSelectedPatternApplication(selection);

			if (!part.equals(currentlyOpenEcoreEditor)) {
				openPoseEcoreEditorChanged((IEditorPart) part);

				if (selectedPatternApplication == null) {
					this.showEmptyPage();
				}
			}

			if (selectedPatternApplication != null) {
				selectedPatternApplicationChanged((IEditorPart) part, selectedPatternApplication);
			}
		}
	}
	
	private void openPoseEcoreEditorChanged(IEditorPart part) {
		currentlyOpenEcoreEditor = part;
		
		MappingViewEditorGroup editorGroup = this.editorGroups.get(part);
		if (editorGroup == null) {
			editorGroup = new MappingViewEditorGroup();
			this.editorGroups.put(part, editorGroup);
		}
	}

	private void selectedPatternApplicationChanged(IEditorPart part, AppliedPattern selectedPatternApplication) {
		MappingViewEditorGroup currentEditorGroup = this.editorGroups.get(part);
		
		try {
			RoleMappingEditor mappingEditor = currentEditorGroup.switchToSelection(selectedPatternApplication, getEditorSite(), book);
			getSite().setSelectionProvider(mappingEditor.getGraphicalViewer());
			
			mappingEditor.getGraphicalViewer().addSelectionChangedListener(new RoleMappingSelectionChangedListener(this));
			
			validateAction.setEnabled(true);
			applyAction.setEnabled(true);

			form.setText(getCurrentTitle(currentEditorGroup.getCurrentPatternApplication()));

			book.showPage(mappingEditor.getControl());
		} catch (PartInitException e) {
			e.printStackTrace();
			ErrorDialog.openError(
					part.getSite().getShell(),
					"Could not open editor",
					"The role mapping editor could not be opened for the selected pattern application.",
					null);
			this.showEmptyPage();
		}
	}
	
	private class RoleMappingSelectionChangedListener implements ISelectionChangedListener {
		
		private MappingView mappingView;
		
		RoleMappingSelectionChangedListener(MappingView mappingView) {
			this.mappingView = mappingView;
		}
		
		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			if (propertySheetPage != null) {
				//IWorkbenchPart part = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
				propertySheetPage.selectionChanged(this.mappingView, event.getSelection());
			}
		}
		
	}

	private String getCurrentTitle(AppliedPattern patternApplication) {
		if (patternApplication != null) {
			return MappingNameCreator.getNameFor(patternApplication);
		}
		return TITLE_NO_PATTERN;
	}
	
	public IEditorPart findEcoreEditorFor(RoleMappingEditor mappingEditor) {
		if (mappingEditor != null) {
			for (IEditorPart ecoreEditor: this.editorGroups.keySet()) {
				MappingViewEditorGroup editorGroup = this.editorGroups.get(ecoreEditor);
				if (editorGroup.getCurrentMappingEditor() == mappingEditor || editorGroup.containsMappingEditor(mappingEditor)) {
					return ecoreEditor;
				}
			}
		}
		
		return null;
	}
	
	private AppliedPattern getSelectedPatternApplication(ISelection selection) {
		AppliedPattern pattern = null;
		
		EObject selectedEObject = MappingViewSelectionService.adaptSelectionToEObject(selection);
		if (selectedEObject instanceof AppliedPattern) {
			pattern = (AppliedPattern) selectedEObject;
		}
		return pattern;
	}
	
	private void showEmptyPage() {
		validateAction.setEnabled(false);
		applyAction.setEnabled(false);
		
		form.setText(getCurrentTitle(null));

		getSite().setSelectionProvider(null);
		
		book.showPage(emptyComposite);
	}

	public IEditorSite getEditorSite() {
		if (editorSite == null) {
			editorSite = new DummyEditorSite(getSite());
		}
		return editorSite;
	}

	private ICustomFeature findCustomFeature(RoleMappingEditor editor, ICustomContext context,
			Class<? extends ICustomFeature> featureClass) {
		IFeatureProvider fp = editor.getDiagramTypeProvider().getFeatureProvider();
		if (fp != null) {
			ICustomFeature[] features = fp.getCustomFeatures(context);
			for (ICustomFeature feature : features) {
				if (featureClass.isAssignableFrom(feature.getClass())) {
					return feature;
				}
			}
		}
		return null;
	}

	private void executeCustomFeature(Class<? extends ICustomFeature> featureClass) {
		MappingViewEditorGroup group = this.editorGroups.get(this.currentlyOpenEcoreEditor);
		if (group != null) {
			RoleMappingEditor roleMappingEditor = group.getCurrentMappingEditor();
			if (roleMappingEditor != null && group.getCurrentDiagram() != null) {
				ICustomContext context = new CustomContext(new PictogramElement[] { group.getCurrentDiagram() });
				ICustomFeature feature = this.findCustomFeature(roleMappingEditor, context, featureClass);
				roleMappingEditor.executeFeature(feature, context);
			}
		}
	}

	public void applyPattern() {
		this.executeCustomFeature(ApplyPatternFeature.class);
	}

	public void validateMapping() {
		this.executeCustomFeature(ValidateMappingFeature.class);
	}
	
}
