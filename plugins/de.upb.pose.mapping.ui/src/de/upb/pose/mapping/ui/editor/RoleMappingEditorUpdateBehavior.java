package de.upb.pose.mapping.ui.editor;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;

public class RoleMappingEditorUpdateBehavior extends DefaultUpdateBehavior {
	
	public RoleMappingEditorUpdateBehavior(RoleMappingEditor editor) {
		super(editor);
	}
	
	private RoleMappingEditor getEditor() {
		return (RoleMappingEditor) this.diagramEditor;
	}
	
	@Override
	protected void createEditingDomain() {
		Diagram diagram = this.getEditor().getDiagram(); 
		if (diagram == null) {
			throw new IllegalStateException("The role mapping editor has no diagram (editor input).");
		} else {
			TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(diagram);
			if (editingDomain == null) {
				throw new IllegalStateException("Could not find / create an editing domain for the diagram.");
			}
			
			initializeEditingDomain(editingDomain);
		}
	}

	@Override
	protected void disposeEditingDomain() {
		// do not dispose editing domain
	}
}
