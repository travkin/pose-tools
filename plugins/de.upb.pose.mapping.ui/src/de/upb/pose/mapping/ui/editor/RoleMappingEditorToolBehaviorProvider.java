package de.upb.pose.mapping.ui.editor;

import java.util.Collection;

import org.eclipse.graphiti.datatypes.ILocation;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IFeature;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.features.context.impl.CustomContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.tb.ContextButtonEntry;
import org.eclipse.graphiti.tb.DefaultToolBehaviorProvider;
import org.eclipse.graphiti.tb.IContextButtonEntry;
import org.eclipse.graphiti.tb.IContextButtonPadData;

import de.upb.pose.mapping.ui.editor.features.custom.AddSetFragmentInstanceFeature;
import de.upb.pose.mapping.ui.editor.features.custom.MapElementFeature;
import de.upb.pose.mapping.ui.editor.features.custom.NavigateSetInstancesFeature;
import de.upb.pose.mapping.ui.editor.features.custom.RemoveSetInstanceFeature;
import de.upb.pose.mapping.ui.editor.features.custom.UnmapFeature;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;

public class RoleMappingEditorToolBehaviorProvider extends DefaultToolBehaviorProvider {
	
	public RoleMappingEditorToolBehaviorProvider(IDiagramTypeProvider dtp) {
		super(dtp);
	}

	@Override
	public IContextButtonPadData getContextButtonPad(IPictogramElementContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getFeatureProvider().getBusinessObjectForPictogramElement(pe);

		// create default/empty pad data
		IContextButtonPadData pad = super.getContextButtonPad(context);
		setGenericContextButtons(pad, pe, 0);

		// for set fragments, show instance navigation buttons
		if (bo instanceof SetFragment) {
			addButtonsFor((SetFragment) bo, pe, pad);
		}

		// for other design elements, add map or un-map buttons
		if (bo instanceof DesignElement) {
			addButtonsFor((DesignElement) bo, pe, pad);
		}

		return pad;
	}
	
	private void addButtonsFor(SetFragment businessObject, PictogramElement businessObjectPE, IContextButtonPadData buttonPad ) {
		// show buttons around the set title
		Collection<IContextButtonEntry> buttons = buttonPad.getGenericContextButtons();
		GraphicsAlgorithm ga = businessObjectPE.getGraphicsAlgorithm();
		ILocation originalGALocation = getAbsoluteLocation(ga);
		GraphicsAlgorithm rectangleGA = ga.getGraphicsAlgorithmChildren().get(0);
		Point point = ((Polygon) rectangleGA.getGraphicsAlgorithmChildren().get(0)).getPoints().get(1);
		buttonPad.getPadLocation().setRectangle(originalGALocation.getX(), originalGALocation.getY(), point.getX(), 25);
		
		ICustomContext customContext = new CustomContext(new PictogramElement[] { businessObjectPE });

		// add set fragment instance buttons
		buttons.add(new ContextButtonEntry(new AddSetFragmentInstanceFeature(getFeatureProvider()), customContext));
		buttons.add(new ContextButtonEntry(new RemoveSetInstanceFeature(getFeatureProvider()), customContext));
		buttons.add(new ContextButtonEntry(new NavigateSetInstancesFeature(getFeatureProvider(), true), customContext));
		buttons.add(new ContextButtonEntry(new NavigateSetInstancesFeature(getFeatureProvider(), false), customContext));
	} 

	private void addButtonsFor(DesignElement businessObject, PictogramElement businessObjectPE, IContextButtonPadData buttonPad ) {
		Collection<IContextButtonEntry> buttons = buttonPad.getGenericContextButtons();
		
		ICustomContext customContext = new CustomContext(new PictogramElement[] { businessObjectPE });

		// try adding un-map or map feature
		IFeature mapFeature = new MapElementFeature(getFeatureProvider());
		IFeature unmapFeature = new UnmapFeature(getFeatureProvider());
		if (mapFeature.canExecute(customContext)) {
			buttons.add(new ContextButtonEntry(mapFeature, customContext));
		}
		if (unmapFeature.canExecute(customContext)) {
			buttons.add(new ContextButtonEntry(unmapFeature, customContext));
		}
	}
	
	@Override
	public boolean isMultiSelectionEnabled() {
		return false;
	}

	@Override
	public boolean isShowFlyoutPalette() {
		return false;
	}
}
