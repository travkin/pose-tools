/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Size;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.ui.editor.features.layout.ActionLayoutFeature;

/**
 * @author Dietrich Travkin
 */
public class MappingActionLayoutFeature extends ActionLayoutFeature
{
	public MappingActionLayoutFeature(RoleMappingEditorFeatureProvider fp) {
		super(fp);
	}
	
//	@Override
//	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
//		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
//			return (pictogramElement instanceof ContainerShape);
//		}		
//		return super.canHandle(labelIdentifier, pictogramElement);
//	}
//
//	@Override
//	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
//		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
//			return getBindingLabel(pictogramElement);
//		}
//		return super.getLabel(labelIdentifier, pictogramElement);
//	}
//	
//	protected Text getBindingLabel(PictogramElement pictogramElement) {
//		return BindingLabelLayout.getLabelAtIndex((ContainerShape) pictogramElement, 1);
//	}
//	
//	protected Text getBindingLabel(ILayoutContext context) {
//		return getBindingLabel(context.getPictogramElement());
//	}
//	
//	protected Size determinePreferredBindingLabelSize(ILayoutContext context) {
//		Text bindingLabel = getBindingLabel(context);
//		if (bindingLabel == null) {
//			return null;
//		}
//		return determinePreferredLabelSize(bindingLabel);
//	}
//	
//	@Override
//	public Size determineMinimalShapeContentsSize(ILayoutContext context)
//	{
//		Size nameLabelAndChildrenSize = super.determineMinimalShapeContentsSize(context);
//		Size bindingLabelSize = determinePreferredBindingLabelSize(context);
//		Size preferredShapeSize = BindingLabelLayout.determineMinimalShapeContentsSize(
//				nameLabelAndChildrenSize, bindingLabelSize);
//		return preferredShapeSize;
//	}
//	
//	@Override
//	protected boolean resizeShapeContents(ILayoutContext context) {
//		boolean sizeChanged = super.resizeShapeContents(context);
//		
//		Text bindingLabel = getBindingLabel(context);
//		Size preferredLabelSize = determinePreferredLabelSize(bindingLabel);
//		
//		// resize label
//		if (bindingLabel != null
//				&& (bindingLabel.getWidth() != preferredLabelSize.getWidth()
//				|| bindingLabel.getHeight() != preferredLabelSize.getHeight())) {
//			bindingLabel.setWidth(preferredLabelSize.getWidth());
//			bindingLabel.setHeight(preferredLabelSize.getHeight());
//			sizeChanged = true;
//		}
//		return sizeChanged;
//	}
//	
//	@Override
//	protected boolean relocateShapeContents(ILayoutContext context) {
//		boolean anythingChanged = false;
//		
//		Text nameLabel = getLabel(context);
//		Text bindingLabel = getBindingLabel(context);
//		
//		if (bindingLabel == null) {
//			return super.relocateShapeContents(context);
//		}
//		
//		Size shapeSize = this.determineCurrentShapeSize(context);
//		
//		// center the main label in the parent's drawing area
//		int name_x = (int) Math.round(shapeSize.getWidth()/2d - nameLabel.getWidth()/2d);
//		int binding_x = (int) Math.round(shapeSize.getWidth()/2d - bindingLabel.getWidth()/2d);
//		
//		int bothLabelsHeight = nameLabel.getHeight() + bindingLabel.getHeight() + BindingLabelLayout.PADDING;
//		int name_y = (int) Math.round(shapeSize.getHeight()/2d - bothLabelsHeight/2d);
//		int binding_y = name_y + nameLabel.getHeight() + BindingLabelLayout.PADDING;
//		
//		if (nameLabel.getX() != name_x || nameLabel.getY() != name_y
//				|| bindingLabel.getX() != binding_x || bindingLabel.getY() != binding_y) {
//			nameLabel.setX(name_x);
//			nameLabel.setY(name_y);
//			bindingLabel.setX(binding_x);
//			bindingLabel.setY(binding_y);
//			anythingChanged = true;
//		}
//		
//		int y = bindingLabel.getY() + bindingLabel.getHeight();
//		anythingChanged = anythingChanged || relocateChildren(context, y);
//		
//		return anythingChanged;
//	}
	
}
