package de.upb.pose.mapping.ui.editor;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;

public class RoleMappingDiagramTypeProvider extends AbstractDiagramTypeProvider {
	
	public static final String DIAGRAM_TYPE_PROVIDER_ID = "de.upb.pose.mapping.diagramtypeprovider";
	public static final String DIAGRAM_TYPE_ID = "de.upb.pose.mapping.diagram.type";
	
	private IToolBehaviorProvider[] tbps;

	public RoleMappingDiagramTypeProvider() {
		setFeatureProvider(new RoleMappingEditorFeatureProvider(this));
	}

	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		if (tbps == null) {
			tbps = new IToolBehaviorProvider[] { new RoleMappingEditorToolBehaviorProvider(this) };
		}
		return tbps;
	}

	@Override
	public boolean isAutoUpdateAtStartup() {
		return true;
	}
}
