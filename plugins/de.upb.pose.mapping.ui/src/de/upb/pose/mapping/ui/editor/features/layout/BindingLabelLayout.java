/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.Size;

/**
 * @author Dietrich Travkin
 */
public class BindingLabelLayout
{
	public static final int PADDING = 0; //GraphicsAlgorithmsFactory.PADDING_SMALL;

	public static Text getLabelAtIndex(ContainerShape pictogramElement, int index)
	{
		int currentIndex = -1;
		for (GraphicsAlgorithm childGA : pictogramElement.getGraphicsAlgorithm().getGraphicsAlgorithmChildren())
		{
			if (childGA instanceof Text)
			{
				currentIndex++;
				if (currentIndex == index) {
					return (Text) childGA;
				}
			}
		}
		return null;
	}
	
	public static Size determineMinimalShapeContentsSize(Size preferredNameLabelSize, Size preferredBindingLabelSize)
	{
		if (preferredBindingLabelSize == null)
		{
			return preferredNameLabelSize;
		}
		
		Size minSize = Size.asRows(preferredNameLabelSize, preferredBindingLabelSize);
		minSize = minSize.padding(0, PADDING);
		return minSize;
	}
	
}
