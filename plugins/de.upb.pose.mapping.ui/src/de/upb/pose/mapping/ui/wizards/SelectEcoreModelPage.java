package de.upb.pose.mapping.ui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

import de.upb.pose.core.ui.Activator;
import de.upb.pose.core.ui.wizards.FileViewerFiler;
import de.upb.pose.core.ui.wizards.WizardPageBase;

public class SelectEcoreModelPage extends WizardPageBase {
	
	private static final String ECORE_MODEL_FILE_EXTENSION = "ecore";
	
	private IResource currentlySelectedResource;
	private IFile ecoreFile;

	private TreeViewer existingTreeViewer;
	
	public SelectEcoreModelPage(IResource selectedResource) {
		super(SelectEcoreModelPage.class.getName());
		this.currentlySelectedResource = selectedResource;
	}

	@Override
	protected void createWidgets(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		existingTreeViewer = new TreeViewer(composite, SWT.BORDER | SWT.SINGLE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(existingTreeViewer.getControl());

		existingTreeViewer.setContentProvider(new WorkbenchContentProvider());
		existingTreeViewer.setLabelProvider(new WorkbenchLabelProvider());
		existingTreeViewer.addFilter(new FileViewerFiler(ECORE_MODEL_FILE_EXTENSION));
		
		existingTreeViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
		if (this.currentlySelectedResource != null && this.currentlySelectedResource.exists()) {
			existingTreeViewer.setSelection(new StructuredSelection(this.currentlySelectedResource), true);
			checkSelection();
		}
	}
	
	@Override
	protected MultiStatus doValidate(MultiStatus status) {
		String pluginId = Activator.get().getSymbolicName();

		if (this.ecoreFile == null) {
			status.add(new Status(IStatus.ERROR, pluginId, "Select a valid ecore file where design patterns are to be applied."));
		}
		return status;
	}

	@Override
	protected void hookListeners() {
		// select model element on tree selection
		existingTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				checkSelection();
				validate();
			}
			
		});
	}
	
	private void checkSelection() {
		if (this.existingTreeViewer != null) {
			ecoreFile = null;
			
			ISelection selection = existingTreeViewer.getSelection();
			if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
				IStructuredSelection actualSelection = (IStructuredSelection) selection;
				Object selectedResource = actualSelection.getFirstElement();
				if (selectedResource instanceof IFile) {
					IFile file = (IFile) selectedResource;
					if (file.exists() && file.isAccessible()) {
						String extension = file.getFileExtension();
						if (ECORE_MODEL_FILE_EXTENSION.equals(extension)) {
							ecoreFile = file;
						}
					}
				}
			}
		}
	}
	
	public IFile getSelectedFile() {
		return this.ecoreFile;
	}
	
}
