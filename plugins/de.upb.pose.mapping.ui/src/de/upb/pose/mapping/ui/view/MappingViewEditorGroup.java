/**
 * 
 */
package de.upb.pose.mapping.ui.view;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingConstants;
import de.upb.pose.mapping.ui.editor.RoleMappingDiagramTypeProvider;
import de.upb.pose.mapping.ui.editor.RoleMappingEditor;

/**
 * @author Dietrich Travkin
 */
public class MappingViewEditorGroup {

	private final Map<Diagram, RoleMappingEditor> roleMappingEditors = new HashMap<Diagram, RoleMappingEditor>();
	
	private AppliedPattern currentPatternApplication = null;
	private Diagram currentDiagram = null;
	
	public AppliedPattern getCurrentPatternApplication() {
		return this.currentPatternApplication;
	}
	
	public Diagram getCurrentDiagram() {
		return this.currentDiagram;
	}
	
	public RoleMappingEditor switchToSelection(AppliedPattern selection, IEditorSite editorSite, Composite parent) throws PartInitException {
		// FIXME check why the selection is sometimes without a parent Resource (eResource() delivers null)
		currentPatternApplication = selection;
		currentDiagram = getDiagram(selection);
		if (currentDiagram == null) {
			throw new IllegalStateException("Could not find diagram for the given pattern application.");
		}
		
		return getMappingDiagramEditorFor(currentDiagram, editorSite, parent);
	}
	
	public RoleMappingEditor getCurrentMappingEditor() {
		if (this.currentDiagram != null) {
			RoleMappingEditor mappingEditor = roleMappingEditors.get(this.currentDiagram);
			return mappingEditor;
		}
		return null;
	}
	
	public boolean containsMappingEditor(RoleMappingEditor mappingEditor) {
		return this.roleMappingEditors.containsValue(mappingEditor);
	}
	
	public void dispose() {
		for (RoleMappingEditor mappingEditor : this.roleMappingEditors.values()) {
			mappingEditor.dispose();
		}
		this.roleMappingEditors.clear();
	}
	
	private Diagram getDiagram(AppliedPattern element) {
		if (element.eResource() == null
				|| element.eResource().getResourceSet() == null) {
			return null;
		}
		
		ResourceSet resourceSet = element.eResource().getResourceSet();
		for (Resource resource : resourceSet.getResources()) {
			String extension = resource.getURI().fileExtension();
			if (MappingConstants.DIAGRAMS_FILE_EXTENSION.equals(extension)) {
				if (!resource.isLoaded()) {
					try {
						resource.load(resourceSet.getLoadOptions());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				for (EObject content : resource.getContents()) {
					if (content instanceof ContainerShape) {
						for (Shape child : ((ContainerShape) content).getChildren()) {
							if (child instanceof Diagram) {
								for (EObject bo : child.getLink().getBusinessObjects()) {
									if (element.equals(bo)) {
										return (Diagram) child;
									}
								}
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	private RoleMappingEditor getMappingDiagramEditorFor(Diagram diagram, IEditorSite editorSite, Composite parent) throws PartInitException {
		RoleMappingEditor mappingEditor = roleMappingEditors.get(diagram);
		if (mappingEditor == null) {
			mappingEditor = new RoleMappingEditor(diagram);
			roleMappingEditors.put(diagram, mappingEditor);
			
			IEditorInput editorInput = DiagramEditorInput.createEditorInput(diagram, RoleMappingDiagramTypeProvider.DIAGRAM_TYPE_PROVIDER_ID);
			mappingEditor.init(editorSite, editorInput);
			mappingEditor.createPartControl(parent);
		}
		return mappingEditor;
	}

}
