package de.upb.pose.mapping.ui.mapping;

import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplicationDiagrams2ModelConnector;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.mapping.ui.editor.RoleMappingDiagramTypeProvider;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.TaskDescription;

public class MappingDiagramCreator
{	
	public static Diagram createMappingDiagramFor(AppliedPattern appliedPattern, Diagram patternSpecDiag)
	{
		// copy specification diagram
		Diagram mappingDiag = EcoreUtil.copy(patternSpecDiag);
		mappingDiag.setDiagramTypeId(RoleMappingDiagramTypeProvider.DIAGRAM_TYPE_ID);
		
		// add diagram to mapping diagram container
		ContainerShape mappingDiagramsContainer = PatternApplicationDiagrams2ModelConnector
				.getLinkedPatternApplicationDiagramsContainer(appliedPattern.getParent());
		mappingDiagramsContainer.getChildren().add(mappingDiag);
		
		// link the additional model elements (mainly bindings) to the pictogram elements
		linkPictogramElements(appliedPattern, mappingDiag);
		
		return mappingDiag;
	}
	
	private static void linkPictogramElements(AppliedPattern appliedPattern, Diagram mappingDiag)
	{
		// link diagram with applied pattern
		mappingDiag.getLink().getBusinessObjects().add(appliedPattern);
		
		// link all role bindings, set bindings, and set element bindings
		TreeIterator<Object> it = EcoreUtil.getAllContents(mappingDiag, true);
		while (it.hasNext())
		{
			Object object = it.next();
			if (object instanceof PictogramElement)
			{
				PictogramElement pe = (PictogramElement) object;
				EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
				
				if (bo instanceof DesignElement)
				{
					linkRoleBindings((DesignElement) bo, pe, appliedPattern);
				}
				else if (bo instanceof SetFragment)
				{
					linkSetBindings((SetFragment) bo, pe, appliedPattern);
				}
				else if (bo instanceof TaskDescription)
				{
					// TODO link tasks
				}
			}
		}
	}
	
	private static void linkRoleBindings(DesignElement patternRole, PictogramElement pe, AppliedPattern appliedPattern)
	{
		// link all available role bindings for the given pattern role
		List<RoleBinding> roleBindings = RoleMapper.findAllRoleBindingsFor(patternRole, appliedPattern);
		pe.getLink().getBusinessObjects().addAll(roleBindings);
	}
	
	private static void linkSetBindings(SetFragment setFragment, PictogramElement pe, AppliedPattern appliedPattern)
	{
		// link the only set binding for the given set fragment
		SetFragmentBinding setBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(setFragment, appliedPattern); // there must be already one set binding
		pe.getLink().getBusinessObjects().add(setBinding);
		
		// link all available set element bindings for this set binding
		for (SetFragmentInstance setElementBinding : setBinding.getSetFragmentInstances())
		{
			pe.getLink().getBusinessObjects().add(setElementBinding);
		}

		// set the currently visible set element binding to the first one
		SetFragmentPEMapping.get().setCurrentlyVisibleSetFragmentInstanceIndex(pe, 1);
	}
	
}