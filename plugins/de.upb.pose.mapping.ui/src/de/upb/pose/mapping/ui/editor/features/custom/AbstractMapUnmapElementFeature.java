/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.custom;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.platform.IDiagramEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;

import de.upb.pose.mapping.ui.editor.RoleMappingEditor;
import de.upb.pose.mapping.ui.view.MappingView;
import de.upb.pose.mapping.ui.view.MappingViewSelectionService;
import de.upb.pose.specification.DesignElement;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractMapUnmapElementFeature extends AbstractCustomFeature {

	public AbstractMapUnmapElementFeature(IFeatureProvider fp) {
		super(fp);
	}

	protected final DesignElement getSelectedPatternRole(ICustomContext context) {
		if (context.getPictogramElements() != null && context.getPictogramElements().length == 1) {
			PictogramElement mappingPE = context.getPictogramElements()[0];
			Object mappingBO = getFeatureProvider().getBusinessObjectForPictogramElement(mappingPE);
			
			if (mappingBO instanceof DesignElement) {
				return (DesignElement) mappingBO;
			}
		}
		return null;
	}
	
	protected final IEditorPart findEcoreEditorPart() {
		MappingView mappingView = MappingView.findMappingView();
		
		if (mappingView != null) {
			IDiagramEditor mappingEditor = this.getDiagramEditor();
			
			if (mappingEditor instanceof RoleMappingEditor) {
				return mappingView.findEcoreEditorFor((RoleMappingEditor) mappingEditor); 
			}
		}
		return null;
	}
	
	protected final EObject[] getSelectedEcoreModelElements(ICustomContext context) {
		DesignElement selectedPatternRole = this.getSelectedPatternRole(context);
		
		if (selectedPatternRole != null) {
			IEditorPart ecoreEditorPart = this.findEcoreEditorPart();
			
			if (ecoreEditorPart != null
					&& ecoreEditorPart.getSite() != null
					&& ecoreEditorPart.getSite().getSelectionProvider() != null) {
				
				ISelection currentSelection = ecoreEditorPart.getSite().getSelectionProvider().getSelection();
				
				if (!currentSelection.isEmpty() && currentSelection instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) currentSelection;
					
					EObject[] selected = new EObject[selection.size()];
					int i = 0;
					@SuppressWarnings("rawtypes")
					Iterator iter = selection.iterator();
					while (iter.hasNext()) {
						Object sel = iter.next();
						selected[i] = MappingViewSelectionService.adaptObjectToEObject(sel);
						i++;
					}
					return selected;
				}
			}
		}
		return new EObject[0];
	}
	
}
