/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.ui.editor.features.layout.ResultVariableLayoutFeature;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class MappingResultVariableLayoutFeature extends ResultVariableLayoutFeature {

	public MappingResultVariableLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#canHandle(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return (pictogramElement instanceof ContainerShape);
		}		
		return super.canHandle(labelIdentifier, pictogramElement);
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#getLabel(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return getBindingLabel(pictogramElement);
		}
		return super.getLabel(labelIdentifier, pictogramElement);
	}
	
	protected Text getBindingLabel(PictogramElement pictogramElement) {
		return BindingLabelLayout.getLabelAtIndex((ContainerShape) pictogramElement, 1);
	}
	
	protected Text getBindingLabel(ILayoutContext context) {
		return getBindingLabel(context.getPictogramElement());
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		Size preferredNameSize = super.determineMinimalShapeContentsSize(context);
		
		Text bindingLabel = getBindingLabel(context);
		if (bindingLabel == null) {
			return preferredNameSize;
		}
		
		Size preferredBindingLabelSize = GS.getSize(bindingLabel).padding(getLabelPadding().getWidth(), 0);
		
		Size preferredSize = Size.asRows(preferredNameSize, preferredBindingLabelSize);
		return preferredSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#resizeShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean anythingChanged = super.resizeShapeContents(context);
		
		Text bindingLabel = getBindingLabel(context);
		if (bindingLabel == null) {
			return anythingChanged;
		}
		
		// resize binding label
		Size preferredBindingLabelSize = GS.getSize(bindingLabel).padding(getLabelPadding().getWidth(), 0);
		if (bindingLabel.getWidth() != preferredBindingLabelSize.getWidth()
				|| bindingLabel.getHeight() != preferredBindingLabelSize.getHeight()) {
			bindingLabel.setWidth(preferredBindingLabelSize.getWidth());
			bindingLabel.setHeight(preferredBindingLabelSize.getHeight());
			return true;
		}
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.ParameterLayoutFeature#relocateShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = super.relocateShapeContents(context);
		
		getLabel(context).setY(0);
		
		Text bindingLabel = getBindingLabel(context);
		if (bindingLabel == null) {
			return anythingChanged;
		}
		
		// center the binding label in the parent's drawing area
		Size shapeSize = this.determineCurrentShapeSize(context); 
		int x = (int) Math.round(shapeSize.getWidth()/2d - bindingLabel.getWidth()/2d);
		int y = getLabel(context).getY() + getLabel(context).getHeight();
		if (bindingLabel.getX() != x || bindingLabel.getY() != y) {
			bindingLabel.setX(x);
			bindingLabel.setY(y);
			return true;
		}
		return anythingChanged;
	}
}
