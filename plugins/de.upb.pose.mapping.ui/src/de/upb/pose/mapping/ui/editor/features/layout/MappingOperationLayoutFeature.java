/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.util.Size;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.ui.editor.features.layout.OperationLayoutFeature;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;

/**
 * @author Dietrich Travkin
 *
 */
public class MappingOperationLayoutFeature extends OperationLayoutFeature
{
	public MappingOperationLayoutFeature(RoleMappingEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return (pictogramElement instanceof ContainerShape);
		}		
		return super.canHandle(labelIdentifier, pictogramElement);
	}

	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return getBindingLabel(pictogramElement);
		}
		return super.getLabel(labelIdentifier, pictogramElement);
	}
	
	protected Text getBindingLabel(PictogramElement pictogramElement) {
		return BindingLabelLayout.getLabelAtIndex((ContainerShape) pictogramElement, 2);
	}
	
	protected Text getBindingLabel(ILayoutContext context) {
		return getBindingLabel(context.getPictogramElement());
	}
	
	protected Size determinePreferredBindingLabelSize(ILayoutContext context) {
		Text bindingLabel = getBindingLabel(context);
		if (bindingLabel == null) {
			return null;
		}
		return determinePreferredLabelSize(bindingLabel);
	}
	
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		Size nameLabelSize = super.determineMinimalShapeContentsSize(context);
		Size bindingLabelSize = determinePreferredBindingLabelSize(context);
		Size preferredShapeSize = BindingLabelLayout.determineMinimalShapeContentsSize(
				nameLabelSize, bindingLabelSize);
		return preferredShapeSize;
	}
	
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean sizeChanged = super.resizeShapeContents(context);
		
		Text bindingLabel = getBindingLabel(context);
		Size preferredLabelSize = determinePreferredLabelSize(bindingLabel);
		
		// resize label
		if (bindingLabel != null
				&& (bindingLabel.getWidth() != preferredLabelSize.getWidth()
				|| bindingLabel.getHeight() != preferredLabelSize.getHeight())) {
			bindingLabel.setWidth(preferredLabelSize.getWidth());
			bindingLabel.setHeight(preferredLabelSize.getHeight());
			sizeChanged = true;
		}
		return sizeChanged;
	}
	
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		ContainerShape shapePE = (ContainerShape) context.getPictogramElement();
		Text nameLabel = getLabel(context);
		Text suffixLabel = OperationUtil.getSuffixLabel(context.getPictogramElement());
		Text bindingLabel = getBindingLabel(context);
		
		if (bindingLabel == null) {
			return super.resizeShapeContents(context);
		}
		
		Size shapeSize = this.determineCurrentShapeSize(context);
		
		// determine the width and height of the name (prefix) label, parameter label children, and the suffix label
		int parentLabelsWidth = nameLabel.getWidth();
		int parentLabelsHeight = nameLabel.getHeight();
		for (Shape child : shapePE.getChildren()) {
			Size preferredChildSize = determineChildSize(child);
			parentLabelsWidth += preferredChildSize.getWidth() + GraphicsAlgorithmsFactory.PADDING_SMALL;
			parentLabelsHeight = Math.max(parentLabelsHeight, preferredChildSize.getHeight());
		}
		if (shapePE.getChildren().size() > 0) {
			parentLabelsWidth -= GraphicsAlgorithmsFactory.PADDING_SMALL;
		}
		parentLabelsWidth += suffixLabel.getWidth();
		
		// center the main labels in the parent's drawing area
		int name_x = (int) Math.round(shapeSize.getWidth()/2d - parentLabelsWidth/2d);
		int binding_x = (int) Math.round(shapeSize.getWidth()/2d - bindingLabel.getWidth()/2d);
		
		int bothLabelsHeight = parentLabelsHeight + bindingLabel.getHeight() + BindingLabelLayout.PADDING;
		int parentLabels_y = (int) Math.round(shapeSize.getHeight()/2d - bothLabelsHeight/2d); 
		int name_y = parentLabels_y;
		if (parentLabelsHeight > nameLabel.getHeight()) {
			name_y += (int) Math.round((parentLabelsHeight - nameLabel.getHeight())/2d);
		}
		int binding_y = name_y + nameLabel.getHeight() + BindingLabelLayout.PADDING;
		
		if (nameLabel.getX() != name_x || nameLabel.getY() != name_y
				|| bindingLabel.getX() != binding_x || bindingLabel.getY() != binding_y) {
			nameLabel.setX(name_x);
			nameLabel.setY(name_y);
			bindingLabel.setX(binding_x);
			bindingLabel.setY(binding_y);
			
			// also re-place the children...
			int x = name_x + nameLabel.getWidth();
			for (Shape child : shapePE.getChildren()) {
				// re-locate the child nodes (parameters)
				Size preferredChildSize = determineChildSize(child);
				
				int y = parentLabels_y;
				if (parentLabelsHeight > preferredChildSize.getHeight()) {
					y += (int) Math.round((parentLabelsHeight - preferredChildSize.getHeight())/2d);
				}
				
				GraphicsAlgorithm childGA = child.getGraphicsAlgorithm();
				childGA.setX(x);
				childGA.setY(y);
				
				x += preferredChildSize.getWidth() + GraphicsAlgorithmsFactory.PADDING_SMALL;
			}
			if (shapePE.getChildren().size() > 0) {
				x -= GraphicsAlgorithmsFactory.PADDING_SMALL;
			}
			
			//... and the suffix label
			if (suffixLabel.getX() != x || suffixLabel.getY() != name_y) {
				suffixLabel.setX(x);
				suffixLabel.setY(name_y);
			}
			
			return true;
		}
		
		return false;
	}
	
}
