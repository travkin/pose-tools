/**
 * 
 */
package de.upb.pose.mapping.ui;

import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.mapping.ui.mapping.SetFragmentPEMapping;
import de.upb.pose.specification.SetFragment;

/**
 * @author Dietrich Travkin
 */
public class MappingLabelCreator {

	public static String getSetElementLabelText(SetFragment setFragment, ContainerShape setFragmentPE) {
		int currentSetElementIndex = SetFragmentPEMapping.get().getCurrentlyVisibleSetFragmentInstanceIndex(setFragmentPE);
		int numberOfAllSetElements = SetFragmentPEMapping.get().getNumberOfSetFragmentInstances(setFragmentPE);
		
		StringBuilder builder = new StringBuilder();
		builder.append(setFragment.getName());
		builder.append(" (");
		builder.append(currentSetElementIndex);
		builder.append(" of ");
		builder.append(numberOfAllSetElements);
		builder.append(')');

		return builder.toString();
	}
	
}
