package de.upb.pose.mapping.ui.editor.features.custom;

import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.ui.MappingImageProvider;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelElementCreator;
import de.upb.pose.specification.DesignElement;

public class MapElementFeature extends AbstractMapUnmapElementFeature {
	
	private DesignElement tmpSelectedRole = null;
	private EObject[] tmpSelectedModelElements = null;
	
	public MapElementFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public void execute(ICustomContext context) {
		PictogramElement pe = context.getPictogramElements()[0];
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);

		EObject[] selectedElements = this.getSelectedEcoreModelElements(context);
				
		// do the mapping
		for (EObject selectedElement: selectedElements) {
			roleBinding.getModelElements().add(selectedElement);
		}

		// update the role binding name
		roleBinding.setName(MappingNameCreator.getNameFor(roleBinding));
		
		// update the application model element name
		String mappedElementName = ApplicationModelElementCreator.getApplicationModelElementName(roleBinding);
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement != null) {
			applicationModelElement.setName(mappedElementName);
		}
		
		updatePictogramElement(pe);
	}
	
	@Override
	public String getImageId() {
		return MappingImageProvider.MAP;
	}

	@Override
	public String getName() {
		String text = getToolTipText();
		if (text == null) {
			text = super.getName();
		}
		
		return text;
	}
	
	private String getToolTipText() {
		if (this.tmpSelectedRole != null
				&& this.tmpSelectedModelElements != null
				&& this.tmpSelectedModelElements.length > 0) {
			
			StringBuilder toolTipBuilder = new StringBuilder();
			toolTipBuilder.append("Bind pattern role '");
			toolTipBuilder.append(this.tmpSelectedRole.getName());
			toolTipBuilder.append("' to ");

			for (int i = 0; i < this.tmpSelectedModelElements.length; i++) {
				EObject selected = this.tmpSelectedModelElements[i];
				
				if (i > 0) {
					toolTipBuilder.append(", ");
				}
				toolTipBuilder.append(selected.eClass().getName());
				if (selected instanceof ENamedElement) {
					String name = ((ENamedElement) selected).getName();
					if (name != null && !name.isEmpty()) {
						toolTipBuilder.append(' ');
						toolTipBuilder.append('\'');
						toolTipBuilder.append(name);
						toolTipBuilder.append('\'');
					}
				}
			}
			return toolTipBuilder.toString();
		}
		return "Bind selected pattern role and model elements to each other";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		boolean canExecute = false;
		DesignElement selectedPatternRole = this.getSelectedPatternRole(context);
		EObject[] selectedElements = this.getSelectedEcoreModelElements(context);
		if (selectedPatternRole != null && selectedElements.length > 0) {
			boolean canMapAllSelectedElements = true;
			for (EObject selectedElement: selectedElements) {
				canMapAllSelectedElements = canMapAllSelectedElements
						&& RoleMapper.canMap(selectedPatternRole, selectedElement);
			}
			canExecute = canMapAllSelectedElements;
		}
		
		if (canExecute) {
			this.tmpSelectedRole = selectedPatternRole;
			this.tmpSelectedModelElements = selectedElements;
		} else {
			this.tmpSelectedRole = null;
			this.tmpSelectedModelElements = null;
		}
		
		return canExecute;
	}
}
