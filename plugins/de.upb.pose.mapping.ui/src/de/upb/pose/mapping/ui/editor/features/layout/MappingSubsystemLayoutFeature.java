/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.mapping.ui.editor.features.update.MappingSubsystemUpdateFeature;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.ui.editor.features.layout.SubsystemLayoutFeature;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public class MappingSubsystemLayoutFeature extends SubsystemLayoutFeature {

	public MappingSubsystemLayoutFeature(RoleMappingEditorFeatureProvider fp)
	{
		super(fp);
	}
	
	/**
	 * @see de.upb.pose.modeling.classes.views.mapping.editor.features.DelegatingLayoutFeature#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context)
	{
		Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
		return super.canLayout(context)
				&& (bo instanceof Subsystem)
				&& (RolePEMapping.get().getRoleBinding(context.getPictogramElement()) != null)
				&& (context.getPictogramElement() instanceof ContainerShape);
	}
	
	/**
	 * @see de.upb.pose.modeling.classes.views.mapping.editor.features.DelegatingShapeLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context)
	{
		Size nameLabelSize = super.determineMinimalShapeContentsSize(context);
		
		PictogramElement pe = context.getPictogramElement();
		int numLabels = getNumberOfBindingLabels(pe);
		
		if (numLabels == 0) {
			return nameLabelSize;
		}
		
		Size minSize = nameLabelSize;
		for (int i = 0; i < numLabels; i++) {
			Text currentBindingLabel = getBindingLabel(pe, i);
			Size bindingLabelSize = GS.getSize(currentBindingLabel).padding(GraphicsAlgorithmsFactory.PADDING_DEFAULT, 0);
			minSize = Size.asRows(minSize, bindingLabelSize);
		}
		
		return minSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#resizeShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean anythingChanged = super.resizeShapeContents(context);
		
		PictogramElement pe = context.getPictogramElement();
		int numLabels = getNumberOfBindingLabels(pe);
		
		// resize binding labels
		for (int i = 0; i < numLabels; i++) {
			Text currentBindingLabel = getBindingLabel(pe, i);
			Size preferredBindingLabelSize = GS.getSize(currentBindingLabel).padding(GraphicsAlgorithmsFactory.PADDING_DEFAULT, 0);

			currentBindingLabel.setWidth(preferredBindingLabelSize.getWidth());
			currentBindingLabel.setHeight(preferredBindingLabelSize.getHeight());
			
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.SubsystemLayoutFeature#relocateShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = super.relocateShapeContents(context);
		
		PictogramElement pe = context.getPictogramElement();
		int numLabels = getNumberOfBindingLabels(pe);
		
		Text nameLabel = getLabel(context);
		
		// relocate binding labels
		int y = nameLabel.getY() + nameLabel.getHeight();
		for (int i = 0; i < numLabels; i++) {
			Text currentBindingLabel = getBindingLabel(pe, i);

			currentBindingLabel.setY(y);
			currentBindingLabel.setX(0);
			
			y += currentBindingLabel.getHeight();
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.editor.features.layout.DelegatingShapeLayoutFeature#canHandle(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (labelIdentifier != null
				&& labelIdentifier.startsWith(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME)) {
			return true;
		}
		return super.canHandle(labelIdentifier, pictogramElement);
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.editor.features.layout.DelegatingShapeLayoutFeature#getLabel(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement)
	{
		if (labelIdentifier != null && labelIdentifier.startsWith(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME)) {
			String indexText = labelIdentifier.substring(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.length());
			try {
				int index = Integer.parseInt(indexText);
				return getBindingLabel(pictogramElement, index);
			}
			catch (NumberFormatException e) {
				return null;
			}
		}
		
		return super.getLabel(labelIdentifier, pictogramElement);
	}
	
	protected Text getBindingLabel(PictogramElement pictogramElement, int index) {
		MappingSubsystemUpdateFeature UpdateFeature = getUpdateFeature(pictogramElement);
		if (UpdateFeature != null) {
			return UpdateFeature.getBindingLabel(pictogramElement, index);
		}
		return null;
	}
	
	protected int getNumberOfBindingLabels(PictogramElement pictogramElement) {
		MappingSubsystemUpdateFeature UpdateFeature = getUpdateFeature(pictogramElement);
		if (UpdateFeature != null) {
			return UpdateFeature.getNumberOfBindingLabels(pictogramElement);
		}
		return 0;
	}
	
	private MappingSubsystemUpdateFeature getUpdateFeature(PictogramElement pictogramElement) {
		IUpdateFeature feature = this.getFeatureProvider().getUpdateFeature(new UpdateContext(pictogramElement));
		if (feature instanceof MappingSubsystemUpdateFeature) {
			MappingSubsystemUpdateFeature roleUpdateFeature = (MappingSubsystemUpdateFeature) feature;
			return roleUpdateFeature;
		}
		return null;
	}
	
}
