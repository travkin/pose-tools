/**
 * 
 */
package de.upb.pose.mapping.ui.editor.multipage;

import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.upb.pose.core.ui.CoreUiImages;
import de.upb.pose.core.ui.editor.MultiPageDiagramEditorOverviewPage;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingImages;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.PatternApplicationDiagrams2ModelConnector;

/**
 * @author Dietrich Travkin
 */
public class MultiPagePatternRoleMappingDiagramOverviewPage extends MultiPageDiagramEditorOverviewPage {

	private static final int OFFSET = 5;
	
	private TreeViewer viewer;
	
	private Action openMappingDiagramAction;
	
	/**
	 * @param editor
	 */
	public MultiPagePatternRoleMappingDiagramOverviewPage(MultiPagePatternRoleMappingDiagramEditor editor) {
		super(editor);
		
		ImageDescriptor icon = CoreUiImages.getDescriptor(CoreUiImages.FIND);
		openMappingDiagramAction = new Action("Open Mapping Diagram", icon) {
			@Override
			public void run() {
				handleOpen();
			}
		};
		openMappingDiagramAction.setEnabled(false);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		if (this.viewer != null && !viewer.getControl().isDisposed()) {
			this.viewer.getControl().dispose();
		}
	}
	
	@Override
	public void setFocus() {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.getControl().setFocus();
		}
	}

	@Override
	public void refresh() {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.refresh();
		}
	}
	
	@Override
	protected void selectionChanged(ISelection selection) {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			//viewer.setSelection(selection, true);
			
			Object selected = this.getSelectedElement(selection);
			if (selected != null) {
				if (selected instanceof Diagram || selected instanceof AppliedPattern) {
					this.openMappingDiagramAction.setEnabled(true);
					return;
				}
			}
			
			this.openMappingDiagramAction.setEnabled(false);
		}
	}
	
	private Object getSelectedElement(ISelection selection) {
		if (selection != null && !selection.isEmpty()
				&& selection instanceof IStructuredSelection
				&& ((IStructuredSelection) selection).size() == 1) {
			return ((IStructuredSelection) selection).getFirstElement();
		}
		return null;
	}
	
	@Override
	protected String getHeaderText() {
		return "Design Pattern Applications (Pattern Role Mapping)";
	}
	
	@Override
	protected Image getHeaderImage() {
		return MappingImages.get(MappingPackage.Literals.PATTERN_APPLICATIONS);
	}

	/**
	 * @see de.upb.pose.core.ui.editor.MultiPageDiagramEditorOverviewPage#createWidgets(org.eclipse.ui.forms.widgets.FormToolkit, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createWidgets(FormToolkit toolkit, Composite parent) {
		// tree viewer
		Tree tree = toolkit.createTree(parent, SWT.NONE);
		FormData treeData = new FormData();
		treeData.left = new FormAttachment(0, OFFSET);
		treeData.right = new FormAttachment(100, -OFFSET);
		treeData.top = new FormAttachment(0, OFFSET);
		treeData.bottom = new FormAttachment(100, -OFFSET);
		tree.setLayoutData(treeData);

		viewer = new TreeViewer(tree);
		viewer.setContentProvider(new MultiPagePatternRoleMappingDiagramEditorContentProvider(getAdapterFactory()));
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(getAdapterFactory()));
		viewer.setAutoExpandLevel(2);
		viewer.setInput(getRootElement());

		setSelectionProvider(viewer);
		viewer.addSelectionChangedListener(this);
		
		viewer.getTree().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				selectionChanged(new SelectionChangedEvent(viewer, viewer.getSelection()));
			}
		});
		
		// toggle a selected element's expand state on double click
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object selected = ((IStructuredSelection) event.getSelection()).getFirstElement();

				if (selected instanceof Diagram) {
					handleOpen();
				} else {
					boolean expanded = viewer.getExpandedState(selected);
					viewer.setExpandedState(selected, !expanded);
				}
			}
		});

	}
	
	private void handleOpen() {
		Object selected = this.getSelectedElement(this.viewer.getSelection());
		
		Diagram diagramToOpen = null;
		if (selected instanceof AppliedPattern) {
			diagramToOpen = PatternApplicationDiagrams2ModelConnector.findDiagramFor((AppliedPattern) selected);
		} else if (selected instanceof Diagram) {
			diagramToOpen = (Diagram) selected;
		}
		
		if (diagramToOpen != null) {
			openDiagram(diagramToOpen);
		}
	}

}
