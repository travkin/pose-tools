/**
 * 
 */
package de.upb.pose.mapping.ui.mapping;

import java.util.ArrayList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.mm.Property;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.ui.editor.features.BusinessObjectFinder;


/**
 * @author Dietrich Travkin
 */
public class SetFragmentPEMapping {

	private static final String PE_PROPERTY_CURRENT_SET_INSTANCE = "index"; //$NON-NLS-1$
	
	private static SetFragmentPEMapping instance = null;
	
	private SetFragmentPEMapping() {}
	
	public static SetFragmentPEMapping get() {
		if (instance == null) {
			instance = new SetFragmentPEMapping();
		}
		return instance;
	}
	
	public SetFragmentBinding getSetFragmentBinding(PictogramElement setFragmentPE)
	{
		return BusinessObjectFinder.get().getFirstBusinessObjectOfType(setFragmentPE, SetFragmentBinding.class);
	}

	/**
	 * Returns the set element binding that is currently displayed in the mapping editor for the given set fragment's pictrogram element.
	 * 
	 * @param setFragmentPE the pictrogram element of the set fragment
	 * @return the currently visible set element binding
	 */
	public SetFragmentInstance getCurrentlyVisibleSetFragmentInstance(PictogramElement setFragmentPE)
	{
		int index = getCurrentlyVisibleSetFragmentInstanceIndex(setFragmentPE);
		return getSetFragmentInstanceWithIndex(setFragmentPE, index);
	}
	
	/**
	 * Counts the number of all available set element bindings for the given set fragment's pictrogram element.
	 * 
	 * @param pe pictogram element for a set fragment
	 * @return number of set element bindings for the given representation of a set fragment
	 */
	public int getNumberOfSetFragmentInstances(PictogramElement setFragmentPE)
	{
		int count = 0;
		for (EObject object: setFragmentPE.getLink().getBusinessObjects())
		{
			if (object instanceof SetFragmentInstance)
				count++;
		}
		return count;
	}
	
	public SetFragmentInstance getSetFragmentInstanceWithIndex(PictogramElement setFragmentPE, int index)
	{
		int currentIndex = 0;
		for (EObject object: setFragmentPE.getLink().getBusinessObjects())
		{
			if (object instanceof SetFragmentInstance)
			{
				currentIndex++;
				if (currentIndex == index)
				{
					return (SetFragmentInstance) object;
				}
			}
		}
		return null;
	}
	
	public SetFragmentInstance removeSetFragmentInstanceWithIndex(PictogramElement setFragmentPE, int index)
	{
		SetFragmentInstance setElementBindingToRemove = getSetFragmentInstanceWithIndex(setFragmentPE, index);
		
		// remove all contained role bindings
		ArrayList<RoleBinding> roleBindings = new ArrayList<RoleBinding>(setElementBindingToRemove.getContainedRoleBindings()); // avoids concurrent modification exception
		for (RoleBinding roleBinding: roleBindings)
		{
			// TODO remove the pictogram element, if necessary
			EcoreUtil.delete(roleBinding);
		}
		
		setFragmentPE.getLink().getBusinessObjects().remove(setElementBindingToRemove);
		EcoreUtil.delete(setElementBindingToRemove);
		
		return setElementBindingToRemove;
	}
	
	public int getCurrentlyVisibleSetFragmentInstanceIndex(PictogramElement setFragmentPE)
	{
		Property property = Graphiti.getPeService().getProperty(setFragmentPE, PE_PROPERTY_CURRENT_SET_INSTANCE);
		if(property != null) {
			return Integer.parseInt(property.getValue());
		}
		return -1;
	}
	
	public void setCurrentlyVisibleSetFragmentInstanceIndex(PictogramElement setFragmentPE, int index)
	{
		Graphiti.getPeService().setPropertyValue(setFragmentPE, PE_PROPERTY_CURRENT_SET_INSTANCE, String.valueOf(index));
	}
	
}
