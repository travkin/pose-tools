/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.editor.features.custom.ApplyPatternFeature;
import de.upb.pose.mapping.ui.editor.features.custom.MapElementFeature;
import de.upb.pose.mapping.ui.editor.features.custom.ValidateMappingFeature;
import de.upb.pose.mapping.ui.editor.features.layout.ClassMappingLabelConstants;
import de.upb.pose.mapping.ui.editor.features.layout.MappingActionLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingAttributeLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingOperationLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingParameterLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingReferenceLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingResultVariableLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingSubsystemLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.layout.MappingTypeLayoutFeature;
import de.upb.pose.mapping.ui.editor.features.update.DelegatingRoleBindingUpdateFeature;
import de.upb.pose.mapping.ui.editor.features.update.MappingSubsystemUpdateFeature;
import de.upb.pose.mapping.ui.editor.features.update.SetBindingUpdateFeature;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature;
import de.upb.pose.specification.ui.editor.features.update.IShapeUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.SubsystemUpdateFeature;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class RoleMappingEditorFeatureProvider extends SpecificationEditorFeatureProvider
{
	public RoleMappingEditorFeatureProvider(IDiagramTypeProvider dtp)
	{
		super(dtp);
	}
	
	@Override
	public ICustomFeature[] getCustomFeatures(ICustomContext context)
	{
		if (context.getPictogramElements().length == 1)
		{
			Collection<ICustomFeature> features = new ArrayList<ICustomFeature>();

			PictogramElement pe = context.getPictogramElements()[0];
			Object patternElement = getBusinessObjectForPictogramElement(pe);
			
			// TODO is this really used?
			// map element
			if (pe.getLink().getBusinessObjects().size() == 1) {
				ICustomFeature feature = new MapElementFeature(this);
				if (feature.canExecute(context)) {
					features.add(feature);
				}
			}

			if (patternElement instanceof PatternSpecification)
			{
				// apply pattern
				features.add(new ApplyPatternFeature(this));
				
				// validate mapping
				features.add(new ValidateMappingFeature(this));
			}

			return features.toArray(new ICustomFeature[features.size()]);
		}
		return super.getCustomFeatures(context);
	}
	
	@Override
	public IUpdateFeature getUpdateFeature(IUpdateContext context)
	{
		PictogramElement pe = context.getPictogramElement();
		Object patternElement = getBusinessObjectForPictogramElement(pe);
		
		if (patternElement instanceof DesignElement)
		{
			IShapeUpdateFeature wrappedUpdateFeature = null;
			IUpdateFeature updateFeature = super.getUpdateFeature(context);
			if (updateFeature != null)
			{
				if (!(updateFeature instanceof IShapeUpdateFeature))
				{
					throw new IllegalStateException("UpdateFeature for DesignElements of type "
							+ ((DesignElement) patternElement).eClass().getName() + " is not an IShapeUpdateFeature, it is a " + updateFeature.getClass().getName());
				}
				wrappedUpdateFeature = (IShapeUpdateFeature) updateFeature;
				
				if (updateFeature instanceof SubsystemUpdateFeature) {
					return new MappingSubsystemUpdateFeature(wrappedUpdateFeature, this);
				}
			}
			else if (patternElement instanceof SelfVariable || patternElement instanceof NullVariable) {
				// ignore this type since it is not visualized
				return super.getUpdateFeature(context);	
			}
			else
			{
				throw new IllegalArgumentException("Unsupported design element: " +((DesignElement) patternElement).eClass().getName());
			}
			
			return new DelegatingRoleBindingUpdateFeature(wrappedUpdateFeature, this);
		}
		else if (patternElement instanceof SetFragment)
		{
			return new SetBindingUpdateFeature(super.getUpdateFeature(context), this);
		}
		
		// TODO add update features for tasks, etc.
		 
		return super.getUpdateFeature(context);
	}
	
	@Override
	public ILayoutFeature getLayoutFeature(ILayoutContext context)
	{
		PictogramElement pe = context.getPictogramElement();
		Object patternElement = getBusinessObjectForPictogramElement(pe);

		if (patternElement instanceof Type)
		{
			return new MappingTypeLayoutFeature(this);
		}
		else if (patternElement instanceof Operation)
		{
			return new MappingOperationLayoutFeature(this);
		}
		else if (patternElement instanceof Reference)
		{
			return new MappingReferenceLayoutFeature(this);
		}
		else if (patternElement instanceof Attribute)
		{
			return new MappingAttributeLayoutFeature(this);
		}
		else if (patternElement instanceof Action)
		{
			return new MappingActionLayoutFeature(this);
		}
		else if (patternElement instanceof Subsystem)
		{
			return new MappingSubsystemLayoutFeature(this);
		}
		else if (patternElement instanceof Parameter) {
			return new MappingParameterLayoutFeature(this);
		}
		else if (patternElement instanceof ResultVariable) {
			return new MappingResultVariableLayoutFeature(this);
		}
		
		return super.getLayoutFeature(context);
	}
	
	@Override
	public IDirectEditingFeature getDirectEditingFeature(IDirectEditingContext context)
	{
		PictogramElement pe = context.getPictogramElement();		
		Object bo = getBusinessObjectForPictogramElement(pe);
		if (bo instanceof DesignElement)
		{
			RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(pe);
			if (roleBinding != null && roleBinding.getModelElements().isEmpty())
			{
				ILayoutFeature layoutFeature = getLayoutFeature(new LayoutContext(context.getPictogramElement()));
				if (layoutFeature != null && layoutFeature instanceof IShapeWithLabelLayoutFeature)
				{
					IShapeWithLabelLayoutFeature labelProvidingLayoutFeature = (IShapeWithLabelLayoutFeature) layoutFeature;
					if (labelProvidingLayoutFeature.getLabel(ClassMappingLabelConstants.LABEL_ID_BINDING_NAME, pe) != null)
					{
						return new NewElementNameDirectEditingFeature(this);
					}
				}
			}
		}
		return null;
	}
	
	@Override
	public IAddFeature getAddFeature(IAddContext context)
	{
		return null;
	}
}
