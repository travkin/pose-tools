package de.upb.pose.mapping.ui.dialogs;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

import de.upb.pose.core.ui.CoreUiImages;
import de.upb.pose.core.ui.dialogs.LoadResourceDialog;
import de.upb.pose.core.util.DisplayUtil;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.PatternSpecificationConstants;

public class SelectPatternSpecificationDialog extends TitleAreaDialog {
	
	private static final int LOAD_ID = 3;

	private LoadResourceDialog dialog;
	private AdapterFactory adapterFactory;

	private ResourceSet resourceSet;

	private TreeViewer viewer;
	private PatternSpecification specification;
	
	private static final String DIALOG_TITLE = "Select a design pattern's specification to be applied.";

	public SelectPatternSpecificationDialog(ResourceSet resourceSet, Resource ecoreModelResource) {
		super(DisplayUtil.getShell());

		this.resourceSet = resourceSet;
		
		setHelpAvailable(false);
		setShellStyle(SWT.MAX);

		dialog = new LoadResourceDialog();
		this.setInitialSelectionInLoadResourceDialog(ecoreModelResource);
		dialog.setFileExtensions(new String[]{PatternSpecificationConstants.DIAGRAMS_FILE_EXTENSION});
		dialog.setTitle("Load Design Pattern Specifications from Workspace");
		adapterFactory = new ComposedAdapterFactory(Registry.INSTANCE);
	}

	private void setInitialSelectionInLoadResourceDialog(Resource ecoreModelResource) {
		if (ecoreModelResource != null) {
			String path = ecoreModelResource.getURI().toPlatformString(true);
			IResource file = ResourcesPlugin.getWorkspace().getRoot().findMember(path);
			if (file != null && file.exists() && file instanceof IFile) {
				IContainer parent = ((IFile) file).getParent();
				if (parent != null && parent.exists()) {
					this.dialog.setInitialSelection(parent);
				}
			}
		}
	}
	
	public PatternSpecification getSpecification() {
		return specification;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle(DIALOG_TITLE);

		Composite area = new Composite((Composite) super.createDialogArea(parent), SWT.NONE);
		GridLayoutFactory.fillDefaults().margins(6, 6).applyTo(area);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(area);

		ScrolledComposite scrolledComposite = new ScrolledComposite(area, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(scrolledComposite);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		viewer = new TreeViewer(scrolledComposite, SWT.SINGLE);
		Tree tree = viewer.getTree();
		scrolledComposite.setContent(tree);
		scrolledComposite.setMinSize(tree.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		viewer.setContentProvider(new SelectPatternSpecificationDialogContentProvider());
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		viewer.setInput(resourceSet);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object selected = ((IStructuredSelection) viewer.getSelection()).getFirstElement();

				if (selected instanceof PatternSpecification) {
					specification = (PatternSpecification) selected;
				} else {
					specification = null;
				}

				// toggle OK button state
				getButton(IDialogConstants.OK_ID).setEnabled(specification != null);
			}
		});

		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				if (selection.size() == 1) {
					Object element = selection.getFirstElement();

					if (element instanceof PatternSpecification) {
						// open element
						close();
					} else {
						// expand element
						boolean state = viewer.getExpandedState(element);
						viewer.setExpandedState(element, !state);
					}
				}
			}
		});

		return area;
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (LOAD_ID == buttonId) {
			if (dialog.open() == Window.OK) {
				URI uri = URI.createPlatformResourceURI(dialog.getElement().getFullPath().toString(), true);
				resourceSet.getResource(uri, true);
				viewer.setInput(resourceSet);
			}
		}
		super.buttonPressed(buttonId);
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		GridLayout parentLayout = (GridLayout) parent.getLayout();
		GridDataFactory.fillDefaults().applyTo(parent);
		parentLayout.numColumns = 2;
		parentLayout.makeColumnsEqualWidth = false;

		// left part
		Composite left = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(0).applyTo(left);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(left);

		Button loadButton = createButton(left, LOAD_ID, "Load Specifications...", false);
		loadButton.setImage(CoreUiImages.get(CoreUiImages.FIND));
		GridDataFactory.fillDefaults().align(SWT.LEAD, SWT.FILL).applyTo(loadButton);

		// right part
		Composite right = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().numColumns(0).applyTo(right);
		GridDataFactory.fillDefaults().grab(false, false).applyTo(right);

		Button okButton = createButton(right, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		okButton.setEnabled(false);

		createButton(right, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);

		newShell.setText("Select a design pattern for application");
	}
}
