/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.update;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class DelegatingUpdateFeature implements IUpdateFeature
{	
	private IUpdateFeature wrappedFeature;
	private RoleMappingEditorFeatureProvider featureProvider;
	
	public DelegatingUpdateFeature(IUpdateFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		this.wrappedFeature = featureToBeWrapped;
		this.featureProvider = fp;
	}
	
	protected IUpdateFeature getWrappedFeature()
	{
		return wrappedFeature;
	}
	
	protected Object getBusinessObjectForPictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().getBusinessObjectForPictogramElement(pe);
	}
	
	protected Color manageColor(IColorConstant colorConstant)
	{
		return Graphiti.getGaService().manageColor(getDiagram(), colorConstant);
	}
	
	protected Diagram getDiagram()
	{
		return getFeatureProvider().getDiagramTypeProvider().getDiagram();
	}
	
	protected IReason layoutPictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().layoutIfPossible(new LayoutContext(pe));
	}
	
	protected IReason updatePictogramElement(PictogramElement pe)
	{
		return getFeatureProvider().updateIfPossible(new UpdateContext(pe));
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IUpdate#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context)
	{
		return wrappedFeature.canUpdate(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context)
	{
		return wrappedFeature.updateNeeded(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context)
	{
		return wrappedFeature.update(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#isAvailable(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean isAvailable(IContext context)
	{
		return wrappedFeature.isAvailable(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#canExecute(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean canExecute(IContext context)
	{
		boolean ret = false;
		if (context instanceof IUpdateContext)
		{
			IUpdateContext updateSemanticsContext = (IUpdateContext) context;
			ret = canUpdate(updateSemanticsContext);
		}
		return ret;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#execute(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public void execute(IContext context)
	{
		if (context instanceof IUpdateContext)
		{
			update((IUpdateContext) context);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#canUndo(org.eclipse.graphiti.features.context.IContext)
	 */
	@Override
	public boolean canUndo(IContext context)
	{
		return wrappedFeature.canUndo(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeature#hasDoneChanges()
	 */
	@Override
	public boolean hasDoneChanges()
	{
		return wrappedFeature.hasDoneChanges();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.IName#getName()
	 */
	@Override
	public String getName()
	{
		return wrappedFeature.getName();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.IDescription#getDescription()
	 */
	@Override
	public String getDescription()
	{
		return wrappedFeature.getDescription();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.IFeatureProviderHolder#getFeatureProvider()
	 */
	@Override
	public RoleMappingEditorFeatureProvider getFeatureProvider()
	{
		return this.featureProvider;
	}
}