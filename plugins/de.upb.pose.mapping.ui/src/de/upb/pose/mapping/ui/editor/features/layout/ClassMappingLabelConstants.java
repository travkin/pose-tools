/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

/**
 * @author Dietrich Travkin
 */
public interface ClassMappingLabelConstants {

	public static final String LABEL_ID_BINDING_NAME = "binding";
	
}
