/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Size;
import de.upb.pose.core.util.Area.Position;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.ui.editor.features.layout.ReferenceNodeLayoutFeature;

/**
 * @author Dietrich Travkin
 *
 */
public class MappingReferenceLayoutFeature extends ReferenceNodeLayoutFeature
{
	public MappingReferenceLayoutFeature(RoleMappingEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return (pictogramElement instanceof ContainerShape);
		}		
		return super.canHandle(labelIdentifier, pictogramElement);
	}

	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return getBindingLabel(pictogramElement);
		}
		return super.getLabel(labelIdentifier, pictogramElement);
	}
	
	protected Text getBindingLabel(PictogramElement pictogramElement) {
		return BindingLabelLayout.getLabelAtIndex((ContainerShape) pictogramElement, 1);
	}
	
	protected Text getBindingLabel(ILayoutContext context) {
		return getBindingLabel(context.getPictogramElement());
	}
	
	protected Size determinePreferredBindingLabelSize(ILayoutContext context) {
		Text bindingLabel = getBindingLabel(context);
		if (bindingLabel == null) {
			return null;
		}
		return determinePreferredLabelSize(bindingLabel);
	}
	
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context)
	{
		Size nameLabelSize = determinePreferredLabelSize(context);
		Size bindingLabelSize = determinePreferredBindingLabelSize(context);
		Size preferredShapeSize = BindingLabelLayout.determineMinimalShapeContentsSize(
				nameLabelSize, bindingLabelSize);
		return preferredShapeSize;
	}
	
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean sizeChanged = super.resizeShapeContents(context);
		
		Text bindingLabel = getBindingLabel(context);
		Size preferredLabelSize = determinePreferredLabelSize(bindingLabel);
		
		// resize label
		if (bindingLabel != null
				&& (bindingLabel.getWidth() != preferredLabelSize.getWidth()
				|| bindingLabel.getHeight() != preferredLabelSize.getHeight())) {
			bindingLabel.setWidth(preferredLabelSize.getWidth());
			bindingLabel.setHeight(preferredLabelSize.getHeight());
			sizeChanged = true;
		}
		return sizeChanged;
	}
	
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		Rectangle invisibleParent = getInvisibleParentGA(context);
		Text nameLabel = getLabel(context);
		Text bindingLabel = getBindingLabel(context);
		
		if (bindingLabel == null) {
			return super.resizeShapeContents(context);
		}
		
		int bothLabelsHeight = nameLabel.getHeight() + bindingLabel.getHeight() + BindingLabelLayout.PADDING;
		
		int x_center_name = (int) Math.round(invisibleParent.getWidth() / 2d - nameLabel.getWidth() / 2d);
		int x_center_binding = (int) Math.round(invisibleParent.getWidth() / 2d - bindingLabel.getWidth() / 2d);
		int y_center = (int) Math.round(invisibleParent.getHeight() / 2d - bothLabelsHeight / 2d);
		
		int name_x, binding_x, name_y, binding_y;
		Position pos = determineReferenceLabelPosition(context);
		
		switch (pos) {
		case RIGHT:
			name_x = 0;
			binding_x = name_x;
			name_y = y_center;
			binding_y = y_center + nameLabel.getHeight() + BindingLabelLayout.PADDING;
			break;
		case LEFT:
			name_x = ARROW_LENGTH;
			binding_x = name_x;
			name_y = y_center;
			binding_y = y_center + nameLabel.getHeight() + BindingLabelLayout.PADDING;
			break;
		case TOP:
			name_x = x_center_name;
			binding_x = x_center_binding;
			name_y = ARROW_LENGTH;
			binding_y = name_y + nameLabel.getHeight() + BindingLabelLayout.PADDING;
			break;
		case BOTTOM:
			name_x = x_center_name;
			binding_x = x_center_binding;
			name_y = 0;
			binding_y = name_y + nameLabel.getHeight() + BindingLabelLayout.PADDING;
			break;
		default:
			name_x = 0;
			binding_x = 0;
			name_y = 0;
			binding_y = 0;
			break;
		}
		
		if (nameLabel.getX() != name_x || nameLabel.getY() != name_y
				|| bindingLabel.getX() != binding_x || bindingLabel.getY() != binding_y) {
			nameLabel.setX(name_x);
			nameLabel.setY(name_y);
			bindingLabel.setX(binding_x);
			bindingLabel.setY(binding_y);
			return true;
		}
		
		return false;
	}
	
}
