/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.mapping.ui.editor.features.update.DelegatingRoleBindingUpdateFeature;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public class DelegatingRoleBindingLayoutFeature extends DelegatingShapeLayoutFeature
{
	public DelegatingRoleBindingLayoutFeature(IShapeWithLabelLayoutFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	/**
	 * @see de.upb.pose.modeling.classes.views.mapping.editor.features.DelegatingLayoutFeature#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context)
	{
		Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
		return super.canLayout(context)
				&& (bo instanceof DesignElement)
				&& (RolePEMapping.get().getRoleBinding(context.getPictogramElement()) != null)
				&& (context.getPictogramElement() instanceof ContainerShape
						|| context.getPictogramElement() instanceof Connection);
	}
	
	/**
	 * @see de.upb.pose.modeling.classes.views.mapping.editor.features.DelegatingShapeLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context)
	{
		Text bindingLabel = getBindingLabel(context.getPictogramElement());
		
		Size nameLabelSize = super.determineMinimalShapeContentsSize(context);
		
		if (bindingLabel == null)
		{
			return nameLabelSize;
		}
		
		Size bindingLabelSize = GS.getSize(bindingLabel).padding(GraphicsAlgorithmsFactory.PADDING_DEFAULT, GraphicsAlgorithmsFactory.PADDING_DEFAULT/2);
		Size minSize = Size.asRows(nameLabelSize, bindingLabelSize);
		
		return minSize;
	}
	
	@Override
	public boolean layoutShapeContents(ILayoutContext context)
	{	
		boolean success = super.layoutShapeContents(context);
		
		// resize binding label
		Text bindingLabel = getBindingLabel(context.getPictogramElement());
		if (bindingLabel != null)
		{
			Text nameLabel = getLabel(context);
			Size nameSize = GS.getSize(nameLabel);
			Size size = GS.getSize(bindingLabel);
			GraphicsAlgorithm parent = bindingLabel.getParentGraphicsAlgorithm();
			
			bindingLabel.setWidth(size.getWidth());
			bindingLabel.setHeight(size.getHeight());
			bindingLabel.setY(nameLabel.getY() + nameSize.getHeight() + GraphicsAlgorithmsFactory.PADDING_DEFAULT);
			bindingLabel.setX(parent.getWidth()/2 - bindingLabel.getWidth()/2);
		}
		
		return success;
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.editor.features.layout.DelegatingShapeLayoutFeature#canHandle(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return true;
		}
		return super.canHandle(labelIdentifier, pictogramElement);
	}
	
	/**
	 * @see de.upb.pose.mapping.ui.editor.features.layout.DelegatingShapeLayoutFeature#getLabel(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement)
	{
		if (ClassMappingLabelConstants.LABEL_ID_BINDING_NAME.equals(labelIdentifier)) {
			return getBindingLabel(pictogramElement);
		}
		
		return super.getLabel(labelIdentifier, pictogramElement);
	}
	
	protected Text getBindingLabel(PictogramElement pictogramElement) {
		IUpdateFeature feature = this.getFeatureProvider().getUpdateFeature(new UpdateContext(pictogramElement));
		if (feature instanceof DelegatingRoleBindingUpdateFeature) {
			DelegatingRoleBindingUpdateFeature roleUpdateFeature = (DelegatingRoleBindingUpdateFeature) feature;
			return roleUpdateFeature.getBindingLabel(pictogramElement);
		}
		return null;
	}
	
}
