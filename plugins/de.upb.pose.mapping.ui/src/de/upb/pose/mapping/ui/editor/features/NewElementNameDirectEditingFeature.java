/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelElementCreator;
import de.upb.pose.specification.DesignElement;

/**
 * @author Dietrich Travkin
 */
public class NewElementNameDirectEditingFeature extends AbstractDirectEditingFeature
{
	public NewElementNameDirectEditingFeature(IFeatureProvider fp)
	{
		super(fp);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IDirectEditing#getEditingType()
	 */
	@Override
	public int getEditingType()
	{
		return TYPE_TEXT;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature#stretchFieldToFitText()
	 */
	@Override
	public boolean stretchFieldToFitText()
	{
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature#canDirectEdit(org.eclipse.graphiti.features.context.IDirectEditingContext)
	 */
	@Override
	public boolean canDirectEdit(IDirectEditingContext context)
	{
		PictogramElement pe = context.getPictogramElement();		
		Object bo = getBusinessObjectForPictogramElement(pe);
		
		return (pe instanceof ContainerShape && bo instanceof DesignElement && RolePEMapping.get().getRoleBinding(pe) != null);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IDirectEditing#getInitialValue(org.eclipse.graphiti.features.context.IDirectEditingContext)
	 */
	@Override
	public String getInitialValue(IDirectEditingContext context)
	{
		return RolePEMapping.get().getRoleBinding(context.getPictogramElement()).getNewElementName();
	}
	
	@Override
	public void setValue(String value, IDirectEditingContext context)
	{
		RoleBinding roleBinding = RolePEMapping.get().getRoleBinding(context.getPictogramElement());
		roleBinding.setNewElementName(value);
		
		// update the role binding name
		roleBinding.setName(MappingNameCreator.getNameFor(roleBinding));

		// update the application model element name
		String mappedElementName = ApplicationModelElementCreator.getApplicationModelElementName(roleBinding);
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement != null) {
			applicationModelElement.setName(mappedElementName);
		}
		
		updatePictogramElement(context.getPictogramElement());
	}
}
