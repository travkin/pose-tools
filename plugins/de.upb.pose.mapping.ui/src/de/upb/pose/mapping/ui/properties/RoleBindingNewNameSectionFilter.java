package de.upb.pose.mapping.ui.properties;

import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.jface.viewers.IFilter;

import de.upb.pose.core.util.PropertiesUtil;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.ui.mapping.RolePEMapping;
import de.upb.pose.specification.actions.Action;

public class RoleBindingNewNameSectionFilter implements IFilter {
	
	@Override
	public boolean select(Object element) {
		PictogramElement pe = PropertiesUtil.getPictogramElement(element);

		if (pe != null) {
			RoleBinding binding = RolePEMapping.get().getRoleBinding(pe);

			return binding != null && binding.getModelElements().isEmpty() && !(binding.getRole() instanceof Action);
		}

		return false;
	}
}
