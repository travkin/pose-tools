/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.update;

import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.ui.editor.features.update.IShapeUpdateFeature;

/**
 * @author Dietrich Travkin
 */
public class DelegatingShapeUpdateFeature extends DelegatingUpdateFeature implements IShapeUpdateFeature
{
	public DelegatingShapeUpdateFeature(IShapeUpdateFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	@Override
	protected IShapeUpdateFeature getWrappedFeature()
	{
		return (IShapeUpdateFeature) super.getWrappedFeature();
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#getBackgroundColor(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public Color getBackgroundColor(IUpdateContext context)
	{
		return getWrappedFeature().getBackgroundColor(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#setBackgroundColor(org.eclipse.graphiti.features.context.IUpdateContext, org.eclipse.graphiti.util.IColorConstant)
	 */
	@Override
	public void setBackgroundColor(IUpdateContext context,
			IColorConstant newColor)
	{
		getWrappedFeature().setBackgroundColor(context, newColor);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#getLineStyle(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public LineStyle getLineStyle(IUpdateContext context)
	{
		return getWrappedFeature().getLineStyle(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#setLineStyle(org.eclipse.graphiti.features.context.IUpdateContext, org.eclipse.graphiti.mm.algorithms.styles.LineStyle)
	 */
	@Override
	public void setLineStyle(IUpdateContext context, LineStyle newLineStyle)
	{
		getWrappedFeature().setLineStyle(context, newLineStyle);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#getLabelText(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public String getLabelText(IUpdateContext context)
	{
		return getWrappedFeature().getLabelText(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#setLabelText(org.eclipse.graphiti.features.context.IUpdateContext, java.lang.String)
	 */
	@Override
	public void setLabelText(IUpdateContext context, String newText)
	{
		getWrappedFeature().setLabelText(context, newText);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#getLabelFont(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public Font getLabelFont(IUpdateContext context)
	{
		return getWrappedFeature().getLabelFont(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeUpdateFeature#setLabelFont(org.eclipse.graphiti.features.context.IUpdateContext, de.upb.pose.core.util.FontDescription)
	 */
	@Override
	public void setLabelFont(IUpdateContext context, FontDescription newFont)
	{
		getWrappedFeature().setLabelFont(context, newFont);
	}

}
