package de.upb.pose.mapping.ui.view;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;

@SuppressWarnings("deprecation")
public class DummyEditorSite implements IEditorSite {
	
	private IWorkbenchPartSite wbps;

	public DummyEditorSite(IWorkbenchPartSite wbps) {
		this.wbps = wbps;
	}

	@Override
	public String getId() {
		return wbps.getId();
	}

	@Override
	public String getPluginId() {
		return wbps.getPluginId();
	}

	@Override
	public String getRegisteredName() {
		return wbps.getRegisteredName();
	}

	@Override
	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider) {
		wbps.registerContextMenu(menuId, menuManager, selectionProvider);
	}

	@Override
	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider) {
		wbps.registerContextMenu(menuManager, selectionProvider);
	}

	@Override
	public IKeyBindingService getKeyBindingService() {
		return wbps.getKeyBindingService();
	}

	@Override
	public IWorkbenchPart getPart() {
		return wbps.getPart();
	}

	@Override
	public IWorkbenchPage getPage() {
		return wbps.getPage();
	}

	@Override
	public ISelectionProvider getSelectionProvider() {
		return wbps.getSelectionProvider();
	}

	@Override
	public Shell getShell() {
		return wbps.getShell();
	}

	@Override
	public IWorkbenchWindow getWorkbenchWindow() {
		return wbps.getWorkbenchWindow();
	}

	@Override
	public void setSelectionProvider(ISelectionProvider provider) {
		wbps.setSelectionProvider(provider);
	}

	@Override
	public Object getAdapter(@SuppressWarnings("rawtypes") Class adapter) {
		return wbps.getAdapter(adapter);
	}

	@Override
	public Object getService(@SuppressWarnings("rawtypes") Class api) {
		return wbps.getService(api);
	}

	@Override
	public boolean hasService(@SuppressWarnings("rawtypes") Class api) {
		return wbps.hasService(api);
	}

	@Override
	public IEditorActionBarContributor getActionBarContributor() {
		return null;
	}

	@Override
	public IActionBars getActionBars() {
		return null;
	}

	@Override
	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
	}

	@Override
	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
	}
}
