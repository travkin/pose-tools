package de.upb.pose.mapping.ui.editor;

import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.ui.editor.DefaultUpdateBehavior;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

public class RoleMappingEditor extends DiagramEditor {
	
	private Composite control;
	private final Diagram diagram;

	public RoleMappingEditor(Diagram diagram) {
		super();
		this.diagram = diagram;
	}
	
	/*package*/ Diagram getDiagram() {
		return this.diagram;
	}
	
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
	}
	
	@Override
	public void refreshTitle() {
		// not necessary due to embedded editor
	}

	@Override
	public void refreshTitleToolTip() {
		// not necessary due to embedded editor
	}

	@Override
	public void createPartControl(Composite parent) {
		control = new Composite(parent, SWT.NONE);
		control.setLayout(new FillLayout());

		super.createPartControl(control);
	}

	public Control getControl() {
		return control;
	}

	@Override
	protected DefaultUpdateBehavior createUpdateBehavior() {
		return new RoleMappingEditorUpdateBehavior(this);
	}

	@Override
	public void dispose() {
		super.dispose();
	}
	
}
