/**
 * 
 */
package de.upb.pose.mapping.ui.editor.multipage;

import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplicationDiagrams2ModelConnector;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.ui.editor.PatternAppicationsContentProvider;

/**
 * @author Dietrich Travkin
 */
public class MultiPagePatternRoleMappingDiagramEditorContentProvider extends AdapterFactoryContentProvider {

	private static final Object[] EMPTY_ARRAY = new Object[0];
	
	private PatternAppicationsContentProvider delegate;
	
	public MultiPagePatternRoleMappingDiagramEditorContentProvider(AdapterFactory af) {
		super(af);
		
		this.delegate = new PatternAppicationsContentProvider(af);
	}
	
	/**
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#hasChildren(java.lang.Object)
	 */
	@Override
	public boolean hasChildren(Object object) {
		return getChildren(object).length > 0;
	}
	
	@Override
	public Object[] getElements(Object element) {
		if (element instanceof ContainerShape) {
			return getChildren(element);
		} else {
			return super.getElements(element);
		}
	}
	
	@Override
	public Object[] getChildren(Object element) {
		if (element instanceof ContainerShape) {
			ContainerShape diagramsParent = (ContainerShape) element;
			PatternApplications patternApplicationsRoot = PatternApplicationDiagrams2ModelConnector.getPatternApplicationsModel(diagramsParent);
			if (patternApplicationsRoot != null) {
				return new PatternApplications[] { patternApplicationsRoot };
			}
			return EMPTY_ARRAY;
		} else if (element instanceof PatternApplications) {
			PatternApplications patternApplications = (PatternApplications) element;
			ArrayList<Object> children = new ArrayList<Object>(patternApplications.getApplications().size() + 1);
			EPackage modelRoot = PatternApplications2EcoreConnector.getDesignModelRoot(patternApplications);
			
			// the model where patterns are applied
			if (modelRoot != null) {
				children.add(modelRoot);
			}
			
			// the pattern applications
			children.addAll(patternApplications.getApplications());
			
			return children.toArray();
		} else if (element instanceof EPackage) {
			URI resourceURI = ((EPackage) element).eResource().getURI();
			if (resourceURI.fileExtension() != null && !resourceURI.fileExtension().isEmpty()) {
				String path = resourceURI.toPlatformString(true);
				IPath filePath = new Path(path);
				IResource fileResource = ResourcesPlugin.getWorkspace().getRoot().findMember(filePath);
				if (fileResource instanceof IFile) {
					return new Object[] { fileResource };
				}
			}
			return EMPTY_ARRAY;
		} else if (element instanceof AppliedPattern) {
			AppliedPattern patternApplication = (AppliedPattern) element;
			Diagram diagram = PatternApplicationDiagrams2ModelConnector.findDiagramFor(patternApplication);
			Object[] children = delegate.getChildren(patternApplication);
			
			if (diagram != null) {
				ArrayList<Object> list = new ArrayList<Object>(children.length + 1);
				list.add(diagram);
				for (Object child: children) {
					list.add(child);
				}
				children = list.toArray();
			} 
			
			return children;
		} else if (element instanceof EObject) {
			return delegate.getChildren(element);
		} else {
			return super.getChildren(element);
		}
	}
	
	@Override
	public Object getParent(Object object) {
		if (object instanceof PatternApplications) {
			return PatternApplicationDiagrams2ModelConnector.getLinkedPatternApplicationDiagramsContainer((PatternApplications) object);
		} else if (object instanceof EPackage) {
			return PatternApplications2EcoreConnector.getLinkedPatternApplications((EPackage) object);
		} else if (object instanceof AppliedPattern) {
			AppliedPattern patternApplication = (AppliedPattern) object;
			return patternApplication.getParent();
		} else if (object instanceof Diagram) {
			Diagram diagram = (Diagram) object;
			if (diagram.getLink() != null
					&& !diagram.getLink().getBusinessObjects().isEmpty()
					&& diagram.getLink().getBusinessObjects().get(0) instanceof AppliedPattern) {
				return diagram.getLink().getBusinessObjects().get(0);
			}
			return null;
		} else {
			return super.getParent(object);
		}
	}
	
}
