/**
 * 
 */
package de.upb.pose.mapping.ui.validation;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;

import de.upb.pose.mapping.AppliedPattern;

/**
 * @author Dietrich Travkin
 */
public class TestModelConstraint extends AbstractModelConstraint {

	/**
	 * @see org.eclipse.emf.validation.AbstractModelConstraint#validate(org.eclipse.emf.validation.IValidationContext)
	 */
	@Override
	public IStatus validate(IValidationContext ctx) {
		EObject target = ctx.getTarget();
		
		if (target instanceof AppliedPattern) {
			AppliedPattern patternApplication = (AppliedPattern) target;
			EPackage parentPackage = (EPackage) patternApplication.getParent().getDesignModelRoot();
			
			// TODO implement the real validation
			
			ctx.addResult(patternApplication);
			ctx.addResult(parentPackage);
			return ctx.createFailureStatus("\"" + patternApplication.getName() + "\"", parentPackage.getName());
		}
		
		return ctx.createSuccessStatus();
	}

}
