/**
 * 
 */
package de.upb.pose.mapping.ui.editor.multipage;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.ui.editor.MultiPageDiagramEditor;
import de.upb.pose.core.ui.editor.MultiPageDiagramEditorOverviewPage;
import de.upb.pose.core.ui.outline.IDiagramEditorOutlinePage;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingImages;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.ui.view.MappingView;

/**
 * @author Dietrich Travkin
 */
public class MultiPagePatternRoleMappingDiagramEditor extends MultiPageDiagramEditor {

	public static final String ID = "de.upb.pose.mapping.ui.editors.mappingdiagram";
	
	/**
	 * @see org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor#getContributorId()
	 */
	@Override
	public String getContributorId() {
		return MappingView.TABBED_PROPERTY_CONTRIBUTOR_ID;
	}

	/**
	 * @see de.upb.pose.core.ui.editor.MultiPageDiagramEditor#createOverviewPage()
	 */
	@Override
	protected MultiPageDiagramEditorOverviewPage createOverviewPage() {
		return new MultiPagePatternRoleMappingDiagramOverviewPage(this);
	}
	
	@Override
	protected IDiagramEditorOutlinePage createOutlinePage() {
		return new MultiPagePatternRoleMappingDiagramEditorOutlinePage(this);
	}
	
	@Override
	protected String getEditorName(EObject element) {
		if (element instanceof AppliedPattern) {
			return MappingNameCreator.getNameFor((AppliedPattern) element);
		}

		return null;
	}
	
	@Override
	protected String getEditorName(Diagram pe) {
		if (pe.getLink() != null) {
			for (EObject bo: pe.getLink().getBusinessObjects()) {
				if (bo instanceof AppliedPattern) {
					return getEditorName(bo);
				}
			}
		}
		return pe.getName();
	}

	@Override
	protected Image getEditorImage(Diagram diagram) {
		return MappingImages.get(MappingPackage.Literals.APPLIED_PATTERN);
	}

}
