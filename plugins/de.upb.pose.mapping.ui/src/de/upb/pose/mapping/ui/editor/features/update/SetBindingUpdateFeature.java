/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.update;

import java.util.List;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.platform.IDiagramEditor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.upb.pose.core.util.GS;
import de.upb.pose.mapping.ui.MappingLabelCreator;
import de.upb.pose.mapping.ui.editor.RoleMappingEditor;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.ui.editor.features.update.UpdateFeatureConstants;
import de.upb.pose.specification.ui.editor.helpers.SetOfElementsHelper;

/**
 * @author Dietrich Travkin
 */
public class SetBindingUpdateFeature extends DelegatingUpdateFeature
{
	public SetBindingUpdateFeature(IUpdateFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	@Override
	public IReason updateNeeded(IUpdateContext context)
	{
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		SetFragment bo = (SetFragment) getBusinessObjectForPictogramElement(pe);

		// instance text
		String boName = MappingLabelCreator.getSetElementLabelText(bo, pe);
		Text peName = SetOfElementsHelper.getNameText(pe);
		if (GS.unequals(boName, peName))
		{
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context)
	{
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		SetFragment bo = (SetFragment) getBusinessObjectForPictogramElement(pe);

		// instance text
		String boName = MappingLabelCreator.getSetElementLabelText(bo, pe);
		Text peName = SetOfElementsHelper.getNameText(pe);
		if (GS.unequals(boName, peName))
		{
			peName.setValue(boName);
		}

		layoutPictogramElement(pe);

		// also update contained children according to the currently visible set element binding
		for (SetFragmentElement element : bo.getContainedElements())
		{
			for (PictogramElement pElement: GS.getAllPEs(getDiagram(), element))
			{
				updatePictogramElement(pElement);
			}
		}

		asyncUpdateEditorSelection();
		
		return true;
	}

	private void asyncUpdateEditorSelection() {
		IDiagramEditor editor = this.getFeatureProvider().getDiagramTypeProvider().getDiagramEditor();
		if (editor instanceof RoleMappingEditor) {
			RoleMappingEditor editorPart = (RoleMappingEditor) editor;
			PictogramElement[] selected = editorPart.getSelectedPictogramElements();
			if (selected != null && selected.length > 0) {
				final GraphicalViewer viewer = editorPart.getGraphicalViewer();
				if (viewer != null) { // could be null if not initialized yet
					List<?> selectedEditParts = viewer.getSelectedEditParts();
					for (Object selEP: selectedEditParts) {
						final EditPart selectedEditPart = (EditPart) selEP;
						for (PictogramElement pe: selected) {
							if (selectedEditPart.getModel().equals(pe)) {
								deselectEditPartInGraphicalViewer(viewer, selectedEditPart);
							}
						}
					}
				}
			}
		}
	}
	
	private static Display getDisplay() {
		Display display = Display.getCurrent();
		if (display == null && PlatformUI.isWorkbenchRunning()) {
			display = PlatformUI.getWorkbench().getDisplay();
		}
		return display != null ? display : Display.getDefault();
	}
	
	private static void deselectEditPartInGraphicalViewer(final GraphicalViewer viewer, final EditPart selectedEditPart) {
		getDisplay().asyncExec(new Runnable() {

			@Override
			public void run() {
				viewer.deselect(selectedEditPart);
			}
			
		});
	}
	
}
