/**
 * 
 */
package de.upb.pose.mapping.ui.editor.multipage;

import org.eclipse.jface.viewers.ITreeContentProvider;

import de.upb.pose.core.ui.outline.DiagramEditorOutlinePage;
import de.upb.pose.core.ui.outline.IOutlineInputProvider;

/**
 * @author Dietrich Travkin
 */
public class MultiPagePatternRoleMappingDiagramEditorOutlinePage extends DiagramEditorOutlinePage {

	public MultiPagePatternRoleMappingDiagramEditorOutlinePage(IOutlineInputProvider provider) {
		super(provider);
	}

	@Override
	protected ITreeContentProvider createContentProvider() {
		return new MultiPagePatternRoleMappingDiagramEditorContentProvider(provider.getAdapterFactory());
	}
	
}
