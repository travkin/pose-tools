/**
 * 
 */
package de.upb.pose.mapping.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Size;
import de.upb.pose.mapping.ui.editor.features.RoleMappingEditorFeatureProvider;
import de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature;

/**
 * @author Dietrich Travkin
 */
public class DelegatingShapeLayoutFeature extends DelegatingLayoutFeature implements IShapeWithLabelLayoutFeature
{
	public DelegatingShapeLayoutFeature(IShapeWithLabelLayoutFeature featureToBeWrapped, RoleMappingEditorFeatureProvider fp)
	{
		super(featureToBeWrapped, fp);
	}
	
	@Override
	protected IShapeWithLabelLayoutFeature getWrappedFeature()
	{
		return (IShapeWithLabelLayoutFeature) super.getWrappedFeature();
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeSize(ILayoutContext context)
	{
		return getWrappedFeature().determineMinimalShapeSize(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context)
	{
		return getWrappedFeature().determineMinimalShapeContentsSize(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layoutShape(ILayoutContext context)
	{
		return getWrappedFeature().layoutShape(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeLayoutFeature#layoutShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layoutShapeContents(ILayoutContext context)
	{
		return getWrappedFeature().layoutShapeContents(context);
	}

	/* (non-Javadoc)
	 * @see de.upb.pose.specification.ui.editor.features.IShapeLayoutFeature#getLabel(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Text getLabel(ILayoutContext context)
	{
		return getWrappedFeature().getLabel(context);
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#canHandle(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement)
	{
		return getWrappedFeature().canHandle(labelIdentifier, pictogramElement);
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#getLabel(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement)
	{
		return getWrappedFeature().getLabel(labelIdentifier, pictogramElement);
	}
}
