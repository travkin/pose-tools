package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.ui.editor.helpers.ParameterAssignmentUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class ParameterAssignmentUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public ParameterAssignmentUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	protected ParameterAssignment getBusinessObject(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		if (pe != null) {
			Object bo = getBusinessObjectForPictogramElement(pe);
			if (bo != null && bo instanceof ParameterAssignment) {
				return (ParameterAssignment) bo;
			}
		}
		return null;
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		return (getBusinessObject(context) != null);
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		ParameterAssignment bo = getBusinessObject(context);

		// text
		String boName = ParameterAssignmentUtil.getText(bo);
		Text peName = ParameterAssignmentUtil.getText(pe);
		if (GS.unequals(boName, peName)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}
		
		// background color
		IColorConstant boColor = getColor(bo);
		Color peColor = getBackgroundColor(context);
		if (GS.differ(boColor, peColor)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		ParameterAssignment bo = getBusinessObject(context);

		// text
		String boName = ParameterAssignmentUtil.getText(bo);
		Text peName = ParameterAssignmentUtil.getText(pe);
		if (GS.unequals(boName, peName)) {
			peName.setValue(boName);
			
			layoutPictogramElement(pe.getContainer());
		}
		
		// background color
		IColorConstant boColor = getColor(bo);
		setBackgroundColor(context, boColor);

		layoutPictogramElement(pe);

		return true;
	}

}
