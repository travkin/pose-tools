package de.upb.pose.specification.ui.properties.sections;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.BooleanCheckboxSectionBase;
import de.upb.pose.specification.SpecificationPackage;

public class PatternElementGenerableSection extends BooleanCheckboxSectionBase {
	@Override
	protected String getLabelText() {
		return "Can Be Generated";
	}

	@Override
	protected EStructuralFeature getFeature() {
		return SpecificationPackage.Literals.PATTERN_ELEMENT__CAN_BE_GENERATED;
	}
}
