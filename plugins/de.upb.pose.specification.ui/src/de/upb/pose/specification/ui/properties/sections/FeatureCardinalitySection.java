package de.upb.pose.specification.ui.properties.sections;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionRadioSectionBase;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.TypesPackage;

public class FeatureCardinalitySection extends CollectionRadioSectionBase<CardinalityType> {
	@Override
	protected String getDescription() {
		return "Cardinality";
	}

	@Override
	protected List<CardinalityType> getValues() {
		return CardinalityType.VALUES;
	}

	@Override
	protected EStructuralFeature getFeature() {
		return TypesPackage.Literals.FEATURE__CARDINALITY;
	}
}
