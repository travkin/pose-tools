package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.PatternEnvironment;

public class PatternEnvironmentCreateFeature extends CreateFeature {
	public PatternEnvironmentCreateFeature(IFeatureProvider fp) {
		super(fp, "Environment");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		ContainerShape cpe = context.getTargetContainer();
		Object cbo = getBusinessObjectForPictogramElement(cpe);

		return cbo instanceof PatternSpecification && ((PatternSpecification) cbo).getEnvironment() == null;
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(AccessPackage.Literals.PATTERN_ENVIRONMENT);
	}

	@Override
	protected EObject doCreate(ICreateContext context) {
		ContainerShape cpe = context.getTargetContainer();
		PatternSpecification cbo = (PatternSpecification) getBusinessObjectForPictogramElement(cpe);

		PatternEnvironment bo = AccessFactory.eINSTANCE.createPatternEnvironment();
		bo.setName("Environment");
		bo.setCanBeGenerated(false);
		bo.setSpecification(cbo);

		return bo;
	}
}
