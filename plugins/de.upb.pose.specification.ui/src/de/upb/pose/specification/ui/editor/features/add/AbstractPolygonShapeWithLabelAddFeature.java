/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.ui.editor.features.layout.AbstractPolygonShapeWithLabelLayoutFeature;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractPolygonShapeWithLabelAddFeature extends AbstractShapeWithLabelAddFeature {

	public AbstractPolygonShapeWithLabelAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	protected int getNumberOfPolygonPoints() {
		return 6;
	}
	
	@Override
	protected boolean updateSetFragments() {
		return true;
	}
	
	protected boolean initLocation() {
		return true;
	}
	
	protected void createAdditionalAnchorsFor(ContainerShape shapePE, Rectangle invisibleParent) {
		// do nothing
	}
	
	@Override
	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		// due to an unexpected translation of polygon figures in graphiti,
		// we need an invisible parent rectangle to correctly use the coordinates
		// implementation/bug in
		// org.eclipse.graphiti.ui.internal.parts.PictogramElementDelegate#refreshFigureForGraphicsAlgorithm(GraphicsAlgorithm,
		// PictogramElement, IReason), lines 493, 494

		// invisible parent rectangle for the polygon and label
		Rectangle invisibleParent = Graphiti.getGaService().createInvisibleRectangle(parentShapePE);
		invisibleParent.setLineWidth(0);

		createAdditionalAnchorsFor(parentShapePE, invisibleParent);

		// frame
		int numberOfPoints = this.getNumberOfPolygonPoints();
		Polygon frame = Graphiti.getGaService().createPolygon(invisibleParent, new int[numberOfPoints * 2]);
		frame.setBackground(colorWhite);
		frame.setForeground(colorBlack);
		frame.setLineVisible(true);
		frame.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);
		return invisibleParent;
	}
	
	@Override
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		// also set the initial location
		if (initLocation()) {
			LayoutContext layoutContext = new LayoutContext(parentShapePE);
			ILayoutFeature tmpLayout = getFeatureProvider().getLayoutFeature(layoutContext);
			
			Assert.isLegal(tmpLayout instanceof AbstractPolygonShapeWithLabelLayoutFeature,
					"The LayoutFeature used with this AddFeature has to be a subclass of "
							+ AbstractPolygonShapeWithLabelLayoutFeature.class.getCanonicalName());
			
			AbstractPolygonShapeWithLabelLayoutFeature layout = (AbstractPolygonShapeWithLabelLayoutFeature) tmpLayout;
			Size shapeSize = layout.determineMinimalShapeSize(layoutContext);
			
			labelParent.setX((int) Math.round(context.getX() - shapeSize.getWidth() / 2d));
			labelParent.setY((int) Math.round(context.getY() - shapeSize.getHeight() / 2d));
		}
	}

}
