package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.util.SpecificationTextUtil;

public class ActionAddFeature extends AbstractShapeWithLabelAddFeature {
	
	public ActionAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	public static GraphicsAlgorithm getChopboxAnchorArea(PictogramElement pe) {
		if (pe instanceof ContainerShape) {
			if (pe.getLink() != null && pe.getLink().getBusinessObjects().size() > 0) {
				EObject bo = pe.getLink().getBusinessObjects().get(0);
				if (bo instanceof Action) {
					ContainerShape actionPE = (ContainerShape) pe;
					// return the rounded rectangle
					RoundedRectangle shape = (RoundedRectangle) actionPE.getGraphicsAlgorithm()
							.getGraphicsAlgorithmChildren().get(0);
					return shape;
				}
			}
		}
		return null;
	}
	
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Action;
	}
	
	protected Action getBusinessObject(IAddContext context) {
		return (Action) context.getNewObject();
	}
	
	protected String getLabelTextForBusinessObject(IAddContext context) {
		Action action = getBusinessObject(context);
		return SpecificationTextUtil.getName(action);
	}
	
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_BOLD_REGULAR;
	}
	
	@Override
	protected boolean enableDirectEditing() {
		return false;
	}
	
	@Override
	protected boolean updateSetFragments() {
		return true;
	}
	
	@Override
	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		// create an invisible parent rectangle to contain
		// the rounded rectangle representing the action itself
		// as well as the result variable's rectangle

		// invisible parent rectangle for the polygon and label
		Rectangle invisibleParent = Graphiti.getGaService().createInvisibleRectangle(parentShapePE);
		invisibleParent.setLineWidth(0);

		// frame
		RoundedRectangle roundedRectangle = GraphicsAlgorithmsFactory.addRoundedRectangle(invisibleParent, colorWhite, colorBlack, LineStyle.SOLID);
		
		return roundedRectangle;
	}
	
	@Override
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		LayoutContext layoutContext = new LayoutContext(parentShapePE);
		ILayoutFeature tmpLayout = getFeatureProvider().getLayoutFeature(layoutContext);
		AbstractShapeWithLabelLayoutFeature layout = (AbstractShapeWithLabelLayoutFeature) tmpLayout;
		Size actionPreferredShapeSize = layout.determineMinimalShapeSize(layoutContext);

		GraphicsAlgorithm invisibleParent = parentShapePE.getGraphicsAlgorithm();
		
		invisibleParent.setX((int) Math.round(context.getX() - actionPreferredShapeSize.getWidth() / 2d));
		invisibleParent.setY((int) Math.round(context.getY() - actionPreferredShapeSize.getHeight() / 2d));
	}

}
