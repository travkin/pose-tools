package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.types.Operation;

public class CallActionCreateFeature extends ActionCreateFeature {
	
	public CallActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Call");
	}
	
	protected CallActionCreateFeature(IFeatureProvider fp, String name) {
		super(fp, name);
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.CALL_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		CallAction newActionBO = ActionsFactory.eINSTANCE.createCallAction();
		newActionBO.setCalledOperation((Operation) target);
		return newActionBO;
	}

	@Override
	protected boolean canFinish(EObject target) {
		return target instanceof Operation;
	}

	@Override
	protected Object getTargetConnectionNewBusinessObject() {
		return ActionsPackage.Literals.CALL_ACTION__CALLED_OPERATION;
	}
}
