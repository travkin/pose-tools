package de.upb.pose.specification.ui.wizards;

import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;

import de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecificationConstants;
import de.upb.pose.specification.ui.PatternSpecificationDiagrams2ModelConnector;
import de.upb.pose.specification.ui.editor.SpecificationEditor;
import de.upb.pose.specification.util.SpecificationUtil;

public class NewSpecificationCatalogWizard extends AbstractGraphitiCatalogWizard<DesignPatternCatalog> {
	
	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getEditorId()
	 */
	@Override
	protected String getEditorId() {
		return SpecificationEditor.ID;
	}
	
	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getModelFileExtension()
	 */
	@Override
	protected String getModelFileExtension() {
		return PatternSpecificationConstants.MODEL_FILE_EXTENSION;
	}

	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#getDiagramFileExtension()
	 */
	@Override
	protected String getDiagramFileExtension() {
		return PatternSpecificationConstants.DIAGRAMS_FILE_EXTENSION;
	}
	
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		
		setWindowTitle("New Design Pattern Catalog");
	}
	
	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#createModelRoot()
	 */
	@Override
	protected DesignPatternCatalog createModelRoot() {
		return SpecificationUtil.createEmptyInitializedCatalog();
	}

	/**
	 * @see de.upb.pose.core.ui.wizards.AbstractGraphitiCatalogWizard#createDiagramsRoot(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected ContainerShape createDiagramsRoot(DesignPatternCatalog modelRoot) {
		ContainerShape diagramsContainer = PatternSpecificationDiagrams2ModelConnector.createPatternSpecificationDiagramsContainer();
		PatternSpecificationDiagrams2ModelConnector.createPointerToPatternSpecificationDiagramsContainer(modelRoot);
		PatternSpecificationDiagrams2ModelConnector.linkPatternSpecificationsCatalogToPatternSpecificationDiagramsContainer(modelRoot, diagramsContainer);
		return diagramsContainer;
	}

}
