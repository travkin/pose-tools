/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.update;

import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.GS;
import de.upb.pose.specification.ui.editor.features.layout.LabelConstants;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractShapeWithLabelUpdateFeature extends AbstractUpdateFeature implements IShapeUpdateFeature
{
	public AbstractShapeWithLabelUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public SpecificationEditorFeatureProvider getFeatureProvider() {
		return (SpecificationEditorFeatureProvider) super.getFeatureProvider();
	}
	
	protected GraphicsAlgorithm getRootGraphicsAlgorithm(IUpdateContext context)
	{
		return context.getPictogramElement().getGraphicsAlgorithm();
	}
	
	public Color getBackgroundColor(IUpdateContext context)
	{
		return getRootGraphicsAlgorithm(context).getBackground();
	}
	
	public void setBackgroundColor(IUpdateContext context, IColorConstant newColor)
	{
		Color peColor = getBackgroundColor(context);
		if (GS.differ(newColor, peColor))
		{
			getRootGraphicsAlgorithm(context).setBackground(manageColor(newColor));
		}
	}
	
	public LineStyle getLineStyle(IUpdateContext context)
	{
		return getRootGraphicsAlgorithm(context).getLineStyle();
	}
	
	public void setLineStyle(IUpdateContext context, LineStyle newLineStyle)
	{
		LineStyle current = getLineStyle(context);
		if (!newLineStyle.equals(current)) {
			getRootGraphicsAlgorithm(context).setLineStyle(newLineStyle);
		}
	}
	
	protected Text getLabel(IUpdateContext context)
	{
		return this.getFeatureProvider().getLabelService().getLabel(LabelConstants.LABEL_ID_NAME, context.getPictogramElement());
	}
	
	public String getLabelText(IUpdateContext context)
	{
		Text label = getLabel(context);
		if (label != null)
		{
			return label.getValue();
		}
		return null;
	}
	
	public void setLabelText(IUpdateContext context, String newText)
	{
		Text label = getLabel(context);
		if (GS.unequals(newText, label))
		{
			label.setValue(newText);
		}
	}
	
	public Font getLabelFont(IUpdateContext context)
	{
		return getLabel(context).getFont();
	}
	
	public void setLabelFont(IUpdateContext context, FontDescription newFont)
	{
		Text label = getLabel(context);
		Font currentFont = label.getFont();
		if (GS.differ(newFont, currentFont))
		{
			label.setFont(newFont.manage(getDiagram()));
		}
	}

}
