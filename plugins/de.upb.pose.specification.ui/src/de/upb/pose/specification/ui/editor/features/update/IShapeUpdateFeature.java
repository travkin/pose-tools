/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.update;

import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;

/**
 * @author Dietrich Travkin
 */
public interface IShapeUpdateFeature extends IUpdateFeature
{
	Color getBackgroundColor(IUpdateContext context);
	void setBackgroundColor(IUpdateContext context, IColorConstant newColor);
	
	LineStyle getLineStyle(IUpdateContext context);
	void setLineStyle(IUpdateContext context, LineStyle newLineStyle);
	
	String getLabelText(IUpdateContext context);
	void setLabelText(IUpdateContext context, String newText);
	
	Font getLabelFont(IUpdateContext context);
	void setLabelFont(IUpdateContext context, FontDescription newFont);
}
