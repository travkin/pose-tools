package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class AttributeLayoutFeature extends AbstractPolygonShapeWithLabelLayoutFeature {
	
	private static final int PEAK_WIDTH = 7;
	
	public AttributeLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof Attribute;
		}
		return false;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	public Size determineMinimalShapeSize(ILayoutContext context) {
		Size minSize = determineMinimalShapeContentsSize(context).add(2 * PEAK_WIDTH, 0);
		return minSize;
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	public boolean layoutShape(ILayoutContext context) {
		Polygon polygon = getShape(context);
		Size minSize = determineMinimalShapeSize(context);
		
		int width = minSize.getWidth();
		int height = minSize.getHeight();
		int y_mid = (int) Math.round(height / 2d);
		
		// then relocate the polygon within the invisible parent's boundaries
		polygon.getPoints().get(0).setX(0);
		polygon.getPoints().get(0).setY(0);
		polygon.getPoints().get(1).setX(width);
		polygon.getPoints().get(1).setY(0);
		polygon.getPoints().get(2).setX(width - PEAK_WIDTH + 1);
		polygon.getPoints().get(2).setY(y_mid);
		polygon.getPoints().get(3).setX(width);
		polygon.getPoints().get(3).setY(height);
		polygon.getPoints().get(4).setX(0);
		polygon.getPoints().get(4).setY(height);
		polygon.getPoints().get(5).setX(PEAK_WIDTH);
		polygon.getPoints().get(5).setY(y_mid);

		return true;
	}
}
