/**
 * 
 */
package de.upb.pose.specification.ui.editor.graphics;

import de.upb.pose.core.util.FontDescription;

/**
 * @author Dietrich Travkin
 */
public interface FontConstants {
	
	public static final String FONT_NAME_DEFAULT = "Segoe UI"; //$NON-NLS-1$
	
	public static final FontDescription FONT_9_NORMAL_REGULAR = new FontDescription(FONT_NAME_DEFAULT, 9, false, false);
	public static final FontDescription FONT_9_BOLD_REGULAR = new FontDescription(FONT_NAME_DEFAULT, 9, true, false);
	public static final FontDescription FONT_10_NORMAL_REGULAR = new FontDescription(FONT_NAME_DEFAULT, 10, false, false);
	public static final FontDescription FONT_10_NORMAL_ITALIC = new FontDescription(FONT_NAME_DEFAULT, 10, false, true);
	public static final FontDescription FONT_12_BOLD_REGULAR = new FontDescription(FONT_NAME_DEFAULT, 12, true, false);
	public static final FontDescription FONT_12_BOLD_ITALIC = new FontDescription(FONT_NAME_DEFAULT, 12, true, true);
}
