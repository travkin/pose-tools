package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesFactory;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.helpers.ReferenceUtil;

public class ReferenceCreateFeature extends CreateConnectionFeature {
	
	public ReferenceCreateFeature(IFeatureProvider fp) {
		super(fp, "Reference");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(TypesPackage.Literals.REFERENCE);
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		Anchor sourcePe = context.getSourceAnchor();
		Anchor targetPe = context.getTargetAnchor();

		Type sourceBo = (Type) getBusinessObjectForPictogramElement(sourcePe.getParent());
		Type targetBo = (Type) getBusinessObjectForPictogramElement(targetPe.getParent());

		Reference bo = TypesFactory.eINSTANCE.createReference();
		bo.setName(ReferenceUtil.getInitialName(sourceBo));

		bo.setParentType(sourceBo);
		bo.setType(targetBo);
		bo.setCardinality(CardinalityType.SINGLE);

		AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
		addContext.setNewObject(bo);

		return (Connection) getFeatureProvider().addIfPossible(addContext);
	}

	@Override
	protected boolean canStart(EObject source) {
		return source instanceof Type;
	}

	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return source instanceof Type && target instanceof Type;
	}
}
