package de.upb.pose.specification.ui.properties.sections;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionComboSectionBase;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.util.SpecificationUtil;

public class ParameterTypeSection extends CollectionComboSectionBase<Type> {
	@Override
	protected EStructuralFeature getFeature() {
		return TypesPackage.Literals.PARAMETER__TYPE;
	}

	@Override
	protected List<Type> getItems() {
		List<Type> items = new ArrayList<Type>();

		// add 'null'
		items.add(null);

		PatternSpecification specification = SpecificationUtil.getSpecification(getElement());
		DesignPatternCatalog catalog = SpecificationUtil.getCatalog(specification);

		// add all primitive types
		items.addAll(catalog.getPrimitiveTypes());

		// collect all types of the specification
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof Type) {
				items.add((Type) element);
			}
		}

		return items;
	}

	@Override
	protected String getText(Type element) {
		if (element != null) {
			return element.getName();
		}
		return EMPTY;
	}

	@Override
	protected String getLabelText() {
		return "Type";
	}
}
