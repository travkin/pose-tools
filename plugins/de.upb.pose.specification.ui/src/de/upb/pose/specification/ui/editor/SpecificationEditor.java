package de.upb.pose.specification.ui.editor;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.ui.editor.MultiPageDiagramEditor;
import de.upb.pose.core.ui.editor.MultiPageDiagramEditorOverviewPage;
import de.upb.pose.core.ui.outline.IDiagramEditorOutlinePage;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.ui.outline.SpecificationEditorOutlinePage;
import de.upb.pose.specification.util.SpecificationTextUtil;

public class SpecificationEditor extends MultiPageDiagramEditor {
	
	public static final String ID = "de.upb.pose.specification.ui.editors.diagram";
	
	@Override
	public String getContributorId() {
		return "de.upb.pose.specification";
	}

	@Override
	protected IDiagramEditorOutlinePage createOutlinePage() {
		return new SpecificationEditorOutlinePage(this);
	}

	@Override
	protected MultiPageDiagramEditorOverviewPage createOverviewPage() {
		return new SpecificationEditorOverviewPage(this);
	}

	@Override
	protected Image getEditorImage(Diagram diagram) {
		return SpecificationImages.get(SpecificationPackage.Literals.DESIGN_PATTERN);
	}

	@Override
	protected String getEditorName(EObject element) {
		if (element instanceof PatternSpecification) {
			return SpecificationTextUtil.get((PatternSpecification) element);
		}

		return null;
	}
}
