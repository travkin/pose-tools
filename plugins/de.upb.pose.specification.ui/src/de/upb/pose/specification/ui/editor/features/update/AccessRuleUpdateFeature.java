package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.AccessRuleUtil.getConnection;
import static de.upb.pose.specification.ui.editor.helpers.AccessRuleUtil.getText;
import static de.upb.pose.specification.ui.editor.helpers.AccessRuleUtil.isForbidden;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.access.AccessRule;

public class AccessRuleUpdateFeature extends AbstractUpdateFeature {
	public AccessRuleUpdateFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof AccessRule;
	}

	@Override
	public boolean update(IUpdateContext context) {
		Connection pe = getConnection(context.getPictogramElement());
		AccessRule bo = (AccessRule) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = getText(bo);
		Text peText = getText(pe);
		if (GS.unequals(boText, peText)) {
			peText.setValue(boText);
		}

		// cross
		boolean boForbidden = bo.isForbidden();
		boolean peForbidden = isForbidden(pe);
		if (boForbidden != peForbidden) {
			if (bo.isForbidden()) {
				ConnectionDecorator cross1Pe = Graphiti.getPeService().createConnectionDecorator(pe, false, 0.5, true);
				Polyline x1l = Graphiti.getGaService().createPolyline(cross1Pe, new int[] { -20, -10, 20, 10 });
				x1l.setForeground(manageColor(IColorConstant.RED));

				ConnectionDecorator cross2Pe = Graphiti.getPeService().createConnectionDecorator(pe, false, 0.5, true);
				Polyline x2l = Graphiti.getGaService().createPolyline(cross2Pe, new int[] { -20, 10, 20, -10 });
				x2l.setForeground(manageColor(IColorConstant.RED));
			} else {
				pe.getConnectionDecorators().remove(2);
				pe.getConnectionDecorators().remove(2);
			}
		}

		return true;
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		AccessRule bo = (AccessRule) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = getText(bo);
		Text peText = getText(pe);
		if (GS.unequals(boText, peText)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}

		// cross
		boolean boForbidden = bo.isForbidden();
		boolean peForbidden = isForbidden(pe);
		if (boForbidden != peForbidden) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_DECORATOR);
		}

		return Reason.createFalseReason();
	}
}
