package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class SetFragmentLayoutFeature extends AbstractShapeWithLabelLayoutFeature {
	
	private static final int COORDINATE_DELTA = 10;
	
	public SetFragmentLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof SetFragment;
		}
		return false;
	}

	/**
	 * Returns the root graphics algorithms, which is a rectangle and the main (root) frame of the whole figure.
	 * 
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#getRootGA(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected Rectangle getRootGA(ILayoutContext context) {
		return (Rectangle) super.getRootGA(context);
	}
	
	/**
	 * Returns the invisible rectangle containing the labels and the polygon surrounding the label.
	 * This rectangle is drawn on the top of the figure, but is not the root frame.
	 */
	private Rectangle getInvisibleLabelContainer(ILayoutContext context) {
		Rectangle rootGA = getRootGA(context);
		
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().size() > 0);
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().get(0) instanceof Rectangle);
		
		return (Rectangle) rootGA.getGraphicsAlgorithmChildren().get(0);
	}
	
	/**
	 * Returns the polygon frame surrounding the label. That one is drawn on the top of the figure, but is not the root
	 * frame.
	 */
	private Polygon getLabelFrame(ILayoutContext context) {
		Rectangle rootGA = getInvisibleLabelContainer(context);
		int index = 0;
		
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().size() > index);
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().get(index) instanceof Polygon);
		
		return (Polygon) rootGA.getGraphicsAlgorithmChildren().get(index);
	}
	
	/**
	 * Returns the first part of the label. This one usually contains the constant string "set".
	 */
	private Text getPrefixLabel(ILayoutContext context) {
		Rectangle rootGA = getInvisibleLabelContainer(context);
		int index = 1;
		
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().size() > index);
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().get(index) instanceof Text);
		
		return (Text) rootGA.getGraphicsAlgorithmChildren().get(index);
	}
	
	/**
	 * Returns the first part of the label. This one usually contains the constant string "set".
	 */
	private Text getSuffixLabel(ILayoutContext context) {
		Rectangle rootGA = getInvisibleLabelContainer(context);
		int index = 2;
		
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().size() > index);
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().get(index) instanceof Text);
		
		return (Text) rootGA.getGraphicsAlgorithmChildren().get(index);
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#getLabel(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Text getLabel(ILayoutContext context) {
		return getSuffixLabel(context);
	}
	
	protected Size determinePreferredPrefixLabelSize(ILayoutContext context) {
		Text label = getPrefixLabel(context);
		Size padding = getLabelPadding();
		Size minSize = GS.getSize(label).padding(padding.getWidth(), padding.getHeight());
		return minSize;
	}
	
	protected Size determinePreferredSuffixLabelSize(ILayoutContext context) {
		return super.determinePreferredLabelSize(context);
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		Size prefixLabelSize = determinePreferredPrefixLabelSize(context);
		Size suffixLabelSize = determinePreferredSuffixLabelSize(context);
		
		Size completeLabelSize = new Size(
				prefixLabelSize.getWidth() + suffixLabelSize.getWidth() + COORDINATE_DELTA,
				Math.max(prefixLabelSize.getHeight(), suffixLabelSize.getHeight()) ); 
		
		return completeLabelSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#layoutRootGA(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean layoutRootGA(ILayoutContext context) {
		boolean anythingChanged = false;
		
		GraphicsAlgorithm rootGA = getRootGA(context);
		Size preferredRootGASize = determineMinimalRootGASize(context);
		
		// only increase the shape's size if it is not big enough
		if (rootGA.getWidth() < preferredRootGASize.getWidth()) {
			rootGA.setWidth(preferredRootGASize.getWidth());
			anythingChanged = true;
		}
		if (rootGA.getHeight() < preferredRootGASize.getHeight()) {
			rootGA.setHeight(preferredRootGASize.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layoutShape(ILayoutContext context) {
		// do nothing, since the shape is the root graphics algorithm,
		// i.e. it is laid out in #layoutRootGA(ILayoutContext).
		return false;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#resizeShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		// resize suffix label
		boolean anythingChanged = super.resizeShapeContents(context);
		
		// resize prefix label
		Size preferredLabelSize = determinePreferredPrefixLabelSize(context);
		Text label = getPrefixLabel(context);
		if (label.getWidth() != preferredLabelSize.getWidth()
				|| label.getHeight() != preferredLabelSize.getHeight()) {
			label.setWidth(preferredLabelSize.getWidth());
			label.setHeight(preferredLabelSize.getHeight());
			anythingChanged = true;
		}
		
		// resize invisible label container
		Size preferredShapeContentsSize = determineMinimalShapeContentsSize(context);
		Rectangle invisibleLabelContainer = getInvisibleLabelContainer(context);
		if (invisibleLabelContainer.getWidth() != preferredShapeContentsSize.getWidth()
				|| invisibleLabelContainer.getHeight() != preferredShapeContentsSize.getHeight()) {
			invisibleLabelContainer.setWidth(preferredShapeContentsSize.getWidth());
			invisibleLabelContainer.setHeight(preferredShapeContentsSize.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#relocateShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		
		// invisible label container
		Rectangle invisibleLabelContainer = getInvisibleLabelContainer(context);
		if (invisibleLabelContainer.getX() != 0 || invisibleLabelContainer.getY() != 0) {
			invisibleLabelContainer.setX(0);
			invisibleLabelContainer.setY(0);
			anythingChanged = true;
		}
		
		// prefix label
		Text prefixLabel = getPrefixLabel(context);
		if (prefixLabel.getX() != 0 || prefixLabel.getY() != 0) {
			prefixLabel.setX(0);
			prefixLabel.setY(0);
			anythingChanged = true;
		}
		
		// suffix label
		Text suffixLabel = getSuffixLabel(context);
		if (suffixLabel.getX() != prefixLabel.getWidth() || suffixLabel.getY() != 0) {
			suffixLabel.setX(prefixLabel.getWidth());
			suffixLabel.setY(0);
			anythingChanged = true;
		}
		
		// resize polygon / label frame
		Polygon labelFrame = getLabelFrame(context);
		labelFrame.getPoints().get(0).setX(0);
		labelFrame.getPoints().get(0).setY(0);
		labelFrame.getPoints().get(1).setX(invisibleLabelContainer.getWidth());
		labelFrame.getPoints().get(1).setY(0);
		labelFrame.getPoints().get(2).setX(invisibleLabelContainer.getWidth());
		labelFrame.getPoints().get(2).setY(invisibleLabelContainer.getHeight() - COORDINATE_DELTA);
		labelFrame.getPoints().get(3).setX(invisibleLabelContainer.getWidth() - COORDINATE_DELTA);
		labelFrame.getPoints().get(3).setY(invisibleLabelContainer.getHeight());
		labelFrame.getPoints().get(4).setX(0);
		labelFrame.getPoints().get(4).setY(invisibleLabelContainer.getHeight());
		
		return anythingChanged;
	}
	
}
