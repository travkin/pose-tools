package de.upb.pose.specification.ui.properties.sections;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.BooleanCheckboxSectionBase;
import de.upb.pose.specification.access.AccessPackage;

public class AccessRuleForbiddenSection extends BooleanCheckboxSectionBase {
	@Override
	protected String getLabelText() {
		return "Forbidden";
	}

	@Override
	protected EStructuralFeature getFeature() {
		return AccessPackage.Literals.ACCESS_RULE__FORBIDDEN;
	}
}
