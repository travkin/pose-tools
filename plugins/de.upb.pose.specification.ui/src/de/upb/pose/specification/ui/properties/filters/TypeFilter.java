package de.upb.pose.specification.ui.properties.filters;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.ui.properties.PropertyFilterBase;
import de.upb.pose.specification.types.Type;

public class TypeFilter extends PropertyFilterBase {
	@Override
	protected boolean show(EObject element) {
		return element instanceof Type;
	}
}
