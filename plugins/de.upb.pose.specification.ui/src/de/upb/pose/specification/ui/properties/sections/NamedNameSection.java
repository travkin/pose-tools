package de.upb.pose.specification.ui.properties.sections;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.CorePackage;
import de.upb.pose.core.ui.properties.StringTextSectionBase;

public class NamedNameSection extends StringTextSectionBase {
	@Override
	protected String getLabelText() {
		return "Name";
	}

	@Override
	protected void postExecute() {
		refreshTitle();
	}

	@Override
	protected EStructuralFeature getFeature() {
		return CorePackage.Literals.NAMED__NAME;
	}
}
