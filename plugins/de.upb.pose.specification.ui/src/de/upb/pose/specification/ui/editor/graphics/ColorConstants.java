/**
 * 
 */
package de.upb.pose.specification.ui.editor.graphics;

import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

/**
 * @author Dietrich Travkin
 */
public interface ColorConstants {
	
	public static final IColorConstant CANNOT_BE_GENERATED = new ColorConstant(180, 180, 180);
	public static final IColorConstant CAN_BE_GENERATED = new ColorConstant(255, 255, 255);
	
}
