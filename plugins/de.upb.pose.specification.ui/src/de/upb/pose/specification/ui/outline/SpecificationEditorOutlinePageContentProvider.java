package de.upb.pose.specification.ui.outline;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.jface.viewers.Viewer;

import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.WriteAction;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.util.SpecificationUtil;

public class SpecificationEditorOutlinePageContentProvider extends AdapterFactoryContentProvider {
	
	private boolean showDiagram;

	public SpecificationEditorOutlinePageContentProvider(AdapterFactory af) {
		super(af);
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	@Override
	public Object[] getChildren(Object element) {
		Collection<Object> children = null;

		if (showDiagram) {
			if (element instanceof PatternSpecification) {
				children = getChildren((PatternSpecification) element);
			} else if (element instanceof SetFragment) {
				children = getChildren((SetFragment) element);
			} else if (element instanceof PatternEnvironment) {
				children = getChildren((PatternEnvironment) element);
			} else if (element instanceof Type) {
				children = getChildren((Type) element);
			} else if (element instanceof Operation) {
				children = getChildren((Operation) element);
			} else if (element instanceof Attribute) {
				children = getChildren((Attribute) element);
			} else if (element instanceof Reference) {
				children = getChildren((Reference) element);
			} else if (element instanceof CallAction) {
				children = getChildren((CallAction) element);
			} else if (element instanceof CreateAction) {
				children = getChildren((CreateAction) element);
			} else if (element instanceof WriteAction) {
				children = getChildren((WriteAction) element);
			} else if (element instanceof ReadAction) {
				children = getChildren((ReadAction) element);
			} else if (element instanceof ParameterAssignment) {
				children = getChildren((ParameterAssignment) element);
			}
		} else {
			if (element instanceof DesignPatternCatalog) {
				children = getChildren((DesignPatternCatalog) element);
			} else if (element instanceof Category) {
				children = getChildren((Category) element);
			} else if (element instanceof DesignPattern) {
				children = getChildren((DesignPattern) element);
			}
		}

		if (children != null) {
			return children.toArray(new Object[children.size()]);
		}
		return new Object[0];
	}

	private static Collection<Object> getChildren(PatternSpecification element) {
		Collection<Object> children = new ArrayList<Object>();

		// environment
		if (element.getEnvironment() != null) {
			children.add(element.getEnvironment());
		}

		// set elements without parent set
		children.addAll(SpecificationUtil.getRootSetElements(element));

		// access rules
		children.addAll(element.getAccessRules());

		return children;
	}

	private static Collection<Object> getChildren(SetFragment element) {
		ArrayList<Object> children = new ArrayList<Object>(element.getContainedElements().size());
		for (SetFragmentElement child: element.getContainedElements()) {
			if (child instanceof Reference || child instanceof SelfVariable) {
				continue;
			} else {
				children.add(child);
			}
		}
		return children;
	}

	private static Collection<Object> getChildren(PatternEnvironment element) {
		return new ArrayList<Object>(element.getAccessingRules());
	}

	private static Collection<Object> getChildren(Type element) {
		Collection<Object> children = new ArrayList<Object>();

		// attributes
		children.addAll(element.getAttributes());

		// operations
		children.addAll(element.getOperations());

		// references
		children.addAll(element.getReferences());
		
		// tasks
		children.addAll(element.getTasks());
		
		if (element.getSelfVariable() != null) {
			children.add(element.getSelfVariable());
		}

		return children;
	}

	private static Collection<Object> getChildren(Operation element) {
		Collection<Object> children = new ArrayList<Object>();

		// tasks
		children.addAll(element.getTasks());

		// parameters
		children.addAll(element.getParameters());

		// actions
		children.addAll(element.getActions());

		return children;
	}

	private static Collection<Object> getChildren(Attribute element) {
		return new ArrayList<Object>(element.getTasks());
	}

	private static Collection<Object> getChildren(Reference element) {
		return new ArrayList<Object>(element.getTasks());
	}

	private static Collection<Object> getChildren(DesignPatternCatalog element) {
		Collection<Object> children = new ArrayList<Object>();

		// collect categories without parent
		children.addAll(SpecificationUtil.getRootCategories(element));

		// collect patterns without category
		children.addAll(SpecificationUtil.getRootPatterns(element));

		return children;
	}

	private static Collection<Object> getChildren(Category element) {
		Collection<Object> children = new ArrayList<Object>();

		// category children
		children.addAll(element.getChildren());

		// patterns
		children.addAll(element.getPatterns());

		return children;
	}

	private static Collection<Object> getChildren(DesignPattern element) {
		return new ArrayList<Object>(element.getSpecifications());
	}
	
	private static Collection<Object> getChildren(CallAction element) {
		Collection<Object> children = new ArrayList<Object>();

		if (element.getTarget() != null) {
			children.add(element.getTarget());
		}
		
		children.addAll(element.getAssignments());

		if (element.getResultVariable() != null) {
			children.add(element.getResultVariable());
		}

		// tasks
		children.addAll(element.getTasks());

		return children;
	}
	
	private static Collection<Object> getChildren(ParameterAssignment element) {
		Collection<Object> children = new ArrayList<Object>();

		if (element.getParameter() != null) {
			children.add(element.getParameter());
		}
		
		if (element.getAssignedVariable() != null) {
			children.add(element.getAssignedVariable());
		}

		// tasks
		children.addAll(element.getTasks());

		return children;
	}
	
	private static Collection<Object> getChildren(CreateAction element) {
		Collection<Object> children = new ArrayList<Object>();

		if (element.getInstantiatedType() != null) {
			children.add(element.getInstantiatedType());
		}

		if (element.getResultVariable() != null) {
			children.add(element.getResultVariable());
		}

		// tasks
		children.addAll(element.getTasks());

		return children;
	}
	
	private static Collection<Object> getChildren(ReadAction element) {
		Collection<Object> children = new ArrayList<Object>();

		if (element.getAccessedVariable() != null) {
			children.add(element.getAccessedVariable());
		}
		
		if (element.getTarget() != null) {
			children.add(element.getTarget());
		}
		
		// tasks
		children.addAll(element.getTasks());

		return children;
	}
	
	private static Collection<Object> getChildren(WriteAction element) {
		Collection<Object> children = new ArrayList<Object>();

		if (element.getAccessedVariable() != null) {
			children.add(element.getAccessedVariable());
		}
		
		if (element.getTarget() != null) {
			children.add(element.getTarget());
		}
		
		if (element.getAssignedVariable() != null) {
			children.add(element.getAssignedVariable());
		}
		
		// tasks
		children.addAll(element.getTasks());

		return children;
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object input) {
		super.inputChanged(viewer, oldInput, input);

		showDiagram = (input instanceof Diagram);
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof DesignPattern) {
			// take first category
			for (Category category : ((DesignPattern) element).getCategories()) {
				return category;
			}
		}
		return super.getParent(element);
	}

	@Override
	public Object[] getElements(Object element) {
		if (element instanceof PictogramElement) {
			PictogramElement pe = (PictogramElement) element;
			EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			return new Object[] { bo };
		}

		return super.getElements(element);
	}
}
