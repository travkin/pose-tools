package de.upb.pose.specification.ui;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.jface.resource.ImageDescriptor;

import de.upb.pose.core.ui.runtime.ImagesImpl;


public final class SpecificationUiImages extends ImagesImpl {
	public static final String ADD_CATEGORY = "icons/editor/add_category.png"; //$NON-NLS-1$
	public static final String ADD_PATTERN = "icons/editor/add_pattern.png"; //$NON-NLS-1$
	public static final String ADD_SPECIFICATION = "icons/editor/add_specification.png"; //$NON-NLS-1$

	private static Collection<String> paths;

	public static ImageDescriptor getDescriptor(String path) {
		return Activator.get().getImageDescriptor(path);
	}

	public static Collection<String> getPaths() {
		if (paths == null) {
			paths = new HashSet<String>();

			paths.add(ADD_CATEGORY);
			paths.add(ADD_PATTERN);
			paths.add(ADD_SPECIFICATION);
		}
		return paths;
	}
}
