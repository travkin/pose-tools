/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddFeature;
import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.util.SetUtil;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractShapeWithLabelAddFeature extends AddFeature {

	public AbstractShapeWithLabelAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	protected abstract EObject getBusinessObject(IAddContext context);
	
	protected abstract String getLabelTextForBusinessObject(IAddContext context);
	
	protected abstract FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context);
	
	protected abstract boolean updateSetFragments();
	
	protected boolean enableDirectEditing() {
		return true;
	}

	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		Rectangle rectangle = GraphicsAlgorithmsFactory.addRectangle(parentShapePE, colorWhite, colorBlack, LineStyle.SOLID);
		return rectangle;
	}
	
	protected Text createLabelAndOtherGraphicsAlgorithms(IAddContext context, GraphicsAlgorithm labelParent) {
		String textValue = this.getLabelTextForBusinessObject(context);
		Font font = this.getLabelFontDescriptionForBusinessObject(context).manage(getDiagram());
		Color colorBlack = manageColor(IColorConstant.BLACK);
		
		return GraphicsAlgorithmsFactory.addLabel(labelParent, textValue, colorBlack, font);
	}
	
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		// do nothing
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IAdd#add(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	public PictogramElement add(IAddContext context) {
		EObject businessObject = this.getBusinessObject(context);
		ContainerShape parentPE = context.getTargetContainer();
		if (parentPE == null) {
			parentPE = getDiagram();
		}
		
		// create PE
		ContainerShape shapePE = Graphiti.getPeService().createContainerShape(parentPE, true);
		Graphiti.getPeCreateService().createChopboxAnchor(shapePE);
		
		link(shapePE, businessObject);
		
		// frame
		GraphicsAlgorithm labelParent = this.createLabelParent(shapePE);
		
		// text
		Text label = createLabelAndOtherGraphicsAlgorithms(context, labelParent);
		
		updatePictogramElement(shapePE);
		
		initializeSizeAndLocation(context, shapePE, labelParent);
		
		layoutPictogramElement(shapePE);
		
		// configure direct editing
		if (this.enableDirectEditing()) {
			IDirectEditingInfo dei = getFeatureProvider().getDirectEditingInfo();
			dei.setActive(true);
			dei.setMainPictogramElement(shapePE);
			dei.setPictogramElement(shapePE);
			dei.setGraphicsAlgorithm(label);
		}
		
		if (context.getTargetContainer() != null) {
			layoutPictogramElement(context.getTargetContainer());
		}
		
		if (updateSetFragments()) {
			SetUtil.updateSets(getDiagram());
		}
		
		return shapePE;
	}

}
