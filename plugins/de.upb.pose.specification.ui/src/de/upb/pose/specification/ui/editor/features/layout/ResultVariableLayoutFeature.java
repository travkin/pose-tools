/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class ResultVariableLayoutFeature extends AbstractShapeWithLabelLayoutFeature {

	private static final Size LABEL_PADDING = new Size(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
	
	public ResultVariableLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof ResultVariable;
		}
		return false;
	}
	
	@Override
	protected Size getLabelPadding() {
		return LABEL_PADDING;
	}
	
}
