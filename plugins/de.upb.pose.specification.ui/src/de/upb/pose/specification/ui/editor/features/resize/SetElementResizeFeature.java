package de.upb.pose.specification.ui.editor.features.resize;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.impl.DefaultResizeShapeFeature;

import de.upb.pose.specification.ui.util.SetUtil;

public class SetElementResizeFeature extends DefaultResizeShapeFeature {
	
	public SetElementResizeFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void resizeShape(IResizeShapeContext context) {
		super.resizeShape(context);
		
		SetUtil.updateSets(getDiagram());
	}
}
