package de.upb.pose.specification.ui.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.upb.pose.core.ui.CoreUiImages;
import de.upb.pose.core.ui.editor.MultiPageDiagramEditor;
import de.upb.pose.core.ui.editor.MultiPageDiagramEditorOverviewPage;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.ui.editor.provider.SpecificationDiagramTypeProvider;
import de.upb.pose.specification.ui.outline.SpecificationEditorOutlinePageContentProvider;
import de.upb.pose.specification.util.SpecificationUtil;

public class SpecificationEditorOverviewPage extends MultiPageDiagramEditorOverviewPage implements IMenuListener {
	
	private Action addCategoryAction;
	private Action addPatternAction;
	private Action addSpecificationAction;
	private Action openSpecificationAction;
	private Action removeAction;

	private TreeViewer viewer;

	public SpecificationEditorOverviewPage(MultiPageDiagramEditor editor) {
		super(editor);

		// create add actions
		addCategoryAction = createAddAction("Add Category", SpecificationPackage.Literals.CATEGORY);
		addPatternAction = createAddAction("Add DesignPattern", SpecificationPackage.Literals.DESIGN_PATTERN);
		addSpecificationAction = createAddAction("Add PatternSpecification", SpecificationPackage.Literals.PATTERN_SPECIFICATION);

		// create open specification action
		ImageDescriptor icon = CoreUiImages.getDescriptor(CoreUiImages.FIND);
		openSpecificationAction = new Action("Open PatternSpecification", icon) {
			@Override
			public void run() {
				handleOpen();
			}
		};
		openSpecificationAction.setEnabled(false);

		// create remove element action
		icon = CoreUiImages.getDescriptor(CoreUiImages.REMOVE);
		removeAction = new Action("Remove Element", icon) {
			@Override
			public void run() {
				handleRemove();
			}
		};
		removeAction.setEnabled(false);
	}

	private Action createAddAction(String name, final EClass type) {
		Action action = new Action(name, SpecificationImages.getDescriptor(type)) {
			@Override
			public void run() {
				handleAdd(type);
			}
		};
		action.setEnabled(false);
		return action;
	}

	@Override
	public void menuAboutToShow(IMenuManager manager) {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

		if (selection.size() == 1) {
			Object element = selection.getFirstElement();

			if (element instanceof DesignPatternCatalog) {
				manager.add(addCategoryAction);
				manager.add(addPatternAction);
			} else if (element instanceof Category) {
				manager.add(addCategoryAction);
				manager.add(addPatternAction);
				manager.add(new Separator());
				manager.add(removeAction);
			} else if (element instanceof DesignPattern) {
				manager.add(addSpecificationAction);
				manager.add(new Separator());
				manager.add(removeAction);
			} else if (element instanceof PatternSpecification) {
				manager.add(openSpecificationAction);
				manager.add(new Separator());
				manager.add(removeAction);
			}
		}
	}

	@Override
	public void setFocus() {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.getControl().setFocus();
		}
	}

	@Override
	public void refresh() {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.refresh();
		}
	}

	@Override
	protected void createWidgets(FormToolkit toolkit, Composite parent) {
		// tree viewer
		Tree tree = toolkit.createTree(parent, SWT.NONE);
		FormData treeData = new FormData();
		treeData.left = new FormAttachment(0, 6);
		treeData.right = new FormAttachment(100, -6);
		treeData.top = new FormAttachment(0, 6);
		treeData.bottom = new FormAttachment(100, -6);
		tree.setLayoutData(treeData);

		viewer = new TreeViewer(tree);
		viewer.setContentProvider(new SpecificationEditorOutlinePageContentProvider(getAdapterFactory()));
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(getAdapterFactory()));
		viewer.setAutoExpandLevel(2);
		viewer.setInput(getRootElement());

		// expand or open element on double click
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				Object selected = ((IStructuredSelection) event.getSelection()).getFirstElement();
				if (selected instanceof PatternSpecification) {
					handleOpen();
				} else {
					// toggle expand state
					boolean expanded = viewer.getExpandedState(selected);
					viewer.setExpandedState(selected, !expanded);
				}
			}
		});

		// update actions on selection change
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

				if (selection.size() == 1) {
					Object element = selection.getFirstElement();

					boolean isContainer = (element instanceof DesignPatternCatalog || element instanceof Category);
					addCategoryAction.setEnabled(isContainer);
					addPatternAction.setEnabled(isContainer);
					addSpecificationAction.setEnabled(element instanceof DesignPattern);
					openSpecificationAction.setEnabled(element instanceof PatternSpecification);
					removeAction.setEnabled(true);
				} else {
					addCategoryAction.setEnabled(false);
					addPatternAction.setEnabled(false);
					addSpecificationAction.setEnabled(false);
					openSpecificationAction.setEnabled(false);
					removeAction.setEnabled(false);
				}

				updateToolBar();
			}
		});
		setSelectionProvider(viewer);
		viewer.addSelectionChangedListener(this);
		viewer.getTree().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				selectionChanged(new SelectionChangedEvent(viewer, viewer.getSelection()));
			}
		});

		// call remove on [DEL] key
		tree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (SWT.DEL == e.character) {
					IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

					if (selection.size() == 1) {
						Object selected = selection.getFirstElement();
						if (selected instanceof EObject && !(selected instanceof DesignPatternCatalog)) {
							handleRemove();
						}
					}
				}
			}
		});

		// create context menu
		createContextMenu(viewer);
	}

	private void createContextMenu(TreeViewer viewer) {
		// create menu manager
		MenuManager manager = new MenuManager("#PopUp");
		manager.setRemoveAllWhenShown(true);
		manager.addMenuListener(this);

		// add the menu
		Menu menu = manager.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
	}

	@Override
	protected void selectionChanged(ISelection selection) {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.setSelection(selection, true);
		}
	}

	@Override
	protected void createToolBar(IToolBarManager tbm) {
		tbm.add(addCategoryAction);
		tbm.add(addPatternAction);
		tbm.add(addSpecificationAction);
		tbm.add(new Separator());
		tbm.add(removeAction);
	}

	protected void handleOpen() {
		PatternSpecification specification = null;

		Object selected = ((IStructuredSelection) viewer.getSelection()).getFirstElement();
		if (selected instanceof PatternSpecification) {
			specification = (PatternSpecification) selected;
		} else if (selected instanceof DesignPattern) {
			DesignPattern pattern = (DesignPattern) selected;
			if (pattern.getSpecifications().size() == 1) {
				specification = pattern.getSpecifications().get(0);
			}
		}

		if (specification != null) {
			openElement(specification);
		}
	}

	protected void handleAdd(final EClass type) {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

		if (selection.size() == 1) {
			String name = null;
			EObject newElement = null;
			if (SpecificationPackage.Literals.CATEGORY.equals(type)) {
				newElement = SpecificationFactory.eINSTANCE.createCategory();
				name = "Create Category";
			} else if (SpecificationPackage.Literals.DESIGN_PATTERN.equals(type)) {
				newElement = SpecificationFactory.eINSTANCE.createDesignPattern();
				name = "Create DesignPattern";
			} else if (SpecificationPackage.Literals.PATTERN_SPECIFICATION.equals(type)) {
				newElement = SpecificationFactory.eINSTANCE.createPatternSpecification();
				name = "Create PatternSpecification";
			}

			// create command
			final Object selected = selection.getFirstElement();
			final EObject element = newElement;
			RecordingCommand command = new RecordingCommand(getEditingDomain(), name) {
				@Override
				protected void doExecute() {
					// category
					if (SpecificationPackage.Literals.CATEGORY.equals(type)) {
						if (selected instanceof DesignPatternCatalog) {
							((DesignPatternCatalog) selected).getCategories().add((Category) element);
						} else if (selected instanceof Category) {
							((Category) selected).getChildren().add((Category) element);
						}
					}

					// pattern
					if (SpecificationPackage.Literals.DESIGN_PATTERN.equals(type)) {
						if (selected instanceof DesignPatternCatalog) {
							((DesignPatternCatalog) selected).getPatterns().add((DesignPattern) element);
						} else if (selected instanceof Category) {
							SpecificationUtil.getCatalog((Category) selected).getPatterns().add((DesignPattern) element);
							((Category) selected).getPatterns().add((DesignPattern) element);
						}
					}

					// specification
					if (SpecificationPackage.Literals.PATTERN_SPECIFICATION.equals(type)) {
						if (selected instanceof DesignPattern) {
							((DesignPattern) selected).getSpecifications().add((PatternSpecification) element);

							// create diagram for it
							String dtid = SpecificationDiagramTypeProvider.ID;
							Diagram diagram = Graphiti.getPeService().createDiagram(dtid,
									((PatternSpecification) element).getName(), true);
							PictogramLink link = PictogramsFactory.eINSTANCE.createPictogramLink();
							link.getBusinessObjects().add(element);
							link.setPictogramElement(diagram);

							getRootElement().getChildren().add(diagram);
						}
					}
				}
			};

			execute(command);

			// select the created element
			viewer.setSelection(new StructuredSelection(element), true);
		}
	}

	protected void handleRemove() {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();

		if (selection.size() == 1) {
			Object selected = selection.getFirstElement();

			if (selected instanceof EObject) {
				final EObject element = (EObject) selected;
				Object parent = ((ITreeContentProvider) viewer.getContentProvider()).getParent(element);

				RecordingCommand command = new RecordingCommand(getEditingDomain(), "Delete Element") {
					@Override
					protected void doExecute() {
						List<EObject> list = new ArrayList<EObject>();
						if (element instanceof Category) {
							fill(list, (Category) element);
						} else if (element instanceof DesignPattern) {
							fill(list, (DesignPattern) element);
						} else if (element instanceof PatternSpecification) {
							fill(list, (PatternSpecification) element);
						}

						for (EObject eObject : list) {
							EcoreUtil.delete(eObject, true);
						}
					}

					private void fill(List<EObject> list, Category element) {
						// delete categories
						for (Category child : element.getChildren()) {
							fill(list, child);
						}

						// delete patterns
						for (DesignPattern pattern : element.getPatterns()) {
							fill(list, pattern);
						}

						// delete element
						list.add(element);
					}

					private void fill(List<EObject> list, DesignPattern element) {
						// delete specifications
						for (PatternSpecification specification : element.getSpecifications()) {
							fill(list, specification);
						}

						// delete element
						list.add(element);
					}

					private void fill(List<EObject> list, PatternSpecification element) {
						// delete diagram
						list.add(getDiagram(element));

						// delete element
						list.add(element);
					}
				};

				execute(command);

				// select the container element
				viewer.setSelection(new StructuredSelection(parent), true);
			}
		}
	}

	@Override
	protected Image getHeaderImage() {
		return SpecificationImages.get(SpecificationPackage.Literals.DESIGN_PATTERN_CATALOG);
	}

	@Override
	protected String getHeaderText() {
		return "Design Pattern Catalog";
	}
}
