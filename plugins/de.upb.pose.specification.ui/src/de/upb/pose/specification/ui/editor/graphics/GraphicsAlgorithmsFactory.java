/**
 * 
 */
package de.upb.pose.specification.ui.editor.graphics;

import org.eclipse.graphiti.mm.GraphicsAlgorithmContainer;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.services.Graphiti;

/**
 * @author Dietrich Travkin
 */
public class GraphicsAlgorithmsFactory {

	public static final int LINE_WIDTH = 1;
	public static final int FILLED_CIRCLE_DECORATOR_WIDTH = 5;
	public static final int ROUNDED_RECTANGLE_CORNER_WIDTH = 20;

	public static final int PADDING_DEFAULT = 4;
	public static final int PADDING_SMALL = 2;
	
	public static Polyline addOpenArrow(ConnectionDecorator parentDecorator, Color arrowColor) {
		int[] arrowPoints =	{ -10, 5, 0, 0, -10, -5 };
		Polyline arrowDrawer = Graphiti.getGaService().createPolyline(parentDecorator, arrowPoints);
		arrowDrawer.setForeground(arrowColor);
		arrowDrawer.setLineWidth(LINE_WIDTH);
		
		return arrowDrawer;
	}
	
	public static Polyline addOrthogonalLine(ConnectionDecorator parentDecorator, Color lineColor) {
		int[] linePoints =	{ -15, 5, -15, -5 };
		Polyline arrowDrawer = Graphiti.getGaService().createPolyline(parentDecorator, linePoints);
		arrowDrawer.setForeground(lineColor);
		arrowDrawer.setLineWidth(LINE_WIDTH);
		
		return arrowDrawer;
	}

	public static Polygon addFilledDiamond(ConnectionDecorator parentDecorator, Color diamondColor) {
		int[] diamondPoints = new int[] { 0, 0, -7, 5, -14, 0, -7, -5, 0, 0 };
		Polygon diamondDrawer = Graphiti.getGaService().createPolygon(parentDecorator, diamondPoints);
		diamondDrawer.setFilled(true);
		diamondDrawer.setBackground(diamondColor);
		diamondDrawer.setForeground(diamondColor);
		diamondDrawer.setLineWidth(LINE_WIDTH);
		diamondDrawer.setLineVisible(true);
		return diamondDrawer;
	}

	public static Polygon addFilledArrow(ConnectionDecorator parentDecorator, Color fillColor, Color lineColor) {
		int[] arrowPoints =  new int[] { 0, 0, -12, -5, -12, 5 };
		Polygon arrowDrawer = Graphiti.getGaService().createPolygon(parentDecorator, arrowPoints);
		updateFillColorIfAvailable(arrowDrawer, fillColor);
		arrowDrawer.setForeground(lineColor);
		arrowDrawer.setLineWidth(LINE_WIDTH);
		arrowDrawer.setLineVisible(true);
		return arrowDrawer;
	}

	public static Ellipse addFilledCircle(ConnectionDecorator parentDecorator, Color circleColor) {
		Ellipse ellipseDrawer = Graphiti.getGaService().createEllipse(parentDecorator);
		ellipseDrawer.setFilled(true);
		ellipseDrawer.setBackground(circleColor);
		ellipseDrawer.setLineVisible(false);
		
		ellipseDrawer.setX(0);
		ellipseDrawer.setY(0);
		ellipseDrawer.setWidth(FILLED_CIRCLE_DECORATOR_WIDTH);
		ellipseDrawer.setHeight(FILLED_CIRCLE_DECORATOR_WIDTH);
		
		return ellipseDrawer;
	}

	public static Text addLabel(ConnectionDecorator parentDecorator, String labelText, Color fontColor) {
		// TODO explicitly set font? call addLabel(GraphicsAlgorithmContainer parent, String labelText, Color fontColor, Font font)
		Text textDrawer = Graphiti.getGaService().createText(parentDecorator, labelText);
		textDrawer.setForeground(fontColor);
		textDrawer.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		textDrawer.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		return textDrawer;
	}
	
	public static Text addLabel(GraphicsAlgorithmContainer parent, String labelText, Color fontColor, Font font) {
		Text textDrawer = Graphiti.getGaService().createText(parent, labelText);
		textDrawer.setForeground(fontColor);
		textDrawer.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		textDrawer.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		textDrawer.setFont(font);
		return textDrawer;
	}
	
	private static void updateFillColorIfAvailable(GraphicsAlgorithm drawnElement, Color fillColor) {
		if (fillColor != null) {
			drawnElement.setBackground(fillColor);
			drawnElement.setFilled(true);
		} else {
			drawnElement.setFilled(false);
		}
	}
	
	public static Rectangle addRectangle(GraphicsAlgorithmContainer parent, Color fillColor, Color lineColor, LineStyle lineStyle) {
		Rectangle rectangleDrawer = Graphiti.getGaService().createRectangle(parent);
		rectangleDrawer.setForeground(lineColor);
		updateFillColorIfAvailable(rectangleDrawer, fillColor);
		rectangleDrawer.setLineWidth(LINE_WIDTH);
		rectangleDrawer.setLineStyle(lineStyle);
		rectangleDrawer.setLineVisible(true);
		return rectangleDrawer;
	}
	
	public static RoundedRectangle addRoundedRectangle(GraphicsAlgorithmContainer parent, Color fillColor, Color lineColor, LineStyle lineStyle) {
		RoundedRectangle rectangleDrawer = Graphiti.getGaService().createRoundedRectangle(
				parent, ROUNDED_RECTANGLE_CORNER_WIDTH, ROUNDED_RECTANGLE_CORNER_WIDTH);
		rectangleDrawer.setForeground(lineColor);
		updateFillColorIfAvailable(rectangleDrawer, fillColor);
		rectangleDrawer.setLineWidth(LINE_WIDTH);
		rectangleDrawer.setLineStyle(lineStyle);
		rectangleDrawer.setLineVisible(true);
		return rectangleDrawer;
	}
	
	public static Polyline addLine(GraphicsAlgorithmContainer parent, Color lineColor, LineStyle lineStyle) {
		Polyline lineDrawer = Graphiti.getGaService().createPolyline(parent);
		lineDrawer.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);
		lineDrawer.setForeground(lineColor);
		lineDrawer.setLineStyle(lineStyle);
		lineDrawer.setLineVisible(true);
		return lineDrawer;
	}
	
}
