package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class TypeLayoutFeature extends AbstractShapeWithLabelLayoutFeature
{
	public TypeLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof Type;
		}
		return false;
	}
	
	@Override
	public boolean layoutShape(ILayoutContext context) {
		boolean anythingChanged = false;
		
		GraphicsAlgorithm shape = getShape(context);
		Size minSize = determineMinimalShapeSize(context);
		
		// only increase shape's size if necessary (super implementation forces reducing the size if bigger than necessary)
		if (shape.getWidth() < minSize.getWidth())
		{
			shape.setWidth(minSize.getWidth());
			anythingChanged = true;
		}
		if (shape.getHeight() < minSize.getHeight())
		{
			shape.setHeight(minSize.getHeight());
			anythingChanged = true;
		}
		
		// re-layout all outgoing references' node positions
		Type type = (Type) this.getBusinessObjectForPictogramElement(context.getPictogramElement());
		for (Reference reference : type.getReferences()) {
			ContainerShape referencePE = PictogrammElementFinder.get() // it's the node PE of the reference
					.findMainPictogramElementInDiagramForBusinessObject(getDiagram(), reference);

			// find the layout feature for this node and re-layout it
			getFeatureProvider().layoutIfPossible(new LayoutContext(referencePE));
		}
		
		return anythingChanged;
	}
	
}
