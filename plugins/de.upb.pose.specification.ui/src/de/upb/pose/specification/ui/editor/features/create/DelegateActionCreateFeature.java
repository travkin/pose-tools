package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.types.Operation;

public class DelegateActionCreateFeature extends CallActionCreateFeature {
	
	public DelegateActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Delegate");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.DELEGATE_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		DelegateAction bo = ActionsFactory.eINSTANCE.createDelegateAction();
		bo.setCalledOperation((Operation) target);
		return bo;
	}
}
