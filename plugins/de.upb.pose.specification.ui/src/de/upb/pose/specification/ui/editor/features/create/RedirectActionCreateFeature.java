package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.types.Operation;

public class RedirectActionCreateFeature extends CallActionCreateFeature {
	
	public RedirectActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Redirect");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.REDIRECT_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		RedirectAction bo = ActionsFactory.eINSTANCE.createRedirectAction();
		bo.setCalledOperation((Operation) target);
		return bo;
	}
}
