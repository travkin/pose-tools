/**
 * 
 */
package de.upb.pose.specification.ui.editor.features;

import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;


/**
 * @author Dietrich Travkin
 */
public class PatternSpecificationLabelProviderService {

	private SpecificationEditorFeatureProvider featureProvider;
	
	public PatternSpecificationLabelProviderService(SpecificationEditorFeatureProvider featureProvider) {
		this.featureProvider = featureProvider;
	}
	
	protected IShapeWithLabelLayoutFeature findLayoutFeature(PictogramElement pictogramElement) {
		ILayoutFeature feature = this.featureProvider.getLayoutFeature(new LayoutContext(pictogramElement));
		if (feature != null && feature instanceof IShapeWithLabelLayoutFeature) {
			return (IShapeWithLabelLayoutFeature) feature;
		}
		return null;
	}
	
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		IShapeWithLabelLayoutFeature layoutFeature = this.findLayoutFeature(pictogramElement);
		if (layoutFeature != null && layoutFeature.canHandle(labelIdentifier, pictogramElement)) {
			return true;
		}
		return false;
	}

	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
		if (this.canHandle(labelIdentifier, pictogramElement)) {
			IShapeWithLabelLayoutFeature layoutFeature = this.findLayoutFeature(pictogramElement);
			return layoutFeature.getLabel(labelIdentifier, pictogramElement);
		}
		return null;
	}

}
