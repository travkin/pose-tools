/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

/**
 * @author Dietrich Travkin
 */
public class ActionParentConnectionAddFeature extends AddConnectionFeature {

	public ActionParentConnectionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see de.upb.pose.core.features.AddConnectionFeature#add(org.eclipse.graphiti.features.context.IAddConnectionContext)
	 */
	@Override
	protected Connection add(IAddConnectionContext context) {
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());
		
		Object sourceBO = getBusinessObjectForPictogramElement(context.getSourceAnchor().getParent());
		Action actionBO = (Action) getBusinessObjectForPictogramElement(context.getTargetAnchor().getParent());
		link(connection, actionBO);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);

		if (sourceBO instanceof Operation) {
			Operation parentOperation = (Operation) sourceBO;
			
			EList<Action> allActions = parentOperation.getActions();
			Assert.isTrue(allActions.contains(actionBO));
			Assert.isTrue(allActions.indexOf(actionBO) == 0);

			// draw a parent connection line with a diamond
			GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.DASH);
			GraphicsFactory.addConnectionDecorator_FilledDiamond_SourceEnd(connection, colorBlack);
		} else if (sourceBO instanceof Action) {
			Action predecessorAction = (Action) sourceBO;
			
			EList<Action> allActions = predecessorAction.getOperation().getActions();
			Assert.isTrue(allActions.contains(actionBO));
			Assert.isTrue(allActions.indexOf(actionBO) == allActions.size() - 1);
			
			// draw a predecessor connection line with an arrow
			GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.SOLID);
			GraphicsFactory.addConnectionDecorator_FilledArrow_TargetEnd(connection, colorBlack, colorBlack);
		}
		
		return connection;
	}

	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return ActionsPackage.Literals.ACTION__OPERATION.equals(element);
	}

}
