/**
 * 
 */
package de.upb.pose.specification.ui.editor.features;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class PictogrammElementFinder {
	
	private static PictogrammElementFinder instance = null;
	
	private PictogrammElementFinder() {}
	
	public static PictogrammElementFinder get() {
		if (instance == null) {
			instance = new PictogrammElementFinder();
		}
		return instance;
	}

	public List<PictogramElement> findAllPictogrammElementsInDiagramForBusinessObject(Diagram diagram, EObject businessObject) {
		List<PictogramElement> pictogrammElements = Graphiti.getLinkService().getPictogramElements(diagram, businessObject);
		return pictogrammElements;
	}
	
	protected PictogramElement findMainPictogramElementInDiagramForBusinessObject(Diagram diagram, EObject businessObject) {
		if (businessObject instanceof Type) {
			return findMainPictogramElementInDiagramForBusinessObject(diagram, (Type) businessObject);
		} else {
			List<PictogramElement> allPictogrammElements = this.findAllPictogrammElementsInDiagramForBusinessObject(diagram, businessObject);
			if (!allPictogrammElements.isEmpty()) {
				return allPictogrammElements.get(0); // take the first one
			}
		}
		return null;
	}
	
	public ContainerShape findMainPictogramElementInDiagramForBusinessObject(Diagram diagram, Type typeBO) {
		return findTheOnlyContainerShape(this.findAllPictogrammElementsInDiagramForBusinessObject(diagram, typeBO));
	}
	
	public ContainerShape findMainPictogramElementInDiagramForBusinessObject(Diagram diagram, Reference referenceBO) {
		return findTheOnlyContainerShape(this.findAllPictogrammElementsInDiagramForBusinessObject(diagram, referenceBO));
	}
	
	public ContainerShape findMainPictogramElementInDiagramForBusinessObject(Diagram diagram, ResultVariable resultVariableBO) {
		return findTheOnlyContainerShape(this.findAllPictogrammElementsInDiagramForBusinessObject(diagram, resultVariableBO));
	}
	
	private ContainerShape findTheOnlyContainerShape(List<PictogramElement> pictogrammElements) {
		ContainerShape shape = null;
		for (PictogramElement pe: pictogrammElements) {
			if (pe instanceof ContainerShape) {
				Assert.isTrue(shape == null); // only one container shape allowed
				shape = (ContainerShape) pe;
			}
		}
		Assert.isNotNull(shape);
		return shape;
	}
	
}
