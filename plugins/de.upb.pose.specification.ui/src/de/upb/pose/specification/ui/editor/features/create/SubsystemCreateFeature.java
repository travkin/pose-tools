/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.create;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.subsystems.SubsystemsFactory;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.ui.editor.helpers.NameGenerator;

/**
 * @author Dietrich Travkin
 */
public class SubsystemCreateFeature extends CreateFeature {

	public SubsystemCreateFeature(IFeatureProvider fp) {
		super(fp, "Subsystem");
	}

	/**
	 * @see org.eclipse.graphiti.features.impl.AbstractCreateFeature#getCreateImageId()
	 */
	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(SubsystemsPackage.Literals.SUBSYSTEM);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.ICreate#canCreate(org.eclipse.graphiti.features.context.ICreateContext)
	 */
	@Override
	public boolean canCreate(ICreateContext context) {
		ContainerShape pe = context.getTargetContainer();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof PatternSpecification || bo instanceof SetFragment || bo instanceof Subsystem;
	}

	/**
	 * @see de.upb.pose.core.features.CreateFeature#doCreate(org.eclipse.graphiti.features.context.ICreateContext)
	 */
	@Override
	protected EObject doCreate(ICreateContext context) {
		ContainerShape targetContainerPE = context.getTargetContainer();
		Object containerBO = getBusinessObjectForPictogramElement(targetContainerPE);

		// create element
		Subsystem subsystemBO = SubsystemsFactory.eINSTANCE.createSubsystem();

		// add to specification
		PatternSpecification specification = null;
		if (containerBO instanceof PatternSpecification) {
			specification = (PatternSpecification) containerBO;
		} else if (containerBO instanceof PatternElement) {
			specification = ((PatternElement) containerBO).getSpecification();
		}
		specification.getPatternElements().add(subsystemBO);

		// set initial name
		subsystemBO.setName(createInitialName(specification));
		
		return subsystemBO;
	}
	
	public String createInitialName(PatternSpecification specification) {
		Collection<Subsystem> existingSubsystems = new HashSet<Subsystem>();
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof Subsystem) {
				existingSubsystems.add((Subsystem) element);
			}
		}
		return NameGenerator.generateUniqueName(existingSubsystems, "Subsystem");
	}

}
