package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.types.Type;

public class ProduceActionCreateFeature extends CreateActionCreateFeature {
	
	public ProduceActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Produce");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.PRODUCE_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		ProduceAction bo = ActionsFactory.eINSTANCE.createProduceAction();
		bo.setInstantiatedType((Type) target);
		return bo;
	}
}
