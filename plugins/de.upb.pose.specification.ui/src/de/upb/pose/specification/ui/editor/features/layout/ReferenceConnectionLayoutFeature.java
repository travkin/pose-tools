/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.helpers.ReferenceUtil;

/**
 * @author Dietrich Travkin
 */
public class ReferenceConnectionLayoutFeature extends AbstractLayoutFeature {

	public ReferenceConnectionLayoutFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof Connection) {
			return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof Reference;
		}
		return false;
	}

	/**
	 * @see org.eclipse.graphiti.func.ILayout#layout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layout(ILayoutContext context) {
		Connection referencePE = (Connection) context.getPictogramElement();
		
		Text label = ReferenceUtil.getCardinalityText(referencePE);
		String currentText = label.getValue();
		Size labelSize = GS.getSize(label.getFont(), currentText).padding(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
		
		label.setWidth(labelSize.getWidth());
		label.setHeight(labelSize.getHeight());
		
		label.setX(0);
		label.setY(0);
		
		return true;
	}

}
