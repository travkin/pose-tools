package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public class PatternEnvironmentUtil extends PatternElementHelper {
	public static Text getText(PictogramElement pe) {
		Assert.isNotNull(pe);

		return (Text) getTextContainer(pe).getGraphicsAlgorithmChildren().get(0);
	}

	public static GraphicsAlgorithm getTextContainer(PictogramElement pe) {
		Assert.isNotNull(pe);

		return pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(8);
	}

	public static FontDescription getFont() {
		return FontConstants.FONT_10_NORMAL_ITALIC;
	}

	public static String getText(PatternEnvironment bo) {
		Assert.isNotNull(bo);

		return bo.getName();
	}

	public static void sizeEllipse(Size size, Ellipse ellipse, int index) {
		int w = size.getWidth();
		int h = size.getHeight();

		int x = -1;
		int y = -1;
		int width = -1;
		int height = -1;

		switch (index) {
		case 0:
			x = w * 1 / 8;
			y = 0;
			width = w / 4;
			height = h / 2;
			break;
		case 1:
			x = w * 3 / 8;
			y = 0;
			width = w / 4;
			height = h / 2;
			break;
		case 2:
			x = w * 5 / 8;
			y = 0;
			width = w / 4;
			height = h / 2;
			break;
		case 3:
			x = w * 1 / 8;
			y = h / 2;
			width = w / 4;
			height = h / 2;
			break;
		case 4:
			x = w * 3 / 8;
			y = h / 2;
			width = w / 4;
			height = h / 2;
			break;
		case 5:
			x = w * 5 / 8;
			y = h / 2;
			width = w / 4;
			height = h / 2;
			break;
		case 6:
			x = 0;
			y = h / 4;
			width = w / 4;
			height = h / 2;
			break;
		case 7:
			x = w * 6 / 8;
			y = h / 4;
			width = w / 4;
			height = h / 2;
			break;
		default:
			break;
		}

		ellipse.setX(Math.round(x));
		ellipse.setY(Math.round(y));
		ellipse.setWidth(Math.round(width));
		ellipse.setHeight(Math.round(height));
	}
}
