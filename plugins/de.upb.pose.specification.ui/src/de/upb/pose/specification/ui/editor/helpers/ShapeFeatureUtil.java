/**
 * 
 */
package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;

/**
 * @author Dietrich Travkin
 */
public class ShapeFeatureUtil
{
	public static Text findLabel(GraphicsAlgorithm ga)
	{
		if (ga != null && !ga.getGraphicsAlgorithmChildren().isEmpty())
		{
			GraphicsAlgorithm child = ga.getGraphicsAlgorithmChildren().get(0);
			if (child instanceof Text)
			{
				return (Text) child;
			}
		}
		return null;
	}
}
