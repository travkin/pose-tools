package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;

public class OperationAddFeature extends AbstractPolygonShapeWithLabelAddFeature {
	
	public OperationAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Operation;
	}
	
	@Override
	protected Operation getBusinessObject(IAddContext context) {
		return (Operation) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		Operation operationBO = this.getBusinessObject(context);
		return OperationUtil.getPrefixLabelText(operationBO);
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return OperationUtil.getFont(this.getBusinessObject(context));
	}
	
	protected Text createLabelAndOtherGraphicsAlgorithms(IAddContext context, GraphicsAlgorithm labelParent) {
		Text prefixLabel = super.createLabelAndOtherGraphicsAlgorithms(context, labelParent);
		
		// add a second label to display the last bracket and the return type
		String suffixText = OperationUtil.getSuffixLabelText(this.getBusinessObject(context));
		Font font = this.getLabelFontDescriptionForBusinessObject(context).manage(getDiagram());
		Color colorBlack = manageColor(IColorConstant.BLACK);
		
		GraphicsAlgorithmsFactory.addLabel(labelParent, suffixText, colorBlack, font);
		
		return prefixLabel;
	}
}
