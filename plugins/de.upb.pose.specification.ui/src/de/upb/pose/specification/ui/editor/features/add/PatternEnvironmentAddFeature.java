package de.upb.pose.specification.ui.editor.features.add;

import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getFont;
import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getText;
import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getTextContainer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddFeature;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.ui.editor.helpers.PatternElementHelper;
import de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil;

public class PatternEnvironmentAddFeature extends AddFeature {
	public PatternEnvironmentAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public PictogramElement add(IAddContext context) {
		PatternEnvironment bo = (PatternEnvironment) context.getNewObject();

		// create PE
		ContainerShape pe = Graphiti.getPeService().createContainerShape(getDiagram(), true);
		Graphiti.getPeCreateService().createChopboxAnchor(pe);
		link(pe, bo);

		int w = context.getWidth();
		w = w < 100 ? 100 : w;

		int h = context.getHeight();
		h = h < 50 ? 50 : h;

		// frame
		Rectangle ga = Graphiti.getGaService().createInvisibleRectangle(pe);
		ga.setX(context.getX());
		ga.setY(context.getY());
		ga.setWidth(w);
		ga.setHeight(h);

		addCloud(bo, ga);

		Text text = Graphiti.getGaService().createText(getTextContainer(pe), getText(bo));
		text.setFont(getFont().manage(getDiagram()));
		text.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
		text.setForeground(manageColor(IColorConstant.BLACK));

		// enable direct editing
		IDirectEditingInfo dei = getFeatureProvider().getDirectEditingInfo();
		dei.setMainPictogramElement(pe);
		dei.setPictogramElement(pe);
		dei.setGraphicsAlgorithm(text);
		
		// TODO layoutPictogramElement(pe);

		return pe;
	}

	private void addCloud(PatternEnvironment bo, Rectangle ga) {
		Size size = new Size(ga.getWidth(), ga.getHeight());

		// upper three
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 0);
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 1);
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 2);

		// lower three
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 3);
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 4);
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 5);

		// left and right
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 6);
		PatternEnvironmentUtil.sizeEllipse(size, createEllipse(bo, ga), 7);

		// text container
		int w = ga.getWidth();
		int h = ga.getHeight();
		createRectangle(bo, ga, w / 6, h / 4, w - h / 2, h / 2);
	}

	private Ellipse createEllipse(PatternEnvironment bo, Rectangle ga) {
		Ellipse ellipse = Graphiti.getGaService().createEllipse(ga);

		ellipse.setBackground(manageColor(PatternElementHelper.getColor(bo)));
		ellipse.setForeground(manageColor(IColorConstant.BLACK));
		ellipse.setLineStyle(LineStyle.SOLID);
		ellipse.setLineVisible(true);
		ellipse.setLineWidth(1);

		return ellipse;
	}

	private void createRectangle(PatternEnvironment bo, Rectangle ga, double x, double y, double width, double height) {
		Rectangle text = Graphiti.getGaService().createRectangle(ga);
		text.setBackground(manageColor(PatternElementHelper.getColor(bo)));
		text.setLineVisible(false);

		text.setX((int) Math.round(x));
		text.setY((int) Math.round(y));
		text.setWidth((int) Math.round(width));
		text.setHeight((int) Math.round(height));
	}

	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof PatternEnvironment;
	}
}
