package de.upb.pose.specification.ui.properties.sections;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.CorePackage;
import de.upb.pose.core.ui.properties.StringMultiTextSection;

public class CommentableComment extends StringMultiTextSection {
	@Override
	protected EStructuralFeature getFeature() {
		return CorePackage.Literals.COMMENTABLE__COMMENT;
	}

	@Override
	protected String getLabelText() {
		return "Comment";
	}
}
