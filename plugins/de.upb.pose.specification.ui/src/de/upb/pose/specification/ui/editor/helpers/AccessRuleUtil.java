package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.util.SpecificationUtil;

public class AccessRuleUtil extends PatternElementHelper {
	
	public static Text getText(PictogramElement pe) {
		return (Text) getConnection(pe).getConnectionDecorators().get(0).getGraphicsAlgorithm();
	}

	public static boolean isForbidden(PictogramElement pe) {
		return getConnection(pe).getConnectionDecorators().size() == 4;
	}

	public static Connection getConnection(PictogramElement pe) {
		if (pe instanceof Connection) {
			return (Connection) pe;
		}

		if (pe instanceof ConnectionDecorator) {
			return ((ConnectionDecorator) pe).getConnection();
		}

		throw new RuntimeException();
	}

	public static FontDescription getFont() {
		return FontConstants.FONT_10_NORMAL_ITALIC;
	}

	public static String getText(AccessRule bo) {
		StringBuilder builder = new StringBuilder();

		String type = "undefined";
		
		builder.append('\u00ab');
		
		AccessType accessType = bo.getAccessType();
		if (accessType != null)
		{
			type = SpecificationUtil.getAccessTypeLabel(accessType);
		}
		
		builder.append(type);
		builder.append('\u00bb');

		return builder.toString();
	}
}
