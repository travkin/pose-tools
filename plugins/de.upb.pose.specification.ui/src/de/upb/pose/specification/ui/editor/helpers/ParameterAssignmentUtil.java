package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public class ParameterAssignmentUtil extends PatternElementHelper {
	
	public static FontDescription getFont() {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}

	public static String getText(ParameterAssignment bo) {
		Parameter parameter = bo.getParameter();
		Variable variable = bo.getAssignedVariable();

		StringBuilder builder = new StringBuilder();

		if (parameter != null) {
			builder.append(parameter.getName());
		} else {
			builder.append('?');
		}

		builder.append(' ');
		builder.append(':');
		builder.append('=');
		builder.append(' ');

		if (variable != null) {
			builder.append(variable.getName());
		} else {
			builder.append('?');
		}

		return builder.toString();
	}

	public static Text getText(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}
}
