package de.upb.pose.specification.ui.editor.helpers;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public final class OperationUtil extends SetElementHelper {
	
	private OperationUtil() {
		// hide constructor
	}

	public static Polygon getPolygon(PictogramElement pe) {
		return (Polygon) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}

	public static Text getPrefixLabel(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(1);
	}
	
	public static Text getSuffixLabel(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(2);
	}
	
	public static String getPrefixLabelText(PictogramElement pe) {
		Text label = getPrefixLabel(pe);
		return label != null ? label.getValue() : null; 
	}

	public static String getSuffixLabelText(PictogramElement pe) {
		Text label = getSuffixLabel(pe);
		return label != null ? label.getValue() : null; 
	}
	
	public static String getInitialName(Type type) {
		Collection<String> existing = new HashSet<String>();
		for (Operation operation : type.getOperations()) {
			existing.add(operation.getName());
		}

		String prefix = "do";
		String name = prefix;
		int index = 2;
		while (existing.contains(name)) {
			name = prefix + "_" + index;
			index++;
		}

		return name;
	}

	public static FontDescription getFont(Operation bo) {
		if (AbstractionType.ABSTRACT.equals(bo.getAbstraction())) {
			return FontConstants.FONT_10_NORMAL_ITALIC;
		} else {
			return FontConstants.FONT_10_NORMAL_REGULAR;
		}
	}

	
	public static String getPrefixLabelText(Operation operation) {
		return operation.getName() + "(";
	}
	
	public static String getSuffixLabelText(Operation operation) {
		StringBuilder builder = new StringBuilder();
		builder.append(')');
		
		// return type
		if (operation.getType() != null) {
			builder.append(": ");
			builder.append(operation.getType().getName());
		}
		
		return builder.toString();
	}
	
	public static LineStyle getLineStyle(Operation bo) {
		switch (bo.getAbstraction()) {
		case ANY:
			return LineStyle.DASH;
		default:
			return LineStyle.SOLID;
		}
	}
}
