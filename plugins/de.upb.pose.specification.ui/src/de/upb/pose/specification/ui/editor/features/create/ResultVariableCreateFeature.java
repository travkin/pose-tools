/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;

/**
 * @author Dietrich Travkin
 */
public class ResultVariableCreateFeature extends CreateFeature {

	public ResultVariableCreateFeature(IFeatureProvider fp) {
		super(fp, "Result Variable");
	}
	
	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.RESULT_VARIABLE);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.ICreate#canCreate(org.eclipse.graphiti.features.context.ICreateContext)
	 */
	@Override
	public boolean canCreate(ICreateContext context) {
		ResultAction parentActionBO = findParentAction(context);
		return (parentActionBO != null && parentActionBO.getResultVariable() == null);
	}

	@Override
	protected EObject doCreate(ICreateContext context) {
		ResultAction parentActionBO = findParentAction(context);

		ResultVariable resultVariableBO = ActionsFactory.eINSTANCE.createResultVariable();
		resultVariableBO.setName("result");
		resultVariableBO.setCanBeGenerated(true);

		parentActionBO.setResultVariable(resultVariableBO);
		
		return resultVariableBO;
	}

	private ResultAction findParentAction(ICreateContext context) {
		ContainerShape parentActionPE = context.getTargetContainer();
		if (parentActionPE != null) {
			Object bo = getBusinessObjectForPictogramElement(parentActionPE);
			if (bo instanceof ResultAction) {
				ResultAction parentActionBO = (ResultAction) bo;
				return parentActionBO;
			}
		}
		return null;
	}

}
