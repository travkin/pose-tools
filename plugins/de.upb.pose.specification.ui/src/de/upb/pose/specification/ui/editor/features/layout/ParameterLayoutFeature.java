package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class ParameterLayoutFeature extends AbstractShapeWithLabelLayoutFeature
{
	private static final Size LABEL_PADDING = new Size(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
	
	public ParameterLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Parameter;
	}
	
	protected Size getLabelPadding() {
		return LABEL_PADDING;
	}
	
	protected boolean relocateShapeContents(ILayoutContext context) {
		return false; // do nothing, the operation layout does this already
	}
	
}
