package de.upb.pose.specification.ui.editor.helpers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public final class ParameterUtil extends PatternElementHelper {
	
	private ParameterUtil() {
		// hide constructor
	}

	public static Text getText(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}

	public static String getText(Parameter parameter) {
		StringBuilder builder = new StringBuilder();

		builder.append(parameter.getName());
		if (parameter.getType() != null) {
			builder.append(": ");
			builder.append(parameter.getType().getName());
		}
		if (parameter.getOperation() != null) {
			int index = parameter.getOperation().getParameters().indexOf(parameter);
			int numberOfParameter = parameter.getOperation().getParameters().size();
			if (index >= 0 && numberOfParameter - 1 > index) {
				builder.append(',');
			}
		}

		return builder.toString();
	}

	public static FontDescription getFont(Parameter bo) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}
	
	public static String determineInitialParemeterName(Operation parentOperation) {
		List<String> names = new ArrayList<String>(parentOperation.getParameters().size());
		for (Parameter param: parentOperation.getParameters()) {
			names.add(param.getName());
		}
		String prefix = "p";
		String name = prefix;
		int index = 2;
		while (names.contains(name)) {
			name = prefix + index;
			index++;
		}
		return name;
	} 
}
