package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

public class TargetConnectionAddFeature extends AddConnectionFeature {
	
	public TargetConnectionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected boolean canAdd(EObject bo) {
		return ActionsPackage.Literals.CALL_ACTION__TARGET.equals(bo)
			|| ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__TARGET.equals(bo) ;
	}
	
	@Override
	protected Connection add(IAddConnectionContext context) {
		ContainerShape sourceActionPE = (ContainerShape) context.getSourceAnchor().getParent();
		Action sourceActionBO = (Action) getBusinessObjectForPictogramElement(sourceActionPE);
		
		// special handling for self references
		if (context.getSourceAnchor().equals(context.getTargetAnchor())) {
			return addSelfVariableContainerShape(sourceActionBO, sourceActionPE);
		}
		
		// regular handling for references to other variables
		
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());
		
		Color colorBlack = manageColor(IColorConstant.BLACK);
		
		link(connection, sourceActionBO);

		// arrow
		GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.DOT);
		GraphicsFactory.addConnectionDecorator_OpenArrow_TargetEnd(connection, colorBlack);
		
		updatePictogramElement(connection);
		
		return connection;
	}

	private Connection addSelfVariableContainerShape(Action sourceActionBO, ContainerShape sourceActionPE) {
		ContainerShape selfVariableShapePE = Graphiti.getPeService().createContainerShape(sourceActionPE, false);
		SelfVariable selfVariable = findSelfVariable(sourceActionBO); 
		
		link(selfVariableShapePE, new Object[] { selfVariable, sourceActionBO });
		
		Rectangle invisibleParent = Graphiti.getGaService().createInvisibleRectangle(selfVariableShapePE);
		invisibleParent.setLineWidth(0);
		
		String textValue = selfVariable.getName();
		Font font = FontConstants.FONT_9_NORMAL_REGULAR.manage(getDiagram());
		Color colorBlack = manageColor(IColorConstant.BLACK);
		
		GraphicsAlgorithmsFactory.addLabel(invisibleParent, textValue, colorBlack, font);
		
		layoutPictogramElement(selfVariableShapePE);
		layoutPictogramElement(sourceActionPE);
		
		return null; // no connection is created, but a container shape
	}

	private SelfVariable findSelfVariable(Action action) {
		return action.getOperation().getParentType().getSelfVariable();
	}
}
