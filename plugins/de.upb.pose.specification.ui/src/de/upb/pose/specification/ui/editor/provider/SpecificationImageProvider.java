package de.upb.pose.specification.ui.editor.provider;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

import de.upb.pose.specification.SpecificationImages;

public class SpecificationImageProvider extends AbstractImageProvider {
	public static final String INHERITANCE = "icons/extra/inheritance.png"; //$NON-NLS-1$
	public static final String TARGET_CONNECTION = "icons/extra/target_connection.png"; //$NON-NLS-1$

	@Override
	protected void addAvailableImages() {
		addImageFilePath(INHERITANCE, INHERITANCE);
		addImageFilePath(TARGET_CONNECTION, TARGET_CONNECTION);

		for (String path : SpecificationImages.getPaths()) {
			addImageFilePath(path, path);
		}
	}
}
