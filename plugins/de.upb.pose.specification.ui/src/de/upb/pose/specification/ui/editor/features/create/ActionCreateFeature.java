package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.AreaContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.util.SpecificationTextUtil;

public abstract class ActionCreateFeature extends CreateConnectionFeature {
	
	public ActionCreateFeature(IFeatureProvider fp, String name) {
		super(fp, name);
	}
	
	@Override
	protected boolean canStart(EObject source) {
		boolean canStart = false;
		if (source instanceof Operation) {
			Operation operation = (Operation) source;
			EList<Action> actions = operation.getActions();
			// there is no action so far
			if (actions.size() == 0) {
				canStart = true;
			}
		}
		if (source instanceof Action) {
			Action sourceAction = (Action) source;
			EList<Action> actions = sourceAction.getOperation().getActions();
			// predecessor action is the last one
			if (actions.indexOf(sourceAction) == actions.size() - 1) {
				canStart = true;
			}
		}
		return canStart;
	}
	
	protected abstract boolean canFinish(EObject target);
	
	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return canStart(source) && canFinish(target);
	}
	
	@Override
	public Connection create(ICreateConnectionContext context) {
		ContainerShape sourcePE = (ContainerShape) context.getSourcePictogramElement();
		EObject sourceBO = (EObject) getBusinessObjectForPictogramElement(sourcePE);

		ContainerShape targetPE = (ContainerShape) context.getTargetPictogramElement();
		EObject targetBO = (EObject) getBusinessObjectForPictogramElement(targetPE);

		// create element
		Action actionBO = createActionBO(targetBO);
		actionBO.setName(SpecificationTextUtil.getName(actionBO));
		if (sourceBO instanceof Operation) {
			actionBO.setOperation((Operation) sourceBO);
		} else if (sourceBO instanceof Action) {
			Action predecessorAction = (Action) sourceBO;
			actionBO.setOperation(predecessorAction.getOperation());
		}
		
		// add action node
		AreaContext addNodeContext = new AreaContext();
		// the following is a work-around to place the action node in the middle between the source and target node
		// (e.g. callee operation and called operation)
		int x = (int) Math.round(context.getSourceLocation().getX() + (context.getTargetLocation().getX() - context.getSourceLocation().getX()) / 2d);
		int y = (int) Math.round(context.getSourceLocation().getY() + (context.getTargetLocation().getY() - context.getSourceLocation().getY()) / 2d);
		addNodeContext.setLocation(x, y);
		PictogramElement actionPE = getFeatureProvider().addIfPossible(new AddContext(addNodeContext, actionBO));

		// add connection from action to parent operation or predecessor action
		Connection sourceToActionConnection = null;
		if (actionPE != null && actionPE instanceof Shape) {
			Shape actionShape = (Shape) actionPE;
			Anchor actionShapeAnchor = Graphiti.getPeService().getChopboxAnchor(actionShape);

			AddConnectionContext addConnectionContext = new AddConnectionContext(
					context.getSourceAnchor(), actionShapeAnchor);
			addConnectionContext.setNewObject(ActionsPackage.Literals.ACTION__OPERATION);
			sourceToActionConnection = (Connection) getFeatureProvider().addIfPossible(addConnectionContext);
			
			// add connection from action to target node
			if (sourceToActionConnection != null) {
				addConnectionContext = new AddConnectionContext(
						actionShapeAnchor, context.getTargetAnchor());
				addConnectionContext.setNewObject(getTargetConnectionNewBusinessObject());
				return (Connection) getFeatureProvider().addIfPossible(addConnectionContext);
			}
		}
		
		// prepare context
		AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
		addContext.setNewObject(actionBO);
		addContext.setTargetContainer(sourcePE.getContainer());

		// add the connections and the container
		Connection pe = (Connection) getFeatureProvider().addIfPossible(addContext);
		link(pe, actionBO);

		return null;
	}

	protected abstract Action createActionBO(EObject target);
	
	/**
	 * Returns the new model object (business object) representing the connection from the created action to its target model element,
	 * e.g. {@link ActionsPackage.Literals#CALL_ACTION__CALLED_OPERATION} in case of a created {@link de.upb.pose.specification.actions.CallAction CallAction}.
	 * @return the new model object (business object) representing the connection from the created action to its target model element
	 */
	protected abstract Object getTargetConnectionNewBusinessObject();
}
