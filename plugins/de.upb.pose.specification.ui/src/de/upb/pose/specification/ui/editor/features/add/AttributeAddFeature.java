package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.helpers.AttributeUtil;

public class AttributeAddFeature extends AbstractPolygonShapeWithLabelAddFeature {
	
	public AttributeAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Attribute;
	}
	
	@Override
	protected Attribute getBusinessObject(IAddContext context) {
		return (Attribute) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		Attribute attributeBO = this.getBusinessObject(context);
		return AttributeUtil.getText(attributeBO);
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}
	
}
