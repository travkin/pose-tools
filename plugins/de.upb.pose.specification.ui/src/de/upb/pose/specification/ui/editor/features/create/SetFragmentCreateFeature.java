package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.subsystems.Subsystem;

public class SetFragmentCreateFeature extends CreateFeature {
	
	public SetFragmentCreateFeature(IFeatureProvider fp) {
		super(fp, "Set Fragment");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(SpecificationPackage.Literals.SET_FRAGMENT);
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		ContainerShape pe = context.getTargetContainer();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof PatternSpecification || bo instanceof SetFragment || bo instanceof Subsystem;
	}

	@Override
	protected EObject doCreate(ICreateContext context) {
		ContainerShape cpe = context.getTargetContainer();
		Object cbo = getBusinessObjectForPictogramElement(cpe);

		// create element
		SetFragment bo = SpecificationFactory.eINSTANCE.createSetFragment();

		// add to specification
		PatternSpecification specification = null;
		if (cbo instanceof PatternSpecification) {
			specification = (PatternSpecification) cbo;
		} else if (cbo instanceof PatternElement) {
			specification = ((PatternElement) cbo).getSpecification();
		}
		specification.getPatternElements().add(bo);

		return bo;
	}
}
