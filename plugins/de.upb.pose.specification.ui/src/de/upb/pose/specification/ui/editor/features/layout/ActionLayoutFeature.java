/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;
import de.upb.pose.specification.ui.editor.features.add.ActionAddFeature;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class ActionLayoutFeature extends AbstractShapeWithLabelLayoutFeature {

	protected static final int SHAPE_PADDING = 3;
	
	public ActionLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof Action;
		}
		return false;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#getShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected GraphicsAlgorithm getShape(ILayoutContext context) {
		// the chopbox anchor area corresponds to the rounded rectangle
		return ActionAddFeature.getChopboxAnchorArea(context.getPictogramElement());
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#getLabel(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Text getLabel(ILayoutContext context) {
		GraphicsAlgorithm shapeGA = getShape(context);
		Text childLabel = null;
		if (shapeGA != null && !shapeGA.getGraphicsAlgorithmChildren().isEmpty())
		{
			// look for the first child label (Text)
			for (GraphicsAlgorithm child: shapeGA.getGraphicsAlgorithmChildren()) {
				if (child instanceof Text) {
					childLabel = (Text) child; break;
				}
			}
		}
		Assert.isNotNull(childLabel);
		return childLabel;
	}
	
	/**
	 * Calculates the size of the invisible root rectangle (graphics algorithm).
	 * 
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#determineMinimalRootGASize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected Size determineMinimalRootGASize(ILayoutContext context) {
		Size shapeSize = determineMinimalShapeSize(context);
		
		Size resultVarSize = determineResultVariableSize(context);
		if (resultVarSize != null) {
			shapeSize.addHeight(resultVarSize.getHeight());
		}
		return shapeSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	public Size determineMinimalShapeSize(ILayoutContext context) {
		int delta = SHAPE_PADDING * 2;
		Size  minSize = determineMinimalShapeContentsSize(context).add(delta, delta);
		
		Size resultVarSize = determineResultVariableSize(context);
		if (resultVarSize != null) {
			minSize.setWidth(Math.max(
					minSize.getWidth(),
					resultVarSize.getWidth() + delta + GraphicsAlgorithmsFactory.ROUNDED_RECTANGLE_CORNER_WIDTH / 2));
		}
		
		return minSize;
	}
	
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		Size minSize = super.determineMinimalShapeContentsSize(context);
		int width = minSize.getWidth();
		int height = minSize.getHeight();
		
		ContainerShape actionPE = (ContainerShape) context.getPictogramElement();
		for (Shape child : actionPE.getChildren()) {
			if (!isResultVariableShape(child)) {
				LayoutContext childLayoutContext = new LayoutContext(child);
				AbstractShapeLayoutFeature childLayoutFeature = (AbstractShapeLayoutFeature) getFeatureProvider()
						.getLayoutFeature(childLayoutContext);
				Size childSize = childLayoutFeature.determineMinimalShapeSize(childLayoutContext);
				
				if (width < childSize.getWidth()) {
					width = childSize.getWidth();
				}
				
				height += childSize.getHeight();
			}
		}
		
		return new Size(width, height);
	}
	
	private Size determineResultVariableSize(ILayoutContext context) {
		Action actionBO = (Action) getBusinessObjectForPictogramElement(context.getPictogramElement());
		ContainerShape resultVariablePE = findResultVariablePE(actionBO);
		if (resultVariablePE != null) {
			LayoutContext resultVarLayoutContext = new LayoutContext(resultVariablePE);
			AbstractShapeWithLabelLayoutFeature layoutFeature = (AbstractShapeWithLabelLayoutFeature) getFeatureProvider()
					.getLayoutFeature(resultVarLayoutContext);
			return layoutFeature.determineMinimalShapeSize(resultVarLayoutContext);
		}
		return null;
	}
	
	private ContainerShape findResultVariablePE(Action actionBO) {
		if (actionBO instanceof ResultAction) {
			ResultAction resultAction = (ResultAction) actionBO;
			if (resultAction.getResultVariable() != null) {
				ContainerShape resultVariablePE = PictogrammElementFinder.get()
						.findMainPictogramElementInDiagramForBusinessObject(getDiagram(), resultAction.getResultVariable());
				return resultVariablePE;
			}
		}
		return null;
	}
	
	private boolean isResultVariableShape(Shape child) {
		Object childBO = getBusinessObjectForPictogramElement(child);
		return childBO instanceof ResultVariable;
	}
	
	private ContainerShape findSelfVariablePE(Action actionBO) {
		if (actionBO.getOperation().getParentType().getSelfVariable() != null) {
			SelfVariable selfVariable = actionBO.getOperation().getParentType().getSelfVariable();

			// the same self variable can be used several times, but there can be only one pictogram element
			// that belongs to this action
			List<PictogramElement> pes = PictogrammElementFinder.get()
					.findAllPictogrammElementsInDiagramForBusinessObject(getDiagram(), selfVariable);
			for (PictogramElement pe: pes) {
				if (pe.getLink().getBusinessObjects().get(1).equals(actionBO)
						&& pe instanceof ContainerShape) {
					return (ContainerShape) pe;
				}
			}
		}
		return null;
	}
	
	@Override
	protected boolean layoutRootGA(ILayoutContext context) {
		// first, resize the root
		boolean anythingChanged = super.layoutRootGA(context);
		
		// then re-locate the potentially available result variable node
		Action actionBO = (Action) getBusinessObjectForPictogramElement(context.getPictogramElement());
		ContainerShape resultVariablePE = findResultVariablePE(actionBO);
		if (resultVariablePE != null) {
			Size actionShapeSize = determineMinimalShapeSize(context);
			Size resultVariableSize = determineResultVariableSize(context);
			
			GraphicsAlgorithm resultVariableGA = resultVariablePE.getGraphicsAlgorithm();
			
			 // centering the variable under the action
			int x_delta = (int) Math.round(actionShapeSize.getWidth()/2d - resultVariableSize.getWidth()/2d);
		
			// re-locate the shape to stick to the parent action
			resultVariableGA.setX(x_delta);
			resultVariableGA.setY(actionShapeSize.getHeight());
			
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		
		// horizontally center the label in the parent's drawing area
		Size preferredShapeSize = determineMinimalShapeSize(context);
		Text label = getLabel(context);
		int x = (int) Math.round(preferredShapeSize.getWidth()/2d - label.getWidth()/2d);
		int y = SHAPE_PADDING;
		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			anythingChanged = true;
		}
		
		// also re-calculate the position of children
		y = label.getHeight();
		anythingChanged = anythingChanged || relocateChildren(context, y);
		
		return anythingChanged;
	}
	
	protected boolean relocateChildren(ILayoutContext context, int y) {
		boolean anythingChanged = false;
		
		ContainerShape actionPE = (ContainerShape) context.getPictogramElement();
		Action actionBO = (Action) getBusinessObjectForPictogramElement(actionPE);
		RoundedRectangle actionGA = (RoundedRectangle) getShape(context);
		ContainerShape selfVariableShapePE = findSelfVariablePE(actionBO);
		
		// ensure that the self variable is the top child, move it to the head of the list
		List<Shape> children = new LinkedList<Shape>();
		children.addAll(actionPE.getChildren());
		if (selfVariableShapePE != null) {
			children.remove(selfVariableShapePE);
			children.add(0, selfVariableShapePE);
		}
		
		for (Shape child : children) {
			if (!isResultVariableShape(child)) {
				LayoutContext childLayoutContext = new LayoutContext(child);
				AbstractShapeLayoutFeature childLayoutFeature = (AbstractShapeLayoutFeature) getFeatureProvider()
						.getLayoutFeature(childLayoutContext);
				Size childSize = childLayoutFeature.determineMinimalShapeSize(childLayoutContext);
				
				GraphicsAlgorithm childDrawer = child.getGraphicsAlgorithm();
				int newX = (actionGA.getWidth() - childSize.getWidth()) / 2;
				int newY = y;
				if (childDrawer.getX() != newX || childDrawer.getY() != newY) {
					childDrawer.setX(newX);
					childDrawer.setY(y);
					anythingChanged = true;
				}
				y = y + childSize.getHeight();
			}
		}
		
		return anythingChanged;
	}

}
