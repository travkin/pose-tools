package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.DeleteAction;

public class DeleteActionCreateFeature extends WriteActionCreateFeature {
	
	public DeleteActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Delete");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.DELETE_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		DeleteAction bo = ActionsFactory.eINSTANCE.createDeleteAction();
		bo.setAccessedVariable(getVariable(target));
		return bo;
	}
}
