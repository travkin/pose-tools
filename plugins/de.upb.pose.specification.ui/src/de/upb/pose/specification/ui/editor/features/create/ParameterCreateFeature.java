package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.TypesFactory;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.helpers.ParameterUtil;

public class ParameterCreateFeature extends CreateFeature {
	public ParameterCreateFeature(IFeatureProvider fp) {
		super(fp, "Parameter");
	}

	@Override
	protected EObject doCreate(ICreateContext context) {
		ContainerShape cpe = context.getTargetContainer();
		Operation cbo = (Operation) getBusinessObjectForPictogramElement(cpe);

		Parameter bo = TypesFactory.eINSTANCE.createParameter();
		bo.setName(ParameterUtil.determineInitialParemeterName(cbo));

		cbo.getParameters().add(bo);

		return bo;
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(TypesPackage.Literals.PARAMETER);
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		ContainerShape pe = context.getTargetContainer();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof Operation;
	}
}
