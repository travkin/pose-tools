package de.upb.pose.specification.ui.editor.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.Named;

public class NamedDirectEditingFeature extends AbstractDirectEditingFeature {
	public NamedDirectEditingFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public int getEditingType() {
		return TYPE_TEXT;
	}

	@Override
	public String getInitialValue(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Named bo = (Named) getBusinessObjectForPictogramElement(pe);

		return bo.getName();
	}

	@Override
	public boolean canDirectEdit(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Named;
	}

	@Override
	public boolean stretchFieldToFitText() {
		return true;
	}

	@Override
	public void setValue(String value, IDirectEditingContext context) {
		PictogramElement pe =  context.getPictogramElement();
		Named bo = (Named) getBusinessObjectForPictogramElement(pe);

		bo.setName(value);

		// TODO: possible not needed in the future
		//updatePictogramElement(((Shape) pe).getContainer());
	}
}
