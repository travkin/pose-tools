package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.ui.editor.helpers.TaskDescriptionHelper;

public class TaskDescriptionCreateFeature extends CreateConnectionFeature {
	public TaskDescriptionCreateFeature(IFeatureProvider fp) {
		super(fp, "Task");
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		ContainerShape spe = (ContainerShape) context.getSourcePictogramElement();
		DesignElement sbo = (DesignElement) getBusinessObjectForPictogramElement(spe);

		ContainerShape tpe = (ContainerShape) context.getTargetPictogramElement();

		// create element
		TaskDescription bo = SpecificationFactory.eINSTANCE.createTaskDescription();
		bo.setElement(sbo);
		bo.setName(TaskDescriptionHelper.getName(sbo));

		// prepare context
		AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
		addContext.setNewObject(bo);
		addContext.setTargetContainer(tpe);
		addContext.setLocation(context.getTargetLocation().getX(), context.getTargetLocation().getY());

		Connection pe = (Connection) getFeatureProvider().addIfPossible(addContext);
		link(pe, bo);

		return pe;
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(SpecificationPackage.Literals.TASK_DESCRIPTION);
	}

	@Override
	protected boolean canStart(EObject source) {
		return source instanceof DesignElement;
	}

	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return source instanceof DesignElement && (target instanceof PatternSpecification || target instanceof SetFragment);
	}
}
