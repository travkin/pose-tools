package de.upb.pose.specification.ui.editor.features.update;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.ui.editor.helpers.TaskDescriptionHelper;

public class TaskDescriptionUpdateFeature extends AbstractUpdateFeature {
	public TaskDescriptionUpdateFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		TaskDescription bo = (TaskDescription) getBusinessObjectForPictogramElement(pe);

		// name
		String boName = bo.getName();
		Text peName = TaskDescriptionHelper.getNameText(pe);

		if (GS.unequals(boName, peName)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}

		// comment
		String boComment = bo.getComment();
		MultiText peComment = TaskDescriptionHelper.getCommentText(pe);

		if (GS.differ(boComment, peComment)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		TaskDescription bo = (TaskDescription) getBusinessObjectForPictogramElement(pe);

		// name
		String boName = bo.getName();
		Text peName = TaskDescriptionHelper.getNameText(pe);

		if (GS.unequals(boName, peName)) {
			peName.setValue(boName);
		}

		// comment
		String boComment = bo.getComment();
		MultiText peComment = TaskDescriptionHelper.getCommentText(pe);

		if (GS.differ(boComment, peComment)) {
			peComment.setValue(boComment);
		}

		layoutPictogramElement(pe);

		return true;
	}

	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof TaskDescription;
	}
}
