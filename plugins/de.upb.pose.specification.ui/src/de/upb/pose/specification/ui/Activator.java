package de.upb.pose.specification.ui;

import de.upb.pose.core.ui.runtime.ActivatorImpl;
import de.upb.pose.core.ui.runtime.IActivator;

public final class Activator extends ActivatorImpl {
	
	private static IActivator instance;

	public static IActivator get() {
		return instance;
	}

	@Override
	protected void initialize() {
		Activator.instance = this;

		// cache images
		for (String path : SpecificationUiImages.getPaths()) {
			addImage(path);
		}
	}

	@Override
	protected void dispose() {
		Activator.instance = null;
	}
}
