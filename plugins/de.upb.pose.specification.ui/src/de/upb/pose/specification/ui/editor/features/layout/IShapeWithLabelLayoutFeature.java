/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

/**
 * @author Dietrich Travkin
 */
public interface IShapeWithLabelLayoutFeature extends IShapeLayoutFeature, LabelConstants
{
	Text getLabel(ILayoutContext context);
	
	boolean canHandle(String labelIdentifier, PictogramElement pictogramElement);
	
	Text getLabel(String labelIdentifier, PictogramElement pictogramElement);
	
}
