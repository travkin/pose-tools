package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.AttributeUtil.getText;
import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.ui.editor.helpers.AttributeUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class AttributeUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public AttributeUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	protected GraphicsAlgorithm getRootGraphicsAlgorithm(IUpdateContext context)
	{
		return AttributeUtil.getPolygon(context.getPictogramElement());
	}
	
	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Attribute;
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Attribute bo = (Attribute) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = getText(bo);
		setLabelText(context, boText);

		// background color
		IColorConstant boColor = getColor(bo);
		setBackgroundColor(context, boColor);

		layoutPictogramElement(pe);

		return true;
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		if (canUpdate(context)) {
			ContainerShape pe = (ContainerShape) context.getPictogramElement();
			Attribute bo = (Attribute) getBusinessObjectForPictogramElement(pe);
			
			// text value
			String boText = getText(bo);
			String peText = getLabelText(context);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// background color
			IColorConstant boColor = getColor(bo);
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		}

		return Reason.createFalseReason();
	}
}
