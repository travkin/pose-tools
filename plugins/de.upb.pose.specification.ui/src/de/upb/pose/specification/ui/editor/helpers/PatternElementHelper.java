package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.ui.editor.graphics.ColorConstants;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public abstract class PatternElementHelper {

	public static IColorConstant getColor(PatternElement element) {
		if (element.isCanBeGenerated()) {
			if (element instanceof ParameterAssignment) {
				CallAction parentAction = ((ParameterAssignment) element).getAction();
				return getColor(parentAction);
			} else if (element instanceof ResultVariable) {
				ResultAction parentAction = ((ResultVariable) element).getAction();
				return getColor(parentAction);
			}
			return ColorConstants.CAN_BE_GENERATED;
		} else {
			return ColorConstants.CANNOT_BE_GENERATED;
		}
	}

	protected static Font getFont(Diagram diagram, int size) {
		return getFont(diagram, size, false, false);
	}

	protected static Font getFont(Diagram diagram, int size, boolean italic, boolean bold) {
		return Graphiti.getGaService().manageFont(diagram, FontConstants.FONT_NAME_DEFAULT, size, italic, bold);
	}
}
