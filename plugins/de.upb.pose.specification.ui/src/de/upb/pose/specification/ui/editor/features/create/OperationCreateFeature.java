package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.features.context.impl.AreaContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesFactory;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;

public class OperationCreateFeature extends CreateConnectionFeature {
	
	public OperationCreateFeature(IFeatureProvider fp) {
		super(fp, "Operation");
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		PictogramElement typePE = context.getSourcePictogramElement();
		Type typeBO = (Type) getBusinessObjectForPictogramElement(typePE);

		Operation operationBO = TypesFactory.eINSTANCE.createOperation();
		operationBO.setParentType(typeBO);
		operationBO.setName(OperationUtil.getInitialName(typeBO));

		// add node
		AreaContext addNodeContext = new AreaContext();
		addNodeContext.setLocation(context.getTargetLocation().getX(), context.getTargetLocation().getY());
		PictogramElement operationPE = getFeatureProvider().addIfPossible(new AddContext(addNodeContext, operationBO));

		// add connection from operation to parent type
		if (operationPE != null && operationPE instanceof Shape) {
			Shape operationShape = (Shape) operationPE;
			Anchor targetAnchor = Graphiti.getPeService().getChopboxAnchor(operationShape);
			
			AddConnectionContext addSourceConnectionContext = new AddConnectionContext(context.getSourceAnchor(), targetAnchor);
			addSourceConnectionContext.setNewObject(TypesPackage.Literals.OPERATION__PARENT_TYPE);
			return (Connection) getFeatureProvider().addIfPossible(addSourceConnectionContext);
		}
		return null;
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(TypesPackage.Literals.OPERATION);
	}

	@Override
	protected boolean canStart(EObject source) {
		return source instanceof Type;
	}

	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return canStart(source) && (target instanceof PatternSpecification || target instanceof SetFragment);
	}
}
