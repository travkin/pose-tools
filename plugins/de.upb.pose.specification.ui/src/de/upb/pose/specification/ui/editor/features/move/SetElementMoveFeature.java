package de.upb.pose.specification.ui.editor.features.move;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.ui.util.SetUtil;

public class SetElementMoveFeature extends DefaultMoveShapeFeature {
	
	public SetElementMoveFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof SetFragmentElement;
	}

	@Override
	protected void postMoveShape(IMoveShapeContext context) {
		SetUtil.updateSets(getDiagram());
	}

}
