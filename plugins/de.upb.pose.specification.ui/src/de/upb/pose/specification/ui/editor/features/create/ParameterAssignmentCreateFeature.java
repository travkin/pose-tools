package de.upb.pose.specification.ui.editor.features.create;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.types.Parameter;

public class ParameterAssignmentCreateFeature extends CreateFeature {
	
	public ParameterAssignmentCreateFeature(IFeatureProvider fp) {
		super(fp, "Parameter Assignment");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.PARAMETER_ASSIGNMENT);
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		List<Parameter> parameters = findCallParameters(context); 
		return parameters != null && parameters.size() > 0;
	}

	protected EObject doCreate(ICreateContext context) {
		ParameterAssignment bo = ActionsFactory.eINSTANCE.createParameterAssignment();

		// get container
		ContainerShape cpe = context.getTargetContainer();
		CallAction cbo = (CallAction) getBusinessObjectForPictogramElement(cpe);
		bo.setAction(cbo);
		
		// find first parameter
		Parameter param = findCallParameters(context).get(0);
		bo.setParameter(param);

		return bo;
	}
	
	private List<Parameter> findCallParameters(ICreateContext context) {
		ContainerShape callActionPE = context.getTargetContainer();
		Object bo = getBusinessObjectForPictogramElement(callActionPE);
		if (bo instanceof CallAction) {
			CallAction callActionBO = (CallAction) bo;
			if (callActionBO.getCalledOperation() != null) {
				return callActionBO.getCalledOperation().getParameters();
			}
		}
		return null;
	}
}
