package de.upb.pose.specification.ui.editor.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.palette.IConnectionCreationToolEntry;
import org.eclipse.graphiti.palette.IObjectCreationToolEntry;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipse.graphiti.palette.impl.ConnectionCreationToolEntry;
import org.eclipse.graphiti.palette.impl.ObjectCreationToolEntry;
import org.eclipse.graphiti.palette.impl.PaletteCompartmentEntry;
import org.eclipse.graphiti.palette.impl.PaletteSeparatorEntry;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.tb.DefaultToolBehaviorProvider;
import org.eclipse.graphiti.tb.IContextButtonPadData;

import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.add.ActionAddFeature;
import de.upb.pose.specification.ui.editor.features.create.AccessRuleCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.AttributeCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.CallActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.CreateActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.DelegateActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.DeleteActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.InheritanceCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.OperationCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ParameterAssignmentCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ParameterCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.PatternEnvironmentCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ProduceActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ReadActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.RedirectActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ReferenceCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ResultVariableCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.ReturnActionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.SetFragmentCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.SubsystemCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.TargetConnectionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.TaskDescriptionCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.TypeCreateFeature;
import de.upb.pose.specification.ui.editor.features.create.WriteActionCreateFeature;
import de.upb.pose.specification.ui.editor.helpers.AttributeUtil;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;
import de.upb.pose.specification.ui.editor.helpers.ReferenceUtil;

public class SpecificationToolBehaviorProvider extends DefaultToolBehaviorProvider {
	
	public SpecificationToolBehaviorProvider(IDiagramTypeProvider dtp) {
		super(dtp);
	}

	protected static IObjectCreationToolEntry createEntry(ICreateFeature feature) {
		String name = feature.getCreateName();
		String description = feature.getCreateDescription();
		String imageId = feature.getCreateImageId();
		String largeImageId = feature.getCreateLargeImageId();

		return new ObjectCreationToolEntry(name, description, imageId, largeImageId, feature);
	}

	protected static IConnectionCreationToolEntry createEntry(ICreateConnectionFeature feature) {
		String name = feature.getCreateName();
		String description = feature.getCreateDescription();
		String imageId = feature.getCreateImageId();
		String largeImageId = feature.getCreateLargeImageId();

		ConnectionCreationToolEntry entry = new ConnectionCreationToolEntry(name, description, imageId, largeImageId);
		entry.addCreateConnectionFeature(feature);

		return entry;
	}

	@Override
	public IPaletteCompartmentEntry[] getPalette() {
		IFeatureProvider fp = getFeatureProvider();

		Collection<IPaletteCompartmentEntry> compartments = new ArrayList<IPaletteCompartmentEntry>();

		// structure
		PaletteCompartmentEntry structural = new PaletteCompartmentEntry("Structure", null);
		compartments.add(structural);

		structural.addToolEntry(createEntry(new TypeCreateFeature(fp)));
		structural.addToolEntry(createEntry(new AttributeCreateFeature(fp)));
		structural.addToolEntry(createEntry(new OperationCreateFeature(fp)));
		structural.addToolEntry(createEntry(new ParameterCreateFeature(fp)));
		structural.addToolEntry(createEntry(new SubsystemCreateFeature(fp)));
		structural.addToolEntry(new PaletteSeparatorEntry());
		structural.addToolEntry(createEntry(new ReferenceCreateFeature(fp)));
		structural.addToolEntry(createEntry(new InheritanceCreateFeature(fp)));
		
		// behavior
		PaletteCompartmentEntry behavioral = new PaletteCompartmentEntry("Behavior", null);
		compartments.add(behavioral);
		
		behavioral.addToolEntry(createEntry(new CallActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new RedirectActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new DelegateActionCreateFeature(fp)));
		behavioral.addToolEntry(new PaletteSeparatorEntry());
		behavioral.addToolEntry(createEntry(new CreateActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new ProduceActionCreateFeature(fp)));
		
		behavioral.addToolEntry(new PaletteSeparatorEntry());
		behavioral.addToolEntry(createEntry(new ReadActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new WriteActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new ReturnActionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new DeleteActionCreateFeature(fp)));
		
		behavioral.addToolEntry(new PaletteSeparatorEntry());
		behavioral.addToolEntry(createEntry(new TargetConnectionCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new ParameterAssignmentCreateFeature(fp)));
		behavioral.addToolEntry(createEntry(new ResultVariableCreateFeature(fp)));
		
		// pattern-specific additions to the design representation
		PaletteCompartmentEntry patternSpecific = new PaletteCompartmentEntry("Patterns", null);
		compartments.add(patternSpecific);
		
		patternSpecific.addToolEntry(createEntry(new SetFragmentCreateFeature(fp)));
		patternSpecific.addToolEntry(new PaletteSeparatorEntry());
		patternSpecific.addToolEntry(createEntry(new TaskDescriptionCreateFeature(fp)));
		patternSpecific.addToolEntry(new PaletteSeparatorEntry());
		patternSpecific.addToolEntry(createEntry(new AccessRuleCreateFeature(fp)));
		patternSpecific.addToolEntry(createEntry(new PatternEnvironmentCreateFeature(fp)));

		return compartments.toArray(new IPaletteCompartmentEntry[compartments.size()]);
	}

	@Override
	public GraphicsAlgorithm[] getClickArea(PictogramElement pe) {
		EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

		if (pe instanceof Shape) {
			if (bo instanceof PatternEnvironment) {
				Collection<GraphicsAlgorithm> cloud = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren();
				return cloud.toArray(new GraphicsAlgorithm[cloud.size()]);
			}
			
			// TODO This should not be necessary! Otherwise add attributes, references, actions
			if (bo instanceof Operation) {
				return new GraphicsAlgorithm[] { OperationUtil.getPolygon(pe) };
			}
		}

		return super.getClickArea(pe);
	}

	@Override
	public GraphicsAlgorithm getChopboxAnchorArea(PictogramElement pe) {
		EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

		if (bo instanceof Reference) {
			return ReferenceUtil.getPolygon(pe);
		}

		if (bo instanceof Operation) {
			return OperationUtil.getPolygon(pe);
		}

		if (bo instanceof Attribute) {
			return AttributeUtil.getPolygon(pe);
		}

		if (bo instanceof Action) {
			return ActionAddFeature.getChopboxAnchorArea(pe);
		}

		return super.getChopboxAnchorArea(pe);
	}

	@Override
	public IContextButtonPadData getContextButtonPad(IPictogramElementContext context) {
		IContextButtonPadData pad = super.getContextButtonPad(context);

		PictogramElement pe = context.getPictogramElement();

		setGenericContextButtons(pad, pe, CONTEXT_BUTTON_DELETE);

		EObject bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

		if (bo instanceof Type) {
			// IFeatureProvider fp = getFeatureProvider();
			//
			// Anchor source = Graphiti.getPeService().getChopboxAnchor((AnchorContainer) pe);
			// CreateConnectionContext ccc = new CreateConnectionContext();
			// ccc.setSourcePictogramElement(pe);
			// ccc.setSourceAnchor(source);
			//
			// // attribute
			// ContextButtonEntry button = new ContextButtonEntry(null, context);
			// button.setText("Create Attribute");
			// button.setIconId(SpecificationImages.getKey(TypesPackage.Literals.ATTRIBUTE));
			// button.addDragAndDropFeature(new AttributeCreateFeature(fp));
			// pad.getDomainSpecificContextButtons().add(button);
			//
			// // reference
			// button = new ContextButtonEntry(null, context);
			// button.setText("Create Reference");
			// button.setIconId(SpecificationImages.getKey(TypesPackage.Literals.REFERENCE));
			// button.addDragAndDropFeature(new ReferenceCreateFeature(fp));
			// pad.getDomainSpecificContextButtons().add(button);
			//
			// // operation
			// button = new ContextButtonEntry(null, context);
			// button.setText("Create Operation");
			// button.setIconId(SpecificationImages.getKey(TypesPackage.Literals.OPERATION));
			// button.addDragAndDropFeature(new OperationCreateFeature(fp));
			// pad.getDomainSpecificContextButtons().add(button);
			//
			// // task
			// button = new ContextButtonEntry(null, context);
			// button.setText("Create Task");
			// button.setIconId(SpecificationImages.getKey(SpecificationPackage.Literals.TASK_DESCRIPTION));
			// button.addDragAndDropFeature(new TaskDescriptionCreateFeature(fp));
			// pad.getDomainSpecificContextButtons().add(button);
		}

		return pad;
	}
}
