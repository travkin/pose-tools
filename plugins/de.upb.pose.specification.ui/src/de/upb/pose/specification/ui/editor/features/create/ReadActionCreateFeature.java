package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Type;

public class ReadActionCreateFeature extends ActionCreateFeature {
	
	public ReadActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Read");
	}
	
	protected ReadActionCreateFeature(IFeatureProvider fp, String name) {
		super(fp, name);
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.READ_ACTION);
	}

	protected Variable getVariable(EObject element) {
		if (element instanceof Variable) {
			return (Variable) element;
		}

		// TODO is this really correct?
		if (element instanceof Type) {
			return ((Type) element).getSelfVariable();
		}
		return null;
	}

	@Override
	protected Action createActionBO(EObject target) {
		ReadAction bo = ActionsFactory.eINSTANCE.createReadAction();
		bo.setAccessedVariable(getVariable(target));
		return bo;
	}

	@Override
	protected boolean canFinish(EObject target) {
		return getVariable(target) instanceof Variable;
	}

	@Override
	protected Object getTargetConnectionNewBusinessObject() {
		return ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE;
	}
}
