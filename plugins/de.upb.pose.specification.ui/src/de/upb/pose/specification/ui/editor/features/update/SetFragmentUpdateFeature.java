package de.upb.pose.specification.ui.editor.features.update;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.ui.editor.helpers.PatternElementHelper;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class SetFragmentUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public SetFragmentUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof SetFragment;
	}
	
	/**
	 * @see org.eclipse.graphiti.features.impl.AbstractFeature#getBusinessObjectForPictogramElement(org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	protected SetFragment getBusinessObjectForPictogramElement(PictogramElement setFragmentPE) {
		return (SetFragment) super.getBusinessObjectForPictogramElement(setFragmentPE);
	}
	
	protected Rectangle getInvisibleLabelParent(IUpdateContext context) {
		GraphicsAlgorithm rootGA = getRootGraphicsAlgorithm(context);
		Rectangle invisibleLabelParent = (Rectangle) rootGA.getGraphicsAlgorithmChildren().get(0);
		return invisibleLabelParent;
	}
	
	protected Polygon getLabelFrame(IUpdateContext context) {
		Rectangle invisibleLabelParent = getInvisibleLabelParent(context);
		Polygon frame = (Polygon) invisibleLabelParent.getGraphicsAlgorithmChildren().get(0);
		return frame;
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context) {
		if (canUpdate(context)) {
			ContainerShape pe = (ContainerShape) context.getPictogramElement();
			SetFragment setFragmentBO = getBusinessObjectForPictogramElement(pe);
			
			// text value
			String boText = setFragmentBO.getName();
			String peText = getLabelText(context);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// background color
			Polygon labelFrame = getLabelFrame(context);
			Color currentColor = labelFrame.getBackground();
			IColorConstant desiredColor = PatternElementHelper.getColor(setFragmentBO);
			if (GS.differ(desiredColor, currentColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		}
		return Reason.createFalseReason();
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context) {
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		SetFragment setFragmentBO = getBusinessObjectForPictogramElement(pe);
		
		// text value
		String boText = setFragmentBO.getName();
		setLabelText(context, boText);
		
		// background color
		IColorConstant desiredColor = PatternElementHelper.getColor(setFragmentBO);
		Polygon labelFrame = getLabelFrame(context);
		labelFrame.setBackground(manageColor(desiredColor));

		layoutPictogramElement(pe);
				
		return true;
	}
	
}
