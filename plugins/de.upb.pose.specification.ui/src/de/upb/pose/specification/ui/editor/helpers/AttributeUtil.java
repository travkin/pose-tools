package de.upb.pose.specification.ui.editor.helpers;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public final class AttributeUtil extends SetElementHelper {
	
	private AttributeUtil() {
		// hide constructor
	}

	public static Polygon getPolygon(PictogramElement pe) {
		return (Polygon) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}

	public static FontDescription getFont() {
		return FontConstants.FONT_10_NORMAL_REGULAR;
	}

	public static String getInitialName(Type type) {
		Collection<String> existing = new HashSet<String>();
		for (Attribute attribute : type.getAttributes()) {
			existing.add(attribute.getName());
		}

		String prefix = "a";
		String name = prefix;
		int index = 2;
		while (existing.contains(name)) {
			name = prefix + "_" + index;
			index++;
		}

		return name;
	}

	public static String getText(Attribute bo) {
		StringBuilder builder = new StringBuilder();

		builder.append(bo.getName());

		if (CardinalityType.MULTIPLE.equals(bo.getCardinality())) {
			builder.append('*');
		}

		if (bo.getType() != null) {
			builder.append(' ');
			builder.append(':');
			builder.append(' ');
			builder.append(bo.getType().getName());
		}

		return builder.toString();
	}

	public static Text getText(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(1);
	}
}
