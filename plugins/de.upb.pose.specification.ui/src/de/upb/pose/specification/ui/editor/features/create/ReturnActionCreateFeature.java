package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ReturnAction;

public class ReturnActionCreateFeature extends ReadActionCreateFeature {
	
	public ReturnActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Return");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.RETURN_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		ReturnAction bo = ActionsFactory.eINSTANCE.createReturnAction();
		bo.setAccessedVariable(getVariable(target));
		return bo;
	}
	
}
