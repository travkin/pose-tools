package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.helpers.ParameterUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class ParameterUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public ParameterUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Parameter;
	}
	
	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Parameter bo = (Parameter) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = ParameterUtil.getText(bo);
		String peText = getLabelText(context);
		if (GS.differ(boText, peText)) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}
		
		// background color
		IColorConstant boColor = getColor(bo);
		Color peColor = getBackgroundColor(context);
		if (GS.differ(boColor, peColor))
		{
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		Parameter bo = (Parameter) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = ParameterUtil.getText(bo);
		String peText = getLabelText(context);
		if (GS.differ(boText, peText)) {
			setLabelText(context, boText);

			// update parent operation
			layoutPictogramElement(pe.getContainer());
		}
		
		// background color
		IColorConstant boColor = getColor(bo);
		setBackgroundColor(context, boColor);

		return true;
	}

}
