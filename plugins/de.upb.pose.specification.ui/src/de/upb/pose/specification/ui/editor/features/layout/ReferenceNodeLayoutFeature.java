package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Area;
import de.upb.pose.core.util.Area.Position;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class ReferenceNodeLayoutFeature extends AbstractPolygonShapeWithLabelLayoutFeature
{
	protected static final int ARROW_LENGTH = 15;
	private static final Size LABEL_PADDING = new Size(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
	
	public ReferenceNodeLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof Reference;
		}
		return false;
	}
	
	@Override
	protected Size getLabelPadding() {
		return LABEL_PADDING;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	public Size determineMinimalShapeSize(ILayoutContext context)
	{
		Size  minSize = determineMinimalShapeContentsSize(context);
		
		Position pos = determineReferenceLabelPosition(context);
		if (pos == Position.BOTTOM || pos == Position.TOP)
		{
			minSize.addHeight(ARROW_LENGTH);
		}
		else if (pos == Position.LEFT || pos == Position.RIGHT)
		{
			minSize.addWidth(ARROW_LENGTH);
		}
		
		return minSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#layoutRootGA(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean layoutRootGA(ILayoutContext context) {
		// first, resize the figure
		boolean anythingChanged = super.layoutRootGA(context);
		
		// then attach this figure to the type's figure that the reference belongs to
		GraphicsAlgorithm referingTypeGA = findTypePEforConnectionSourceEnd(context).getGraphicsAlgorithm();
		int sx = referingTypeGA.getX();
		int sy = referingTypeGA.getY();
		int sw = referingTypeGA.getWidth();
		int sh = referingTypeGA.getHeight();
		
		Rectangle invisibleParent = getInvisibleParentGA(context);
		Size minSize = determineMinimalShapeSize(context);
		
		int x_delta = (int) Math.round(sw/2d - minSize.getWidth()/2d);
		int y_delta = (int) Math.round(sh/2d - minSize.getHeight()/2d);
		
		Position pos = determineReferenceLabelPosition(context);
		switch (pos)
		{
		case TOP:
			invisibleParent.setX(sx + x_delta);
			invisibleParent.setY(sy - invisibleParent.getHeight());
			break;
		case BOTTOM:
			invisibleParent.setX(sx + x_delta);
			invisibleParent.setY(sy + sh - 1);
			break;
		case LEFT:
			invisibleParent.setX(sx - invisibleParent.getWidth());
			invisibleParent.setY(sy + y_delta);
			break;
		case RIGHT:
			invisibleParent.setX(sx + sw - 1);
			invisibleParent.setY(sy + y_delta);
			break;
		case SAME:
		default:
			break;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	public boolean layoutShape(ILayoutContext context)
	{
//		ContainerShape referenceNodePE = (ContainerShape) context.getPictogramElement();
//		BoxRelativeAnchor anchor = (BoxRelativeAnchor) referenceNodePE.getAnchors().get(1);
		
		Polygon polygon = getShape(context);
		Size minSize = determineMinimalShapeSize(context);
		
		int w = minSize.getWidth();
		int h = minSize.getHeight();
		
		int x_mid = (int) Math.round(w/2d);
		int y_mid = (int) Math.round(h/2d);
		
		// relocate the polygon and label within the invisible parent's boundaries
		// points
		Position pos = determineReferenceLabelPosition(context);
		switch (pos)
		{
		case TOP:
			polygon.getPoints().get(0).setX(0);
			polygon.getPoints().get(0).setY(h);
			polygon.getPoints().get(1).setX(0);
			polygon.getPoints().get(1).setY(ARROW_LENGTH);
			polygon.getPoints().get(2).setX(x_mid);
			polygon.getPoints().get(2).setY(0);
			polygon.getPoints().get(3).setX(w);
			polygon.getPoints().get(3).setY(ARROW_LENGTH);
			polygon.getPoints().get(4).setX(w);
			polygon.getPoints().get(4).setY(h);
			polygon.getPoints().get(5).setX(0);
			polygon.getPoints().get(5).setY(h);
			
//			anchor.setRelativeWidth(0.5);
//			anchor.setRelativeHeight(0.0);
			break;
		case BOTTOM:
			polygon.getPoints().get(0).setX(0);
			polygon.getPoints().get(0).setY(0);
			polygon.getPoints().get(1).setX(0);
			polygon.getPoints().get(1).setY(h - ARROW_LENGTH);
			polygon.getPoints().get(2).setX(x_mid);
			polygon.getPoints().get(2).setY(h);
			polygon.getPoints().get(3).setX(w);
			polygon.getPoints().get(3).setY(h - ARROW_LENGTH);
			polygon.getPoints().get(4).setX(w);
			polygon.getPoints().get(4).setY(0);
			polygon.getPoints().get(5).setX(0);
			polygon.getPoints().get(5).setY(0);
			
//			anchor.setRelativeWidth(0.5);
//			anchor.setRelativeHeight(1.0);
			break;
		case LEFT:
			polygon.getPoints().get(0).setX(w);
			polygon.getPoints().get(0).setY(0);
			polygon.getPoints().get(1).setX(ARROW_LENGTH);
			polygon.getPoints().get(1).setY(0);
			polygon.getPoints().get(2).setX(0);
			polygon.getPoints().get(2).setY(y_mid);
			polygon.getPoints().get(3).setX(ARROW_LENGTH);
			polygon.getPoints().get(3).setY(h);
			polygon.getPoints().get(4).setX(w);
			polygon.getPoints().get(4).setY(h);
			polygon.getPoints().get(5).setX(w);
			polygon.getPoints().get(5).setY(0);
			
//			anchor.setRelativeWidth(0.0);
//			anchor.setRelativeHeight(0.5);
			break;
		case RIGHT:
			polygon.getPoints().get(0).setX(0);
			polygon.getPoints().get(0).setY(0);
			polygon.getPoints().get(1).setX(w - ARROW_LENGTH);
			polygon.getPoints().get(1).setY(0);
			polygon.getPoints().get(2).setX(w);
			polygon.getPoints().get(2).setY(y_mid);
			polygon.getPoints().get(3).setX(w - ARROW_LENGTH);
			polygon.getPoints().get(3).setY(h);
			polygon.getPoints().get(4).setX(0);
			polygon.getPoints().get(4).setY(h);
			polygon.getPoints().get(5).setX(0);
			polygon.getPoints().get(5).setY(0);
			
//			anchor.setRelativeWidth(1.0);
//			anchor.setRelativeHeight(0.5);
			break;
		case SAME:
		default:
			break;
		}
		
		return true;
	}
	
	protected boolean relocateShapeContents(ILayoutContext context) {
		// place the label in the parent's drawing area (parent and label sizes are already set)
		Rectangle invisibleParent = getInvisibleParentGA(context);
		Text label = getLabel(context);

		int x_center = (int) Math.round(invisibleParent.getWidth() / 2d - label.getWidth() / 2d);
		int y_center = (int) Math.round(invisibleParent.getHeight() / 2d - label.getHeight() / 2d);

		int x, y;
		Position pos = determineReferenceLabelPosition(context);

		switch (pos) {
		case RIGHT:
			x = 0;
			y = y_center;
			break;
		case LEFT:
			x = ARROW_LENGTH;
			y = y_center;
			break;
		case TOP:
			x = x_center;
			y = ARROW_LENGTH;
			break;
		case BOTTOM:
			x = x_center;
			y = 0;
			break;
		default:
			x = 0;
			y = 0;
			break;
		}

		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			return true;
		}
		return false;
	}
	
	private Reference getBusinessObject(ILayoutContext context) {
		PictogramElement referencePE = context.getPictogramElement();
		Reference reference = (Reference) getBusinessObjectForPictogramElement(referencePE);
		return reference;
	}
	
	private PictogramElement findPictogramElementFor(Type typeBO) {
		return PictogrammElementFinder.get().findMainPictogramElementInDiagramForBusinessObject(getDiagram(), typeBO);
	}
	
	private PictogramElement findTypePEforConnectionSourceEnd(ILayoutContext context) {
		Reference reference = getBusinessObject(context);
		return findPictogramElementFor(reference.getParentType());
	}
	
	private PictogramElement findTypePEforConnectionTargetEnd(ILayoutContext context) {
		Reference reference = getBusinessObject(context);
		return findPictogramElementFor(reference.getType());
	}
	
	protected Position determineReferenceLabelPosition(ILayoutContext context) {
		Area sourceArea = Area.fromPE(findTypePEforConnectionSourceEnd(context));
		Area targetArea = Area.fromPE(findTypePEforConnectionTargetEnd(context));
		
		if (sourceArea.getX() + sourceArea.getWidth() < targetArea.getX()) { // from left to right
			return Position.RIGHT;
		} else if (targetArea.getX() + targetArea.getWidth() < sourceArea.getX()) { // from right to left
			return Position.LEFT;
		} else if (sourceArea.getY() + sourceArea.getHeight() < targetArea.getY()) { // from top to bottom
			return Position.BOTTOM;
		} else if (targetArea.getY() + targetArea.getHeight() < sourceArea.getY()) { // from bottom to top
			return Position.TOP;
		} else { // default
			return Position.RIGHT;
		}
	}
}
