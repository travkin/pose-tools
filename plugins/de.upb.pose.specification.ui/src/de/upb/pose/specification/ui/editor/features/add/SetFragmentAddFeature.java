package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public class SetFragmentAddFeature extends AbstractShapeWithLabelAddFeature {
	
	public SetFragmentAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof SetFragment;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected SetFragment getBusinessObject(IAddContext context) {
		return (SetFragment) context.getNewObject();
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelTextForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		SetFragment setFragment = getBusinessObject(context);
		return setFragment.getName();
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelFontDescriptionForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#updateSetFragments()
	 */
	@Override
	protected boolean updateSetFragments() {
		return true;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#createLabelParent(org.eclipse.graphiti.mm.pictograms.ContainerShape)
	 */
	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		Rectangle frame = GraphicsAlgorithmsFactory.addRectangle(parentShapePE, colorWhite, colorBlack, LineStyle.SOLID);
		frame.setTransparency(0.7d);
		
		// invisible label container
		Rectangle labelContainer = GraphicsAlgorithmsFactory.addRectangle(frame, null, colorBlack, LineStyle.SOLID);
		labelContainer.setLineVisible(false);
		labelContainer.setLineWidth(0);
		
		return labelContainer;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#createLabelAndOtherGraphicsAlgorithms(org.eclipse.graphiti.features.context.IAddContext, org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm)
	 */
	protected Text createLabelAndOtherGraphicsAlgorithms(IAddContext context, GraphicsAlgorithm labelParent) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		Polygon labelFrame = Graphiti.getGaService().createPolygon(labelParent, new int[10]);
		labelFrame.setFilled(true);
		labelFrame.setBackground(colorWhite);
		labelFrame.setForeground(colorBlack);
		labelFrame.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);
		
		GraphicsAlgorithmsFactory.addLabel(labelParent,
				"set", colorBlack, FontConstants.FONT_9_BOLD_REGULAR.manage(getDiagram()));
				
		Text suffixLabel = super.createLabelAndOtherGraphicsAlgorithms(context, labelParent);
		
		return suffixLabel;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#initializeSizeAndLocation(org.eclipse.graphiti.features.context.IAddContext, org.eclipse.graphiti.mm.pictograms.ContainerShape, org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm)
	 */
	@Override
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		GraphicsAlgorithm rootGA = parentShapePE.getGraphicsAlgorithm();
		
		rootGA.setX(context.getX());
		rootGA.setY(context.getY());
		rootGA.setWidth(context.getWidth());
		rootGA.setHeight(context.getHeight());
		
		if (context.getTargetContainer().equals(getDiagram())) {
			Graphiti.getPeService().sendToBack(parentShapePE);
		}
	}

}
