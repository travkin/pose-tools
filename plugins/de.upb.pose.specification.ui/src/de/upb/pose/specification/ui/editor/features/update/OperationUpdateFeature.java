package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;
import de.upb.pose.specification.ui.editor.helpers.ParameterUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class OperationUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public OperationUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected GraphicsAlgorithm getRootGraphicsAlgorithm(IUpdateContext context)
	{
		return OperationUtil.getPolygon(context.getPictogramElement());
	}
	
	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Operation;
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		if (canUpdate(context)) {
			PictogramElement pe = context.getPictogramElement();
			Operation bo = (Operation) getBusinessObjectForPictogramElement(pe);
			
			// name text
			String boText = OperationUtil.getPrefixLabelText(bo);
			String peText = OperationUtil.getPrefixLabelText(pe);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// type text
			boText = OperationUtil.getSuffixLabelText(bo);
			peText = OperationUtil.getSuffixLabelText(pe);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// text font
			FontDescription boFont = OperationUtil.getFont(bo);
			Font peFont = getLabelFont(context);
			if (GS.differ(boFont, peFont)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_FONT);
			}
			
			// background color
			IColorConstant boColor = getColor(bo);
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
			
			// line style
			LineStyle boLine = OperationUtil.getLineStyle(bo);
			LineStyle peLine = getLineStyle(context);
			if (!boLine.equals(peLine)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_LINE_STYLE);
			}
			
			// children
			if (pe instanceof ContainerShape) {
				ContainerShape shape = (ContainerShape) pe;
				GraphicsAlgorithm shapeGA = shape.getGraphicsAlgorithm();
				
				LayoutContext layoutContext = new LayoutContext(shape);
				AbstractShapeLayoutFeature layoutFeature = (AbstractShapeLayoutFeature) getFeatureProvider()
						.getLayoutFeature(layoutContext);
				Size preferredSize = layoutFeature.determineMinimalShapeSize(layoutContext);
				if (shapeGA.getWidth() != preferredSize.getWidth() || shapeGA.getHeight() != preferredSize.getWidth()) {
					return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_LAYOUT);
				}
			}
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Operation bo = (Operation) getBusinessObjectForPictogramElement(pe);

		// name text
		String boText = OperationUtil.getPrefixLabelText(bo);
		setLabelText(context, boText);

		// type text
		boText = OperationUtil.getSuffixLabelText(bo);
		Text label = OperationUtil.getSuffixLabel(pe);
		if (GS.unequals(boText, label))
		{
			label.setValue(boText);
		}

		// text font
		FontDescription boFont = OperationUtil.getFont(bo);
		setLabelFont(context, boFont);
		
		// background color
		IColorConstant boColor = getColor(bo);
		setBackgroundColor(context, boColor);

		// line style
		LineStyle boLine = OperationUtil.getLineStyle(bo);
		setLineStyle(context, boLine);

		// update parameters
		for (Shape cpe : ((ContainerShape) pe).getChildren()) {
			ParameterUtil.getText((ContainerShape) cpe).setFont(boFont.manage(getDiagram()));
		}

		layoutPictogramElement(pe);

		return true;
	}

}
