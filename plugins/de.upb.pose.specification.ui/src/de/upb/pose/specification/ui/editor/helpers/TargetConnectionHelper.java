/**
 * 
 */
package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public class TargetConnectionHelper extends PatternElementHelper {

	public static String getTargetLabelText(CallAction callAction) {
		if (callAction.getTarget() != null && callAction.getTarget() instanceof Reference) {
			Reference callActionTargetReference = (Reference) callAction.getTarget();
			if (CardinalityType.MULTIPLE.equals(callActionTargetReference.getCardinality())) {
				if (callAction.isForAll()) {
					return "\u2200";
				} else {
					return "\u2203";
				}
			}
		}
		return null;
	}
	
	public static Text getTargetLabelText(Connection targetConnection) {
		if (targetConnection.getLink() != null) {
			EObject businessOject = targetConnection.getLink().getBusinessObjects().get(0);
			if (businessOject instanceof CallAction) {
				// if there is a decorator in addition to the arrow
				if (targetConnection.getConnectionDecorators().size() > 1) {
					ConnectionDecorator decorator = targetConnection.getConnectionDecorators().get(1);
					return (Text) decorator.getGraphicsAlgorithm();
				}
			}
		}
		return null;
	}
	
	public static void updateTargetLabelSizeAndLocation(Text targetLabel, Diagram parentDiagram) {
		String currentText = targetLabel.getValue();
		
		Font font = ReferenceUtil.getFont().manage(parentDiagram);
		Size size = GS.getSize(font, currentText).padding(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
		
		targetLabel.setFont(font);
		targetLabel.setX(0);
		targetLabel.setY(0);
		targetLabel.setWidth(size.getWidth());
		targetLabel.setHeight(size.getHeight());
	}
}
