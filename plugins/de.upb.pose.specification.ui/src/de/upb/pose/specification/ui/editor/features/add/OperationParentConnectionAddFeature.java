/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

/**
 * @author Dietrich Travkin
 */
public class OperationParentConnectionAddFeature extends AddConnectionFeature {

	public OperationParentConnectionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected Connection add(IAddConnectionContext context) {
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());
		
		Operation operationBO = (Operation) getBusinessObjectForPictogramElement(context.getTargetAnchor().getParent());
		link(connection, operationBO);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);

		GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.SOLID);
		GraphicsFactory.addConnectionDecorator_FilledDiamond_SourceEnd(connection, colorBlack);
		
		return connection;
	}

	@Override
	protected boolean canAdd(EObject element) {
		return TypesPackage.Literals.OPERATION__PARENT_TYPE.equals(element);
	}

}
