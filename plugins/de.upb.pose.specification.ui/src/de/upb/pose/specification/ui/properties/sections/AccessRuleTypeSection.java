package de.upb.pose.specification.ui.properties.sections;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionComboSectionBase;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.util.SpecificationUtil;

public class AccessRuleTypeSection extends CollectionComboSectionBase<AccessType> {
	
	@Override
	protected EStructuralFeature getFeature() {
		return AccessPackage.Literals.ACCESS_RULE__ACCESS_TYPE;
	}

	@Override
	protected List<AccessType> getItems()
	{
		List<AccessType> items = new ArrayList<AccessType>();

		// add 'null'
		items.add(null);
		
		PatternSpecification specification = SpecificationUtil.getSpecification(getElement());
		items.addAll(specification.getPattern().getCatalog().getAccessTypes());
		
		return items;
	}
	
	@Override
	protected String getText(AccessType element) {
		if (element != null) {
			return element.getName();
		}
		return EMPTY;
	}

	@Override
	protected String getLabelText()
	{
		return "Type";
	}
}
