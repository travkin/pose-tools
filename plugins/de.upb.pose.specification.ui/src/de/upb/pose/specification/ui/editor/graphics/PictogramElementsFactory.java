/**
 * 
 */
package de.upb.pose.specification.ui.editor.graphics;

import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;

/**
 * @author Dietrich Travkin
 */
public class PictogramElementsFactory {

	public static Connection addConnection(Diagram parentDiagram, Anchor sourceAnchor, Anchor targetAnchor) {
		Connection connection = Graphiti.getPeService().createFreeFormConnection(parentDiagram);
		connection.setStart(sourceAnchor);
		connection.setEnd(targetAnchor);
		return connection;
	}
	
	public static ConnectionDecorator addConnectionDecorator(Connection parentConnection, boolean sourceEnd) {
		return addConnectionDecorator(parentConnection, sourceEnd, false);
	}
	
	public static ConnectionDecorator addConnectionDecorator(Connection parentConnection, boolean sourceEnd, boolean active) {
		return Graphiti.getPeService().createConnectionDecorator(
				parentConnection, active, (sourceEnd ? 0 : 1), true);
	}

}
