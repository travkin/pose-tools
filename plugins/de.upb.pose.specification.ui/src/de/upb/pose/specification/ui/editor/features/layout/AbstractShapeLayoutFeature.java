/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractShapeLayoutFeature extends AbstractLayoutFeature implements IShapeLayoutFeature
{
	public AbstractShapeLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public SpecificationEditorFeatureProvider getFeatureProvider() {
		return (SpecificationEditorFeatureProvider) super.getFeatureProvider();
	}
	
	/**
	 * @param context this feature's layout context
	 * @return the main (parent) graphics algorithm (could also be invisible)
	 * 
	 * @see #getShape(ILayoutContext)
	 */
	protected GraphicsAlgorithm getRootGA(ILayoutContext context) {
		return context.getPictogramElement().getGraphicsAlgorithm();
	}
	
	/**
	 * @param context this feature's layout context
	 * @return the main (parent) graphics algorithm's size (this one could also be invisible)
	 */
	protected Size determineMinimalRootGASize(ILayoutContext context) {
		return determineMinimalShapeSize(context);
	}
	
	/**
	 * Returns the main (visible) graphics algorithm representing the whole figure to be drawn.
	 * That graphics algorithm is usually the parent of other graphics algorithms,
	 * but could also not be the parent of all graphics algorithm (be itself a child of an invisible graphics algorithms).
	 * 
	 * @param context this feature's layout context
	 * @return the main (not necessarily parent) graphics algorithm (usually something like a {@link Rectangle)
	 * 
	 * @see #getRootGA(ILayoutContext)
	 */
	protected GraphicsAlgorithm getShape(ILayoutContext context) {
		return getRootGA(context);
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.IShapeWithLabelLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeSize(ILayoutContext context) {
		return determineMinimalShapeContentsSize(context);
	}
	
	/**
	 * Performs a (re-)layout of the root graphics algorithm.
	 * In the default implementation the root (parent) graphics algorithm is the shape graphics algorithm.
	 * If this is not intended, override {@link #layoutShape(ILayoutContext)} to layout the shape.
	 * 
	 * @see #layoutShape(ILayoutContext)
	 */
	protected boolean layoutRootGA(ILayoutContext context) {
		boolean anythingChanged = false;
		
		GraphicsAlgorithm rootGA = getRootGA(context);
		Size preferredRootGASize = determineMinimalRootGASize(context);
		
		// adapt shape's size if necessary
		if (rootGA.getWidth() != preferredRootGASize.getWidth()) {
			rootGA.setWidth(preferredRootGASize.getWidth());
			anythingChanged = true;
		}
		if (rootGA.getHeight() != preferredRootGASize.getHeight()) {
			rootGA.setHeight(preferredRootGASize.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * Performs a (re-)layout of the shape to be drawn.
	 * In the default implementation the root (parent) graphics algorithm is the shape graphics algorithm.
	 * If this is not intended, override this method to layout the shape separately from the root. 
	 * 
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 * @see #layoutRootGA(ILayoutContext)
	 */
	@Override
	public boolean layoutShape(ILayoutContext context) {
		boolean anythingChanged = false;
		
		GraphicsAlgorithm shape = getShape(context);
		Size minSize = determineMinimalShapeSize(context);
		
		// adapt shape's size if necessary
		if (shape.getWidth() != minSize.getWidth())
		{
			shape.setWidth(minSize.getWidth());
			anythingChanged = true;
		}
		if (shape.getHeight() != minSize.getHeight())
		{
			shape.setHeight(minSize.getHeight());
			anythingChanged = true;
		}

		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.IShapeWithLabelLayoutFeature#layoutShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public final boolean layoutShapeContents(ILayoutContext context) {
		boolean anythingChanged = resizeShapeContents(context);
		anythingChanged = relocateShapeContents(context) || anythingChanged;
		return anythingChanged;
	}
	
	protected abstract boolean resizeShapeContents(ILayoutContext context);
	
	protected abstract boolean relocateShapeContents(ILayoutContext context);
	

	/**
	 * @see org.eclipse.graphiti.func.ILayout#layout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public final boolean layout(ILayoutContext context) {
		// layout the root graphics algorithm (this one could be invisible)
		boolean anythingChanged = layoutRootGA(context);
		
		// layout the shape (the main graphics algorithm)
		anythingChanged = layoutShape(context) || anythingChanged;
		
		// layout shape contents, e.g.: labels, etc. (graphics algorithms children)
		anythingChanged = layoutShapeContents(context) || anythingChanged;
		
		// resize pictogram element children
		PictogramElement pe = context.getPictogramElement();
		if (pe instanceof ContainerShape)
		{
			for (Shape child : ((ContainerShape) pe).getChildren())
			{
				anythingChanged = layoutPictogramElement(child).toBoolean() || anythingChanged;
			}
		}
		
		return anythingChanged;
	}
}
