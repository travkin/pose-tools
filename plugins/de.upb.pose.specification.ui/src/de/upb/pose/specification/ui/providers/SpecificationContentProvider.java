package de.upb.pose.specification.ui.providers;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;

import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;

public class SpecificationContentProvider extends ArrayContentProvider implements ITreeContentProvider {
	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element).length > 0;
	}

	@Override
	public Object[] getElements(Object element) {
		return getChildren(element);
	}

	@Override
	public Object[] getChildren(Object element) {
		// resource
		if (element instanceof Resource) {
			Collection<?> children = ((Resource) element).getContents();
			return children.toArray(new Object[children.size()]);
		}

		// catalog
		if (element instanceof DesignPatternCatalog) {
			Collection<Object> children = new ArrayList<Object>();

			// add categories
			children.addAll(((DesignPatternCatalog) element).getCategories());

			// add patterns that have no category assigned
			for (DesignPattern pattern : ((DesignPatternCatalog) element).getPatterns()) {
				if (pattern.getCategories().isEmpty()) {
					children.add(pattern);
				}
			}

			return children.toArray(new Object[children.size()]);
		}

		// category
		if (element instanceof Category) {
			Collection<Object> children = new ArrayList<Object>();

			// add sub categories
			children.addAll(((Category) element).getChildren());

			// add patterns
			children.addAll(((Category) element).getPatterns());

			return children.toArray(new Object[children.size()]);
		}

		// pattern
		if (element instanceof DesignPattern) {
			Collection<?> children = ((DesignPattern) element).getSpecifications();

			return children.toArray(new Object[children.size()]);
		}

		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}
}
