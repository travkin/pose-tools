package de.upb.pose.specification.ui.properties.sections;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionComboSectionBase;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.types.PrimitiveType;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.util.SpecificationUtil;

public class AttributeTypeSection extends CollectionComboSectionBase<PrimitiveType> {
	@Override
	protected String getLabelText() {
		return "Type";
	}

	@Override
	protected List<PrimitiveType> getItems() {
		List<PrimitiveType> items = new ArrayList<PrimitiveType>();

		// add 'null'
		items.add(null);
		
		PatternSpecification specification = SpecificationUtil.getSpecification(getElement());
		items.addAll(specification.getPattern().getCatalog().getPrimitiveTypes());
		
		return items;
	}
	
	@Override
	protected String getText(PrimitiveType element) {
		if (element != null) {
			return element.getName();
		}
		return EMPTY;
	}

	@Override
	protected EStructuralFeature getFeature() {
		return TypesPackage.Literals.FEATURE__TYPE;
	}
}
