/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

/**
 * @author Dietrich Travkin
 */
public class ResultVariableAddFeature extends AbstractShapeWithLabelAddFeature {

	public ResultVariableAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof ResultVariable;
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected ResultVariable getBusinessObject(IAddContext context) {
		return (ResultVariable) context.getNewObject();
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelTextForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		return getBusinessObject(context).getName();
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelFontDescriptionForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#updateSetFragments()
	 */
	@Override
	protected boolean updateSetFragments() {
		return false;
	}

}
