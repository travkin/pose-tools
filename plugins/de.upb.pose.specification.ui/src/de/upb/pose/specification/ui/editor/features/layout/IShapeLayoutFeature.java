/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.context.ILayoutContext;

import de.upb.pose.core.util.Size;

/**
 * @author Dietrich Travkin
 */
public interface IShapeLayoutFeature extends ILayoutFeature {

	Size determineMinimalShapeSize(ILayoutContext context);
	Size determineMinimalShapeContentsSize(ILayoutContext context);
	
	boolean layoutShape(ILayoutContext context);
	boolean layoutShapeContents(ILayoutContext context);
	
}
