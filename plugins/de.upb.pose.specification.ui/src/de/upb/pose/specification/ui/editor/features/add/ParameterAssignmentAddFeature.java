package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.helpers.ParameterAssignmentUtil;

public class ParameterAssignmentAddFeature extends AbstractShapeWithLabelAddFeature {
	
	public ParameterAssignmentAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof ParameterAssignment;
	}
	
	@Override
	protected ParameterAssignment getBusinessObject(IAddContext context) {
		return (ParameterAssignment) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		return ParameterAssignmentUtil.getText(getBusinessObject(context));
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}

	@Override
	protected boolean updateSetFragments() {
		return false;
	}
	
	@Override
	protected boolean enableDirectEditing() {
		return false;
	}
	
	@Override
	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		GraphicsAlgorithm parentGA = super.createLabelParent(parentShapePE);
		parentGA.setLineVisible(false);
		return parentGA;
	}

	
}
