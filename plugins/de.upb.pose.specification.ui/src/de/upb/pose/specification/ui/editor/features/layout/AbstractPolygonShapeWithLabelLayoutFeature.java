/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Point;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractPolygonShapeWithLabelLayoutFeature extends AbstractShapeWithLabelLayoutFeature {

	public AbstractPolygonShapeWithLabelLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	protected Rectangle getInvisibleParentGA(ILayoutContext context) {
		return (Rectangle) getRootGA(context);
	}
	
	@Override
	protected Polygon getShape(ILayoutContext context) {
		Rectangle rootGA = getInvisibleParentGA(context);
		Polygon childPolygon = null;
		if (rootGA != null && !rootGA.getGraphicsAlgorithmChildren().isEmpty())
		{
			// look for the first child polygon (Text)
			for (GraphicsAlgorithm child: rootGA.getGraphicsAlgorithmChildren()) {
				if (child instanceof Polygon) {
					childPolygon = (Polygon) child; break;
				}
			}
		}
		Assert.isNotNull(childPolygon);
		return childPolygon;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#determineCurrentShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected Size determineCurrentShapeSize(ILayoutContext context) {
		Polygon shapeGA = getShape(context);
		
		// Unexpected behavior in graphiti requires own calculation of polygon width and height,
		// i.e. the polygon point coordinates have no influence on the width and height returned by a Polygon object.
		// Work-around: calculate the polygon width and height based on the polygon point coordinates on our own.
		return determinePolygonSize(shapeGA);
	}

	private Size determinePolygonSize(Polygon polygon) {
		if (polygon.getPoints().size() <= 0) {
			return new Size(0, 0);
		}
		
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE, maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
		for (Point point: polygon.getPoints()) {
			if (point.getX() < minX) {
				minX = point.getX();
			}
			if (point.getX() > maxX) {
				maxX = point.getX();
			}
			if (point.getY() < minY) {
				minY = point.getY();
			}
			if (point.getY() > maxY) {
				maxY = point.getY();
			}
		}
		return new Size(maxX - minX, maxY - minY);
	}

}
