package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.helpers.OperationUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class OperationLayoutFeature extends AbstractPolygonShapeWithLabelLayoutFeature
{
	private static final int PEAK_WIDTH = 10;
	
	public OperationLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof Operation;
		}
		return false;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#determineMinimalShapeSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeSize(ILayoutContext context) {
		Size minSize = determineMinimalShapeContentsSize(context).add(2 * PEAK_WIDTH, 0);
		return minSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.AbstractShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layoutShape(ILayoutContext context) {
		Polygon polygon = getShape(context);
		Size minSize = determineMinimalShapeSize(context);
		
		int width = minSize.getWidth();
		int height = minSize.getHeight();
		int y_mid = (int) Math.round(height / 2d);
		
		// then relocate the polygon within the invisible parent's boundaries
		polygon.getPoints().get(0).setX(PEAK_WIDTH);
		polygon.getPoints().get(0).setY(0);
		polygon.getPoints().get(1).setX(width - PEAK_WIDTH);
		polygon.getPoints().get(1).setY(0);
		polygon.getPoints().get(2).setX(width);
		polygon.getPoints().get(2).setY(y_mid);
		polygon.getPoints().get(3).setX(width - PEAK_WIDTH);
		polygon.getPoints().get(3).setY(height);
		polygon.getPoints().get(4).setX(PEAK_WIDTH);
		polygon.getPoints().get(4).setY(height);
		polygon.getPoints().get(5).setX(0);
		polygon.getPoints().get(5).setY(y_mid);
		
		return true;
	}
	
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		
		// resize prefix label
		Size preferredPrefixLabelSize = determineMinimalPrefixLabelSize(context);
		Text prefixLabel = OperationUtil.getPrefixLabel(context.getPictogramElement());
		if (prefixLabel.getWidth() != preferredPrefixLabelSize.getWidth()
				|| prefixLabel.getHeight() != preferredPrefixLabelSize.getHeight()) {
			prefixLabel.setWidth(preferredPrefixLabelSize.getWidth());
			prefixLabel.setHeight(preferredPrefixLabelSize.getHeight());
			anythingChanged = true;
		}
		
		// resize suffix label
		Size preferredSuffixLabelSize = determineMinimalSuffixLabelSize(context);
		Text suffixLabel = OperationUtil.getSuffixLabel(context.getPictogramElement());
		if (suffixLabel.getWidth() != preferredSuffixLabelSize.getWidth()
				|| suffixLabel.getHeight() != preferredSuffixLabelSize.getHeight()) {
			suffixLabel.setWidth(preferredSuffixLabelSize.getWidth());
			suffixLabel.setHeight(preferredSuffixLabelSize.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	
	private Size determineMinimalPrefixLabelSize(ILayoutContext context) {
		return determinePreferredLabelSize(context);
	}
	
	private Size determineMinimalSuffixLabelSize(ILayoutContext context) {
		Text label = OperationUtil.getSuffixLabel(context.getPictogramElement());
		Size padding = getLabelPadding();
		Size minSize = GS.getSize(label).padding(padding.getWidth(), padding.getHeight());
		return minSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		ContainerShape shapePE = (ContainerShape) context.getPictogramElement();
		int numberOfParameters = shapePE.getChildren().size();
		
		Size minSize = this.determineMinimalPrefixLabelSize(context);
		if (numberOfParameters > 1) {
			minSize.addWidth( (numberOfParameters - 1) * GraphicsAlgorithmsFactory.PADDING_SMALL );
		}
		for (Shape child : shapePE.getChildren()) {
			Size preferredChildSize = determineChildSize(child);
			minSize.addWidth(preferredChildSize.getWidth());
			minSize.setHeight(Math.max(minSize.getHeight(), preferredChildSize.getHeight()));
		}
		Size suffixLabelSize = this.determineMinimalSuffixLabelSize(context);
		minSize.addWidth(suffixLabelSize.getWidth());
		if (suffixLabelSize.getHeight() > minSize.getHeight()) {
			minSize.setHeight(suffixLabelSize.getHeight());
		}
		
		return minSize;
	}
	
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		ContainerShape shapePE = (ContainerShape) context.getPictogramElement();
		GraphicsAlgorithm rootGA = getRootGA(context);
		
		// place the prefix label vertically centered, left-aligned in the parent's drawing area
		Text label = OperationUtil.getPrefixLabel(context.getPictogramElement());
		int x = PEAK_WIDTH;
		int y = (int) Math.round(rootGA.getHeight()/2d - label.getHeight()/2d);
		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			anythingChanged = true;
		}
		x += label.getWidth();
		
		for (Shape child : shapePE.getChildren()) {
			// re-locate the child nodes (parameters)
			Size preferredChildSize = determineChildSize(child);
			
			y = (int) Math.round(rootGA.getHeight()/2d - preferredChildSize.getHeight()/2d);
			
			GraphicsAlgorithm childGA = child.getGraphicsAlgorithm();
			childGA.setX(x);
			childGA.setY(y);
			
			x += preferredChildSize.getWidth() + GraphicsAlgorithmsFactory.PADDING_SMALL;
		}
		if (shapePE.getChildren().size() > 0) {
			x -= GraphicsAlgorithmsFactory.PADDING_SMALL;
		}
		
		// also place the suffix label vertically centered, left-aligned in the parent's drawing area
		label = OperationUtil.getSuffixLabel(context.getPictogramElement());
		y = (int) Math.round(rootGA.getHeight()/2d - label.getHeight()/2d);
		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			anythingChanged = true;
		}
		
		return anythingChanged;
	}

	protected Size determineChildSize(Shape childPE) {
		LayoutContext childLayoutContext = new LayoutContext(childPE);
		AbstractShapeWithLabelLayoutFeature childLayoutFeature = (AbstractShapeWithLabelLayoutFeature) getFeatureProvider()
				.getLayoutFeature(childLayoutContext);
		Size preferredChildSize = childLayoutFeature.determineMinimalShapeSize(childLayoutContext);
		return preferredChildSize;
	}
}
