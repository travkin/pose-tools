/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;
import de.upb.pose.specification.ui.editor.helpers.TargetConnectionHelper;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class ActionUpdateFeature extends AbstractShapeWithLabelUpdateFeature {

	public ActionUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	protected Action getBusinessObject(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		if (pe != null) {
			Object bo = getBusinessObjectForPictogramElement(pe);
			if (bo != null && bo instanceof Action) {
				return (Action) bo;
			}
		}
		return null;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.update.AbstractShapeWithLabelUpdateFeature#getRootGraphicsAlgorithm(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	protected GraphicsAlgorithm getRootGraphicsAlgorithm(IUpdateContext context)
	{
		return context.getPictogramElement().getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		return (getBusinessObject(context) != null);
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context) {
		Action bo = getBusinessObject(context);
		
		if (context.getPictogramElement() instanceof Shape) {
			// background color
			IColorConstant boColor = getColor(bo);
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		} else if (isCallActionsTargetConnection(context)) {
			// call target label
			String requiredText = TargetConnectionHelper.getTargetLabelText((CallAction) bo);
			Text visibleText = TargetConnectionHelper.getTargetLabelText((Connection) context.getPictogramElement());
			if (GS.unequals(requiredText, visibleText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_DECORATOR);
			}
		}
		
		return Reason.createFalseReason();
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context) {
		Action bo = getBusinessObject(context);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);
		
		if (context.getPictogramElement() instanceof Shape) {
			// background color
			IColorConstant boColor = getColor(bo);
			setBackgroundColor(context, boColor);
		} else if (isCallActionsTargetConnection(context)) {
			Connection connection = (Connection) context.getPictogramElement();
			// this must be a target connection --> update the label
			String newText = TargetConnectionHelper.getTargetLabelText((CallAction) bo);
			Text label = TargetConnectionHelper.getTargetLabelText(connection);
			if (newText != null) {
				if (label == null) {
					ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(connection, false, true);
					label = GraphicsAlgorithmsFactory.addLabel(decorator, newText, colorBlack);
					// TODO should there be a layout feature?
					TargetConnectionHelper.updateTargetLabelSizeAndLocation(label, getDiagram());
				}
				label.setValue(newText);
				label.getPictogramElement().setVisible(true);
			} else {
				if (label != null) {
					label.getPictogramElement().setVisible(false);
				}
			}
		}
		
		layoutPictogramElement(context.getPictogramElement());
		
		return true;
	}
	
	private boolean isCallActionsTargetConnection(IUpdateContext context) {
		Action bo = getBusinessObject(context);
		if (context.getPictogramElement() instanceof Connection && bo instanceof CallAction) {
			Connection connection = (Connection) context.getPictogramElement();
			PictogramLink targetBOLink = connection.getEnd().getParent().getLink();
			if (targetBOLink != null && targetBOLink.getBusinessObjects().get(0) instanceof Variable) {
				// this must be a target connection
				return true;
			}
		}
		return false;
	}

}
