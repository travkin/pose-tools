package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.GraphitiUtil;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;
import de.upb.pose.specification.ui.util.CrossReferenceUtil;

public class TypeUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public TypeUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return pe instanceof ContainerShape && bo instanceof Type;
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();

		if (pe instanceof ContainerShape) {
			Type bo = (Type) getBusinessObjectForPictogramElement(pe);
			
			// text value
			String boText = bo.getName();
			String peText = getLabelText(context);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// text font
			FontDescription boFont = getFont(bo);
			Font peFont = getLabelFont(context);
			if (GS.differ(boFont, peFont)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_FONT);
			}
			
			// background color
			IColorConstant boColor = getColor(bo);
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
			
			// line style
			LineStyle boLine = getLineStyle(bo);
			LineStyle peLine = getLineStyle(context);
			if (!boLine.equals(peLine)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_LINE_STYLE);
			}
		}

		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Type bo = (Type) getBusinessObjectForPictogramElement(pe);

		// text value
		String boText = bo.getName();
		String peText = getLabelText(context);
		if (GS.differ(boText, peText)) {
			setLabelText(context, boText);

			// update all elements that reference this type
			Collection<EObject> references = CrossReferenceUtil.getReferences(bo);
			for (EObject refBo : references) {
				for (PictogramElement refPe : GraphitiUtil.getPictogramElements(getDiagram(), refBo)) {
					updatePictogramElement(refPe);
				}
			}
		}

		// text font
		FontDescription boFont = getFont(bo);
		setLabelFont(context, boFont);

		// background color
		IColorConstant boColor = getColor(bo);
		setBackgroundColor(context, boColor);

		// line style
		LineStyle boLine = getLineStyle(bo);
		setLineStyle(context, boLine);

		layoutPictogramElement(pe);

		return true;
	}
	
	public LineStyle getLineStyle(Type bo) {
		switch (bo.getAbstraction()) {
		case ANY:
			return LineStyle.DASH;

		default:
			return LineStyle.SOLID;
		}
	}
	
	public FontDescription getFont(Type bo) {
		if (AbstractionType.ABSTRACT.equals(bo.getAbstraction())) {
			return FontConstants.FONT_12_BOLD_ITALIC;
		} else {
			return FontConstants.FONT_12_BOLD_REGULAR;
		}
	}

}
