/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

/**
 * @author Dietrich Travkin
 */
public class SubsystemAddFeature extends AbstractShapeWithLabelAddFeature {

	public SubsystemAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Subsystem;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected Subsystem getBusinessObject(IAddContext context) {
		return (Subsystem) context.getNewObject();
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelTextForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		Subsystem subsystem = getBusinessObject(context);
		return subsystem.getName();
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#getLabelFontDescriptionForBusinessObject(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_12_BOLD_REGULAR;
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#updateSetFragments()
	 */
	@Override
	protected boolean updateSetFragments() {
		return true;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#createLabelParent(org.eclipse.graphiti.mm.pictograms.ContainerShape)
	 */
	protected GraphicsAlgorithm createLabelParent(ContainerShape parentShapePE) {
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		Color colorGray = manageColor(IColorConstant.LIGHT_GRAY);
		
		Rectangle frame = GraphicsAlgorithmsFactory.addRectangle(parentShapePE, colorWhite, colorBlack, LineStyle.SOLID);
		frame.setTransparency(0.5d);
		
		Rectangle rectangle = GraphicsAlgorithmsFactory.addRectangle(frame, colorGray, colorBlack, LineStyle.SOLID);
		
		return rectangle;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.add.AbstractShapeWithLabelAddFeature#initializeSizeAndLocation(org.eclipse.graphiti.features.context.IAddContext, org.eclipse.graphiti.mm.pictograms.ContainerShape, org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm)
	 */
	@Override
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		GraphicsAlgorithm rootGA = parentShapePE.getGraphicsAlgorithm();
		rootGA.setX(context.getX());
		rootGA.setY(context.getY());
		rootGA.setWidth(context.getWidth());
		rootGA.setHeight(context.getHeight());
	}

}
