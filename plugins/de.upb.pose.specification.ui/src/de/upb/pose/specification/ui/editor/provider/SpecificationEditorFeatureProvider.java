package de.upb.pose.specification.ui.editor.provider;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.IResizeShapeFeature;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider;

import de.upb.pose.core.Named;
import de.upb.pose.core.features.NoMoveShapeFeature;
import de.upb.pose.core.features.NoResizeFeature;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.features.NamedDirectEditingFeature;
import de.upb.pose.specification.ui.editor.features.PatternSpecificationLabelProviderService;
import de.upb.pose.specification.ui.editor.features.add.AccessRuleAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ActionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ActionParentConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.AttributeAddFeature;
import de.upb.pose.specification.ui.editor.features.add.AttributeParentConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.CallActionCalleeConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.CreateActionInstantiatedTypeConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.InheritanceAddFeature;
import de.upb.pose.specification.ui.editor.features.add.OperationAddFeature;
import de.upb.pose.specification.ui.editor.features.add.OperationParentConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ParameterAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ParameterAssignmentAddFeature;
import de.upb.pose.specification.ui.editor.features.add.PatternEnvironmentAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ReferenceConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ReferenceNodeAddFeature;
import de.upb.pose.specification.ui.editor.features.add.ResultVariableAddFeature;
import de.upb.pose.specification.ui.editor.features.add.SetFragmentAddFeature;
import de.upb.pose.specification.ui.editor.features.add.SubsystemAddFeature;
import de.upb.pose.specification.ui.editor.features.add.TargetConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.TaskDescriptionAddFeature;
import de.upb.pose.specification.ui.editor.features.add.TypeAddFeature;
import de.upb.pose.specification.ui.editor.features.add.VariableAccessActionAccessedVariableConnectionAddFeature;
import de.upb.pose.specification.ui.editor.features.layout.ActionLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.AttributeLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.OperationLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.ParameterAssignmentLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.ParameterLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.PatternEnvironmentLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.ReferenceConnectionLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.ReferenceNodeLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.ResultVariableLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.SelfVariableLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.SetFragmentLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.SubsystemLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.TaskDescriptionLayoutFeature;
import de.upb.pose.specification.ui.editor.features.layout.TypeLayoutFeature;
import de.upb.pose.specification.ui.editor.features.move.ParameterMoveFeature;
import de.upb.pose.specification.ui.editor.features.move.ReferenceMoveFeature;
import de.upb.pose.specification.ui.editor.features.move.SetElementMoveFeature;
import de.upb.pose.specification.ui.editor.features.move.TypeMoveFeature;
import de.upb.pose.specification.ui.editor.features.resize.SetElementResizeFeature;
import de.upb.pose.specification.ui.editor.features.update.AccessRuleUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.ActionUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.AttributeUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.OperationUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.ParameterAssignmentUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.ParameterUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.PatternEnvironmentUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.ReferenceUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.ResultVariableUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.SetFragmentUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.SubsystemUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.TaskDescriptionUpdateFeature;
import de.upb.pose.specification.ui.editor.features.update.TypeUpdateFeature;

public class SpecificationEditorFeatureProvider extends DefaultFeatureProvider {
	
	private PatternSpecificationLabelProviderService labelService;
	
	public SpecificationEditorFeatureProvider(IDiagramTypeProvider dtp) {
		super(dtp);
		this.labelService = new PatternSpecificationLabelProviderService(this);
	}
	
	public PatternSpecificationLabelProviderService getLabelService() {
		return this.labelService;
	}

	@Override
	public IDirectEditingFeature getDirectEditingFeature(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof Named) {
			return new NamedDirectEditingFeature(this);
		}

		return super.getDirectEditingFeature(context);
	}

	@Override
	public IAddFeature getAddFeature(IAddContext context) {
		Object bo = context.getNewObject();

		if (bo instanceof Type) {
			return new TypeAddFeature(this);
		}
		if (bo instanceof Attribute) {
			return new AttributeAddFeature(this);
		}
		if (TypesPackage.Literals.FEATURE__PARENT_TYPE.equals(bo)) {
			return new AttributeParentConnectionAddFeature(this);
		}
		if (bo instanceof Operation) {
			return new OperationAddFeature(this);
		}
		if (TypesPackage.Literals.OPERATION__PARENT_TYPE.equals(bo)) {
			return new OperationParentConnectionAddFeature(this);
		}
		if (bo instanceof Parameter) {
			return new ParameterAddFeature(this);
		}
		if (bo instanceof Reference) {
			if (context instanceof IAddConnectionContext) {
				return new ReferenceConnectionAddFeature(this);
			} else {
				return new ReferenceNodeAddFeature(this);
			}
		}

		if (TypesPackage.Literals.TYPE__SUPER_TYPE.equals(bo) || TypesPackage.Literals.OPERATION__OVERRIDES.equals(bo)) {
			return new InheritanceAddFeature(this);
		}

		if (bo instanceof SetFragment) {
			return new SetFragmentAddFeature(this);
		}
		if (bo instanceof TaskDescription) {
			return new TaskDescriptionAddFeature(this);
		}

		if (bo instanceof PatternEnvironment) {
			return new PatternEnvironmentAddFeature(this);
		}
		if (bo instanceof AccessRule) {
			return new AccessRuleAddFeature(this);
		}
		if (bo instanceof Action) {
			return new ActionAddFeature(this);
		}
		if (ActionsPackage.Literals.ACTION__OPERATION.equals(bo)) {
			return new ActionParentConnectionAddFeature(this);
		}
		if (ActionsPackage.Literals.CALL_ACTION__CALLED_OPERATION.equals(bo)) {
			return new CallActionCalleeConnectionAddFeature(this);
		}
		if (ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE.equals(bo)) {
			return new VariableAccessActionAccessedVariableConnectionAddFeature(this);
		}
		if (ActionsPackage.Literals.CREATE_ACTION__INSTANTIATED_TYPE.equals(bo)) {
			return new CreateActionInstantiatedTypeConnectionAddFeature(this);
		}
		if (ActionsPackage.Literals.CALL_ACTION__TARGET.equals(bo) || ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__TARGET.equals(bo)) {
			return new TargetConnectionAddFeature(this);
		}
		if (bo instanceof ParameterAssignment) {
			return new ParameterAssignmentAddFeature(this);
		}
		if (bo instanceof ResultVariable) {
			return new ResultVariableAddFeature(this);
		}
		
		if (bo instanceof Subsystem) {
			return new SubsystemAddFeature(this);
		}

		return super.getAddFeature(context);
	}

	@Override
	public IMoveShapeFeature getMoveShapeFeature(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof ParameterAssignment) {
			return new NoMoveShapeFeature(this);
		}

		if (bo instanceof Parameter) {
			return new ParameterMoveFeature(this);
		}

		if (bo instanceof SetFragmentElement) {
			if (bo instanceof Reference) {
				return new ReferenceMoveFeature(this);
			}

			if (bo instanceof Type) {
				return new TypeMoveFeature(this);
			}

			return new SetElementMoveFeature(this);
		}

		return super.getMoveShapeFeature(context);
	}

	@Override
	public IUpdateFeature getUpdateFeature(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		// types
		if (bo instanceof Type) {
			return new TypeUpdateFeature(this);
		}
		if (bo instanceof Reference) {
			return new ReferenceUpdateFeature(this);
		}
		if (bo instanceof Attribute) {
			return new AttributeUpdateFeature(this);
		}
		if (bo instanceof Operation) {
			return new OperationUpdateFeature(this);
		}
		if (bo instanceof Parameter) {
			return new ParameterUpdateFeature(this);
		}
		if (bo instanceof Subsystem) {
			return new SubsystemUpdateFeature(this);
		}
				
		// actions
		if (bo instanceof ParameterAssignment) {
			return new ParameterAssignmentUpdateFeature(this);
		}
		if (bo instanceof Action) {
			return new ActionUpdateFeature(this);
		}
		if (bo instanceof ResultVariable) {
			return new ResultVariableUpdateFeature(this);
		}

		if (bo instanceof SetFragment) {
			return new SetFragmentUpdateFeature(this);
		}
		if (bo instanceof TaskDescription) {
			return new TaskDescriptionUpdateFeature(this);
		}
		
		// access
		if (bo instanceof PatternEnvironment) {
			return new PatternEnvironmentUpdateFeature(this);
		}
		if (bo instanceof AccessRule) {
			return new AccessRuleUpdateFeature(this);
		}

		return super.getUpdateFeature(context);
	}

	@Override
	public ILayoutFeature getLayoutFeature(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof SetFragment) {
			return new SetFragmentLayoutFeature(this);
		}
		if (bo instanceof TaskDescription) {
			return new TaskDescriptionLayoutFeature(this);
		}

		if (bo instanceof Action) {
			return new ActionLayoutFeature(this);
		}
		if (bo instanceof ParameterAssignment) {
			return new ParameterAssignmentLayoutFeature(this);
		}

		if (bo instanceof Type) {
			return new TypeLayoutFeature(this);
		}
		if (bo instanceof Attribute) {
			return new AttributeLayoutFeature(this);
		}
		if (bo instanceof Operation) {
			return new OperationLayoutFeature(this);
		}
		if (bo instanceof Reference) {
			if(pe instanceof ContainerShape) {
				return new ReferenceNodeLayoutFeature(this);
			} else if (pe instanceof Connection) {
				return new ReferenceConnectionLayoutFeature(this);
			}
		}
		if (bo instanceof Parameter) {
			return new ParameterLayoutFeature(this);
		}
		if (bo instanceof Subsystem) {
			return new SubsystemLayoutFeature(this);
		}
		
		if (bo instanceof ResultVariable) {
			return new ResultVariableLayoutFeature(this);
		}
		if (bo instanceof SelfVariable) {
			return new SelfVariableLayoutFeature(this);
		}
		
		if (bo instanceof PatternEnvironment) {
			return new PatternEnvironmentLayoutFeature(this);
		}

		return super.getLayoutFeature(context);
	}

	@Override
	public IResizeShapeFeature getResizeShapeFeature(IResizeShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof Type
				|| bo instanceof SetFragment
				|| bo instanceof Subsystem 
				|| bo instanceof TaskDescription) {
			return new SetElementResizeFeature(this);
		} else {
			return new NoResizeFeature(this);
		}
	}
}
