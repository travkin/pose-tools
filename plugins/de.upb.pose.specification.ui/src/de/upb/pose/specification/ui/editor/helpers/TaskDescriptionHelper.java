package de.upb.pose.specification.ui.editor.helpers;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.TaskDescription;

public final class TaskDescriptionHelper extends SetElementHelper {
	
	private TaskDescriptionHelper() {
		// hide constructor
	}

	public static ContainerShape getShape(PictogramElement pe) {
		if (pe instanceof ContainerShape) {
			return (ContainerShape) pe;
		}

		if (pe instanceof Connection) {
			return getShape(((Connection) pe).getEnd().getParent());
		}

		return null;
	}

	public static Text getNameText(PictogramElement pe) {
		return (Text) getShape(pe).getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0)
				.getGraphicsAlgorithmChildren().get(1);
	}

	public static MultiText getCommentText(PictogramElement pe) {
		return (MultiText) getShape(pe).getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0)
				.getGraphicsAlgorithmChildren().get(2);
	}

	public static String getName(DesignElement sbo) {
		Collection<String> existing = new HashSet<String>();
		for (TaskDescription task : sbo.getTasks()) {
			existing.add(task.getName());
		}

		String prefix = "Task";
		String name = prefix;
		int index = 2;
		while (existing.contains(name)) {
			name = prefix + " (" + index + ")";
		}

		return name;
	}

	public static Polygon getPolygon(PictogramElement pe) {
		return (Polygon) getShape(pe).getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}

	public static Polygon getDogEar(PictogramElement pe) {
		return (Polygon) getShape(pe).getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0)
				.getGraphicsAlgorithmChildren().get(0);
	}

	public static Font getTitleFont(Diagram diagram) {
		return getFont(diagram, 10, false, true);
	}

	public static Font getTextFont(Diagram diagram) {
		return getFont(diagram, 9, false, false);
	}

	public static Size getMinimumSize(TaskDescription bo) {
		return new Size(80, 50);
	}
}
