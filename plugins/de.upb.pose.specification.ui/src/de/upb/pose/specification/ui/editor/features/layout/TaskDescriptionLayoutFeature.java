package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.ui.editor.helpers.TaskDescriptionHelper;

public class TaskDescriptionLayoutFeature extends AbstractLayoutFeature {
	public TaskDescriptionLayoutFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean layout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();

		Text nameText = TaskDescriptionHelper.getNameText(pe);
		MultiText commentText = TaskDescriptionHelper.getCommentText(pe);

		int w = TaskDescriptionHelper.getShape(pe).getGraphicsAlgorithm().getWidth();
		int h = TaskDescriptionHelper.getShape(pe).getGraphicsAlgorithm().getHeight();

		Polygon polygon = TaskDescriptionHelper.getPolygon(pe);
		polygon.getPoints().get(1).setX(w - 16);
		polygon.getPoints().get(2).setX(w);
		polygon.getPoints().get(3).setX(w);
		polygon.getPoints().get(3).setY(h);
		polygon.getPoints().get(4).setY(h);

		Polygon dogEar = TaskDescriptionHelper.getDogEar(pe);
		dogEar.getPoints().get(0).setX(w - 16);
		dogEar.getPoints().get(1).setX(w);
		dogEar.getPoints().get(2).setX(w - 16);
		dogEar.getPoints().get(3).setX(w - 16);

		nameText.setWidth(w - 8);
		commentText.setWidth(w - 8);
		commentText.setHeight(h - 20);

		return true;
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof TaskDescription;
	}
}
