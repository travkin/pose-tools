package de.upb.pose.specification.ui.properties.sections;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionComboSectionBase;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.util.VisibleVariablesCollector;

public class ParameterAssignmentVariableSection extends CollectionComboSectionBase<Variable> {
	
	@Override
	protected EStructuralFeature getFeature() {
		return ActionsPackage.Literals.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE;
	}

	@Override
	protected List<Variable> getItems() {
		List<Variable> items = new ArrayList<Variable>();

		ParameterAssignment bo = (ParameterAssignment) getElement();
		CallAction parentAction = bo.getAction();
		Set<Variable> visibleVariables = VisibleVariablesCollector.get().findVariablesVisibleFor(parentAction);
		
		items.add(null); // add 'no value'
		items.addAll(visibleVariables);
		
		return items;
	}

	@Override
	protected String getText(Variable element) {
		if (element != null) {
			return element.getName();
		}
		return EMPTY;
	}

	@Override
	protected String getLabelText() {
		return "Variable";
	}
}
