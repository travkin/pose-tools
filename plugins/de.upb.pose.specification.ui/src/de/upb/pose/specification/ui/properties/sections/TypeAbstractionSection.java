package de.upb.pose.specification.ui.properties.sections;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionRadioSectionBase;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.TypesPackage;

public class TypeAbstractionSection extends CollectionRadioSectionBase<AbstractionType> {
	@Override
	protected String getDescription() {
		return "Abstraction";
	}

	@Override
	protected List<AbstractionType> getValues() {
		return AbstractionType.VALUES;
	}

	@Override
	protected EStructuralFeature getFeature() {
		return TypesPackage.Literals.TYPE__ABSTRACTION;
	}
}
