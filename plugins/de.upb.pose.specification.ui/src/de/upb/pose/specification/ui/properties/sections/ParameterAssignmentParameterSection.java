package de.upb.pose.specification.ui.properties.sections;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;

import de.upb.pose.core.ui.properties.CollectionComboSectionBase;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.types.Parameter;

public class ParameterAssignmentParameterSection extends CollectionComboSectionBase<Parameter> {
	
	@Override
	protected EStructuralFeature getFeature() {
		return ActionsPackage.Literals.PARAMETER_ASSIGNMENT__PARAMETER;
	}

	@Override
	protected List<Parameter> getItems() {
		List<Parameter> items = new ArrayList<Parameter>();

		ParameterAssignment bo = (ParameterAssignment) getElement();

		// add all parameters of operation
		items.addAll(bo.getAction().getCalledOperation().getParameters());

		return items;
	}

	@Override
	protected String getText(Parameter element) {
		if (element != null) {
			return element.getName();
		}
		return EMPTY;
	}

	@Override
	protected String getLabelText() {
		return "Parameter";
	}
}
