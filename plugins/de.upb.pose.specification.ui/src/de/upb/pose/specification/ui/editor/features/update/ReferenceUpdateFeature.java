package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternElementHelper.getColor;
import static de.upb.pose.specification.ui.editor.helpers.ReferenceUtil.getText;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.ui.editor.helpers.ReferenceUtil;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

public class ReferenceUpdateFeature extends AbstractShapeWithLabelUpdateFeature {
	
	public ReferenceUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	protected GraphicsAlgorithm getRootGraphicsAlgorithm(IUpdateContext context)
	{
		return ReferenceUtil.getPolygon(context.getPictogramElement());
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Reference bo = (Reference) getBusinessObjectForPictogramElement(pe);
		
		if (pe instanceof ContainerShape) {
			// name
			String boName = getText(bo);
			String peName = getLabelText(context);
			if (GS.differ(boName, peName)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
			
			// background color
			IColorConstant boColor = getColor(bo);
			Color peColor = getBackgroundColor(context);
			if (GS.differ(boColor, peColor)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_BG_COLOR);
			}
		} else if (pe instanceof Connection) {
			// cardinality text
			String boCardinality = ReferenceUtil.getCardinality(bo);
			Text peCardinality = ReferenceUtil.getCardinalityText(pe);
			if (GS.unequals(boCardinality, peCardinality)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_DECORATOR);
			}

			// reference shape line style
			LineStyle boLineStyle = ReferenceUtil.getLineStyle(bo);
			LineStyle peLineStyle = getLineStyle(context);
			if (!boLineStyle.equals(peLineStyle)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_LINE_STYLE);
			}
		}
		
		return Reason.createFalseReason();
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Reference bo = (Reference) getBusinessObjectForPictogramElement(pe);

		if (pe instanceof ContainerShape) {
			// name text
			String boName = ReferenceUtil.getText(bo);
			setLabelText(context, boName);
			
			// background color
			IColorConstant boColor = getColor(bo);
			setBackgroundColor(context, boColor);
		} else if (pe instanceof Connection) {
			// cardinality text
			String boCardinality = ReferenceUtil.getCardinality(bo);
			Text peCardinality = ReferenceUtil.getCardinalityText(pe);
			if (GS.unequals(boCardinality, peCardinality)) {
				peCardinality.setValue(boCardinality);
			}
	
			// reference shape line style
			LineStyle boLineStyle = ReferenceUtil.getLineStyle(bo);
			setLineStyle(context, boLineStyle);
		}

		layoutPictogramElement(pe);

		return true;
	}

	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof Reference;
	}
}
