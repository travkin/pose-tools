package de.upb.pose.specification.ui.editor.features.move;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.specification.types.Parameter;

public class ParameterMoveFeature extends DefaultMoveShapeFeature {
	
	public ParameterMoveFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		Shape pe = context.getShape();
		Object bo = getBusinessObjectForPictogramElement(pe);

		if (bo instanceof Parameter) {
			// TODO: implement moving parameter in list
		}
		return false;
	}
}
