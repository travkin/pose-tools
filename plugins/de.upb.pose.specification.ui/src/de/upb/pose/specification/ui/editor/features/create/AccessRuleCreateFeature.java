package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.util.SpecificationUtil;

public class AccessRuleCreateFeature extends CreateConnectionFeature {
	public AccessRuleCreateFeature(IFeatureProvider fp) {
		super(fp, "Access Rule");
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		PictogramElement spe = context.getSourcePictogramElement();
		PatternElement sbo = (PatternElement) getBusinessObjectForPictogramElement(spe);

		PictogramElement tpe = context.getTargetPictogramElement();
		Accessable tbo = (Accessable) getBusinessObjectForPictogramElement(tpe);

		PatternSpecification specification = SpecificationUtil.getSpecification(sbo);

		DesignPatternCatalog catalog = specification.getPattern().getCatalog();
		AccessType accessType = catalog.getAccessTypes().get(0);
		
		AccessRule bo = AccessFactory.eINSTANCE.createAccessRule();
		bo.setSpecification(specification);
		bo.setAccessor(sbo);
		bo.setAccessedElement(tbo);
		bo.setCanBeGenerated(false);
		bo.setAccessType(accessType);
		bo.setForbidden(false);

		AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
		addContext.setNewObject(bo);

		return (Connection) getFeatureProvider().addIfPossible(addContext);
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(AccessPackage.Literals.ACCESS_RULE);
	}

	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return source instanceof PatternElement && target instanceof Accessable;
	}

	@Override
	protected boolean canStart(EObject source) {
		return source instanceof PatternElement;
	}
}
