/**
 * 
 */
package de.upb.pose.specification.ui.editor.features;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.util.GraphitiUtil;

/**
 * @author Dietrich Travkin
 */
public class BusinessObjectFinder {

	private static BusinessObjectFinder instance = null;
	
	private BusinessObjectFinder() {}
	
	public static BusinessObjectFinder get() {
		if (instance == null) {
			instance = new BusinessObjectFinder();
		}
		return instance;
	}
	
	public EObject getFirstBusinessObject(PictogramElement pictogramElement) {
		return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pictogramElement);
	}
	
	public <T> T getFirstBusinessObjectOfType(PictogramElement pictogramElement, Class<T> businessObjectType)
	{
		return GraphitiUtil.getFirstBusinessObjectOfType(pictogramElement, businessObjectType);
	}
	
}
