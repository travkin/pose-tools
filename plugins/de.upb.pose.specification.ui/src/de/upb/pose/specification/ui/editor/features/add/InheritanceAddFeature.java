package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

public class InheritanceAddFeature extends AddConnectionFeature {
	public InheritanceAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected boolean canAdd(EObject element) {
		return TypesPackage.Literals.TYPE__SUPER_TYPE.equals(element)
				|| TypesPackage.Literals.OPERATION__OVERRIDES.equals(element);
	}

	@Override
	public Connection add(IAddConnectionContext context) {
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());
		
		Object inheritingTypeOrOperationBO = getBusinessObjectForPictogramElement(context.getSourceAnchor().getParent());
		link(connection, inheritingTypeOrOperationBO);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);

		GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.SOLID);
		GraphicsFactory.addConnectionDecorator_FilledArrow_TargetEnd(connection, colorWhite, colorBlack);
		
		return connection;
	}
}
