package de.upb.pose.specification.ui.editor.features.update;

import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getText;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.access.PatternEnvironment;

public class PatternEnvironmentUpdateFeature extends AbstractUpdateFeature {
	public PatternEnvironmentUpdateFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canUpdate(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof PatternEnvironment;
	}

	@Override
	public boolean update(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		PatternEnvironment bo = (PatternEnvironment) getBusinessObjectForPictogramElement(pe);

		// text value
		String boName = getText(bo);
		Text peName = getText(pe);
		if (GS.unequals(boName, peName)) {
			peName.setValue(boName);
		}

		layoutPictogramElement(pe);

		return true;
	}

	@Override
	public IReason updateNeeded(IUpdateContext context) {
		PictogramElement pe = context.getPictogramElement();
		PatternEnvironment bo = (PatternEnvironment) getBusinessObjectForPictogramElement(pe);

		// text value
		if (GS.unequals(getText(bo), getText(pe))) {
			return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
		}

		return Reason.createFalseReason();
	}
}
