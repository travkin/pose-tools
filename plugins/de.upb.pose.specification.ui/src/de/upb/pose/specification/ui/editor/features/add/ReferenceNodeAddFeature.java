/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.helpers.ReferenceUtil;

/**
 * @author Dietrich Travkin
 */
public class ReferenceNodeAddFeature extends AbstractPolygonShapeWithLabelAddFeature {

	public ReferenceNodeAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Reference;
	}
	
	@Override
	protected Reference getBusinessObject(IAddContext context) {
		return (Reference) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		Reference referenceBO = this.getBusinessObject(context);
		return ReferenceUtil.getText(referenceBO);
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}
	
	@Override
	protected boolean initLocation() {
		return false;
	}
	
	protected void createAdditionalAnchorsFor(ContainerShape shapePE, Rectangle invisibleParent) {
//		BoxRelativeAnchor anchor = Graphiti.getPeCreateService().createBoxRelativeAnchor(shapePE);
//		anchor.setReferencedGraphicsAlgorithm(invisibleParent);
//		anchor.setRelativeHeight(0.5);
//		anchor.setRelativeWidth(1.0);
//		anchor.setActive(false);
//		anchor.setVisible(false);
//		anchor.setUseAnchorLocationAsConnectionEndpoint(true);
	}

}
