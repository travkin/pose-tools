package de.upb.pose.specification.ui.editor.helpers;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Font;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;

public class ReferenceUtil extends PatternElementHelper {
	
	public static FontDescription getFont() {
		return FontConstants.FONT_9_NORMAL_REGULAR;
	}

	public static String getText(Reference bo) {
		return bo.getName();
	}

	public static String getInitialName(Type bo) {
		Collection<String> existing = new HashSet<String>();
		for (Reference reference : bo.getReferences()) {
			existing.add(reference.getName());
		}

		String prefix = "ref";
		String name = prefix;
		int index = 2;
		while (existing.contains(name)) {
			name = prefix + "_" + index;
			index++;
		}

		return name;
	}

	public static String getCardinality(Reference bo) {
		switch (bo.getCardinality()) {
		case MULTIPLE:
			return "*";
		case SINGLE:
			return "0..1";
		case ANY:
		default:
			return "";
		}
	}

	public static LineStyle getLineStyle(Reference bo) {
		switch (bo.getCardinality()) {
		case ANY:
			return LineStyle.DASH;
		default:
			return LineStyle.SOLID;
		}
	}

	public static Polygon getPolygon(PictogramElement pe) {
		// TODO this is a hack in my opinion, generalize the solution to find label und polygon for all figures!
		if (pe instanceof ContainerShape
				&& !pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().isEmpty()) {
			return (Polygon) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
		}

		if (pe instanceof Connection) {
			return getPolygon(((Connection) pe).getStart().getParent());
		}
		System.out.println("err!");
		return null;
	}

	public static Text getText(PictogramElement pe) {
		// TODO this is a hack in my opinion, generalize the solution to find label und polygon for all figures!
		if (pe instanceof ContainerShape
				&& !pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().isEmpty()) {
			return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(1);
		}

		if (pe instanceof Connection
				&& getText(((Connection) pe).getStart().getParent()) != null) {
			return getText(((Connection) pe).getStart().getParent());
		}
		System.out.println("err!");
		return null;
	}

	public static Text getCardinalityText(PictogramElement pe) {
		if (pe instanceof Connection) {
			ConnectionDecorator decorator = ((Connection) pe).getConnectionDecorators().get(1);
			return (Text) decorator.getGraphicsAlgorithm();
		} else if (pe instanceof ContainerShape) {
			for (Anchor anchor : ((ContainerShape) pe).getAnchors()) {
				for (Connection outgoing : anchor.getOutgoingConnections()) {
					return getCardinalityText(outgoing);
				}
			}
		}
		return null;
	}
	
	public static void updateCardinalitySizeAndLocation(Text cardinalityLabel, Diagram parentDiagram) {
		String currentText = cardinalityLabel.getValue();
		
		Font font = ReferenceUtil.getFont().manage(parentDiagram);
		Size size = GS.getSize(font, currentText).padding(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
		
		cardinalityLabel.setFont(font);
		cardinalityLabel.setX(0);
		cardinalityLabel.setY(-size.getHeight());
		cardinalityLabel.setWidth(size.getWidth());
		cardinalityLabel.setHeight(size.getHeight());
	}
}
