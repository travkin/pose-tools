package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.WriteAction;

public class WriteActionCreateFeature extends ReadActionCreateFeature {
	
	public WriteActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Write");
	}
	
	protected WriteActionCreateFeature(IFeatureProvider fp, String name) {
		super(fp, name);
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.WRITE_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		WriteAction bo = ActionsFactory.eINSTANCE.createWriteAction();
		bo.setAccessedVariable(getVariable(target));
		return bo;
	}
}
