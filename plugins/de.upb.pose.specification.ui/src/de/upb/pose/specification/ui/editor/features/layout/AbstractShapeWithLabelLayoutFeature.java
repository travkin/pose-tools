/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.impl.LayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.GS;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.ui.editor.features.PatternSpecificationLabelProviderService;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractShapeWithLabelLayoutFeature extends AbstractShapeLayoutFeature implements IShapeWithLabelLayoutFeature {

	private static final Size LABEL_DEFAULT_PADDING = new Size(GraphicsAlgorithmsFactory.PADDING_DEFAULT, GraphicsAlgorithmsFactory.PADDING_DEFAULT);
	
	public AbstractShapeWithLabelLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	protected PatternSpecificationLabelProviderService getLabelProviderService() {
		return this.getFeatureProvider().getLabelService();
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#canHandle(java.lang.String, org.eclipse.graphiti.mm.pictograms.PictogramElement)
	 */
	@Override
	public boolean canHandle(String labelIdentifier, PictogramElement pictogramElement) {
		if (LabelConstants.LABEL_ID_NAME.equals(labelIdentifier)) {
			return true;
		}
		return false;
	}
	
	@Override
	public Text getLabel(String labelIdentifier, PictogramElement pictogramElement) {
		if (LabelConstants.LABEL_ID_NAME.equals(labelIdentifier)) {
			return getLabel(new LayoutContext(pictogramElement));
		}
		return null;
	}

	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#getLabel(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Text getLabel(ILayoutContext context) {
		GraphicsAlgorithm rootGA = getRootGA(context);
		Text childLabel = null;
		if (rootGA != null && !rootGA.getGraphicsAlgorithmChildren().isEmpty())
		{
			// look for the first child label (Text)
			for (GraphicsAlgorithm child: rootGA.getGraphicsAlgorithmChildren()) {
				if (child instanceof Text) {
					childLabel = (Text) child; break;
				}
			}
		}
		Assert.isNotNull(childLabel);
		return childLabel;
	}
	
	protected Size getLabelPadding() {
		return LABEL_DEFAULT_PADDING;
	}
	
	
	protected Size determinePreferredLabelSize(ILayoutContext context) {
		Text label = getLabel(context);
		return determinePreferredLabelSize(label);
	}
	
	protected Size determinePreferredLabelSize(Text label) {
		if (label == null) {
			return null;
		}
		
		Size padding = getLabelPadding();
		Size minSize = GS.getSize(label).padding(padding.getWidth(), padding.getHeight());
		return minSize;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.IShapeWithLabelLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		return determinePreferredLabelSize(context);
	}
	
	protected boolean resizeShapeContents(ILayoutContext context) {
		Size preferredLabelSize = determinePreferredLabelSize(context);
		
		// resize label
		Text label = getLabel(context);
		if (label.getWidth() != preferredLabelSize.getWidth()
				|| label.getHeight() != preferredLabelSize.getHeight()) {
			label.setWidth(preferredLabelSize.getWidth());
			label.setHeight(preferredLabelSize.getHeight());
			return true;
		}
		return false;
	}
	
	protected boolean relocateShapeContents(ILayoutContext context) {
		Size shapeSize = this.determineCurrentShapeSize(context); 
				
		// center the label in the parent's drawing area
		Text label = getLabel(context);
		int x = (int) Math.round(shapeSize.getWidth()/2d - label.getWidth()/2d);
		int y = (int) Math.round(shapeSize.getHeight()/2d - label.getHeight()/2d);
		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			return true;
		}
		return false;
	}
	
	protected Size determineCurrentShapeSize(ILayoutContext context) {
		GraphicsAlgorithm shapeGA = getShape(context);
		return new Size(shapeGA.getWidth(), shapeGA.getHeight());
	}
	
}
