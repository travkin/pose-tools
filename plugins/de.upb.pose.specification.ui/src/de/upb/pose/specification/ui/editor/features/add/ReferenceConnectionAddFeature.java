package de.upb.pose.specification.ui.editor.features.add;

import static de.upb.pose.specification.ui.editor.helpers.ReferenceUtil.getCardinality;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;
import de.upb.pose.specification.ui.util.SetUtil;

public class ReferenceConnectionAddFeature extends AddConnectionFeature {
	
	public ReferenceConnectionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected Connection add(IAddConnectionContext context) {
		Reference referenceBO = (Reference) context.getNewObject();
		
		// add the reference node using another add feature
		AddContext addNodeContext = new AddContext();
		addNodeContext.setNewObject(referenceBO);
		ContainerShape referenceShape = (ContainerShape) getFeatureProvider().addIfPossible(addNodeContext);
		
		// add the connection
		if (referenceShape != null) {
			Connection connection = PictogramElementsFactory.addConnection(
					getDiagram(), referenceShape.getAnchors().get(0), context.getTargetAnchor());
			link(connection, referenceBO);
			
			Color colorBlack = manageColor(IColorConstant.BLACK);
			
			// line
			GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.SOLID);
			GraphicsFactory.addConnectionDecorator_OpenArrow_TargetEnd(connection, colorBlack);

			// cardinality decorator
			ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(connection, true, true);
			GraphicsAlgorithmsFactory.addLabel(decorator, getCardinality(referenceBO), colorBlack, FontConstants.FONT_9_NORMAL_REGULAR.manage(getDiagram()));
			
			layoutPictogramElement(connection);
			
			SetUtil.updateSets(getDiagram());
			
			return connection;
		}

		return null;
	}

	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Reference;
	}
}
