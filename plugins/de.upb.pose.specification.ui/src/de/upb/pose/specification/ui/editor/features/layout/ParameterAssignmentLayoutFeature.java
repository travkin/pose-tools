/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class ParameterAssignmentLayoutFeature extends AbstractShapeWithLabelLayoutFeature {

	private static final Size LABEL_PADDING = new Size(GraphicsAlgorithmsFactory.PADDING_SMALL, GraphicsAlgorithmsFactory.PADDING_SMALL);
	
	public ParameterAssignmentLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}
	
	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
			return bo instanceof ParameterAssignment;
		}
		return false;
	}

	@Override
	protected Size getLabelPadding() {
		return LABEL_PADDING;
	}
	
	protected boolean relocateShapeContents(ILayoutContext context) {
		return false; // do nothing, the operation layout does this already
	}

}
