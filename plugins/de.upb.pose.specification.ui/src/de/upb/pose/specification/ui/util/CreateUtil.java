package de.upb.pose.specification.ui.util;

import java.util.Collection;
import java.util.HashSet;

import de.upb.pose.core.Named;

public final class CreateUtil {
	
	private CreateUtil() {
		// hide constructor
	}

	public static String getInitialName(Collection<? extends Named> existings, String prefix) {
		Collection<String> names = new HashSet<String>();

		for (Named exisings : existings) {
			names.add(exisings.getName());
		}

		return getUniqueName(names, prefix);
	}

	private static String getUniqueName(Collection<String> existings, String prefix) {
		String name = prefix;
		int index = 2;

		while (existings.contains(name)) {
			name = prefix + " (" + index++ + ")";
		}

		return name;
	}
}
