package de.upb.pose.specification.ui.editor.features.create;

import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.ui.editor.provider.SpecificationImageProvider;
import de.upb.pose.specification.util.VisibleVariablesCollector;

public class TargetConnectionCreateFeature extends CreateConnectionFeature {
	
	public TargetConnectionCreateFeature(IFeatureProvider fp) {
		super(fp, "Target Connection");
	}
	
	@Override
	protected boolean canStart(EObject source) {
		if (source instanceof CallAction) {
			return ((CallAction) source).getTarget() == null;
		} else if (source instanceof VariableAccessAction) {
			return ((VariableAccessAction) source).getTarget() == null;
		}
		return false;
	}
	
	@Override
	protected boolean canCreate(EObject source, EObject target) {
		if (canStart(source)) {
			if (target instanceof Variable) {
				Action sourceAction = (Action) source;
				// only allow variables that are visible
				Set<Variable> visibleVariables = VisibleVariablesCollector.get().findVariablesVisibleFor(sourceAction);
				if (visibleVariables.contains(target)) {
					return true;
				}
			} else if (target.equals(source)) { // self-reference
				return true;
			}
		}
		return false;
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		PictogramElement spe = context.getSourcePictogramElement();
		Object srcBusinessObject = getBusinessObjectForPictogramElement(spe);
		
		PictogramElement tpe = context.getTargetPictogramElement();
		Object targetBusinessObject = getBusinessObjectForPictogramElement(tpe);
		Variable targetVariable = null;
		if (targetBusinessObject instanceof Variable) {
			targetVariable = (Variable) targetBusinessObject;
		} else { // self-reference
			targetVariable = findSelfVariable((Action) targetBusinessObject);
		}

		if (targetVariable == null) {
			return null; // abort
		}
		
		AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
		
		if (srcBusinessObject instanceof CallAction) {
			CallAction action = (CallAction) srcBusinessObject;
			action.setTarget(targetVariable);
			addContext.setNewObject(ActionsPackage.Literals.CALL_ACTION__TARGET);
		} else if (srcBusinessObject instanceof VariableAccessAction) {
			VariableAccessAction action = (VariableAccessAction) srcBusinessObject;
			action.setTarget(targetVariable);
			addContext.setNewObject(ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__TARGET);
		} else {
			throw new IllegalStateException("Unexpected action type: " + srcBusinessObject.getClass().getCanonicalName());
		}

		// draw the arrow using the add feature
		Connection pe = (Connection) getFeatureProvider().addIfPossible(addContext);

		return pe;
	}
	
	private SelfVariable findSelfVariable(Action action) {
		return action.getOperation().getParentType().getSelfVariable();
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImageProvider.TARGET_CONNECTION;
	}

}
