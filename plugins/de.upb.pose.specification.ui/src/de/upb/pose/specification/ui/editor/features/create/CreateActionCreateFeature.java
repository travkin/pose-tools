package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;

import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.types.Type;

public class CreateActionCreateFeature extends ActionCreateFeature {
	
	public CreateActionCreateFeature(IFeatureProvider fp) {
		super(fp, "Create");
	}
	
	protected CreateActionCreateFeature(IFeatureProvider fp, String name) {
		super(fp, name);
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(ActionsPackage.Literals.CREATE_ACTION);
	}

	@Override
	protected Action createActionBO(EObject target) {
		CreateAction bo = ActionsFactory.eINSTANCE.createCreateAction();
		bo.setInstantiatedType((Type) target);
		return bo;
	}

	@Override
	protected boolean canFinish(EObject target) {
		return target instanceof Type;
	}

	@Override
	protected Object getTargetConnectionNewBusinessObject() {
		return ActionsPackage.Literals.CREATE_ACTION__INSTANTIATED_TYPE;
	}
}
