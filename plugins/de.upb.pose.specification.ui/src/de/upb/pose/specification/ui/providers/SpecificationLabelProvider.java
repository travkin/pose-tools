package de.upb.pose.specification.ui.providers;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory.Descriptor.Registry;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.Named;

public class SpecificationLabelProvider extends LabelProvider {
	
	private AdapterFactoryLabelProvider aflp;

	public SpecificationLabelProvider() {
		Registry registry = Registry.INSTANCE;
		AdapterFactory af = new ComposedAdapterFactory(registry);
		aflp = new AdapterFactoryLabelProvider(af);
	}

	@Override
	public String getText(Object element) {
		if (element instanceof Named) {
			String name = ((Named) element).getName();
			if (name != null && !name.trim().isEmpty()) {
				return name;
			}
		}
		return aflp.getText(element);
	}

	@Override
	public void dispose() {
		aflp.dispose();
		aflp = null;

		super.dispose();
	}

	@Override
	public Image getImage(Object element) {
		return aflp.getImage(element);
	}
}
