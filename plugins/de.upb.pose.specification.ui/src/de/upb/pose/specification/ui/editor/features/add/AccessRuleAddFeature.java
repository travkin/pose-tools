package de.upb.pose.specification.ui.editor.features.add;

import static de.upb.pose.specification.ui.editor.helpers.AccessRuleUtil.getFont;
import static de.upb.pose.specification.ui.editor.helpers.AccessRuleUtil.getText;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

public class AccessRuleAddFeature extends AddConnectionFeature {
	public AccessRuleAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected Connection add(IAddConnectionContext context) {
		// create and link connection
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());

		AccessRule bo = (AccessRule) context.getNewObject();
		link(connection, bo);

		// add GA
		Polyline ga = Graphiti.getGaService().createPolyline(connection);
		ga.setLineWidth(1);
		ga.setLineStyle(LineStyle.DASH);
		ga.setForeground(manageColor(IColorConstant.BLACK));

		// add text
		{
			ConnectionDecorator childPe = Graphiti.getPeService().createConnectionDecorator(connection, true, 0.5, true);
			link(childPe, bo);

			Text childGa = Graphiti.getGaService().createText(childPe, getText(bo));
			childGa.setFont(getFont().manage(getDiagram()));
			childGa.setForeground(manageColor(IColorConstant.BLACK));
		}

		// add arrow
		{
			// TODO extract
			ConnectionDecorator childPe = Graphiti.getPeService().createConnectionDecorator(connection, false, 1, true);

			int[] coords = { -10, 5, 0, 0, -10, -5 };
			Polyline childGa = Graphiti.getGaService().createPolyline(childPe, coords);
			childGa.setForeground(manageColor(IColorConstant.BLACK));
		}

		// add cross
		if (bo.isForbidden()) {
			// TODO extract
			ConnectionDecorator nwsePe = Graphiti.getPeService().createConnectionDecorator(connection, false, 0.5, true);
			Polyline nwseGa = Graphiti.getGaService().createPolyline(nwsePe, new int[] { -10, -20, 10, 20 });
			nwseGa.setForeground(manageColor(IColorConstant.RED));

			ConnectionDecorator swnePe = Graphiti.getPeService().createConnectionDecorator(connection, false, 0.5, true);
			Polyline swneGa = Graphiti.getGaService().createPolyline(swnePe, new int[] { -10, 20, 10, -20 });
			swneGa.setForeground(manageColor(IColorConstant.RED));
		}
		
		return connection;
	}

	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof AccessRule;
	}
}
