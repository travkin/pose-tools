package de.upb.pose.specification.ui.editor.features.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.features.CreateConnectionFeature;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.provider.SpecificationImageProvider;

public class InheritanceCreateFeature extends CreateConnectionFeature {
	public InheritanceCreateFeature(IFeatureProvider fp) {
		super(fp, "Inheritance");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImageProvider.INHERITANCE;
	}

	@Override
	public Connection create(ICreateConnectionContext context) {
		PictogramElement sourcePe = context.getSourcePictogramElement();
		Anchor sourceAnchor = context.getSourceAnchor();
		Object sourceBo = getBusinessObjectForPictogramElement(sourcePe);

		PictogramElement targetPe = context.getTargetPictogramElement();
		Anchor targetAnchor = context.getTargetAnchor();
		Object targetBo = getBusinessObjectForPictogramElement(targetPe);

		if (sourceBo instanceof Type) {
			Type sourceType = (Type) sourceBo;
			Type targetType = (Type) targetBo;

			sourceType.setSuperType(targetType);

			AddConnectionContext addContext = new AddConnectionContext(sourceAnchor, targetAnchor);
			addContext.setNewObject(TypesPackage.Literals.TYPE__SUPER_TYPE);

			return (Connection) getFeatureProvider().addIfPossible(addContext);
		} else if (sourceBo instanceof Operation) {
			Operation sourceOperation = (Operation) sourceBo;
			Operation targetOperation = (Operation) targetBo;

			sourceOperation.setOverrides(targetOperation);

			AddConnectionContext addContext = new AddConnectionContext(sourceAnchor, targetAnchor);
			addContext.setNewObject(TypesPackage.Literals.OPERATION__OVERRIDES);

			return (Connection) getFeatureProvider().addIfPossible(addContext);
		} else {
			return null;
		}
	}

	@Override
	protected boolean canStart(EObject source) {
		return source instanceof Type || source instanceof Operation;
	}

	@Override
	protected boolean canCreate(EObject source, EObject target) {
		return (source instanceof Type && target instanceof Type)
				|| (source instanceof Operation && target instanceof Operation);
	}
}
