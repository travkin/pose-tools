package de.upb.pose.specification.ui.outline;

import org.eclipse.jface.viewers.ITreeContentProvider;

import de.upb.pose.core.ui.outline.DiagramEditorOutlinePage;
import de.upb.pose.core.ui.outline.IOutlineInputProvider;

public class SpecificationEditorOutlinePage extends DiagramEditorOutlinePage {
	public SpecificationEditorOutlinePage(IOutlineInputProvider provider) {
		super(provider);
	}

	@Override
	protected ITreeContentProvider createContentProvider() {
		return new SpecificationEditorOutlinePageContentProvider(provider.getAdapterFactory());
	}
}
