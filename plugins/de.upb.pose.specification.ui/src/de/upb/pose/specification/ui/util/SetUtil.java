package de.upb.pose.specification.ui.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.ILocationContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.util.Area;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.util.SpecificationUtil;

public final class SetUtil {
	
	private SetUtil() {
		// hide constructor
	}

	/**
	 * Updates all {@link SetFragment}s of the specification to reflect their current visual appearance.
	 * 
	 * @param diagram The {@link Diagram} which to update. <b>This has to be linked to a {@link PatternSpecification}</b>.
	 */
	public static void updateSets(Diagram diagram) {
		PatternSpecification specification = (PatternSpecification) getBO(diagram);

		// get areas of sets
		Map<SetFragment, Area> areas = new LinkedHashMap<SetFragment, Area>();
		for (SetFragment bo : SpecificationUIUtil.getSetFragments(specification)) {
			areas.put(bo, Area.fromPE(getPE(diagram, bo)));
		}

		// go through whole diagram
		TreeIterator<EObject> it = EcoreUtil.getAllContents(diagram, true);
		while (it.hasNext()) {
			EObject element = (EObject) it.next();
			if (element instanceof ContainerShape) {
				ContainerShape pe = (ContainerShape) element;
				EObject bo = getBO(pe);

				// only check for other sets, types and actions
				if (bo instanceof SetFragmentElement) {
					Area inner = Area.fromPE(pe);
					for (SetFragment set : areas.keySet()) {
						// ignore myself
						if (!set.equals(bo)) {
							Area outer = areas.get(set);
							if (outer.contains(inner)) {
								if (!set.getContainedElements().contains(bo)) {
									// add to the set
									set.getContainedElements().add((SetFragmentElement) bo);

									// TODO bring all PEs which are nodes and not set fragments to front and sent set fragments according to their hierarchy to background
									// bring their PE to front
									Graphiti.getPeService().sendToFront((ContainerShape) pe);
								}
							} else {
								// the element is not inside the set
								set.getContainedElements().remove(bo);
							}
						}
					}
				}
			}
		}

		// remove elements that are contained in an 'inner' set
		for (SetFragment set : areas.keySet()) {
			if (!set.getContainingSets().isEmpty()) {
				// collect elements that have to be removed from the 'outer' set
				Collection<SetFragmentElement> toRemove = new HashSet<SetFragmentElement>();
				for (SetFragmentElement element : set.getContainedElements()) {
					if (element.getContainingSets().contains(set)) {
						toRemove.add(element);
					}
				}

				// remove them from the outer set
				for (SetFragment outer : set.getContainingSets()) {
					outer.getContainedElements().removeAll(toRemove);
				}
			}
		}
		
		// finally, go through all references and self variables and ensure they are in the same sets as their parent types
		List<PatternElement> allSpecificationElements = SpecificationUtil.collectAllPatternElements(specification);
		for (PatternElement element: allSpecificationElements) {
			if (element instanceof Type) {
				Type type = (Type) element;
				if (type.getSelfVariable() != null) {
					SelfVariable selfVariable = type.getSelfVariable();
					
					selfVariable.getContainingSets().clear();
					for (SetFragment set: type.getContainingSets()) {
						set.getContainedElements().add(selfVariable);
					}
				}
				
				for (Reference childReference: type.getReferences()) {
					childReference.getContainingSets().clear();
					for (SetFragment set: type.getContainingSets()) {
						set.getContainedElements().add(childReference);
					}
				}
			}
		}
	}

	private static EObject getBO(PictogramElement pe) {
		return Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
	}

	private static PictogramElement getPE(Diagram diagram, EObject bo) {
		List<PictogramElement> pes = getPEs(diagram, bo);

		assert (pes.size() == 1);

		return pes.get(0);
	}

	private static List<PictogramElement> getPEs(Diagram diagram, EObject bo) {
		assert diagram != null;

		return Graphiti.getLinkService().getPictogramElements(diagram, bo);
	}

	public static Collection<SetFragmentElement> getElements(ICreateContext context) {
		Collection<SetFragmentElement> elements = new ArrayList<SetFragmentElement>();

		int cX1 = context.getX();
		int cY1 = context.getY();
		int cX2 = cX1 + context.getWidth();
		int cY2 = cY1 + context.getHeight();

		// go through container
		for (Shape shape : context.getTargetContainer().getChildren()) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(shape);
			if (bo instanceof SetFragmentElement) {
				GraphicsAlgorithm ga = shape.getGraphicsAlgorithm();

				int eX1 = ga.getX();
				int eY1 = ga.getY();
				int eX2 = eX1 + ga.getWidth();
				int eY2 = eY1 + ga.getHeight();

				if (eX1 >= cX1 && eX2 <= cX2 && eY1 >= cY1 && eY2 <= cY2) {
					elements.add((SetFragmentElement) bo);
				}
			}
		}

		return elements;
	}

	public static Collection<SetFragment> getContainingSets(ILocationContext context, ContainerShape cpe,
			PatternSpecification specification) {
		Diagram diagram = null;
		EObject container = cpe;
		while (container != null) {
			if (container instanceof Diagram) {
				diagram = (Diagram) container;
			}
			container = container.eContainer();
		}

		Collection<SetFragment> containing = new ArrayList<SetFragment>();
		int x = context.getX() + cpe.getGraphicsAlgorithm().getX();
		int y = context.getY() + cpe.getGraphicsAlgorithm().getY();
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof SetFragment) {
				List<PictogramElement> pes = Graphiti.getLinkService().getPictogramElements(diagram, element);
				if (pes.size() == 1) {
					PictogramElement pe = pes.get(0);

					int setX = pe.getGraphicsAlgorithm().getX();
					int setY = pe.getGraphicsAlgorithm().getY();
					int setWidth = pe.getGraphicsAlgorithm().getWidth();
					int setHeight = pe.getGraphicsAlgorithm().getHeight();

					if (x >= setX && x <= setX + setWidth && y >= setY && y <= setY + setHeight) {
						containing.add((SetFragment) element);
					}
				}
			}
		}
		return containing;
	}
}
