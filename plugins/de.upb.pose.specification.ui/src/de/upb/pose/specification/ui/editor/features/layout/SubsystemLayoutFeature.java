/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.layout;

import org.eclipse.core.runtime.Assert;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class SubsystemLayoutFeature extends AbstractShapeWithLabelLayoutFeature {

	public SubsystemLayoutFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see org.eclipse.graphiti.func.ILayout#canLayout(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean canLayout(ILayoutContext context) {
		if (context.getPictogramElement() instanceof ContainerShape) {
			return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof Subsystem;
		}
		return false;
	}
	
	/**
	 * Returns the root graphics algorithms, which is a rectangle and the main (root) frame of the whole figure.
	 * 
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#getRootGA(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	protected Rectangle getRootGA(ILayoutContext context) {
		return (Rectangle) super.getRootGA(context);
	}
	
	/**
	 * Returns the frame surrounding the label. That one is on the top of the figure, but not the root frame.
	 */
	private Rectangle getLabelFrame(ILayoutContext context) {
		Rectangle rootGA = getRootGA(context);
		
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().size() > 0);
		Assert.isTrue(rootGA.getGraphicsAlgorithmChildren().get(0) instanceof Rectangle);
		
		return (Rectangle) rootGA.getGraphicsAlgorithmChildren().get(0);
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#getLabel(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Text getLabel(ILayoutContext context) {
		Rectangle textFrame = getLabelFrame(context);
		
		// look for the first child label (Text)
		Text childLabel = null;
		for (GraphicsAlgorithm child: textFrame.getGraphicsAlgorithmChildren()) {
			if (child instanceof Text) {
				childLabel = (Text) child; break;
			}
		}
		
		Assert.isNotNull(childLabel);
		
		return childLabel;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#determineMinimalShapeContentsSize(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public Size determineMinimalShapeContentsSize(ILayoutContext context) {
		Size labelSize = super.determineMinimalShapeContentsSize(context);
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		
		Size containedElementsSize = new Size(0,0);
		for (Shape childPE: pe.getChildren()) {
			GraphicsAlgorithm childGA = childPE.getGraphicsAlgorithm();
			containedElementsSize.setWidth(Math.max(containedElementsSize.getWidth(), childGA.getX() + childGA.getWidth()));
			containedElementsSize.setHeight(Math.max(containedElementsSize.getHeight(), childGA.getY() + childGA.getHeight()));
		}
		
		return new Size(Math.max(labelSize.getWidth(), containedElementsSize.getWidth()), Math.max(labelSize.getHeight(), containedElementsSize.getHeight()));
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#layoutRootGA(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean layoutRootGA(ILayoutContext context) {
		boolean anythingChanged = false;
		
		GraphicsAlgorithm rootGA = getRootGA(context);
		Size preferredRootGASize = determineMinimalRootGASize(context);
		
		// only increase the shape's size if it is not big enough
		if (rootGA.getWidth() < preferredRootGASize.getWidth()) {
			rootGA.setWidth(preferredRootGASize.getWidth());
			anythingChanged = true;
		}
		if (rootGA.getHeight() < preferredRootGASize.getHeight()) {
			rootGA.setHeight(preferredRootGASize.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeLayoutFeature#layoutShape(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	public boolean layoutShape(ILayoutContext context) {
		// do nothing, since the shape is the root graphics algorithm,
		// i.e. it is laid out in #layoutRootGA(ILayoutContext).
		return false;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#resizeShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean resizeShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		
		// resize label
		anythingChanged = super.resizeShapeContents(context);
		
		// resize the label's frame to fit the root's width
		GraphicsAlgorithm rootGA = getRootGA(context);
		Rectangle labelFrame = getLabelFrame(context);
		Text label = getLabel(context);
		if (labelFrame.getWidth() != rootGA.getWidth() || labelFrame.getHeight() != label.getHeight()) {
			labelFrame.setWidth(rootGA.getWidth());
			labelFrame.setHeight(label.getHeight());
			anythingChanged = true;
		}
		
		return anythingChanged;
	}
	
	/**
	 * @see de.upb.pose.specification.ui.editor.features.layout.AbstractShapeWithLabelLayoutFeature#relocateShapeContents(org.eclipse.graphiti.features.context.ILayoutContext)
	 */
	@Override
	protected boolean relocateShapeContents(ILayoutContext context) {
		boolean anythingChanged = false;
		
		Rectangle labelFrame = getLabelFrame(context);
		if (labelFrame.getX() != 0 || labelFrame.getY() != 0) {
			labelFrame.setX(0);
			labelFrame.setY(0);
			anythingChanged = true;
		}
		
		// horizontally center the label in the parent's drawing area, align it to the top
		Text label = getLabel(context);
		int x = (int) Math.round(labelFrame.getWidth()/2d - label.getWidth()/2d);
		int y = 0;
		if (label.getX() != x || label.getY() != y) {
			label.setX(x);
			label.setY(y);
			anythingChanged = true;
		}
		
		return anythingChanged;
	}

}
