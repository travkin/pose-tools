package de.upb.pose.specification.ui.util;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;

public final class SpecificationUIUtil {
	
	// TODO move most of this functionality to {@link PatternSpecificationDiagrams2ModelConnector}
	
	private SpecificationUIUtil() {
		// hide constructor
	}

	public static DesignPatternCatalog getCatalog(Category category) {
		EObject container = category;
		while (container != null) {
			if (container instanceof DesignPatternCatalog) {
				return (DesignPatternCatalog) container;
			}
			container = container.eContainer();
		}
		return null;
	}

	public static PatternSpecification getSpecification(EObject element) {
		EObject container = element;
		while (container != null) {
			if (container instanceof PatternSpecification) {
				return (PatternSpecification) container;
			}
			container = container.eContainer();
		}
		return null;
	}

	public static Collection<SetFragment> getSetFragments(PatternSpecification specification) {
		Collection<SetFragment> sets = new ArrayList<SetFragment>();

		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof SetFragment) {
				sets.add((SetFragment) element);
			}
		}

		return sets;
	}

	public static Collection<SetFragmentElement> getSetElements(PatternSpecification specification) {
		Collection<SetFragmentElement> elements = new ArrayList<SetFragmentElement>();
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof SetFragmentElement) {
				elements.add((SetFragmentElement) element);
			}
		}
		return elements;
	}
}
