package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.IConnectionContext;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.core.util.Size;
import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;
import de.upb.pose.specification.ui.editor.helpers.TaskDescriptionHelper;

public class TaskDescriptionAddFeature extends AddConnectionFeature {
	public TaskDescriptionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected Connection add(IAddConnectionContext context) {
		// create PE
		ContainerShape pe = addShape(context);

		// create connection
		
		Anchor sourceAnchor = ((IConnectionContext) context).getSourceAnchor();
		Anchor targetAnchor = Graphiti.getPeService().getChopboxAnchor(pe);
		Connection connection = addConnection(sourceAnchor, targetAnchor);

		// configure direct editing
		IDirectEditingInfo dei = getFeatureProvider().getDirectEditingInfo();
		dei.setMainPictogramElement(pe);
		dei.setPictogramElement(pe);
		dei.setGraphicsAlgorithm(TaskDescriptionHelper.getNameText(pe));
		
		// TODO layoutPictogramElement(pe);

		return connection;
	}

	private ContainerShape addShape(IAddContext context) {
		TaskDescription bo = (TaskDescription) context.getNewObject();

		Color colorBlack = manageColor(IColorConstant.BLACK);
		Color colorWhite = manageColor(IColorConstant.WHITE);
		
		ContainerShape pe = Graphiti.getPeService().createContainerShape(getDiagram(), true);
		Graphiti.getPeService().createChopboxAnchor(pe);
		link(pe, bo);

		Size size = TaskDescriptionHelper.getMinimumSize(bo);

		int w = size.getWidth();
		int h = size.getHeight();

		// frame
		Rectangle container = Graphiti.getGaService().createRectangle(pe);
		container.setFilled(false);
		container.setLineVisible(false);
		container.setLineWidth(0);

		container.setX(context.getX() - w / 2);
		container.setY(context.getY() - h / 2);
		container.setWidth(w);
		container.setHeight(h);

		int[] coords = new int[] { 0, 0, w - 15, 0, w, 15, w, h, 0, h, 0, 0 };
		Polygon ga = Graphiti.getGaService().createPolygon(container, coords);
		ga.setBackground(colorWhite);
		ga.setForeground(colorBlack);
		ga.setLineStyle(LineStyle.SOLID);
		ga.setLineVisible(true);
		ga.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);

		// dog-ear
		coords = new int[] { w - 16, 0, w - 16, 15, w, 16, w - 16, 0 };
		Polygon dogEar = Graphiti.getGaService().createPolygon(ga, coords);
		dogEar.setForeground(colorBlack);
		dogEar.setBackground(colorWhite);
		dogEar.setLineStyle(LineStyle.SOLID);
		dogEar.setLineVisible(true);
		dogEar.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);

		// title
		Text title = Graphiti.getGaService().createText(ga, bo.getName());
		title.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
		title.setForeground(colorBlack);
		title.setFont(TaskDescriptionHelper.getTitleFont(getDiagram()));

		title.setX(8);
		title.setY(4);
		title.setHeight(20);
		title.setWidth(container.getWidth() - 24);

		// text
		MultiText text = Graphiti.getGaService().createMultiText(ga, bo.getComment());
		text.setVerticalAlignment(Orientation.ALIGNMENT_TOP);
		text.setForeground(colorBlack);
		text.setFont(TaskDescriptionHelper.getTextFont(getDiagram()));

		text.setX(4);
		text.setY(20);
		text.setHeight(container.getHeight() - 24);
		text.setWidth(container.getWidth() - 8);

		return pe;
	}

	private Connection addConnection(Anchor sourceAnchor, Anchor targetAnchor) {
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), sourceAnchor, targetAnchor);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);

		Polyline line = Graphiti.getGaService().createPolyline(connection);
		line.setLineWidth(GraphicsAlgorithmsFactory.LINE_WIDTH);
		line.setLineStyle(LineStyle.DASH);
		line.setForeground(colorBlack);

		GraphicsFactory.addConnectionDecorator_FilledCircle_SourceEnd(connection, colorBlack);

		return connection;
	}

	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof TaskDescription;
	}
}
