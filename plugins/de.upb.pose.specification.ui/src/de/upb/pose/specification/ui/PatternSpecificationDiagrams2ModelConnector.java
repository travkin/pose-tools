/**
 * 
 */
package de.upb.pose.specification.ui;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.CoreConstants;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.PatternSpecificationConstants;

/**
 * @author Dietrich Travkin
 */
public class PatternSpecificationDiagrams2ModelConnector {

	/**
	 * An {@link org.eclipse.emf.ecore.EAnnotation} with this source key
	 * references the root element of all pattern specification diagrams
	 * corresponding to a pattern specifications model file (pattern catalog).
	 */
	public static final String ANNOTATION_SRC_PATTERN_SPECIFICATION_DIAGRAMS_REF = CoreConstants.ANNOTATION_SRC_DIAGRAMS_REF;
	

	public static EAnnotation getPointerToPatternSpecificationDiagramsContainer(DesignPatternCatalog patternSpecificationsModel)
	{
		return patternSpecificationsModel.getAnnotation(ANNOTATION_SRC_PATTERN_SPECIFICATION_DIAGRAMS_REF);
	}
	
	public static EAnnotation createPointerToPatternSpecificationDiagramsContainer(DesignPatternCatalog patternSpecificationsModel) {
		EAnnotation pointerToPatternSpecificationDiagramsRoot = EcoreFactory.eINSTANCE.createEAnnotation();
		pointerToPatternSpecificationDiagramsRoot.setSource(ANNOTATION_SRC_PATTERN_SPECIFICATION_DIAGRAMS_REF);
		patternSpecificationsModel.getAnnotations().add(pointerToPatternSpecificationDiagramsRoot);
		return pointerToPatternSpecificationDiagramsRoot;
	}
	
	public static ContainerShape getLinkedPatternSpecificationDiagramsContainer(DesignPatternCatalog patternSpecificationsModel)
	{
		EAnnotation pointerToPatternSpecificationDiagramsContainer = getPointerToPatternSpecificationDiagramsContainer(patternSpecificationsModel);
		if (pointerToPatternSpecificationDiagramsContainer != null)
		{
			if (pointerToPatternSpecificationDiagramsContainer.getReferences().size() > 1) {
				throw new IllegalStateException("There is more than one reference from a PatternSpecification object to a pattern specification diagrams container.");
			}
			
			ContainerShape patternSpecificationDiagramsContainer = (ContainerShape) pointerToPatternSpecificationDiagramsContainer.getReferences().get(0);
			return patternSpecificationDiagramsContainer;
		}
		return null;
	}
	
	public static ContainerShape createPatternSpecificationDiagramsContainer() {
		return PictogramsFactory.eINSTANCE.createContainerShape();
	}
	
	public static void linkPatternSpecificationsCatalogToPatternSpecificationDiagramsContainer(DesignPatternCatalog patternSpecificationsModel, ContainerShape patternSpecificationDiagramsContainer) {
		EAnnotation pointerToPatternSpecificationDiagramsContainer = getPointerToPatternSpecificationDiagramsContainer(patternSpecificationsModel);
		if (pointerToPatternSpecificationDiagramsContainer == null) {
			throw new IllegalStateException("No pointer (EAnnotation) to the given pattern specification's diagrams container available.");
		}
		
		// pointer --> container
		if (!pointerToPatternSpecificationDiagramsContainer.getReferences().isEmpty()) {
			pointerToPatternSpecificationDiagramsContainer.getReferences().clear();
		}
		pointerToPatternSpecificationDiagramsContainer.getReferences().add(patternSpecificationDiagramsContainer);
		
		// container --> pattern specifications catalog
		PictogramLink link = patternSpecificationDiagramsContainer.getLink();
		if (link == null) {
			link = PictogramsFactory.eINSTANCE.createPictogramLink();
			link.setPictogramElement(patternSpecificationDiagramsContainer);
		} else {
			link.getBusinessObjects().clear();
		}
		link.getBusinessObjects().add(patternSpecificationsModel);
	}
	
	private static ContainerShape findPatternSpecificationDiagramsContainer(DesignPatternCatalog patternSpecificationsModel) {
		ContainerShape container = getLinkedPatternSpecificationDiagramsContainer(patternSpecificationsModel);
		
		if (container != null) {
			return container;
		}
		
		// try finding the diagram by searching in the resource set if no link to diagrams is available
		if (PatternSpecificationDiagrams2ModelConnector.getPointerToPatternSpecificationDiagramsContainer(patternSpecificationsModel) == null) {
			ResourceSet resourceSet = patternSpecificationsModel.eResource().getResourceSet();
			
			for (Resource resource: resourceSet.getResources()) {
				if (resource.getURI() != null
						&& resource.getURI().fileExtension() != null
						&& resource.getURI().fileExtension().equals(PatternSpecificationConstants.DIAGRAMS_FILE_EXTENSION)) {
					
					container = getDiagramsContainerFromResourceForSpecificationCatalog(resource, patternSpecificationsModel);
					if (container != null) {
						return container;
					}
				}
			}
			
			// if we still have not found the container, try to find a diagrams file in the same folder as the specifications file
			Resource specificationResource = patternSpecificationsModel.eResource();
			URI diagramsUri = specificationResource.getURI().trimFileExtension().appendFileExtension(PatternSpecificationConstants.DIAGRAMS_FILE_EXTENSION);
			Resource diagramsResource = resourceSet.getResource(diagramsUri, true);
			if (diagramsResource != null) {
				container = getDiagramsContainerFromResourceForSpecificationCatalog(diagramsResource, patternSpecificationsModel);
			}
		}
		
		return container;
	}
	
	public static Diagram findPatternSpecificationDiagram(PatternSpecification patternSpecification) {
		ContainerShape diagramsContainer = findPatternSpecificationDiagramsContainer(patternSpecification.getPattern().getCatalog());
		
		if (diagramsContainer != null) {
			for (Shape child : diagramsContainer.getChildren()) {
				if (child instanceof Diagram
						&& child.getLink() != null
						&& child.getLink().getBusinessObjects().size() == 1
						&& child.getLink().getBusinessObjects().get(0) instanceof PatternSpecification) {
					
					PatternSpecification linkedSpecification = (PatternSpecification) child.getLink().getBusinessObjects().get(0);
					if (patternSpecification.equals(linkedSpecification)) {
						return (Diagram) child;
					}
				}
			}
		}
		
		return null;
	}
	
	private static ContainerShape getDiagramsContainerFromResourceForSpecificationCatalog(Resource diagramResource, DesignPatternCatalog patternSpecifications) {
		if (diagramResource != null && patternSpecifications != null) {
			ContainerShape container = getSpecificationDiagramsContainer(diagramResource);
			DesignPatternCatalog linkedCatalog = getLinkedDesignPatternCatalog(container);
			
			// ensure the diagrams container belongs to the given catalog
			if (linkedCatalog.equals(patternSpecifications)) {
				return container;
			}
		}
		return null;
	}
	
	private static ContainerShape getSpecificationDiagramsContainer(Resource diagramResource) {
		if (diagramResource != null
				&& diagramResource.getContents().size() == 1
				&& diagramResource.getContents().get(0) instanceof ContainerShape) {
			return (ContainerShape) diagramResource.getContents().get(0);
		}
		return null;
	}
	
	private static DesignPatternCatalog getLinkedDesignPatternCatalog(ContainerShape specificationDiagramsContainer) {
		if (specificationDiagramsContainer != null
				&& specificationDiagramsContainer.getLink() != null
				&& specificationDiagramsContainer.getLink().getBusinessObjects().size() == 1
				&& specificationDiagramsContainer.getLink().getBusinessObjects().get(0) instanceof DesignPatternCatalog) {
			return (DesignPatternCatalog) specificationDiagramsContainer.getLink().getBusinessObjects().get(0);
		}
		return null;
	}
	
}
