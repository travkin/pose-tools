/**
 * 
 */
package de.upb.pose.specification.ui.editor.graphics;

import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;

/**
 * @author Dietrich Travkin
 */
public class GraphicsFactory {
	
	public static ConnectionDecorator addConnectionDecorator_OpenArrow_TargetEnd(Connection parentConnection, Color arrowColor) {
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, false);
		GraphicsAlgorithmsFactory.addOpenArrow(decorator, arrowColor);
		return decorator;
	}
	
	public static ConnectionDecorator addConnectionDecorator_OrthogonalLine_TargetEnd(Connection parentConnection, Color arrowColor) {
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, false);
		GraphicsAlgorithmsFactory.addOrthogonalLine(decorator, arrowColor);
		return decorator;
	}
	
	public static ConnectionDecorator addConnectionDecorator_FilledDiamond_SourceEnd(Connection parentConnection, Color diamondColor) {
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, true);
		GraphicsAlgorithmsFactory.addFilledDiamond(decorator, diamondColor);
		return decorator;
	}

	
	public static ConnectionDecorator addConnectionDecorator_FilledArrow_TargetEnd(Connection parentConnection, Color fillColor, Color lineColor) {
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, false);
		GraphicsAlgorithmsFactory.addFilledArrow(decorator, fillColor, lineColor);
		return decorator;
	}

	public static ConnectionDecorator addConnectionDecorator_FilledCircle_SourceEnd(Connection parentConnection, Color circleColor) {
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, true);
		GraphicsAlgorithmsFactory.addFilledCircle(decorator, circleColor);
		return decorator;
	}

	public static ConnectionDecorator addConnectionDecorator_Label(Connection parentConnection, String labelText, boolean sourceEnd, Color fontColor) {
		// TODO should the decorator be active?
		ConnectionDecorator decorator = PictogramElementsFactory.addConnectionDecorator(parentConnection, sourceEnd);
		GraphicsAlgorithmsFactory.addLabel(decorator, labelText, fontColor);
		return decorator;
	}

}