package de.upb.pose.specification.ui.editor.features.move;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.impl.MoveShapeContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;

public class TypeMoveFeature extends SetElementMoveFeature {
	private int beforeX;
	private int beforeY;

	public TypeMoveFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof Type;
	}

	@Override
	protected void preMoveShape(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		GraphicsAlgorithm ga = pe.getGraphicsAlgorithm();

		beforeX = ga.getX();
		beforeY = ga.getY();
	}

	@Override
	protected void postMoveShape(IMoveShapeContext context) {
		moveReferences(context);

		super.postMoveShape(context);
	}

	private void moveReferences(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		GraphicsAlgorithm ga = pe.getGraphicsAlgorithm();
		Type bo = (Type) getBusinessObjectForPictogramElement(pe);

		int deltaX = ga.getX() - beforeX;
		int deltaY = ga.getY() - beforeY;

		// collect PEs for references
		for (Reference reference : bo.getReferences()) {
			ContainerShape rpe = PictogrammElementFinder.get()
					.findMainPictogramElementInDiagramForBusinessObject(getDiagram(), reference);
			GraphicsAlgorithm rga = rpe.getGraphicsAlgorithm();
			
			MoveShapeContext mcontext = new MoveShapeContext(rpe);
			mcontext.setSourceContainer(rpe.getContainer());
			mcontext.setTargetContainer(rpe.getContainer());
			mcontext.setLocation(rga.getX() + deltaX, rga.getY() + deltaY);

			IMoveShapeFeature feature = getFeatureProvider().getMoveShapeFeature(mcontext);
			if (feature.canExecute(mcontext)) {
				feature.execute(mcontext);
			}
		}
	}
}
