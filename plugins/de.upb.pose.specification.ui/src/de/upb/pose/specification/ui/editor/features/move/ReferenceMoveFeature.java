package de.upb.pose.specification.ui.editor.features.move;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.Area;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;

public class ReferenceMoveFeature extends SetElementMoveFeature {
	
	public ReferenceMoveFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof Reference;
	}

	@Override
	protected void postMoveShape(IMoveShapeContext context) {
		// TODO fix moving reference node
		PictogramElement referencePe = context.getPictogramElement();
		Reference referenceBo = (Reference) getBusinessObjectForPictogramElement(referencePe);

		Type typeBo = referenceBo.getParentType();
		ContainerShape typePE = PictogrammElementFinder.get().findMainPictogramElementInDiagramForBusinessObject(getDiagram(), typeBo);
		
		Area referenceArea = Area.fromPE(referencePe);
		Area typeArea = Area.fromPE(typePE);
		Area snapped = referenceArea.snapTo(typeArea);

		snapped.applyTo(referencePe);

		layoutPictogramElement(referencePe);

		super.postMoveShape(context);
	}
}
