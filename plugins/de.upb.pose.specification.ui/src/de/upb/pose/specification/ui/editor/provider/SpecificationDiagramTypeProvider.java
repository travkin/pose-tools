package de.upb.pose.specification.ui.editor.provider;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

public class SpecificationDiagramTypeProvider extends AbstractDiagramTypeProvider {
	
	public static final String ID = "de.upb.pose.specification";
	
	private final IToolBehaviorProvider[] toolBehaviorProviders;

	public SpecificationDiagramTypeProvider() {
		setFeatureProvider(new SpecificationEditorFeatureProvider(this));

		toolBehaviorProviders = new IToolBehaviorProvider[] { new SpecificationToolBehaviorProvider(this) };
	}

	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		return toolBehaviorProviders;
	}
}
