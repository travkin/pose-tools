package de.upb.pose.specification.ui.editor.features.add;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.impl.UpdateContext;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.ui.editor.features.PictogrammElementFinder;
import de.upb.pose.specification.ui.editor.helpers.ParameterUtil;

public class ParameterAddFeature extends AbstractShapeWithLabelAddFeature {
	
	public ParameterAddFeature(IFeatureProvider fp) {
		super(fp);
	}
	
	@Override
	protected boolean canAdd(EObject element) {
		return element instanceof Parameter;
	}
	
	@Override
	protected Parameter getBusinessObject(IAddContext context) {
		return (Parameter) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		return ParameterUtil.getText(getBusinessObject(context));
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return ParameterUtil.getFont(getBusinessObject(context));
	}

	@Override
	protected boolean updateSetFragments() {
		return false;
	}
	
	@Override
	public PictogramElement add(IAddContext context) {
		PictogramElement addedPE = super.add(context);
		
		// also update the predecessor parameter
		Parameter param = this.getBusinessObject(context);
		if (param.getOperation().getParameters().size() > 1) {
			int index = param.getOperation().getParameters().indexOf(param);
			if (!param.getOperation().getParameters().get(0).equals(param)) {
				Parameter predecessor = param.getOperation().getParameters().get(index - 1);
				List<PictogramElement> peElements = PictogrammElementFinder.get()
						.findAllPictogrammElementsInDiagramForBusinessObject(getDiagram(), predecessor);
				for (PictogramElement pe: peElements) {
					getFeatureProvider().updateIfPossible(new UpdateContext(pe));
				}
			}
		}
		
		return addedPE;
	}

}
