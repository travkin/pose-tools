/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.update;

import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.GS;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.ui.editor.provider.SpecificationEditorFeatureProvider;

/**
 * @author Dietrich Travkin
 */
public class SubsystemUpdateFeature extends AbstractShapeWithLabelUpdateFeature {

	public SubsystemUpdateFeature(SpecificationEditorFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#canUpdate(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean canUpdate(IUpdateContext context) {
		return getBusinessObjectForPictogramElement(context.getPictogramElement()) instanceof Subsystem;
	}
	
	/**
	 * @see org.eclipse.graphiti.func.IUpdate#updateNeeded(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public IReason updateNeeded(IUpdateContext context) {
		if (canUpdate(context)) {
			ContainerShape pe = (ContainerShape) context.getPictogramElement();
			Subsystem bo = (Subsystem) getBusinessObjectForPictogramElement(pe);
			
			// text value
			String boText = bo.getName();
			String peText = getLabelText(context);
			if (GS.differ(boText, peText)) {
				return Reason.createTrueReason(UpdateFeatureConstants.UPDATE_REASON_TEXT);
			}
		}
		return Reason.createFalseReason();
	}

	/**
	 * @see org.eclipse.graphiti.func.IUpdate#update(org.eclipse.graphiti.features.context.IUpdateContext)
	 */
	@Override
	public boolean update(IUpdateContext context) {
		ContainerShape pe = (ContainerShape) context.getPictogramElement();
		Subsystem bo = (Subsystem) getBusinessObjectForPictogramElement(pe);
		
		// text value
		String boText = bo.getName();
		setLabelText(context, boText);

		layoutPictogramElement(pe);
				
		return true;
	}

}
