/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.update;

/**
 * @author Dietrich Travkin
 */
public interface UpdateFeatureConstants
{
	public static final String UPDATE_REASON_TEXT = "The text is out of date.";
	public static final String UPDATE_REASON_FONT = "The font is out of date.";
	public static final String UPDATE_REASON_BG_COLOR = "The background color is out of date.";
	public static final String UPDATE_REASON_LINE_STYLE = "The line style is out of date.";
	public static final String UPDATE_REASON_DECORATOR = "The decorators are out of date.";
	public static final String UPDATE_REASON_LAYOUT = "The layout is out of date.";
}
