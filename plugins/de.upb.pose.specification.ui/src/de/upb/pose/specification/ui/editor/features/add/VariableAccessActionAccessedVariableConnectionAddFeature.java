/**
 * 
 */
package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.util.IColorConstant;

import de.upb.pose.core.features.AddConnectionFeature;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.ui.editor.graphics.GraphicsAlgorithmsFactory;
import de.upb.pose.specification.ui.editor.graphics.GraphicsFactory;
import de.upb.pose.specification.ui.editor.graphics.PictogramElementsFactory;

/**
 * @author Dietrich Travkin
 */
public class VariableAccessActionAccessedVariableConnectionAddFeature extends AddConnectionFeature {

	public VariableAccessActionAccessedVariableConnectionAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	/**
	 * @see de.upb.pose.core.features.AddConnectionFeature#add(org.eclipse.graphiti.features.context.IAddConnectionContext)
	 */
	@Override
	protected Connection add(IAddConnectionContext context) {
		Connection connection = PictogramElementsFactory.addConnection(
				getDiagram(), context.getSourceAnchor(), context.getTargetAnchor());
		
		Action sourceBo = (Action) getBusinessObjectForPictogramElement(context.getSourceAnchor().getParent());
		link(connection, sourceBo);
		
		Color colorBlack = manageColor(IColorConstant.BLACK);

		GraphicsAlgorithmsFactory.addLine(connection, colorBlack, LineStyle.DASH);
		GraphicsFactory.addConnectionDecorator_FilledArrow_TargetEnd(connection, colorBlack, colorBlack);
		
		return connection;
	}

	/**
	 * @see de.upb.pose.core.features.AddFeature#canAdd(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	protected boolean canAdd(EObject element) {
		return ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE.equals(element);
	}

}
