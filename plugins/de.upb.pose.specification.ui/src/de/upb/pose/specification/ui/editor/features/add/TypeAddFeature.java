package de.upb.pose.specification.ui.editor.features.add;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.util.FontDescription;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.ui.editor.graphics.FontConstants;

public class TypeAddFeature extends AbstractShapeWithLabelAddFeature {
	
	public TypeAddFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	protected boolean canAdd(EObject bo) {
		return bo instanceof Type;
	}

	@Override
	protected Type getBusinessObject(IAddContext context) {
		return (Type) context.getNewObject();
	}

	@Override
	protected String getLabelTextForBusinessObject(IAddContext context) {
		return getBusinessObject(context).getName();
	}

	@Override
	protected FontDescription getLabelFontDescriptionForBusinessObject(IAddContext context) {
		return FontConstants.FONT_12_BOLD_REGULAR;
	}

	@Override
	protected boolean updateSetFragments() {
		return true;
	}
	
	@Override
	protected void initializeSizeAndLocation(IAddContext context, ContainerShape parentShapePE, GraphicsAlgorithm labelParent) {
		labelParent.setX(context.getX());
		labelParent.setY(context.getY());
		if (context.getWidth() > 0) {
			labelParent.setWidth(context.getWidth());
		}
		if (context.getHeight() > 0) {
			labelParent.setHeight(context.getHeight());
		}
	}
	
}