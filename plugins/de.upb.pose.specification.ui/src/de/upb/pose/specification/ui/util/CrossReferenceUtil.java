package de.upb.pose.specification.ui.util;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;

public class CrossReferenceUtil {
	
	public static Collection<EObject> getReferences(Type element) {
		Collection<EObject> result = new ArrayList<EObject>();

		collect(element, TypesPackage.Literals.OPERATION__TYPE, result);
		collect(element, TypesPackage.Literals.PARAMETER__TYPE, result);

		return result;
	}

	private static void collect(Type element, EReference reference, Collection<EObject> result) {
		for (Setting setting : EcoreUtil.UsageCrossReferencer.find(element, element.eResource())) {
			if (reference.equals(setting.getEStructuralFeature())) {
				result.add(setting.getEObject());
			}
		}
	}
}
