package de.upb.pose.specification.ui.editor.helpers;

import java.util.ArrayList;
import java.util.Collection;

import de.upb.pose.core.Named;

/**
 * @author Dietrich Travkin
 */
public class NameGenerator {

	private NameGenerator() { }
	
	private static String generateUniqueName(String namePrefix, Collection<String> existingNames) {
		String name = namePrefix;
		int index = 2;
		while (existingNames.contains(name)) {
			name = namePrefix + index;
			index++;
		}

		return name;
	}
	
	public static String generateUniqueName(Collection<? extends Named> existingElements, String namePrefix) {
		Collection<String> existingNames = new ArrayList<String>(existingElements.size());
		for (Named namedElement: existingElements) {
			existingNames.add(namedElement.getName());
		}
		return generateUniqueName(namePrefix, existingNames);
	}
		
}
