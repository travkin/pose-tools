package de.upb.pose.specification.ui.editor.features.layout;

import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getText;
import static de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil.getTextContainer;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import de.upb.pose.core.util.Size;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.ui.editor.helpers.PatternEnvironmentUtil;

public class PatternEnvironmentLayoutFeature extends AbstractLayoutFeature {
	public PatternEnvironmentLayoutFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canLayout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);

		return bo instanceof PatternEnvironment;
	}

	@Override
	public boolean layout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		GraphicsAlgorithm ga = pe.getGraphicsAlgorithm();

		int w = ga.getWidth();
		w = w < 100 ? 100 : w;

		int h = ga.getHeight();
		h = h < 50 ? 50 : h;

		Size size = new Size(w, h);

		// cloud
		for (int i = 0; i < 8; i++) {
			PatternEnvironmentUtil.sizeEllipse(size, (Ellipse) ga.getGraphicsAlgorithmChildren().get(i), i);
		}

		// text container
		int x = w / 8;
		int y = h / 4 + 2;
		w = w - w / 4;
		h = h / 2 - 4;
		Graphiti.getGaService().setLocationAndSize(getTextContainer(pe), x, y, w, h);

		// text
		Graphiti.getGaService().setLocationAndSize(getText(pe), 0, 0, w, h);

		return true;
	}
}
