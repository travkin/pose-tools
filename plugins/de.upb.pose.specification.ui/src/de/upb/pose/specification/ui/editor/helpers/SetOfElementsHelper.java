package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

public final class SetOfElementsHelper extends SetElementHelper {
	
	private SetOfElementsHelper() {
		// hide constructor
	}

	public static Polygon getSetPolygon(ContainerShape pe) {
		return (Polygon) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0).getGraphicsAlgorithmChildren()
				.get(0);
	}

	public static Text getSetText(ContainerShape pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0).getGraphicsAlgorithmChildren()
				.get(1);
	}

	public static Text getNameText(ContainerShape pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0).getGraphicsAlgorithmChildren()
				.get(2);
	}
}
