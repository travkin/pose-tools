package de.upb.pose.specification.ui.editor.features.create;

import java.util.Collection;
import java.util.HashSet;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;

import de.upb.pose.core.features.CreateFeature;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationImages;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesFactory;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.ui.editor.helpers.NameGenerator;
import de.upb.pose.specification.util.SpecificationUtil;

public class TypeCreateFeature extends CreateFeature {
	
	public TypeCreateFeature(IFeatureProvider fp) {
		super(fp, "Type");
	}

	@Override
	public String getCreateImageId() {
		return SpecificationImages.getKey(TypesPackage.Literals.TYPE);
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		ContainerShape cpe = context.getTargetContainer();
		Object cbo = getBusinessObjectForPictogramElement(cpe);

		// TODO: get all relevant sets
		return cbo instanceof PatternSpecification || cbo instanceof SetFragment || cbo instanceof Subsystem;
	}

	@Override
	protected EObject doCreate(ICreateContext context) {
		// get container
		ContainerShape targetContainerPE = context.getTargetContainer();
		PatternSpecification specificationBO = SpecificationUtil
				.getSpecification((EObject) getBusinessObjectForPictogramElement(targetContainerPE));

		Type typeBO = TypesFactory.eINSTANCE.createType();
		typeBO.setName(createInitialName(specificationBO));
		typeBO.setSpecification(specificationBO);
		
		SelfVariable selfVariableBO = ActionsFactory.eINSTANCE.createSelfVariable();
		selfVariableBO.setCanBeGenerated(true);
		selfVariableBO.setName("self");
		typeBO.setSelfVariable(selfVariableBO);

		return typeBO;
	}
	
	public String createInitialName(PatternSpecification specification) {
		Collection<Type> existingTypes = new HashSet<Type>();
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof Type) {
				existingTypes.add((Type) element);
			}
		}
		return NameGenerator.generateUniqueName(existingTypes, "Type");
	}
}
