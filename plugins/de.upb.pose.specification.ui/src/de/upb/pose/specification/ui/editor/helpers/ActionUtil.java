package de.upb.pose.specification.ui.editor.helpers;

import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

public class ActionUtil {
	
	public static Text getText(PictogramElement pe) {
		return (Text) pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
	}
	
}
