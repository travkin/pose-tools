/**
 * 
 */
package de.upb.pose.patternapplication;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingFactory;
import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.types.PrimitiveType;

/**
 * @author Dietrich Travkin
 */
public class PatternApplicationCreator
{
	private PatternApplicationCreator()
	{
	}
	
	public static AppliedPattern createPatternApplicationFor(PatternSpecification specification, PatternApplications parent)
	{
		Assert.isNotNull(specification);
		Assert.isNotNull(parent);
		
		// step 1: create a pattern application and link it to the specification
		AppliedPattern patternApplication = createEmptyPatternApplicationFor(specification, parent);
		
		// step 2: create a role binding for each pattern role
		Map<DesignElement, RoleBinding> patternRoleToRoleBindingMap = createRoleBindingsForEachPatternRole(patternApplication);
		
		// step 3: create a set fragment binding for each set fragment
		Map<SetFragment, SetFragmentBinding> setFragmentToSetFragmentBindingMap = createSetFragmentBindingsForEachSetFragment(patternApplication);
		
		// step 4: create a set fragment instance for each set fragment binding
		createSetFragmentInstancesFor(setFragmentToSetFragmentBindingMap.values());
		
		// steps 5 & 6: set containment relations between set fragment instances and role binding according to the specification
		setContainmentRelations(patternApplication, patternRoleToRoleBindingMap, setFragmentToSetFragmentBindingMap);
		
		return patternApplication;
	}
	
	private static AppliedPattern createEmptyPatternApplicationFor(PatternSpecification specification, PatternApplications parent) {
		AppliedPattern patternApplication = MappingFactory.eINSTANCE.createAppliedPattern();
		patternApplication.setPatternSpecification(specification);
		parent.getApplications().add(patternApplication);
		patternApplication.setName(MappingNameCreator.getNameFor(patternApplication));
		
		return patternApplication;
	}
	
	private static Map<DesignElement, RoleBinding> createRoleBindingsForEachPatternRole(AppliedPattern patternApplication) {
		PatternSpecification specification = patternApplication.getPatternSpecification();
		
		if (specification.getPatternElements().isEmpty()) {
			return Collections.emptyMap();
		}
		
		Map<DesignElement, RoleBinding> patternRoleToRoleBindingMap = new HashMap<DesignElement, RoleBinding>();
		TreeIterator<Object> iter = EcoreUtil.getAllContents(specification, true);
		while (iter.hasNext()) {
			Object element = iter.next();
			if (element instanceof DesignElement) {
				DesignElement patternRole = (DesignElement) element;
				RoleBinding newRoleBinding = RoleMapper.createRoleBindingFor(patternRole, patternApplication);
				patternRoleToRoleBindingMap.put(patternRole, newRoleBinding);
			}
		}
		
		for (PrimitiveType primitiveType : specification.getPattern().getCatalog().getPrimitiveTypes()) {
			RoleBinding newRoleBinding = RoleMapper.createRoleBindingFor(primitiveType, patternApplication);
			patternRoleToRoleBindingMap.put(primitiveType, newRoleBinding);
		}
		
		NullVariable nullVariable = specification.getPattern().getCatalog().getNullVariable();
		RoleBinding newRoleBinding = RoleMapper.createRoleBindingFor(nullVariable, patternApplication);
		patternRoleToRoleBindingMap.put(nullVariable, newRoleBinding);
		
		return patternRoleToRoleBindingMap;
	}
	
	private static Map<SetFragment, SetFragmentBinding> createSetFragmentBindingsForEachSetFragment(AppliedPattern patternApplication) {
		PatternSpecification specification = patternApplication.getPatternSpecification();
		
		if (specification.getPatternElements().isEmpty()) {
			return Collections.emptyMap();
		}
		
		Map<SetFragment, SetFragmentBinding> setFragmentToSetFragmentBindingMap = new HashMap<SetFragment, SetFragmentBinding>();
		for (PatternElement element: specification.getPatternElements()) {
			if (element instanceof SetFragment) {
				SetFragment setFragment = (SetFragment) element;
				SetFragmentBinding newSetFragmentBinding = SetFragmentBindingCreator
						.createSetFragmentBindingFor(setFragment, patternApplication);
				setFragmentToSetFragmentBindingMap.put(setFragment, newSetFragmentBinding);
			}
		}
		return setFragmentToSetFragmentBindingMap;
	}
	
	private static List<SetFragmentInstance> createSetFragmentInstancesFor(Collection<SetFragmentBinding> setFragmentBindings) {
		List<SetFragmentInstance> newSetFragmentInstances = new LinkedList<SetFragmentInstance>();
		for (SetFragmentBinding setFragmentBinding: setFragmentBindings) {
			SetFragmentInstance newSetFragmentInstance = SetFragmentBindingCreator.createSetFragmentInstance(setFragmentBinding);
			newSetFragmentInstances.add(newSetFragmentInstance);
		}
		return newSetFragmentInstances;
	}
	
	private static void setContainmentRelations(AppliedPattern patternApplication,
			Map<DesignElement, RoleBinding> patternRoleToRoleBindingMap,
			Map<SetFragment, SetFragmentBinding> setFragmentToSetFragmentBindingMap) {
		PatternSpecification specification = patternApplication.getPatternSpecification();
		
		for (PatternElement patternElement: specification.getPatternElements()) {
			if (patternElement instanceof SetFragment) {
				SetFragment setFragment = (SetFragment) patternElement;
				SetFragmentInstance setFragmentInstance = getSetFragmentInstanceFor(setFragment, setFragmentToSetFragmentBindingMap);
				
				for (SetFragmentElement containedElement: setFragment.getContainedElements()) {
					if (containedElement instanceof SetFragment) {
						SetFragment containedSetFragment = (SetFragment) containedElement;
						SetFragmentInstance containedSetFragmentInstance = getSetFragmentInstanceFor(
								containedSetFragment, setFragmentToSetFragmentBindingMap);
						setFragmentInstance.getContainedSetFragmentInstances().add(containedSetFragmentInstance);
					}
					else if (containedElement instanceof DesignElement) {
						DesignElement containedPatternRole = (DesignElement) containedElement;
						RoleBinding containedRoleBinding = getRoleBindingFor(containedPatternRole, patternRoleToRoleBindingMap);
						setFragmentInstance.getContainedRoleBindings().add(containedRoleBinding);
					}
				}
			}
		}
	}
	
	private static SetFragmentInstance getSetFragmentInstanceFor(SetFragment setFragment,
			Map<SetFragment, SetFragmentBinding> setFragmentToSetFragmentBindingMap) {
		SetFragmentBinding setFragmentBinding = setFragmentToSetFragmentBindingMap.get(setFragment);
		
		Assert.isNotNull(setFragmentBinding);
		Assert.isTrue(setFragmentBinding.getSetFragmentInstances().size() == 1);
		
		SetFragmentInstance setFragmentInstance = setFragmentBinding.getSetFragmentInstances().get(0);
		
		return setFragmentInstance;
	}
	
	private static RoleBinding getRoleBindingFor(DesignElement patternRole,
			Map<DesignElement, RoleBinding> patternRoleToRoleBindingMap) {
		RoleBinding roleBinding = patternRoleToRoleBindingMap.get(patternRole);
		
		Assert.isNotNull(roleBinding);
		
		return roleBinding;
	}
	
}
