/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import de.upb.pose.mapping.MappingFactory;
import de.upb.pose.mapping.Token;

/**
 * @author Dietrich Travkin
 */
public enum TokenType {

	OPERATION_BEHAVIOR("operation behavior"),
	CONTROL_FLOW("control flow"),
	CALL("call"),
	TARGET_OBJECT("target object"),
	ARGUMENT("argument"),
	VARIABLE("variable"),
	BIND_REFERENCE("bind reference"),
	CALL_RESULT("call result"),
	ITERATION("iteration"),
	CREATION("creation"),
	RETURN("return value");
	
	private final String name;
	
	private TokenType(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	public boolean compliesWith(Token token) {
		return (token != null
				&& token.getName() != null
				&& this.name.equals(token.getName()));
	}
	
	public static boolean complyWith(Token token, TokenType tokenType) {
		return tokenType.compliesWith(token);
	}
	
	public static Token createToken(TokenType tokenType) {
		Token newToken = MappingFactory.eINSTANCE.createToken();
		newToken.setName(tokenType.getName());
		return newToken;
	}
	
	public static Token findSubTokenOfType(Token parentToken, TokenType tokenType) {
		for (Token subToken: parentToken.getSubTokens()) {
			if (complyWith(subToken, tokenType)) {
				return subToken;
			}
		}
		return null;
	}
	
}
