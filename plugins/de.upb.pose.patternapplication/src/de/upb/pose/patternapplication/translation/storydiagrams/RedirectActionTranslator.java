/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.RedirectAction;

/**
 * @author Dietrich Travkin
 */
public class RedirectActionTranslator extends AbstractRedirectActionTranslator<RedirectAction> {
	
	public RedirectActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
}
