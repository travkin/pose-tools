/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class TypeInheritanceTranslator extends DesignElementTranslator<Type, EObject> {

	public TypeInheritanceTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Type typeToTranslate, RoleBinding roleBinding) {
		return (this.isApplicationModelElementMappedToTargetElements(typeToTranslate)
				&& typeToTranslate.getSuperType() != null
				&& this.isApplicationModelElementMappedToTargetElements(typeToTranslate.getSuperType()));
	}

	@Override
	public List<EObject> translate(Type typeToTranslate, RoleBinding roleBinding) {
		Type superType = typeToTranslate.getSuperType();
		
		EClass typeEClass = (EClass) roleBinding.getModelElements().get(0);
		EClass superTypeEClass = (EClass) this.getAllTargetElementsFor(superType).get(0);
		
		if (!superTypeEClass.isSuperTypeOf(typeEClass)) {
			if (typeEClass.equals(superTypeEClass)
					|| typeEClass.isSuperTypeOf(superTypeEClass)) {
				// TODO warn user
			} else {
				typeEClass.getESuperTypes().add(superTypeEClass);
			}
		}
		
		return Collections.emptyList();
	}

}
