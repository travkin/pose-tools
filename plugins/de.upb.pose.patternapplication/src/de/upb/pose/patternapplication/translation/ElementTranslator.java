/**
 * 
 */
package de.upb.pose.patternapplication.translation;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.actions.Action;

/**
 * @author Dietrich Travkin
 * 
 * @param <S> the source type, i.e. the type of the design element (in the application model) to be translated
 * @param <T> the target type, i.e. the type of the model element to be created as representation for a design element
 *            of type S
 */
public abstract class ElementTranslator<S extends EObject, T extends EObject> {
	
	private ApplicationModel2EcoreAndSDsTranslator translationService;

	Class<S> elementToTranslateType;
	Class<T> translationResultType;

	public ElementTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		translationService = parent;
	}
	
	protected Type[] getActualTypeArguments() {
		Type genericSuperClass = this.getClass().getGenericSuperclass();
		Type[] typeArguments = ((ParameterizedType) genericSuperClass).getActualTypeArguments();
		return typeArguments;
	}

	@SuppressWarnings("unchecked")
	protected Class<S> getElementToTranslateType() {
		if (this.elementToTranslateType == null) {
			Type[] typeArguments = this.getActualTypeArguments();
			this.elementToTranslateType = (Class<S>) typeArguments[0];
		}
		return this.elementToTranslateType;
	}

	@SuppressWarnings("unchecked")
	protected Class<T> getTranslationResultType() {
		if (this.translationResultType == null) {
			Type[] typeArguments = this.getActualTypeArguments();
			this.translationResultType = (Class<T>) typeArguments[1];
		}
		return this.translationResultType;
	}
	
	public abstract boolean canTranslate(EObject elementToTranslate);
	
	public final List<EObject> translate(EObject elementToTranslate) {
		if (!canTranslate(elementToTranslate)) {
			throw new IllegalStateException("Translate operation is called, although the translation precondition is not satisfied!" +
					" Translator: " + this.getClass().getName());
		}
		
		List<EObject> translationResults = doTranslate(elementToTranslate);
		
		// check type of the first element in the translation result list
		if (!translationResults.isEmpty()) {
			Class<? extends EObject> resultType = translationResults.get(0).getClass();
			if (!this.getTranslationResultType().isAssignableFrom(resultType))
			{
				throw new IllegalStateException("The first element in the translation result has an unexpected type: "
						+ translationResults.get(0).getClass().getName());
			}
		}
		
		return translationResults;
	}
	
	protected abstract List<EObject> doTranslate(EObject elementToTranslate);
	
	protected ApplicationModel2EcoreAndSDsTranslator getTranslationService() {
		return this.translationService;
	}
	
	protected <E extends DesignElement> DesignElementTranslator<E, ? extends EObject> getTranslatorFor(Class<E> type) {
		return this.translationService.getTranslatorFor(type);
	}
	
	protected DesignElementTranslator<? extends Action, ? extends EObject> getTranslatorFor(Action action) {
		return this.translationService.getTranslatorFor(action);
	}
	
	protected TokenTranslator<? extends EObject> getTranslatorFor(TokenType tokenType) {
		return this.translationService.getTranslatorFor(tokenType);
	}

	protected <E extends EObject> void addTranslationStep(E elementToTranslate,
			ElementTranslator<E, ? extends EObject> translatorToUse) {
		this.translationService.addTranslationStep(elementToTranslate, translatorToUse);
	}
	
	protected <E extends Action> void addTranslationStep(Action actionToTranslate) {
		this.translationService.addTranslationStep(actionToTranslate);
	}
	
	protected final List<EObject> wrapInList(EObject element) {
		Assert.isNotNull(element);

		ArrayList<EObject> list = new ArrayList<EObject>(1);
		list.add(element);
		return list;
	}
	
	protected RoleBinding getRoleBinding(DesignElement applicationModelElement) {
		return this.getTranslationService().getRoleBinding(applicationModelElement);
	}
	
	protected RoleBinding getRoleBinding(Token token) {
		return this.getTranslationService().getRoleBinding(token);
	}

	protected final List<? extends EObject> getAllTargetElementsFor(DesignElement applicationModelElement) {
		Assert.isNotNull(applicationModelElement);
		
		RoleBinding roleBinding = getRoleBinding(applicationModelElement);
		
		if (roleBinding == null) {
			return Collections.emptyList();
		}
		return roleBinding.getModelElements();
	}
	
	protected final boolean isApplicationModelElementMappedToTargetElements(DesignElement applicationModelElement) {
		return !getAllTargetElementsFor(applicationModelElement).isEmpty();
	}

	protected final boolean isTokenMappedToTargetElements(Token token) {
		return !token.getMapsTo().isEmpty();
	}

}
