/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.StatementNode;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.DeleteAction;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.actions.WriteAction;

/**
 * @author Dietrich Travkin
 */
public class VariableAccessActionTranslator extends AbstractActionTranslator<VariableAccessAction, StatementNode> {
	
	public VariableAccessActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected Class<StatementNode> getTranslationResultType() {
		return StatementNode.class;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.storydiagrams.AbstractActionTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.actions.Action, de.upb.pose.mapping.RoleBinding)
	 */
	protected boolean isTranslationPreconditionSatisfied(VariableAccessAction action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		return preconditionSatisfied; 
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(VariableAccessAction action, RoleBinding roleBinding) {
		Activity parentActivity = this.getParentActivity(action);
		
		StatementNode mappedStatementNode = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			List<EObject> createdElements = new LinkedList<EObject>();
			
			mappedStatementNode = this.createNode(parentActivity, createdElements);
			ActivityEdge outgoingEdge = this.createOutgoingEdge(mappedStatementNode, parentActivity, createdElements);
			
			Token rootControlFlowToken = this.createControlFlowToken(roleBinding, createdElements);
			
			appendNewNodeToControlFlow(rootControlFlowToken, mappedStatementNode);
			setLastEdgeInControlflow(rootControlFlowToken, outgoingEdge);
			
			String nodeName = "ToDo: ";
			if (action instanceof ReadAction) {
				nodeName += "read variable";
			} else if (action instanceof WriteAction) {
				nodeName += "write variable";
			} else if (action instanceof ReturnAction) {
				nodeName += "read and return variable";
			} else if (action instanceof DeleteAction) {
				nodeName += "unset variable";
			}
			
			if (action.getAccessedVariable() != null
					&& action.getAccessedVariable().getName() != null
					&& !action.getAccessedVariable().getName().isEmpty()) {
				nodeName += " \"" + action.getAccessedVariable().getName() + "\"";
			}
			
			mappedStatementNode.setName(nodeName);
			
			return createdElements;
		} else {
			mappedStatementNode = (StatementNode) roleBinding.getModelElements().get(0);
			
			// TODO correspondence check
		}
		
		return wrapInList(mappedStatementNode);
	}
	
	private StatementNode createNode(Activity parentActivity, List<EObject> createdElements) {
		StatementNode newStatementNode = ActivitiesFactory.eINSTANCE.createStatementNode();
		parentActivity.getOwnedActivityNodes().add(newStatementNode);
		createdElements.add(newStatementNode);
		return newStatementNode;
	}
	
}
