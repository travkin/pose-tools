/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.OperationExtension;
import org.storydriven.storydiagrams.calls.CallsFactory;
import org.storydriven.storydiagrams.calls.InvocationResultVariable;
import org.storydriven.storydiagrams.calls.OutParameterBinding;
import org.storydriven.storydiagrams.calls.ParameterExtension;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class CallResultVariableTranslator extends DesignElementTranslator<ResultVariable, OutParameterBinding> {

	public CallResultVariableTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(ResultVariable resultVariable, RoleBinding roleBinding) {
		ResultAction resultAction = resultVariable.getAction();
		
		boolean preconditionSatisfied = true;
		
		Assert.isTrue(resultAction instanceof CallAction);
		
		CallAction callAction = (CallAction) resultAction;
			
		Operation calledOperation = callAction.getCalledOperation();
		Type returnType = calledOperation.getType();
			
		preconditionSatisfied = this.isApplicationModelElementMappedToTargetElements(calledOperation)
					&& (returnType == null || this.isApplicationModelElementMappedToTargetElements(returnType));
		
		return preconditionSatisfied;  
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(ResultVariable resultVariable, RoleBinding roleBinding) {
		CallAction callAction = (CallAction) resultVariable.getAction();
		
		Token callResultToken = TokenType.createToken(TokenType.CALL_RESULT);
		roleBinding.setRootToken(callResultToken);
		
		OutParameterBinding parameterBinding = CallsFactory.eINSTANCE.createOutParameterBinding();
		
		Operation parentOperation = callAction.getOperation();
		RoleBinding operationRoleBinding = this.getRoleBinding(parentOperation);
		Token rootToken = operationRoleBinding.getRootToken();
		
		Assert.isNotNull(rootToken);
		Assert.isTrue(TokenType.complyWith(rootToken, TokenType.OPERATION_BEHAVIOR));
		
		Activity parentActivity = (Activity) rootToken.getMapsTo().get(TokenMapKey.OPERATION_BEHAVIOR__STORY_DIAGRAM.getKey());
		
		Assert.isNotNull(parentActivity);
			
		Operation calledOperation = callAction.getCalledOperation();
		EOperation eOperation = (EOperation) this.getAllTargetElementsFor(calledOperation).get(0);
			
		OperationExtension operationExtension = StoryDiagramsEcoreConnector.provideOperationExtension(eOperation, parentActivity);
		EParameter returnParameter = operationExtension.getOutParameters().get(0);
		
		ParameterExtension parameterExtension = StoryDiagramsEcoreConnector.provideParameterExtension(returnParameter, parentActivity);
			
		parameterBinding.setParameter(returnParameter);
			
		callResultToken.getMapsTo().put(TokenMapKey.CALL_RESULT__VARIABLE.getKey(), parameterExtension);
			
		InvocationResultVariable callResultVariable = CallsFactory.eINSTANCE.createInvocationResultVariable();
		callResultVariable.setName(resultVariable.getName());
		callResultVariable.setType(eOperation.getEType());
		//callResultVariable.setOwningInvocation(parameterBinding.getInvocation()); // this is null at this point
		
		parameterBinding.setResultVariable(callResultVariable);
			
		StoryDiagramsEcoreConnector.getParameterExtension(returnParameter, parentActivity);
		
		return wrapInList(parameterBinding);
		
	}

}
