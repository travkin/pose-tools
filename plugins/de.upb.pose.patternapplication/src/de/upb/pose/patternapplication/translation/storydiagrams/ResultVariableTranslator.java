/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;

/**
 * @author Dietrich Travkin
 */
public class ResultVariableTranslator extends DesignElementTranslator<ResultVariable, EObject> {

	private final CallResultVariableTranslator callResultVariableTranslator;
	private final CreationResultVariableTranslator creationResultVariableTranslator;
	
	public ResultVariableTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
		
		this.callResultVariableTranslator = new CallResultVariableTranslator(parent);
		this.creationResultVariableTranslator = new CreationResultVariableTranslator(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(ResultVariable resultVariable, RoleBinding roleBinding) {
		ResultAction resultAction = resultVariable.getAction();
		
		boolean preconditionSatisfied = true; 
		if (resultAction instanceof CallAction) {
			preconditionSatisfied = this.callResultVariableTranslator.isTranslationPreconditionSatisfied(resultVariable, roleBinding);
		} else if (resultAction instanceof CreateAction) {
			preconditionSatisfied = this.creationResultVariableTranslator.isTranslationPreconditionSatisfied(resultVariable, roleBinding);
		}
		
		return preconditionSatisfied;  
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(ResultVariable resultVariable, RoleBinding roleBinding) {
		ResultAction resultAction = resultVariable.getAction();
		
		if (resultAction instanceof CallAction) {
			return this.callResultVariableTranslator.translate(resultVariable, roleBinding);
		} else if (resultAction instanceof CreateAction) {
			return this.creationResultVariableTranslator.translate(resultVariable, roleBinding);
		}
		
		throw new IllegalStateException("No fitting result variable translator found");
	}

}
