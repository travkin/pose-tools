/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.core.expressions.Expression;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityFinalNode;
import org.storydriven.storydiagrams.calls.InvocationResultVariable;
import org.storydriven.storydiagrams.calls.OutParameterBinding;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.InvocationResultVariableExpression;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.expressions.ObjectVariableExpression;
import org.storydriven.storydiagrams.patterns.expressions.PatternsExpressionsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class ReturnActionTranslator extends AbstractActionTranslator<ReturnAction, ActivityFinalNode> {
	
	public ReturnActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected Class<ActivityFinalNode> getTranslationResultType() {
		return ActivityFinalNode.class;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.storydiagrams.AbstractActionTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.actions.Action, de.upb.pose.mapping.RoleBinding)
	 */
	protected boolean isTranslationPreconditionSatisfied(ReturnAction action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		Variable returnedValueVariable = action.getAccessedVariable();
		
		Assert.isNotNull(returnedValueVariable);
		
		if (action.isCanBeGenerated()) {
			if (returnedValueVariable instanceof Reference
					|| returnedValueVariable instanceof Attribute
					|| returnedValueVariable instanceof Parameter
					|| returnedValueVariable instanceof ResultVariable) {
				preconditionSatisfied = preconditionSatisfied
						&& this.isApplicationModelElementMappedToTargetElements(returnedValueVariable);
			}
			
			if (returnedValueVariable instanceof Reference) {
				Assert.isTrue(((Reference) returnedValueVariable).getCardinality().equals(CardinalityType.SINGLE));
			}
		}
		
		return preconditionSatisfied; 
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(ReturnAction action, RoleBinding roleBinding) {
		Activity parentActivity = this.getParentActivity(action);
		Variable returnedValueVariable = action.getAccessedVariable();
		EObject mappedVariableEObject = returnedValueVariable != null ? this.getAllTargetElementsFor(returnedValueVariable).get(0) : null;
		
		ActivityFinalNode mappedActivityFinalNode = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			List<EObject> createdElements = new LinkedList<EObject>();
			
			mappedActivityFinalNode = this.createNode(parentActivity, createdElements);
			//ActivityEdge outgoingEdge = this.createOutgoingEdge(mappedActivityFinalNode, parentActivity, createdElements);
			
			Token rootControlFlowToken = this.createControlFlowToken(roleBinding, createdElements);
			
			appendNewNodeToControlFlow(rootControlFlowToken, mappedActivityFinalNode);
			setLastEdgeInControlflow(rootControlFlowToken, null);
			
			if (action.isCanBeGenerated()) {
				Token returnToken = this.createReturnToken(rootControlFlowToken, createdElements);
				
				returnToken.getMapsTo().put(TokenMapKey.RETURN__NODE.getKey(), mappedActivityFinalNode);
				
				Expression returnValueExpression = null;
				EClassifier returnExpressionType = null;
				if (returnedValueVariable instanceof ResultVariable) {
					if (mappedVariableEObject instanceof OutParameterBinding) {
						OutParameterBinding outParameterBinding = (OutParameterBinding) mappedVariableEObject;
						InvocationResultVariable resultVariable = outParameterBinding.getResultVariable();
						
						InvocationResultVariableExpression resultVariableExpression = CallsExpressionsFactory.eINSTANCE.createInvocationResultVariableExpression();
						resultVariableExpression.setResultVariable(resultVariable);
						
						returnValueExpression = resultVariableExpression;
						returnExpressionType = resultVariable.getType();
					} else if (mappedVariableEObject instanceof ObjectVariable) {
						ObjectVariable objectVariable = (ObjectVariable) mappedVariableEObject;
						
						ObjectVariableExpression objectVariableExpression = PatternsExpressionsFactory.eINSTANCE.createObjectVariableExpression();
						objectVariableExpression.setObject(objectVariable);
						
						returnValueExpression = objectVariableExpression;
						returnExpressionType = objectVariable.getClassifier();
					}
				}
				// TODO add generation for other variable types
				
				Assert.isNotNull(returnValueExpression);
				Assert.isNotNull(returnExpressionType);
				
				mappedActivityFinalNode.getReturnValues().add(returnValueExpression);
				createdElements.add(returnValueExpression);
				
				returnToken.getMapsTo().put(TokenMapKey.RETURN__EXPRESSION.getKey(), returnValueExpression);
				
				// ensure, return type is compatible to the return variable type
				EClassifier returnParameterType = parentActivity.getOutParameters().get(0).getEType();
				if (returnExpressionType instanceof EClass) {
					EClass type = (EClass) returnExpressionType;
					Assert.isTrue(type.getEAllSuperTypes().contains(returnParameterType) || type.equals(returnParameterType));
				} else {
					Assert.isTrue(returnExpressionType.equals(returnParameterType));
				}
			}
			
			return createdElements;
		} else {
			mappedActivityFinalNode = (ActivityFinalNode) roleBinding.getModelElements().get(0);
			
			// TODO correspondence check
		}
		
		return wrapInList(mappedActivityFinalNode);
	}
	
	private ActivityFinalNode createNode(Activity parentActivity, List<EObject> createdElements) {
		ActivityFinalNode newActivityFinalNode = ActivitiesFactory.eINSTANCE.createActivityFinalNode();
		parentActivity.getOwnedActivityNodes().add(newActivityFinalNode);
		createdElements.add(newActivityFinalNode);
		return newActivityFinalNode;
	}
	
	private Token createReturnToken(Token rootControlFlowToken, List<EObject> createdElements) {
		Assert.isTrue(TokenType.complyWith(rootControlFlowToken, TokenType.CONTROL_FLOW));
		
		Token returnToken = TokenType.createToken(TokenType.RETURN);
		rootControlFlowToken.getSubTokens().add(returnToken);
		createdElements.add(returnToken);
		
		return returnToken;
	}
	
}
