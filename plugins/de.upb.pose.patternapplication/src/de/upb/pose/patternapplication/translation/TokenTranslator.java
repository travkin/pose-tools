/**
 * 
 */
package de.upb.pose.patternapplication.translation;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;

/**
 * @author Dietrich Travkin
 */
public abstract class TokenTranslator<T extends EObject> extends ElementTranslator<Token, T> {

	public TokenTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected Class<Token> getElementToTranslateType() {
		if (this.elementToTranslateType == null) {
			this.elementToTranslateType = Token.class;
		}
		return this.elementToTranslateType;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	protected Class<T> getTranslationResultType() {
		if (this.translationResultType == null) {
			Type[] typeArguments = this.getActualTypeArguments();
			this.translationResultType = (Class<T>) typeArguments[0];
		}
		return this.translationResultType;
	}
	
	public final boolean canTranslate(EObject elementToTranslate) {
		if (elementToTranslate instanceof Token) {
			return canTranslate((Token) elementToTranslate);
		}
		return false;
	}
	
	public final List<EObject> doTranslate(EObject elementToTranslate) {
		if (elementToTranslate instanceof Token) {
			return translate((Token) elementToTranslate);
		}
		
		throw new IllegalArgumentException("Expected a " + Token.class.getName()
				+ ", but got a " + elementToTranslate.getClass().getName());
	}
	
	private boolean canTranslate(Token token) {
		if (this.getTranslatableTokenType().getName().equals(token.getName())) {
			RoleBinding roleBinding = this.getRoleBinding(token);
			if (roleBinding != null) {
				return isTranslationPreconditionSatisfied(token, roleBinding);
			}
		}
		
		return false;
	}
	
	private List<EObject> translate(Token token) {
		if (canTranslate(token)) {
			RoleBinding roleBinding = this.getRoleBinding(token);
			return translate(token, roleBinding);
			// List<T> targetElements = translate(token, roleBinding);
			// TODO do this? roleBinding.getModelElements().addAll(targetElements); 
		}
		return Collections.emptyList();
	}
	
	protected abstract TokenType getTranslatableTokenType();
	
	protected abstract boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding);
	
	protected abstract List<EObject> translate(Token token, RoleBinding roleBinding);

}
