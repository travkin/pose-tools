/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.core.expressions.Expression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class VariableAllTypesTranslator extends AbstractVariableTranslator<Expression> {
	
	private Map<Class<? extends Variable>, AbstractVariableTranslator<? extends Expression>> childVariableTranslators
			= new HashMap<Class<? extends Variable>, AbstractVariableTranslator<? extends Expression>>();

	public VariableAllTypesTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
		
		addChildTranslator(Reference.class, new VariableReferenceTranslator(parent));
		addChildTranslator(Attribute.class, new VariableAttributeTranslator(parent));
		addChildTranslator(Parameter.class, new VariableParameterTranslator(parent));
		addChildTranslator(SelfVariable.class, new VariableSelfTranslator(parent));
		addChildTranslator(NullVariable.class, new VariableNullTranslator(parent));
		addChildTranslator(ResultVariable.class, new VariableResultTranslator(parent));
	}
	
	private void addChildTranslator(Class<? extends Variable> variableType, AbstractVariableTranslator<? extends Expression> childTranslator) {
		this.childVariableTranslators.put(variableType, childTranslator);
	}
	
	private AbstractVariableTranslator<? extends Expression> getTranslatorFor(Variable variable)
	{
		Class<? extends Variable> variableClass = null;
		
		if (variable instanceof Reference) {
			variableClass = Reference.class;
		}
		else if (variable instanceof Attribute) {
			variableClass = Attribute.class;
		}
		else if (variable instanceof Parameter) {
			variableClass = Parameter.class;
		}
		else if (variable instanceof SelfVariable) {
			variableClass = SelfVariable.class;
		}
		else if (variable instanceof NullVariable) {
			variableClass = NullVariable.class;
		}
		else if (variable instanceof ResultVariable) {
			variableClass = ResultVariable.class;
		}
		
		return childVariableTranslators.get(variableClass);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable variable = getVariable(token);
		Assert.isNotNull(getTranslatorFor(variable));
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Variable variable = getVariable(token);
		AbstractVariableTranslator<? extends Expression> childTranslator = getTranslatorFor(variable);
		return childTranslator.translate(token);
	}

}
