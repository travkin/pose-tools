/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityFinalNode;
import org.storydriven.storydiagrams.activities.OperationExtension;
import org.storydriven.storydiagrams.activities.StatementNode;
import org.storydriven.storydiagrams.calls.CallsFactory;
import org.storydriven.storydiagrams.calls.InvocationResultVariable;
import org.storydriven.storydiagrams.calls.OutParameterBinding;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.InvocationResultVariableExpression;
import org.storydriven.storydiagrams.calls.expressions.MethodCallExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.DelegateAction;

/**
 * @author Dietrich Travkin
 */
public class DelegateActionTranslator extends AbstractRedirectActionTranslator<DelegateAction> {
	
	public DelegateActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	protected boolean isTranslationPreconditionSatisfied(DelegateAction action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		
		// no return variables allowed for delegate actions
		Assert.isTrue(action.getResultVariable() == null);
		
		// delegate action has to be the last action
		int index = action.getOperation().getActions().indexOf(action);
		Assert.isTrue(index == action.getOperation().getActions().size() - 1);
		
		EOperation callerEOperation = (EOperation) this.getAllTargetElementsFor(action.getOperation()).get(0);
		EOperation calleeEOperation = (EOperation) this.getAllTargetElementsFor(action.getCalledOperation()).get(0);
		
		// caller and callee return types are compliant to each other, cancel otherwise
		boolean compliantReturnTypes = callerEOperation.getEType() == null
				&& calleeEOperation.getEType() == null;
		compliantReturnTypes = compliantReturnTypes
				|| (callerEOperation.getEType() != null && callerEOperation.getEType().equals(calleeEOperation.getEType()));		
		
		if (!compliantReturnTypes
				&& callerEOperation.getEType() instanceof EClass
				&& calleeEOperation.getEType() instanceof EClass) {
			EClass callerReturnType = (EClass) callerEOperation.getEType();
			EClass calleeReturnType = (EClass) calleeEOperation.getEType();
			compliantReturnTypes = callerReturnType.isSuperTypeOf(calleeReturnType);
		}
		
		Assert.isTrue(compliantReturnTypes);
		
		// TODO this assertion seems to be too strict. Check if it can be removed.
//		// target object reference has never the cardinality *
//		if (action.getTarget() instanceof Reference) {
//			Reference targetReference = (Reference) action.getTarget();
//			
//			Assert.isTrue(targetReference.getCardinality().getValue() != CardinalityType.MULTIPLE_VALUE);
//		}
		
		return preconditionSatisfied; 
	}
	
	@Override
	protected List<EObject> translate(DelegateAction action, RoleBinding roleBinding) {
		List<EObject> createdElements = super.translate(action, roleBinding);
		Activity parentActivity = this.getParentActivity(action);
		
		StatementNode mappedStatementNode = (StatementNode) createdElements.get(0);
		MethodCallExpression methodCallExpression = (MethodCallExpression) mappedStatementNode.getStatementExpression();
		
		EOperation calleeEOperation = (EOperation) this.getAllTargetElementsFor(action.getCalledOperation()).get(0);
		
		if (calleeEOperation.getEType() != null) {
			// add call result variable to call statement node
			
			OutParameterBinding parameterBinding = CallsFactory.eINSTANCE.createOutParameterBinding();
			methodCallExpression.getOwnedParameterBindings().add(parameterBinding);
			
			OperationExtension operationExtension = StoryDiagramsEcoreConnector.provideOperationExtension(calleeEOperation, parentActivity);
			EParameter returnParameter = operationExtension.getOutParameters().get(0);
			
//			ParameterExtension parameterExtension = StoryDiagramsEcoreConnector.provideParameterExtension(returnParameter, parentActivity);
			
			InvocationResultVariable callResultVariable = CallsFactory.eINSTANCE.createInvocationResultVariable();
			callResultVariable.setName("callResult");
			callResultVariable.setType(calleeEOperation.getEType());
			callResultVariable.setOwningInvocation(methodCallExpression);
			
			parameterBinding.setParameter(returnParameter);
			parameterBinding.setResultVariable(callResultVariable);
			
			createdElements.add(parameterBinding);
			createdElements.add(callResultVariable);
			
			mappedStatementNode.getDeclaredVariables().add(callResultVariable);
			
			
			// add final node with return expression
			
			Token rootControlflowToken = roleBinding.getRootToken();
			
			Assert.isTrue(TokenType.complyWith(rootControlflowToken, TokenType.CONTROL_FLOW));
			Assert.isNotNull(rootControlflowToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__LAST_EDGE.getKey()));
			
			ActivityFinalNode finalNode = ActivitiesFactory.eINSTANCE.createActivityFinalNode();
			finalNode.setOwningActivity(parentActivity);
			createdElements.add(finalNode);
			
			appendNewNodeToControlFlow(rootControlflowToken, finalNode);
			
			InvocationResultVariableExpression callResultExpression = CallsExpressionsFactory.eINSTANCE.createInvocationResultVariableExpression();
			callResultExpression.setResultVariable(callResultVariable);
			finalNode.getReturnValues().add(callResultExpression);
			createdElements.add(callResultExpression);
		}
		
		return createdElements;
	}

}
