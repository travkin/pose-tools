/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.patterns.ObjectVariable;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;

/**
 * @author Dietrich Travkin
 */
public class CreationResultVariableTranslator extends DesignElementTranslator<ResultVariable, ObjectVariable> {

	public CreationResultVariableTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(ResultVariable resultVariable, RoleBinding roleBinding) {
		ResultAction resultAction = resultVariable.getAction();
		
		boolean preconditionSatisfied = true;
		
		Assert.isTrue(resultAction instanceof CreateAction);
		
		CreateAction createAction = (CreateAction) resultAction;
		preconditionSatisfied = (this.findCreationVariable(createAction) != null);
		
		return preconditionSatisfied;  
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(ResultVariable resultVariable, RoleBinding roleBinding) {
		// only remember the mapping to object variable, do not create anything in case of create action
		CreateAction createAction = (CreateAction) resultVariable.getAction();
		ObjectVariable objectVariableForCreationResult = this.findCreationVariable(createAction);
		
		return wrapInList(objectVariableForCreationResult); // parent class maps the returned variable in the role binding
	}
	
	private ObjectVariable findCreationVariable(CreateAction action) {
		RoleBinding actionRoleBinding = this.getRoleBinding(action);
		if (actionRoleBinding != null) {
			Token rootToken = actionRoleBinding.getRootToken();
			Token creationToken = findCreationToken(rootToken);
			if (creationToken != null) {
				ObjectVariable objectVariable = (ObjectVariable) creationToken.getMapsTo().get(TokenMapKey.CREATION__VARIABLE.getKey());
				if (objectVariable != null) {
					return objectVariable;
				}
			}
		}
		return null;
	}
	
	private Token findCreationToken(Token controlFlowToken) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		Token creationToken = TokenType.findSubTokenOfType(controlFlowToken, TokenType.CREATION);
		
		return creationToken;
	}

}
