/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.calls.ParameterExtension;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.ParameterExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public class VariableParameterTranslator extends AbstractVariableTranslator<ParameterExpression> {

	public VariableParameterTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof Parameter);
		
		Parameter param = (Parameter) var;
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding)
				&& this.isApplicationModelElementMappedToTargetElements(param);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Parameter parameter = (Parameter) getVariable(token);
		EParameter eParameter = (EParameter) this.getAllTargetElementsFor(parameter).get(0);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__VARIABLE.getKey(), eParameter);
		
		Activity parentActivity = null;
		Operation parentOperation = getParentOperation(roleBinding);
		
		RoleBinding operationRoleBinding = this.getRoleBinding(parentOperation);
		Token rootToken = operationRoleBinding.getRootToken();
		
		Assert.isNotNull(rootToken);
		Assert.isTrue(TokenType.complyWith(rootToken, TokenType.OPERATION_BEHAVIOR));
		
		parentActivity = (Activity) rootToken.getMapsTo().get(TokenMapKey.OPERATION_BEHAVIOR__STORY_DIAGRAM.getKey());
		
		Assert.isNotNull(parentActivity);
		
		ParameterExtension parameterExtension = StoryDiagramsEcoreConnector.provideParameterExtension(eParameter, parentActivity);
		
		ParameterExpression mappedExpression = CallsExpressionsFactory.eINSTANCE.createParameterExpression();
		mappedExpression.setParameter(parameterExtension);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), mappedExpression);
				
		return wrapInList(mappedExpression);
	}

}
