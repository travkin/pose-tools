/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.patterns.expressions.AttributeValueExpression;
import org.storydriven.storydiagrams.patterns.expressions.PatternsExpressionsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class VariableAttributeTranslator extends AbstractVariableTranslator<AttributeValueExpression> {

	public VariableAttributeTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof Attribute);
		
		Attribute attribute = (Attribute) var;
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding)
				&& this.isApplicationModelElementMappedToTargetElements(attribute);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Attribute attribute = (Attribute) getVariable(token);
		EAttribute eAttribute = (EAttribute) this.getAllTargetElementsFor(attribute).get(0);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__VARIABLE.getKey(), eAttribute);
		
		Operation parentOperation = getParentOperation(roleBinding);
		Type parentType = parentOperation.getParentType();
		
		Assert.isNotNull(parentType);
		
		// TODO also allow the attribute to be in super types
		// has to be visible --> has to be in the same class
		Assert.isTrue(parentType.equals(attribute.getParentType()));

		EClass parentEClass = (EClass) this.getAllTargetElementsFor(parentType).get(0);
		
		Assert.isNotNull(parentEClass);
		
		// TODO adapt story diagram meta-model to describe the target by using an expression
//		ThisVariableExpression mappedExpression = CallsExpressionsFactory.eINSTANCE.createThisVariableExpression();
//		mappedExpression.setType(parentEClass);
		
		AttributeValueExpression mappedExpression = PatternsExpressionsFactory.eINSTANCE.createAttributeValueExpression();
		mappedExpression.setAttribute(eAttribute);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), mappedExpression);
				
		return wrapInList(mappedExpression);
	}

}
