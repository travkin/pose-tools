/**
 * 
 */
package de.upb.pose.patternapplication.translation;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.specification.DesignElement;

/**
 * @author Dietrich Travkin
 */
public abstract class DesignElementTranslator<S extends DesignElement, T extends EObject> extends ElementTranslator<S, T> {

	public DesignElementTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	public final boolean canTranslate(EObject elementToTranslate) {
		if (elementToTranslate instanceof DesignElement) {
			return canTranslate((DesignElement) elementToTranslate);
		}
		return false;
	}
	
	public final List<EObject> doTranslate(EObject elementToTranslate) {
		if (elementToTranslate instanceof DesignElement) {
			return translate((DesignElement) elementToTranslate);
		}
		
		throw new IllegalArgumentException("Expected a " + DesignElement.class.getName()
				+ ", but got a " + elementToTranslate.getClass().getName());
	}
	
	@SuppressWarnings("unchecked")
	private boolean canTranslate(DesignElement applicationModelElement) {
		if (this.getElementToTranslateType().isInstance(applicationModelElement)) {
			RoleBinding roleBinding = this.getRoleBinding(applicationModelElement);
			if (roleBinding != null) {
				if (roleBinding.getApplicationModelElement() == applicationModelElement) {
					return isTranslationPreconditionSatisfied((S) applicationModelElement, roleBinding);
				}
			}
		}
		
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private List<EObject> translate(DesignElement applicationModelElement) {
		if (canTranslate(applicationModelElement)) {
			RoleBinding roleBinding = this.getRoleBinding(applicationModelElement);
			List<EObject> targetElements = translate((S) applicationModelElement, roleBinding);
			
			for (EObject obj: targetElements) {
				if (!roleBinding.getModelElements().contains(obj)) {
					roleBinding.getModelElements().add(obj);
				}
			}
			return targetElements;
		}
		return Collections.emptyList();
	}
	
	protected abstract boolean isTranslationPreconditionSatisfied(S applicationModelElement, RoleBinding roleBinding);
	
	protected abstract List<EObject> translate(S applicationModelElement, RoleBinding roleBinding);

}
