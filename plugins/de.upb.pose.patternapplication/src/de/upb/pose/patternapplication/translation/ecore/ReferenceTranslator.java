/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class ReferenceTranslator extends DesignElementTranslator<Reference, EReference>
{
	public ReferenceTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Reference reference, RoleBinding roleBinding) {
		return (reference.getParentType() != null
				&& reference.getType() != null
				&& this.isApplicationModelElementMappedToTargetElements(reference.getParentType())
				&& this.isApplicationModelElementMappedToTargetElements(reference.getType()));
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Reference reference, RoleBinding roleBinding) {
		EClass parentClass = (EClass) this.getAllTargetElementsFor(reference.getParentType()).get(0);
		EClass referencedClass = (EClass) this.getAllTargetElementsFor(reference.getType()).get(0);
		
		EReference mappedReference = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			mappedReference = EcoreFactory.eINSTANCE.createEReference();
			parentClass.getEStructuralFeatures().add(mappedReference);
			mappedReference.setEType(referencedClass);
			
			mappedReference.setName(reference.getName());
			
			switch (reference.getCardinality().getValue()) {
			case CardinalityType.SINGLE_VALUE:
				mappedReference.setUpperBound(1);
				break;
			case CardinalityType.MULTIPLE_VALUE:
				mappedReference.setUpperBound(EStructuralFeature.UNBOUNDED_MULTIPLICITY);
				break;

			default:
				mappedReference.setUpperBound(EStructuralFeature.UNBOUNDED_MULTIPLICITY);
				break;
			}
			
		} else {
			mappedReference = (EReference) roleBinding.getModelElements().get(0);
			
			// TODO perform correspondence check
		}
		
		// TODO set opposite reference
		
		return wrapInList(mappedReference);
	}

}
