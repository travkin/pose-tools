/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.types.DataType;
import de.upb.pose.specification.types.PrimitiveType;

/**
 * @author Dietrich Travkin
 */
public class PrimitiveTypeTranslator extends DesignElementTranslator<PrimitiveType, EDataType> {

	public PrimitiveTypeTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(PrimitiveType primitiveType, RoleBinding roleBinding) {
		return (primitiveType.getType() != null
				&& primitiveType.eContainer() != null);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(PrimitiveType primitiveType, RoleBinding roleBinding) {
		DataType dataType = primitiveType.getType();
		EDataType targetDataType = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			switch (dataType.getValue()) {
			case DataType.BOOLEAN_VALUE:
				targetDataType = EcorePackage.eINSTANCE.getEBoolean();
				break;
			case DataType.INTEGER_VALUE:
				targetDataType = EcorePackage.eINSTANCE.getEInt();
				break;
			case DataType.REAL_VALUE:
				targetDataType = EcorePackage.eINSTANCE.getEFloat();
				break;
			case DataType.STRING_VALUE:
				targetDataType = EcorePackage.eINSTANCE.getEString();
				break;
				
			default:
				targetDataType = EcorePackage.eINSTANCE.getEString();
				break;
			}
		} else {
			targetDataType = (EDataType) roleBinding.getModelElements().get(0);
		}
		
		return wrapInList(targetDataType);
	}

}
