/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import org.eclipse.core.runtime.Assert;
import org.storydriven.core.expressions.Expression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Operation;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractVariableTranslator<T extends Expression> extends TokenTranslator<T> {

	public AbstractVariableTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#getTranslatableTokenType()
	 */
	@Override
	protected TokenType getTranslatableTokenType() {
		return TokenType.VARIABLE;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Assert.isTrue(TokenType.complyWith(token, TokenType.VARIABLE));
		Assert.isNotNull(getVariable(token));
		
		return true; // no real precondition, only consistency checks
	}
	
	protected Variable getVariable(Token variableToken) {
		Variable variable = (Variable) variableToken.getMapsTo().get(TokenMapKey.VARIABLE__IDENTIFIER.getKey());
		return variable;
	}
	
	protected Operation getParentOperation(RoleBinding roleBinding)
	{
		Operation parentOperation = null;
		
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement instanceof CallAction) {
			CallAction parentAction = (CallAction) applicationModelElement;
			parentOperation = parentAction.getOperation();
		}
		else if (applicationModelElement instanceof ParameterAssignment) {
			ParameterAssignment assignement = (ParameterAssignment) applicationModelElement;
			parentOperation = assignement.getAction().getOperation();
		}
		
		Assert.isNotNull(parentOperation);
		
		return parentOperation;
	}

}
