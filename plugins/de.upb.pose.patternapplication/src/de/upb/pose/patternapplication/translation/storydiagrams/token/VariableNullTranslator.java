/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.core.expressions.common.CommonExpressionsFactory;
import org.storydriven.core.expressions.common.LiteralExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.Variable;

/**
 * @author Dietrich Travkin
 */
public class VariableNullTranslator extends AbstractVariableTranslator<LiteralExpression> {

	public VariableNullTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof NullVariable);
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		LiteralExpression mappedExpression = CommonExpressionsFactory.eINSTANCE.createLiteralExpression();
		mappedExpression.setValue("null");
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), mappedExpression);
				
		return wrapInList(mappedExpression);
	}

}
