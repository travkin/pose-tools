/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.MatchingStoryNode;
import org.storydriven.storydiagrams.patterns.BindingOperator;
import org.storydriven.storydiagrams.patterns.BindingSemantics;
import org.storydriven.storydiagrams.patterns.BindingState;
import org.storydriven.storydiagrams.patterns.LinkVariable;
import org.storydriven.storydiagrams.patterns.MatchingPattern;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.PatternsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class BindReferenceTranslator extends TokenTranslator<ObjectVariable> {

	public BindReferenceTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#getTranslatableTokenType()
	 */
	@Override
	protected TokenType getTranslatableTokenType() {
		return TokenType.BIND_REFERENCE;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		EObject ref = token.getMapsTo().get(TokenMapKey.BIND_REFERENCE__REFERENCE.getKey());
		return ref != null
				&& ref instanceof Reference
				&& this.isApplicationModelElementMappedToTargetElements((Reference) ref)
				&& this.findParentActivity(roleBinding) != null;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Reference reference = (Reference) token.getMapsTo().get(TokenMapKey.BIND_REFERENCE__REFERENCE.getKey());
		EReference eReference = (EReference) this.getAllTargetElementsFor(reference).get(0);
		Activity parentActivity = this.findParentActivity(roleBinding);
		
		List<EObject> createdElements = new LinkedList<EObject>();
		
		MatchingStoryNode storyNode = ActivitiesFactory.eINSTANCE.createMatchingStoryNode();
		storyNode.setName("Bind the object referenced by \"" + eReference.getName() + "\"");
		storyNode.setOwningActivity(parentActivity);
		
		token.getMapsTo().put(TokenMapKey.BIND_REFERENCE__NODE.getKey(), storyNode);
		
		ActivityEdge outgoingEdge = ActivitiesFactory.eINSTANCE.createActivityEdge();
		outgoingEdge.setSource(storyNode);
		outgoingEdge.setOwningActivity(parentActivity);
		
		token.getMapsTo().put(TokenMapKey.BIND_REFERENCE__EDGE.getKey(), outgoingEdge);
		
		MatchingPattern pattern = PatternsFactory.eINSTANCE.createMatchingPattern();
		storyNode.setOwnedPattern(pattern);
		
		EOperation parentEOperation = StoryDiagramsEcoreConnector.getLinkedEOperation(parentActivity);
		EClass parentEClass = parentEOperation.getEContainingClass();
		
		ObjectVariable thisVariable = PatternsFactory.eINSTANCE.createObjectVariable();
		thisVariable.setName("this"); // TODO find the constant for this value
		thisVariable.setBindingState(BindingState.BOUND);
		thisVariable.setBindingSemantics(BindingSemantics.MANDATORY);
		thisVariable.setBindingOperator(BindingOperator.CHECK_ONLY);
		thisVariable.setClassifier(parentEClass);
		pattern.getVariables().add(thisVariable);
		
		EClass referencedClass = eReference.getEReferenceType();
		
		ObjectVariable referenceVariable = PatternsFactory.eINSTANCE.createObjectVariable();
		referenceVariable.setName("reference");
		referenceVariable.setBindingState(BindingState.UNBOUND);
		referenceVariable.setBindingSemantics(BindingSemantics.MANDATORY);
		referenceVariable.setBindingOperator(BindingOperator.CHECK_ONLY);
		referenceVariable.setClassifier(referencedClass);
		pattern.getVariables().add(referenceVariable);
		storyNode.getDeclaredVariables().add(referenceVariable);
		
		token.getMapsTo().put(TokenMapKey.BIND_REFERENCE__VARIABLE.getKey(), referenceVariable);
		
		LinkVariable link = PatternsFactory.eINSTANCE.createLinkVariable();
		link.setBindingOperator(BindingOperator.CHECK_ONLY);
		link.setBindingSemantics(BindingSemantics.MANDATORY);
		link.setName(eReference.getName());
		link.setSource(thisVariable);
		link.setTarget(referenceVariable);
		link.setTargetEnd(eReference); // the link type, i.e. the EReference
		pattern.getLinkVariables().add(link);
		
		
		createdElements.add(referenceVariable); // has to be the first one
		createdElements.add(storyNode);
		createdElements.add(outgoingEdge);
		createdElements.add(pattern);
		createdElements.add(thisVariable);
		createdElements.add(link);
		
		return createdElements;
	}
	
	private Activity findParentActivity(RoleBinding bindReferenceTokenParentRoleBinding) {
		Action action = (Action) bindReferenceTokenParentRoleBinding.getApplicationModelElement();
		
		RoleBinding operationRoleBinding = this.getRoleBinding(action.getOperation());
		Token operationRootToken = operationRoleBinding.getRootToken();
		
		Assert.isTrue(TokenType.complyWith(operationRootToken, TokenType.OPERATION_BEHAVIOR));
		
		Activity activity = (Activity) operationRootToken.getMapsTo().get(TokenMapKey.OPERATION_BEHAVIOR__STORY_DIAGRAM.getKey());
		return activity;
	}

}
