/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.storydriven.core.expressions.Expression;
import org.storydriven.storydiagrams.calls.CallsFactory;
import org.storydriven.storydiagrams.calls.InParameterBinding;
import org.storydriven.storydiagrams.calls.ParameterBinding;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public class ParameterAssignmentTranslator extends DesignElementTranslator<ParameterAssignment, ParameterBinding> {

	public ParameterAssignmentTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(ParameterAssignment parameterAssignment, RoleBinding roleBinding) {
		//Variable assignedVariable = parameterAssignment.getAssignedVariable();
		Parameter parameter = parameterAssignment.getParameter();
		
		// parameter is translated
		boolean preconditionSatisfied = this.isApplicationModelElementMappedToTargetElements(parameter);
		
//		// variable is translated if it's not 'null' or 'this'
//		if (!(assignedVariable instanceof SelfVariable) && !(assignedVariable instanceof NullVariable)) {
//			preconditionSatisfied = preconditionSatisfied && this.isApplicationModelElementMappedToTargetElements(assignedVariable);
//		}
		
		return preconditionSatisfied; 
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.DesignElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(ParameterAssignment parameterAssignment, RoleBinding roleBinding) {
		Variable assignedVariable = parameterAssignment.getAssignedVariable();
		Parameter parameter = parameterAssignment.getParameter();
		EParameter eParameter = (EParameter) this.getAllTargetElementsFor(parameter).get(0);

		Token argumentToken = TokenType.createToken(TokenType.ARGUMENT);
		roleBinding.setRootToken(argumentToken);
		
		Token variableSubToken = TokenType.createToken(TokenType.VARIABLE);
		argumentToken.getSubTokens().add(variableSubToken);
		
		variableSubToken.getMapsTo().put(TokenMapKey.VARIABLE__IDENTIFIER.getKey(), assignedVariable);
		
		InParameterBinding parameterBinding = CallsFactory.eINSTANCE.createInParameterBinding();
		parameterBinding.setParameter(eParameter);

		TokenTranslator<? extends EObject> variableTranslator = this.getTranslatorFor(TokenType.VARIABLE);
		variableTranslator.translate(variableSubToken);
		
		Expression callArgumentExpression = (Expression) variableSubToken.getMapsTo().get(TokenMapKey.VARIABLE__EXPRESSION.getKey());
		
		Assert.isNotNull(callArgumentExpression);
		
		argumentToken.getMapsTo().put(TokenMapKey.ARGUMENT__EXPRESSION.getKey(), callArgumentExpression);
		
		parameterBinding.setValueExpression(callArgumentExpression);
		
		return wrapInList(parameterBinding);
	}

}
