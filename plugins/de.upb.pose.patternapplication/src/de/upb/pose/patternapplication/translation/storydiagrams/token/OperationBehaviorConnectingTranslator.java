/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.ActivityFinalNode;
import org.storydriven.storydiagrams.activities.ActivityNode;
import org.storydriven.storydiagrams.activities.EdgeGuard;
import org.storydriven.storydiagrams.activities.InitialNode;
import org.storydriven.storydiagrams.activities.StoryNode;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.types.Operation;

/**
 * @author Dietrich Travkin
 */
public class OperationBehaviorConnectingTranslator extends TokenTranslator<EObject> {

	public OperationBehaviorConnectingTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#getTranslatableTokenType()
	 */
	@Override
	protected TokenType getTranslatableTokenType() {
		return TokenType.OPERATION_BEHAVIOR;
	}
	
	private Operation getParentOperationInApplicationModel(RoleBinding roleBinding) {
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement != null
				&& applicationModelElement instanceof Operation) {
			return (Operation) applicationModelElement;
		}
		return null;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		// story diagram is created
		boolean preconditionSatisfied = this.isTokenMappedToTargetElements(token);
		
		// actions are translated
		if (preconditionSatisfied) {
			Operation parentOperation = this.getParentOperationInApplicationModel(roleBinding);
			for (Action childAction: parentOperation.getActions()) {
				preconditionSatisfied = preconditionSatisfied
						&& this.isApplicationModelElementMappedToTargetElements(childAction);
			}
		}
			
		return preconditionSatisfied;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Operation parentOperation = getParentOperationInApplicationModel(roleBinding);
		Activity mappedStoryDiagram = (Activity) token.getMapsTo().get(TokenMapKey.OPERATION_BEHAVIOR__STORY_DIAGRAM.getKey());
		
		Assert.isNotNull(mappedStoryDiagram);
		
		InitialNode startNode = null;
		
		for (ActivityNode node: mappedStoryDiagram.getOwnedActivityNodes()) {
			if (node instanceof InitialNode) {
				startNode = (InitialNode) node;
				break;
			}
		}
		
		Assert.isNotNull(startNode);
		
		Assert.isTrue(startNode.getOutgoings().size() == 1);
		
		ActivityEdge lastEdge = startNode.getOutgoings().get(0);
		
		
		// go through all parent operation's actions contained in the application model and connect them
		for (Action containedAction : parentOperation.getActions()) {
			RoleBinding actionRoleBinding = getRoleBinding(containedAction);
			Token rootActionToken = actionRoleBinding.getRootToken();

			Assert.isTrue(TokenType.CONTROL_FLOW.compliesWith(rootActionToken));
			
			ActivityNode firstActionNode = (ActivityNode) rootActionToken.getMapsTo().get(
					TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey());
			
			Assert.isNotNull(firstActionNode);
			
			lastEdge.setTarget(firstActionNode);
			
			ActivityNode lastNodeInControlflow = this.findTheLastNodeInControlFlow(rootActionToken);
			
			if (!(lastNodeInControlflow instanceof ActivityFinalNode)) {
				Assert.isNotNull(rootActionToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__LAST_EDGE.getKey()));
				
				lastEdge = (ActivityEdge) rootActionToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__LAST_EDGE.getKey());
			} else {
				lastEdge = null;
			}
		}

		// add a final node if it is missing
		if (lastEdge != null && lastEdge.getTarget() == null) {
			ActivityFinalNode finalNode = ActivitiesFactory.eINSTANCE.createActivityFinalNode();
			finalNode.setOwningActivity(mappedStoryDiagram);
			lastEdge.setTarget(finalNode);
			
			return wrapInList(finalNode);
		}

		return Collections.emptyList();
	}
	
	protected final ActivityNode findTheLastNodeInControlFlow(Token controlFlowToken) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		
		ActivityNode node = (ActivityNode) controlFlowToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey());
		ActivityEdge outgoingEdge = getDefaultoutgoingEdge(node);
		
		while (outgoingEdge != null && outgoingEdge.getTarget() != null) {
			node = outgoingEdge.getTarget();
			outgoingEdge = getDefaultoutgoingEdge(node);
		}
		
		return node;
	}
	
	protected ActivityEdge getDefaultoutgoingEdge(ActivityNode sourceNode) {
		if (sourceNode.getOutgoings().isEmpty()) {
			return null;
		}
		
		if (sourceNode instanceof StoryNode
				&& ((StoryNode) sourceNode).isForEach()) {
			Assert.isTrue(sourceNode.getOutgoings().size() == 2);
			
			for (ActivityEdge outgoingEdge: sourceNode.getOutgoings()) {
				if (EdgeGuard.END.equals(outgoingEdge.getGuard())) {
					return outgoingEdge;
				}
			}
			return null;
		} else {
			Assert.isTrue(sourceNode.getOutgoings().size() == 1);
			return sourceNode.getOutgoings().get(0);
		}
	}
	
}
