/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.expressions.ObjectVariableExpression;
import org.storydriven.storydiagrams.patterns.expressions.PatternsExpressionsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class VariableReferenceTranslator extends AbstractVariableTranslator<ObjectVariableExpression> {

	public VariableReferenceTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof Reference);
		
		Reference reference = (Reference) var;
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding)
				&& this.isApplicationModelElementMappedToTargetElements(reference);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Reference reference = (Reference) getVariable(token);
		
		Token subTokenBindReference = TokenType.createToken(TokenType.BIND_REFERENCE);
		token.getSubTokens().add(subTokenBindReference);
		
		subTokenBindReference.getMapsTo().put(TokenMapKey.BIND_REFERENCE__REFERENCE.getKey(), reference);
		
		TokenTranslator<? extends EObject> translator = this.getTranslatorFor(TokenType.BIND_REFERENCE);
		translator.translate(subTokenBindReference);
		
		ObjectVariable objectVariable = (ObjectVariable) subTokenBindReference.getMapsTo().get(TokenMapKey.BIND_REFERENCE__VARIABLE.getKey());
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__VARIABLE.getKey(), objectVariable);
		
		ObjectVariableExpression objectVariableExpression = PatternsExpressionsFactory.eINSTANCE.createObjectVariableExpression();
		objectVariableExpression.setObject(objectVariable);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), objectVariableExpression);
				
		return wrapInList(objectVariableExpression);
	}

}
