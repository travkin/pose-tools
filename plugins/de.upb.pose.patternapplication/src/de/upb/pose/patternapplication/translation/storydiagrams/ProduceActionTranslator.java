/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityFinalNode;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.expressions.ObjectVariableExpression;
import org.storydriven.storydiagrams.patterns.expressions.PatternsExpressionsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.ProduceAction;

/**
 * @author Dietrich Travkin
 */
public class ProduceActionTranslator extends AbstractCreateActionTranslator<ProduceAction> {
	
	public ProduceActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected List<EObject> translate(ProduceAction action, RoleBinding roleBinding) {
		List<EObject> createdElements = super.translate(action, roleBinding);
		
		Activity parentActivity = this.getParentActivity(action);
		
		//ModifyingStoryNode mappedModifyingStoryNode;
		if (roleBinding.getModelElements().isEmpty()) {
			//mappedModifyingStoryNode = (ModifyingStoryNode) createdElements.get(0);
			
			ActivityFinalNode finalNode = ActivitiesFactory.eINSTANCE.createActivityFinalNode();
			parentActivity.getOwnedActivityNodes().add(finalNode);
			createdElements.add(finalNode);
			
			Token rootControlFlowToken = roleBinding.getRootToken();
			this.appendNewNodeToControlFlow(rootControlFlowToken, finalNode);
			
			Token creationToken = TokenType.findSubTokenOfType(rootControlFlowToken, TokenType.CREATION);
			
			Assert.isNotNull(creationToken);
			
			ObjectVariable objectVariable = (ObjectVariable) creationToken.getMapsTo().get(TokenMapKey.CREATION__VARIABLE.getKey());
			
			Assert.isNotNull(objectVariable);
			
			ObjectVariableExpression objectVariableExpression = PatternsExpressionsFactory.eINSTANCE.createObjectVariableExpression();
			objectVariableExpression.setObject(objectVariable);
			finalNode.getReturnValues().add(objectVariableExpression);
			createdElements.add(objectVariableExpression);
			
			EClass objectVariableType = objectVariable.getClassifier();
			
			// ensure, return type is compatible to the return variable type
			EClassifier returnParameterType = parentActivity.getOutParameters().get(0).getEType();
			Assert.isTrue(objectVariableType.getEAllSuperTypes().contains(returnParameterType));
		} else {
			//mappedModifyingStoryNode = (ModifyingStoryNode) roleBinding.getModelElements().get(0);
			
			// TODO correspondence check
		}
		return createdElements;
	}
	
}
