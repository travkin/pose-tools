/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcoreFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.ElementTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public class OperationTranslator extends DesignElementTranslator<Operation, EOperation>{

	public OperationTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Operation operation, RoleBinding roleBinding) {
		// parent type is translated
		boolean satisfied = (operation.getParentType() != null
				&& this.isApplicationModelElementMappedToTargetElements(operation.getParentType()));
		
		// return type is translated
		if (operation.getType() != null) {
			satisfied = satisfied && this.isApplicationModelElementMappedToTargetElements(operation.getType());
		}
		
		// super operation is translated
		if (operation.getOverrides() != null) {
			satisfied = satisfied && this.isApplicationModelElementMappedToTargetElements(operation.getOverrides());
		}
		
		return satisfied;
	}
	

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Operation operation, RoleBinding roleBinding) {
		EClass parentClass = (EClass) this.getAllTargetElementsFor(operation.getParentType()).get(0);
		
		EOperation mappedOperation = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			// create a new operation
			mappedOperation = EcoreFactory.eINSTANCE.createEOperation();
			parentClass.getEOperations().add(mappedOperation);
			
			if (operation.getOverrides() != null) {
				// take signature from overridden operation
				EOperation overriddenOperation = (EOperation) this.getAllTargetElementsFor(operation.getOverrides()).get(0);
				adaptOperationSignatureToOverriddenOperation(mappedOperation, overriddenOperation);
			} else {
				// set name
				mappedOperation.setName(operation.getName());
				
				// set return type
				if (operation.getType() != null) {
					EObject returnType = this.getAllTargetElementsFor(operation.getType()).get(0);
					EClassifier resultTypeClass = (EClassifier) returnType;
					mappedOperation.setEType(resultTypeClass);
				}
			}
			
			// TODO somehow set the abstraction, there seems to be no attribute in Ecore meta-model for that
		} else {
			mappedOperation = (EOperation) roleBinding.getModelElements().get(0);
			
			// TODO perform correspondence check
		}
		
		// translate parameters
		if (!operation.getParameters().isEmpty()) {
			@SuppressWarnings("unchecked")
			ElementTranslator<Parameter, EParameter>  paramTranslator = (ElementTranslator<Parameter, EParameter>) this.getTranslatorFor(Parameter.class);
			for (Parameter parameter: operation.getParameters()) {
				this.addTranslationStep(parameter, paramTranslator);
			}					
		}
		
		// translate the actions
		if (!operation.getActions().isEmpty()) {
			Token translateBehaviorToken = TokenType.createToken(TokenType.OPERATION_BEHAVIOR);
			roleBinding.setRootToken(translateBehaviorToken);
				
			TokenTranslator<? extends EObject>  tokenTranslator = this.getTranslatorFor(TokenType.OPERATION_BEHAVIOR);
			addTranslationStep(translateBehaviorToken, tokenTranslator);
		}
				
		return wrapInList(mappedOperation);
	}
	
	private void adaptOperationSignatureToOverriddenOperation(EOperation operationToAdapt, EOperation overriddenOperation) {
		operationToAdapt.setName(overriddenOperation.getName());
		
		// return type
		operationToAdapt.setEType(overriddenOperation.getEType());
		
		// parameters
		for (EParameter parentParam: overriddenOperation.getEParameters()) {
			EParameter newParam = EcoreFactory.eINSTANCE.createEParameter();
			newParam.setName(parentParam.getName());
			newParam.setEType(parentParam.getEType());
			operationToAdapt.getEParameters().add(newParam);
		}
	}

}