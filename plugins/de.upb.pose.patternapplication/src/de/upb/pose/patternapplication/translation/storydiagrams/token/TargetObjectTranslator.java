/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.core.expressions.Expression;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.EdgeGuard;
import org.storydriven.storydiagrams.activities.MatchingStoryNode;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Reference;

/**
 * @author Dietrich Travkin
 */
public class TargetObjectTranslator extends TokenTranslator<Expression> {

	public TargetObjectTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#getTranslatableTokenType()
	 */
	@Override
	protected TokenType getTranslatableTokenType() {
		return TokenType.TARGET_OBJECT;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Assert.isTrue(TokenType.complyWith(token, TokenType.TARGET_OBJECT));
		Assert.isNotNull(getVariable(roleBinding));
		
		return true; // no real precondition, only consistency checks
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token targetObjectToken, RoleBinding roleBinding) {
		Variable variable = getVariable(roleBinding);
		
		Expression mappedExpression = (Expression) targetObjectToken.getMapsTo().get(TokenMapKey.TARGET_OBJECT__EXPRESSION.getKey());
		
		if (mappedExpression == null) {
			
			Token variableToken = TokenType.createToken(TokenType.VARIABLE);
			targetObjectToken.getSubTokens().add(variableToken);
			variableToken.getMapsTo().put(TokenMapKey.VARIABLE__IDENTIFIER.getKey(), variable);
			
			TokenTranslator<? extends EObject> variableTranslator = this.getTranslatorFor(TokenType.VARIABLE);
			variableTranslator.translate(variableToken);
			
			mappedExpression = (Expression) variableToken.getMapsTo().get(TokenMapKey.VARIABLE__EXPRESSION.getKey());
			
			Assert.isNotNull(mappedExpression);
			
			List<EObject> createdElements = new LinkedList<EObject>();
			createdElements.add(mappedExpression);
			
			targetObjectToken.getMapsTo().put(TokenMapKey.TARGET_OBJECT__EXPRESSION.getKey(), mappedExpression);
			
			if (variable instanceof Reference) {
				Reference reference = (Reference) variable;
				CardinalityType cardinality = reference.getCardinality();
				if (CardinalityType.MULTIPLE.equals(cardinality)) {
					CallAction callAction = (CallAction) roleBinding.getApplicationModelElement();
					if (callAction.isForAll()) {
						Token subTokenBindReference = TokenType.findSubTokenOfType(variableToken, TokenType.BIND_REFERENCE);
						
						MatchingStoryNode storyNode = (MatchingStoryNode) subTokenBindReference.getMapsTo().get(TokenMapKey.BIND_REFERENCE__NODE.getKey());
						ActivityEdge outgoingEdge = (ActivityEdge) subTokenBindReference.getMapsTo().get(TokenMapKey.BIND_REFERENCE__EDGE.getKey());
						
						ActivityEdge terminationEdge = ActivitiesFactory.eINSTANCE.createActivityEdge();
						terminationEdge.setOwningActivity(outgoingEdge.getOwningActivity());
						terminationEdge.setSource(storyNode);
						createdElements.add(terminationEdge);
						
						Token iterationToken = TokenType.createToken(TokenType.ITERATION);
						targetObjectToken.getSubTokens().add(iterationToken);
						
						storyNode.setForEach(true);
						outgoingEdge.setGuard(EdgeGuard.EACH_TIME);
						terminationEdge.setGuard(EdgeGuard.END);
						
						iterationToken.getMapsTo().put(TokenMapKey.ITERATION__NODE.getKey(), storyNode);
						iterationToken.getMapsTo().put(TokenMapKey.ITERATION__EDGE.getKey(), outgoingEdge);
						iterationToken.getMapsTo().put(TokenMapKey.ITERATION__TERMINATION_EDGE.getKey(), terminationEdge);
					}
				}
			}
			return createdElements;
		}
		
		return wrapInList(mappedExpression);
	}

	private Variable getVariable(RoleBinding actionRoleBinding) {
		Assert.isNotNull(actionRoleBinding.getApplicationModelElement());
		Assert.isTrue(actionRoleBinding.getApplicationModelElement() instanceof CallAction);
		CallAction action = (CallAction) actionRoleBinding.getApplicationModelElement();
		
		return action.getTarget();
	}
	
}
