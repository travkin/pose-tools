/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.InitialNode;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public class OperationBehaviorTranslator extends TokenTranslator<Activity> {

	private final OperationBehaviorConnectingTranslator controlflowConnectingChildTranslator;
	
	public OperationBehaviorTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
		this.controlflowConnectingChildTranslator = new OperationBehaviorConnectingTranslator(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#getTranslatableTokenType()
	 */
	@Override
	protected TokenType getTranslatableTokenType() {
		return TokenType.OPERATION_BEHAVIOR;
	}
	
	private Operation getParentOperationInApplicationModel(RoleBinding roleBinding) {
		DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
		if (applicationModelElement != null
				&& applicationModelElement instanceof Operation) {
			return (Operation) applicationModelElement;
		}
		return null;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Operation parentOperation = getParentOperationInApplicationModel(roleBinding);
		
		// parent operation is translated
		boolean preconditionSatisfied = parentOperation != null
				&& this.isApplicationModelElementMappedToTargetElements(parentOperation);
		
		// parent operation's parameter are translated, too
		if (preconditionSatisfied && !parentOperation.getParameters().isEmpty()) {
			for (Parameter param: parentOperation.getParameters()) {
				preconditionSatisfied = preconditionSatisfied
						&& this.isApplicationModelElementMappedToTargetElements(param); 
			}
		}
		
		return preconditionSatisfied;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		Operation parentOperation = getParentOperationInApplicationModel(roleBinding);
		EOperation parentEOperation = (EOperation) this.getAllTargetElementsFor(parentOperation).get(0);
		
		Activity mappedStoryDiagram = StoryDiagramsEcoreConnector.getLinkedActivity(parentEOperation);
		
		if (!this.isTokenMappedToTargetElements(token)) {
			if (mappedStoryDiagram != null) {
				throw new IllegalStateException("The parent EOperation is already connected to a story diagram. " +
						"That's inconsistent to the token state.");
			}
			
			// create a new story diagram resource
			Assert.isNotNull(parentEOperation.eResource());
			Assert.isNotNull(parentEOperation.eResource().getResourceSet());
			
			ResourceSet parentResourceSet = parentEOperation.eResource().getResourceSet();
			URI sdResourceUri = StoryDiagramsEcoreConnector.getUriForStoryDiagram(parentEOperation);
			if (!StoryDiagramsEcoreConnector.isValidUriWithNoExistingFile(sdResourceUri)) {
				throw new IllegalStateException("Unexpected URI format or file already exists: " + sdResourceUri);
			}
			Resource sdResource = StoryDiagramsEcoreConnector.createStoryDiagramResource(parentResourceSet, sdResourceUri);
			Assert.isNotNull(sdResource);
			
			List<EObject> createdElements = new LinkedList<EObject>();
			
			// create and fill a new story diagram (model) and add it to the resource
			mappedStoryDiagram = StoryDiagramsEcoreConnector.createActivityFor(parentEOperation);
			sdResource.getContents().add(mappedStoryDiagram);
			
			// collect first set of created elements
			createdElements.add(mappedStoryDiagram);
			TreeIterator<EObject> iter = mappedStoryDiagram.eAllContents();
			while (iter.hasNext()) {
				createdElements.add(iter.next());
			}
			
			// map token with the created story diagram
			token.getMapsTo().put(TokenMapKey.OPERATION_BEHAVIOR__STORY_DIAGRAM.getKey(), mappedStoryDiagram);
			
			// connect the operation with the corresponding story diagram
			EAnnotation pointerToStoryDiagram = StoryDiagramsEcoreConnector.getPointerToActivity(parentEOperation);
			if (pointerToStoryDiagram == null) {
				pointerToStoryDiagram = StoryDiagramsEcoreConnector.createPointerToActivity(parentEOperation);
				
				createdElements.add(pointerToStoryDiagram);
			}
			StoryDiagramsEcoreConnector.linkEOperationToActivity(parentEOperation, mappedStoryDiagram);
			
			// create an edge outgoing from start node
			ActivityEdge lastEdge = ActivitiesFactory.eINSTANCE.createActivityEdge();
			lastEdge.setOwningActivity(mappedStoryDiagram);
			
			createdElements.add(lastEdge);
			
			InitialNode startNode = (InitialNode) mappedStoryDiagram.getOwnedActivityNodes().get(0);
			lastEdge.setSource(startNode);
			
			// go through all parent operation's actions contained in the application model and ensure they will be translated
			for (Action currentAction: parentOperation.getActions()) {
				this.addTranslationStep(currentAction);
			}
			
			// use another token translator to connect the intermediate translation results (translated actions) in a common control flow 
			this.addTranslationStep(token, this.controlflowConnectingChildTranslator);
			
			return createdElements;
		} else {
			// TODO correspondence check: activity signature vs. (E)Operation signature
		}

		return wrapInList(mappedStoryDiagram);
	}
	
}
