/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.PrimitiveType;

/**
 * @author Dietrich Travkin
 */
public class AttributeTranslator extends DesignElementTranslator<Attribute, EAttribute>
{
	public AttributeTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Attribute attribute, RoleBinding roleBinding) {
		boolean preconditionSatisfied = (attribute.getParentType() != null
				&& this.isApplicationModelElementMappedToTargetElements(attribute.getParentType()));
		
		if (attribute.getType() != null) {
			preconditionSatisfied = preconditionSatisfied
					&& this.isApplicationModelElementMappedToTargetElements(attribute.getType()); 
		}
		
		return preconditionSatisfied;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Attribute attribute, RoleBinding roleBinding) {
		EClass parentClass = (EClass) this.getAllTargetElementsFor(attribute.getParentType()).get(0);
		
		EAttribute mappedAttribute = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			mappedAttribute = EcoreFactory.eINSTANCE.createEAttribute();
			parentClass.getEStructuralFeatures().add(mappedAttribute);
			
			mappedAttribute.setName(attribute.getName());
			
			switch (attribute.getCardinality().getValue()) {
			case CardinalityType.MULTIPLE_VALUE:
				mappedAttribute.setUpperBound(EStructuralFeature.UNBOUNDED_MULTIPLICITY);
				break;

			default:
				mappedAttribute.setUpperBound(1);
				break;
			}
			
			PrimitiveType attributeType = (PrimitiveType) attribute.getType();
			if (attributeType != null) {
				EDataType type = (EDataType) this.getAllTargetElementsFor(attributeType).get(0);
				
				Assert.isNotNull(type);
				
				mappedAttribute.setEType(type);
			} else {
				mappedAttribute.setEType(EcorePackage.eINSTANCE.getEString());
			}
		} else {
			mappedAttribute = (EAttribute) roleBinding.getModelElements().get(0);
			
			// TODO perform correspondence check
		}
		
		return wrapInList(mappedAttribute);
	}

}
