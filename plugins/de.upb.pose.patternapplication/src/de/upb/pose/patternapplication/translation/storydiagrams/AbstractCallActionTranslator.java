/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.storydriven.core.expressions.Expression;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.ActivityNode;
import org.storydriven.storydiagrams.activities.EdgeGuard;
import org.storydriven.storydiagrams.activities.OperationExtension;
import org.storydriven.storydiagrams.activities.StatementNode;
import org.storydriven.storydiagrams.activities.StoryNode;
import org.storydriven.storydiagrams.calls.OutParameterBinding;
import org.storydriven.storydiagrams.calls.ParameterBinding;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.MethodCallExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.TokenTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractCallActionTranslator<A extends CallAction> extends AbstractActionTranslator<A, StatementNode> {
	
	public AbstractCallActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected Class<StatementNode> getTranslationResultType() {
		return StatementNode.class;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.storydiagrams.AbstractActionTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.actions.Action, de.upb.pose.mapping.RoleBinding)
	 */
	protected boolean isTranslationPreconditionSatisfied(A action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		
		if (action.isCanBeGenerated()) {
			// called operation is translated
			preconditionSatisfied = preconditionSatisfied && this.isApplicationModelElementMappedToTargetElements(action.getCalledOperation());
			
			// called operation's parameters are translated
			for (Parameter param: action.getCalledOperation().getParameters()) {
				preconditionSatisfied = preconditionSatisfied
						&& this.isApplicationModelElementMappedToTargetElements(param);
			}
			
			// target variable is translated
			if (action.getTarget() != null) {
				preconditionSatisfied = preconditionSatisfied
						&& this.isApplicationModelElementMappedToTargetElements(action.getTarget());
			}
		}
		
		return preconditionSatisfied; 
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(A action, RoleBinding roleBinding) {
		Activity parentActivity = this.getParentActivity(action);
		
		StatementNode mappedStatementNode = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			List<EObject> createdElements = new LinkedList<EObject>();
			
			mappedStatementNode = this.createNode(parentActivity, createdElements);
			ActivityEdge outgoingEdge = this.createOutgoingEdge(mappedStatementNode, parentActivity, createdElements);
			
			Token rootControlFlowToken = this.createControlFlowToken(roleBinding, createdElements);
			
			MethodCallExpression methodCallExpression = null;
			
			if (action.isCanBeGenerated()) {
				Token callToken = this.createCallToken(rootControlFlowToken, createdElements);
				Token targetObjectToken = this.createTargetObjectToken(rootControlFlowToken, createdElements);
			
				callToken.getMapsTo().put(TokenMapKey.CALL__NODE.getKey(), mappedStatementNode);
				callToken.getMapsTo().put(TokenMapKey.CALL__EDGE.getKey(), outgoingEdge);
				
				methodCallExpression = this.createMethodCallExpression(
						action, mappedStatementNode, parentActivity, createdElements);
				
				callToken.getMapsTo().put(TokenMapKey.CALL__EXPRESSION.getKey(), methodCallExpression);
				
				Expression targetExpression = this.translateTargetObjectToken(targetObjectToken);
				methodCallExpression.setTarget(targetExpression);
				
				ActivityNode newNode = findBindReferenceNode(targetObjectToken);
				if (newNode != null) {
					appendNewNodeToControlFlow(rootControlFlowToken, newNode);
				}
				
				for (ParameterAssignment parameterAssignment: action.getAssignments()) {
					ParameterBinding parameterBinding = translateCallArgument(parameterAssignment);
					methodCallExpression.getOwnedParameterBindings().add(parameterBinding);
					
					Token argumentToken = findArgumentToken(parameterAssignment);
					newNode = findBindReferenceNode(argumentToken);
					if (newNode != null) {
						appendNewNodeToControlFlow(rootControlFlowToken, newNode);
					}
				}
			} else {
				String nodeName = "ToDo";
				if (action.getCalledOperation() != null
						&& action.getCalledOperation().getName() != null
						&& !action.getCalledOperation().getName().isEmpty()) {
					nodeName += ": call \"" + action.getCalledOperation().getName() + "\"";
				}
				mappedStatementNode.setName(nodeName);
			}
			
			appendNewNodeToControlFlow(rootControlFlowToken, mappedStatementNode);
			
			if (action.getResultVariable() != null && action.isCanBeGenerated()) {
				OutParameterBinding parameterBinding = translateResultVariable(action.getResultVariable());
				parameterBinding.getResultVariable().setOwningInvocation(methodCallExpression);
				mappedStatementNode.getDeclaredVariables().add(parameterBinding.getResultVariable());
				methodCallExpression.getOwnedParameterBindings().add(parameterBinding);
			}
			
			// complete control flow and handle iteration if necessary
			completeControlFlowForCall(rootControlFlowToken, mappedStatementNode);
			
			return createdElements;
		} else {
			mappedStatementNode = (StatementNode) roleBinding.getModelElements().get(0);
			
			// TODO correspondence check
		}
		return wrapInList(mappedStatementNode);
	}
	
	private Expression translateTargetObjectToken(Token targetObjectToken) {
		Assert.isTrue(TokenType.complyWith(targetObjectToken, TokenType.TARGET_OBJECT));
		
		TokenTranslator<? extends EObject> translator = this.getTranslatorFor(TokenType.TARGET_OBJECT);
		translator.translate(targetObjectToken);
		Expression targetExpression = (Expression) targetObjectToken.getMapsTo().get(TokenMapKey.TARGET_OBJECT__EXPRESSION.getKey());;
		Assert.isNotNull(targetExpression);
		
		return targetExpression;
	}
	
	private ParameterBinding translateCallArgument(ParameterAssignment argument) {
		DesignElementTranslator<ParameterAssignment, ? extends EObject> translator = this.getTranslatorFor(ParameterAssignment.class);
		translator.translate(argument);
		
		ParameterBinding parameterBinding = (ParameterBinding) this.getAllTargetElementsFor(argument).get(0);
		return parameterBinding;
	}
	
	private OutParameterBinding translateResultVariable(ResultVariable resultVariable) {
		DesignElementTranslator<ResultVariable, ? extends EObject> translator = this.getTranslatorFor(ResultVariable.class);
		translator.translate(resultVariable);
		
		OutParameterBinding parameterBinding = (OutParameterBinding) this.getAllTargetElementsFor(resultVariable).get(0);
		return parameterBinding;
	}
	
	private StatementNode createNode(Activity parentActivity, List<EObject> createdElements) {
		StatementNode newStatementNode = ActivitiesFactory.eINSTANCE.createStatementNode();
		parentActivity.getOwnedActivityNodes().add(newStatementNode);
		createdElements.add(newStatementNode);
		return newStatementNode;
	}
	
	private Token createCallToken(Token rootControlFlowToken, List<EObject> createdElements) {
		Assert.isTrue(TokenType.complyWith(rootControlFlowToken, TokenType.CONTROL_FLOW));
		
		Token callToken = TokenType.createToken(TokenType.CALL);
		rootControlFlowToken.getSubTokens().add(callToken);
		createdElements.add(callToken);
		
		return callToken;
	}
	
	private Token createTargetObjectToken(Token rootControlFlowToken, List<EObject> createdElements) {
		Assert.isTrue(TokenType.complyWith(rootControlFlowToken, TokenType.CONTROL_FLOW));
		
		Token targetObjectToken = TokenType.createToken(TokenType.TARGET_OBJECT);
		rootControlFlowToken.getSubTokens().add(targetObjectToken);
		createdElements.add(targetObjectToken);
		
		return targetObjectToken;
	}
	
	private MethodCallExpression createMethodCallExpression(A action, StatementNode parentStatementNode, Activity parentActivity, List<EObject> createdElements) {
		MethodCallExpression methodCallExpression = CallsExpressionsFactory.eINSTANCE.createMethodCallExpression();
		parentStatementNode.setStatementExpression(methodCallExpression);
		createdElements.add(methodCallExpression);
		
		Operation calledOperation = action.getCalledOperation();
		EOperation calledEOperation = (EOperation) this.getAllTargetElementsFor(calledOperation).get(0);
		
		// create an OperationExtension if it is missing
		OperationExtension operationExtension = StoryDiagramsEcoreConnector.getOperationExtension(calledEOperation, parentActivity);
		if (operationExtension == null) {
			operationExtension = StoryDiagramsEcoreConnector.createOperationExtension(calledEOperation, parentActivity);
			createdElements.add(operationExtension);
		}
		
		methodCallExpression.setCallee(operationExtension);
		
		return methodCallExpression;
	}
	
	private Token findArgumentToken(ParameterAssignment argument) {
		RoleBinding argumentRoleBinding = this.getRoleBinding(argument);
		Token rootToken = argumentRoleBinding.getRootToken();
		
		Assert.isNotNull(rootToken);
		Assert.isTrue(TokenType.complyWith(rootToken, TokenType.ARGUMENT));
		
		return rootToken;
	}
	
	private ActivityNode findBindReferenceNode(Token targetObjectOrArgumentToken) {
		Assert.isTrue(TokenType.complyWith(targetObjectOrArgumentToken, TokenType.TARGET_OBJECT)
				|| TokenType.complyWith(targetObjectOrArgumentToken, TokenType.ARGUMENT));
		
		Token variableSubToken = TokenType.findSubTokenOfType(targetObjectOrArgumentToken, TokenType.VARIABLE);
		if (variableSubToken != null) {
			Token bindReferenceSubToken = TokenType.findSubTokenOfType(variableSubToken, TokenType.BIND_REFERENCE);
			if (bindReferenceSubToken != null) {
				ActivityNode node = (ActivityNode) bindReferenceSubToken.getMapsTo().get(TokenMapKey.BIND_REFERENCE__NODE.getKey());
				return node;
			}
		}
		return null;
	}
	
	private Token findIterationToken(Token controlFlowToken) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		
		Token targetObjectToken = TokenType.findSubTokenOfType(controlFlowToken, TokenType.TARGET_OBJECT);
		
		Token iterationSubToken = targetObjectToken != null ? TokenType.findSubTokenOfType(targetObjectToken, TokenType.ITERATION) : null;
		return iterationSubToken;
	}
	
	private void completeControlFlowForCall(Token controlFlowToken, StatementNode callNode) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		
		ActivityNode lastNode = findTheLastNodeInControlFlow(controlFlowToken);
		ActivityEdge lastOutgoingEdge = getDefaultoutgoingEdge(lastNode);
		
		Token iterationToken = this.findIterationToken(controlFlowToken);
		if (iterationToken == null) {
			setLastEdgeInControlflow(controlFlowToken, lastOutgoingEdge);
		} else {
			ActivityNode iterationNode = (ActivityNode) iterationToken.getMapsTo().get(TokenMapKey.ITERATION__NODE.getKey());
			ActivityNode firstNode = (ActivityNode) controlFlowToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey());
			
			Assert.isNotNull(iterationNode);
			Assert.isNotNull(firstNode);
			Assert.isTrue(firstNode.equals(iterationNode));
			
			ActivityEdge terminationEdge = (ActivityEdge) iterationToken.getMapsTo().get(TokenMapKey.ITERATION__TERMINATION_EDGE.getKey());
			
			Assert.isNotNull(terminationEdge);
			Assert.isTrue(lastOutgoingEdge.getTarget() == null);
			
			lastOutgoingEdge.setTarget(iterationNode);
			
			setLastEdgeInControlflow(controlFlowToken, terminationEdge);
		}
	}
	
	@Override
	protected ActivityEdge getDefaultoutgoingEdge(ActivityNode sourceNode) {
		if (sourceNode instanceof StoryNode
				&& ((StoryNode) sourceNode).isForEach()) {
			Assert.isTrue(sourceNode.getOutgoings().size() == 2);
			
			for (ActivityEdge outgoingEdge: sourceNode.getOutgoings()) {
				if (EdgeGuard.EACH_TIME.equals(outgoingEdge.getGuard())) {
					return outgoingEdge;
				}
			}
			return null;
		} else {
			return super.getDefaultoutgoingEdge(sourceNode);
		}
	}
	
}
