/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;


/**
 * @author Dietrich Travkin
 */
public enum TokenMapKey {

	OPERATION_BEHAVIOR__STORY_DIAGRAM(TokenType.OPERATION_BEHAVIOR, "story diagram"),
	CONTROL_FLOW__FIRST_NODE(TokenType.CONTROL_FLOW, "first node"),
	CONTROL_FLOW__LAST_EDGE(TokenType.CONTROL_FLOW, "last edge"),
	CALL__NODE(TokenType.CALL, "node"),
	CALL__EDGE(TokenType.CALL, "edge"),
	CALL__EXPRESSION(TokenType.CALL, "expression"),
	TARGET_OBJECT__EXPRESSION(TokenType.TARGET_OBJECT, "expression"),
	VARIABLE__VARIABLE(TokenType.VARIABLE, "variable"),
	VARIABLE__EXPRESSION(TokenType.VARIABLE, "expression"),
	VARIABLE__IDENTIFIER(TokenType.VARIABLE, "identifier"),
	ARGUMENT__EXPRESSION(TokenType.ARGUMENT, "expression"),
	BIND_REFERENCE__NODE(TokenType.BIND_REFERENCE, "node"),
	BIND_REFERENCE__EDGE(TokenType.BIND_REFERENCE, "edge"),
	BIND_REFERENCE__VARIABLE(TokenType.BIND_REFERENCE, "variable"),
	BIND_REFERENCE__REFERENCE(TokenType.BIND_REFERENCE, "reference"),
	CALL_RESULT__VARIABLE(TokenType.CALL_RESULT, "variable"),
	ITERATION__NODE(TokenType.ITERATION, "node"),
	ITERATION__EDGE(TokenType.ITERATION, "iteration edge"),
	ITERATION__TERMINATION_EDGE(TokenType.ITERATION, "termination edge"),
	CREATION__NODE(TokenType.CREATION, "node"),
	CREATION__EDGE(TokenType.CREATION, "edge"),
	CREATION__VARIABLE(TokenType.CREATION, "object variable"),
	RETURN__NODE(TokenType.RETURN, "node"),
	RETURN__EXPRESSION(TokenType.RETURN, "expression");
	
	private final String key;
	private final TokenType tokenType;
	
	TokenMapKey(TokenType tokenType, String key) {
		this.key = key;
		this.tokenType = tokenType;
	}
	
	public TokenType getTokenType() {
		return this.tokenType;
	}
	
	public String getKey() {
		return this.key;
	}
}
