/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.ElementTranslator;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class TypeTranslator extends DesignElementTranslator<Type, EClass> {
	
	private final TypeInheritanceTranslator inheritanceTranslator;
	
	public TypeTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
		this.inheritanceTranslator = new TypeInheritanceTranslator(parent);
	}
	
	private EPackage getTargetModelRoot(RoleBinding roleBinding) {
		if (roleBinding != null
				&& roleBinding.getAppliedPattern() != null
				&& roleBinding.getAppliedPattern().getParent() != null
				&& roleBinding.getAppliedPattern().getParent().getDesignModelRoot() != null) {
			
			EObject designModelRoot = roleBinding.getAppliedPattern().getParent().getDesignModelRoot();
			
			if (designModelRoot instanceof EPackage) {
				return (EPackage) designModelRoot;
			}
		}
		return null;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Type typeToTranslate, RoleBinding roleBinding) {
		return (getTargetModelRoot(roleBinding) != null);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Type typeToTranslate, RoleBinding roleBinding) {
		EPackage parentPackage = getTargetModelRoot(roleBinding);
		
		EClass mappedClass = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			mappedClass = EcoreFactory.eINSTANCE.createEClass();
			parentPackage.getEClassifiers().add(mappedClass);
			
			mappedClass.setName(typeToTranslate.getName());
			mappedClass.setAbstract(typeToTranslate.getAbstraction().equals(AbstractionType.ABSTRACT));
			
		} else {
			mappedClass = (EClass) roleBinding.getModelElements().get(0);
			// TODO perform correspondence check
		}
		
		if (typeToTranslate.getSuperType() != null) {
			addTranslationStep(typeToTranslate, this.inheritanceTranslator);
		}
		
		ElementTranslator<Attribute, ? extends EObject> attributeTranslator = this.getTranslatorFor(Attribute.class);
		for (Attribute attribute: typeToTranslate.getAttributes()) {
			addTranslationStep(attribute, attributeTranslator);
		}
		
		ElementTranslator<Operation, ? extends EObject> operationTranslator = this.getTranslatorFor(Operation.class);
		for (Operation operation: typeToTranslate.getOperations()) {
			addTranslationStep(operation, operationTranslator);
		}
		
		ElementTranslator<Reference, ? extends EObject> referenceTranslator = this.getTranslatorFor(Reference.class);
		for (Reference reference: typeToTranslate.getReferences()) {
			addTranslationStep(reference, referenceTranslator);
		}
		
		return wrapInList(mappedClass);
	}

}
