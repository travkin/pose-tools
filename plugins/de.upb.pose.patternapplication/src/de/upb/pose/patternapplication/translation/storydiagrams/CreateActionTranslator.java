/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.CreateAction;

/**
 * @author Dietrich Travkin
 */
public class CreateActionTranslator extends AbstractCreateActionTranslator<CreateAction> {
	
	public CreateActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
}
