/**
 * 
 */
package de.upb.pose.patternapplication.translation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ecore.AttributeTranslator;
import de.upb.pose.patternapplication.translation.ecore.OperationTranslator;
import de.upb.pose.patternapplication.translation.ecore.ParameterTranslator;
import de.upb.pose.patternapplication.translation.ecore.PrimitiveTypeTranslator;
import de.upb.pose.patternapplication.translation.ecore.ReferenceTranslator;
import de.upb.pose.patternapplication.translation.ecore.TypeTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.CallActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.CreateActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.DelegateActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.ParameterAssignmentTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.ProduceActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.RedirectActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.ResultVariableTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.ReturnActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.VariableAccessActionTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.BindReferenceTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.OperationBehaviorTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TargetObjectTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.patternapplication.translation.storydiagrams.token.VariableAllTypesTranslator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.types.PrimitiveType;
import de.upb.pose.specification.types.Type;


/**
 * @author Dietrich Travkin
 */
public class ApplicationModel2EcoreAndSDsTranslator
{
	private static ApplicationModel2EcoreAndSDsTranslator instance = null;
	
	private static ApplicationModel2EcoreAndSDsTranslator get()
	{
		if (instance == null)
		{
			instance = new ApplicationModel2EcoreAndSDsTranslator();
		}
		return instance;
	}
	
	private Map<Class<? extends DesignElement>, DesignElementTranslator<? extends DesignElement, ? extends EObject>> designElementTranslators
		= new HashMap<Class<? extends DesignElement>, DesignElementTranslator<? extends DesignElement, ? extends EObject>>();
	
	private Map<TokenType, TokenTranslator<? extends EObject>> tokenTranslators = new HashMap<TokenType, TokenTranslator<? extends EObject>>();
	
	private Map<DesignElement, RoleBinding> applicationModelElementsToRoleBindingMap = new HashMap<DesignElement, RoleBinding>();
	
	private List<TranslationStep<? extends EObject>> translationStepQueue = new LinkedList<TranslationStep<? extends EObject>>();

	private ApplicationModel2EcoreAndSDsTranslator()
	{
		// ecore
		addTranslator(new TypeTranslator(this)); // incl. type inheritance translator
		addTranslator(new OperationTranslator(this));
		addTranslator(new ReferenceTranslator(this));
		addTranslator(new AttributeTranslator(this));
		addTranslator(new ParameterTranslator(this));
		addTranslator(new PrimitiveTypeTranslator(this));
		
		// story diagrams
		addTranslator(new CallActionTranslator(this));
		addTranslator(new RedirectActionTranslator(this));
		addTranslator(new DelegateActionTranslator(this));
		addTranslator(new CreateActionTranslator(this));
		addTranslator(new ProduceActionTranslator(this));
		addTranslator(new ReturnActionTranslator(this));
		addTranslator(new VariableAccessActionTranslator(this));
		
		addTranslator(new ParameterAssignmentTranslator(this));
		addTranslator(new ResultVariableTranslator(this));
		
		// tokens in story diagrams
		addTranslator(new OperationBehaviorTranslator(this));
		addTranslator(new TargetObjectTranslator(this));
		addTranslator(new VariableAllTypesTranslator(this)); // responsible for all types of variables
		addTranslator(new BindReferenceTranslator(this));
		
		// TODO add others
	}
	
	@SuppressWarnings("unchecked")
	private void addTranslator(ElementTranslator<? extends EObject, ? extends EObject> translator)
	{
		Class<? extends EObject> sourceElementType = translator.getElementToTranslateType();
		if (DesignElement.class.isAssignableFrom(sourceElementType)) {
			this.designElementTranslators.put(
					(Class<? extends DesignElement>) sourceElementType,
					(DesignElementTranslator<? extends DesignElement, ? extends EObject>) translator);
		} else if (Token.class.isAssignableFrom(sourceElementType)) {
			TokenTranslator<? extends EObject> tokenTranslator = (TokenTranslator<? extends EObject>) translator;
			TokenType tokenType = tokenTranslator.getTranslatableTokenType();
			this.tokenTranslators.put(
					tokenType,
					tokenTranslator);
		} else {
			throw new IllegalArgumentException("Unexpected translator type. Only translators for the classes " +
					DesignElement.class.getName() + " and " +
					Token.class.getName() + " are expected.");
		}
	}
	
	
	/**
	 * Returns the element translator registered for the given type.
	 * 
	 * @param type the type (class) S of application model elements to be translated
	 * @return the translator registered for the given type S, if available. <code>null</code> otherwise
	 */
	@SuppressWarnings("unchecked")
	/*package*/ <S extends DesignElement> DesignElementTranslator<S, ? extends EObject> getTranslatorFor(Class<S> type)
	{
		return (DesignElementTranslator<S, ? extends EObject>) designElementTranslators.get(type);
	}
	
	private Class<? extends Action> getActionClass(Action action) {
		Class<? extends Action> actionClass = null;
		
		if (action instanceof DelegateAction) {
			actionClass = DelegateAction.class;
		}
		else if (action instanceof RedirectAction) {
			actionClass = RedirectAction.class;
		}
		else if (action instanceof CallAction) {
			actionClass = CallAction.class;
		}
		else if (action instanceof ReturnAction) {
			actionClass = ReturnAction.class;
		}
		else if (action instanceof ProduceAction) {
			actionClass = ProduceAction.class;
		}
		else if (action instanceof CreateAction) {
			actionClass = CreateAction.class;
		}
		else if (action instanceof VariableAccessAction) {
			actionClass = VariableAccessAction.class;
		}
		return actionClass;
	}
	
	@SuppressWarnings("unchecked")
	/*package*/ DesignElementTranslator<? extends Action, ? extends EObject> getTranslatorFor(Action action)
	{
		Class<? extends Action> actionClass = getActionClass(action);
		
		Assert.isNotNull(action);
		
		return (DesignElementTranslator<? extends Action, ? extends EObject>) designElementTranslators.get(actionClass);
	}
	
	/*package*/ TokenTranslator<? extends EObject> getTranslatorFor(TokenType tokenType)
	{
		return tokenTranslators.get(tokenType);
	}
	
	/*package*/ RoleBinding getRoleBinding(DesignElement applicationModelElement)
	{
		return applicationModelElementsToRoleBindingMap.get(applicationModelElement);
	}
	
	/*package*/ RoleBinding getRoleBinding(Token token)
	{
		Token rootToken = token;
		while (rootToken.getParentToken() != null) {
			rootToken = rootToken.getParentToken();
		}
		
		return rootToken.getParentRoleBinding();
	}
	
	/*package*/ <E extends EObject> void addTranslationStep(E elementToTranslate, ElementTranslator<E, ? extends EObject> translatorToUse)
	{
		Assert.isNotNull(elementToTranslate);
		Assert.isNotNull(translatorToUse);
		
		this.translationStepQueue.add(new TranslationStep<E>(elementToTranslate, translatorToUse));
	}
	
	@SuppressWarnings("unchecked")
	/*package*/ <E extends Action> void addTranslationStep(Action action)
	{
		Assert.isNotNull(action);
		
		E actionToTranslate = (E) action;
		DesignElementTranslator<E, ? extends EObject> translatorToUse = (DesignElementTranslator<E, ? extends EObject>) getTranslatorFor(actionToTranslate);
		
		this.translationStepQueue.add(new TranslationStep<E>(actionToTranslate, translatorToUse));
	}
	
	public static void performPatternApplication(AppliedPattern appliedPattern, ResourceSet designModelResources)
	{
		ApplicationModel2EcoreAndSDsTranslator.get().translateApplicationModel2Ecore(appliedPattern, designModelResources);
	}
	
	private void translateApplicationModel2Ecore(AppliedPattern patternApplication, ResourceSet designModelResources)
	{
		Assert.isNotNull(patternApplication, "No pattern application given to be translated.");
		Assert.isNotNull(designModelResources, "No resource set given to be filled during translation.");
		Assert.isNotNull(patternApplication.getApplicationModel(),
				"The given pattern application has not application model, which is required for translation.");
		
		// translate the application model, element by element, starting with types
		initializeTranslation(patternApplication);
		translate();
	}
	
	private void initializeTranslation(AppliedPattern patternApplication)
	{
		initializeApplicationModelElement2RoleBindingMap(patternApplication);
		initializeTranslationStepQueue(patternApplication);
	}
	
	private void translate() {
		try {
			while (!this.translationStepQueue.isEmpty()) {
				TranslationStep<? extends EObject> translationStep = this.translationStepQueue.get(0);
				this.translationStepQueue.remove(0);
	
				ElementTranslator<? extends EObject, ? extends EObject> translator = translationStep.getTranslator();
				EObject elementToTranslate = translationStep.getElementToTranslate();
				
				if (translator == null) {
					throw new IllegalStateException("No translator found to translate the following element"
							+ "\n\telement type: " + elementToTranslate.getClass().getName()
							+ "\n\telement: " + elementToTranslate.toString());
				}
				
				if (elementToTranslate instanceof PatternElement
						&& !((PatternElement) elementToTranslate).isCanBeGenerated()
						&& !(elementToTranslate instanceof Action)) {
					System.out.println("Ignoring pattern element which cannot be generated according to the specification. Details:"
							+ "\n\telement type: " + elementToTranslate.getClass().getName()
							+ "\n\ttranslator: " + translator.getClass().getName()
							+ "\n\telement: " + elementToTranslate.toString());
					continue;
				}
				
				if (translator.canTranslate(elementToTranslate))
				{
					translator.translate(elementToTranslate);
				}
				else
				{
					if (!atLeastOneTranslationStepCanBePerformed())
					{
						throw new IllegalStateException("Only inapplicable translation steps are pending. Translation incompele.");
					}
					
					// move this step to the end of the list
					this.translationStepQueue.add(translationStep);
				}
			}
		}
		catch (Exception e) {
			// ensure that the real exception is visible
			e.printStackTrace();
		}
	}
	
	private boolean atLeastOneTranslationStepCanBePerformed()
	{
		for (TranslationStep<? extends EObject> step: this.translationStepQueue)
		{
			if (step.getTranslator().canTranslate(step.getElementToTranslate()))
			{
				return true;
			}
		}
		return false;
	}
	
	private void initializeApplicationModelElement2RoleBindingMap(AppliedPattern patternApplication)
	{
		this.applicationModelElementsToRoleBindingMap.clear();
		
		for (RoleBinding roleBinding: patternApplication.getRoleBindings())
		{
			if (roleBinding.getApplicationModelElement() != null)
			{
				this.applicationModelElementsToRoleBindingMap.put(roleBinding.getApplicationModelElement(), roleBinding);
			}
		}
	}
	
	private void initializeTranslationStepQueue(AppliedPattern patternApplication)
	{
		Assert.isNotNull(patternApplication.getParent(), "No pattern applications root node available for the given pattern application.");
		EObject designModelRoot = patternApplication.getParent().getDesignModelRoot();
		Assert.isNotNull(designModelRoot, "There is no design model root for the given pattern application.");
		
		this.translationStepQueue.clear();
		
		for (RoleBinding roleBinding: patternApplication.getRoleBindings())
		{
			DesignElement applicationModelElement = roleBinding.getApplicationModelElement();
			if (applicationModelElement instanceof Type)
			{
				if (applicationModelElement instanceof PrimitiveType) {
					ElementTranslator<PrimitiveType, ? extends EObject> typeTranslator = getTranslatorFor(PrimitiveType.class);
					addTranslationStep((PrimitiveType) applicationModelElement, typeTranslator);
				} else {
					ElementTranslator<Type, ? extends EObject> typeTranslator = getTranslatorFor(Type.class);
					addTranslationStep((Type) applicationModelElement, typeTranslator);
				}
			}
		}
	}
	
	private static class TranslationStep<T extends EObject>
	{
		private final T elementToTranslate;
		private final ElementTranslator<T, ? extends EObject> translator;
		
		public TranslationStep(T elementToTranslate, ElementTranslator<T, ? extends EObject> translator) {
			this.elementToTranslate = elementToTranslate;
			this.translator = translator;
		}
		
		public T getElementToTranslate() {
			return this.elementToTranslate;
		}
		
		public ElementTranslator<T, ? extends EObject> getTranslator() {
			return this.translator;
		}
	}
	
}
