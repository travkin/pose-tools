/**
 * 
 */
package de.upb.pose.patternapplication.translation.ecore;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EcoreFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.specification.types.Parameter;

/**
 * @author Dietrich Travkin
 */
public class ParameterTranslator extends DesignElementTranslator<Parameter, EParameter> {

	public ParameterTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	private Parameter findPredecessor(Parameter parameter) {
		Assert.isNotNull(parameter);
		Assert.isNotNull(parameter.getOperation());
		
		List<Parameter> allParameters = parameter.getOperation().getParameters();
		
		if (allParameters.size() > 1) {
			int paramIndex= allParameters.indexOf(parameter);
			
			Assert.isTrue(paramIndex != -1); // parameter is found
			
			int predecessorIndex = paramIndex - 1;
			if (predecessorIndex >= 0) {
				Parameter predecessor = allParameters.get(predecessorIndex);
				
				Assert.isNotNull(predecessor);
				
				return predecessor;
			}
		}
		
		return null;
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Parameter parameter, RoleBinding roleBinding) {
		// parent operation is translated
		boolean preconditionSatisfied = parameter.getOperation() != null
				&& this.isApplicationModelElementMappedToTargetElements(parameter.getOperation());
	
		// parameter type is translated
		preconditionSatisfied = preconditionSatisfied
				&& parameter.getType() != null
				&& this.isApplicationModelElementMappedToTargetElements(parameter.getType());
		
		// predecessor parameter is translated
		Parameter predecessor = findPredecessor(parameter);
		if (predecessor != null) {
			preconditionSatisfied = preconditionSatisfied && this.isApplicationModelElementMappedToTargetElements(predecessor); 
		}
		
		return preconditionSatisfied;  
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Parameter parameter, RoleBinding roleBinding) {
		EOperation parentOperation = (EOperation) this.getAllTargetElementsFor(parameter.getOperation()).get(0);
		EClassifier parameterType = (EClassifier) this.getAllTargetElementsFor(parameter.getType()).get(0);
		
		EParameter mappedParameter = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			mappedParameter = EcoreFactory.eINSTANCE.createEParameter();
			parentOperation.getEParameters().add(mappedParameter);
			
			mappedParameter.setName(parameter.getName());
			
			mappedParameter.setEType(parameterType);
			
		} else {
			mappedParameter = (EParameter) roleBinding.getModelElements().get(0);
			
			// TODO perform correspondence check
		}
		
		return wrapInList(mappedParameter);
	}

	
}
