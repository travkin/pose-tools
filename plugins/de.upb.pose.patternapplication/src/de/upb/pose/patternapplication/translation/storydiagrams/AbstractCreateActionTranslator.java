/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.ModifyingStoryNode;
import org.storydriven.storydiagrams.patterns.BindingOperator;
import org.storydriven.storydiagrams.patterns.BindingSemantics;
import org.storydriven.storydiagrams.patterns.BindingState;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.PatternsFactory;
import org.storydriven.storydiagrams.patterns.StoryPattern;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractCreateActionTranslator<A extends CreateAction> extends AbstractActionTranslator<A, ModifyingStoryNode> {
	
	public AbstractCreateActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	@Override
	protected Class<ModifyingStoryNode> getTranslationResultType() {
		return ModifyingStoryNode.class;
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.storydiagrams.AbstractActionTranslator#isTranslationPreconditionSatisfied(de.upb.pose.specification.actions.Action, de.upb.pose.mapping.RoleBinding)
	 */
	protected boolean isTranslationPreconditionSatisfied(A action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		
		if (action.isCanBeGenerated()) {
			preconditionSatisfied = preconditionSatisfied && this.isApplicationModelElementMappedToTargetElements(action.getInstantiatedType()); 
		}
		
		return preconditionSatisfied; 
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.ElementTranslator#translate(de.upb.pose.specification.DesignElement, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(A action, RoleBinding roleBinding) {
		Activity parentActivity = this.getParentActivity(action);
		Type typeToInstantiate = action.getInstantiatedType();
		EClass eClassToInstantiate = typeToInstantiate != null ? (EClass) this.getAllTargetElementsFor(typeToInstantiate).get(0) : null;
		
		ModifyingStoryNode mappedModifyingStoryNode = null;
		
		if (roleBinding.getModelElements().isEmpty()) {
			List<EObject> createdElements = new LinkedList<EObject>();
			
			mappedModifyingStoryNode = this.createNode(parentActivity, createdElements);
			ActivityEdge outgoingEdge = this.createOutgoingEdge(mappedModifyingStoryNode, parentActivity, createdElements);
			
			Token rootControlFlowToken = this.createControlFlowToken(roleBinding, createdElements);
			
			StoryPattern storyPattern = PatternsFactory.eINSTANCE.createStoryPattern();
			mappedModifyingStoryNode.setOwnedRule(storyPattern);
			createdElements.add(storyPattern);
			
			ObjectVariable objectVariable = PatternsFactory.eINSTANCE.createObjectVariable();
			objectVariable.setBindingState(BindingState.UNBOUND);
			objectVariable.setBindingSemantics(BindingSemantics.MANDATORY);
			objectVariable.setBindingOperator(BindingOperator.CREATE);
			storyPattern.getVariables().add(objectVariable);
			createdElements.add(objectVariable);
			
			if (eClassToInstantiate != null) {
				objectVariable.setName("new" + eClassToInstantiate.getName());
				objectVariable.setType(eClassToInstantiate);
			}
			
			mappedModifyingStoryNode.getDeclaredVariables().add(objectVariable);
			
			appendNewNodeToControlFlow(rootControlFlowToken, mappedModifyingStoryNode);
			setLastEdgeInControlflow(rootControlFlowToken, outgoingEdge);
			
			if (action.isCanBeGenerated()) {
				Token creationToken = this.createCreationToken(rootControlFlowToken, createdElements);
				
				creationToken.getMapsTo().put(TokenMapKey.CREATION__NODE.getKey(), mappedModifyingStoryNode);
				creationToken.getMapsTo().put(TokenMapKey.CREATION__EDGE.getKey(), outgoingEdge);
				creationToken.getMapsTo().put(TokenMapKey.CREATION__VARIABLE.getKey(), objectVariable);
				
				mappedModifyingStoryNode.setName("instantiate " + eClassToInstantiate.getName());
				
				if (action.getResultVariable() != null) {
					objectVariable.setName(action.getResultVariable().getName());
					
					DesignElementTranslator<ResultVariable, ? extends EObject> translator = this.getTranslatorFor(ResultVariable.class);
					this.addTranslationStep(action.getResultVariable(), translator);
				}
			} else {
				mappedModifyingStoryNode.setName("ToDo");
			}
			
			return createdElements;
		} else {
			mappedModifyingStoryNode = (ModifyingStoryNode) roleBinding.getModelElements().get(0);
			
			// TODO correspondence check
		}
		
		return wrapInList(mappedModifyingStoryNode);
	}
	
	private ModifyingStoryNode createNode(Activity parentActivity, List<EObject> createdElements) {
		ModifyingStoryNode newModifyingStoryNode = ActivitiesFactory.eINSTANCE.createModifyingStoryNode();
		parentActivity.getOwnedActivityNodes().add(newModifyingStoryNode);
		createdElements.add(newModifyingStoryNode);
		return newModifyingStoryNode;
	}
	
	private Token createCreationToken(Token rootControlFlowToken, List<EObject> createdElements) {
		Assert.isTrue(TokenType.complyWith(rootControlFlowToken, TokenType.CONTROL_FLOW));
		
		Token creationToken = TokenType.createToken(TokenType.CREATION);
		rootControlFlowToken.getSubTokens().add(creationToken);
		createdElements.add(creationToken);
		
		return creationToken;
	}
	
}
