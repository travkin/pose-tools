/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.ThisVariableExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class VariableSelfTranslator extends AbstractVariableTranslator<ThisVariableExpression> {

	public VariableSelfTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof SelfVariable);
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		
		Operation parentOperation = getParentOperation(roleBinding);
		Type parentType = parentOperation.getParentType();
		
		Assert.isNotNull(parentType);
		
		EClass parentEClass = (EClass) this.getAllTargetElementsFor(parentType).get(0);
		
		Assert.isNotNull(parentEClass);
		
		ThisVariableExpression mappedExpression = CallsExpressionsFactory.eINSTANCE.createThisVariableExpression();
		mappedExpression.setType(parentEClass);
		
		token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), mappedExpression);
				
		return wrapInList(mappedExpression);
	}

}
