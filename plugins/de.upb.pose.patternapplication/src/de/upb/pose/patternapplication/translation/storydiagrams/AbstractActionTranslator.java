/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.ActivitiesFactory;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.ActivityEdge;
import org.storydriven.storydiagrams.activities.ActivityNode;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.patternapplication.translation.DesignElementTranslator;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenMapKey;
import de.upb.pose.patternapplication.translation.storydiagrams.token.TokenType;
import de.upb.pose.specification.actions.Action;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractActionTranslator<A extends Action, T extends ActivityNode> extends DesignElementTranslator<A, T>
{
	public AbstractActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	protected boolean isTranslationPreconditionSatisfied(A action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = this.isApplicationModelElementMappedToTargetElements(action.getOperation())
				&& this.getParentActivity(action) != null;
		
		// check if the predecessor actions have been translated
		List<Action> allActions = action.getOperation().getActions(); 
		if (allActions.size() > 1) {
			int actionIndex = allActions.indexOf(action);
			
			Assert.isTrue(actionIndex != -1); // action is found
			
			int predecessorIndex = actionIndex - 1;
			if (predecessorIndex >= 0) {
				Action predecessor = allActions.get(predecessorIndex);
				
				Assert.isNotNull(predecessor);
				
				preconditionSatisfied = preconditionSatisfied && this.isApplicationModelElementMappedToTargetElements(predecessor);
			}
		}
		
		return preconditionSatisfied;
	}
	
	protected final Activity getParentActivity(A action) {
		List<? extends EObject> mappedOperationElements = this.getAllTargetElementsFor(action.getOperation());
		if (!mappedOperationElements.isEmpty()) {
			EOperation parentEOperation = (EOperation) mappedOperationElements.get(0);
			return StoryDiagramsEcoreConnector.getLinkedActivity(parentEOperation);
		}
		return null;
	}
	
	
	protected final Token createControlFlowToken(RoleBinding actionRoleBinding, List<EObject> createdElements) {
		Assert.isTrue(actionRoleBinding.getRootToken() == null);
		
		Token rootControlFlowToken = TokenType.createToken(TokenType.CONTROL_FLOW);
		actionRoleBinding.setRootToken(rootControlFlowToken);
		createdElements.add(rootControlFlowToken);
		
		return rootControlFlowToken;
	}
	
	protected final ActivityEdge createOutgoingEdge(ActivityNode sourceNode, Activity parentActivity, List<EObject> createdElements) {
		ActivityEdge outgoingEdge = ActivitiesFactory.eINSTANCE.createActivityEdge();
		parentActivity.getOwnedActivityEdges().add(outgoingEdge);
		outgoingEdge.setSource(sourceNode);
		createdElements.add(outgoingEdge);
		return outgoingEdge;
	}
	
	protected final void appendNewNodeToControlFlow(Token controlFlowToken, ActivityNode newNode) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		
		ActivityNode node = (ActivityNode) controlFlowToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey());
		if (node == null) {
			controlFlowToken.getMapsTo().put(TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey(), newNode);
		} else {
			node = findTheLastNodeInControlFlow(controlFlowToken);
			ActivityEdge outgoingEdge = getDefaultoutgoingEdge(node);
			outgoingEdge.setTarget(newNode);
		}
	}
	
	protected final void setLastEdgeInControlflow(Token controlFlowToken, ActivityEdge lastEdge) {
		controlFlowToken.getMapsTo().put(TokenMapKey.CONTROL_FLOW__LAST_EDGE.getKey(), lastEdge);
	}
	
	protected final ActivityNode findTheLastNodeInControlFlow(Token controlFlowToken) {
		Assert.isTrue(TokenType.complyWith(controlFlowToken, TokenType.CONTROL_FLOW));
		
		ActivityNode node = (ActivityNode) controlFlowToken.getMapsTo().get(TokenMapKey.CONTROL_FLOW__FIRST_NODE.getKey());
		ActivityEdge outgoingEdge = getDefaultoutgoingEdge(node);
		
		while (outgoingEdge.getTarget() != null) {
			node = outgoingEdge.getTarget();
			outgoingEdge = getDefaultoutgoingEdge(node);
		}
		
		return node;
	}
	
	protected ActivityEdge getDefaultoutgoingEdge(ActivityNode sourceNode) {
		Assert.isTrue(sourceNode.getOutgoings().size() == 1);
		return sourceNode.getOutgoings().get(0);
	}
	
}
