/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.CallAction;

/**
 * @author Dietrich Travkin
 */
public class CallActionTranslator extends AbstractCallActionTranslator<CallAction> {
	
	public CallActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
}
