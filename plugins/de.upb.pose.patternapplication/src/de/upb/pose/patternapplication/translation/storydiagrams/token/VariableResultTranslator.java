/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams.token;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EObject;
import org.storydriven.core.expressions.Expression;
import org.storydriven.storydiagrams.calls.InvocationResultVariable;
import org.storydriven.storydiagrams.calls.OutParameterBinding;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.InvocationResultVariableExpression;
import org.storydriven.storydiagrams.patterns.ObjectVariable;
import org.storydriven.storydiagrams.patterns.expressions.ObjectVariableExpression;
import org.storydriven.storydiagrams.patterns.expressions.PatternsExpressionsFactory;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.Variable;

/**
 * @author Dietrich Travkin
 */
public class VariableResultTranslator extends AbstractVariableTranslator<Expression> {

	public VariableResultTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}

	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#isTranslationPreconditionSatisfied(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected boolean isTranslationPreconditionSatisfied(Token token, RoleBinding roleBinding) {
		Variable var = getVariable(token);
		
		Assert.isTrue(var instanceof ResultVariable);
		
		ResultVariable resultVariable = (ResultVariable) var;
		
		return super.isTranslationPreconditionSatisfied(token, roleBinding)
				&& this.isApplicationModelElementMappedToTargetElements(resultVariable);
	}
	
	/**
	 * @see de.upb.pose.patternapplication.translation.TokenTranslator#translate(de.upb.pose.mapping.Token, de.upb.pose.mapping.RoleBinding)
	 */
	@Override
	protected List<EObject> translate(Token token, RoleBinding roleBinding) {
		ResultVariable resultVariable = (ResultVariable) getVariable(token);
		EObject eObjectMappedToResultVariable = this.getAllTargetElementsFor(resultVariable).get(0);
		
		Expression resultVariableExpression = null;
		
		if (eObjectMappedToResultVariable instanceof OutParameterBinding) {
			OutParameterBinding outParameterBinding = (OutParameterBinding) eObjectMappedToResultVariable;
			InvocationResultVariable invocationResultVariable = outParameterBinding.getResultVariable();
			
			token.getMapsTo().put(TokenMapKey.VARIABLE__VARIABLE.getKey(), invocationResultVariable);
			
			InvocationResultVariableExpression invocationResultVariableExpression = CallsExpressionsFactory.eINSTANCE.createInvocationResultVariableExpression();
			invocationResultVariableExpression.setResultVariable(invocationResultVariable);
			
			token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), invocationResultVariableExpression);
			
			resultVariableExpression = invocationResultVariableExpression;
		}
		else if (eObjectMappedToResultVariable instanceof ObjectVariable) {
			ObjectVariable objectVariable = (ObjectVariable) eObjectMappedToResultVariable;
			
			token.getMapsTo().put(TokenMapKey.VARIABLE__VARIABLE.getKey(), objectVariable);
			
			ObjectVariableExpression objectVariableExpression = PatternsExpressionsFactory.eINSTANCE.createObjectVariableExpression();
			objectVariableExpression.setObject(objectVariable);
			
			token.getMapsTo().put(TokenMapKey.VARIABLE__EXPRESSION.getKey(), objectVariableExpression);
			
			resultVariableExpression = objectVariableExpression;
		}
		
		Assert.isNotNull(resultVariableExpression);
				
		return wrapInList(resultVariableExpression);
	}

}
