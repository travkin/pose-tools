/**
 * 
 */
package de.upb.pose.patternapplication.translation.storydiagrams;

import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.activities.Activity;
import org.storydriven.storydiagrams.activities.StatementNode;
import org.storydriven.storydiagrams.calls.CallsFactory;
import org.storydriven.storydiagrams.calls.InParameterBinding;
import org.storydriven.storydiagrams.calls.ParameterExtension;
import org.storydriven.storydiagrams.calls.expressions.CallsExpressionsFactory;
import org.storydriven.storydiagrams.calls.expressions.MethodCallExpression;
import org.storydriven.storydiagrams.calls.expressions.ParameterExpression;

import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.util.TypeRelationsChecker;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractRedirectActionTranslator<A extends RedirectAction> extends AbstractCallActionTranslator<A> {
	
	public AbstractRedirectActionTranslator(ApplicationModel2EcoreAndSDsTranslator parent) {
		super(parent);
	}
	
	protected boolean isTranslationPreconditionSatisfied(A action, RoleBinding actionRoleBinding) {
		boolean preconditionSatisfied = super.isTranslationPreconditionSatisfied(action, actionRoleBinding);
		
		// no parameter assignments allowed for redirect actions
		Assert.isTrue(action.getAssignments().isEmpty());
		
		EOperation callerEOperation = (EOperation) this.getAllTargetElementsFor(action.getOperation()).get(0);
		EOperation calleeEOperation = (EOperation) this.getAllTargetElementsFor(action.getCalledOperation()).get(0);
		
		// caller and callee parameters are compliant to each other, cancel otherwise
		
		// pattern specification side:
		if (!action.getOperation().getParameters().isEmpty() || !action.getCalledOperation().getParameters().isEmpty()) {
			Assert.isTrue(action.getOperation().getParameters().size() == action.getCalledOperation().getParameters().size());
			
			for (int i = 0; i < action.getOperation().getParameters().size(); i++) {
				Type callerParameterType = action.getOperation().getParameters().get(i).getType();
				Type calleeParameterType = action.getCalledOperation().getParameters().get(i).getType();
			
				Assert.isTrue(TypeRelationsChecker.isEqualOrSubtypeOf(callerParameterType, calleeParameterType));
			}
		}
		
		// ecore model side:
		if (!callerEOperation.getEParameters().isEmpty() || !calleeEOperation.getEParameters().isEmpty()) {
			Assert.isTrue(callerEOperation.getEParameters().size() == calleeEOperation.getEParameters().size());
			
			for (int i = 0; i < callerEOperation.getEParameters().size(); i++) {
				EClassifier callerParameterType = callerEOperation.getEParameters().get(i).getEType();
				EClassifier calleeParameterType = calleeEOperation.getEParameters().get(i).getEType();
				
				if (!(calleeParameterType instanceof EClass)) {
					Assert.isTrue(callerParameterType.equals(calleeParameterType));
				} else {
					Assert.isTrue(callerParameterType instanceof EClass);
					
					EClass callerParameterEClass = (EClass) callerParameterType;
					EClass calleeParameterEClass = (EClass) calleeParameterType;
					
					Assert.isTrue(calleeParameterEClass.isSuperTypeOf(callerParameterEClass));
				}
			}
		}
		
		return preconditionSatisfied; 
	}
	
	@Override
	protected List<EObject> translate(A action, RoleBinding roleBinding) {
		List<EObject> createdElements = super.translate(action, roleBinding);
		Activity parentActivity = this.getParentActivity(action);
		
		StatementNode mappedStatementNode = (StatementNode) createdElements.get(0);
		MethodCallExpression methodCallExpression = (MethodCallExpression) mappedStatementNode.getStatementExpression();
		
		EOperation callerEOperation = (EOperation) this.getAllTargetElementsFor(action.getOperation()).get(0);
		EOperation calleeEOperation = (EOperation) this.getAllTargetElementsFor(action.getCalledOperation()).get(0);
		
		for (int i = 0; i < calleeEOperation.getEParameters().size(); i++) {
			EParameter calleeParameter = calleeEOperation.getEParameters().get(i);
			EParameter callerParameter = callerEOperation.getEParameters().get(i);
			
			InParameterBinding parameterBinding = CallsFactory.eINSTANCE.createInParameterBinding();
			parameterBinding.setParameter(calleeParameter);
			
			ParameterExtension callerParameterExtension = StoryDiagramsEcoreConnector.provideParameterExtension(callerParameter, parentActivity);
			
			ParameterExpression callArgumentExpression = CallsExpressionsFactory.eINSTANCE.createParameterExpression();
			callArgumentExpression.setParameter(callerParameterExtension);
			
			parameterBinding.setValueExpression(callArgumentExpression);
			
			methodCallExpression.getOwnedParameterBindings().add(parameterBinding);
			createdElements.add(parameterBinding);
		}
		
		return createdElements;
	}
	
}
