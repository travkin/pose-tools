package de.upb.pose.patternapplication;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.ModelExtent;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.Log;

import de.upb.pose.mapping.AppliedPattern;

@Deprecated
public class QVTOPatternApplicator {
	
	public void execute(Resource ecoreModelResource, AppliedPattern pattern) {
		assert ecoreModelResource != null;
		assert pattern != null;
		assert pattern.getApplicationModel() != null;

		deriveEcoreModelFromApplicationModel(pattern, ecoreModelResource);
	}
	
	private void deriveEcoreModelFromApplicationModel(AppliedPattern appliedPattern, Resource ecoreModel)
	{
		// TODO if required, choose the QVT-O transformation according to the
		// chosen pattern variant

		// for testing purposes take the apply Observer pattern transformation
		URI transformationURI = URI.createURI("platform:/plugin/de.upb.pose.patternapplication/transforms/ApplicationModel2Ecore.qvto");
		TransformationExecutor executor = new TransformationExecutor(transformationURI);

		// define the transformation arguments
		List<EObject> objects = new ArrayList<EObject>(1);
		//objects.add(appliedPattern.getPatternSpecification());
		//ModelExtent patternSpecificationExtent = new BasicModelExtent(objects);
		//ModelExtent ecoreModelExtent = new BasicModelExtent(ecoreModel.getContents());
		//objects = new ArrayList<EObject>(1);
		objects.add(appliedPattern);
		ModelExtent appliedPatternExtent = new BasicModelExtent(objects);

		// setup the execution environment details -> configuration properties, logger, monitor object etc.
		ExecutionContextImpl context = new ExecutionContextImpl();
		context.setLog(new Log()
			{
				@Override
				public void log(int level, String message, Object param)
				{
					log(message);
				}
				@Override
				public void log(int level, String message)
				{
					log(message);
				}

				@Override
				public void log(String message, Object param)
				{
					log(message);
				}

				@Override
				public void log(String message)
				{
					System.out.println(message);
				}
			});
		// TODO set the actual execution environment properties
		// context.setConfigProperty("keepModeling", true);

		// run the transformation assigned to the executor with the given
		// input and output and execution context
		//ExecutionDiagnostic result = executor.execute(context, patternSpecificationExtent, ecoreModelExtent, appliedPatternExtent);
		ExecutionDiagnostic result = executor.execute(context, appliedPatternExtent);

		// TODO properly handle errors during execution
		// check the result for success
		if (result.getSeverity() == Diagnostic.OK)
		{
			System.out.println("Transformation successfully finished.");
//			// the output objects got captured in the output extent
//			List<EObject> outObjects = ecoreModelExtent.getContents();
//			// let's persist them using a resource
//			ResourceSet resourceSet2 = new ResourceSetImpl();
//			Resource outResource = resourceSet2
//					.getResource(
//							URI.createURI("platform:/resource/myqvtprj/tomorrow.betterWorld"),
//							true);
//			outResource.getContents().addAll(outObjects);
//			outResource.save(Collections.emptyMap());
			
			// TODO save the modifications or refresh the view?
		} else
		{
			System.err.println("Transformation unsuccessful.");
			System.err.println(result.getMessage());
//			// turn the result diagnostic into status and send it to error log
//			IStatus status = BasicDiagnostic.toIStatus(result);
//			Activator.get().getLog().log(status);
		}
	}

	@Deprecated
	private void test(EPackage ePackage) {
		EClass a = addClass(ePackage, "ClassA");
		addAttribute(a, "first", EcorePackage.Literals.ESTRING);
		addAttribute(a, "second", EcorePackage.Literals.EBOOLEAN);
		addOperation(a, "doSome", EcorePackage.Literals.ESTRING);
		addOperation(a, "doSomeOther", null);

		EClass b = addClass(ePackage, "ClassB");
		EClass c = addClass(ePackage, "ClassC");
		c.getESuperTypes().add(b);

		addReference(a, b, "fromA2B");
		addReference(a, c, "fromA2C");
	}

	private EClass addClass(EPackage ePackage, String name) {
		EClass element = EcoreFactory.eINSTANCE.createEClass();
		element.setName(name);
		ePackage.getEClassifiers().add(element);
		return element;
	}

	private EAttribute addAttribute(EClass eClass, String name, EClassifier classifier) {
		EAttribute element = EcoreFactory.eINSTANCE.createEAttribute();
		element.setName(name);
		element.setEType(classifier);
		eClass.getEStructuralFeatures().add(element);
		return element;
	}

	private EOperation addOperation(EClass eClass, String name, EClassifier classifier) {
		EOperation element = EcoreFactory.eINSTANCE.createEOperation();
		element.setName(name);
		element.setEType(classifier);
		eClass.getEOperations().add(element);
		return element;
	}

	private EReference addReference(EClass source, EClass target, String name) {
		EReference element = EcoreFactory.eINSTANCE.createEReference();
		element.setEType(target);
		element.setName(name);
		source.getEStructuralFeatures().add(element);
		return element;
	}
}
