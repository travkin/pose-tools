/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingFactory;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.mapping.Task;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.TaskDescription;

/**
 * The main class that implements the algorithm that derives an application model from a given mapping.
 * The application model is created by step-wise deriving design elements from pattern specification elements
 * by means of a chain of responsibility built of {@link ApplicationModelElementCreator}s.
 * 
 * @author Dietrich Travkin
 */
public class ApplicationModelCreator {
	
	private static ApplicationModelCreator instance = null;
	
	public static ApplicationModelCreator get() {
		if (instance == null) {
			instance = new ApplicationModelCreator();
		}
		return instance;
	}

	private final Map<DesignElement, List<RoleBinding>> patternRole2RoleBindingsMap = new HashMap<DesignElement, List<RoleBinding>>();
	
	private final ApplicationModelElementCreator elementCreator = new ApplicationModelElementCreator(this);
	
	private ApplicationModelCreator() {
	}
	
	public ApplicationModelElementCreator getApplicationModelElementCreator() {
		return this.elementCreator;
	}
	
	public List<RoleBinding> findRoleBindingsForPatternRole(DesignElement patternRole) {
		List<RoleBinding> result = this.patternRole2RoleBindingsMap.get(patternRole);
		
		if (result == null) {
			return Collections.emptyList();
		}
		return result;
	}
	
	private void addRolebindingForPatternRoleToInternalMap(DesignElement patternRole, RoleBinding roleBinding) {
		Assert.isNotNull(patternRole);
		Assert.isNotNull(roleBinding);
		
		List<RoleBinding> roleBindingsList = this.findRoleBindingsForPatternRole(patternRole); 
		if (roleBindingsList.isEmpty() && !(roleBindingsList instanceof LinkedList)) {
			roleBindingsList = new LinkedList<RoleBinding>();
			this.patternRole2RoleBindingsMap.put(patternRole, roleBindingsList);
		}
		
		roleBindingsList.add(roleBinding);
	}
	
	/**
	 * Initialization of the application model, i.e. a new (initial) application model is created
	 * in correspondence to the given pattern application's existing role bindings.
	 * 
	 * @param patternApplicaiton
	 * @return the created (or completed) initial application model
	 */
	public static ApplicationModel createInitialApplicationModelFromMapping(AppliedPattern patternApplication) {
		ApplicationModelCreator creator = ApplicationModelCreator.get();
		creator.patternRole2RoleBindingsMap.clear();
		
		// ### step 1 ###
		// create an empty application model
		ApplicationModel applicationModel = creator.createEmptyApplicationModel(patternApplication);
		
		// ### step 2 ###
		// create application model elements for all existing role bindings (pattern roles)
		creator.createMissingApplicationModelElementsForRoleBindings(patternApplication);
		
		// ### step 3 ###
		// create application model element references as specified in the pattern
		creator.createApplicationModelElementRelations(patternApplication);
		
		// ### step 4 ###
		// create tasks
		creator.createApplicationModelElementTasksToRoleBindings(patternApplication);

		return applicationModel;
	}
	
	/**
	 * Creation of an additional set fragment instance in the existing pattern application and application model.
	 * The new set fragment instance is created in analogously to the given exemplary set fragment instance.
	 * 
	 * @param setFragmentInstanceExampleToCopy the exemplary set fragment instance to be "copied"
	 * @return the created set fragment instance
	 */
	public static SetFragmentInstance addSetFragmentInstance(SetFragmentInstance setFragmentInstanceExampleToCopy) {
		ApplicationModelCreator creator = ApplicationModelCreator.get();
		
		//return creator.addSetFragmentInstanceFirstImpl(setFragmentInstanceExampleToCopy);
		return creator.addSetFragmentInstanceNewImpl(setFragmentInstanceExampleToCopy);
	}
	
	private SetFragmentInstance addSetFragmentInstanceNewImpl(SetFragmentInstance setFragmentInstanceExampleToCopy) {
		// temporary map of role bindings to copy and their copies
		Map<RoleBinding, RoleBinding> originalRoleBindingToRoleBindingCopyMap = new HashMap<RoleBinding, RoleBinding>();
		
		// recursively replicate the given set fragment instance
		SetFragmentInstance newSetFragmentInstance = recursivelyCopySetFragmentInstance(
				setFragmentInstanceExampleToCopy, setFragmentInstanceExampleToCopy, originalRoleBindingToRoleBindingCopyMap);
		
		// recursively replicate the relations in the given set fragment instance and create them in the new set fragment instance
		recursivelyCopyRelationsInSetFragmentInstance(
				setFragmentInstanceExampleToCopy, setFragmentInstanceExampleToCopy, originalRoleBindingToRoleBindingCopyMap);
		
		// add the new set fragment instance to the same parent set fragment instances as those of the given set fragment instance
		for (SetFragmentInstance containingSetFragmentInstance: setFragmentInstanceExampleToCopy.getContainingSetFragmentInstances()) {
			containingSetFragmentInstance.getContainedSetFragmentInstances().add(newSetFragmentInstance);
		}
		
		return newSetFragmentInstance;
	}
	
	private SetFragmentInstance recursivelyCopySetFragmentInstance(
			SetFragmentInstance setFragmentInstanceToCopyNow, // j
			SetFragmentInstance rootSetFragmentInstanceToCopy, // i
			Map<RoleBinding, RoleBinding> originalRoleBindingToRoleBindingCopyMap) { // img_V
		
		// create a new set fragment instance typed like setFragmentInstanceToCopyNow
		SetFragmentBinding setFragmentBinding = setFragmentInstanceToCopyNow.getParentSetFragmentBinding();
		SetFragmentInstance newSetFragmentInstance = SetFragmentBindingCreator.createSetFragmentInstance(setFragmentBinding); // j'
		
		for (SetFragmentInstance containedInstance: setFragmentInstanceToCopyNow.getContainedSetFragmentInstances()) { // l
			// recursively copy the contained set fragment instance
			SetFragmentInstance copyOfContainedInstance = recursivelyCopySetFragmentInstance( // l'
					containedInstance, rootSetFragmentInstanceToCopy, originalRoleBindingToRoleBindingCopyMap);
			
			// copy containment
			newSetFragmentInstance.getContainedSetFragmentInstances().add(copyOfContainedInstance);
			
			// insert the copyOfContainedInstance into the not copied set fragment instance parent of containedInstance
			for (SetFragmentInstance parentOfContainedInstance: containedInstance.getContainingSetFragmentInstances()) { // k
				if (parentOfContainedInstance != setFragmentInstanceToCopyNow // k != j
						&& parentOfContainedInstance != rootSetFragmentInstanceToCopy // k != i
						&& !in(rootSetFragmentInstanceToCopy, parentOfContainedInstance)) { // not in(i, k)
					parentOfContainedInstance.getContainedSetFragmentInstances().add(copyOfContainedInstance);
				}
			}
		}
		
		for (RoleBinding roleBinding: setFragmentInstanceToCopyNow.getContainedRoleBindings()) { // r
			// copy correspondence node contained in the set fragment instance to be copied
			RoleBinding newRoleBinding = RoleMapper.createRoleBindingFor( // r'
					roleBinding.getRole(), roleBinding.getAppliedPattern());
			
			// copy application model element contained in the set fragment instance to be copied
			@SuppressWarnings("unused")
			DesignElement applicationModelElementToCopy = roleBinding.getApplicationModelElement(); // v
			@SuppressWarnings("unused")
			DesignElement newApplicationModelElement = createApplicationModelElementForRoleBinding(newRoleBinding); // v' 
			
			// temporarily save the mapping of copied role binding to the role binding copy
			// and accordingly save the mapping of the copied application model element to its copy
			originalRoleBindingToRoleBindingCopyMap.put(roleBinding, newRoleBinding); // r --> r' (and v --> v')
			
			// add the role binding copy (and the application model element copy) to the newSetFragmentInstance
			newSetFragmentInstance.getContainedRoleBindings().add(newRoleBinding);
			
			// insert the newRoleBinding into the not copied set fragment instance parent of roleBinding
			for (SetFragmentInstance parentOfRoleBinding: roleBinding.getContainingSetFragmentInstances()) { // k
				if (parentOfRoleBinding != setFragmentInstanceToCopyNow // k != j
						&& parentOfRoleBinding != rootSetFragmentInstanceToCopy // k != i
						&& !in(rootSetFragmentInstanceToCopy, parentOfRoleBinding)) { // not in(i, k)
					parentOfRoleBinding.getContainedRoleBindings().add(newRoleBinding);
				}
			}
			
			// create the tasks for the new role binding according to the pattern specification
			createTasksForRoleBinding(newRoleBinding);
		}
		
		return newSetFragmentInstance;
	}
	
	private void recursivelyCopyRelationsInSetFragmentInstance(
			SetFragmentInstance setFragmentInstanceToCopyNow, // j
			SetFragmentInstance rootSetFragmentInstanceToCopy, // i
			Map<RoleBinding, RoleBinding> originalRoleBindingToRoleBindingCopyMap) { // img_V
		
		for (SetFragmentInstance containedInstance: setFragmentInstanceToCopyNow.getContainedSetFragmentInstances()) { // l
			// recursively copy all relations contained in the containedInstance
			recursivelyCopyRelationsInSetFragmentInstance(containedInstance, rootSetFragmentInstanceToCopy, originalRoleBindingToRoleBindingCopyMap);
		}
		
		// run through all nodes contained in setFragmentInstanceToCopyNow and through their outgoing and incoming relations (edges)
		for (RoleBinding r1: setFragmentInstanceToCopyNow.getContainedRoleBindings()) {
			DesignElement v1 = r1.getApplicationModelElement();
			RoleBinding r1_copy = originalRoleBindingToRoleBindingCopyMap.get(r1);
			DesignElement v1_copy = r1_copy.getApplicationModelElement();
			
			// run through all outgoing relations of v1
			List<Relation> v1_outgoingRelations = this.elementCreator.collectOutgoingDesignElementRelationsFor(v1);
			for (Relation e_out: v1_outgoingRelations) {
				DesignElement v2 = e_out.getTargetElement();
				RoleBinding r2 = findRoleBindingForApplicationModelElement(v2, r1.getAppliedPattern());
				
				DesignElement e_out_copy_source = v1_copy;
				DesignElement e_out_copy_target;
				
				if (in(rootSetFragmentInstanceToCopy, r2)) {
					RoleBinding r2_copy = originalRoleBindingToRoleBindingCopyMap.get(r2);
					DesignElement v2_copy = r2_copy.getApplicationModelElement();
					
					e_out_copy_target = v2_copy;
				} else {
					e_out_copy_target = v2;
				}
				
				Relation e_out_copy = new Relation(e_out, e_out_copy_source, e_out_copy_target);
				this.elementCreator.createRelation(e_out_copy);
			}
			
			// run through all incoming relations of v1
			List<Relation> v1_incomingRelations = collectIncomingApplicationModelElementRelationsFor(r1, originalRoleBindingToRoleBindingCopyMap);
			for (Relation e_in: v1_incomingRelations) {
				DesignElement v2 = e_in.getSourceElement();
				RoleBinding r2 = findRoleBindingForApplicationModelElement(v2, r1.getAppliedPattern());
				
				if (!in(rootSetFragmentInstanceToCopy, r2)) {
					// create a new relation
					
					DesignElement e_in_copy_source = v2;
					DesignElement e_in_copy_target = v1_copy;
					
					Relation e_in_copy = new Relation(e_in, e_in_copy_source, e_in_copy_target);
					this.elementCreator.createRelation(e_in_copy);
				}
			}
		}
	}
	
	private boolean in(
			SetFragmentInstance parentSetFragmentInstance, // i
			SetFragmentInstance potentiallyContainedSetFragmentInstance) { // j
		
		for (SetFragmentInstance containedInstance: parentSetFragmentInstance.getContainedSetFragmentInstances()) { // k
			if (containedInstance == potentiallyContainedSetFragmentInstance
					|| in(containedInstance, potentiallyContainedSetFragmentInstance)) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean in(
			SetFragmentInstance parentSetFragmentInstance, // i
			RoleBinding potentiallyContainedRoleBinding) { // v
		
		if (parentSetFragmentInstance.getContainedRoleBindings().contains(potentiallyContainedRoleBinding)) {
			return true;
		}
		
		for (SetFragmentInstance containedInstance: parentSetFragmentInstance.getContainedSetFragmentInstances()) { // k
			if (in(containedInstance, potentiallyContainedRoleBinding)) {
				return true;
			}
		}
		
		return false;
	}
	
	private RoleBinding findRoleBindingForApplicationModelElement(DesignElement applicationModelElement, AppliedPattern patternApplication) {
		for (RoleBinding roleBinding: patternApplication.getRoleBindings()) {
			if (roleBinding.getApplicationModelElement() == applicationModelElement) {
				return roleBinding;
			}
		}
		return null;
	}

	private ApplicationModel createEmptyApplicationModel(AppliedPattern patternApplication) {
		if (patternApplication.getApplicationModel() != null) {
			throw new IllegalStateException("There is already an application model for the given pattern application." +
					" Creation could lead to incosistencies.");
		}
		
		// create the application model to store the unfolded design model
		// (1-to-1 design model representation without set fragments)
		ApplicationModel applicationModel = MappingFactory.eINSTANCE.createApplicationModel();
		patternApplication.setApplicationModel(applicationModel);
		applicationModel.setName(patternApplication.getName());
		return applicationModel;
	}
	
	private void createMissingApplicationModelElementsForRoleBindings(AppliedPattern patternApplication) {
		for (RoleBinding roleBinding : patternApplication.getRoleBindings()) {
			if (roleBinding.getApplicationModelElement() == null) {
				createApplicationModelElementForRoleBinding(roleBinding);
			}

			this.addRolebindingForPatternRoleToInternalMap(roleBinding.getRole(), roleBinding);
		}
	}
	
	private DesignElement createApplicationModelElementForRoleBinding(RoleBinding roleBinding) {
		if(roleBinding.getApplicationModelElement() != null) {
			throw new IllegalStateException("An application model element is already available for the given role binding.");
		}
		
		DesignElement newApplicationModelElement = this.elementCreator.createApplicationModelElement(roleBinding);
		
		Assert.isNotNull(newApplicationModelElement);

		// add the new application model element to the model
		ApplicationModel applicationModel = roleBinding.getAppliedPattern().getApplicationModel();
		applicationModel.getDesignElements().add(newApplicationModelElement);
		roleBinding.setApplicationModelElement(newApplicationModelElement);
		
		return newApplicationModelElement;
	}
	
	private void createApplicationModelElementRelations(AppliedPattern patternApplication) {
		// 3.1: collect all relations
		List<Relation> allApplicationModelElementRelations = new LinkedList<Relation>();
		for (RoleBinding roleBinding : patternApplication.getRoleBindings()) {
			List<Relation> outgoingApplicationModelElementRelations = this.elementCreator
					.collectOutgoingApplicationModelRelationsFor(roleBinding);
			allApplicationModelElementRelations.addAll(outgoingApplicationModelElementRelations);
		}
		// 3.2: instantiate all relations (i.e. connect the application model elements accordingly)
		for (Relation applicationModelElementRelation : allApplicationModelElementRelations) {
			this.elementCreator.createRelation(applicationModelElementRelation);
		}
	}
	
	private void createApplicationModelElementTasksToRoleBindings(AppliedPattern patternApplication) {
		for (RoleBinding roleBinding : patternApplication.getRoleBindings()) {
			createTasksForRoleBinding(roleBinding);
		}
	}
	
	private void createTasksForRoleBinding(RoleBinding roleBinding) {
		// TODO check if the following check is necessary and the test model (editor.mappings) has to be modified (remove tasks)
//		if (!roleBinding.getTasks().isEmpty()) {
//			throw new IllegalStateException("The given role binding has already connected tasks.");
//		}
		
		for (TaskDescription taskDescription : roleBinding.getRole().getTasks()) {
			Task task = MappingFactory.eINSTANCE.createTask();
			task.setTaskDescription(taskDescription);
			task.setID(EcoreUtil.generateUUID());
			task.setCompleted(false);
			roleBinding.getTasks().add(task);
		}
	}
	
	private List<Relation> collectIncomingApplicationModelElementRelationsFor(RoleBinding targetRoleBinding, Map<RoleBinding, RoleBinding> originalRoleBindingToRoleBindingCopyMap) {
		AppliedPattern patternApplication = targetRoleBinding.getAppliedPattern();
		DesignElement targetApplicationModelElement = targetRoleBinding.getApplicationModelElement();
		List<Relation> incomingRelations = new LinkedList<Relation>();
		
		TreeIterator<Object> iter = EcoreUtil.getAllContents(patternApplication, true);
		while (iter.hasNext()) {
			Object next = iter.next();
			if (next instanceof RoleBinding
					&& next != targetRoleBinding
					// ignore role bindings that are currently created for the new set fragment instance
					&& !isRoleBindingAValueInMap((RoleBinding) next, originalRoleBindingToRoleBindingCopyMap)) {
				RoleBinding anotherRoleBinding = (RoleBinding) next;
				DesignElement anotherApplicationModelElement = anotherRoleBinding.getApplicationModelElement();
				
				if (anotherApplicationModelElement != null) {
					List<Relation> outgoingRelations = this.elementCreator.collectOutgoingDesignElementRelationsFor(anotherApplicationModelElement);
					
					for (Relation outgoingRelation: outgoingRelations) {
						if (outgoingRelation.getTargetElement() == targetApplicationModelElement) {
							Assert.isTrue(outgoingRelation.getSourceElement() == anotherApplicationModelElement);
							
							incomingRelations.add(outgoingRelation);
						}
					}
				}					
			}
		}
		return incomingRelations;
	}
	
	private boolean isRoleBindingAValueInMap(RoleBinding roleBinding, Map<RoleBinding, RoleBinding> originalRoleBindingToRoleBindingCopyMap) {
		for (RoleBinding valueInMap: originalRoleBindingToRoleBindingCopyMap.values()) {
			if (valueInMap == roleBinding) {
				return true;
			}
		}
		
		return false;
	}
	
}
