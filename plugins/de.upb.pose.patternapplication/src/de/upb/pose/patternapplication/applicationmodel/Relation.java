/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import org.eclipse.emf.ecore.EReference;

import de.upb.pose.specification.DesignElement;

/**
 * Represents any kind of relation between two design elements.
 * 
 * @author Dietrich Travkin
 */
public class Relation
{
	private EReference relationType;
	private DesignElement sourceElement;
	private DesignElement targetElement;
	
	/**
	 * @param relationType the {@link org.eclipse.emf.ecore.EReference EReference} representing this relation
	 * @param sourceElement the element that is source of this relation
	 * @param targetElement the element that is target of this relation
	 */
	public Relation(EReference relationType, DesignElement sourceElement, DesignElement targetElement)
	{
		this.relationType = relationType;
		this.sourceElement = sourceElement;
		this.targetElement = targetElement;
	}
	
	public Relation(Relation relationOfSameType, DesignElement sourceElement, DesignElement targetElement)
	{
		this.relationType = relationOfSameType.relationType;
		this.sourceElement = sourceElement;
		this.targetElement = targetElement;
	}
	
	public EReference getRelationType() {
		return this.relationType;
	}
	
	/**
	 * @return the feature id of this relation, e.g. the id of the inheritance relation
	 */
	public int getRelationFeatureId()
	{
		return this.relationType.getFeatureID();
	}
	
	public boolean isToMany() {
		return this.relationType.isMany();
	}
	
	/**
	 * @return the design element that is source of this relation
	 */
	public DesignElement getSourceElement()
	{
		return this.sourceElement;
	}
	
	/**
	 * @return the design element that is target of this relation
	 */
	public DesignElement getTargetElement()
	{
		return this.targetElement;
	}
}
