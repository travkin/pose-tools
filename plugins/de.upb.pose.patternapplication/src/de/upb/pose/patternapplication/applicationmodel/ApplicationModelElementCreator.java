/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.mapping.MappingNameCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.util.SpecificationUtil;

/**
 * @author Dietrich Travkin
 */
public class ApplicationModelElementCreator
{
	private ApplicationModelCreator modelCreator;
	
	ApplicationModelElementCreator(ApplicationModelCreator parent) {
		this.modelCreator = parent;
	}
	
	/**
	 * Creates a copy of the pattern specification element (a design element) that represents
	 * the pattern role corresponding to the given role binding.
	 * If this creator is not responsible the role binding is handed over to the next
	 * creator in the chain of responsibility. 
	 * 
	 * @param roleBinding the role binding to create an application model element for
	 * @return the created application model element
	 */
	public final DesignElement createApplicationModelElement(RoleBinding roleBinding) {
		DesignElement copiedElement = EcoreUtil.copy(roleBinding.getRole());
		
		// remove all copied contained elements
		List<EObject> containedElements = new ArrayList<EObject>(copiedElement.eContents().size());
		containedElements.addAll(copiedElement.eContents());
		for (EObject contained: containedElements)
		{
			EcoreUtil.remove(contained);
		}
		
		// generate new UUID
		copiedElement.setID(EcoreUtil.generateUUID());
		
		// set the right name for the application model element
		copiedElement.setName(getApplicationModelElementName(roleBinding));
		
		return copiedElement;
	}
	
	public final List<Relation> collectOutgoingApplicationModelRelationsFor(RoleBinding sourceRoleBinding) {
		DesignElement sourcePatternRole = sourceRoleBinding.getRole();
		List<Relation> outgoingPatternRoleRelations = collectOutgoingDesignElementRelationsFor(sourceRoleBinding.getRole());
		List<Relation> allApplicationModelElementRelations = new LinkedList<Relation>();
		for (Relation patternRoleRelation: outgoingPatternRoleRelations) {
			Assert.isTrue(sourcePatternRole == patternRoleRelation.getSourceElement());
			Assert.isNotNull(patternRoleRelation.getTargetElement());
			
			List<Relation> relations = createApplicationModelElementRelationsForPatternRoleRelation(patternRoleRelation);
			allApplicationModelElementRelations.addAll(relations);
		}
		return allApplicationModelElementRelations;
	}
	
	/*package*/ List<Relation> collectOutgoingDesignElementRelationsFor(DesignElement sourceDesignElement) {
		EClass eClass = sourceDesignElement.eClass();
		EList<EReference> eClassReferences = eClass.getEAllReferences();

		List<Relation> outgoingRelations = new LinkedList<Relation>();
		for (EReference outgoingReference: eClassReferences) {
			if (sourceDesignElement.eIsSet(outgoingReference)) {
				Object value = sourceDesignElement.eGet(outgoingReference, true);
				if (outgoingReference.isMany()) {
					EList<?> values = (EList<?>) value;
					for (Object object: values) {
						addNewRelationIfTargetIsDesignElement(object, sourceDesignElement, outgoingReference, outgoingRelations);
					}
				} else {
					addNewRelationIfTargetIsDesignElement(value, sourceDesignElement, outgoingReference, outgoingRelations);
				}
			}
		}
		return outgoingRelations;
	}
	
	private void addNewRelationIfTargetIsDesignElement(Object target, DesignElement sourceDesignElement, EReference relationType, List<Relation> relations) {
		if (target instanceof DesignElement) {
			DesignElement targetDesignElement = (DesignElement) target;
			relations.add(new Relation(relationType, sourceDesignElement, targetDesignElement));
		}
	}
	
	@SuppressWarnings("unchecked")
	/*package*/ void createRelation(Relation applicationModelElementRelation) {
		DesignElement source = applicationModelElementRelation.getSourceElement();
		DesignElement target = applicationModelElementRelation.getTargetElement();
		
		Assert.isNotNull(source);
		Assert.isNotNull(target);
		
		EReference reference = applicationModelElementRelation.getRelationType();
		EClass sourceType = (EClass) reference.eContainer();
		EClass targetType = reference.getEReferenceType();
		
		Assert.isTrue(sourceType.isInstance(source));
		Assert.isTrue(targetType.isInstance(target));
		
		// actual relation creation
		if (reference.isMany()) {
			@SuppressWarnings("rawtypes")
			EList eList = (EList) source.eGet(reference, true);
			if (!eList.contains(target)) {
				eList.add(target);
			}
		} else {
			Object currentValue = source.eGet(reference, true);
			if (!target.equals(currentValue)) {
				source.eSet(reference, target);
			}
		}
	}
	
	private List<Relation> createApplicationModelElementRelationsForPatternRoleRelation(Relation patternRoleRelation) {
		DesignElement sourcePatternRole = patternRoleRelation.getSourceElement();
		DesignElement targetPatternRole = patternRoleRelation.getTargetElement();
		List<Relation> applicationModelElementRelations = new LinkedList<Relation>();
			
		List<RoleBinding> sourceRoleBindings = this.modelCreator.findRoleBindingsForPatternRole(sourcePatternRole);
		List<RoleBinding> targetRoleBindings = this.modelCreator.findRoleBindingsForPatternRole(targetPatternRole);
			
		Assert.isTrue(sourceRoleBindings.size() > 0);
		Assert.isTrue(targetRoleBindings.size() > 0);
			
		for (RoleBinding sourceRoleBinding: sourceRoleBindings) {
			List<Relation> relations = createApplicationModelElementRelationsFor(patternRoleRelation, sourceRoleBinding, targetRoleBindings);
			applicationModelElementRelations.addAll(relations);
		}
			
		return applicationModelElementRelations;
	}
	
	private List<Relation> createApplicationModelElementRelationsFor(Relation patternRoleRelation, RoleBinding sourceRoleBinding, List<RoleBinding> targetRoleBindings) {
		DesignElement sourcePatternRole = patternRoleRelation.getSourceElement();
		DesignElement targetPatternRole = patternRoleRelation.getTargetElement();
		List<Relation> applicationModelElementRelations = new LinkedList<Relation>();
		
		// case 1: target role is not in a set fragment
		if (!SpecificationUtil.inASetFragment(targetPatternRole))
		{
			//  there must be exactly one target (role binding / design element) for the source (role binding / design element)
			Assert.isTrue(targetRoleBindings.size() == 1);
			RoleBinding targetRoleBinding = targetRoleBindings.get(0);
			
			applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
		}
		else // target role is in a set fragment
		{
			if (!patternRoleRelation.isToMany()
					&& !SpecificationUtil.inTheSameSetFragmentArea(sourcePatternRole, targetPatternRole)
					&& !SpecificationUtil.inASetFragment(sourcePatternRole)) {
				
				if (sourcePatternRole.isCanBeGenerated()) {
					throw new IllegalStateException("Illegal pattern specification!\n" +
							"For a 0..1 cardinality relation the specification contains potentially multiple targets.\n" +
							"Relation type=" + patternRoleRelation.getRelationType().getName() + "\n" +
							"Source pattern role: type=" + sourcePatternRole.getClass().getName() + ", name=" + sourcePatternRole.getName() + "\n" +
							"target pattern role: type=" + targetPatternRole.getClass().getName() + ", name=" + targetPatternRole.getName() + ".");
				} else {
					// In order not to leave references to elements in the pattern specification (through references copied during initialization of the application model)
					// I create an arbitrary of several possible relations to replace a referenced pattern element with a referenced application model element
					Assert.isTrue(!targetRoleBindings.isEmpty());
					
					RoleBinding arbitraryTargetRoleBinding = targetRoleBindings.get(0);
					
					applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, arbitraryTargetRoleBinding));
					return applicationModelElementRelations;
				}
			}
			
			// case 2: the source role is not in a set fragment, but the target role is
			if (!SpecificationUtil.inASetFragment(sourcePatternRole)) {
				for (RoleBinding targetRoleBinding: targetRoleBindings) {
					applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
				}
			}
			// case 4: source and target roles are in the same set fragment (or intersection)
			else if (SpecificationUtil.inTheSameSetFragmentArea(sourcePatternRole, targetPatternRole))
			{
				// take any set element binding and find a target role binding in the same set element binding
				Assert.isTrue(sourceRoleBinding.getContainingSetFragmentInstances().size() >= 1);
				SetFragmentInstance setElementBinding = sourceRoleBinding.getContainingSetFragmentInstances().get(0);
				List<RoleBinding> filteredTargetRoleBindings = selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(targetRoleBindings, setElementBinding);
				
				// there must be exactly one target role binding in the same set element binding
				Assert.isTrue(filteredTargetRoleBindings.size() == 1);
				RoleBinding targetRoleBinding = filteredTargetRoleBindings.get(0);
				
				applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
			}
			// source and target roles are both in set fragments, but in different set fragment areas
			else
			{
				if (SpecificationUtil.inDisjointSetFragments(sourcePatternRole, targetPatternRole))
				{
					if (sourcePatternRole.isCanBeGenerated()) {
						throw new IllegalStateException("Illegal pattern specification!\n" +
								"The specification has an ambiguously specified relation between two disjoint set fragment areas.\n" +
								"Relation type=" + patternRoleRelation.getRelationType().getName() + "\n" +
								"Source pattern role: type=" + sourcePatternRole.getClass().getName() + ", name=" + sourcePatternRole.getName() + "\n" +
								"target pattern role: type=" + targetPatternRole.getClass().getName() + ", name=" + targetPatternRole.getName() + ".");
					} else {
						// In order not to leave references to elements in the pattern specification (through references copied during initialization of the application model)
						// I create an arbitrary of several possible relations to replace a referenced pattern element with a referenced application model element
						Assert.isTrue(!targetRoleBindings.isEmpty());
						
						RoleBinding arbitraryTargetRoleBinding = targetRoleBindings.get(0);
						
						applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, arbitraryTargetRoleBinding));
						return applicationModelElementRelations;
					}
				}
				
				// case 5: source and target roles are in set fragments,
				// the target role's parent set fragments are completely contained in parent set fragments of the source role,
				// (similarly to case 2) there are more targets than sources
				if (SpecificationUtil.setFragmentsOfElement1ContainSetFragmentsOfElement2(sourcePatternRole, targetPatternRole)) {
					// take any set element binding containing the source role and find all target role bindings in the same set element binding
					Assert.isTrue(sourceRoleBinding.getContainingSetFragmentInstances().size() >= 1);
					SetFragmentInstance setElementBinding = sourceRoleBinding.getContainingSetFragmentInstances().get(0);
					List<RoleBinding> filteredTargetRoleBindings = selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(targetRoleBindings, setElementBinding);
					
					Assert.isTrue(filteredTargetRoleBindings.size() > 0);
					for (RoleBinding targetRoleBinding: filteredTargetRoleBindings) {
						applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
					}
				}
				// case 6: source and target roles are in set fragments,
				// the source role's parent set fragments are completely contained in parent set fragments of the target role,
				// (similarly to case 1) there must be exactly one target role binding for the source role binding
				else if (SpecificationUtil.setFragmentsOfElement1ContainSetFragmentsOfElement2(targetPatternRole, sourcePatternRole)) {
					// take any set element binding containing the source role and find the one target role binding in a parent set element binding
					Assert.isTrue(sourceRoleBinding.getContainingSetFragmentInstances().size() >= 1);
					SetFragmentInstance setElementBinding = sourceRoleBinding.getContainingSetFragmentInstances().get(0);
					List<RoleBinding> filteredTargetRoleBindings = selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(targetRoleBindings, setElementBinding);
					Assert.isTrue(filteredTargetRoleBindings.isEmpty());
					
					List<SetFragmentInstance> pendingParentBindings = new LinkedList<SetFragmentInstance>();
					pendingParentBindings.addAll(setElementBinding.getContainingSetFragmentInstances());
					while (filteredTargetRoleBindings.isEmpty() && !pendingParentBindings.isEmpty()) {
						setElementBinding = pendingParentBindings.remove(0);
						pendingParentBindings.addAll(setElementBinding.getContainingSetFragmentInstances());
						
						filteredTargetRoleBindings = selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(targetRoleBindings, setElementBinding);
					}

					// there must be exactly one target role binding for the given source role binding
					Assert.isTrue(filteredTargetRoleBindings.size() == 1);
					RoleBinding targetRoleBinding = filteredTargetRoleBindings.get(0);
					
					applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
				}
				// case 7: there must be an intersection between the parent set fragments of source and target roles
				else {
					// go through all parent set element bindings of the source role and find all target roles in the same set element binding, if any
					Assert.isTrue(sourceRoleBinding.getContainingSetFragmentInstances().size() >= 1);
					for (SetFragmentInstance setElementBinding: sourceRoleBinding.getContainingSetFragmentInstances()) {
						List<RoleBinding> filteredTargetRoleBindings = selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(targetRoleBindings, setElementBinding);
						for (RoleBinding targetRoleBinding: filteredTargetRoleBindings) {
							applicationModelElementRelations.add(createRelationFor(patternRoleRelation, sourceRoleBinding, targetRoleBinding));
						}
					}
					// at least one of the source's parent set element bindings has to offer at least one contained target role binding
					Assert.isTrue(!applicationModelElementRelations.isEmpty());
				}
			}
		}
		return applicationModelElementRelations;
	}
	
	private Relation createRelationFor(Relation patternRoleRelation, RoleBinding sourceRoleBinding, RoleBinding targetRoleBinding) {
		DesignElement source = sourceRoleBinding.getApplicationModelElement();
		DesignElement target = targetRoleBinding.getApplicationModelElement();
			
		Assert.isNotNull(source);
		Assert.isNotNull(target);
			
		return new Relation(patternRoleRelation, source, target);
	}
	
	private List<RoleBinding> selectRoleBindingsDirectlyOrIndirectlyContainedInSetFragmentInstance(List<RoleBinding> roleBindings, SetFragmentInstance setFragmentInstance) {
		List<RoleBinding> filteredRolebindings = new ArrayList<RoleBinding>(roleBindings.size());
		for (RoleBinding aRoleBinding: roleBindings) {
			if (SetFragmentBindingCreator.containsSetElementBindingDirectlyOrIndirectlyRoleBinding(setFragmentInstance, aRoleBinding)) {
				filteredRolebindings.add(aRoleBinding);
			}
		}
		return filteredRolebindings;
	}
	
	public static String getApplicationModelElementName(RoleBinding roleBinding) {
		// TODO add a suffix number to avoid non-unique names
		String applicationModelElementName = MappingNameCreator.getMappedToText(roleBinding);
		if (applicationModelElementName != null) {
			if (roleBinding.getNewElementName() != null && !roleBinding.getNewElementName().isEmpty()) {
				applicationModelElementName = roleBinding.getNewElementName();
			}
		} else {
			applicationModelElementName = roleBinding.getRole().getName();
		}
		return applicationModelElementName;
	}
	
}
