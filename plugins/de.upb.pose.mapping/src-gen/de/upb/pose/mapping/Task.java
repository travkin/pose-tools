/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import de.upb.pose.core.Named;
import de.upb.pose.specification.TaskDescription;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a user task to be manually completed to completely apply the pattern. Each task is assigned to role binding, i.e. to a realization of a pattern role.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.Task#getTaskDescription <em>Task Description</em>}</li>
 *   <li>{@link de.upb.pose.mapping.Task#isCompleted <em>Completed</em>}</li>
 *   <li>{@link de.upb.pose.mapping.Task#getParentRoleBinding <em>Parent Role Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getTask()
 * @generated
 */
public interface Task extends Named {
	/**
	 * Returns the value of the '<em><b>Task Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Description</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Description</em>' reference.
	 * @see #setTaskDescription(TaskDescription)
	 * @see de.upb.pose.mapping.MappingPackage#getTask_TaskDescription()
	 * @generated
	 */
	TaskDescription getTaskDescription();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.Task#getTaskDescription <em>Task Description</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Description</em>' reference.
	 * @see #getTaskDescription()
	 * @generated
	 */
	void setTaskDescription(TaskDescription value);

	/**
	 * Returns the value of the '<em><b>Completed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Completed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This attribute represents the status of the corresponding task. If the task is completed, the value of this attribute is true.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Completed</em>' attribute.
	 * @see #setCompleted(boolean)
	 * @see de.upb.pose.mapping.MappingPackage#getTask_Completed()
	 * @generated
	 */
	boolean isCompleted();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.Task#isCompleted <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completed</em>' attribute.
	 * @see #isCompleted()
	 * @generated
	 */
	void setCompleted(boolean value);

	/**
	 * Returns the value of the '<em><b>Parent Role Binding</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.RoleBinding#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Role Binding</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Role Binding</em>' container reference.
	 * @see #setParentRoleBinding(RoleBinding)
	 * @see de.upb.pose.mapping.MappingPackage#getTask_ParentRoleBinding()
	 * @see de.upb.pose.mapping.RoleBinding#getTasks
	 * @generated
	 */
	RoleBinding getParentRoleBinding();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.Task#getParentRoleBinding <em>Parent Role Binding</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Role Binding</em>' container reference.
	 * @see #getParentRoleBinding()
	 * @generated
	 */
	void setParentRoleBinding(RoleBinding value);

} // Task
