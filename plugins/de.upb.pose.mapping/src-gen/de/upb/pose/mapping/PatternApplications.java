/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.Named;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DesignPattern Applications</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Collection of all documented pattern applications in a software design model. This class represents the root element for all pattern applications.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.PatternApplications#getApplications <em>Applications</em>}</li>
 *   <li>{@link de.upb.pose.mapping.PatternApplications#getDesignModelRoot <em>Design Model Root</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getPatternApplications()
 * @generated
 */
public interface PatternApplications extends Named {
	/**
	 * Returns the value of the '<em><b>Applications</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.AppliedPattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.AppliedPattern#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applications</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getPatternApplications_Applications()
	 * @see de.upb.pose.mapping.AppliedPattern#getParent
	 * @generated
	 */
	EList<AppliedPattern> getApplications();

	/**
	 * Returns the value of the '<em><b>Design Model Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Design Model Root</em>' reference.
	 * @see #setDesignModelRoot(EObject)
	 * @see de.upb.pose.mapping.MappingPackage#getPatternApplications_DesignModelRoot()
	 * @generated
	 */
	EObject getDesignModelRoot();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.PatternApplications#getDesignModelRoot <em>Design Model Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Design Model Root</em>' reference.
	 * @see #getDesignModelRoot()
	 * @generated
	 */
	void setDesignModelRoot(EObject value);

} // PatternApplications
