/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.Named;
import de.upb.pose.specification.DesignElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the assignment of one or more software design elements to a single pattern role, i.e. the design elements play the specified pattern role. In case that no design elements are chosen and new design elements have to be created to play the pattern role, then the newElementName attribute stores the user-chosen name for design element to be created during pattern application.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getModelElements <em>Model Elements</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getRole <em>Role</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getAppliedPattern <em>Applied Pattern</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getTasks <em>Tasks</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getApplicationModelElement <em>Application Model Element</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getNewElementName <em>New Element Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.RoleBinding#getRootToken <em>Root Token</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding()
 * @generated
 */
public interface RoleBinding extends Named {
	/**
	 * Returns the value of the '<em><b>Model Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Elements</em>' reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_ModelElements()
	 * @generated
	 */
	EList<EObject> getModelElements();

	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(DesignElement)
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_Role()
	 * @generated
	 */
	DesignElement getRole();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.RoleBinding#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(DesignElement value);

	/**
	 * Returns the value of the '<em><b>Applied Pattern</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.AppliedPattern#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied DesignPattern</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Pattern</em>' container reference.
	 * @see #setAppliedPattern(AppliedPattern)
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_AppliedPattern()
	 * @see de.upb.pose.mapping.AppliedPattern#getRoleBindings
	 * @generated
	 */
	AppliedPattern getAppliedPattern();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.RoleBinding#getAppliedPattern <em>Applied Pattern</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Pattern</em>' container reference.
	 * @see #getAppliedPattern()
	 * @generated
	 */
	void setAppliedPattern(AppliedPattern value);

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.Task}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.Task#getParentRoleBinding <em>Parent Role Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tasks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_Tasks()
	 * @see de.upb.pose.mapping.Task#getParentRoleBinding
	 * @generated
	 */
	EList<Task> getTasks();

	/**
	 * Returns the value of the '<em><b>Containing Set Fragment Instances</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.SetFragmentInstance}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentInstance#getContainedRoleBindings <em>Contained Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Set Fragment Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Set Fragment Instances</em>' reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_ContainingSetFragmentInstances()
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainedRoleBindings
	 * @generated
	 */
	EList<SetFragmentInstance> getContainingSetFragmentInstances();

	/**
	 * Returns the value of the '<em><b>Application Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Model Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Model Element</em>' reference.
	 * @see #setApplicationModelElement(DesignElement)
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_ApplicationModelElement()
	 * @generated
	 */
	DesignElement getApplicationModelElement();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.RoleBinding#getApplicationModelElement <em>Application Model Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Model Element</em>' reference.
	 * @see #getApplicationModelElement()
	 * @generated
	 */
	void setApplicationModelElement(DesignElement value);

	/**
	 * Returns the value of the '<em><b>New Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * In case that no design elements are chosen for this role binding and new design elements have to be created to play the pattern role, then this attribute stores the user-chosen name for the design element to be created during pattern application.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>New Element Name</em>' attribute.
	 * @see #setNewElementName(String)
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_NewElementName()
	 * @generated
	 */
	String getNewElementName();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.RoleBinding#getNewElementName <em>New Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Element Name</em>' attribute.
	 * @see #getNewElementName()
	 * @generated
	 */
	void setNewElementName(String value);

	/**
	 * Returns the value of the '<em><b>Root Token</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.Token#getParentRoleBinding <em>Parent Role Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Token</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Token</em>' containment reference.
	 * @see #setRootToken(Token)
	 * @see de.upb.pose.mapping.MappingPackage#getRoleBinding_RootToken()
	 * @see de.upb.pose.mapping.Token#getParentRoleBinding
	 * @generated
	 */
	Token getRootToken();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.RoleBinding#getRootToken <em>Root Token</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Token</em>' containment reference.
	 * @see #getRootToken()
	 * @generated
	 */
	void setRootToken(Token value);

} // RoleBinding
