/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Named;
import de.upb.pose.specification.DesignElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * An abstract model of the software design as it is intended to be after pattern application. Here, only DesignElements are used to create a 1-to-1 mapping from abstract design elements in the application model to concrete design elements in the software design model where the pattern is actually applied.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.ApplicationModel#getDesignElements <em>Design Elements</em>}</li>
 *   <li>{@link de.upb.pose.mapping.ApplicationModel#getAppliedPattern <em>Applied Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getApplicationModel()
 * @generated
 */
public interface ApplicationModel extends Named {
	/**
	 * Returns the value of the '<em><b>Design Elements</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.DesignElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Design Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Design Elements</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getApplicationModel_DesignElements()
	 * @generated
	 */
	EList<DesignElement> getDesignElements();

	/**
	 * Returns the value of the '<em><b>Applied Pattern</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.AppliedPattern#getApplicationModel <em>Application Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied DesignPattern</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Pattern</em>' container reference.
	 * @see #setAppliedPattern(AppliedPattern)
	 * @see de.upb.pose.mapping.MappingPackage#getApplicationModel_AppliedPattern()
	 * @see de.upb.pose.mapping.AppliedPattern#getApplicationModel
	 * @generated
	 */
	AppliedPattern getAppliedPattern();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.ApplicationModel#getAppliedPattern <em>Applied Pattern</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Pattern</em>' container reference.
	 * @see #getAppliedPattern()
	 * @generated
	 */
	void setAppliedPattern(AppliedPattern value);

} // ApplicationModel
