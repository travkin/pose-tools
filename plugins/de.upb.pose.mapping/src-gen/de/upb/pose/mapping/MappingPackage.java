/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.pose.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.pose.mapping.MappingFactory
 * @generated
 */
public interface MappingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mapping"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni-paderborn.de/pose/mapping"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mapping"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MappingPackage eINSTANCE = de.upb.pose.mapping.impl.MappingPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.AppliedPatternImpl <em>Applied Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.AppliedPatternImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getAppliedPattern()
	 * @generated
	 */
	int APPLIED_PATTERN = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__PARENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pattern Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__PATTERN_SPECIFICATION = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Role Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__ROLE_BINDINGS = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Set Fragment Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__SET_FRAGMENT_BINDINGS = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Application Model</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN__APPLICATION_MODEL = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Applied Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Applied Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLIED_PATTERN_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.PatternApplicationsImpl <em>Pattern Applications</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.PatternApplicationsImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getPatternApplications()
	 * @generated
	 */
	int PATTERN_APPLICATIONS = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Applications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS__APPLICATIONS = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Design Model Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pattern Applications</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Pattern Applications</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATIONS_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.RoleBindingImpl <em>Role Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.RoleBindingImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getRoleBinding()
	 * @generated
	 */
	int ROLE_BINDING = 2;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Model Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__MODEL_ELEMENTS = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__ROLE = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Applied Pattern</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__APPLIED_PATTERN = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__TASKS = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Containing Set Fragment Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Application Model Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__APPLICATION_MODEL_ELEMENT = CorePackage.NAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>New Element Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__NEW_ELEMENT_NAME = CorePackage.NAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Root Token</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING__ROOT_TOKEN = CorePackage.NAMED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Role Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Role Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_BINDING_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.TaskImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Task Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__TASK_DESCRIPTION = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__COMPLETED = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Role Binding</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__PARENT_ROLE_BINDING = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl <em>Set Fragment Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.SetFragmentBindingImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getSetFragmentBinding()
	 * @generated
	 */
	int SET_FRAGMENT_BINDING = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Applied Pattern</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__APPLIED_PATTERN = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Set Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__SET_FRAGMENT = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Set Fragment Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Set Fragment Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Set Fragment Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_BINDING_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl <em>Set Fragment Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.SetFragmentInstanceImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getSetFragmentInstance()
	 * @generated
	 */
	int SET_FRAGMENT_INSTANCE = 5;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Parent Set Fragment Binding</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Contained Role Bindings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Containing Set Fragment Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contained Set Fragment Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Set Fragment Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Set Fragment Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_INSTANCE_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.ApplicationModelImpl <em>Application Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.ApplicationModelImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getApplicationModel()
	 * @generated
	 */
	int APPLICATION_MODEL = 6;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Design Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL__DESIGN_ELEMENTS = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Applied Pattern</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL__APPLIED_PATTERN = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Application Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Application Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MODEL_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.TokenImpl <em>Token</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.TokenImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getToken()
	 * @generated
	 */
	int TOKEN = 7;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Parent Role Binding</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__PARENT_ROLE_BINDING = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sub Tokens</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__SUB_TOKENS = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Token</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__PARENT_TOKEN = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Maps To</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN__MAPS_TO = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Token</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Token</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOKEN_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.mapping.impl.StringEObjectMapEntryImpl <em>String EObject Map Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.mapping.impl.StringEObjectMapEntryImpl
	 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getStringEObjectMapEntry()
	 * @generated
	 */
	int STRING_EOBJECT_MAP_ENTRY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EOBJECT_MAP_ENTRY__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EOBJECT_MAP_ENTRY__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EOBJECT_MAP_ENTRY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String EObject Map Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EOBJECT_MAP_ENTRY_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.AppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Applied Pattern</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern
	 * @generated
	 */
	EClass getAppliedPattern();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.AppliedPattern#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern#getParent()
	 * @see #getAppliedPattern()
	 * @generated
	 */
	EReference getAppliedPattern_Parent();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.AppliedPattern#getPatternSpecification <em>Pattern Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pattern Specification</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern#getPatternSpecification()
	 * @see #getAppliedPattern()
	 * @generated
	 */
	EReference getAppliedPattern_PatternSpecification();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.AppliedPattern#getRoleBindings <em>Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Bindings</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern#getRoleBindings()
	 * @see #getAppliedPattern()
	 * @generated
	 */
	EReference getAppliedPattern_RoleBindings();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.AppliedPattern#getSetFragmentBindings <em>Set Fragment Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Set Fragment Bindings</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern#getSetFragmentBindings()
	 * @see #getAppliedPattern()
	 * @generated
	 */
	EReference getAppliedPattern_SetFragmentBindings();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.pose.mapping.AppliedPattern#getApplicationModel <em>Application Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Application Model</em>'.
	 * @see de.upb.pose.mapping.AppliedPattern#getApplicationModel()
	 * @see #getAppliedPattern()
	 * @generated
	 */
	EReference getAppliedPattern_ApplicationModel();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.PatternApplications <em>Pattern Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern Applications</em>'.
	 * @see de.upb.pose.mapping.PatternApplications
	 * @generated
	 */
	EClass getPatternApplications();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.PatternApplications#getApplications <em>Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Applications</em>'.
	 * @see de.upb.pose.mapping.PatternApplications#getApplications()
	 * @see #getPatternApplications()
	 * @generated
	 */
	EReference getPatternApplications_Applications();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.PatternApplications#getDesignModelRoot <em>Design Model Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Design Model Root</em>'.
	 * @see de.upb.pose.mapping.PatternApplications#getDesignModelRoot()
	 * @see #getPatternApplications()
	 * @generated
	 */
	EReference getPatternApplications_DesignModelRoot();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.RoleBinding <em>Role Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Binding</em>'.
	 * @see de.upb.pose.mapping.RoleBinding
	 * @generated
	 */
	EClass getRoleBinding();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.mapping.RoleBinding#getModelElements <em>Model Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Model Elements</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getModelElements()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_ModelElements();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.RoleBinding#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getRole()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_Role();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.RoleBinding#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Applied Pattern</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getAppliedPattern()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_AppliedPattern();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.RoleBinding#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getTasks()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_Tasks();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.mapping.RoleBinding#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Containing Set Fragment Instances</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getContainingSetFragmentInstances()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_ContainingSetFragmentInstances();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.RoleBinding#getApplicationModelElement <em>Application Model Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Application Model Element</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getApplicationModelElement()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_ApplicationModelElement();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.pose.mapping.RoleBinding#getNewElementName <em>New Element Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Element Name</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getNewElementName()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EAttribute getRoleBinding_NewElementName();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.pose.mapping.RoleBinding#getRootToken <em>Root Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Token</em>'.
	 * @see de.upb.pose.mapping.RoleBinding#getRootToken()
	 * @see #getRoleBinding()
	 * @generated
	 */
	EReference getRoleBinding_RootToken();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see de.upb.pose.mapping.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.Task#getTaskDescription <em>Task Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task Description</em>'.
	 * @see de.upb.pose.mapping.Task#getTaskDescription()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_TaskDescription();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.pose.mapping.Task#isCompleted <em>Completed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completed</em>'.
	 * @see de.upb.pose.mapping.Task#isCompleted()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Completed();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.Task#getParentRoleBinding <em>Parent Role Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Role Binding</em>'.
	 * @see de.upb.pose.mapping.Task#getParentRoleBinding()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_ParentRoleBinding();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.SetFragmentBinding <em>Set Fragment Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Fragment Binding</em>'.
	 * @see de.upb.pose.mapping.SetFragmentBinding
	 * @generated
	 */
	EClass getSetFragmentBinding();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Applied Pattern</em>'.
	 * @see de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern()
	 * @see #getSetFragmentBinding()
	 * @generated
	 */
	EReference getSetFragmentBinding_AppliedPattern();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragment <em>Set Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Set Fragment</em>'.
	 * @see de.upb.pose.mapping.SetFragmentBinding#getSetFragment()
	 * @see #getSetFragmentBinding()
	 * @generated
	 */
	EReference getSetFragmentBinding_SetFragment();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragmentInstances <em>Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Set Fragment Instances</em>'.
	 * @see de.upb.pose.mapping.SetFragmentBinding#getSetFragmentInstances()
	 * @see #getSetFragmentBinding()
	 * @generated
	 */
	EReference getSetFragmentBinding_SetFragmentInstances();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.SetFragmentInstance <em>Set Fragment Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Fragment Instance</em>'.
	 * @see de.upb.pose.mapping.SetFragmentInstance
	 * @generated
	 */
	EClass getSetFragmentInstance();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding <em>Parent Set Fragment Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Set Fragment Binding</em>'.
	 * @see de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding()
	 * @see #getSetFragmentInstance()
	 * @generated
	 */
	EReference getSetFragmentInstance_ParentSetFragmentBinding();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.mapping.SetFragmentInstance#getContainedRoleBindings <em>Contained Role Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contained Role Bindings</em>'.
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainedRoleBindings()
	 * @see #getSetFragmentInstance()
	 * @generated
	 */
	EReference getSetFragmentInstance_ContainedRoleBindings();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.mapping.SetFragmentInstance#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Containing Set Fragment Instances</em>'.
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainingSetFragmentInstances()
	 * @see #getSetFragmentInstance()
	 * @generated
	 */
	EReference getSetFragmentInstance_ContainingSetFragmentInstances();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.mapping.SetFragmentInstance#getContainedSetFragmentInstances <em>Contained Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contained Set Fragment Instances</em>'.
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainedSetFragmentInstances()
	 * @see #getSetFragmentInstance()
	 * @generated
	 */
	EReference getSetFragmentInstance_ContainedSetFragmentInstances();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.ApplicationModel <em>Application Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application Model</em>'.
	 * @see de.upb.pose.mapping.ApplicationModel
	 * @generated
	 */
	EClass getApplicationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.ApplicationModel#getDesignElements <em>Design Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Design Elements</em>'.
	 * @see de.upb.pose.mapping.ApplicationModel#getDesignElements()
	 * @see #getApplicationModel()
	 * @generated
	 */
	EReference getApplicationModel_DesignElements();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.ApplicationModel#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Applied Pattern</em>'.
	 * @see de.upb.pose.mapping.ApplicationModel#getAppliedPattern()
	 * @see #getApplicationModel()
	 * @generated
	 */
	EReference getApplicationModel_AppliedPattern();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.mapping.Token <em>Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Token</em>'.
	 * @see de.upb.pose.mapping.Token
	 * @generated
	 */
	EClass getToken();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.Token#getParentRoleBinding <em>Parent Role Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Role Binding</em>'.
	 * @see de.upb.pose.mapping.Token#getParentRoleBinding()
	 * @see #getToken()
	 * @generated
	 */
	EReference getToken_ParentRoleBinding();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.mapping.Token#getSubTokens <em>Sub Tokens</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Tokens</em>'.
	 * @see de.upb.pose.mapping.Token#getSubTokens()
	 * @see #getToken()
	 * @generated
	 */
	EReference getToken_SubTokens();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.mapping.Token#getParentToken <em>Parent Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Token</em>'.
	 * @see de.upb.pose.mapping.Token#getParentToken()
	 * @see #getToken()
	 * @generated
	 */
	EReference getToken_ParentToken();

	/**
	 * Returns the meta object for the map '{@link de.upb.pose.mapping.Token#getMapsTo <em>Maps To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Maps To</em>'.
	 * @see de.upb.pose.mapping.Token#getMapsTo()
	 * @see #getToken()
	 * @generated
	 */
	EReference getToken_MapsTo();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String EObject Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String EObject Map Entry</em>'.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	EClass getStringEObjectMapEntry();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringEObjectMapEntry()
	 * @generated
	 */
	EAttribute getStringEObjectMapEntry_Key();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getStringEObjectMapEntry()
	 * @generated
	 */
	EReference getStringEObjectMapEntry_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MappingFactory getMappingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.AppliedPatternImpl <em>Applied Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.AppliedPatternImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getAppliedPattern()
		 * @generated
		 */
		EClass APPLIED_PATTERN = eINSTANCE.getAppliedPattern();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLIED_PATTERN__PARENT = eINSTANCE.getAppliedPattern_Parent();

		/**
		 * The meta object literal for the '<em><b>Pattern Specification</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLIED_PATTERN__PATTERN_SPECIFICATION = eINSTANCE.getAppliedPattern_PatternSpecification();

		/**
		 * The meta object literal for the '<em><b>Role Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLIED_PATTERN__ROLE_BINDINGS = eINSTANCE.getAppliedPattern_RoleBindings();

		/**
		 * The meta object literal for the '<em><b>Set Fragment Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLIED_PATTERN__SET_FRAGMENT_BINDINGS = eINSTANCE.getAppliedPattern_SetFragmentBindings();

		/**
		 * The meta object literal for the '<em><b>Application Model</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLIED_PATTERN__APPLICATION_MODEL = eINSTANCE.getAppliedPattern_ApplicationModel();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.PatternApplicationsImpl <em>Pattern Applications</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.PatternApplicationsImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getPatternApplications()
		 * @generated
		 */
		EClass PATTERN_APPLICATIONS = eINSTANCE.getPatternApplications();

		/**
		 * The meta object literal for the '<em><b>Applications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_APPLICATIONS__APPLICATIONS = eINSTANCE.getPatternApplications_Applications();

		/**
		 * The meta object literal for the '<em><b>Design Model Root</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT = eINSTANCE.getPatternApplications_DesignModelRoot();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.RoleBindingImpl <em>Role Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.RoleBindingImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getRoleBinding()
		 * @generated
		 */
		EClass ROLE_BINDING = eINSTANCE.getRoleBinding();

		/**
		 * The meta object literal for the '<em><b>Model Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__MODEL_ELEMENTS = eINSTANCE.getRoleBinding_ModelElements();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__ROLE = eINSTANCE.getRoleBinding_Role();

		/**
		 * The meta object literal for the '<em><b>Applied Pattern</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__APPLIED_PATTERN = eINSTANCE.getRoleBinding_AppliedPattern();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__TASKS = eINSTANCE.getRoleBinding_Tasks();

		/**
		 * The meta object literal for the '<em><b>Containing Set Fragment Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES = eINSTANCE
				.getRoleBinding_ContainingSetFragmentInstances();

		/**
		 * The meta object literal for the '<em><b>Application Model Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__APPLICATION_MODEL_ELEMENT = eINSTANCE.getRoleBinding_ApplicationModelElement();

		/**
		 * The meta object literal for the '<em><b>New Element Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_BINDING__NEW_ELEMENT_NAME = eINSTANCE.getRoleBinding_NewElementName();

		/**
		 * The meta object literal for the '<em><b>Root Token</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_BINDING__ROOT_TOKEN = eINSTANCE.getRoleBinding_RootToken();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.TaskImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Task Description</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__TASK_DESCRIPTION = eINSTANCE.getTask_TaskDescription();

		/**
		 * The meta object literal for the '<em><b>Completed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__COMPLETED = eINSTANCE.getTask_Completed();

		/**
		 * The meta object literal for the '<em><b>Parent Role Binding</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__PARENT_ROLE_BINDING = eINSTANCE.getTask_ParentRoleBinding();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl <em>Set Fragment Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.SetFragmentBindingImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getSetFragmentBinding()
		 * @generated
		 */
		EClass SET_FRAGMENT_BINDING = eINSTANCE.getSetFragmentBinding();

		/**
		 * The meta object literal for the '<em><b>Applied Pattern</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_BINDING__APPLIED_PATTERN = eINSTANCE.getSetFragmentBinding_AppliedPattern();

		/**
		 * The meta object literal for the '<em><b>Set Fragment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_BINDING__SET_FRAGMENT = eINSTANCE.getSetFragmentBinding_SetFragment();

		/**
		 * The meta object literal for the '<em><b>Set Fragment Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES = eINSTANCE
				.getSetFragmentBinding_SetFragmentInstances();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl <em>Set Fragment Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.SetFragmentInstanceImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getSetFragmentInstance()
		 * @generated
		 */
		EClass SET_FRAGMENT_INSTANCE = eINSTANCE.getSetFragmentInstance();

		/**
		 * The meta object literal for the '<em><b>Parent Set Fragment Binding</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING = eINSTANCE
				.getSetFragmentInstance_ParentSetFragmentBinding();

		/**
		 * The meta object literal for the '<em><b>Contained Role Bindings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS = eINSTANCE
				.getSetFragmentInstance_ContainedRoleBindings();

		/**
		 * The meta object literal for the '<em><b>Containing Set Fragment Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES = eINSTANCE
				.getSetFragmentInstance_ContainingSetFragmentInstances();

		/**
		 * The meta object literal for the '<em><b>Contained Set Fragment Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES = eINSTANCE
				.getSetFragmentInstance_ContainedSetFragmentInstances();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.ApplicationModelImpl <em>Application Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.ApplicationModelImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getApplicationModel()
		 * @generated
		 */
		EClass APPLICATION_MODEL = eINSTANCE.getApplicationModel();

		/**
		 * The meta object literal for the '<em><b>Design Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_MODEL__DESIGN_ELEMENTS = eINSTANCE.getApplicationModel_DesignElements();

		/**
		 * The meta object literal for the '<em><b>Applied Pattern</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICATION_MODEL__APPLIED_PATTERN = eINSTANCE.getApplicationModel_AppliedPattern();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.TokenImpl <em>Token</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.TokenImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getToken()
		 * @generated
		 */
		EClass TOKEN = eINSTANCE.getToken();

		/**
		 * The meta object literal for the '<em><b>Parent Role Binding</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOKEN__PARENT_ROLE_BINDING = eINSTANCE.getToken_ParentRoleBinding();

		/**
		 * The meta object literal for the '<em><b>Sub Tokens</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOKEN__SUB_TOKENS = eINSTANCE.getToken_SubTokens();

		/**
		 * The meta object literal for the '<em><b>Parent Token</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOKEN__PARENT_TOKEN = eINSTANCE.getToken_ParentToken();

		/**
		 * The meta object literal for the '<em><b>Maps To</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOKEN__MAPS_TO = eINSTANCE.getToken_MapsTo();

		/**
		 * The meta object literal for the '{@link de.upb.pose.mapping.impl.StringEObjectMapEntryImpl <em>String EObject Map Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.mapping.impl.StringEObjectMapEntryImpl
		 * @see de.upb.pose.mapping.impl.MappingPackageImpl#getStringEObjectMapEntry()
		 * @generated
		 */
		EClass STRING_EOBJECT_MAP_ENTRY = eINSTANCE.getStringEObjectMapEntry();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_EOBJECT_MAP_ENTRY__KEY = eINSTANCE.getStringEObjectMapEntry_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRING_EOBJECT_MAP_ENTRY__VALUE = eINSTANCE.getStringEObjectMapEntry_Value();

	}

} //MappingPackage
