/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Named;
import de.upb.pose.specification.PatternSpecification;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Applied DesignPattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a single pattern application. i.e. a place in a software design model where a certain pattern is applied (implemented). An AppliedPattern object maps all pattern roles to software design model elements to completely document the pattern application and the pattern's participants.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.AppliedPattern#getParent <em>Parent</em>}</li>
 *   <li>{@link de.upb.pose.mapping.AppliedPattern#getPatternSpecification <em>Pattern Specification</em>}</li>
 *   <li>{@link de.upb.pose.mapping.AppliedPattern#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.AppliedPattern#getSetFragmentBindings <em>Set Fragment Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.AppliedPattern#getApplicationModel <em>Application Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern()
 * @generated
 */
public interface AppliedPattern extends Named {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.PatternApplications#getApplications <em>Applications</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(PatternApplications)
	 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern_Parent()
	 * @see de.upb.pose.mapping.PatternApplications#getApplications
	 * @generated
	 */
	PatternApplications getParent();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.AppliedPattern#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(PatternApplications value);

	/**
	 * Returns the value of the '<em><b>Pattern Specification</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DesignPattern PatternSpecification</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pattern Specification</em>' reference.
	 * @see #setPatternSpecification(PatternSpecification)
	 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern_PatternSpecification()
	 * @generated
	 */
	PatternSpecification getPatternSpecification();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.AppliedPattern#getPatternSpecification <em>Pattern Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pattern Specification</em>' reference.
	 * @see #getPatternSpecification()
	 * @generated
	 */
	void setPatternSpecification(PatternSpecification value);

	/**
	 * Returns the value of the '<em><b>Role Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.RoleBinding}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.RoleBinding#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Bindings</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern_RoleBindings()
	 * @see de.upb.pose.mapping.RoleBinding#getAppliedPattern
	 * @generated
	 */
	EList<RoleBinding> getRoleBindings();

	/**
	 * Returns the value of the '<em><b>Set Fragment Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.SetFragmentBinding}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Fragment Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Fragment Bindings</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern_SetFragmentBindings()
	 * @see de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern
	 * @generated
	 */
	EList<SetFragmentBinding> getSetFragmentBindings();

	/**
	 * Returns the value of the '<em><b>Application Model</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.ApplicationModel#getAppliedPattern <em>Applied Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application Model</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application Model</em>' containment reference.
	 * @see #setApplicationModel(ApplicationModel)
	 * @see de.upb.pose.mapping.MappingPackage#getAppliedPattern_ApplicationModel()
	 * @see de.upb.pose.mapping.ApplicationModel#getAppliedPattern
	 * @generated
	 */
	ApplicationModel getApplicationModel();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.AppliedPattern#getApplicationModel <em>Application Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application Model</em>' containment reference.
	 * @see #getApplicationModel()
	 * @generated
	 */
	void setApplicationModel(ApplicationModel value);

} // AppliedPattern
