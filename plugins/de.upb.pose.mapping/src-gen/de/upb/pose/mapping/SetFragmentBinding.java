/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import de.upb.pose.core.Named;

import de.upb.pose.specification.SetFragment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Fragment Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A set binding represents all realizations of the software design that is defined in a set fragment, i.e. all role bindings for roles in a certain set fragment for a certain pattern application. Each occurrence/realization of the elements in a set fragment is represented by a set element binding.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern <em>Applied Pattern</em>}</li>
 *   <li>{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragment <em>Set Fragment</em>}</li>
 *   <li>{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragmentInstances <em>Set Fragment Instances</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentBinding()
 * @generated
 */
public interface SetFragmentBinding extends Named {
	/**
	 * Returns the value of the '<em><b>Applied Pattern</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.AppliedPattern#getSetFragmentBindings <em>Set Fragment Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applied Pattern</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applied Pattern</em>' container reference.
	 * @see #setAppliedPattern(AppliedPattern)
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentBinding_AppliedPattern()
	 * @see de.upb.pose.mapping.AppliedPattern#getSetFragmentBindings
	 * @generated
	 */
	AppliedPattern getAppliedPattern();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.SetFragmentBinding#getAppliedPattern <em>Applied Pattern</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applied Pattern</em>' container reference.
	 * @see #getAppliedPattern()
	 * @generated
	 */
	void setAppliedPattern(AppliedPattern value);

	/**
	 * Returns the value of the '<em><b>Set Fragment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Fragment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Fragment</em>' reference.
	 * @see #setSetFragment(SetFragment)
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentBinding_SetFragment()
	 * @generated
	 */
	SetFragment getSetFragment();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragment <em>Set Fragment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Set Fragment</em>' reference.
	 * @see #getSetFragment()
	 * @generated
	 */
	void setSetFragment(SetFragment value);

	/**
	 * Returns the value of the '<em><b>Set Fragment Instances</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.SetFragmentInstance}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding <em>Parent Set Fragment Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Fragment Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Fragment Instances</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentBinding_SetFragmentInstances()
	 * @see de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding
	 * @generated
	 */
	EList<SetFragmentInstance> getSetFragmentInstances();

} // SetFragmentBinding
