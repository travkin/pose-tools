/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import de.upb.pose.core.Named;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Fragment Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents an occurrence/realization of the elements in a set fragment, i.e. it contains role bindings for each of the pattern roles in the corresponding set fragment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding <em>Parent Set Fragment Binding</em>}</li>
 *   <li>{@link de.upb.pose.mapping.SetFragmentInstance#getContainedRoleBindings <em>Contained Role Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.SetFragmentInstance#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}</li>
 *   <li>{@link de.upb.pose.mapping.SetFragmentInstance#getContainedSetFragmentInstances <em>Contained Set Fragment Instances</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentInstance()
 * @generated
 */
public interface SetFragmentInstance extends Named {
	/**
	 * Returns the value of the '<em><b>Parent Set Fragment Binding</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentBinding#getSetFragmentInstances <em>Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Set Fragment Binding</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Set Fragment Binding</em>' container reference.
	 * @see #setParentSetFragmentBinding(SetFragmentBinding)
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentInstance_ParentSetFragmentBinding()
	 * @see de.upb.pose.mapping.SetFragmentBinding#getSetFragmentInstances
	 * @generated
	 */
	SetFragmentBinding getParentSetFragmentBinding();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.SetFragmentInstance#getParentSetFragmentBinding <em>Parent Set Fragment Binding</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Set Fragment Binding</em>' container reference.
	 * @see #getParentSetFragmentBinding()
	 * @generated
	 */
	void setParentSetFragmentBinding(SetFragmentBinding value);

	/**
	 * Returns the value of the '<em><b>Contained Role Bindings</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.RoleBinding}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.RoleBinding#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Role Bindings</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Role Bindings</em>' reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentInstance_ContainedRoleBindings()
	 * @see de.upb.pose.mapping.RoleBinding#getContainingSetFragmentInstances
	 * @generated
	 */
	EList<RoleBinding> getContainedRoleBindings();

	/**
	 * Returns the value of the '<em><b>Containing Set Fragment Instances</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.SetFragmentInstance}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentInstance#getContainedSetFragmentInstances <em>Contained Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Set Fragment Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Set Fragment Instances</em>' reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentInstance_ContainingSetFragmentInstances()
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainedSetFragmentInstances
	 * @generated
	 */
	EList<SetFragmentInstance> getContainingSetFragmentInstances();

	/**
	 * Returns the value of the '<em><b>Contained Set Fragment Instances</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.SetFragmentInstance}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.SetFragmentInstance#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Set Fragment Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Set Fragment Instances</em>' reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getSetFragmentInstance_ContainedSetFragmentInstances()
	 * @see de.upb.pose.mapping.SetFragmentInstance#getContainingSetFragmentInstances
	 * @generated
	 */
	EList<SetFragmentInstance> getContainedSetFragmentInstances();

} // SetFragmentInstance
