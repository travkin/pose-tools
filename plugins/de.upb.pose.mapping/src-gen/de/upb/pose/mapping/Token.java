/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.Named;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Token</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a conceptional part of the realization of a pattern role. E.g. an action describes a behavior on a very high abstraction level. Tokens can be used to represent certain steps of the behavior to step-wise translate the abstract design model described in a pattern specification to the actual design in the design model.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.Token#getParentRoleBinding <em>Parent Role Binding</em>}</li>
 *   <li>{@link de.upb.pose.mapping.Token#getSubTokens <em>Sub Tokens</em>}</li>
 *   <li>{@link de.upb.pose.mapping.Token#getParentToken <em>Parent Token</em>}</li>
 *   <li>{@link de.upb.pose.mapping.Token#getMapsTo <em>Maps To</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.mapping.MappingPackage#getToken()
 * @generated
 */
public interface Token extends Named {
	/**
	 * Returns the value of the '<em><b>Parent Role Binding</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.RoleBinding#getRootToken <em>Root Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Role Binding</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Role Binding</em>' container reference.
	 * @see #setParentRoleBinding(RoleBinding)
	 * @see de.upb.pose.mapping.MappingPackage#getToken_ParentRoleBinding()
	 * @see de.upb.pose.mapping.RoleBinding#getRootToken
	 * @generated
	 */
	RoleBinding getParentRoleBinding();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.Token#getParentRoleBinding <em>Parent Role Binding</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Role Binding</em>' container reference.
	 * @see #getParentRoleBinding()
	 * @generated
	 */
	void setParentRoleBinding(RoleBinding value);

	/**
	 * Returns the value of the '<em><b>Sub Tokens</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.mapping.Token}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.Token#getParentToken <em>Parent Token</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Tokens</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * A token can be sub-divided into further sub-tokens, e.g. a task can be built of several sub-tasks. The sub-tokens represent smaller parts of this token's concept.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sub Tokens</em>' containment reference list.
	 * @see de.upb.pose.mapping.MappingPackage#getToken_SubTokens()
	 * @see de.upb.pose.mapping.Token#getParentToken
	 * @generated
	 */
	EList<Token> getSubTokens();

	/**
	 * Returns the value of the '<em><b>Parent Token</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.mapping.Token#getSubTokens <em>Sub Tokens</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Token</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Token</em>' container reference.
	 * @see #setParentToken(Token)
	 * @see de.upb.pose.mapping.MappingPackage#getToken_ParentToken()
	 * @see de.upb.pose.mapping.Token#getSubTokens
	 * @generated
	 */
	Token getParentToken();

	/**
	 * Sets the value of the '{@link de.upb.pose.mapping.Token#getParentToken <em>Parent Token</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Token</em>' container reference.
	 * @see #getParentToken()
	 * @generated
	 */
	void setParentToken(Token value);

	/**
	 * Returns the value of the '<em><b>Maps To</b></em>' map.
	 * The key is of type {@link java.lang.String},
	 * and the value is of type {@link org.eclipse.emf.ecore.EObject},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maps To</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maps To</em>' map.
	 * @see de.upb.pose.mapping.MappingPackage#getToken_MapsTo()
	 * @generated
	 */
	EMap<String, EObject> getMapsTo();

} // Token
