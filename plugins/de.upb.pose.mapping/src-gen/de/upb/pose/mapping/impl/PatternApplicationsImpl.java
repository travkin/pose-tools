/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.PatternApplications;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DesignPattern Applications</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.PatternApplicationsImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.PatternApplicationsImpl#getApplications <em>Applications</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.PatternApplicationsImpl#getDesignModelRoot <em>Design Model Root</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PatternApplicationsImpl extends IdentifierImpl implements PatternApplications {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getApplications() <em>Applications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplications()
	 * @generated
	 * @ordered
	 */
	protected EList<AppliedPattern> applications;

	/**
	 * The cached value of the '{@link #getDesignModelRoot() <em>Design Model Root</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesignModelRoot()
	 * @generated
	 * @ordered
	 */
	protected EObject designModelRoot;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternApplicationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.PATTERN_APPLICATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.PATTERN_APPLICATIONS__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AppliedPattern> getApplications() {
		if (applications == null) {
			applications = new EObjectContainmentWithInverseEList<AppliedPattern>(AppliedPattern.class, this,
					MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS, MappingPackage.APPLIED_PATTERN__PARENT);
		}
		return applications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getDesignModelRoot() {
		if (designModelRoot != null && designModelRoot.eIsProxy()) {
			InternalEObject oldDesignModelRoot = (InternalEObject) designModelRoot;
			designModelRoot = eResolveProxy(oldDesignModelRoot);
			if (designModelRoot != oldDesignModelRoot) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT, oldDesignModelRoot, designModelRoot));
			}
		}
		return designModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetDesignModelRoot() {
		return designModelRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDesignModelRoot(EObject newDesignModelRoot) {
		EObject oldDesignModelRoot = designModelRoot;
		designModelRoot = newDesignModelRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT, oldDesignModelRoot, designModelRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getApplications()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			return ((InternalEList<?>) getApplications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__NAME:
			return getName();
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			return getApplications();
		case MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT:
			if (resolve)
				return getDesignModelRoot();
			return basicGetDesignModelRoot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			getApplications().clear();
			getApplications().addAll((Collection<? extends AppliedPattern>) newValue);
			return;
		case MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT:
			setDesignModelRoot((EObject) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			getApplications().clear();
			return;
		case MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT:
			setDesignModelRoot((EObject) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.PATTERN_APPLICATIONS__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS:
			return applications != null && !applications.isEmpty();
		case MappingPackage.PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT:
			return designModelRoot != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //PatternApplicationsImpl
