/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.specification.DesignElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Application Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.ApplicationModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.ApplicationModelImpl#getDesignElements <em>Design Elements</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.ApplicationModelImpl#getAppliedPattern <em>Applied Pattern</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ApplicationModelImpl extends IdentifierImpl implements ApplicationModel {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDesignElements() <em>Design Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesignElements()
	 * @generated
	 * @ordered
	 */
	protected EList<DesignElement> designElements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.APPLICATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.APPLICATION_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DesignElement> getDesignElements() {
		if (designElements == null) {
			designElements = new EObjectContainmentEList<DesignElement>(DesignElement.class, this,
					MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS);
		}
		return designElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AppliedPattern getAppliedPattern() {
		if (eContainerFeatureID() != MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN)
			return null;
		return (AppliedPattern) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAppliedPattern(AppliedPattern newAppliedPattern, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newAppliedPattern,
				MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedPattern(AppliedPattern newAppliedPattern) {
		if (newAppliedPattern != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN && newAppliedPattern != null)) {
			if (EcoreUtil.isAncestor(this, newAppliedPattern))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAppliedPattern != null)
				msgs = ((InternalEObject) newAppliedPattern).eInverseAdd(this,
						MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL, AppliedPattern.class, msgs);
			msgs = basicSetAppliedPattern(newAppliedPattern, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN,
					newAppliedPattern, newAppliedPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetAppliedPattern((AppliedPattern) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS:
			return ((InternalEList<?>) getDesignElements()).basicRemove(otherEnd, msgs);
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			return basicSetAppliedPattern(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			return eInternalContainer().eInverseRemove(this, MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL,
					AppliedPattern.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__NAME:
			return getName();
		case MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS:
			return getDesignElements();
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			return getAppliedPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS:
			getDesignElements().clear();
			getDesignElements().addAll((Collection<? extends DesignElement>) newValue);
			return;
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS:
			getDesignElements().clear();
			return;
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.APPLICATION_MODEL__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.APPLICATION_MODEL__DESIGN_ELEMENTS:
			return designElements != null && !designElements.isEmpty();
		case MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN:
			return getAppliedPattern() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ApplicationModelImpl
