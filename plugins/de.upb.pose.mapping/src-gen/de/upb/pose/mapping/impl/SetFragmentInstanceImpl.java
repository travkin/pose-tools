/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import de.upb.pose.core.impl.IdentifierImpl;

import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Fragment Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl#getParentSetFragmentBinding <em>Parent Set Fragment Binding</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl#getContainedRoleBindings <em>Contained Role Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentInstanceImpl#getContainedSetFragmentInstances <em>Contained Set Fragment Instances</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SetFragmentInstanceImpl extends IdentifierImpl implements SetFragmentInstance {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainedRoleBindings() <em>Contained Role Bindings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<RoleBinding> containedRoleBindings;

	/**
	 * The cached value of the '{@link #getContainingSetFragmentInstances() <em>Containing Set Fragment Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSetFragmentInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentInstance> containingSetFragmentInstances;

	/**
	 * The cached value of the '{@link #getContainedSetFragmentInstances() <em>Contained Set Fragment Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedSetFragmentInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentInstance> containedSetFragmentInstances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetFragmentInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.SET_FRAGMENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.SET_FRAGMENT_INSTANCE__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetFragmentBinding getParentSetFragmentBinding() {
		if (eContainerFeatureID() != MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING)
			return null;
		return (SetFragmentBinding) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentSetFragmentBinding(SetFragmentBinding newParentSetFragmentBinding,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentSetFragmentBinding,
				MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentSetFragmentBinding(SetFragmentBinding newParentSetFragmentBinding) {
		if (newParentSetFragmentBinding != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING && newParentSetFragmentBinding != null)) {
			if (EcoreUtil.isAncestor(this, newParentSetFragmentBinding))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentSetFragmentBinding != null)
				msgs = ((InternalEObject) newParentSetFragmentBinding).eInverseAdd(this,
						MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES, SetFragmentBinding.class, msgs);
			msgs = basicSetParentSetFragmentBinding(newParentSetFragmentBinding, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING, newParentSetFragmentBinding,
					newParentSetFragmentBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoleBinding> getContainedRoleBindings() {
		if (containedRoleBindings == null) {
			containedRoleBindings = new EObjectWithInverseResolvingEList.ManyInverse<RoleBinding>(RoleBinding.class,
					this, MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS,
					MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES);
		}
		return containedRoleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentInstance> getContainingSetFragmentInstances() {
		if (containingSetFragmentInstances == null) {
			containingSetFragmentInstances = new EObjectWithInverseResolvingEList.ManyInverse<SetFragmentInstance>(
					SetFragmentInstance.class, this,
					MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES,
					MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES);
		}
		return containingSetFragmentInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentInstance> getContainedSetFragmentInstances() {
		if (containedSetFragmentInstances == null) {
			containedSetFragmentInstances = new EObjectWithInverseResolvingEList.ManyInverse<SetFragmentInstance>(
					SetFragmentInstance.class, this,
					MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES,
					MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES);
		}
		return containedSetFragmentInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentSetFragmentBinding((SetFragmentBinding) otherEnd, msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainedRoleBindings()).basicAdd(otherEnd,
					msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainingSetFragmentInstances()).basicAdd(
					otherEnd, msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainedSetFragmentInstances()).basicAdd(
					otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			return basicSetParentSetFragmentBinding(null, msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			return ((InternalEList<?>) getContainedRoleBindings()).basicRemove(otherEnd, msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<?>) getContainingSetFragmentInstances()).basicRemove(otherEnd, msgs);
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<?>) getContainedSetFragmentInstances()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			return eInternalContainer().eInverseRemove(this,
					MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES, SetFragmentBinding.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__NAME:
			return getName();
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			return getParentSetFragmentBinding();
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			return getContainedRoleBindings();
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			return getContainingSetFragmentInstances();
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			return getContainedSetFragmentInstances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			setParentSetFragmentBinding((SetFragmentBinding) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			getContainedRoleBindings().clear();
			getContainedRoleBindings().addAll((Collection<? extends RoleBinding>) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			getContainingSetFragmentInstances().clear();
			getContainingSetFragmentInstances().addAll((Collection<? extends SetFragmentInstance>) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			getContainedSetFragmentInstances().clear();
			getContainedSetFragmentInstances().addAll((Collection<? extends SetFragmentInstance>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			setParentSetFragmentBinding((SetFragmentBinding) null);
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			getContainedRoleBindings().clear();
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			getContainingSetFragmentInstances().clear();
			return;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			getContainedSetFragmentInstances().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_INSTANCE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING:
			return getParentSetFragmentBinding() != null;
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS:
			return containedRoleBindings != null && !containedRoleBindings.isEmpty();
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES:
			return containingSetFragmentInstances != null && !containingSetFragmentInstances.isEmpty();
		case MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES:
			return containedSetFragmentInstances != null && !containedSetFragmentInstances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SetFragmentInstanceImpl
