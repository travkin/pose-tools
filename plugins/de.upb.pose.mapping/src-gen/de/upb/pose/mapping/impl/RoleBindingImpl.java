/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.mapping.Task;
import de.upb.pose.mapping.Token;
import de.upb.pose.specification.DesignElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Role Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getModelElements <em>Model Elements</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getRole <em>Role</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getAppliedPattern <em>Applied Pattern</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getTasks <em>Tasks</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getContainingSetFragmentInstances <em>Containing Set Fragment Instances</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getApplicationModelElement <em>Application Model Element</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getNewElementName <em>New Element Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.RoleBindingImpl#getRootToken <em>Root Token</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RoleBindingImpl extends IdentifierImpl implements RoleBinding {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModelElements() <em>Model Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelElements()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> modelElements;

	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected DesignElement role;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> tasks;

	/**
	 * The cached value of the '{@link #getContainingSetFragmentInstances() <em>Containing Set Fragment Instances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSetFragmentInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentInstance> containingSetFragmentInstances;

	/**
	 * The cached value of the '{@link #getApplicationModelElement() <em>Application Model Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationModelElement()
	 * @generated
	 * @ordered
	 */
	protected DesignElement applicationModelElement;

	/**
	 * The default value of the '{@link #getNewElementName() <em>New Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewElementName()
	 * @generated
	 * @ordered
	 */
	protected static final String NEW_ELEMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNewElementName() <em>New Element Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewElementName()
	 * @generated
	 * @ordered
	 */
	protected String newElementName = NEW_ELEMENT_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRootToken() <em>Root Token</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootToken()
	 * @generated
	 * @ordered
	 */
	protected Token rootToken;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoleBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.ROLE_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.ROLE_BINDING__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getModelElements() {
		if (modelElements == null) {
			modelElements = new EObjectResolvingEList<EObject>(EObject.class, this,
					MappingPackage.ROLE_BINDING__MODEL_ELEMENTS);
		}
		return modelElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignElement getRole() {
		if (role != null && role.eIsProxy()) {
			InternalEObject oldRole = (InternalEObject) role;
			role = (DesignElement) eResolveProxy(oldRole);
			if (role != oldRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MappingPackage.ROLE_BINDING__ROLE,
							oldRole, role));
			}
		}
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignElement basicGetRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRole(DesignElement newRole) {
		DesignElement oldRole = role;
		role = newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.ROLE_BINDING__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AppliedPattern getAppliedPattern() {
		if (eContainerFeatureID() != MappingPackage.ROLE_BINDING__APPLIED_PATTERN)
			return null;
		return (AppliedPattern) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAppliedPattern(AppliedPattern newAppliedPattern, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newAppliedPattern, MappingPackage.ROLE_BINDING__APPLIED_PATTERN,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedPattern(AppliedPattern newAppliedPattern) {
		if (newAppliedPattern != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.ROLE_BINDING__APPLIED_PATTERN && newAppliedPattern != null)) {
			if (EcoreUtil.isAncestor(this, newAppliedPattern))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAppliedPattern != null)
				msgs = ((InternalEObject) newAppliedPattern).eInverseAdd(this,
						MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS, AppliedPattern.class, msgs);
			msgs = basicSetAppliedPattern(newAppliedPattern, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.ROLE_BINDING__APPLIED_PATTERN,
					newAppliedPattern, newAppliedPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentWithInverseEList<Task>(Task.class, this, MappingPackage.ROLE_BINDING__TASKS,
					MappingPackage.TASK__PARENT_ROLE_BINDING);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentInstance> getContainingSetFragmentInstances() {
		if (containingSetFragmentInstances == null) {
			containingSetFragmentInstances = new EObjectWithInverseResolvingEList.ManyInverse<SetFragmentInstance>(
					SetFragmentInstance.class, this, MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES,
					MappingPackage.SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS);
		}
		return containingSetFragmentInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignElement getApplicationModelElement() {
		if (applicationModelElement != null && applicationModelElement.eIsProxy()) {
			InternalEObject oldApplicationModelElement = (InternalEObject) applicationModelElement;
			applicationModelElement = (DesignElement) eResolveProxy(oldApplicationModelElement);
			if (applicationModelElement != oldApplicationModelElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT, oldApplicationModelElement,
							applicationModelElement));
			}
		}
		return applicationModelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignElement basicGetApplicationModelElement() {
		return applicationModelElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationModelElement(DesignElement newApplicationModelElement) {
		DesignElement oldApplicationModelElement = applicationModelElement;
		applicationModelElement = newApplicationModelElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT, oldApplicationModelElement,
					applicationModelElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNewElementName() {
		return newElementName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNewElementName(String newNewElementName) {
		String oldNewElementName = newElementName;
		newElementName = newNewElementName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.ROLE_BINDING__NEW_ELEMENT_NAME,
					oldNewElementName, newElementName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Token getRootToken() {
		return rootToken;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootToken(Token newRootToken, NotificationChain msgs) {
		Token oldRootToken = rootToken;
		rootToken = newRootToken;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MappingPackage.ROLE_BINDING__ROOT_TOKEN, oldRootToken, newRootToken);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootToken(Token newRootToken) {
		if (newRootToken != rootToken) {
			NotificationChain msgs = null;
			if (rootToken != null)
				msgs = ((InternalEObject) rootToken).eInverseRemove(this, MappingPackage.TOKEN__PARENT_ROLE_BINDING,
						Token.class, msgs);
			if (newRootToken != null)
				msgs = ((InternalEObject) newRootToken).eInverseAdd(this, MappingPackage.TOKEN__PARENT_ROLE_BINDING,
						Token.class, msgs);
			msgs = basicSetRootToken(newRootToken, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.ROLE_BINDING__ROOT_TOKEN,
					newRootToken, newRootToken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetAppliedPattern((AppliedPattern) otherEnd, msgs);
		case MappingPackage.ROLE_BINDING__TASKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getTasks()).basicAdd(otherEnd, msgs);
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainingSetFragmentInstances()).basicAdd(
					otherEnd, msgs);
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			if (rootToken != null)
				msgs = ((InternalEObject) rootToken).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- MappingPackage.ROLE_BINDING__ROOT_TOKEN, null, msgs);
			return basicSetRootToken((Token) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			return basicSetAppliedPattern(null, msgs);
		case MappingPackage.ROLE_BINDING__TASKS:
			return ((InternalEList<?>) getTasks()).basicRemove(otherEnd, msgs);
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			return ((InternalEList<?>) getContainingSetFragmentInstances()).basicRemove(otherEnd, msgs);
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			return basicSetRootToken(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			return eInternalContainer().eInverseRemove(this, MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS,
					AppliedPattern.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__NAME:
			return getName();
		case MappingPackage.ROLE_BINDING__MODEL_ELEMENTS:
			return getModelElements();
		case MappingPackage.ROLE_BINDING__ROLE:
			if (resolve)
				return getRole();
			return basicGetRole();
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			return getAppliedPattern();
		case MappingPackage.ROLE_BINDING__TASKS:
			return getTasks();
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			return getContainingSetFragmentInstances();
		case MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT:
			if (resolve)
				return getApplicationModelElement();
			return basicGetApplicationModelElement();
		case MappingPackage.ROLE_BINDING__NEW_ELEMENT_NAME:
			return getNewElementName();
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			return getRootToken();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.ROLE_BINDING__MODEL_ELEMENTS:
			getModelElements().clear();
			getModelElements().addAll((Collection<? extends EObject>) newValue);
			return;
		case MappingPackage.ROLE_BINDING__ROLE:
			setRole((DesignElement) newValue);
			return;
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) newValue);
			return;
		case MappingPackage.ROLE_BINDING__TASKS:
			getTasks().clear();
			getTasks().addAll((Collection<? extends Task>) newValue);
			return;
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			getContainingSetFragmentInstances().clear();
			getContainingSetFragmentInstances().addAll((Collection<? extends SetFragmentInstance>) newValue);
			return;
		case MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT:
			setApplicationModelElement((DesignElement) newValue);
			return;
		case MappingPackage.ROLE_BINDING__NEW_ELEMENT_NAME:
			setNewElementName((String) newValue);
			return;
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			setRootToken((Token) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.ROLE_BINDING__MODEL_ELEMENTS:
			getModelElements().clear();
			return;
		case MappingPackage.ROLE_BINDING__ROLE:
			setRole((DesignElement) null);
			return;
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) null);
			return;
		case MappingPackage.ROLE_BINDING__TASKS:
			getTasks().clear();
			return;
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			getContainingSetFragmentInstances().clear();
			return;
		case MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT:
			setApplicationModelElement((DesignElement) null);
			return;
		case MappingPackage.ROLE_BINDING__NEW_ELEMENT_NAME:
			setNewElementName(NEW_ELEMENT_NAME_EDEFAULT);
			return;
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			setRootToken((Token) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.ROLE_BINDING__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.ROLE_BINDING__MODEL_ELEMENTS:
			return modelElements != null && !modelElements.isEmpty();
		case MappingPackage.ROLE_BINDING__ROLE:
			return role != null;
		case MappingPackage.ROLE_BINDING__APPLIED_PATTERN:
			return getAppliedPattern() != null;
		case MappingPackage.ROLE_BINDING__TASKS:
			return tasks != null && !tasks.isEmpty();
		case MappingPackage.ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES:
			return containingSetFragmentInstances != null && !containingSetFragmentInstances.isEmpty();
		case MappingPackage.ROLE_BINDING__APPLICATION_MODEL_ELEMENT:
			return applicationModelElement != null;
		case MappingPackage.ROLE_BINDING__NEW_ELEMENT_NAME:
			return NEW_ELEMENT_NAME_EDEFAULT == null ? newElementName != null : !NEW_ELEMENT_NAME_EDEFAULT
					.equals(newElementName);
		case MappingPackage.ROLE_BINDING__ROOT_TOKEN:
			return rootToken != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", newElementName: "); //$NON-NLS-1$
		result.append(newElementName);
		result.append(')');
		return result.toString();
	}

} //RoleBindingImpl
