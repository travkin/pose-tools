/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import de.upb.pose.core.impl.IdentifierImpl;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;

import de.upb.pose.specification.SetFragment;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Fragment Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl#getAppliedPattern <em>Applied Pattern</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl#getSetFragment <em>Set Fragment</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.SetFragmentBindingImpl#getSetFragmentInstances <em>Set Fragment Instances</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SetFragmentBindingImpl extends IdentifierImpl implements SetFragmentBinding {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSetFragment() <em>Set Fragment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetFragment()
	 * @generated
	 * @ordered
	 */
	protected SetFragment setFragment;

	/**
	 * The cached value of the '{@link #getSetFragmentInstances() <em>Set Fragment Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetFragmentInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentInstance> setFragmentInstances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetFragmentBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.SET_FRAGMENT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.SET_FRAGMENT_BINDING__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AppliedPattern getAppliedPattern() {
		if (eContainerFeatureID() != MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN)
			return null;
		return (AppliedPattern) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAppliedPattern(AppliedPattern newAppliedPattern, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newAppliedPattern,
				MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppliedPattern(AppliedPattern newAppliedPattern) {
		if (newAppliedPattern != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN && newAppliedPattern != null)) {
			if (EcoreUtil.isAncestor(this, newAppliedPattern))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAppliedPattern != null)
				msgs = ((InternalEObject) newAppliedPattern).eInverseAdd(this,
						MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS, AppliedPattern.class, msgs);
			msgs = basicSetAppliedPattern(newAppliedPattern, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN,
					newAppliedPattern, newAppliedPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetFragment getSetFragment() {
		if (setFragment != null && setFragment.eIsProxy()) {
			InternalEObject oldSetFragment = (InternalEObject) setFragment;
			setFragment = (SetFragment) eResolveProxy(oldSetFragment);
			if (setFragment != oldSetFragment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT, oldSetFragment, setFragment));
			}
		}
		return setFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetFragment basicGetSetFragment() {
		return setFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSetFragment(SetFragment newSetFragment) {
		SetFragment oldSetFragment = setFragment;
		setFragment = newSetFragment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT,
					oldSetFragment, setFragment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentInstance> getSetFragmentInstances() {
		if (setFragmentInstances == null) {
			setFragmentInstances = new EObjectContainmentWithInverseEList<SetFragmentInstance>(
					SetFragmentInstance.class, this, MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES,
					MappingPackage.SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING);
		}
		return setFragmentInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetAppliedPattern((AppliedPattern) otherEnd, msgs);
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSetFragmentInstances()).basicAdd(otherEnd,
					msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			return basicSetAppliedPattern(null, msgs);
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			return ((InternalEList<?>) getSetFragmentInstances()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			return eInternalContainer().eInverseRemove(this, MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS,
					AppliedPattern.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__NAME:
			return getName();
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			return getAppliedPattern();
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT:
			if (resolve)
				return getSetFragment();
			return basicGetSetFragment();
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			return getSetFragmentInstances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT:
			setSetFragment((SetFragment) newValue);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			getSetFragmentInstances().clear();
			getSetFragmentInstances().addAll((Collection<? extends SetFragmentInstance>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			setAppliedPattern((AppliedPattern) null);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT:
			setSetFragment((SetFragment) null);
			return;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			getSetFragmentInstances().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.SET_FRAGMENT_BINDING__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN:
			return getAppliedPattern() != null;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT:
			return setFragment != null;
		case MappingPackage.SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES:
			return setFragmentInstances != null && !setFragmentInstances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //SetFragmentBindingImpl
