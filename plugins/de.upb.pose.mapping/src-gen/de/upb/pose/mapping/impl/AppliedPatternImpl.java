/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.specification.PatternSpecification;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Applied DesignPattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getPatternSpecification <em>Pattern Specification</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getRoleBindings <em>Role Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getSetFragmentBindings <em>Set Fragment Bindings</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.AppliedPatternImpl#getApplicationModel <em>Application Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AppliedPatternImpl extends IdentifierImpl implements AppliedPattern {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPatternSpecification() <em>Pattern Specification</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPatternSpecification()
	 * @generated
	 * @ordered
	 */
	protected PatternSpecification patternSpecification;

	/**
	 * The cached value of the '{@link #getRoleBindings() <em>Role Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoleBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<RoleBinding> roleBindings;

	/**
	 * The cached value of the '{@link #getSetFragmentBindings() <em>Set Fragment Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetFragmentBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentBinding> setFragmentBindings;

	/**
	 * The cached value of the '{@link #getApplicationModel() <em>Application Model</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationModel()
	 * @generated
	 * @ordered
	 */
	protected ApplicationModel applicationModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AppliedPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.APPLIED_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.APPLIED_PATTERN__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternApplications getParent() {
		if (eContainerFeatureID() != MappingPackage.APPLIED_PATTERN__PARENT)
			return null;
		return (PatternApplications) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(PatternApplications newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParent, MappingPackage.APPLIED_PATTERN__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(PatternApplications newParent) {
		if (newParent != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.APPLIED_PATTERN__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject) newParent).eInverseAdd(this,
						MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS, PatternApplications.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.APPLIED_PATTERN__PARENT, newParent,
					newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternSpecification getPatternSpecification() {
		if (patternSpecification != null && patternSpecification.eIsProxy()) {
			InternalEObject oldPatternSpecification = (InternalEObject) patternSpecification;
			patternSpecification = (PatternSpecification) eResolveProxy(oldPatternSpecification);
			if (patternSpecification != oldPatternSpecification) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION, oldPatternSpecification,
							patternSpecification));
			}
		}
		return patternSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternSpecification basicGetPatternSpecification() {
		return patternSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPatternSpecification(PatternSpecification newPatternSpecification) {
		PatternSpecification oldPatternSpecification = patternSpecification;
		patternSpecification = newPatternSpecification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION, oldPatternSpecification,
					patternSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoleBinding> getRoleBindings() {
		if (roleBindings == null) {
			roleBindings = new EObjectContainmentWithInverseEList<RoleBinding>(RoleBinding.class, this,
					MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS, MappingPackage.ROLE_BINDING__APPLIED_PATTERN);
		}
		return roleBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentBinding> getSetFragmentBindings() {
		if (setFragmentBindings == null) {
			setFragmentBindings = new EObjectContainmentWithInverseEList<SetFragmentBinding>(SetFragmentBinding.class,
					this, MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS,
					MappingPackage.SET_FRAGMENT_BINDING__APPLIED_PATTERN);
		}
		return setFragmentBindings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationModel getApplicationModel() {
		return applicationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApplicationModel(ApplicationModel newApplicationModel, NotificationChain msgs) {
		ApplicationModel oldApplicationModel = applicationModel;
		applicationModel = newApplicationModel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL, oldApplicationModel, newApplicationModel);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicationModel(ApplicationModel newApplicationModel) {
		if (newApplicationModel != applicationModel) {
			NotificationChain msgs = null;
			if (applicationModel != null)
				msgs = ((InternalEObject) applicationModel).eInverseRemove(this,
						MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN, ApplicationModel.class, msgs);
			if (newApplicationModel != null)
				msgs = ((InternalEObject) newApplicationModel).eInverseAdd(this,
						MappingPackage.APPLICATION_MODEL__APPLIED_PATTERN, ApplicationModel.class, msgs);
			msgs = basicSetApplicationModel(newApplicationModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL,
					newApplicationModel, newApplicationModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__PARENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParent((PatternApplications) otherEnd, msgs);
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getRoleBindings()).basicAdd(otherEnd, msgs);
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSetFragmentBindings()).basicAdd(otherEnd,
					msgs);
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			if (applicationModel != null)
				msgs = ((InternalEObject) applicationModel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL, null, msgs);
			return basicSetApplicationModel((ApplicationModel) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__PARENT:
			return basicSetParent(null, msgs);
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			return ((InternalEList<?>) getRoleBindings()).basicRemove(otherEnd, msgs);
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			return ((InternalEList<?>) getSetFragmentBindings()).basicRemove(otherEnd, msgs);
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			return basicSetApplicationModel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.APPLIED_PATTERN__PARENT:
			return eInternalContainer().eInverseRemove(this, MappingPackage.PATTERN_APPLICATIONS__APPLICATIONS,
					PatternApplications.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__NAME:
			return getName();
		case MappingPackage.APPLIED_PATTERN__PARENT:
			return getParent();
		case MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION:
			if (resolve)
				return getPatternSpecification();
			return basicGetPatternSpecification();
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			return getRoleBindings();
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			return getSetFragmentBindings();
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			return getApplicationModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.APPLIED_PATTERN__PARENT:
			setParent((PatternApplications) newValue);
			return;
		case MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION:
			setPatternSpecification((PatternSpecification) newValue);
			return;
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			getRoleBindings().clear();
			getRoleBindings().addAll((Collection<? extends RoleBinding>) newValue);
			return;
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			getSetFragmentBindings().clear();
			getSetFragmentBindings().addAll((Collection<? extends SetFragmentBinding>) newValue);
			return;
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			setApplicationModel((ApplicationModel) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.APPLIED_PATTERN__PARENT:
			setParent((PatternApplications) null);
			return;
		case MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION:
			setPatternSpecification((PatternSpecification) null);
			return;
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			getRoleBindings().clear();
			return;
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			getSetFragmentBindings().clear();
			return;
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			setApplicationModel((ApplicationModel) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.APPLIED_PATTERN__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.APPLIED_PATTERN__PARENT:
			return getParent() != null;
		case MappingPackage.APPLIED_PATTERN__PATTERN_SPECIFICATION:
			return patternSpecification != null;
		case MappingPackage.APPLIED_PATTERN__ROLE_BINDINGS:
			return roleBindings != null && !roleBindings.isEmpty();
		case MappingPackage.APPLIED_PATTERN__SET_FRAGMENT_BINDINGS:
			return setFragmentBindings != null && !setFragmentBindings.isEmpty();
		case MappingPackage.APPLIED_PATTERN__APPLICATION_MODEL:
			return applicationModel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AppliedPatternImpl
