/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Token;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Token</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.TokenImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TokenImpl#getParentRoleBinding <em>Parent Role Binding</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TokenImpl#getSubTokens <em>Sub Tokens</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TokenImpl#getParentToken <em>Parent Token</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TokenImpl#getMapsTo <em>Maps To</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TokenImpl extends IdentifierImpl implements Token {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubTokens() <em>Sub Tokens</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubTokens()
	 * @generated
	 * @ordered
	 */
	protected EList<Token> subTokens;

	/**
	 * The cached value of the '{@link #getMapsTo() <em>Maps To</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapsTo()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, EObject> mapsTo;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TokenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.TOKEN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TOKEN__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleBinding getParentRoleBinding() {
		if (eContainerFeatureID() != MappingPackage.TOKEN__PARENT_ROLE_BINDING)
			return null;
		return (RoleBinding) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentRoleBinding(RoleBinding newParentRoleBinding, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentRoleBinding, MappingPackage.TOKEN__PARENT_ROLE_BINDING,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentRoleBinding(RoleBinding newParentRoleBinding) {
		if (newParentRoleBinding != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.TOKEN__PARENT_ROLE_BINDING && newParentRoleBinding != null)) {
			if (EcoreUtil.isAncestor(this, newParentRoleBinding))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentRoleBinding != null)
				msgs = ((InternalEObject) newParentRoleBinding).eInverseAdd(this,
						MappingPackage.ROLE_BINDING__ROOT_TOKEN, RoleBinding.class, msgs);
			msgs = basicSetParentRoleBinding(newParentRoleBinding, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TOKEN__PARENT_ROLE_BINDING,
					newParentRoleBinding, newParentRoleBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Token> getSubTokens() {
		if (subTokens == null) {
			subTokens = new EObjectContainmentWithInverseEList<Token>(Token.class, this,
					MappingPackage.TOKEN__SUB_TOKENS, MappingPackage.TOKEN__PARENT_TOKEN);
		}
		return subTokens;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Token getParentToken() {
		if (eContainerFeatureID() != MappingPackage.TOKEN__PARENT_TOKEN)
			return null;
		return (Token) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentToken(Token newParentToken, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentToken, MappingPackage.TOKEN__PARENT_TOKEN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentToken(Token newParentToken) {
		if (newParentToken != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.TOKEN__PARENT_TOKEN && newParentToken != null)) {
			if (EcoreUtil.isAncestor(this, newParentToken))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentToken != null)
				msgs = ((InternalEObject) newParentToken).eInverseAdd(this, MappingPackage.TOKEN__SUB_TOKENS,
						Token.class, msgs);
			msgs = basicSetParentToken(newParentToken, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TOKEN__PARENT_TOKEN, newParentToken,
					newParentToken));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, EObject> getMapsTo() {
		if (mapsTo == null) {
			mapsTo = new EcoreEMap<String, EObject>(MappingPackage.Literals.STRING_EOBJECT_MAP_ENTRY,
					StringEObjectMapEntryImpl.class, this, MappingPackage.TOKEN__MAPS_TO);
		}
		return mapsTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentRoleBinding((RoleBinding) otherEnd, msgs);
		case MappingPackage.TOKEN__SUB_TOKENS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSubTokens()).basicAdd(otherEnd, msgs);
		case MappingPackage.TOKEN__PARENT_TOKEN:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentToken((Token) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			return basicSetParentRoleBinding(null, msgs);
		case MappingPackage.TOKEN__SUB_TOKENS:
			return ((InternalEList<?>) getSubTokens()).basicRemove(otherEnd, msgs);
		case MappingPackage.TOKEN__PARENT_TOKEN:
			return basicSetParentToken(null, msgs);
		case MappingPackage.TOKEN__MAPS_TO:
			return ((InternalEList<?>) getMapsTo()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			return eInternalContainer().eInverseRemove(this, MappingPackage.ROLE_BINDING__ROOT_TOKEN,
					RoleBinding.class, msgs);
		case MappingPackage.TOKEN__PARENT_TOKEN:
			return eInternalContainer().eInverseRemove(this, MappingPackage.TOKEN__SUB_TOKENS, Token.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.TOKEN__NAME:
			return getName();
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			return getParentRoleBinding();
		case MappingPackage.TOKEN__SUB_TOKENS:
			return getSubTokens();
		case MappingPackage.TOKEN__PARENT_TOKEN:
			return getParentToken();
		case MappingPackage.TOKEN__MAPS_TO:
			if (coreType)
				return getMapsTo();
			else
				return getMapsTo().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.TOKEN__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			setParentRoleBinding((RoleBinding) newValue);
			return;
		case MappingPackage.TOKEN__SUB_TOKENS:
			getSubTokens().clear();
			getSubTokens().addAll((Collection<? extends Token>) newValue);
			return;
		case MappingPackage.TOKEN__PARENT_TOKEN:
			setParentToken((Token) newValue);
			return;
		case MappingPackage.TOKEN__MAPS_TO:
			((EStructuralFeature.Setting) getMapsTo()).set(newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.TOKEN__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			setParentRoleBinding((RoleBinding) null);
			return;
		case MappingPackage.TOKEN__SUB_TOKENS:
			getSubTokens().clear();
			return;
		case MappingPackage.TOKEN__PARENT_TOKEN:
			setParentToken((Token) null);
			return;
		case MappingPackage.TOKEN__MAPS_TO:
			getMapsTo().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.TOKEN__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.TOKEN__PARENT_ROLE_BINDING:
			return getParentRoleBinding() != null;
		case MappingPackage.TOKEN__SUB_TOKENS:
			return subTokens != null && !subTokens.isEmpty();
		case MappingPackage.TOKEN__PARENT_TOKEN:
			return getParentToken() != null;
		case MappingPackage.TOKEN__MAPS_TO:
			return mapsTo != null && !mapsTo.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //TokenImpl
