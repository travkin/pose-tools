/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.MappingFactory;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.mapping.Task;
import de.upb.pose.mapping.Token;
import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MappingPackageImpl extends EPackageImpl implements MappingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass appliedPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternApplicationsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setFragmentBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setFragmentInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tokenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringEObjectMapEntryEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.mapping.MappingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MappingPackageImpl() {
		super(eNS_URI, MappingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MappingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MappingPackage init() {
		if (isInited)
			return (MappingPackage) EPackage.Registry.INSTANCE.getEPackage(MappingPackage.eNS_URI);

		// Obtain or create and register package
		MappingPackageImpl theMappingPackage = (MappingPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MappingPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new MappingPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		SpecificationPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theMappingPackage.createPackageContents();

		// Initialize created meta-data
		theMappingPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMappingPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MappingPackage.eNS_URI, theMappingPackage);
		return theMappingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAppliedPattern() {
		return appliedPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAppliedPattern_Parent() {
		return (EReference) appliedPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAppliedPattern_PatternSpecification() {
		return (EReference) appliedPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAppliedPattern_RoleBindings() {
		return (EReference) appliedPatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAppliedPattern_SetFragmentBindings() {
		return (EReference) appliedPatternEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAppliedPattern_ApplicationModel() {
		return (EReference) appliedPatternEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternApplications() {
		return patternApplicationsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternApplications_Applications() {
		return (EReference) patternApplicationsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternApplications_DesignModelRoot() {
		return (EReference) patternApplicationsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleBinding() {
		return roleBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_ModelElements() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_Role() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_AppliedPattern() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_Tasks() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_ContainingSetFragmentInstances() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_ApplicationModelElement() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoleBinding_NewElementName() {
		return (EAttribute) roleBindingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleBinding_RootToken() {
		return (EReference) roleBindingEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_TaskDescription() {
		return (EReference) taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_Completed() {
		return (EAttribute) taskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_ParentRoleBinding() {
		return (EReference) taskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetFragmentBinding() {
		return setFragmentBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentBinding_AppliedPattern() {
		return (EReference) setFragmentBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentBinding_SetFragment() {
		return (EReference) setFragmentBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentBinding_SetFragmentInstances() {
		return (EReference) setFragmentBindingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetFragmentInstance() {
		return setFragmentInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentInstance_ParentSetFragmentBinding() {
		return (EReference) setFragmentInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentInstance_ContainedRoleBindings() {
		return (EReference) setFragmentInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentInstance_ContainingSetFragmentInstances() {
		return (EReference) setFragmentInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentInstance_ContainedSetFragmentInstances() {
		return (EReference) setFragmentInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicationModel() {
		return applicationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationModel_DesignElements() {
		return (EReference) applicationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicationModel_AppliedPattern() {
		return (EReference) applicationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getToken() {
		return tokenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getToken_ParentRoleBinding() {
		return (EReference) tokenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getToken_SubTokens() {
		return (EReference) tokenEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getToken_ParentToken() {
		return (EReference) tokenEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getToken_MapsTo() {
		return (EReference) tokenEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringEObjectMapEntry() {
		return stringEObjectMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringEObjectMapEntry_Key() {
		return (EAttribute) stringEObjectMapEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStringEObjectMapEntry_Value() {
		return (EReference) stringEObjectMapEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MappingFactory getMappingFactory() {
		return (MappingFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		appliedPatternEClass = createEClass(APPLIED_PATTERN);
		createEReference(appliedPatternEClass, APPLIED_PATTERN__PARENT);
		createEReference(appliedPatternEClass, APPLIED_PATTERN__PATTERN_SPECIFICATION);
		createEReference(appliedPatternEClass, APPLIED_PATTERN__ROLE_BINDINGS);
		createEReference(appliedPatternEClass, APPLIED_PATTERN__SET_FRAGMENT_BINDINGS);
		createEReference(appliedPatternEClass, APPLIED_PATTERN__APPLICATION_MODEL);

		patternApplicationsEClass = createEClass(PATTERN_APPLICATIONS);
		createEReference(patternApplicationsEClass, PATTERN_APPLICATIONS__APPLICATIONS);
		createEReference(patternApplicationsEClass, PATTERN_APPLICATIONS__DESIGN_MODEL_ROOT);

		roleBindingEClass = createEClass(ROLE_BINDING);
		createEReference(roleBindingEClass, ROLE_BINDING__MODEL_ELEMENTS);
		createEReference(roleBindingEClass, ROLE_BINDING__ROLE);
		createEReference(roleBindingEClass, ROLE_BINDING__APPLIED_PATTERN);
		createEReference(roleBindingEClass, ROLE_BINDING__TASKS);
		createEReference(roleBindingEClass, ROLE_BINDING__CONTAINING_SET_FRAGMENT_INSTANCES);
		createEReference(roleBindingEClass, ROLE_BINDING__APPLICATION_MODEL_ELEMENT);
		createEAttribute(roleBindingEClass, ROLE_BINDING__NEW_ELEMENT_NAME);
		createEReference(roleBindingEClass, ROLE_BINDING__ROOT_TOKEN);

		taskEClass = createEClass(TASK);
		createEReference(taskEClass, TASK__TASK_DESCRIPTION);
		createEAttribute(taskEClass, TASK__COMPLETED);
		createEReference(taskEClass, TASK__PARENT_ROLE_BINDING);

		setFragmentBindingEClass = createEClass(SET_FRAGMENT_BINDING);
		createEReference(setFragmentBindingEClass, SET_FRAGMENT_BINDING__APPLIED_PATTERN);
		createEReference(setFragmentBindingEClass, SET_FRAGMENT_BINDING__SET_FRAGMENT);
		createEReference(setFragmentBindingEClass, SET_FRAGMENT_BINDING__SET_FRAGMENT_INSTANCES);

		setFragmentInstanceEClass = createEClass(SET_FRAGMENT_INSTANCE);
		createEReference(setFragmentInstanceEClass, SET_FRAGMENT_INSTANCE__PARENT_SET_FRAGMENT_BINDING);
		createEReference(setFragmentInstanceEClass, SET_FRAGMENT_INSTANCE__CONTAINED_ROLE_BINDINGS);
		createEReference(setFragmentInstanceEClass, SET_FRAGMENT_INSTANCE__CONTAINING_SET_FRAGMENT_INSTANCES);
		createEReference(setFragmentInstanceEClass, SET_FRAGMENT_INSTANCE__CONTAINED_SET_FRAGMENT_INSTANCES);

		applicationModelEClass = createEClass(APPLICATION_MODEL);
		createEReference(applicationModelEClass, APPLICATION_MODEL__DESIGN_ELEMENTS);
		createEReference(applicationModelEClass, APPLICATION_MODEL__APPLIED_PATTERN);

		tokenEClass = createEClass(TOKEN);
		createEReference(tokenEClass, TOKEN__PARENT_ROLE_BINDING);
		createEReference(tokenEClass, TOKEN__SUB_TOKENS);
		createEReference(tokenEClass, TOKEN__PARENT_TOKEN);
		createEReference(tokenEClass, TOKEN__MAPS_TO);

		stringEObjectMapEntryEClass = createEClass(STRING_EOBJECT_MAP_ENTRY);
		createEAttribute(stringEObjectMapEntryEClass, STRING_EOBJECT_MAP_ENTRY__KEY);
		createEReference(stringEObjectMapEntryEClass, STRING_EOBJECT_MAP_ENTRY__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage) EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		SpecificationPackage theSpecificationPackage = (SpecificationPackage) EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		appliedPatternEClass.getESuperTypes().add(theCorePackage.getNamed());
		patternApplicationsEClass.getESuperTypes().add(theCorePackage.getNamed());
		roleBindingEClass.getESuperTypes().add(theCorePackage.getNamed());
		taskEClass.getESuperTypes().add(theCorePackage.getNamed());
		setFragmentBindingEClass.getESuperTypes().add(theCorePackage.getNamed());
		setFragmentInstanceEClass.getESuperTypes().add(theCorePackage.getNamed());
		applicationModelEClass.getESuperTypes().add(theCorePackage.getNamed());
		tokenEClass.getESuperTypes().add(theCorePackage.getNamed());

		// Initialize classes, features, and operations; add parameters
		initEClass(appliedPatternEClass, AppliedPattern.class,
				"AppliedPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getAppliedPattern_Parent(),
				this.getPatternApplications(),
				this.getPatternApplications_Applications(),
				"parent", null, 1, 1, AppliedPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getAppliedPattern_PatternSpecification(),
				theSpecificationPackage.getPatternSpecification(),
				null,
				"patternSpecification", null, 1, 1, AppliedPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getAppliedPattern_RoleBindings(),
				this.getRoleBinding(),
				this.getRoleBinding_AppliedPattern(),
				"roleBindings", null, 0, -1, AppliedPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getAppliedPattern_SetFragmentBindings(),
				this.getSetFragmentBinding(),
				this.getSetFragmentBinding_AppliedPattern(),
				"setFragmentBindings", null, 0, -1, AppliedPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getAppliedPattern_ApplicationModel(),
				this.getApplicationModel(),
				this.getApplicationModel_AppliedPattern(),
				"applicationModel", null, 0, 1, AppliedPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(patternApplicationsEClass, PatternApplications.class,
				"PatternApplications", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getPatternApplications_Applications(),
				this.getAppliedPattern(),
				this.getAppliedPattern_Parent(),
				"applications", null, 0, -1, PatternApplications.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternApplications_DesignModelRoot(),
				ecorePackage.getEObject(),
				null,
				"designModelRoot", null, 0, 1, PatternApplications.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(roleBindingEClass, RoleBinding.class,
				"RoleBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getRoleBinding_ModelElements(),
				ecorePackage.getEObject(),
				null,
				"modelElements", null, 1, -1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_Role(),
				theSpecificationPackage.getDesignElement(),
				null,
				"role", null, 1, 1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_AppliedPattern(),
				this.getAppliedPattern(),
				this.getAppliedPattern_RoleBindings(),
				"appliedPattern", null, 1, 1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_Tasks(),
				this.getTask(),
				this.getTask_ParentRoleBinding(),
				"tasks", null, 0, -1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_ContainingSetFragmentInstances(),
				this.getSetFragmentInstance(),
				this.getSetFragmentInstance_ContainedRoleBindings(),
				"containingSetFragmentInstances", null, 0, 2, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_ApplicationModelElement(),
				theSpecificationPackage.getDesignElement(),
				null,
				"applicationModelElement", null, 0, 1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getRoleBinding_NewElementName(),
				ecorePackage.getEString(),
				"newElementName", null, 0, 1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getRoleBinding_RootToken(),
				this.getToken(),
				this.getToken_ParentRoleBinding(),
				"rootToken", null, 0, 1, RoleBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getTask_TaskDescription(),
				theSpecificationPackage.getTaskDescription(),
				null,
				"taskDescription", null, 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getTask_Completed(),
				ecorePackage.getEBoolean(),
				"completed", "false", 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(
				getTask_ParentRoleBinding(),
				this.getRoleBinding(),
				this.getRoleBinding_Tasks(),
				"parentRoleBinding", null, 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(setFragmentBindingEClass, SetFragmentBinding.class,
				"SetFragmentBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSetFragmentBinding_AppliedPattern(),
				this.getAppliedPattern(),
				this.getAppliedPattern_SetFragmentBindings(),
				"appliedPattern", null, 1, 1, SetFragmentBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSetFragmentBinding_SetFragment(),
				theSpecificationPackage.getSetFragment(),
				null,
				"setFragment", null, 1, 1, SetFragmentBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSetFragmentBinding_SetFragmentInstances(),
				this.getSetFragmentInstance(),
				this.getSetFragmentInstance_ParentSetFragmentBinding(),
				"setFragmentInstances", null, 0, -1, SetFragmentBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(setFragmentInstanceEClass, SetFragmentInstance.class,
				"SetFragmentInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSetFragmentInstance_ParentSetFragmentBinding(),
				this.getSetFragmentBinding(),
				this.getSetFragmentBinding_SetFragmentInstances(),
				"parentSetFragmentBinding", null, 1, 1, SetFragmentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSetFragmentInstance_ContainedRoleBindings(),
				this.getRoleBinding(),
				this.getRoleBinding_ContainingSetFragmentInstances(),
				"containedRoleBindings", null, 0, -1, SetFragmentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSetFragmentInstance_ContainingSetFragmentInstances(),
				this.getSetFragmentInstance(),
				this.getSetFragmentInstance_ContainedSetFragmentInstances(),
				"containingSetFragmentInstances", null, 0, -1, SetFragmentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSetFragmentInstance_ContainedSetFragmentInstances(),
				this.getSetFragmentInstance(),
				this.getSetFragmentInstance_ContainingSetFragmentInstances(),
				"containedSetFragmentInstances", null, 0, -1, SetFragmentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(applicationModelEClass, ApplicationModel.class,
				"ApplicationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getApplicationModel_DesignElements(),
				theSpecificationPackage.getDesignElement(),
				null,
				"designElements", null, 0, -1, ApplicationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getApplicationModel_AppliedPattern(),
				this.getAppliedPattern(),
				this.getAppliedPattern_ApplicationModel(),
				"appliedPattern", null, 1, 1, ApplicationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(tokenEClass, Token.class, "Token", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getToken_ParentRoleBinding(),
				this.getRoleBinding(),
				this.getRoleBinding_RootToken(),
				"parentRoleBinding", null, 0, 1, Token.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getToken_SubTokens(),
				this.getToken(),
				this.getToken_ParentToken(),
				"subTokens", null, 0, -1, Token.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getToken_ParentToken(),
				this.getToken(),
				this.getToken_SubTokens(),
				"parentToken", null, 0, 1, Token.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getToken_MapsTo(),
				this.getStringEObjectMapEntry(),
				null,
				"mapsTo", null, 0, -1, Token.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(stringEObjectMapEntryEClass, Map.Entry.class,
				"StringEObjectMapEntry", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getStringEObjectMapEntry_Key(),
				ecorePackage.getEString(),
				"key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getStringEObjectMapEntry_Value(),
				ecorePackage.getEObject(),
				null,
				"value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(
				appliedPatternEClass,
				source,
				new String[] {
						"documentation", "This represents a single pattern application. i.e. a place in a software design model where a certain pattern is applied (implemented). An AppliedPattern object maps all pattern roles to software design model elements to completely document the pattern application and the pattern\'s participants." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				patternApplicationsEClass,
				source,
				new String[] {
						"documentation", "Collection of all documented pattern applications in a software design model. This class represents the root element for all pattern applications." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				roleBindingEClass,
				source,
				new String[] {
						"documentation", "This represents the assignment of one or more software design elements to a single pattern role, i.e. the design elements play the specified pattern role. In case that no design elements are chosen and new design elements have to be created to play the pattern role, then the newElementName attribute stores the user-chosen name for design element to be created during pattern application." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getRoleBinding_NewElementName(),
				source,
				new String[] {
						"documentation", "In case that no design elements are chosen for this role binding and new design elements have to be created to play the pattern role, then this attribute stores the user-chosen name for the design element to be created during pattern application." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				taskEClass,
				source,
				new String[] {
						"documentation", "This represents a user task to be manually completed to completely apply the pattern. Each task is assigned to role binding, i.e. to a realization of a pattern role." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getTask_Completed(),
				source,
				new String[] {
						"documentation", "This attribute represents the status of the corresponding task. If the task is completed, the value of this attribute is true." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				setFragmentBindingEClass,
				source,
				new String[] {
						"documentation", "A set binding represents all realizations of the software design that is defined in a set fragment, i.e. all role bindings for roles in a certain set fragment for a certain pattern application. Each occurrence/realization of the elements in a set fragment is represented by a set element binding." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				setFragmentInstanceEClass,
				source,
				new String[] {
						"documentation", "This represents an occurrence/realization of the elements in a set fragment, i.e. it contains role bindings for each of the pattern roles in the corresponding set fragment." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				applicationModelEClass,
				source,
				new String[] {
						"documentation", "An abstract model of the software design as it is intended to be after pattern application. Here, only DesignElements are used to create a 1-to-1 mapping from abstract design elements in the application model to concrete design elements in the software design model where the pattern is actually applied." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				tokenEClass,
				source,
				new String[] {
						"documentation", "This represents a conceptional part of the realization of a pattern role. E.g. an action describes a behavior on a very high abstraction level. Tokens can be used to represent certain steps of the behavior to step-wise translate the abstract design model described in a pattern specification to the actual design in the design model." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getToken_SubTokens(),
				source,
				new String[] {
						"documentation", "A token can be sub-divided into further sub-tokens, e.g. a task can be built of several sub-tasks. The sub-tokens represent smaller parts of this token\'s concept." //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

} //MappingPackageImpl
