/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.mapping.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.Task;
import de.upb.pose.specification.TaskDescription;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.mapping.impl.TaskImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TaskImpl#getTaskDescription <em>Task Description</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TaskImpl#isCompleted <em>Completed</em>}</li>
 *   <li>{@link de.upb.pose.mapping.impl.TaskImpl#getParentRoleBinding <em>Parent Role Binding</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TaskImpl extends IdentifierImpl implements Task {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTaskDescription() <em>Task Description</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDescription()
	 * @generated
	 * @ordered
	 */
	protected TaskDescription taskDescription;

	/**
	 * The default value of the '{@link #isCompleted() <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCompleted()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMPLETED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCompleted() <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCompleted()
	 * @generated
	 * @ordered
	 */
	protected boolean completed = COMPLETED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MappingPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TASK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDescription getTaskDescription() {
		if (taskDescription != null && taskDescription.eIsProxy()) {
			InternalEObject oldTaskDescription = (InternalEObject) taskDescription;
			taskDescription = (TaskDescription) eResolveProxy(oldTaskDescription);
			if (taskDescription != oldTaskDescription) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MappingPackage.TASK__TASK_DESCRIPTION,
							oldTaskDescription, taskDescription));
			}
		}
		return taskDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDescription basicGetTaskDescription() {
		return taskDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDescription(TaskDescription newTaskDescription) {
		TaskDescription oldTaskDescription = taskDescription;
		taskDescription = newTaskDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TASK__TASK_DESCRIPTION,
					oldTaskDescription, taskDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCompleted() {
		return completed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompleted(boolean newCompleted) {
		boolean oldCompleted = completed;
		completed = newCompleted;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TASK__COMPLETED, oldCompleted,
					completed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleBinding getParentRoleBinding() {
		if (eContainerFeatureID() != MappingPackage.TASK__PARENT_ROLE_BINDING)
			return null;
		return (RoleBinding) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentRoleBinding(RoleBinding newParentRoleBinding, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentRoleBinding, MappingPackage.TASK__PARENT_ROLE_BINDING,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentRoleBinding(RoleBinding newParentRoleBinding) {
		if (newParentRoleBinding != eInternalContainer()
				|| (eContainerFeatureID() != MappingPackage.TASK__PARENT_ROLE_BINDING && newParentRoleBinding != null)) {
			if (EcoreUtil.isAncestor(this, newParentRoleBinding))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentRoleBinding != null)
				msgs = ((InternalEObject) newParentRoleBinding).eInverseAdd(this, MappingPackage.ROLE_BINDING__TASKS,
						RoleBinding.class, msgs);
			msgs = basicSetParentRoleBinding(newParentRoleBinding, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MappingPackage.TASK__PARENT_ROLE_BINDING,
					newParentRoleBinding, newParentRoleBinding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentRoleBinding((RoleBinding) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			return basicSetParentRoleBinding(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			return eInternalContainer().eInverseRemove(this, MappingPackage.ROLE_BINDING__TASKS, RoleBinding.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MappingPackage.TASK__NAME:
			return getName();
		case MappingPackage.TASK__TASK_DESCRIPTION:
			if (resolve)
				return getTaskDescription();
			return basicGetTaskDescription();
		case MappingPackage.TASK__COMPLETED:
			return isCompleted();
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			return getParentRoleBinding();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MappingPackage.TASK__NAME:
			setName((String) newValue);
			return;
		case MappingPackage.TASK__TASK_DESCRIPTION:
			setTaskDescription((TaskDescription) newValue);
			return;
		case MappingPackage.TASK__COMPLETED:
			setCompleted((Boolean) newValue);
			return;
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			setParentRoleBinding((RoleBinding) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MappingPackage.TASK__NAME:
			setName(NAME_EDEFAULT);
			return;
		case MappingPackage.TASK__TASK_DESCRIPTION:
			setTaskDescription((TaskDescription) null);
			return;
		case MappingPackage.TASK__COMPLETED:
			setCompleted(COMPLETED_EDEFAULT);
			return;
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			setParentRoleBinding((RoleBinding) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MappingPackage.TASK__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case MappingPackage.TASK__TASK_DESCRIPTION:
			return taskDescription != null;
		case MappingPackage.TASK__COMPLETED:
			return completed != COMPLETED_EDEFAULT;
		case MappingPackage.TASK__PARENT_ROLE_BINDING:
			return getParentRoleBinding() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", completed: "); //$NON-NLS-1$
		result.append(completed);
		result.append(')');
		return result.toString();
	}

} //TaskImpl
