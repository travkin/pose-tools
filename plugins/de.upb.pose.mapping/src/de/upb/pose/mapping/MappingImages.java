package de.upb.pose.mapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.ui.runtime.ImagesImpl;

@Deprecated
public final class MappingImages extends ImagesImpl {
	
	public static final String VALIDATE = "icons/commands/validate.png"; //$NON-NLS-1$
	public static final String APPLY = "icons/commands/apply.png"; //$NON-NLS-1$

	private static Map<EClass, String> map;

	public static Image get(Object element) {
		return Activator.get().getImage(getKey(element));
	}

	public static ImageDescriptor getDescriptor(Object element) {
		return Activator.get().getImageDescriptor(getKey(element));
	}

	public static String getKey(Object element) {
		if (element instanceof String) {
			return (String) element;
		}

		if (element instanceof EClass) {
			return getMap().get(element);
		}

		if (element instanceof EObject) {
			return getKey(((EObject) element).eClass());
		}

		return null;
	}

	public static Collection<String> getPaths() {
		Collection<String> list = new ArrayList<String>(getMap().values());

		list.add(VALIDATE);
		list.add(APPLY);

		return list;
	}

	private static Map<EClass, String> getMap() {
		if (map == null) {
			map = new LinkedHashMap<EClass, String>();

			initialize(map, MappingPackage.eINSTANCE);
		}
		return map;
	}
}
