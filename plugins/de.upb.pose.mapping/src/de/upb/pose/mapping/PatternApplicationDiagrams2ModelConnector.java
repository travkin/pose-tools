/**
 * 
 */
package de.upb.pose.mapping;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramLink;
import org.eclipse.graphiti.mm.pictograms.PictogramsFactory;
import org.eclipse.graphiti.mm.pictograms.Shape;

import de.upb.pose.core.CoreConstants;

/**
 * @author Dietrich Travkin
 */
public class PatternApplicationDiagrams2ModelConnector {

	/**
	 * An {@link org.eclipse.emf.ecore.EAnnotation} with this source key
	 * references the root element of all pattern application diagrams (pattern role mapping diagrams)
	 * corresponding to a pattern applications model file.
	 */
	public static final String ANNOTATION_SRC_PATTERN_APPLICATION_DIAGRAMS_REF = CoreConstants.ANNOTATION_SRC_DIAGRAMS_REF;
	

	public static EAnnotation getPointerToPatternApplicationDiagramsContainer(PatternApplications patternApplicationsModel)
	{
		return patternApplicationsModel.getAnnotation(ANNOTATION_SRC_PATTERN_APPLICATION_DIAGRAMS_REF);
	}
	
	public static EAnnotation createPointerToPatternApplicationDiagramsContainer(PatternApplications patternApplicationsModel) {
		EAnnotation pointerToPatternApplicationDiagramsContainer = EcoreFactory.eINSTANCE.createEAnnotation();
		pointerToPatternApplicationDiagramsContainer.setSource(ANNOTATION_SRC_PATTERN_APPLICATION_DIAGRAMS_REF);
		patternApplicationsModel.getAnnotations().add(pointerToPatternApplicationDiagramsContainer);
		return pointerToPatternApplicationDiagramsContainer;
	}
	
	public static ContainerShape getLinkedPatternApplicationDiagramsContainer(PatternApplications patternApplicationsModel)
	{
		EAnnotation pointerToPatternApplicationDiagramsContainer = getPointerToPatternApplicationDiagramsContainer(patternApplicationsModel);
		if (pointerToPatternApplicationDiagramsContainer != null)
		{
			if (pointerToPatternApplicationDiagramsContainer.getReferences().size() > 1) {
				throw new IllegalStateException("There is more than one reference from a PatternApplications object to a pattern application diagrams container.");
			}
			
			ContainerShape patternApplicationDiagramsContainer = (ContainerShape) pointerToPatternApplicationDiagramsContainer.getReferences().get(0);
			return patternApplicationDiagramsContainer;
		}
		return null;
	}
	
	public static ContainerShape createPatternApplicationDiagramsContainer() {
		return PictogramsFactory.eINSTANCE.createContainerShape();
	}
	
	public static void linkPatternApplicationsToPatternApplicationDiagramsContainer(PatternApplications patternApplicationsModel, ContainerShape patternApplicationDiagramsContainer) {
		EAnnotation pointerToPatternApplicationDiagramsContainer = getPointerToPatternApplicationDiagramsContainer(patternApplicationsModel);
		if (pointerToPatternApplicationDiagramsContainer == null) {
			throw new IllegalStateException("No pointer (EAnnotation) to the given PatternApplications' diagrams container available.");
		}
		
		// pointer --> container
		if (!pointerToPatternApplicationDiagramsContainer.getReferences().isEmpty()) {
			pointerToPatternApplicationDiagramsContainer.getReferences().clear();
		}
		pointerToPatternApplicationDiagramsContainer.getReferences().add(patternApplicationDiagramsContainer);
		
		// container --> pattern applications model
		PictogramLink link = patternApplicationDiagramsContainer.getLink();
		if (link == null) {
			link = PictogramsFactory.eINSTANCE.createPictogramLink();
			link.setPictogramElement(patternApplicationDiagramsContainer);
		} else {
			link.getBusinessObjects().clear();
		}
		link.getBusinessObjects().add(patternApplicationsModel);
	}
	
	public static PatternApplications getPatternApplicationsModel(ContainerShape patternApplicationDiagramsContainer) {
		if (patternApplicationDiagramsContainer != null
				&& patternApplicationDiagramsContainer.getLink() != null
				&& !patternApplicationDiagramsContainer.getLink().getBusinessObjects().isEmpty()
				&& patternApplicationDiagramsContainer.getLink().getBusinessObjects().get(0) instanceof PatternApplications) {
			return (PatternApplications) patternApplicationDiagramsContainer.getLink().getBusinessObjects().get(0);
		}
		return null;
	}
	
	public static Diagram findDiagramFor(AppliedPattern patternApplication) {
		ContainerShape diagramsParent = getLinkedPatternApplicationDiagramsContainer(patternApplication.getParent());
		if (diagramsParent != null) {
			for (Shape shape: diagramsParent.getChildren()) {
				if (shape instanceof Diagram) {
					Diagram diagram = (Diagram) shape;
					if (diagram.getLink() != null
							&& !diagram.getLink().getBusinessObjects().isEmpty()) {
						for (EObject obj: diagram.getLink().getBusinessObjects()) {
							if (obj instanceof AppliedPattern && obj.equals(patternApplication)) {
								return diagram;
							}
						}
					}
				}
			}
		}
		return null;
	}
}
