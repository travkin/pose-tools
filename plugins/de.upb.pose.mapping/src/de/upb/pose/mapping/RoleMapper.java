/**
 * 
 */
package de.upb.pose.mapping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class RoleMapper {

	public static boolean canMap(Object designElementInPatternSpecification, EObject designElementInEcoreModel)
	{
		// TODO use ApplicationModel2EcoreAndSDsTranslator instead of listing all combinations
		return (designElementInPatternSpecification instanceof Type && designElementInEcoreModel instanceof EClass)
				|| (designElementInPatternSpecification instanceof Operation && designElementInEcoreModel instanceof EOperation)
				|| (designElementInPatternSpecification instanceof Parameter && designElementInEcoreModel instanceof EParameter)
				|| (designElementInPatternSpecification instanceof Attribute && designElementInEcoreModel instanceof EAttribute)
				|| (designElementInPatternSpecification instanceof Reference && designElementInEcoreModel instanceof EReference)
				|| (designElementInPatternSpecification instanceof Subsystem && designElementInEcoreModel instanceof EPackage)
				|| (designElementInPatternSpecification instanceof Subsystem && designElementInEcoreModel instanceof EClass);
	}
	
	public static boolean isMapped(EObject element) {
		if (!findAllRoleBindingsPointingToObject(element).isEmpty()) {
			return true;
		}
		return false;
	}

	private static List<RoleBinding> findAllRoleBindingsPointingToObject(EObject ecoreModelElement) {
		List<RoleBinding> roleBindings = new ArrayList<RoleBinding>();
		
		PatternApplications patternApplications = findPatternApplications(ecoreModelElement);
		if (patternApplications != null) {
			for (AppliedPattern patternApplication: patternApplications.getApplications()) {
				for (RoleBinding roleBinding: patternApplication.getRoleBindings()) {
					if (!roleBinding.getModelElements().isEmpty()
							&& roleBinding.getModelElements().contains(ecoreModelElement)) {
						roleBindings.add(roleBinding);
					}
				}
			}
		}
		
		return roleBindings;
	}
	
	private static PatternApplications findPatternApplications(EObject ecoreModelElement) {
		if (ecoreModelElement != null
				&& ecoreModelElement.eResource() != null
				&& ecoreModelElement.eResource().getResourceSet() != null) {
			ResourceSet resourceSet = ecoreModelElement.eResource().getResourceSet();
			
			Resource mappingModelResource = findMappingModelResource(resourceSet);
			return findPatternApplications(mappingModelResource);
		}
		return null;
	}
	
	private static Resource findMappingModelResource(ResourceSet resourceSet) {
		for (Resource containedResource: resourceSet.getResources()) {
			if (containedResource.getURI() != null
					&& MappingConstants.MODEL_FILE_EXTENSION.equals(containedResource.getURI().fileExtension())) {
				return containedResource;
			}
		}
		return null;
	}
	
	private static PatternApplications findPatternApplications(Resource mappingModelResource) {
		if (mappingModelResource != null
				&& mappingModelResource.getContents().size() == 1
				&& mappingModelResource.getContents().get(0) instanceof PatternApplications) {
			return (PatternApplications) mappingModelResource.getContents().get(0);
		}
		return null;
	}

	public static RoleBinding createRoleBindingFor(DesignElement patternRole, AppliedPattern patternApplication)
	{
		Assert.isNotNull(patternRole);
		Assert.isNotNull(patternApplication);
		
		RoleBinding roleBinding = MappingFactory.eINSTANCE.createRoleBinding();
		roleBinding.setRole(patternRole);
		patternApplication.getRoleBindings().add(roleBinding);
		roleBinding.setName(MappingNameCreator.getNameFor(roleBinding));
		return roleBinding;
	}

	/**
	 * Collects all role bindings in the given pattern application for the given pattern role (design element).
	 * 
	 * @param patternRole the pattern role
	 * @param patternApplication the parent pattern application
	 * @return all role bindings in the given pattern application for the given pattern role (design element)
	 */
	public static List<RoleBinding> findAllRoleBindingsFor(DesignElement patternRole, AppliedPattern patternApplication) {
		ArrayList<RoleBinding> roleBindings = new ArrayList<RoleBinding>();
		for (RoleBinding roleBinding : patternApplication.getRoleBindings()) {
			if (roleBinding.getRole() == patternRole) {
				roleBindings.add(roleBinding);
			}
		}
		return roleBindings;
	}

}
