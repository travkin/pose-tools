/**
 * 
 */
package de.upb.pose.mapping;

import java.util.ArrayList;
import java.util.List;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.util.SpecificationUtil;

/**
 * @author Dietrich Travkin
 * 
 * @deprecated Use {@link de.upb.pose.patternapplication.PatternApplicationCreator} instead
 */
@Deprecated
public class BindingCreator
{
	private static BindingCreator instance = null;
	
	private BindingCreator()
	{
	}
	
	private static BindingCreator get()
	{
		if (instance == null)
		{
			instance = new BindingCreator();
		}
		return instance;
	}
	
	public static void createMissingBindings(AppliedPattern appliedPattern)
	{
		BindingCreator creator = get();
		
		List<PatternElement> allPatternSpecificationElements = SpecificationUtil.collectAllPatternElements(appliedPattern.getPatternSpecification());
		
		// create missing role bindings for all pattern roles that are not in a set
		creator.createMissingNonSetBindings(appliedPattern, allPatternSpecificationElements);
		
		// create missing set bindings and set element bindings for all sets in the pattern specification
		creator.createMissingSetBindingsAndSetElementBindings(appliedPattern, allPatternSpecificationElements);
		
		// create role bindings for all pattern roles in sets		
		creator.createBindingsForElementsInSets(appliedPattern, allPatternSpecificationElements);
	}

	private void createMissingNonSetBindings(AppliedPattern appliedPattern, List<PatternElement> allPatternSpecificationElements) {
		for (PatternElement patternElement : allPatternSpecificationElements)
		{
			if (patternElement instanceof DesignElement
					&& ((DesignElement) patternElement).getContainingSets().isEmpty())
			{
				DesignElement designElement = (DesignElement) patternElement;
					
				// there can be only one role binding, since this element is not in a set
				RoleBinding roleBinding = findFirstRoleBindingFor(designElement, appliedPattern); 
				if (roleBinding == null)
				{
					roleBinding = RoleMapper.createRoleBindingFor(designElement, appliedPattern);
				}
			}
		}
	}
	
	private void createMissingSetBindingsAndSetElementBindings(AppliedPattern appliedPattern, List<PatternElement> allPatternSpecificationElements)
	{
		// TODO consider containment of set fragments in set fragments
		for (PatternElement patternElement : allPatternSpecificationElements)
		{
			if (patternElement instanceof SetFragment)
			{
				SetFragment set = (SetFragment) patternElement;
				SetFragmentBinding setBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(set, appliedPattern);
				if (setBinding == null)
				{
					setBinding = SetFragmentBindingCreator.findOrCreateSetBindingFor(set, appliedPattern);

					// create at least one set element binding
					SetFragmentBindingCreator.createSetFragmentInstance(setBinding);
				}
			}
		}
	}
	
	public static List<RoleBinding> createRoleBindingsInSetFragmentInstance(SetFragmentInstance setFragmentInstance)
	{
		assert setFragmentInstance.getContainedRoleBindings().isEmpty();
		assert setFragmentInstance.getParentSetFragmentBinding().getAppliedPattern() != null;
		
		SetFragmentBinding setBinding = setFragmentInstance.getParentSetFragmentBinding();
		SetFragment setFragment = setBinding.getSetFragment();
		AppliedPattern appliedPattern = setBinding.getAppliedPattern();
		
		List<RoleBinding> createdRoleBindings = new ArrayList<RoleBinding>();
		
		for (SetFragmentElement containedElement : setFragment.getContainedElements())
		{
			if (containedElement instanceof DesignElement)
			{
				DesignElement patternRole = (DesignElement) containedElement;
				
				RoleBinding roleBinding = get().findFirstRoleBindingFor(patternRole, setFragmentInstance, appliedPattern);
				if (roleBinding == null)
				{
					// create new role binding for this set element binding
					roleBinding = RoleMapper.createRoleBindingFor(patternRole, appliedPattern);
					createdRoleBindings.add(roleBinding);
					setFragmentInstance.getContainedRoleBindings().add(roleBinding);
					
					// check if it has to be added to other set element bindings
					SetFragment otherSet = get().findAnotherContainingSet(patternRole, setFragment); // there can be only two containing sets
					if (otherSet != null)
					{
						SetFragmentBinding otherSetBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(otherSet, appliedPattern); // there must be already one set binding
						
						// run through all set element bindings and combine them with this one
						for (SetFragmentInstance otherSetElementBinding : otherSetBinding.getSetFragmentInstances())
						{
							otherSetElementBinding.getContainedRoleBindings().add(roleBinding);
						}
					}
				}
			}
			else if (containedElement instanceof SetFragment)
			{
				// TODO handle contained set fragments
				throw new UnsupportedOperationException("SetFragments contained in SetFragments are not supported yet.");
			}
		}
		
		return createdRoleBindings;
	}
	
	private void createBindingsForElementsInSets(AppliedPattern appliedPattern, List<PatternElement> allPatternSpecificationElements)
	{
		for (PatternElement patternElement : allPatternSpecificationElements)
		{
			if (patternElement instanceof SetFragment)
			{
				SetFragment setFragment = (SetFragment) patternElement;
				SetFragmentBinding setBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(setFragment, appliedPattern); // there must be already one set binding

				// ensure each set element binding to contain all role bindings
				for (SetFragmentInstance setElementBinding : setBinding.getSetFragmentInstances())
				{
					createRoleBindingsInSetFragmentInstance(setElementBinding);
				}
			}
		}
	}

	private RoleBinding findFirstRoleBindingFor(DesignElement patternRole, AppliedPattern appliedPattern)
	{
		assert patternRole != null;
		assert appliedPattern != null;
		
		for (RoleBinding roleBinding : appliedPattern.getRoleBindings())
		{
			if (roleBinding.getRole() == patternRole)
			{
				return roleBinding;
			}
		}
		return null;
	}
	
	private RoleBinding findFirstRoleBindingFor(DesignElement patternRole, SetFragmentInstance setFragmentInstance, AppliedPattern appliedPattern)
	{
		assert setFragmentInstance != null;
		assert patternRole != null;
		assert appliedPattern != null;
		
		for (RoleBinding roleBinding : setFragmentInstance.getContainedRoleBindings())
		{
			if (roleBinding.getRole() == patternRole)
			{
				return roleBinding;
			}
		}
		return null;
	}
	
	private SetFragment findAnotherContainingSet(DesignElement patternRole, SetFragment containingSet)
	{
		for (SetFragment set : patternRole.getContainingSets())
		{
			if (set != containingSet)
			{
				return set;
			}
		}
		return null;
	}
}
