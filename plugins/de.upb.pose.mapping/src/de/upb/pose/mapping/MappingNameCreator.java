/**
 * 
 */
package de.upb.pose.mapping;

import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.util.SpecificationTextUtil;

/**
 * @author Dietrich Travkin
 */
public class MappingNameCreator {

	public static String getNameFor(AppliedPattern appliedPattern)
	{
		assert appliedPattern != null;
		assert appliedPattern.getPatternSpecification() != null;
		assert appliedPattern.getPatternSpecification().getPattern() != null;
		
		String name = "Application of \""
			+ appliedPattern.getPatternSpecification().getPattern().getName()
			+ "\" pattern";
		
		if (appliedPattern.getPatternSpecification().getPattern().getSpecifications().size() > 1)
		{
			name += "(" + appliedPattern.getPatternSpecification().getName() + ")";
		}
		
		return name;
	}
	
	public static String getNameFor(SetFragmentBinding setBinding)
	{
		assert setBinding != null;
		assert setBinding.getSetFragment() != null;
		
		return "Bindings for set \"" + setBinding.getSetFragment().getName() + "\"";
	}
	
	public static String getNameFor(SetFragmentInstance setElementBinding)
	{
		assert setElementBinding != null;
		assert setElementBinding.getParentSetFragmentBinding() != null;
		assert setElementBinding.getParentSetFragmentBinding().getSetFragment() != null;
		
		return "Set element \""
			+ setElementBinding.getParentSetFragmentBinding().getSetFragment().getName()
			+ "\" ["
			+ (setElementBinding.getParentSetFragmentBinding().getSetFragmentInstances().indexOf(setElementBinding) + 1)
			+ "]";
	}
	
	public static String getNameFor(RoleBinding roleBinding)
	{
		assert roleBinding != null;
		assert roleBinding.getRole() != null;
		
		String to = "?";
		
		
		String patternRoleName = roleBinding.getRole().getName();
		if (patternRoleName == null || patternRoleName.isEmpty() && roleBinding.getRole() instanceof Action) {
			Action action = (Action) roleBinding.getRole();
			patternRoleName = SpecificationTextUtil.getName(action);
		}
		
		String tmpTo = getMappedToText(roleBinding);
		if (tmpTo != null && !tmpTo.isEmpty()) {
			to = tmpTo;
		}
		
		return "Binding \"" + patternRoleName + "\" -> \"" + to + "\"";
	}
	
	public static String getMappedToText(RoleBinding roleBinding) {
		String to = null;
		
		EObject firstMappedElement = roleBinding.getModelElements().isEmpty() ? null : roleBinding.getModelElements().get(0);
		
		// the name of the first mapped element or...
		if (firstMappedElement != null)
		{
			to = retrieveEObjectName(firstMappedElement);
		// ... the name of the element to be created
		} else if (getNewElementName(roleBinding) != null) {
			to = getNewElementNameDisplayText(roleBinding);
		}
		
		return to;
	}
	
	/**
	 * @param roleBinding the pattern role
	 * @return the name for the new design element to be created for this pattern role
	 */
	public static String getNewElementName(RoleBinding roleBinding)
	{
		if (roleBinding != null && roleBinding.getNewElementName() != null && !roleBinding.getNewElementName().isEmpty())
		{
			return roleBinding.getNewElementName();
		}
		return null;
	}
	
	public static String getNewElementNameDisplayText(RoleBinding roleBinding) {
		String name = getNewElementName(roleBinding);
		if (name != null && !name.isEmpty()) {
			return "*" + name + "*";
		}
		return null;
	}
	
	public static String retrieveEObjectName(EObject object) {
		String name = null;
		if (object instanceof ENamedElement) {
			name = ((ENamedElement) object).getName();
		} else { // reflection
			for (EStructuralFeature feature: object.eClass().getEAllStructuralFeatures()) {
				if (feature.getName().equals("name") && feature.getEType().equals(EcorePackage.Literals.ESTRING)) {
					try {
						name = (String) object.eGet(feature, true);
						break;
					}
					catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		if (name == null) {
			name = "a " + object.getClass().getSimpleName() + " object";
		}
		
		return name;
	}
	
}
