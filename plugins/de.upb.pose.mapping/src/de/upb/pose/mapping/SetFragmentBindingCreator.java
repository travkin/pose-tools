/**
 * 
 */
package de.upb.pose.mapping;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;

/**
 * @author Dietrich Travkin
 */
public class SetFragmentBindingCreator {

	public static boolean containsSetElementBindingDirectlyOrIndirectlyRoleBinding(SetFragmentInstance setFragmentInstance, RoleBinding roleBinding) {
		if (setFragmentInstance.getContainedRoleBindings().contains(roleBinding)) {
			return true;
		} else { // indirect containment?
			for (SetFragmentInstance containedSetElementBinding: setFragmentInstance.getContainedSetFragmentInstances()) {
				if (containsSetElementBindingDirectlyOrIndirectlyRoleBinding(containedSetElementBinding, roleBinding)) {
					return true;
				}
			}
		}
		return false;
	}

	public static SetFragmentInstance createSetFragmentInstance(SetFragmentBinding parentSetFragmentBinding)
	{
		Assert.isNotNull(parentSetFragmentBinding);
		
		SetFragmentInstance setElementBinding = MappingFactory.eINSTANCE.createSetFragmentInstance();
		parentSetFragmentBinding.getSetFragmentInstances().add(setElementBinding);
		setElementBinding.setName(MappingNameCreator.getNameFor(setElementBinding));
		return setElementBinding;
	}
	
	public static SetFragmentBinding createSetFragmentBindingFor(SetFragment set, AppliedPattern appliedPattern) {
		Assert.isNotNull(set);
		Assert.isNotNull(appliedPattern);
		
		SetFragmentBinding setFragmentBinding = MappingFactory.eINSTANCE.createSetFragmentBinding();
		setFragmentBinding.setSetFragment(set);
		appliedPattern.getSetFragmentBindings().add(setFragmentBinding);
		setFragmentBinding.setName(MappingNameCreator.getNameFor(setFragmentBinding));
		return setFragmentBinding;
	}

	public static SetFragmentBinding findOrCreateSetBindingFor(SetFragment set, AppliedPattern appliedPattern)
	{
		assert set != null;
		assert appliedPattern != null;
				
		SetFragmentBinding setFragmentBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(set, appliedPattern);
		if (setFragmentBinding == null)
		{
			setFragmentBinding = createSetFragmentBindingFor(set, appliedPattern);
			return setFragmentBinding;
		}
		return setFragmentBinding;
	}

	/**
	 * Returns the set binding for the given set fragment in the given pattern application if available.
	 * 
	 * @param setFragment a set fragment
	 * @param pattern the parent pattern application
	 * @return the set binding for the given set fragment in the given pattern application or <code>null</code>.
	 */
	public static SetFragmentBinding findSetFragmentBindingFor(SetFragment setFragment, AppliedPattern pattern) {
		for (SetFragmentBinding setBinding : pattern.getSetFragmentBindings()) {
			if (setBinding.getSetFragment() == setFragment) {
				return setBinding;
			}
		}
		return null;
	}

	/**
	 * Collects all role bindings in the given set element binding for the given pattern role (design element).
	 * 
	 * @param designElement the pattern role
	 * @param setElementBinding the parent set element binding
	 * @return all role bindings in the given set element binding for the given pattern role (design element).
	 */
	public static List<RoleBinding> findAllRoleBindingsFor(DesignElement designElement,
			SetFragmentInstance setElementBinding) {
		ArrayList<RoleBinding> roleBindings = new ArrayList<RoleBinding>();
		for (RoleBinding roleBinding : setElementBinding.getContainedRoleBindings()) {
			if (roleBinding.getRole() == designElement) {
				roleBindings.add(roleBinding);
			}
		}
		return roleBindings;
	}

	public static List<RoleBinding> collectAllRoleBindingInSetFragmentInstance(SetFragmentInstance setFragmentInstance) {
		List<RoleBinding> containedRoleBindings = new LinkedList<RoleBinding>();
		for (SetFragmentInstance containedSetFragmentInstance: setFragmentInstance.getContainedSetFragmentInstances()) {
			containedRoleBindings.addAll(collectAllRoleBindingInSetFragmentInstance(containedSetFragmentInstance));
		}
		containedRoleBindings.addAll(setFragmentInstance.getContainedRoleBindings());
		return containedRoleBindings;
	}

}
