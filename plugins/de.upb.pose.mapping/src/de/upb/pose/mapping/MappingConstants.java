package de.upb.pose.mapping;


public final class MappingConstants {
	
	public static final String MODEL_FILE_EXTENSION = "mappings"; //$NON-NLS-1$
	public static final String DIAGRAMS_FILE_EXTENSION = "mappings_diagrams"; //$NON-NLS-1$

}
