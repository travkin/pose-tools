/**
 * 
 */
package de.upb.pose.mapping;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * @author Dietrich Travkin
 */
public class PatternApplications2EcoreConnector {

	/**
	 * An {@link org.eclipse.emf.ecore.EAnnotation} with this source key
	 * references the root element ({@link PatternApplications}) of all pattern applications (pattern role mappings)
	 * corresponding to an ecore file.
	 */
	public static final String ANNOTATION_SRC_PATTERN_APPLICATIONS_REF = "http://www.uni-paderborn.de/pose/mappings";
	

	public static EAnnotation getPointerToPatternApplications(EPackage rootPackage)
	{
		return rootPackage.getEAnnotation(PatternApplications2EcoreConnector.ANNOTATION_SRC_PATTERN_APPLICATIONS_REF);
	}
	
	public static EAnnotation createPointerToPatternApplications(EPackage rootPackage) {
		EAnnotation pointerToAllPatternApplications = EcoreFactory.eINSTANCE.createEAnnotation();
		pointerToAllPatternApplications.setSource(PatternApplications2EcoreConnector.ANNOTATION_SRC_PATTERN_APPLICATIONS_REF);
		pointerToAllPatternApplications.setEModelElement(rootPackage);
		return pointerToAllPatternApplications;
	}
	
	public static PatternApplications getLinkedPatternApplications(EPackage rootPackage)
	{
		EAnnotation pointerToAllPatternApplications = getPointerToPatternApplications(rootPackage);
		if (pointerToAllPatternApplications != null
				&& !pointerToAllPatternApplications.getReferences().isEmpty())
		{
			if (pointerToAllPatternApplications.getReferences().size() > 1) {
				throw new IllegalStateException("There is more than one reference from an EPackage to a PatternApplications element.");
			}
			
			PatternApplications referencedPatternApplications = (PatternApplications) pointerToAllPatternApplications.getReferences().get(0);
			return referencedPatternApplications;
		}
		return null;
	}
	
	public static void linkPatternApplicationsRootToEPackage(PatternApplications patternApplicationsRootNode, EPackage rootPackage) {
		patternApplicationsRootNode.setDesignModelRoot(rootPackage);
	}
	
	public static void linkEPackageToPatternApplicationsRoot(EPackage rootPackage, PatternApplications patternApplicationsRootNode) {
		EAnnotation pointerToAllPatternApplications = getPointerToPatternApplications(rootPackage);
		if (pointerToAllPatternApplications == null) {
			throw new IllegalStateException("No PatternApplications pointer (EAnnotation) in the given EPackage available.");
		}
		
		if (!pointerToAllPatternApplications.getReferences().isEmpty()) {
			pointerToAllPatternApplications.getReferences().clear();
		}
		pointerToAllPatternApplications.getReferences().add(patternApplicationsRootNode);
	}
	
	public static EPackage getDesignModelRoot(PatternApplications patternApplicationsRootNode) {
		if (patternApplicationsRootNode != null) {
			return (EPackage) patternApplicationsRootNode.getDesignModelRoot();
		}
		return null;
	}
	
	public static PatternApplications createPatternApplicationsForEcoreFile(Resource ecoreFile) {
		Assert.isNotNull(ecoreFile);
		Assert.isNotNull(ecoreFile.getURI(), "Ecore file has no URI.");
		
		URI fileUri = ecoreFile.getURI();
		String fileExtension = fileUri.fileExtension();
		Assert.isNotNull(fileExtension, "Given resource has no file extension.");
		Assert.isTrue(fileExtension.equals("ecore"), "Given resource has not the expected file extension: .ecore");
		
		Assert.isTrue(ecoreFile.isLoaded(), "Ecore file " + ecoreFile.getURI().toString() + " is not loaded.");
		
		if (ecoreFile.getContents().size() != 1) {
			throw new IllegalStateException("More than one root element or none found in ecore file!");
		}
		
		if (!(ecoreFile.getContents().get(0) instanceof EPackage)) {
			throw new IllegalStateException("Root element in ecore file is no an EPackage!");
		}
		
		EPackage rootPackageInEcoreFile = (EPackage) ecoreFile.getContents().get(0);
		
		String fileName = fileUri.trimFileExtension().lastSegment();
		fileName = fileName + "." + fileExtension;
		
		// actual creation
		PatternApplications patternApplicationsModel = MappingFactory.eINSTANCE.createPatternApplications();
		patternApplicationsModel.setName("Pattern Applications Model for " + fileName);
		linkPatternApplicationsRootToEPackage(patternApplicationsModel, rootPackageInEcoreFile);
		
		return patternApplicationsModel;
	}

}
