/**
 * 
 */
package de.upb.pose.ecoremodelling.editor.provider;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider;

import de.upb.pose.ecoremodelling.editor.features.AddEClassFeature;

/**
 * @author Dietrich Travkin
 *
 */
public class ClassDiagramFeatureProvider extends DefaultFeatureProvider
{
	public ClassDiagramFeatureProvider(IDiagramTypeProvider dtp)
	{
		super(dtp);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.features.impl.AbstractFeatureProvider#getAddFeature(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	public IAddFeature getAddFeature(IAddContext context)
	{
		if (context.getNewObject() instanceof EClass)
		{
			return new AddEClassFeature(this);
		}
		return super.getAddFeature(context);
	}
}