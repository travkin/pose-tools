/**
 * 
 */
package de.upb.pose.ecoremodelling.editor.provider;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;

/**
 * @author Dietrich Travkin
 *
 */
public class ClassDiagramTypeProvider extends AbstractDiagramTypeProvider
{
	public ClassDiagramTypeProvider()
	{
		super();
		setFeatureProvider(new ClassDiagramFeatureProvider(this));
	}
}
