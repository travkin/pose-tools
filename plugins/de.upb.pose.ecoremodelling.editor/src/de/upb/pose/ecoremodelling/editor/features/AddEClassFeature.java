/**
 * 
 */
package de.upb.pose.ecoremodelling.editor.features;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

/**
 * @author Dietrich Travkin
 *
 */
public class AddEClassFeature extends AbstractAddShapeFeature
{
    private static final IColorConstant CLASS_TEXT_FOREGROUND = new ColorConstant(51, 51, 153);     
    private static final IColorConstant CLASS_FOREGROUND = new ColorConstant(255, 102, 0);
    private static final IColorConstant CLASS_BACKGROUND = new ColorConstant(255, 204, 153);

	public AddEClassFeature(IFeatureProvider fp)
	{
		super(fp);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IAdd#canAdd(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	public boolean canAdd(IAddContext context)
	{
		if (context.getNewObject() instanceof EClass)
		{

			if (context.getTargetContainer() instanceof Diagram)
			{
				return true;
			}
		}

		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.graphiti.func.IAdd#add(org.eclipse.graphiti.features.context.IAddContext)
	 */
	@Override
	public PictogramElement add(IAddContext context)
	{
		EClass addedClass = (EClass) context.getNewObject();
        Diagram targetDiagram = (Diagram) context.getTargetContainer();

        IPeCreateService peCreateService = Graphiti.getPeCreateService();
        ContainerShape containerShape = peCreateService.createContainerShape(targetDiagram, true);

        // define a default size for the shape
        int width = 100;
        int height = 50; 
        IGaService gaService = Graphiti.getGaService();

        {
			// create and set graphics algorithm
			Rectangle rectangle = gaService.createRectangle(containerShape);
			rectangle.setForeground(manageColor(CLASS_FOREGROUND));
			rectangle.setBackground(manageColor(CLASS_BACKGROUND));
			rectangle.setLineWidth(2);
//			rectangle.setFilled(true); 
//			rectangle.setLineStyle(LineStyleEnum.SOLID); 
//			rectangle.setLineVisible(true); 
//			rectangle.setTransparency(0.0);

			gaService.setLocationAndSize(rectangle, context.getX(), context.getY(), width, height);

			// if added Class has no resource we add it to the resource
			// of the diagram
			// in a real scenario the business model would have its own resource
			if (addedClass.eResource() == null)
			{
				// FIXME add the class to the right resource
				getDiagram().eResource().getContents().add(addedClass);
			}

			// create link and wire it
			link(containerShape, addedClass);
        }

        // SHAPE WITH LINE
        {
            // create shape for line
            Shape shape = peCreateService.createShape(containerShape, false);

            // create and set graphics algorithm
            Polyline polyline = gaService.createPolyline(shape, new int[] { 0, 20, width, 20 });
            polyline.setForeground(manageColor(CLASS_FOREGROUND));
            polyline.setLineWidth(2);
        }
 
        // SHAPE WITH TEXT
        {
            // create shape for text
            Shape shape = peCreateService.createShape(containerShape, false);
 
            // create and set text graphics algorithm
            Text text = gaService.createDefaultText(getDiagram(), shape, addedClass.getName());
            text.setForeground(manageColor(CLASS_TEXT_FOREGROUND));
            text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
            text.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
            //text.getFont().setBold(true);
            gaService.setLocationAndSize(text, 0, 0, width, 20);

            // create link and wire it
            link(shape, addedClass);
        }

        return containerShape;
	}

}
