/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a set of software design elements or pattern specification elements. All elements in a set fragment are allowed to occurr as a whole arbitrarily often in the pattern implementation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.SetFragment#getContainedElements <em>Contained Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getSetFragment()
 * @generated
 */
public interface SetFragment extends PatternElement, SetFragmentElement {
	/**
	 * Returns the value of the '<em><b>Contained Elements</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.SetFragmentElement}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.SetFragmentElement#getContainingSets <em>Containing Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The elements that are contained in this set.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Contained Elements</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getSetFragment_ContainedElements()
	 * @see de.upb.pose.specification.SetFragmentElement#getContainingSets
	 * @generated
	 */
	EList<SetFragmentElement> getContainedElements();

} // SetFragment
