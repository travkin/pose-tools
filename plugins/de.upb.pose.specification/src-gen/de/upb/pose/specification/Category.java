/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Named;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a category for design patterns.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.Category#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link de.upb.pose.specification.Category#getParent <em>Parent</em>}</li>
 *   <li>{@link de.upb.pose.specification.Category#getChildren <em>Children</em>}</li>
 *   <li>{@link de.upb.pose.specification.Category#getPatterns <em>Patterns</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getCategory()
 * @generated
 */
public interface Category extends Named, Commentable {
	/**
	 * Returns the value of the '<em><b>Catalog</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignPatternCatalog#getCategories <em>Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The catalog in which the category is contained in.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Catalog</em>' container reference.
	 * @see #setCatalog(DesignPatternCatalog)
	 * @see de.upb.pose.specification.SpecificationPackage#getCategory_Catalog()
	 * @see de.upb.pose.specification.DesignPatternCatalog#getCategories
	 * @generated
	 */
	DesignPatternCatalog getCatalog();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.Category#getCatalog <em>Catalog</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catalog</em>' container reference.
	 * @see #getCatalog()
	 * @generated
	 */
	void setCatalog(DesignPatternCatalog value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.Category#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parent category of this category.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Category)
	 * @see de.upb.pose.specification.SpecificationPackage#getCategory_Parent()
	 * @see de.upb.pose.specification.Category#getChildren
	 * @generated
	 */
	Category getParent();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.Category#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Category value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.Category}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.Category#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The sub-categories of this category.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getCategory_Children()
	 * @see de.upb.pose.specification.Category#getParent
	 * @generated
	 */
	EList<Category> getChildren();

	/**
	 * Returns the value of the '<em><b>Patterns</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.DesignPattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignPattern#getCategories <em>Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The design patterns that belong to this category.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Patterns</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getCategory_Patterns()
	 * @see de.upb.pose.specification.DesignPattern#getCategories
	 * @generated
	 */
	EList<DesignPattern> getPatterns();

} // Category
