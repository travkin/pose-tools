/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Named;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a design pattern. Each design pattern is defined by one or multiple alternative design pattern specifications.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.DesignPattern#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPattern#getCategories <em>Categories</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPattern#getSpecifications <em>Specifications</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getDesignPattern()
 * @generated
 */
public interface DesignPattern extends Named, Commentable {
	/**
	 * Returns the value of the '<em><b>Catalog</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignPatternCatalog#getPatterns <em>Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The catalog in which this design pattern is contained in.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Catalog</em>' container reference.
	 * @see #setCatalog(DesignPatternCatalog)
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPattern_Catalog()
	 * @see de.upb.pose.specification.DesignPatternCatalog#getPatterns
	 * @generated
	 */
	DesignPatternCatalog getCatalog();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.DesignPattern#getCatalog <em>Catalog</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Catalog</em>' container reference.
	 * @see #getCatalog()
	 * @generated
	 */
	void setCatalog(DesignPatternCatalog value);

	/**
	 * Returns the value of the '<em><b>Categories</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.Category}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.Category#getPatterns <em>Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The categories to which this design pattern belongs to.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Categories</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPattern_Categories()
	 * @see de.upb.pose.specification.Category#getPatterns
	 * @generated
	 */
	EList<Category> getCategories();

	/**
	 * Returns the value of the '<em><b>Specifications</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.PatternSpecification}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.PatternSpecification#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The pattern specifications for this design pattern.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Specifications</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPattern_Specifications()
	 * @see de.upb.pose.specification.PatternSpecification#getPattern
	 * @generated
	 */
	EList<PatternSpecification> getSpecifications();

} // DesignPattern
