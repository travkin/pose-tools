/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the base class for all design elements. Each design element represents an element in a software design model, i.e. a part of a pattern implementation.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.DesignElement#getDesignModel <em>Design Model</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignElement#getTasks <em>Tasks</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getDesignElement()
 * @generated
 */
public interface DesignElement extends PatternElement, SetFragmentElement {
	/**
	 * Returns the value of the '<em><b>Design Model</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignModel#getDesignElements <em>Design Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Design Model</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Design Model</em>' container reference.
	 * @see #setDesignModel(DesignModel)
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignElement_DesignModel()
	 * @see de.upb.pose.specification.DesignModel#getDesignElements
	 * @generated
	 */
	DesignModel getDesignModel();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.DesignElement#getDesignModel <em>Design Model</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Design Model</em>' container reference.
	 * @see #getDesignModel()
	 * @generated
	 */
	void setDesignModel(DesignModel value);

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.TaskDescription}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.TaskDescription#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The task descriptions that are belonging to this design element.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignElement_Tasks()
	 * @see de.upb.pose.specification.TaskDescription#getElement
	 * @generated
	 */
	EList<TaskDescription> getTasks();

} // DesignElement
