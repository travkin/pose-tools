/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Named;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.types.PrimitiveType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern Catalog</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a collection of categorized design patterns and the patterns' formal specifications.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.DesignPatternCatalog#getCategories <em>Categories</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPatternCatalog#getPatterns <em>Patterns</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPatternCatalog#getAccessTypes <em>Access Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPatternCatalog#getPrimitiveTypes <em>Primitive Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.DesignPatternCatalog#getNullVariable <em>Null Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog()
 * @generated
 */
public interface DesignPatternCatalog extends Named, Commentable {
	/**
	 * Returns the value of the '<em><b>Categories</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.Category}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.Category#getCatalog <em>Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The categories that are contained in this catalog.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Categories</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog_Categories()
	 * @see de.upb.pose.specification.Category#getCatalog
	 * @generated
	 */
	EList<Category> getCategories();

	/**
	 * Returns the value of the '<em><b>Patterns</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.DesignPattern}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignPattern#getCatalog <em>Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The design patterns that are contained in this catalog.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Patterns</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog_Patterns()
	 * @see de.upb.pose.specification.DesignPattern#getCatalog
	 * @generated
	 */
	EList<DesignPattern> getPatterns();

	/**
	 * Returns the value of the '<em><b>Access Types</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.access.AccessType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Types</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog_AccessTypes()
	 * @generated
	 */
	EList<AccessType> getAccessTypes();

	/**
	 * Returns the value of the '<em><b>Primitive Types</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.PrimitiveType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primitive Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primitive Types</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog_PrimitiveTypes()
	 * @generated
	 */
	EList<PrimitiveType> getPrimitiveTypes();

	/**
	 * Returns the value of the '<em><b>Null Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Variable</em>' containment reference.
	 * @see #setNullVariable(NullVariable)
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignPatternCatalog_NullVariable()
	 * @generated
	 */
	NullVariable getNullVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.DesignPatternCatalog#getNullVariable <em>Null Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Variable</em>' containment reference.
	 * @see #getNullVariable()
	 * @generated
	 */
	void setNullVariable(NullVariable value);

} // DesignPatternCatalog
