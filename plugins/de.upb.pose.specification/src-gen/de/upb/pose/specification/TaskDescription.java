/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import de.upb.pose.core.Commentable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a task description for a design element. The described task is a necessary modification of the software design model where a design pattern is applied to completely implement the specified pattern.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.TaskDescription#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getTaskDescription()
 * @generated
 */
public interface TaskDescription extends PatternElement, Commentable, SetFragmentElement {
	/**
	 * Returns the value of the '<em><b>Element</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignElement#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The design element to which this task description belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Element</em>' container reference.
	 * @see #setElement(DesignElement)
	 * @see de.upb.pose.specification.SpecificationPackage#getTaskDescription_Element()
	 * @see de.upb.pose.specification.DesignElement#getTasks
	 * @generated
	 */
	DesignElement getElement();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.TaskDescription#getElement <em>Element</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' container reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(DesignElement value);

} // TaskDescription
