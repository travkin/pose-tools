/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignModel;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.TaskDescription;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.impl.AccessPackageImpl;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.impl.ActionsPackageImpl;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SpecificationPackageImpl extends EPackageImpl implements SpecificationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternCatalogEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass categoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setFragmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass setFragmentElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.specification.SpecificationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SpecificationPackageImpl() {
		super(eNS_URI, SpecificationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SpecificationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SpecificationPackage init() {
		if (isInited)
			return (SpecificationPackage) EPackage.Registry.INSTANCE.getEPackage(SpecificationPackage.eNS_URI);

		// Obtain or create and register package
		SpecificationPackageImpl theSpecificationPackage = (SpecificationPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof SpecificationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
				: new SpecificationPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		AccessPackageImpl theAccessPackage = (AccessPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) instanceof AccessPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) : AccessPackage.eINSTANCE);
		ActionsPackageImpl theActionsPackage = (ActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) instanceof ActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) : ActionsPackage.eINSTANCE);
		SubsystemsPackageImpl theSubsystemsPackage = (SubsystemsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) instanceof SubsystemsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) : SubsystemsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theSpecificationPackage.createPackageContents();
		theAccessPackage.createPackageContents();
		theActionsPackage.createPackageContents();
		theSubsystemsPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theSpecificationPackage.initializePackageContents();
		theAccessPackage.initializePackageContents();
		theActionsPackage.initializePackageContents();
		theSubsystemsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSpecificationPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SpecificationPackage.eNS_URI, theSpecificationPackage);
		return theSpecificationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPatternCatalog() {
		return designPatternCatalogEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternCatalog_Categories() {
		return (EReference) designPatternCatalogEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternCatalog_Patterns() {
		return (EReference) designPatternCatalogEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternCatalog_AccessTypes() {
		return (EReference) designPatternCatalogEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternCatalog_PrimitiveTypes() {
		return (EReference) designPatternCatalogEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternCatalog_NullVariable() {
		return (EReference) designPatternCatalogEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCategory() {
		return categoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategory_Catalog() {
		return (EReference) categoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategory_Parent() {
		return (EReference) categoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategory_Children() {
		return (EReference) categoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCategory_Patterns() {
		return (EReference) categoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPattern() {
		return designPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPattern_Catalog() {
		return (EReference) designPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPattern_Categories() {
		return (EReference) designPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPattern_Specifications() {
		return (EReference) designPatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternSpecification() {
		return patternSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_Pattern() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_PatternElements() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_DesignElements() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_SetFragments() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_AccessRules() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSpecification_Environment() {
		return (EReference) patternSpecificationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternElement() {
		return patternElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternElement_Specification() {
		return (EReference) patternElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPatternElement_CanBeGenerated() {
		return (EAttribute) patternElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternElement_AccessorRules() {
		return (EReference) patternElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskDescription() {
		return taskDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDescription_Element() {
		return (EReference) taskDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignElement() {
		return designElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignElement_DesignModel() {
		return (EReference) designElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignElement_Tasks() {
		return (EReference) designElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetFragment() {
		return setFragmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragment_ContainedElements() {
		return (EReference) setFragmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignModel() {
		return designModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignModel_DesignElements() {
		return (EReference) designModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSetFragmentElement() {
		return setFragmentElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSetFragmentElement_ContainingSets() {
		return (EReference) setFragmentElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationFactory getSpecificationFactory() {
		return (SpecificationFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		designPatternCatalogEClass = createEClass(DESIGN_PATTERN_CATALOG);
		createEReference(designPatternCatalogEClass, DESIGN_PATTERN_CATALOG__CATEGORIES);
		createEReference(designPatternCatalogEClass, DESIGN_PATTERN_CATALOG__PATTERNS);
		createEReference(designPatternCatalogEClass, DESIGN_PATTERN_CATALOG__ACCESS_TYPES);
		createEReference(designPatternCatalogEClass, DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES);
		createEReference(designPatternCatalogEClass, DESIGN_PATTERN_CATALOG__NULL_VARIABLE);

		categoryEClass = createEClass(CATEGORY);
		createEReference(categoryEClass, CATEGORY__CATALOG);
		createEReference(categoryEClass, CATEGORY__PARENT);
		createEReference(categoryEClass, CATEGORY__CHILDREN);
		createEReference(categoryEClass, CATEGORY__PATTERNS);

		designPatternEClass = createEClass(DESIGN_PATTERN);
		createEReference(designPatternEClass, DESIGN_PATTERN__CATALOG);
		createEReference(designPatternEClass, DESIGN_PATTERN__CATEGORIES);
		createEReference(designPatternEClass, DESIGN_PATTERN__SPECIFICATIONS);

		patternSpecificationEClass = createEClass(PATTERN_SPECIFICATION);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__PATTERN);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__PATTERN_ELEMENTS);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__DESIGN_ELEMENTS);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__SET_FRAGMENTS);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__ACCESS_RULES);
		createEReference(patternSpecificationEClass, PATTERN_SPECIFICATION__ENVIRONMENT);

		patternElementEClass = createEClass(PATTERN_ELEMENT);
		createEReference(patternElementEClass, PATTERN_ELEMENT__SPECIFICATION);
		createEAttribute(patternElementEClass, PATTERN_ELEMENT__CAN_BE_GENERATED);
		createEReference(patternElementEClass, PATTERN_ELEMENT__ACCESSOR_RULES);

		taskDescriptionEClass = createEClass(TASK_DESCRIPTION);
		createEReference(taskDescriptionEClass, TASK_DESCRIPTION__ELEMENT);

		designElementEClass = createEClass(DESIGN_ELEMENT);
		createEReference(designElementEClass, DESIGN_ELEMENT__DESIGN_MODEL);
		createEReference(designElementEClass, DESIGN_ELEMENT__TASKS);

		setFragmentEClass = createEClass(SET_FRAGMENT);
		createEReference(setFragmentEClass, SET_FRAGMENT__CONTAINED_ELEMENTS);

		designModelEClass = createEClass(DESIGN_MODEL);
		createEReference(designModelEClass, DESIGN_MODEL__DESIGN_ELEMENTS);

		setFragmentElementEClass = createEClass(SET_FRAGMENT_ELEMENT);
		createEReference(setFragmentElementEClass, SET_FRAGMENT_ELEMENT__CONTAINING_SETS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AccessPackage theAccessPackage = (AccessPackage) EPackage.Registry.INSTANCE.getEPackage(AccessPackage.eNS_URI);
		ActionsPackage theActionsPackage = (ActionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI);
		SubsystemsPackage theSubsystemsPackage = (SubsystemsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage) EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theAccessPackage);
		getESubpackages().add(theActionsPackage);
		getESubpackages().add(theSubsystemsPackage);
		getESubpackages().add(theTypesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		designPatternCatalogEClass.getESuperTypes().add(theCorePackage.getNamed());
		designPatternCatalogEClass.getESuperTypes().add(theCorePackage.getCommentable());
		categoryEClass.getESuperTypes().add(theCorePackage.getNamed());
		categoryEClass.getESuperTypes().add(theCorePackage.getCommentable());
		designPatternEClass.getESuperTypes().add(theCorePackage.getNamed());
		designPatternEClass.getESuperTypes().add(theCorePackage.getCommentable());
		patternSpecificationEClass.getESuperTypes().add(theCorePackage.getNamed());
		patternSpecificationEClass.getESuperTypes().add(theCorePackage.getCommentable());
		patternElementEClass.getESuperTypes().add(theCorePackage.getNamed());
		taskDescriptionEClass.getESuperTypes().add(this.getPatternElement());
		taskDescriptionEClass.getESuperTypes().add(theCorePackage.getCommentable());
		taskDescriptionEClass.getESuperTypes().add(this.getSetFragmentElement());
		designElementEClass.getESuperTypes().add(this.getPatternElement());
		designElementEClass.getESuperTypes().add(this.getSetFragmentElement());
		setFragmentEClass.getESuperTypes().add(this.getPatternElement());
		setFragmentEClass.getESuperTypes().add(this.getSetFragmentElement());
		designModelEClass.getESuperTypes().add(this.getPatternElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(designPatternCatalogEClass, DesignPatternCatalog.class,
				"DesignPatternCatalog", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getDesignPatternCatalog_Categories(),
				this.getCategory(),
				this.getCategory_Catalog(),
				"categories", null, 0, -1, DesignPatternCatalog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPatternCatalog_Patterns(),
				this.getDesignPattern(),
				this.getDesignPattern_Catalog(),
				"patterns", null, 0, -1, DesignPatternCatalog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPatternCatalog_AccessTypes(),
				theAccessPackage.getAccessType(),
				null,
				"accessTypes", null, 0, -1, DesignPatternCatalog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPatternCatalog_PrimitiveTypes(),
				theTypesPackage.getPrimitiveType(),
				null,
				"primitiveTypes", null, 0, -1, DesignPatternCatalog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPatternCatalog_NullVariable(),
				theActionsPackage.getNullVariable(),
				null,
				"nullVariable", null, 0, 1, DesignPatternCatalog.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(categoryEClass, Category.class, "Category", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getCategory_Catalog(),
				this.getDesignPatternCatalog(),
				this.getDesignPatternCatalog_Categories(),
				"catalog", null, 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getCategory_Parent(),
				this.getCategory(),
				this.getCategory_Children(),
				"parent", null, 0, 1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getCategory_Children(),
				this.getCategory(),
				this.getCategory_Parent(),
				"children", null, 0, -1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getCategory_Patterns(),
				this.getDesignPattern(),
				this.getDesignPattern_Categories(),
				"patterns", null, 0, -1, Category.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(designPatternEClass, DesignPattern.class,
				"DesignPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getDesignPattern_Catalog(),
				this.getDesignPatternCatalog(),
				this.getDesignPatternCatalog_Patterns(),
				"catalog", null, 1, 1, DesignPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPattern_Categories(),
				this.getCategory(),
				this.getCategory_Patterns(),
				"categories", null, 0, -1, DesignPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignPattern_Specifications(),
				this.getPatternSpecification(),
				this.getPatternSpecification_Pattern(),
				"specifications", null, 0, -1, DesignPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(patternSpecificationEClass, PatternSpecification.class,
				"PatternSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_Pattern(),
				this.getDesignPattern(),
				this.getDesignPattern_Specifications(),
				"pattern", null, 1, 1, PatternSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_PatternElements(),
				this.getPatternElement(),
				this.getPatternElement_Specification(),
				"patternElements", null, 0, -1, PatternSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_DesignElements(),
				this.getDesignElement(),
				null,
				"designElements", null, 0, -1, PatternSpecification.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_SetFragments(),
				this.getSetFragment(),
				null,
				"setFragments", null, 0, -1, PatternSpecification.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_AccessRules(),
				theAccessPackage.getAccessRule(),
				null,
				"accessRules", null, 0, -1, PatternSpecification.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getPatternSpecification_Environment(),
				theAccessPackage.getPatternEnvironment(),
				null,
				"environment", null, 0, 1, PatternSpecification.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(patternElementEClass, PatternElement.class,
				"PatternElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getPatternElement_Specification(),
				this.getPatternSpecification(),
				this.getPatternSpecification_PatternElements(),
				"specification", null, 0, 1, PatternElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getPatternElement_CanBeGenerated(),
				ecorePackage.getEBoolean(),
				"canBeGenerated", "true", 1, 1, PatternElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(
				getPatternElement_AccessorRules(),
				theAccessPackage.getAccessRule(),
				theAccessPackage.getAccessRule_Accessor(),
				"accessorRules", null, 0, -1, PatternElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(taskDescriptionEClass, TaskDescription.class,
				"TaskDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getTaskDescription_Element(),
				this.getDesignElement(),
				this.getDesignElement_Tasks(),
				"element", null, 1, 1, TaskDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(designElementEClass, DesignElement.class,
				"DesignElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getDesignElement_DesignModel(),
				this.getDesignModel(),
				this.getDesignModel_DesignElements(),
				"designModel", null, 0, 1, DesignElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getDesignElement_Tasks(),
				this.getTaskDescription(),
				this.getTaskDescription_Element(),
				"tasks", null, 0, -1, DesignElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(setFragmentEClass, SetFragment.class,
				"SetFragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSetFragment_ContainedElements(),
				this.getSetFragmentElement(),
				this.getSetFragmentElement_ContainingSets(),
				"containedElements", null, 0, -1, SetFragment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(designModelEClass, DesignModel.class,
				"DesignModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getDesignModel_DesignElements(),
				this.getDesignElement(),
				this.getDesignElement_DesignModel(),
				"designElements", null, 0, -1, DesignModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(setFragmentElementEClass, SetFragmentElement.class,
				"SetFragmentElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSetFragmentElement_ContainingSets(),
				this.getSetFragment(),
				this.getSetFragment_ContainedElements(),
				"containingSets", null, 0, 2, SetFragmentElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(
				this,
				source,
				new String[] {
						"documentation", "This package and its sub-packages contain all elements that are used to specify object-oriented design patterns." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				designPatternCatalogEClass,
				source,
				new String[] {
						"documentation", "This represents a collection of categorized design patterns and the patterns\' formal specifications." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getDesignPatternCatalog_Categories(), source, new String[] {
				"documentation", "The categories that are contained in this catalog." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getDesignPatternCatalog_Patterns(), source, new String[] {
				"documentation", "The design patterns that are contained in this catalog." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(categoryEClass, source, new String[] {
				"documentation", "This represents a category for design patterns." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCategory_Catalog(), source, new String[] {
				"documentation", "The catalog in which the category is contained in." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCategory_Parent(), source, new String[] {
				"documentation", "The parent category of this category." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCategory_Children(), source, new String[] {
				"documentation", "The sub-categories of this category." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCategory_Patterns(), source, new String[] {
				"documentation", "The design patterns that belong to this category." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				designPatternEClass,
				source,
				new String[] {
						"documentation", "This represents a design pattern. Each design pattern is defined by one or multiple alternative design pattern specifications." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getDesignPattern_Catalog(), source, new String[] {
				"documentation", "The catalog in which this design pattern is contained in." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getDesignPattern_Categories(), source, new String[] {
				"documentation", "The categories to which this design pattern belongs to." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getDesignPattern_Specifications(), source, new String[] {
				"documentation", "The pattern specifications for this design pattern." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				patternSpecificationEClass,
				source,
				new String[] {
						"documentation", "This represents a pattern specification for a design pattern. Each specification describes one possibility to apply a design pattern in a software design." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getPatternSpecification_Pattern(), source, new String[] {
				"documentation", "The design pattern to which this specification belongs to." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getPatternSpecification_PatternElements(), source, new String[] {
				"documentation", "The pattern elements that are contained in this pattern specification." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getPatternSpecification_AccessRules(), source, new String[] {
				"documentation", "The access rules which are contained in this pattern specification." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				getPatternSpecification_Environment(),
				source,
				new String[] {
						"documentation", "The pattern environment that is contained in this pattern specification. Can be empty." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(patternElementEClass, source, new String[] {
				"documentation", "This is the base class for all elements that are used in a pattern specification." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getPatternElement_Specification(), source, new String[] {
				"documentation", "The specification to which this pattern element belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getPatternElement_CanBeGenerated(), source, new String[] {
				"documentation", "Whether the element can be generated." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				taskDescriptionEClass,
				source,
				new String[] {
						"documentation", "This represents a task description for a design element. The described task is a necessary modification of the software design model where a design pattern is applied to completely implement the specified pattern." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getTaskDescription_Element(), source, new String[] {
				"documentation", "The design element to which this task description belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				designElementEClass,
				source,
				new String[] {
						"documentation", "This is the base class for all design elements. Each design element represents an element in a software design model, i.e. a part of a pattern implementation." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getDesignElement_Tasks(), source, new String[] {
				"documentation", "The task descriptions that are belonging to this design element." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				setFragmentEClass,
				source,
				new String[] {
						"documentation", "This represents a set of software design elements or pattern specification elements. All elements in a set fragment are allowed to occurr as a whole arbitrarily often in the pattern implementation." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getSetFragment_ContainedElements(), source, new String[] {
				"documentation", "The elements that are contained in this set." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				designModelEClass,
				source,
				new String[] {
						"documentation", "Represents a design model that can contain several design elements. A design model represents the root element of a software model where a pattern is to be applied." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getDesignModel_DesignElements(), source, new String[] {
				"documentation", "The elements that are contained in this design model." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(setFragmentElementEClass, source, new String[] {
				"documentation", "Represents a pattern specification element that can be contained in a set fragment." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSetFragmentElement_ContainingSets(), source, new String[] {
				"documentation", "The sets in which this design element is contained in." //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL"; //$NON-NLS-1$	
		addAnnotation(
				getPatternSpecification_DesignElements(),
				source,
				new String[] {
						"derivation", "self.patternElements->select(element : PatternElement | element.oclIsKindOf(DesignElement))->collect(e : PatternElement | e.oclAsType(DesignElement))->asOrderedSet()" //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getPatternSpecification_SetFragments(),
				source,
				new String[] {
						"derivation", "self.patternElements->select(element : PatternElement | element.oclIsKindOf(SetFragment))->collect(e : PatternElement | e.oclAsType(SetFragment))->asOrderedSet()" //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getPatternSpecification_AccessRules(),
				source,
				new String[] {
						"derivation", "self.patternElements->select(element : PatternElement | element.oclIsKindOf(access::AccessRule))->collect(e : PatternElement | e.oclAsType(access::AccessRule))->asOrderedSet()" //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getPatternSpecification_Environment(),
				source,
				new String[] {
						"derivation", "let environment : OrderedSet(access::PatternEnvironment) = self.patternElements->select(element : PatternElement | element.oclIsKindOf(access::PatternEnvironment))->collect(e : PatternElement | e.oclAsType(access::PatternEnvironment))->asOrderedSet()\r\nin if environment->isEmpty() then null else environment->first() endif" //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

} //SpecificationPackageImpl
