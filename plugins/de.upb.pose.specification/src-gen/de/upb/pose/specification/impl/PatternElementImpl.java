/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.PatternElementImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternElementImpl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternElementImpl#isCanBeGenerated <em>Can Be Generated</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternElementImpl#getAccessorRules <em>Accessor Rules</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class PatternElementImpl extends IdentifierImpl implements PatternElement {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isCanBeGenerated() <em>Can Be Generated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCanBeGenerated()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CAN_BE_GENERATED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isCanBeGenerated() <em>Can Be Generated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCanBeGenerated()
	 * @generated
	 * @ordered
	 */
	protected boolean canBeGenerated = CAN_BE_GENERATED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccessorRules() <em>Accessor Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessorRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessorRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.PATTERN_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.PATTERN_ELEMENT__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternSpecification getSpecification() {
		if (eContainerFeatureID() != SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION)
			return null;
		return (PatternSpecification) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpecification(PatternSpecification newSpecification, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newSpecification,
				SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecification(PatternSpecification newSpecification) {
		if (newSpecification != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION && newSpecification != null)) {
			if (EcoreUtil.isAncestor(this, newSpecification))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newSpecification != null)
				msgs = ((InternalEObject) newSpecification).eInverseAdd(this,
						SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS, PatternSpecification.class, msgs);
			msgs = basicSetSpecification(newSpecification, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION,
					newSpecification, newSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCanBeGenerated() {
		return canBeGenerated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCanBeGenerated(boolean newCanBeGenerated) {
		boolean oldCanBeGenerated = canBeGenerated;
		canBeGenerated = newCanBeGenerated;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED, oldCanBeGenerated, canBeGenerated));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessorRules() {
		if (accessorRules == null) {
			accessorRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES, AccessPackage.ACCESS_RULE__ACCESSOR);
		}
		return accessorRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetSpecification((PatternSpecification) otherEnd, msgs);
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessorRules()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			return basicSetSpecification(null, msgs);
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			return ((InternalEList<?>) getAccessorRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			return eInternalContainer().eInverseRemove(this,
					SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS, PatternSpecification.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__NAME:
			return getName();
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			return getSpecification();
		case SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED:
			return isCanBeGenerated();
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			return getAccessorRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__NAME:
			setName((String) newValue);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			setSpecification((PatternSpecification) newValue);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED:
			setCanBeGenerated((Boolean) newValue);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			getAccessorRules().clear();
			getAccessorRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__NAME:
			setName(NAME_EDEFAULT);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			setSpecification((PatternSpecification) null);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED:
			setCanBeGenerated(CAN_BE_GENERATED_EDEFAULT);
			return;
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			getAccessorRules().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_ELEMENT__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION:
			return getSpecification() != null;
		case SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED:
			return canBeGenerated != CAN_BE_GENERATED_EDEFAULT;
		case SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES:
			return accessorRules != null && !accessorRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", canBeGenerated: "); //$NON-NLS-1$
		result.append(canBeGenerated);
		result.append(')');
		return result.toString();
	}

} //PatternElementImpl
