/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignModel;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.TaskDescription;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.DesignElementImpl#getContainingSets <em>Containing Sets</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignElementImpl#getDesignModel <em>Design Model</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignElementImpl#getTasks <em>Tasks</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class DesignElementImpl extends PatternElementImpl implements DesignElement {
	/**
	 * The cached value of the '{@link #getContainingSets() <em>Containing Sets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSets()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragment> containingSets;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDescription> tasks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.DESIGN_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragment> getContainingSets() {
		if (containingSets == null) {
			containingSets = new EObjectWithInverseResolvingEList.ManyInverse<SetFragment>(SetFragment.class, this,
					SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS,
					SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS);
		}
		return containingSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignModel getDesignModel() {
		if (eContainerFeatureID() != SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL)
			return null;
		return (DesignModel) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDesignModel(DesignModel newDesignModel, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newDesignModel, SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDesignModel(DesignModel newDesignModel) {
		if (newDesignModel != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL && newDesignModel != null)) {
			if (EcoreUtil.isAncestor(this, newDesignModel))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDesignModel != null)
				msgs = ((InternalEObject) newDesignModel).eInverseAdd(this,
						SpecificationPackage.DESIGN_MODEL__DESIGN_ELEMENTS, DesignModel.class, msgs);
			msgs = basicSetDesignModel(newDesignModel, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL,
					newDesignModel, newDesignModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDescription> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentWithInverseEList<TaskDescription>(TaskDescription.class, this,
					SpecificationPackage.DESIGN_ELEMENT__TASKS, SpecificationPackage.TASK_DESCRIPTION__ELEMENT);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainingSets()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetDesignModel((DesignModel) otherEnd, msgs);
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getTasks()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			return ((InternalEList<?>) getContainingSets()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			return basicSetDesignModel(null, msgs);
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			return ((InternalEList<?>) getTasks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.DESIGN_MODEL__DESIGN_ELEMENTS,
					DesignModel.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			return getContainingSets();
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			return getDesignModel();
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			return getTasks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			getContainingSets().clear();
			getContainingSets().addAll((Collection<? extends SetFragment>) newValue);
			return;
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			setDesignModel((DesignModel) newValue);
			return;
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			getTasks().clear();
			getTasks().addAll((Collection<? extends TaskDescription>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			getContainingSets().clear();
			return;
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			setDesignModel((DesignModel) null);
			return;
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			getTasks().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
			return containingSets != null && !containingSets.isEmpty();
		case SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL:
			return getDesignModel() != null;
		case SpecificationPackage.DESIGN_ELEMENT__TASKS:
			return tasks != null && !tasks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == SetFragmentElement.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS:
				return SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == SetFragmentElement.class) {
			switch (baseFeatureID) {
			case SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS:
				return SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //DesignElementImpl
