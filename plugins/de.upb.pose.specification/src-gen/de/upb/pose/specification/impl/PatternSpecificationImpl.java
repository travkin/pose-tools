/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.PatternEnvironment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getPattern <em>Pattern</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getPatternElements <em>Pattern Elements</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getDesignElements <em>Design Elements</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getSetFragments <em>Set Fragments</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getAccessRules <em>Access Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.PatternSpecificationImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PatternSpecificationImpl extends IdentifierImpl implements PatternSpecification {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPatternElements() <em>Pattern Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPatternElements()
	 * @generated
	 * @ordered
	 */
	protected EList<PatternElement> patternElements;

	/**
	 * The cached setting delegate for the '{@link #getDesignElements() <em>Design Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesignElements()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate DESIGN_ELEMENTS__ESETTING_DELEGATE = ((EStructuralFeature.Internal) SpecificationPackage.Literals.PATTERN_SPECIFICATION__DESIGN_ELEMENTS)
			.getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getSetFragments() <em>Set Fragments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSetFragments()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate SET_FRAGMENTS__ESETTING_DELEGATE = ((EStructuralFeature.Internal) SpecificationPackage.Literals.PATTERN_SPECIFICATION__SET_FRAGMENTS)
			.getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getAccessRules() <em>Access Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessRules()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate ACCESS_RULES__ESETTING_DELEGATE = ((EStructuralFeature.Internal) SpecificationPackage.Literals.PATTERN_SPECIFICATION__ACCESS_RULES)
			.getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getEnvironment() <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate ENVIRONMENT__ESETTING_DELEGATE = ((EStructuralFeature.Internal) SpecificationPackage.Literals.PATTERN_SPECIFICATION__ENVIRONMENT)
			.getSettingDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.PATTERN_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.PATTERN_SPECIFICATION__NAME,
					oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.PATTERN_SPECIFICATION__COMMENT,
					oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPattern getPattern() {
		if (eContainerFeatureID() != SpecificationPackage.PATTERN_SPECIFICATION__PATTERN)
			return null;
		return (DesignPattern) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPattern(DesignPattern newPattern, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newPattern, SpecificationPackage.PATTERN_SPECIFICATION__PATTERN,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPattern(DesignPattern newPattern) {
		if (newPattern != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.PATTERN_SPECIFICATION__PATTERN && newPattern != null)) {
			if (EcoreUtil.isAncestor(this, newPattern))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newPattern != null)
				msgs = ((InternalEObject) newPattern).eInverseAdd(this,
						SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS, DesignPattern.class, msgs);
			msgs = basicSetPattern(newPattern, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.PATTERN_SPECIFICATION__PATTERN,
					newPattern, newPattern));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PatternElement> getPatternElements() {
		if (patternElements == null) {
			patternElements = new EObjectContainmentWithInverseEList<PatternElement>(PatternElement.class, this,
					SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS,
					SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION);
		}
		return patternElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DesignElement> getDesignElements() {
		return (EList<DesignElement>) DESIGN_ELEMENTS__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SetFragment> getSetFragments() {
		return (EList<SetFragment>) SET_FRAGMENTS__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AccessRule> getAccessRules() {
		return (EList<AccessRule>) ACCESS_RULES__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternEnvironment getEnvironment() {
		return (PatternEnvironment) ENVIRONMENT__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternEnvironment basicGetEnvironment() {
		return (PatternEnvironment) ENVIRONMENT__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetPattern((DesignPattern) otherEnd, msgs);
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPatternElements()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			return basicSetPattern(null, msgs);
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			return ((InternalEList<?>) getPatternElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS,
					DesignPattern.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__NAME:
			return getName();
		case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
			return getComment();
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			return getPattern();
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			return getPatternElements();
		case SpecificationPackage.PATTERN_SPECIFICATION__DESIGN_ELEMENTS:
			return getDesignElements();
		case SpecificationPackage.PATTERN_SPECIFICATION__SET_FRAGMENTS:
			return getSetFragments();
		case SpecificationPackage.PATTERN_SPECIFICATION__ACCESS_RULES:
			return getAccessRules();
		case SpecificationPackage.PATTERN_SPECIFICATION__ENVIRONMENT:
			if (resolve)
				return getEnvironment();
			return basicGetEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__NAME:
			setName((String) newValue);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
			setComment((String) newValue);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			setPattern((DesignPattern) newValue);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			getPatternElements().clear();
			getPatternElements().addAll((Collection<? extends PatternElement>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__NAME:
			setName(NAME_EDEFAULT);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			setPattern((DesignPattern) null);
			return;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			getPatternElements().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.PATTERN_SPECIFICATION__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN:
			return getPattern() != null;
		case SpecificationPackage.PATTERN_SPECIFICATION__PATTERN_ELEMENTS:
			return patternElements != null && !patternElements.isEmpty();
		case SpecificationPackage.PATTERN_SPECIFICATION__DESIGN_ELEMENTS:
			return DESIGN_ELEMENTS__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		case SpecificationPackage.PATTERN_SPECIFICATION__SET_FRAGMENTS:
			return SET_FRAGMENTS__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		case SpecificationPackage.PATTERN_SPECIFICATION__ACCESS_RULES:
			return ACCESS_RULES__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		case SpecificationPackage.PATTERN_SPECIFICATION__ENVIRONMENT:
			return ENVIRONMENT__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.PATTERN_SPECIFICATION__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return SpecificationPackage.PATTERN_SPECIFICATION__COMMENT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(')');
		return result.toString();
	}

} //PatternSpecificationImpl
