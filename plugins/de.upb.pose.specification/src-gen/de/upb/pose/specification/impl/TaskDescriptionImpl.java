/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.TaskDescription;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.TaskDescriptionImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.TaskDescriptionImpl#getContainingSets <em>Containing Sets</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.TaskDescriptionImpl#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TaskDescriptionImpl extends PatternElementImpl implements TaskDescription {
	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainingSets() <em>Containing Sets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSets()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragment> containingSets;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.TASK_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragment> getContainingSets() {
		if (containingSets == null) {
			containingSets = new EObjectWithInverseResolvingEList.ManyInverse<SetFragment>(SetFragment.class, this,
					SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS,
					SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS);
		}
		return containingSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.TASK_DESCRIPTION__COMMENT,
					oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignElement getElement() {
		if (eContainerFeatureID() != SpecificationPackage.TASK_DESCRIPTION__ELEMENT)
			return null;
		return (DesignElement) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElement(DesignElement newElement, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newElement, SpecificationPackage.TASK_DESCRIPTION__ELEMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement(DesignElement newElement) {
		if (newElement != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.TASK_DESCRIPTION__ELEMENT && newElement != null)) {
			if (EcoreUtil.isAncestor(this, newElement))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newElement != null)
				msgs = ((InternalEObject) newElement).eInverseAdd(this, SpecificationPackage.DESIGN_ELEMENT__TASKS,
						DesignElement.class, msgs);
			msgs = basicSetElement(newElement, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.TASK_DESCRIPTION__ELEMENT,
					newElement, newElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainingSets()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetElement((DesignElement) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			return ((InternalEList<?>) getContainingSets()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			return basicSetElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.DESIGN_ELEMENT__TASKS,
					DesignElement.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__COMMENT:
			return getComment();
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			return getContainingSets();
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			return getElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__COMMENT:
			setComment((String) newValue);
			return;
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			getContainingSets().clear();
			getContainingSets().addAll((Collection<? extends SetFragment>) newValue);
			return;
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			setElement((DesignElement) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			getContainingSets().clear();
			return;
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			setElement((DesignElement) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.TASK_DESCRIPTION__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
			return containingSets != null && !containingSets.isEmpty();
		case SpecificationPackage.TASK_DESCRIPTION__ELEMENT:
			return getElement() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.TASK_DESCRIPTION__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		if (baseClass == SetFragmentElement.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS:
				return SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return SpecificationPackage.TASK_DESCRIPTION__COMMENT;
			default:
				return -1;
			}
		}
		if (baseClass == SetFragmentElement.class) {
			switch (baseFeatureID) {
			case SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS:
				return SpecificationPackage.TASK_DESCRIPTION__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(')');
		return result.toString();
	}

} //TaskDescriptionImpl
