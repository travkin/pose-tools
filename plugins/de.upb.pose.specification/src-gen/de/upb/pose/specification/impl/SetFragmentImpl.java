/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.SetFragmentImpl#getContainingSets <em>Containing Sets</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.SetFragmentImpl#getContainedElements <em>Contained Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SetFragmentImpl extends PatternElementImpl implements SetFragment {
	/**
	 * The cached value of the '{@link #getContainingSets() <em>Containing Sets</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSets()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragment> containingSets;

	/**
	 * The cached value of the '{@link #getContainedElements() <em>Contained Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<SetFragmentElement> containedElements;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetFragmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.SET_FRAGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragment> getContainingSets() {
		if (containingSets == null) {
			containingSets = new EObjectWithInverseResolvingEList.ManyInverse<SetFragment>(SetFragment.class, this,
					SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS,
					SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS);
		}
		return containingSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SetFragmentElement> getContainedElements() {
		if (containedElements == null) {
			containedElements = new EObjectWithInverseResolvingEList.ManyInverse<SetFragmentElement>(
					SetFragmentElement.class, this, SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS,
					SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS);
		}
		return containedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainingSets()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainedElements())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			return ((InternalEList<?>) getContainingSets()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			return ((InternalEList<?>) getContainedElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			return getContainingSets();
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			return getContainedElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			getContainingSets().clear();
			getContainingSets().addAll((Collection<? extends SetFragment>) newValue);
			return;
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			getContainedElements().clear();
			getContainedElements().addAll((Collection<? extends SetFragmentElement>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			getContainingSets().clear();
			return;
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			getContainedElements().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
			return containingSets != null && !containingSets.isEmpty();
		case SpecificationPackage.SET_FRAGMENT__CONTAINED_ELEMENTS:
			return containedElements != null && !containedElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == SetFragmentElement.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS:
				return SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == SetFragmentElement.class) {
			switch (baseFeatureID) {
			case SpecificationPackage.SET_FRAGMENT_ELEMENT__CONTAINING_SETS:
				return SpecificationPackage.SET_FRAGMENT__CONTAINING_SETS;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //SetFragmentImpl
