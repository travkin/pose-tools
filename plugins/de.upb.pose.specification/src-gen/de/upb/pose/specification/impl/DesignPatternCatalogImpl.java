/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.types.PrimitiveType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Pattern Catalog</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getCategories <em>Categories</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getPatterns <em>Patterns</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getAccessTypes <em>Access Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getPrimitiveTypes <em>Primitive Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl#getNullVariable <em>Null Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DesignPatternCatalogImpl extends IdentifierImpl implements DesignPatternCatalog {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCategories() <em>Categories</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> categories;

	/**
	 * The cached value of the '{@link #getPatterns() <em>Patterns</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPatterns()
	 * @generated
	 * @ordered
	 */
	protected EList<DesignPattern> patterns;

	/**
	 * The cached value of the '{@link #getAccessTypes() <em>Access Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessType> accessTypes;

	/**
	 * The cached value of the '{@link #getPrimitiveTypes() <em>Primitive Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimitiveTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<PrimitiveType> primitiveTypes;

	/**
	 * The cached value of the '{@link #getNullVariable() <em>Null Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullVariable()
	 * @generated
	 * @ordered
	 */
	protected NullVariable nullVariable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternCatalogImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.DESIGN_PATTERN_CATALOG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_PATTERN_CATALOG__NAME,
					oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT,
					oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getCategories() {
		if (categories == null) {
			categories = new EObjectContainmentWithInverseEList<Category>(Category.class, this,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES, SpecificationPackage.CATEGORY__CATALOG);
		}
		return categories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DesignPattern> getPatterns() {
		if (patterns == null) {
			patterns = new EObjectContainmentWithInverseEList<DesignPattern>(DesignPattern.class, this,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS, SpecificationPackage.DESIGN_PATTERN__CATALOG);
		}
		return patterns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessType> getAccessTypes() {
		if (accessTypes == null) {
			accessTypes = new EObjectContainmentEList<AccessType>(AccessType.class, this,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES);
		}
		return accessTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrimitiveType> getPrimitiveTypes() {
		if (primitiveTypes == null) {
			primitiveTypes = new EObjectContainmentEList<PrimitiveType>(PrimitiveType.class, this,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES);
		}
		return primitiveTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullVariable getNullVariable() {
		return nullVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullVariable(NullVariable newNullVariable, NotificationChain msgs) {
		NullVariable oldNullVariable = nullVariable;
		nullVariable = newNullVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE, oldNullVariable, newNullVariable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullVariable(NullVariable newNullVariable) {
		if (newNullVariable != nullVariable) {
			NotificationChain msgs = null;
			if (nullVariable != null)
				msgs = ((InternalEObject) nullVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE, null, msgs);
			if (newNullVariable != null)
				msgs = ((InternalEObject) newNullVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE
						- SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE, null, msgs);
			msgs = basicSetNullVariable(newNullVariable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE, newNullVariable, newNullVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCategories()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPatterns()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			return ((InternalEList<?>) getCategories()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			return ((InternalEList<?>) getPatterns()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES:
			return ((InternalEList<?>) getAccessTypes()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES:
			return ((InternalEList<?>) getPrimitiveTypes()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE:
			return basicSetNullVariable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NAME:
			return getName();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT:
			return getComment();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			return getCategories();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			return getPatterns();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES:
			return getAccessTypes();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES:
			return getPrimitiveTypes();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE:
			return getNullVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NAME:
			setName((String) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT:
			setComment((String) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			getCategories().clear();
			getCategories().addAll((Collection<? extends Category>) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			getPatterns().clear();
			getPatterns().addAll((Collection<? extends DesignPattern>) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES:
			getAccessTypes().clear();
			getAccessTypes().addAll((Collection<? extends AccessType>) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES:
			getPrimitiveTypes().clear();
			getPrimitiveTypes().addAll((Collection<? extends PrimitiveType>) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE:
			setNullVariable((NullVariable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NAME:
			setName(NAME_EDEFAULT);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			getCategories().clear();
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			getPatterns().clear();
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES:
			getAccessTypes().clear();
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES:
			getPrimitiveTypes().clear();
			return;
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE:
			setNullVariable((NullVariable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES:
			return categories != null && !categories.isEmpty();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS:
			return patterns != null && !patterns.isEmpty();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__ACCESS_TYPES:
			return accessTypes != null && !accessTypes.isEmpty();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES:
			return primitiveTypes != null && !primitiveTypes.isEmpty();
		case SpecificationPackage.DESIGN_PATTERN_CATALOG__NULL_VARIABLE:
			return nullVariable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return SpecificationPackage.DESIGN_PATTERN_CATALOG__COMMENT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(')');
		return result.toString();
	}

} //DesignPatternCatalogImpl
