/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternImpl#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternImpl#getCategories <em>Categories</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.DesignPatternImpl#getSpecifications <em>Specifications</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DesignPatternImpl extends IdentifierImpl implements DesignPattern {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCategories() <em>Categories</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategories()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> categories;

	/**
	 * The cached value of the '{@link #getSpecifications() <em>Specifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecifications()
	 * @generated
	 * @ordered
	 */
	protected EList<PatternSpecification> specifications;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.DESIGN_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_PATTERN__NAME, oldName,
					name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_PATTERN__COMMENT,
					oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternCatalog getCatalog() {
		if (eContainerFeatureID() != SpecificationPackage.DESIGN_PATTERN__CATALOG)
			return null;
		return (DesignPatternCatalog) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatalog(DesignPatternCatalog newCatalog, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCatalog, SpecificationPackage.DESIGN_PATTERN__CATALOG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCatalog(DesignPatternCatalog newCatalog) {
		if (newCatalog != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.DESIGN_PATTERN__CATALOG && newCatalog != null)) {
			if (EcoreUtil.isAncestor(this, newCatalog))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCatalog != null)
				msgs = ((InternalEObject) newCatalog).eInverseAdd(this,
						SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS, DesignPatternCatalog.class, msgs);
			msgs = basicSetCatalog(newCatalog, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.DESIGN_PATTERN__CATALOG,
					newCatalog, newCatalog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getCategories() {
		if (categories == null) {
			categories = new EObjectWithInverseResolvingEList.ManyInverse<Category>(Category.class, this,
					SpecificationPackage.DESIGN_PATTERN__CATEGORIES, SpecificationPackage.CATEGORY__PATTERNS);
		}
		return categories;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PatternSpecification> getSpecifications() {
		if (specifications == null) {
			specifications = new EObjectContainmentWithInverseEList<PatternSpecification>(PatternSpecification.class,
					this, SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS,
					SpecificationPackage.PATTERN_SPECIFICATION__PATTERN);
		}
		return specifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCatalog((DesignPatternCatalog) otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCategories()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSpecifications()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			return basicSetCatalog(null, msgs);
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			return ((InternalEList<?>) getCategories()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			return ((InternalEList<?>) getSpecifications()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.DESIGN_PATTERN_CATALOG__PATTERNS,
					DesignPatternCatalog.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__NAME:
			return getName();
		case SpecificationPackage.DESIGN_PATTERN__COMMENT:
			return getComment();
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			return getCatalog();
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			return getCategories();
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			return getSpecifications();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__NAME:
			setName((String) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN__COMMENT:
			setComment((String) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			setCatalog((DesignPatternCatalog) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			getCategories().clear();
			getCategories().addAll((Collection<? extends Category>) newValue);
			return;
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			getSpecifications().clear();
			getSpecifications().addAll((Collection<? extends PatternSpecification>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__NAME:
			setName(NAME_EDEFAULT);
			return;
		case SpecificationPackage.DESIGN_PATTERN__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			setCatalog((DesignPatternCatalog) null);
			return;
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			getCategories().clear();
			return;
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			getSpecifications().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.DESIGN_PATTERN__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case SpecificationPackage.DESIGN_PATTERN__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case SpecificationPackage.DESIGN_PATTERN__CATALOG:
			return getCatalog() != null;
		case SpecificationPackage.DESIGN_PATTERN__CATEGORIES:
			return categories != null && !categories.isEmpty();
		case SpecificationPackage.DESIGN_PATTERN__SPECIFICATIONS:
			return specifications != null && !specifications.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.DESIGN_PATTERN__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return SpecificationPackage.DESIGN_PATTERN__COMMENT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(')');
		return result.toString();
	}

} //DesignPatternImpl
