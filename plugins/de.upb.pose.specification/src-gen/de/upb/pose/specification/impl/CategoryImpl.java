/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.core.impl.IdentifierImpl;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Category</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getCatalog <em>Catalog</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link de.upb.pose.specification.impl.CategoryImpl#getPatterns <em>Patterns</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CategoryImpl extends IdentifierImpl implements Category {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<Category> children;

	/**
	 * The cached value of the '{@link #getPatterns() <em>Patterns</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPatterns()
	 * @generated
	 * @ordered
	 */
	protected EList<DesignPattern> patterns;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CategoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SpecificationPackage.Literals.CATEGORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.CATEGORY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.CATEGORY__COMMENT, oldComment,
					comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternCatalog getCatalog() {
		if (eContainerFeatureID() != SpecificationPackage.CATEGORY__CATALOG)
			return null;
		return (DesignPatternCatalog) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCatalog(DesignPatternCatalog newCatalog, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCatalog, SpecificationPackage.CATEGORY__CATALOG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCatalog(DesignPatternCatalog newCatalog) {
		if (newCatalog != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.CATEGORY__CATALOG && newCatalog != null)) {
			if (EcoreUtil.isAncestor(this, newCatalog))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCatalog != null)
				msgs = ((InternalEObject) newCatalog).eInverseAdd(this,
						SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES, DesignPatternCatalog.class, msgs);
			msgs = basicSetCatalog(newCatalog, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.CATEGORY__CATALOG, newCatalog,
					newCatalog));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Category getParent() {
		if (eContainerFeatureID() != SpecificationPackage.CATEGORY__PARENT)
			return null;
		return (Category) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Category newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParent, SpecificationPackage.CATEGORY__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Category newParent) {
		if (newParent != eInternalContainer()
				|| (eContainerFeatureID() != SpecificationPackage.CATEGORY__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject) newParent).eInverseAdd(this, SpecificationPackage.CATEGORY__CHILDREN,
						Category.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SpecificationPackage.CATEGORY__PARENT, newParent,
					newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Category> getChildren() {
		if (children == null) {
			children = new EObjectContainmentWithInverseEList<Category>(Category.class, this,
					SpecificationPackage.CATEGORY__CHILDREN, SpecificationPackage.CATEGORY__PARENT);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DesignPattern> getPatterns() {
		if (patterns == null) {
			patterns = new EObjectWithInverseResolvingEList.ManyInverse<DesignPattern>(DesignPattern.class, this,
					SpecificationPackage.CATEGORY__PATTERNS, SpecificationPackage.DESIGN_PATTERN__CATEGORIES);
		}
		return patterns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__CATALOG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCatalog((DesignPatternCatalog) otherEnd, msgs);
		case SpecificationPackage.CATEGORY__PARENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParent((Category) otherEnd, msgs);
		case SpecificationPackage.CATEGORY__CHILDREN:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getChildren()).basicAdd(otherEnd, msgs);
		case SpecificationPackage.CATEGORY__PATTERNS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getPatterns()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__CATALOG:
			return basicSetCatalog(null, msgs);
		case SpecificationPackage.CATEGORY__PARENT:
			return basicSetParent(null, msgs);
		case SpecificationPackage.CATEGORY__CHILDREN:
			return ((InternalEList<?>) getChildren()).basicRemove(otherEnd, msgs);
		case SpecificationPackage.CATEGORY__PATTERNS:
			return ((InternalEList<?>) getPatterns()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SpecificationPackage.CATEGORY__CATALOG:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.DESIGN_PATTERN_CATALOG__CATEGORIES,
					DesignPatternCatalog.class, msgs);
		case SpecificationPackage.CATEGORY__PARENT:
			return eInternalContainer().eInverseRemove(this, SpecificationPackage.CATEGORY__CHILDREN, Category.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__NAME:
			return getName();
		case SpecificationPackage.CATEGORY__COMMENT:
			return getComment();
		case SpecificationPackage.CATEGORY__CATALOG:
			return getCatalog();
		case SpecificationPackage.CATEGORY__PARENT:
			return getParent();
		case SpecificationPackage.CATEGORY__CHILDREN:
			return getChildren();
		case SpecificationPackage.CATEGORY__PATTERNS:
			return getPatterns();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__NAME:
			setName((String) newValue);
			return;
		case SpecificationPackage.CATEGORY__COMMENT:
			setComment((String) newValue);
			return;
		case SpecificationPackage.CATEGORY__CATALOG:
			setCatalog((DesignPatternCatalog) newValue);
			return;
		case SpecificationPackage.CATEGORY__PARENT:
			setParent((Category) newValue);
			return;
		case SpecificationPackage.CATEGORY__CHILDREN:
			getChildren().clear();
			getChildren().addAll((Collection<? extends Category>) newValue);
			return;
		case SpecificationPackage.CATEGORY__PATTERNS:
			getPatterns().clear();
			getPatterns().addAll((Collection<? extends DesignPattern>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__NAME:
			setName(NAME_EDEFAULT);
			return;
		case SpecificationPackage.CATEGORY__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case SpecificationPackage.CATEGORY__CATALOG:
			setCatalog((DesignPatternCatalog) null);
			return;
		case SpecificationPackage.CATEGORY__PARENT:
			setParent((Category) null);
			return;
		case SpecificationPackage.CATEGORY__CHILDREN:
			getChildren().clear();
			return;
		case SpecificationPackage.CATEGORY__PATTERNS:
			getPatterns().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SpecificationPackage.CATEGORY__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case SpecificationPackage.CATEGORY__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case SpecificationPackage.CATEGORY__CATALOG:
			return getCatalog() != null;
		case SpecificationPackage.CATEGORY__PARENT:
			return getParent() != null;
		case SpecificationPackage.CATEGORY__CHILDREN:
			return children != null && !children.isEmpty();
		case SpecificationPackage.CATEGORY__PATTERNS:
			return patterns != null && !patterns.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case SpecificationPackage.CATEGORY__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return SpecificationPackage.CATEGORY__COMMENT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: "); //$NON-NLS-1$
		result.append(name);
		result.append(", comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(')');
		return result.toString();
	}

} //CategoryImpl
