/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Set Fragment Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Represents a pattern specification element that can be contained in a set fragment.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.SetFragmentElement#getContainingSets <em>Containing Sets</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getSetFragmentElement()
 * @generated
 */
public interface SetFragmentElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Containing Sets</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.SetFragment}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.SetFragment#getContainedElements <em>Contained Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The sets in which this design element is contained in.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Sets</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getSetFragmentElement_ContainingSets()
	 * @see de.upb.pose.specification.SetFragment#getContainedElements
	 * @generated
	 */
	EList<SetFragment> getContainingSets();

} // SetFragmentElement
