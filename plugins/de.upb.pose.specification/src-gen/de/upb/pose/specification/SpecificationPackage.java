/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.pose.core.CorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package and its sub-packages contain all elements that are used to specify object-oriented design patterns.
 * <!-- end-model-doc -->
 * @see de.upb.pose.specification.SpecificationFactory
 * @generated
 */
public interface SpecificationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "specification"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni-paderborn.de/pose/specification"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "specification"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SpecificationPackage eINSTANCE = de.upb.pose.specification.impl.SpecificationPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl <em>Design Pattern Catalog</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.DesignPatternCatalogImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignPatternCatalog()
	 * @generated
	 */
	int DESIGN_PATTERN_CATALOG = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__COMMENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Categories</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__CATEGORIES = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Patterns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__PATTERNS = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Access Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__ACCESS_TYPES = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Primitive Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Null Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG__NULL_VARIABLE = CorePackage.NAMED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Design Pattern Catalog</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Design Pattern Catalog</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_CATALOG_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.CategoryImpl <em>Category</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.CategoryImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getCategory()
	 * @generated
	 */
	int CATEGORY = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__COMMENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Catalog</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CATALOG = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__PARENT = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__CHILDREN = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Patterns</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY__PATTERNS = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Category</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATEGORY_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.DesignPatternImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignPattern()
	 * @generated
	 */
	int DESIGN_PATTERN = 2;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__COMMENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Catalog</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__CATALOG = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Categories</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__CATEGORIES = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Specifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN__SPECIFICATIONS = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.PatternSpecificationImpl <em>Pattern Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.PatternSpecificationImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getPatternSpecification()
	 * @generated
	 */
	int PATTERN_SPECIFICATION = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__COMMENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pattern</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__PATTERN = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Pattern Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__PATTERN_ELEMENTS = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Design Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__DESIGN_ELEMENTS = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Set Fragments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__SET_FRAGMENTS = CorePackage.NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Access Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__ACCESS_RULES = CorePackage.NAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION__ENVIRONMENT = CorePackage.NAMED_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Pattern Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Pattern Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SPECIFICATION_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.PatternElementImpl <em>Pattern Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.PatternElementImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getPatternElement()
	 * @generated
	 */
	int PATTERN_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__SPECIFICATION = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__CAN_BE_GENERATED = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT__ACCESSOR_RULES = CorePackage.NAMED_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Pattern Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Pattern Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ELEMENT_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.SetFragmentElement <em>Set Fragment Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.SetFragmentElement
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getSetFragmentElement()
	 * @generated
	 */
	int SET_FRAGMENT_ELEMENT = 9;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.TaskDescriptionImpl <em>Task Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.TaskDescriptionImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getTaskDescription()
	 * @generated
	 */
	int TASK_DESCRIPTION = 5;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__ID = PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__ANNOTATIONS = PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__NAME = PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__SPECIFICATION = PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__CAN_BE_GENERATED = PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__ACCESSOR_RULES = PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__COMMENT = PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__CONTAINING_SETS = PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Element</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION__ELEMENT = PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Task Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION_FEATURE_COUNT = PATTERN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION___GET_ANNOTATION__STRING = PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Task Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DESCRIPTION_OPERATION_COUNT = PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.DesignElementImpl <em>Design Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.DesignElementImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignElement()
	 * @generated
	 */
	int DESIGN_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__ID = PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__ANNOTATIONS = PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__NAME = PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__SPECIFICATION = PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__CAN_BE_GENERATED = PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__ACCESSOR_RULES = PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__CONTAINING_SETS = PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__DESIGN_MODEL = PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT__TASKS = PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Design Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT_FEATURE_COUNT = PATTERN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT___GET_ANNOTATION__STRING = PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Design Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_ELEMENT_OPERATION_COUNT = PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.SetFragmentImpl <em>Set Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.SetFragmentImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getSetFragment()
	 * @generated
	 */
	int SET_FRAGMENT = 7;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__ID = PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__ANNOTATIONS = PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__NAME = PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__SPECIFICATION = PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__CAN_BE_GENERATED = PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__ACCESSOR_RULES = PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__CONTAINING_SETS = PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT__CONTAINED_ELEMENTS = PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Set Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_FEATURE_COUNT = PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT___GET_ANNOTATION__STRING = PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Set Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_OPERATION_COUNT = PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.impl.DesignModelImpl <em>Design Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.impl.DesignModelImpl
	 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignModel()
	 * @generated
	 */
	int DESIGN_MODEL = 8;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__ID = PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__ANNOTATIONS = PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__NAME = PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__SPECIFICATION = PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__CAN_BE_GENERATED = PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__ACCESSOR_RULES = PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Design Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL__DESIGN_ELEMENTS = PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Design Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL_FEATURE_COUNT = PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL___GET_ANNOTATION__STRING = PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Design Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_MODEL_OPERATION_COUNT = PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_ELEMENT__CONTAINING_SETS = 0;

	/**
	 * The number of structural features of the '<em>Set Fragment Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Set Fragment Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_FRAGMENT_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.DesignPatternCatalog <em>Design Pattern Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern Catalog</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog
	 * @generated
	 */
	EClass getDesignPatternCatalog();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignPatternCatalog#getCategories <em>Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Categories</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog#getCategories()
	 * @see #getDesignPatternCatalog()
	 * @generated
	 */
	EReference getDesignPatternCatalog_Categories();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignPatternCatalog#getPatterns <em>Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Patterns</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog#getPatterns()
	 * @see #getDesignPatternCatalog()
	 * @generated
	 */
	EReference getDesignPatternCatalog_Patterns();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignPatternCatalog#getAccessTypes <em>Access Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Access Types</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog#getAccessTypes()
	 * @see #getDesignPatternCatalog()
	 * @generated
	 */
	EReference getDesignPatternCatalog_AccessTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignPatternCatalog#getPrimitiveTypes <em>Primitive Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Primitive Types</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog#getPrimitiveTypes()
	 * @see #getDesignPatternCatalog()
	 * @generated
	 */
	EReference getDesignPatternCatalog_PrimitiveTypes();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.pose.specification.DesignPatternCatalog#getNullVariable <em>Null Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Null Variable</em>'.
	 * @see de.upb.pose.specification.DesignPatternCatalog#getNullVariable()
	 * @see #getDesignPatternCatalog()
	 * @generated
	 */
	EReference getDesignPatternCatalog_NullVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.Category <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Category</em>'.
	 * @see de.upb.pose.specification.Category
	 * @generated
	 */
	EClass getCategory();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.Category#getCatalog <em>Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Catalog</em>'.
	 * @see de.upb.pose.specification.Category#getCatalog()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Catalog();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.Category#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see de.upb.pose.specification.Category#getParent()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.Category#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see de.upb.pose.specification.Category#getChildren()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Children();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.Category#getPatterns <em>Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Patterns</em>'.
	 * @see de.upb.pose.specification.Category#getPatterns()
	 * @see #getCategory()
	 * @generated
	 */
	EReference getCategory_Patterns();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.DesignPattern <em>Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern</em>'.
	 * @see de.upb.pose.specification.DesignPattern
	 * @generated
	 */
	EClass getDesignPattern();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.DesignPattern#getCatalog <em>Catalog</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Catalog</em>'.
	 * @see de.upb.pose.specification.DesignPattern#getCatalog()
	 * @see #getDesignPattern()
	 * @generated
	 */
	EReference getDesignPattern_Catalog();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.DesignPattern#getCategories <em>Categories</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Categories</em>'.
	 * @see de.upb.pose.specification.DesignPattern#getCategories()
	 * @see #getDesignPattern()
	 * @generated
	 */
	EReference getDesignPattern_Categories();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignPattern#getSpecifications <em>Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Specifications</em>'.
	 * @see de.upb.pose.specification.DesignPattern#getSpecifications()
	 * @see #getDesignPattern()
	 * @generated
	 */
	EReference getDesignPattern_Specifications();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.PatternSpecification <em>Pattern Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern Specification</em>'.
	 * @see de.upb.pose.specification.PatternSpecification
	 * @generated
	 */
	EClass getPatternSpecification();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.PatternSpecification#getPattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Pattern</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getPattern()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_Pattern();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.PatternSpecification#getPatternElements <em>Pattern Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pattern Elements</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getPatternElements()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_PatternElements();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.PatternSpecification#getDesignElements <em>Design Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Design Elements</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getDesignElements()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_DesignElements();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.PatternSpecification#getSetFragments <em>Set Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Set Fragments</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getSetFragments()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_SetFragments();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.PatternSpecification#getAccessRules <em>Access Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Access Rules</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getAccessRules()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_AccessRules();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.PatternSpecification#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment</em>'.
	 * @see de.upb.pose.specification.PatternSpecification#getEnvironment()
	 * @see #getPatternSpecification()
	 * @generated
	 */
	EReference getPatternSpecification_Environment();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.PatternElement <em>Pattern Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern Element</em>'.
	 * @see de.upb.pose.specification.PatternElement
	 * @generated
	 */
	EClass getPatternElement();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.PatternElement#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Specification</em>'.
	 * @see de.upb.pose.specification.PatternElement#getSpecification()
	 * @see #getPatternElement()
	 * @generated
	 */
	EReference getPatternElement_Specification();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.pose.specification.PatternElement#isCanBeGenerated <em>Can Be Generated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Can Be Generated</em>'.
	 * @see de.upb.pose.specification.PatternElement#isCanBeGenerated()
	 * @see #getPatternElement()
	 * @generated
	 */
	EAttribute getPatternElement_CanBeGenerated();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.PatternElement#getAccessorRules <em>Accessor Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Accessor Rules</em>'.
	 * @see de.upb.pose.specification.PatternElement#getAccessorRules()
	 * @see #getPatternElement()
	 * @generated
	 */
	EReference getPatternElement_AccessorRules();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.TaskDescription <em>Task Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Description</em>'.
	 * @see de.upb.pose.specification.TaskDescription
	 * @generated
	 */
	EClass getTaskDescription();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.TaskDescription#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Element</em>'.
	 * @see de.upb.pose.specification.TaskDescription#getElement()
	 * @see #getTaskDescription()
	 * @generated
	 */
	EReference getTaskDescription_Element();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.DesignElement <em>Design Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Element</em>'.
	 * @see de.upb.pose.specification.DesignElement
	 * @generated
	 */
	EClass getDesignElement();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.DesignElement#getDesignModel <em>Design Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Design Model</em>'.
	 * @see de.upb.pose.specification.DesignElement#getDesignModel()
	 * @see #getDesignElement()
	 * @generated
	 */
	EReference getDesignElement_DesignModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignElement#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see de.upb.pose.specification.DesignElement#getTasks()
	 * @see #getDesignElement()
	 * @generated
	 */
	EReference getDesignElement_Tasks();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.SetFragment <em>Set Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Fragment</em>'.
	 * @see de.upb.pose.specification.SetFragment
	 * @generated
	 */
	EClass getSetFragment();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.SetFragment#getContainedElements <em>Contained Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contained Elements</em>'.
	 * @see de.upb.pose.specification.SetFragment#getContainedElements()
	 * @see #getSetFragment()
	 * @generated
	 */
	EReference getSetFragment_ContainedElements();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.DesignModel <em>Design Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Model</em>'.
	 * @see de.upb.pose.specification.DesignModel
	 * @generated
	 */
	EClass getDesignModel();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.DesignModel#getDesignElements <em>Design Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Design Elements</em>'.
	 * @see de.upb.pose.specification.DesignModel#getDesignElements()
	 * @see #getDesignModel()
	 * @generated
	 */
	EReference getDesignModel_DesignElements();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.SetFragmentElement <em>Set Fragment Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Fragment Element</em>'.
	 * @see de.upb.pose.specification.SetFragmentElement
	 * @generated
	 */
	EClass getSetFragmentElement();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.SetFragmentElement#getContainingSets <em>Containing Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Containing Sets</em>'.
	 * @see de.upb.pose.specification.SetFragmentElement#getContainingSets()
	 * @see #getSetFragmentElement()
	 * @generated
	 */
	EReference getSetFragmentElement_ContainingSets();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SpecificationFactory getSpecificationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.DesignPatternCatalogImpl <em>Design Pattern Catalog</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.DesignPatternCatalogImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignPatternCatalog()
		 * @generated
		 */
		EClass DESIGN_PATTERN_CATALOG = eINSTANCE.getDesignPatternCatalog();

		/**
		 * The meta object literal for the '<em><b>Categories</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_CATALOG__CATEGORIES = eINSTANCE.getDesignPatternCatalog_Categories();

		/**
		 * The meta object literal for the '<em><b>Patterns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_CATALOG__PATTERNS = eINSTANCE.getDesignPatternCatalog_Patterns();

		/**
		 * The meta object literal for the '<em><b>Access Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_CATALOG__ACCESS_TYPES = eINSTANCE.getDesignPatternCatalog_AccessTypes();

		/**
		 * The meta object literal for the '<em><b>Primitive Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_CATALOG__PRIMITIVE_TYPES = eINSTANCE.getDesignPatternCatalog_PrimitiveTypes();

		/**
		 * The meta object literal for the '<em><b>Null Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_CATALOG__NULL_VARIABLE = eINSTANCE.getDesignPatternCatalog_NullVariable();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.CategoryImpl <em>Category</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.CategoryImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getCategory()
		 * @generated
		 */
		EClass CATEGORY = eINSTANCE.getCategory();

		/**
		 * The meta object literal for the '<em><b>Catalog</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__CATALOG = eINSTANCE.getCategory_Catalog();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__PARENT = eINSTANCE.getCategory_Parent();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__CHILDREN = eINSTANCE.getCategory_Children();

		/**
		 * The meta object literal for the '<em><b>Patterns</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATEGORY__PATTERNS = eINSTANCE.getCategory_Patterns();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.DesignPatternImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignPattern()
		 * @generated
		 */
		EClass DESIGN_PATTERN = eINSTANCE.getDesignPattern();

		/**
		 * The meta object literal for the '<em><b>Catalog</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN__CATALOG = eINSTANCE.getDesignPattern_Catalog();

		/**
		 * The meta object literal for the '<em><b>Categories</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN__CATEGORIES = eINSTANCE.getDesignPattern_Categories();

		/**
		 * The meta object literal for the '<em><b>Specifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN__SPECIFICATIONS = eINSTANCE.getDesignPattern_Specifications();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.PatternSpecificationImpl <em>Pattern Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.PatternSpecificationImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getPatternSpecification()
		 * @generated
		 */
		EClass PATTERN_SPECIFICATION = eINSTANCE.getPatternSpecification();

		/**
		 * The meta object literal for the '<em><b>Pattern</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__PATTERN = eINSTANCE.getPatternSpecification_Pattern();

		/**
		 * The meta object literal for the '<em><b>Pattern Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__PATTERN_ELEMENTS = eINSTANCE.getPatternSpecification_PatternElements();

		/**
		 * The meta object literal for the '<em><b>Design Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__DESIGN_ELEMENTS = eINSTANCE.getPatternSpecification_DesignElements();

		/**
		 * The meta object literal for the '<em><b>Set Fragments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__SET_FRAGMENTS = eINSTANCE.getPatternSpecification_SetFragments();

		/**
		 * The meta object literal for the '<em><b>Access Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__ACCESS_RULES = eINSTANCE.getPatternSpecification_AccessRules();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SPECIFICATION__ENVIRONMENT = eINSTANCE.getPatternSpecification_Environment();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.PatternElementImpl <em>Pattern Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.PatternElementImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getPatternElement()
		 * @generated
		 */
		EClass PATTERN_ELEMENT = eINSTANCE.getPatternElement();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_ELEMENT__SPECIFICATION = eINSTANCE.getPatternElement_Specification();

		/**
		 * The meta object literal for the '<em><b>Can Be Generated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATTERN_ELEMENT__CAN_BE_GENERATED = eINSTANCE.getPatternElement_CanBeGenerated();

		/**
		 * The meta object literal for the '<em><b>Accessor Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_ELEMENT__ACCESSOR_RULES = eINSTANCE.getPatternElement_AccessorRules();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.TaskDescriptionImpl <em>Task Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.TaskDescriptionImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getTaskDescription()
		 * @generated
		 */
		EClass TASK_DESCRIPTION = eINSTANCE.getTaskDescription();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DESCRIPTION__ELEMENT = eINSTANCE.getTaskDescription_Element();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.DesignElementImpl <em>Design Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.DesignElementImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignElement()
		 * @generated
		 */
		EClass DESIGN_ELEMENT = eINSTANCE.getDesignElement();

		/**
		 * The meta object literal for the '<em><b>Design Model</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_ELEMENT__DESIGN_MODEL = eINSTANCE.getDesignElement_DesignModel();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_ELEMENT__TASKS = eINSTANCE.getDesignElement_Tasks();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.SetFragmentImpl <em>Set Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.SetFragmentImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getSetFragment()
		 * @generated
		 */
		EClass SET_FRAGMENT = eINSTANCE.getSetFragment();

		/**
		 * The meta object literal for the '<em><b>Contained Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT__CONTAINED_ELEMENTS = eINSTANCE.getSetFragment_ContainedElements();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.impl.DesignModelImpl <em>Design Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.impl.DesignModelImpl
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getDesignModel()
		 * @generated
		 */
		EClass DESIGN_MODEL = eINSTANCE.getDesignModel();

		/**
		 * The meta object literal for the '<em><b>Design Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_MODEL__DESIGN_ELEMENTS = eINSTANCE.getDesignModel_DesignElements();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.SetFragmentElement <em>Set Fragment Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.SetFragmentElement
		 * @see de.upb.pose.specification.impl.SpecificationPackageImpl#getSetFragmentElement()
		 * @generated
		 */
		EClass SET_FRAGMENT_ELEMENT = eINSTANCE.getSetFragmentElement();

		/**
		 * The meta object literal for the '<em><b>Containing Sets</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_FRAGMENT_ELEMENT__CONTAINING_SETS = eINSTANCE.getSetFragmentElement_ContainingSets();

	}

} //SpecificationPackage
