/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Named;
import de.upb.pose.specification.access.AccessRule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the base class for all elements that are used in a pattern specification.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.PatternElement#getSpecification <em>Specification</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternElement#isCanBeGenerated <em>Can Be Generated</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternElement#getAccessorRules <em>Accessor Rules</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getPatternElement()
 * @generated
 */
public interface PatternElement extends Named {
	/**
	 * Returns the value of the '<em><b>Specification</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.PatternSpecification#getPatternElements <em>Pattern Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The specification to which this pattern element belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Specification</em>' container reference.
	 * @see #setSpecification(PatternSpecification)
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternElement_Specification()
	 * @see de.upb.pose.specification.PatternSpecification#getPatternElements
	 * @generated
	 */
	PatternSpecification getSpecification();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.PatternElement#getSpecification <em>Specification</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' container reference.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(PatternSpecification value);

	/**
	 * Returns the value of the '<em><b>Can Be Generated</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Whether the element can be generated.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Can Be Generated</em>' attribute.
	 * @see #setCanBeGenerated(boolean)
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternElement_CanBeGenerated()
	 * @generated
	 */
	boolean isCanBeGenerated();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.PatternElement#isCanBeGenerated <em>Can Be Generated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Can Be Generated</em>' attribute.
	 * @see #isCanBeGenerated()
	 * @generated
	 */
	void setCanBeGenerated(boolean value);

	/**
	 * Returns the value of the '<em><b>Accessor Rules</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.access.AccessRule}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.access.AccessRule#getAccessor <em>Accessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accessor Rules</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessor Rules</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternElement_AccessorRules()
	 * @see de.upb.pose.specification.access.AccessRule#getAccessor
	 * @generated
	 */
	EList<AccessRule> getAccessorRules();

} // PatternElement
