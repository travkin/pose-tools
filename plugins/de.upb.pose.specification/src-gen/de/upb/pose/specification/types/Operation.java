/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.Action;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an operation of a type.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.Operation#getParentType <em>Parent Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getActions <em>Actions</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Operation#getAbstraction <em>Abstraction</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.types.TypesPackage#getOperation()
 * @generated
 */
public interface Operation extends DesignElement, Accessable {
	/**
	 * Returns the value of the '<em><b>Parent Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Type#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The type to which this operation belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent Type</em>' container reference.
	 * @see #setParentType(Type)
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_ParentType()
	 * @see de.upb.pose.specification.types.Type#getOperations
	 * @generated
	 */
	Type getParentType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Operation#getParentType <em>Parent Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Type</em>' container reference.
	 * @see #getParentType()
	 * @generated
	 */
	void setParentType(Type value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Parameter}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Parameter#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parameters of this operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Parameters()
	 * @see de.upb.pose.specification.types.Parameter#getOperation
	 * @generated
	 */
	EList<Parameter> getParameters();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The return type of this operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Type()
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Operation#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Overrides</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Operation#getOverriding <em>Overriding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The operation which this operation overrides.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Overrides</em>' reference.
	 * @see #setOverrides(Operation)
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Overrides()
	 * @see de.upb.pose.specification.types.Operation#getOverriding
	 * @generated
	 */
	Operation getOverrides();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Operation#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overrides</em>' reference.
	 * @see #getOverrides()
	 * @generated
	 */
	void setOverrides(Operation value);

	/**
	 * Returns the value of the '<em><b>Overriding</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Operation}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Operation#getOverrides <em>Overrides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The operation which overrides this method.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Overriding</em>' reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Overriding()
	 * @see de.upb.pose.specification.types.Operation#getOverrides
	 * @generated
	 */
	EList<Operation> getOverriding();

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.actions.Action}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.Action#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The actions which belong to this operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Actions()
	 * @see de.upb.pose.specification.actions.Action#getOperation
	 * @generated
	 */
	EList<Action> getActions();

	/**
	 * Returns the value of the '<em><b>Abstraction</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.pose.specification.types.AbstractionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstraction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstraction</em>' attribute.
	 * @see de.upb.pose.specification.types.AbstractionType
	 * @see #setAbstraction(AbstractionType)
	 * @see de.upb.pose.specification.types.TypesPackage#getOperation_Abstraction()
	 * @generated
	 */
	AbstractionType getAbstraction();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Operation#getAbstraction <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstraction</em>' attribute.
	 * @see de.upb.pose.specification.types.AbstractionType
	 * @see #getAbstraction()
	 * @generated
	 */
	void setAbstraction(AbstractionType value);

} // Operation
