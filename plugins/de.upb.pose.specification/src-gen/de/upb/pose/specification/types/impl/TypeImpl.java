/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.impl.DesignElementImpl;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.Feature;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getAccessingRules <em>Accessing Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getReferences <em>References</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getOperations <em>Operations</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getSelfVariable <em>Self Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getAbstraction <em>Abstraction</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.TypeImpl#getFeatures <em>Features</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TypeImpl extends DesignElementImpl implements Type {
	/**
	 * The cached value of the '{@link #getAccessingRules() <em>Accessing Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessingRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessingRules;

	/**
	 * The cached setting delegate for the '{@link #getAttributes() <em>Attributes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate ATTRIBUTES__ESETTING_DELEGATE = ((EStructuralFeature.Internal) TypesPackage.Literals.TYPE__ATTRIBUTES)
			.getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getReferences() <em>References</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferences()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate REFERENCES__ESETTING_DELEGATE = ((EStructuralFeature.Internal) TypesPackage.Literals.TYPE__REFERENCES)
			.getSettingDelegate();

	/**
	 * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> operations;

	/**
	 * The cached value of the '{@link #getSubTypes() <em>Sub Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> subTypes;

	/**
	 * The cached value of the '{@link #getSuperType() <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperType()
	 * @generated
	 * @ordered
	 */
	protected Type superType;

	/**
	 * The cached value of the '{@link #getSelfVariable() <em>Self Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfVariable()
	 * @generated
	 * @ordered
	 */
	protected SelfVariable selfVariable;

	/**
	 * The default value of the '{@link #getAbstraction() <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstraction()
	 * @generated
	 * @ordered
	 */
	protected static final AbstractionType ABSTRACTION_EDEFAULT = AbstractionType.ANY;

	/**
	 * The cached value of the '{@link #getAbstraction() <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstraction()
	 * @generated
	 * @ordered
	 */
	protected AbstractionType abstraction = ABSTRACTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature> features;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessingRules() {
		if (accessingRules == null) {
			accessingRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					TypesPackage.TYPE__ACCESSING_RULES, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT);
		}
		return accessingRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Attribute> getAttributes() {
		return (EList<Attribute>) ATTRIBUTES__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Reference> getReferences() {
		return (EList<Reference>) REFERENCES__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOperations() {
		if (operations == null) {
			operations = new EObjectContainmentWithInverseEList<Operation>(Operation.class, this,
					TypesPackage.TYPE__OPERATIONS, TypesPackage.OPERATION__PARENT_TYPE);
		}
		return operations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getSubTypes() {
		if (subTypes == null) {
			subTypes = new EObjectWithInverseResolvingEList<Type>(Type.class, this, TypesPackage.TYPE__SUB_TYPES,
					TypesPackage.TYPE__SUPER_TYPE);
		}
		return subTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getSuperType() {
		if (superType != null && superType.eIsProxy()) {
			InternalEObject oldSuperType = (InternalEObject) superType;
			superType = (Type) eResolveProxy(oldSuperType);
			if (superType != oldSuperType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.TYPE__SUPER_TYPE,
							oldSuperType, superType));
			}
		}
		return superType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetSuperType() {
		return superType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuperType(Type newSuperType, NotificationChain msgs) {
		Type oldSuperType = superType;
		superType = newSuperType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					TypesPackage.TYPE__SUPER_TYPE, oldSuperType, newSuperType);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperType(Type newSuperType) {
		if (newSuperType != superType) {
			NotificationChain msgs = null;
			if (superType != null)
				msgs = ((InternalEObject) superType).eInverseRemove(this, TypesPackage.TYPE__SUB_TYPES, Type.class,
						msgs);
			if (newSuperType != null)
				msgs = ((InternalEObject) newSuperType).eInverseAdd(this, TypesPackage.TYPE__SUB_TYPES, Type.class,
						msgs);
			msgs = basicSetSuperType(newSuperType, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__SUPER_TYPE, newSuperType,
					newSuperType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelfVariable getSelfVariable() {
		return selfVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelfVariable(SelfVariable newSelfVariable, NotificationChain msgs) {
		SelfVariable oldSelfVariable = selfVariable;
		selfVariable = newSelfVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					TypesPackage.TYPE__SELF_VARIABLE, oldSelfVariable, newSelfVariable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelfVariable(SelfVariable newSelfVariable) {
		if (newSelfVariable != selfVariable) {
			NotificationChain msgs = null;
			if (selfVariable != null)
				msgs = ((InternalEObject) selfVariable).eInverseRemove(this, ActionsPackage.SELF_VARIABLE__TYPE,
						SelfVariable.class, msgs);
			if (newSelfVariable != null)
				msgs = ((InternalEObject) newSelfVariable).eInverseAdd(this, ActionsPackage.SELF_VARIABLE__TYPE,
						SelfVariable.class, msgs);
			msgs = basicSetSelfVariable(newSelfVariable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__SELF_VARIABLE, newSelfVariable,
					newSelfVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractionType getAbstraction() {
		return abstraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstraction(AbstractionType newAbstraction) {
		AbstractionType oldAbstraction = abstraction;
		abstraction = newAbstraction == null ? ABSTRACTION_EDEFAULT : newAbstraction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.TYPE__ABSTRACTION, oldAbstraction,
					abstraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentWithInverseEList<Feature>(Feature.class, this,
					TypesPackage.TYPE__FEATURES, TypesPackage.FEATURE__PARENT_TYPE);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessingRules()).basicAdd(otherEnd, msgs);
		case TypesPackage.TYPE__OPERATIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOperations()).basicAdd(otherEnd, msgs);
		case TypesPackage.TYPE__SUB_TYPES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getSubTypes()).basicAdd(otherEnd, msgs);
		case TypesPackage.TYPE__SUPER_TYPE:
			if (superType != null)
				msgs = ((InternalEObject) superType).eInverseRemove(this, TypesPackage.TYPE__SUB_TYPES, Type.class,
						msgs);
			return basicSetSuperType((Type) otherEnd, msgs);
		case TypesPackage.TYPE__SELF_VARIABLE:
			if (selfVariable != null)
				msgs = ((InternalEObject) selfVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE
						- TypesPackage.TYPE__SELF_VARIABLE, null, msgs);
			return basicSetSelfVariable((SelfVariable) otherEnd, msgs);
		case TypesPackage.TYPE__FEATURES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getFeatures()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			return ((InternalEList<?>) getAccessingRules()).basicRemove(otherEnd, msgs);
		case TypesPackage.TYPE__OPERATIONS:
			return ((InternalEList<?>) getOperations()).basicRemove(otherEnd, msgs);
		case TypesPackage.TYPE__SUB_TYPES:
			return ((InternalEList<?>) getSubTypes()).basicRemove(otherEnd, msgs);
		case TypesPackage.TYPE__SUPER_TYPE:
			return basicSetSuperType(null, msgs);
		case TypesPackage.TYPE__SELF_VARIABLE:
			return basicSetSelfVariable(null, msgs);
		case TypesPackage.TYPE__FEATURES:
			return ((InternalEList<?>) getFeatures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			return getAccessingRules();
		case TypesPackage.TYPE__ATTRIBUTES:
			return getAttributes();
		case TypesPackage.TYPE__REFERENCES:
			return getReferences();
		case TypesPackage.TYPE__OPERATIONS:
			return getOperations();
		case TypesPackage.TYPE__SUB_TYPES:
			return getSubTypes();
		case TypesPackage.TYPE__SUPER_TYPE:
			if (resolve)
				return getSuperType();
			return basicGetSuperType();
		case TypesPackage.TYPE__SELF_VARIABLE:
			return getSelfVariable();
		case TypesPackage.TYPE__ABSTRACTION:
			return getAbstraction();
		case TypesPackage.TYPE__FEATURES:
			return getFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			getAccessingRules().clear();
			getAccessingRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		case TypesPackage.TYPE__OPERATIONS:
			getOperations().clear();
			getOperations().addAll((Collection<? extends Operation>) newValue);
			return;
		case TypesPackage.TYPE__SUB_TYPES:
			getSubTypes().clear();
			getSubTypes().addAll((Collection<? extends Type>) newValue);
			return;
		case TypesPackage.TYPE__SUPER_TYPE:
			setSuperType((Type) newValue);
			return;
		case TypesPackage.TYPE__SELF_VARIABLE:
			setSelfVariable((SelfVariable) newValue);
			return;
		case TypesPackage.TYPE__ABSTRACTION:
			setAbstraction((AbstractionType) newValue);
			return;
		case TypesPackage.TYPE__FEATURES:
			getFeatures().clear();
			getFeatures().addAll((Collection<? extends Feature>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			getAccessingRules().clear();
			return;
		case TypesPackage.TYPE__OPERATIONS:
			getOperations().clear();
			return;
		case TypesPackage.TYPE__SUB_TYPES:
			getSubTypes().clear();
			return;
		case TypesPackage.TYPE__SUPER_TYPE:
			setSuperType((Type) null);
			return;
		case TypesPackage.TYPE__SELF_VARIABLE:
			setSelfVariable((SelfVariable) null);
			return;
		case TypesPackage.TYPE__ABSTRACTION:
			setAbstraction(ABSTRACTION_EDEFAULT);
			return;
		case TypesPackage.TYPE__FEATURES:
			getFeatures().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TypesPackage.TYPE__ACCESSING_RULES:
			return accessingRules != null && !accessingRules.isEmpty();
		case TypesPackage.TYPE__ATTRIBUTES:
			return ATTRIBUTES__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		case TypesPackage.TYPE__REFERENCES:
			return REFERENCES__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		case TypesPackage.TYPE__OPERATIONS:
			return operations != null && !operations.isEmpty();
		case TypesPackage.TYPE__SUB_TYPES:
			return subTypes != null && !subTypes.isEmpty();
		case TypesPackage.TYPE__SUPER_TYPE:
			return superType != null;
		case TypesPackage.TYPE__SELF_VARIABLE:
			return selfVariable != null;
		case TypesPackage.TYPE__ABSTRACTION:
			return abstraction != ABSTRACTION_EDEFAULT;
		case TypesPackage.TYPE__FEATURES:
			return features != null && !features.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (derivedFeatureID) {
			case TypesPackage.TYPE__ACCESSING_RULES:
				return AccessPackage.ACCESSABLE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (baseFeatureID) {
			case AccessPackage.ACCESSABLE__ACCESSING_RULES:
				return TypesPackage.TYPE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (abstraction: "); //$NON-NLS-1$
		result.append(abstraction);
		result.append(')');
		return result.toString();
	}

} //TypeImpl
