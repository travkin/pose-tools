/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.SelfVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents a type, e.g. a class.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.Type#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getReferences <em>References</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getOperations <em>Operations</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getSubTypes <em>Sub Types</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getSelfVariable <em>Self Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getAbstraction <em>Abstraction</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Type#getFeatures <em>Features</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.types.TypesPackage#getType()
 * @generated
 */
public interface Type extends DesignElement, Accessable {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Attribute}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The attributes of this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Attributes</em>' reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getType_Attributes()
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>References</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Reference}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The references of this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>References</em>' reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getType_References()
	 * @generated
	 */
	EList<Reference> getReferences();

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Operation}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Operation#getParentType <em>Parent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The operations of this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getType_Operations()
	 * @see de.upb.pose.specification.types.Operation#getParentType
	 * @generated
	 */
	EList<Operation> getOperations();

	/**
	 * Returns the value of the '<em><b>Sub Types</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Type}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Type#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The sub-types of this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Sub Types</em>' reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getType_SubTypes()
	 * @see de.upb.pose.specification.types.Type#getSuperType
	 * @generated
	 */
	EList<Type> getSubTypes();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Type#getSubTypes <em>Sub Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The super-type of this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Super Type</em>' reference.
	 * @see #setSuperType(Type)
	 * @see de.upb.pose.specification.types.TypesPackage#getType_SuperType()
	 * @see de.upb.pose.specification.types.Type#getSubTypes
	 * @generated
	 */
	Type getSuperType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Type#getSuperType <em>Super Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' reference.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(Type value);

	/**
	 * Returns the value of the '<em><b>Self Variable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.SelfVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The variable that represent this type.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Self Variable</em>' containment reference.
	 * @see #setSelfVariable(SelfVariable)
	 * @see de.upb.pose.specification.types.TypesPackage#getType_SelfVariable()
	 * @see de.upb.pose.specification.actions.SelfVariable#getType
	 * @generated
	 */
	SelfVariable getSelfVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Type#getSelfVariable <em>Self Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Self Variable</em>' containment reference.
	 * @see #getSelfVariable()
	 * @generated
	 */
	void setSelfVariable(SelfVariable value);

	/**
	 * Returns the value of the '<em><b>Abstraction</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.pose.specification.types.AbstractionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstraction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstraction</em>' attribute.
	 * @see de.upb.pose.specification.types.AbstractionType
	 * @see #setAbstraction(AbstractionType)
	 * @see de.upb.pose.specification.types.TypesPackage#getType_Abstraction()
	 * @generated
	 */
	AbstractionType getAbstraction();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Type#getAbstraction <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstraction</em>' attribute.
	 * @see de.upb.pose.specification.types.AbstractionType
	 * @see #getAbstraction()
	 * @generated
	 */
	void setAbstraction(AbstractionType value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Feature}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Feature#getParentType <em>Parent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see de.upb.pose.specification.types.TypesPackage#getType_Features()
	 * @see de.upb.pose.specification.types.Feature#getParentType
	 * @generated
	 */
	EList<Feature> getFeatures();

} // Type
