/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.impl.DesignElementImpl;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getAccessingRules <em>Accessing Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getParentType <em>Parent Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.OperationImpl#getAbstraction <em>Abstraction</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OperationImpl extends DesignElementImpl implements Operation {
	/**
	 * The cached value of the '{@link #getAccessingRules() <em>Accessing Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessingRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessingRules;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<Parameter> parameters;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * The cached value of the '{@link #getOverrides() <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverrides()
	 * @generated
	 * @ordered
	 */
	protected Operation overrides;

	/**
	 * The cached value of the '{@link #getOverriding() <em>Overriding</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriding()
	 * @generated
	 * @ordered
	 */
	protected EList<Operation> overriding;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> actions;

	/**
	 * The default value of the '{@link #getAbstraction() <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstraction()
	 * @generated
	 * @ordered
	 */
	protected static final AbstractionType ABSTRACTION_EDEFAULT = AbstractionType.ANY;

	/**
	 * The cached value of the '{@link #getAbstraction() <em>Abstraction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstraction()
	 * @generated
	 * @ordered
	 */
	protected AbstractionType abstraction = ABSTRACTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessingRules() {
		if (accessingRules == null) {
			accessingRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					TypesPackage.OPERATION__ACCESSING_RULES, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT);
		}
		return accessingRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getParentType() {
		if (eContainerFeatureID() != TypesPackage.OPERATION__PARENT_TYPE)
			return null;
		return (Type) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentType(Type newParentType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentType, TypesPackage.OPERATION__PARENT_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentType(Type newParentType) {
		if (newParentType != eInternalContainer()
				|| (eContainerFeatureID() != TypesPackage.OPERATION__PARENT_TYPE && newParentType != null)) {
			if (EcoreUtil.isAncestor(this, newParentType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentType != null)
				msgs = ((InternalEObject) newParentType).eInverseAdd(this, TypesPackage.TYPE__OPERATIONS, Type.class,
						msgs);
			msgs = basicSetParentType(newParentType, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.OPERATION__PARENT_TYPE, newParentType,
					newParentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Parameter> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentWithInverseEList<Parameter>(Parameter.class, this,
					TypesPackage.OPERATION__PARAMETERS, TypesPackage.PARAMETER__OPERATION);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject) type;
			type = (Type) eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.OPERATION__TYPE, oldType,
							type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.OPERATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getOverrides() {
		if (overrides != null && overrides.eIsProxy()) {
			InternalEObject oldOverrides = (InternalEObject) overrides;
			overrides = (Operation) eResolveProxy(oldOverrides);
			if (overrides != oldOverrides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.OPERATION__OVERRIDES,
							oldOverrides, overrides));
			}
		}
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetOverrides() {
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverrides(Operation newOverrides, NotificationChain msgs) {
		Operation oldOverrides = overrides;
		overrides = newOverrides;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					TypesPackage.OPERATION__OVERRIDES, oldOverrides, newOverrides);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverrides(Operation newOverrides) {
		if (newOverrides != overrides) {
			NotificationChain msgs = null;
			if (overrides != null)
				msgs = ((InternalEObject) overrides).eInverseRemove(this, TypesPackage.OPERATION__OVERRIDING,
						Operation.class, msgs);
			if (newOverrides != null)
				msgs = ((InternalEObject) newOverrides).eInverseAdd(this, TypesPackage.OPERATION__OVERRIDING,
						Operation.class, msgs);
			msgs = basicSetOverrides(newOverrides, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.OPERATION__OVERRIDES, newOverrides,
					newOverrides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation> getOverriding() {
		if (overriding == null) {
			overriding = new EObjectWithInverseResolvingEList<Operation>(Operation.class, this,
					TypesPackage.OPERATION__OVERRIDING, TypesPackage.OPERATION__OVERRIDES);
		}
		return overriding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentWithInverseEList<Action>(Action.class, this,
					TypesPackage.OPERATION__ACTIONS, ActionsPackage.ACTION__OPERATION);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractionType getAbstraction() {
		return abstraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstraction(AbstractionType newAbstraction) {
		AbstractionType oldAbstraction = abstraction;
		abstraction = newAbstraction == null ? ABSTRACTION_EDEFAULT : newAbstraction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.OPERATION__ABSTRACTION, oldAbstraction,
					abstraction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessingRules()).basicAdd(otherEnd, msgs);
		case TypesPackage.OPERATION__PARENT_TYPE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentType((Type) otherEnd, msgs);
		case TypesPackage.OPERATION__PARAMETERS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getParameters()).basicAdd(otherEnd, msgs);
		case TypesPackage.OPERATION__OVERRIDES:
			if (overrides != null)
				msgs = ((InternalEObject) overrides).eInverseRemove(this, TypesPackage.OPERATION__OVERRIDING,
						Operation.class, msgs);
			return basicSetOverrides((Operation) otherEnd, msgs);
		case TypesPackage.OPERATION__OVERRIDING:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getOverriding()).basicAdd(otherEnd, msgs);
		case TypesPackage.OPERATION__ACTIONS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getActions()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			return ((InternalEList<?>) getAccessingRules()).basicRemove(otherEnd, msgs);
		case TypesPackage.OPERATION__PARENT_TYPE:
			return basicSetParentType(null, msgs);
		case TypesPackage.OPERATION__PARAMETERS:
			return ((InternalEList<?>) getParameters()).basicRemove(otherEnd, msgs);
		case TypesPackage.OPERATION__OVERRIDES:
			return basicSetOverrides(null, msgs);
		case TypesPackage.OPERATION__OVERRIDING:
			return ((InternalEList<?>) getOverriding()).basicRemove(otherEnd, msgs);
		case TypesPackage.OPERATION__ACTIONS:
			return ((InternalEList<?>) getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case TypesPackage.OPERATION__PARENT_TYPE:
			return eInternalContainer().eInverseRemove(this, TypesPackage.TYPE__OPERATIONS, Type.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			return getAccessingRules();
		case TypesPackage.OPERATION__PARENT_TYPE:
			return getParentType();
		case TypesPackage.OPERATION__PARAMETERS:
			return getParameters();
		case TypesPackage.OPERATION__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case TypesPackage.OPERATION__OVERRIDES:
			if (resolve)
				return getOverrides();
			return basicGetOverrides();
		case TypesPackage.OPERATION__OVERRIDING:
			return getOverriding();
		case TypesPackage.OPERATION__ACTIONS:
			return getActions();
		case TypesPackage.OPERATION__ABSTRACTION:
			return getAbstraction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			getAccessingRules().clear();
			getAccessingRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		case TypesPackage.OPERATION__PARENT_TYPE:
			setParentType((Type) newValue);
			return;
		case TypesPackage.OPERATION__PARAMETERS:
			getParameters().clear();
			getParameters().addAll((Collection<? extends Parameter>) newValue);
			return;
		case TypesPackage.OPERATION__TYPE:
			setType((Type) newValue);
			return;
		case TypesPackage.OPERATION__OVERRIDES:
			setOverrides((Operation) newValue);
			return;
		case TypesPackage.OPERATION__OVERRIDING:
			getOverriding().clear();
			getOverriding().addAll((Collection<? extends Operation>) newValue);
			return;
		case TypesPackage.OPERATION__ACTIONS:
			getActions().clear();
			getActions().addAll((Collection<? extends Action>) newValue);
			return;
		case TypesPackage.OPERATION__ABSTRACTION:
			setAbstraction((AbstractionType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			getAccessingRules().clear();
			return;
		case TypesPackage.OPERATION__PARENT_TYPE:
			setParentType((Type) null);
			return;
		case TypesPackage.OPERATION__PARAMETERS:
			getParameters().clear();
			return;
		case TypesPackage.OPERATION__TYPE:
			setType((Type) null);
			return;
		case TypesPackage.OPERATION__OVERRIDES:
			setOverrides((Operation) null);
			return;
		case TypesPackage.OPERATION__OVERRIDING:
			getOverriding().clear();
			return;
		case TypesPackage.OPERATION__ACTIONS:
			getActions().clear();
			return;
		case TypesPackage.OPERATION__ABSTRACTION:
			setAbstraction(ABSTRACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TypesPackage.OPERATION__ACCESSING_RULES:
			return accessingRules != null && !accessingRules.isEmpty();
		case TypesPackage.OPERATION__PARENT_TYPE:
			return getParentType() != null;
		case TypesPackage.OPERATION__PARAMETERS:
			return parameters != null && !parameters.isEmpty();
		case TypesPackage.OPERATION__TYPE:
			return type != null;
		case TypesPackage.OPERATION__OVERRIDES:
			return overrides != null;
		case TypesPackage.OPERATION__OVERRIDING:
			return overriding != null && !overriding.isEmpty();
		case TypesPackage.OPERATION__ACTIONS:
			return actions != null && !actions.isEmpty();
		case TypesPackage.OPERATION__ABSTRACTION:
			return abstraction != ABSTRACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (derivedFeatureID) {
			case TypesPackage.OPERATION__ACCESSING_RULES:
				return AccessPackage.ACCESSABLE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (baseFeatureID) {
			case AccessPackage.ACCESSABLE__ACCESSING_RULES:
				return TypesPackage.OPERATION__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (abstraction: "); //$NON-NLS-1$
		result.append(abstraction);
		result.append(')');
		return result.toString();
	}

} //OperationImpl
