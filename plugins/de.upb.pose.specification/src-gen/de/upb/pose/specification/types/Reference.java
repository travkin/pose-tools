/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents a type's reference to a type.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.Reference#getSourceReference <em>Source Reference</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Reference#getTargetReference <em>Target Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.types.TypesPackage#getReference()
 * @generated
 */
public interface Reference extends Feature {
	/**
	 * Returns the value of the '<em><b>Source Reference</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Reference#getTargetReference <em>Target Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * No documentation provided.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Source Reference</em>' reference.
	 * @see #setSourceReference(Reference)
	 * @see de.upb.pose.specification.types.TypesPackage#getReference_SourceReference()
	 * @see de.upb.pose.specification.types.Reference#getTargetReference
	 * @generated
	 */
	Reference getSourceReference();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Reference#getSourceReference <em>Source Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Reference</em>' reference.
	 * @see #getSourceReference()
	 * @generated
	 */
	void setSourceReference(Reference value);

	/**
	 * Returns the value of the '<em><b>Target Reference</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Reference#getSourceReference <em>Source Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * No documentation provided.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Target Reference</em>' reference.
	 * @see #setTargetReference(Reference)
	 * @see de.upb.pose.specification.types.TypesPackage#getReference_TargetReference()
	 * @see de.upb.pose.specification.types.Reference#getSourceReference
	 * @generated
	 */
	Reference getTargetReference();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Reference#getTargetReference <em>Target Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Reference</em>' reference.
	 * @see #getTargetReference()
	 * @generated
	 */
	void setTargetReference(Reference value);

} // Reference
