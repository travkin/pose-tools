/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.impl.DesignElementImpl;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.Feature;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.impl.FeatureImpl#getAccessingRules <em>Accessing Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.FeatureImpl#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.FeatureImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.FeatureImpl#getParentType <em>Parent Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class FeatureImpl extends DesignElementImpl implements Feature {
	/**
	 * The cached value of the '{@link #getAccessingRules() <em>Accessing Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessingRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessingRules;

	/**
	 * The default value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected static final CardinalityType CARDINALITY_EDEFAULT = CardinalityType.ANY;

	/**
	 * The cached value of the '{@link #getCardinality() <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardinality()
	 * @generated
	 * @ordered
	 */
	protected CardinalityType cardinality = CARDINALITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessingRules() {
		if (accessingRules == null) {
			accessingRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					TypesPackage.FEATURE__ACCESSING_RULES, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT);
		}
		return accessingRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CardinalityType getCardinality() {
		return cardinality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardinality(CardinalityType newCardinality) {
		CardinalityType oldCardinality = cardinality;
		cardinality = newCardinality == null ? CARDINALITY_EDEFAULT : newCardinality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FEATURE__CARDINALITY, oldCardinality,
					cardinality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject) type;
			type = (Type) eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.FEATURE__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FEATURE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getParentType() {
		if (eContainerFeatureID() != TypesPackage.FEATURE__PARENT_TYPE)
			return null;
		return (Type) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentType(Type newParentType, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentType, TypesPackage.FEATURE__PARENT_TYPE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentType(Type newParentType) {
		if (newParentType != eInternalContainer()
				|| (eContainerFeatureID() != TypesPackage.FEATURE__PARENT_TYPE && newParentType != null)) {
			if (EcoreUtil.isAncestor(this, newParentType))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentType != null)
				msgs = ((InternalEObject) newParentType).eInverseAdd(this, TypesPackage.TYPE__FEATURES, Type.class,
						msgs);
			msgs = basicSetParentType(newParentType, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.FEATURE__PARENT_TYPE, newParentType,
					newParentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessingRules()).basicAdd(otherEnd, msgs);
		case TypesPackage.FEATURE__PARENT_TYPE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentType((Type) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			return ((InternalEList<?>) getAccessingRules()).basicRemove(otherEnd, msgs);
		case TypesPackage.FEATURE__PARENT_TYPE:
			return basicSetParentType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case TypesPackage.FEATURE__PARENT_TYPE:
			return eInternalContainer().eInverseRemove(this, TypesPackage.TYPE__FEATURES, Type.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			return getAccessingRules();
		case TypesPackage.FEATURE__CARDINALITY:
			return getCardinality();
		case TypesPackage.FEATURE__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case TypesPackage.FEATURE__PARENT_TYPE:
			return getParentType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			getAccessingRules().clear();
			getAccessingRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		case TypesPackage.FEATURE__CARDINALITY:
			setCardinality((CardinalityType) newValue);
			return;
		case TypesPackage.FEATURE__TYPE:
			setType((Type) newValue);
			return;
		case TypesPackage.FEATURE__PARENT_TYPE:
			setParentType((Type) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			getAccessingRules().clear();
			return;
		case TypesPackage.FEATURE__CARDINALITY:
			setCardinality(CARDINALITY_EDEFAULT);
			return;
		case TypesPackage.FEATURE__TYPE:
			setType((Type) null);
			return;
		case TypesPackage.FEATURE__PARENT_TYPE:
			setParentType((Type) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TypesPackage.FEATURE__ACCESSING_RULES:
			return accessingRules != null && !accessingRules.isEmpty();
		case TypesPackage.FEATURE__CARDINALITY:
			return cardinality != CARDINALITY_EDEFAULT;
		case TypesPackage.FEATURE__TYPE:
			return type != null;
		case TypesPackage.FEATURE__PARENT_TYPE:
			return getParentType() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (derivedFeatureID) {
			case TypesPackage.FEATURE__ACCESSING_RULES:
				return AccessPackage.ACCESSABLE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (derivedFeatureID) {
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (baseFeatureID) {
			case AccessPackage.ACCESSABLE__ACCESSING_RULES:
				return TypesPackage.FEATURE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		if (baseClass == Variable.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (cardinality: "); //$NON-NLS-1$
		result.append(cardinality);
		result.append(')');
		return result.toString();
	}

} //FeatureImpl
