/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.impl.AccessPackageImpl;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.impl.ActionsPackageImpl;
import de.upb.pose.specification.impl.SpecificationPackageImpl;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.Attribute;
import de.upb.pose.specification.types.CardinalityType;
import de.upb.pose.specification.types.DataType;
import de.upb.pose.specification.types.Feature;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.PrimitiveType;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesFactory;
import de.upb.pose.specification.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypesPackageImpl extends EPackageImpl implements TypesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primitiveTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cardinalityTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum abstractionTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.specification.types.TypesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TypesPackageImpl() {
		super(eNS_URI, TypesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TypesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TypesPackage init() {
		if (isInited)
			return (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Obtain or create and register package
		TypesPackageImpl theTypesPackage = (TypesPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new TypesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SpecificationPackageImpl theSpecificationPackage = (SpecificationPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) instanceof SpecificationPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) : SpecificationPackage.eINSTANCE);
		AccessPackageImpl theAccessPackage = (AccessPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) instanceof AccessPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) : AccessPackage.eINSTANCE);
		ActionsPackageImpl theActionsPackage = (ActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) instanceof ActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) : ActionsPackage.eINSTANCE);
		SubsystemsPackageImpl theSubsystemsPackage = (SubsystemsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) instanceof SubsystemsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) : SubsystemsPackage.eINSTANCE);

		// Create package meta-data objects
		theTypesPackage.createPackageContents();
		theSpecificationPackage.createPackageContents();
		theAccessPackage.createPackageContents();
		theActionsPackage.createPackageContents();
		theSubsystemsPackage.createPackageContents();

		// Initialize created meta-data
		theTypesPackage.initializePackageContents();
		theSpecificationPackage.initializePackageContents();
		theAccessPackage.initializePackageContents();
		theActionsPackage.initializePackageContents();
		theSubsystemsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTypesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TypesPackage.eNS_URI, theTypesPackage);
		return theTypesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_Attributes() {
		return (EReference) typeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_References() {
		return (EReference) typeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_Operations() {
		return (EReference) typeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_SubTypes() {
		return (EReference) typeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_SuperType() {
		return (EReference) typeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_SelfVariable() {
		return (EReference) typeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getType_Abstraction() {
		return (EAttribute) typeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getType_Features() {
		return (EReference) typeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimitiveType() {
		return primitiveTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrimitiveType_Type() {
		return (EAttribute) primitiveTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperation() {
		return operationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_ParentType() {
		return (EReference) operationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Parameters() {
		return (EReference) operationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Type() {
		return (EReference) operationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Overrides() {
		return (EReference) operationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Overriding() {
		return (EReference) operationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Actions() {
		return (EReference) operationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperation_Abstraction() {
		return (EAttribute) operationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameter_Operation() {
		return (EReference) parameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameter_Type() {
		return (EReference) parameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature() {
		return featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeature_Cardinality() {
		return (EAttribute) featureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature_Type() {
		return (EReference) featureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeature_ParentType() {
		return (EReference) featureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttribute() {
		return attributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReference() {
		return referenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference_SourceReference() {
		return (EReference) referenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReference_TargetReference() {
		return (EReference) referenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataType() {
		return dataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCardinalityType() {
		return cardinalityTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAbstractionType() {
		return abstractionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypesFactory getTypesFactory() {
		return (TypesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		typeEClass = createEClass(TYPE);
		createEReference(typeEClass, TYPE__ATTRIBUTES);
		createEReference(typeEClass, TYPE__REFERENCES);
		createEReference(typeEClass, TYPE__OPERATIONS);
		createEReference(typeEClass, TYPE__SUB_TYPES);
		createEReference(typeEClass, TYPE__SUPER_TYPE);
		createEReference(typeEClass, TYPE__SELF_VARIABLE);
		createEAttribute(typeEClass, TYPE__ABSTRACTION);
		createEReference(typeEClass, TYPE__FEATURES);

		primitiveTypeEClass = createEClass(PRIMITIVE_TYPE);
		createEAttribute(primitiveTypeEClass, PRIMITIVE_TYPE__TYPE);

		operationEClass = createEClass(OPERATION);
		createEReference(operationEClass, OPERATION__PARENT_TYPE);
		createEReference(operationEClass, OPERATION__PARAMETERS);
		createEReference(operationEClass, OPERATION__TYPE);
		createEReference(operationEClass, OPERATION__OVERRIDES);
		createEReference(operationEClass, OPERATION__OVERRIDING);
		createEReference(operationEClass, OPERATION__ACTIONS);
		createEAttribute(operationEClass, OPERATION__ABSTRACTION);

		parameterEClass = createEClass(PARAMETER);
		createEReference(parameterEClass, PARAMETER__OPERATION);
		createEReference(parameterEClass, PARAMETER__TYPE);

		featureEClass = createEClass(FEATURE);
		createEAttribute(featureEClass, FEATURE__CARDINALITY);
		createEReference(featureEClass, FEATURE__TYPE);
		createEReference(featureEClass, FEATURE__PARENT_TYPE);

		attributeEClass = createEClass(ATTRIBUTE);

		referenceEClass = createEClass(REFERENCE);
		createEReference(referenceEClass, REFERENCE__SOURCE_REFERENCE);
		createEReference(referenceEClass, REFERENCE__TARGET_REFERENCE);

		// Create enums
		dataTypeEEnum = createEEnum(DATA_TYPE);
		cardinalityTypeEEnum = createEEnum(CARDINALITY_TYPE);
		abstractionTypeEEnum = createEEnum(ABSTRACTION_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationPackage theSpecificationPackage = (SpecificationPackage) EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI);
		AccessPackage theAccessPackage = (AccessPackage) EPackage.Registry.INSTANCE.getEPackage(AccessPackage.eNS_URI);
		ActionsPackage theActionsPackage = (ActionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		typeEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		typeEClass.getESuperTypes().add(theAccessPackage.getAccessable());
		primitiveTypeEClass.getESuperTypes().add(this.getType());
		operationEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		operationEClass.getESuperTypes().add(theAccessPackage.getAccessable());
		parameterEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		parameterEClass.getESuperTypes().add(theActionsPackage.getVariable());
		featureEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		featureEClass.getESuperTypes().add(theAccessPackage.getAccessable());
		featureEClass.getESuperTypes().add(theActionsPackage.getVariable());
		attributeEClass.getESuperTypes().add(this.getFeature());
		referenceEClass.getESuperTypes().add(this.getFeature());

		// Initialize classes, features, and operations; add parameters
		initEClass(typeEClass, Type.class, "Type", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getType_Attributes(),
				this.getAttribute(),
				null,
				"attributes", null, 0, -1, Type.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_References(),
				this.getReference(),
				null,
				"references", null, 0, -1, Type.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_Operations(),
				this.getOperation(),
				this.getOperation_ParentType(),
				"operations", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_SubTypes(),
				this.getType(),
				this.getType_SuperType(),
				"subTypes", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_SuperType(),
				this.getType(),
				this.getType_SubTypes(),
				"superType", null, 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_SelfVariable(),
				theActionsPackage.getSelfVariable(),
				theActionsPackage.getSelfVariable_Type(),
				"selfVariable", null, 0, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getType_Abstraction(),
				this.getAbstractionType(),
				"abstraction", null, 1, 1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getType_Features(),
				this.getFeature(),
				this.getFeature_ParentType(),
				"features", null, 0, -1, Type.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(primitiveTypeEClass, PrimitiveType.class,
				"PrimitiveType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getPrimitiveType_Type(),
				this.getDataType(),
				"type", null, 1, 1, PrimitiveType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(operationEClass, Operation.class,
				"Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getOperation_ParentType(),
				this.getType(),
				this.getType_Operations(),
				"parentType", null, 1, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getOperation_Parameters(),
				this.getParameter(),
				this.getParameter_Operation(),
				"parameters", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getOperation_Type(),
				this.getType(),
				null,
				"type", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getOperation_Overrides(),
				this.getOperation(),
				this.getOperation_Overriding(),
				"overrides", null, 0, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getOperation_Overriding(),
				this.getOperation(),
				this.getOperation_Overrides(),
				"overriding", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getOperation_Actions(),
				theActionsPackage.getAction(),
				theActionsPackage.getAction_Operation(),
				"actions", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getOperation_Abstraction(),
				this.getAbstractionType(),
				"abstraction", null, 1, 1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(parameterEClass, Parameter.class,
				"Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getParameter_Operation(),
				this.getOperation(),
				this.getOperation_Parameters(),
				"operation", null, 1, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getParameter_Type(),
				this.getType(),
				null,
				"type", null, 1, 1, Parameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(featureEClass, Feature.class, "Feature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getFeature_Cardinality(),
				this.getCardinalityType(),
				"cardinality", null, 1, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getFeature_Type(),
				this.getType(),
				null,
				"type", null, 1, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getFeature_ParentType(),
				this.getType(),
				this.getType_Features(),
				"parentType", null, 1, 1, Feature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(attributeEClass, Attribute.class,
				"Attribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(referenceEClass, Reference.class,
				"Reference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getReference_SourceReference(),
				this.getReference(),
				this.getReference_TargetReference(),
				"sourceReference", null, 0, 1, Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getReference_TargetReference(),
				this.getReference(),
				this.getReference_SourceReference(),
				"targetReference", null, 0, 1, Reference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Initialize enums and add enum literals
		initEEnum(dataTypeEEnum, DataType.class, "DataType"); //$NON-NLS-1$
		addEEnumLiteral(dataTypeEEnum, DataType.BOOLEAN);
		addEEnumLiteral(dataTypeEEnum, DataType.INTEGER);
		addEEnumLiteral(dataTypeEEnum, DataType.REAL);
		addEEnumLiteral(dataTypeEEnum, DataType.STRING);

		initEEnum(cardinalityTypeEEnum, CardinalityType.class, "CardinalityType"); //$NON-NLS-1$
		addEEnumLiteral(cardinalityTypeEEnum, CardinalityType.ANY);
		addEEnumLiteral(cardinalityTypeEEnum, CardinalityType.SINGLE);
		addEEnumLiteral(cardinalityTypeEEnum, CardinalityType.MULTIPLE);

		initEEnum(abstractionTypeEEnum, AbstractionType.class, "AbstractionType"); //$NON-NLS-1$
		addEEnumLiteral(abstractionTypeEEnum, AbstractionType.ANY);
		addEEnumLiteral(abstractionTypeEEnum, AbstractionType.ABSTRACT);
		addEEnumLiteral(abstractionTypeEEnum, AbstractionType.CONCRETE);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] {
				"documentation", "This package contains all elements that are related to type declaration." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(typeEClass, source, new String[] { "documentation", "This class represents a type, e.g. a class." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_Attributes(), source, new String[] { "documentation", "The attributes of this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_References(), source, new String[] { "documentation", "The references of this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_Operations(), source, new String[] { "documentation", "The operations of this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_SubTypes(), source, new String[] { "documentation", "The sub-types of this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_SuperType(), source, new String[] { "documentation", "The super-type of this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getType_SelfVariable(), source, new String[] {
				"documentation", "The variable that represent this type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(primitiveTypeEClass, source, new String[] {
				"documentation", "This represents a primitive data type that can be used as type of an attribute." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(operationEClass, source, new String[] {
				"documentation", "This class represents an operation of a type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_ParentType(), source, new String[] {
				"documentation", "The type to which this operation belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_Parameters(), source, new String[] {
				"documentation", "The parameters of this operation." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_Type(), source, new String[] { "documentation", "The return type of this operation." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_Overrides(), source, new String[] {
				"documentation", "The operation which this operation overrides." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_Overriding(), source, new String[] {
				"documentation", "The operation which overrides this method." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getOperation_Actions(), source, new String[] {
				"documentation", "The actions which belong to this operation." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(parameterEClass, source, new String[] {
				"documentation", "This class represents an operation\'s parameter." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getParameter_Operation(), source, new String[] {
				"documentation", "The operation to which this parameter belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getParameter_Type(), source, new String[] { "documentation", "The type of the parameter." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				featureEClass,
				source,
				new String[] {
						"documentation", "This represents a type\'s feature. Each feature can have the cardinality 0..1 (single), 0..* (multiple), or any." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getFeature_Type(), source, new String[] { "documentation", "The type of the reference." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				attributeEClass,
				source,
				new String[] {
						"documentation", "This class represents a type\'s attribute. An attribute with cardinality 0..* (multiple) is an array." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(referenceEClass, source, new String[] {
				"documentation", "This class represents a type\'s reference to a type." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getReference_SourceReference(), source, new String[] {
				"documentation", "No documentation provided." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getReference_TargetReference(), source, new String[] {
				"documentation", "No documentation provided." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(dataTypeEEnum, source, new String[] {
				"documentation", "This enumeration defines all possible primitve data types usable with attributes." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(dataTypeEEnum.getELiterals().get(0), source, new String[] {
				"documentation", "This data type represents an value of a boolean type (Boolean)." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(dataTypeEEnum.getELiterals().get(1), source, new String[] {
				"documentation", "This data type represents an value of an integer type (BigInteger)." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(dataTypeEEnum.getELiterals().get(2), source, new String[] {
				"documentation", "This data type represents an value of a real type (BigDecimal)." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(dataTypeEEnum.getELiterals().get(3), source, new String[] {
				"documentation", "This data type represents an value of a real type (String)." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				cardinalityTypeEEnum,
				source,
				new String[] {
						"documentation", "This represents the cardinality of a feature (attribute or reference). ANY means, the cardinality can be 0..1 or 0..*." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				abstractionTypeEEnum,
				source,
				new String[] {
						"documentation", "This determines feasible kinds of abstraction for types and operations. ANY means, a type or operation can be abstract or concrete." //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL"; //$NON-NLS-1$	
		addAnnotation(
				getType_Attributes(),
				source,
				new String[] {
						"derivation", "self.features->select(feature : Feature | feature.oclIsKindOf(Attribute))->collect(f : Feature | f.oclAsType(Attribute))->asOrderedSet()" //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				getType_References(),
				source,
				new String[] {
						"derivation", "self.features->select(feature : Feature | feature.oclIsKindOf(Reference))->collect(f : Feature | f.oclAsType(Reference))->asOrderedSet()" //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

} //TypesPackageImpl
