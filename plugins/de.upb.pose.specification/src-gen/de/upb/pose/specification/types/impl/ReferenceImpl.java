/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.TypesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.impl.ReferenceImpl#getSourceReference <em>Source Reference</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.impl.ReferenceImpl#getTargetReference <em>Target Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferenceImpl extends FeatureImpl implements Reference {
	/**
	 * The cached value of the '{@link #getSourceReference() <em>Source Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceReference()
	 * @generated
	 * @ordered
	 */
	protected Reference sourceReference;

	/**
	 * The cached value of the '{@link #getTargetReference() <em>Target Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetReference()
	 * @generated
	 * @ordered
	 */
	protected Reference targetReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypesPackage.Literals.REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getSourceReference() {
		if (sourceReference != null && sourceReference.eIsProxy()) {
			InternalEObject oldSourceReference = (InternalEObject) sourceReference;
			sourceReference = (Reference) eResolveProxy(oldSourceReference);
			if (sourceReference != oldSourceReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.REFERENCE__SOURCE_REFERENCE,
							oldSourceReference, sourceReference));
			}
		}
		return sourceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetSourceReference() {
		return sourceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceReference(Reference newSourceReference, NotificationChain msgs) {
		Reference oldSourceReference = sourceReference;
		sourceReference = newSourceReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					TypesPackage.REFERENCE__SOURCE_REFERENCE, oldSourceReference, newSourceReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceReference(Reference newSourceReference) {
		if (newSourceReference != sourceReference) {
			NotificationChain msgs = null;
			if (sourceReference != null)
				msgs = ((InternalEObject) sourceReference).eInverseRemove(this,
						TypesPackage.REFERENCE__TARGET_REFERENCE, Reference.class, msgs);
			if (newSourceReference != null)
				msgs = ((InternalEObject) newSourceReference).eInverseAdd(this,
						TypesPackage.REFERENCE__TARGET_REFERENCE, Reference.class, msgs);
			msgs = basicSetSourceReference(newSourceReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.REFERENCE__SOURCE_REFERENCE,
					newSourceReference, newSourceReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference getTargetReference() {
		if (targetReference != null && targetReference.eIsProxy()) {
			InternalEObject oldTargetReference = (InternalEObject) targetReference;
			targetReference = (Reference) eResolveProxy(oldTargetReference);
			if (targetReference != oldTargetReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TypesPackage.REFERENCE__TARGET_REFERENCE,
							oldTargetReference, targetReference));
			}
		}
		return targetReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference basicGetTargetReference() {
		return targetReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetReference(Reference newTargetReference, NotificationChain msgs) {
		Reference oldTargetReference = targetReference;
		targetReference = newTargetReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					TypesPackage.REFERENCE__TARGET_REFERENCE, oldTargetReference, newTargetReference);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetReference(Reference newTargetReference) {
		if (newTargetReference != targetReference) {
			NotificationChain msgs = null;
			if (targetReference != null)
				msgs = ((InternalEObject) targetReference).eInverseRemove(this,
						TypesPackage.REFERENCE__SOURCE_REFERENCE, Reference.class, msgs);
			if (newTargetReference != null)
				msgs = ((InternalEObject) newTargetReference).eInverseAdd(this,
						TypesPackage.REFERENCE__SOURCE_REFERENCE, Reference.class, msgs);
			msgs = basicSetTargetReference(newTargetReference, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TypesPackage.REFERENCE__TARGET_REFERENCE,
					newTargetReference, newTargetReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			if (sourceReference != null)
				msgs = ((InternalEObject) sourceReference).eInverseRemove(this,
						TypesPackage.REFERENCE__TARGET_REFERENCE, Reference.class, msgs);
			return basicSetSourceReference((Reference) otherEnd, msgs);
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			if (targetReference != null)
				msgs = ((InternalEObject) targetReference).eInverseRemove(this,
						TypesPackage.REFERENCE__SOURCE_REFERENCE, Reference.class, msgs);
			return basicSetTargetReference((Reference) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			return basicSetSourceReference(null, msgs);
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			return basicSetTargetReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			if (resolve)
				return getSourceReference();
			return basicGetSourceReference();
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			if (resolve)
				return getTargetReference();
			return basicGetTargetReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			setSourceReference((Reference) newValue);
			return;
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			setTargetReference((Reference) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			setSourceReference((Reference) null);
			return;
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			setTargetReference((Reference) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case TypesPackage.REFERENCE__SOURCE_REFERENCE:
			return sourceReference != null;
		case TypesPackage.REFERENCE__TARGET_REFERENCE:
			return targetReference != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferenceImpl
