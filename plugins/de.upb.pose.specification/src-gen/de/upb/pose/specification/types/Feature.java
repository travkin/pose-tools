/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a type's feature. Each feature can have the cardinality 0..1 (single), 0..* (multiple), or any.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.Feature#getCardinality <em>Cardinality</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Feature#getType <em>Type</em>}</li>
 *   <li>{@link de.upb.pose.specification.types.Feature#getParentType <em>Parent Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.types.TypesPackage#getFeature()
 * @generated
 */
public interface Feature extends DesignElement, Accessable, Variable {
	/**
	 * Returns the value of the '<em><b>Cardinality</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.pose.specification.types.CardinalityType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cardinality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cardinality</em>' attribute.
	 * @see de.upb.pose.specification.types.CardinalityType
	 * @see #setCardinality(CardinalityType)
	 * @see de.upb.pose.specification.types.TypesPackage#getFeature_Cardinality()
	 * @generated
	 */
	CardinalityType getCardinality();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Feature#getCardinality <em>Cardinality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cardinality</em>' attribute.
	 * @see de.upb.pose.specification.types.CardinalityType
	 * @see #getCardinality()
	 * @generated
	 */
	void setCardinality(CardinalityType value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The type of the reference.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see de.upb.pose.specification.types.TypesPackage#getFeature_Type()
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Feature#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Parent Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Type#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Type</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Type</em>' container reference.
	 * @see #setParentType(Type)
	 * @see de.upb.pose.specification.types.TypesPackage#getFeature_ParentType()
	 * @see de.upb.pose.specification.types.Type#getFeatures
	 * @generated
	 */
	Type getParentType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.Feature#getParentType <em>Parent Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Type</em>' container reference.
	 * @see #getParentType()
	 * @generated
	 */
	void setParentType(Type value);

} // Feature
