/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a primitive data type that can be used as type of an attribute.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.types.PrimitiveType#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.types.TypesPackage#getPrimitiveType()
 * @generated
 */
public interface PrimitiveType extends Type {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.upb.pose.specification.types.DataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see de.upb.pose.specification.types.DataType
	 * @see #setType(DataType)
	 * @see de.upb.pose.specification.types.TypesPackage#getPrimitiveType_Type()
	 * @generated
	 */
	DataType getType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.types.PrimitiveType#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see de.upb.pose.specification.types.DataType
	 * @see #getType()
	 * @generated
	 */
	void setType(DataType value);

} // PrimitiveType
