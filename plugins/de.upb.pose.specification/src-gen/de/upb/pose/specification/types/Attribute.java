/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.types;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents a type's attribute. An attribute with cardinality 0..* (multiple) is an array.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.types.TypesPackage#getAttribute()
 * @generated
 */
public interface Attribute extends Feature {
} // Attribute
