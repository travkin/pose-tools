/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Named;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.PatternEnvironment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a pattern specification for a design pattern. Each specification describes one possibility to apply a design pattern in a software design.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getPattern <em>Pattern</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getPatternElements <em>Pattern Elements</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getDesignElements <em>Design Elements</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getSetFragments <em>Set Fragments</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getAccessRules <em>Access Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.PatternSpecification#getEnvironment <em>Environment</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification()
 * @generated
 */
public interface PatternSpecification extends Named, Commentable {
	/**
	 * Returns the value of the '<em><b>Pattern</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignPattern#getSpecifications <em>Specifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The design pattern to which this specification belongs to.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Pattern</em>' container reference.
	 * @see #setPattern(DesignPattern)
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_Pattern()
	 * @see de.upb.pose.specification.DesignPattern#getSpecifications
	 * @generated
	 */
	DesignPattern getPattern();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.PatternSpecification#getPattern <em>Pattern</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pattern</em>' container reference.
	 * @see #getPattern()
	 * @generated
	 */
	void setPattern(DesignPattern value);

	/**
	 * Returns the value of the '<em><b>Pattern Elements</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.PatternElement}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.PatternElement#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The pattern elements that are contained in this pattern specification.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Pattern Elements</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_PatternElements()
	 * @see de.upb.pose.specification.PatternElement#getSpecification
	 * @generated
	 */
	EList<PatternElement> getPatternElements();

	/**
	 * Returns the value of the '<em><b>Design Elements</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.DesignElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Design Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Design Elements</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_DesignElements()
	 * @generated
	 */
	EList<DesignElement> getDesignElements();

	/**
	 * Returns the value of the '<em><b>Set Fragments</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.SetFragment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Set Fragments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Set Fragments</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_SetFragments()
	 * @generated
	 */
	EList<SetFragment> getSetFragments();

	/**
	 * Returns the value of the '<em><b>Access Rules</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.access.AccessRule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The access rules which are contained in this pattern specification.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Access Rules</em>' reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_AccessRules()
	 * @generated
	 */
	EList<AccessRule> getAccessRules();

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The pattern environment that is contained in this pattern specification. Can be empty.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Environment</em>' reference.
	 * @see de.upb.pose.specification.SpecificationPackage#getPatternSpecification_Environment()
	 * @generated
	 */
	PatternEnvironment getEnvironment();

} // PatternSpecification
