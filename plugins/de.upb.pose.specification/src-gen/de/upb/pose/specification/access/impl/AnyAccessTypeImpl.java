/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import org.eclipse.emf.ecore.EClass;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AnyAccessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Any Access Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AnyAccessTypeImpl extends AccessTypeImpl implements AnyAccessType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnyAccessTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AccessPackage.Literals.ANY_ACCESS_TYPE;
	}

} //AnyAccessTypeImpl
