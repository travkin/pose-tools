/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains all elements that are related to access constraining.
 * <!-- end-model-doc -->
 * @see de.upb.pose.specification.access.AccessFactory
 * @generated
 */
public interface AccessPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "access"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni-paderborn.de/pose/specification/access"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "access"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AccessPackage eINSTANCE = de.upb.pose.specification.access.impl.AccessPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.PatternEnvironmentImpl <em>Pattern Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.PatternEnvironmentImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getPatternEnvironment()
	 * @generated
	 */
	int PATTERN_ENVIRONMENT = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__ID = SpecificationPackage.PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__ANNOTATIONS = SpecificationPackage.PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__NAME = SpecificationPackage.PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__SPECIFICATION = SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__CAN_BE_GENERATED = SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__ACCESSOR_RULES = SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Accessing Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT__ACCESSING_RULES = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pattern Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT_FEATURE_COUNT = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT___GET_ANNOTATION__STRING = SpecificationPackage.PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Pattern Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_ENVIRONMENT_OPERATION_COUNT = SpecificationPackage.PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.AccessRuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.AccessRuleImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessRule()
	 * @generated
	 */
	int ACCESS_RULE = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ID = SpecificationPackage.PATTERN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ANNOTATIONS = SpecificationPackage.PATTERN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__NAME = SpecificationPackage.PATTERN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__SPECIFICATION = SpecificationPackage.PATTERN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__CAN_BE_GENERATED = SpecificationPackage.PATTERN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ACCESSOR_RULES = SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__COMMENT = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Accessed Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ACCESSED_ELEMENT = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Accessor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ACCESSOR = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Forbidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__FORBIDDEN = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Access Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE__ACCESS_TYPE = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE_FEATURE_COUNT = SpecificationPackage.PATTERN_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE___GET_ANNOTATION__STRING = SpecificationPackage.PATTERN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_RULE_OPERATION_COUNT = SpecificationPackage.PATTERN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.Accessable <em>Accessable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.Accessable
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessable()
	 * @generated
	 */
	int ACCESSABLE = 2;

	/**
	 * The feature id for the '<em><b>Accessing Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSABLE__ACCESSING_RULES = 0;

	/**
	 * The number of structural features of the '<em>Accessable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Accessable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESSABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.AccessTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.AccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessType()
	 * @generated
	 */
	int ACCESS_TYPE = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE__ID = CorePackage.NAMED__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE__ANNOTATIONS = CorePackage.NAMED__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE__NAME = CorePackage.NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE__COMMENT = CorePackage.NAMED_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_FEATURE_COUNT = CorePackage.NAMED_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE___GET_ANNOTATION__STRING = CorePackage.NAMED___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_TYPE_OPERATION_COUNT = CorePackage.NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.AnyAccessTypeImpl <em>Any Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.AnyAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAnyAccessType()
	 * @generated
	 */
	int ANY_ACCESS_TYPE = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE__ID = ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE__ANNOTATIONS = ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE__NAME = ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE__COMMENT = ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Any Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE_FEATURE_COUNT = ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE___GET_ANNOTATION__STRING = ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Any Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANY_ACCESS_TYPE_OPERATION_COUNT = ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.ReferAccessTypeImpl <em>Refer Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.ReferAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getReferAccessType()
	 * @generated
	 */
	int REFER_ACCESS_TYPE = 5;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE__ID = ANY_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE__ANNOTATIONS = ANY_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE__NAME = ANY_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE__COMMENT = ANY_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Refer Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE_FEATURE_COUNT = ANY_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE___GET_ANNOTATION__STRING = ANY_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Refer Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFER_ACCESS_TYPE_OPERATION_COUNT = ANY_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.AccessAccessTypeImpl <em>Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.AccessAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessAccessType()
	 * @generated
	 */
	int ACCESS_ACCESS_TYPE = 6;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE__ID = ANY_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE__ANNOTATIONS = ANY_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE__NAME = ANY_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE__COMMENT = ANY_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE_FEATURE_COUNT = ANY_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE___GET_ANNOTATION__STRING = ANY_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_ACCESS_TYPE_OPERATION_COUNT = ANY_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.SpecializeAccessTypeImpl <em>Specialize Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.SpecializeAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getSpecializeAccessType()
	 * @generated
	 */
	int SPECIALIZE_ACCESS_TYPE = 7;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE__ID = REFER_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE__ANNOTATIONS = REFER_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE__NAME = REFER_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE__COMMENT = REFER_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Specialize Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE_FEATURE_COUNT = REFER_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE___GET_ANNOTATION__STRING = REFER_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Specialize Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPECIALIZE_ACCESS_TYPE_OPERATION_COUNT = REFER_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.InstantiateAccessTypeImpl <em>Instantiate Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.InstantiateAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getInstantiateAccessType()
	 * @generated
	 */
	int INSTANTIATE_ACCESS_TYPE = 8;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE__ID = REFER_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE__ANNOTATIONS = REFER_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE__NAME = REFER_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE__COMMENT = REFER_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Instantiate Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE_FEATURE_COUNT = REFER_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE___GET_ANNOTATION__STRING = REFER_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Instantiate Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTANTIATE_ACCESS_TYPE_OPERATION_COUNT = REFER_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.ReadAccessTypeImpl <em>Read Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.ReadAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getReadAccessType()
	 * @generated
	 */
	int READ_ACCESS_TYPE = 9;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE__ID = ACCESS_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE__ANNOTATIONS = ACCESS_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE__NAME = ACCESS_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE__COMMENT = ACCESS_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Read Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE_FEATURE_COUNT = ACCESS_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE___GET_ANNOTATION__STRING = ACCESS_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Read Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACCESS_TYPE_OPERATION_COUNT = ACCESS_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.WriteAccessTypeImpl <em>Write Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.WriteAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getWriteAccessType()
	 * @generated
	 */
	int WRITE_ACCESS_TYPE = 10;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE__ID = ACCESS_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE__ANNOTATIONS = ACCESS_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE__NAME = ACCESS_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE__COMMENT = ACCESS_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Write Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE_FEATURE_COUNT = ACCESS_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE___GET_ANNOTATION__STRING = ACCESS_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Write Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACCESS_TYPE_OPERATION_COUNT = ACCESS_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.access.impl.CallAccessTypeImpl <em>Call Access Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.access.impl.CallAccessTypeImpl
	 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getCallAccessType()
	 * @generated
	 */
	int CALL_ACCESS_TYPE = 11;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE__ID = ACCESS_ACCESS_TYPE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE__ANNOTATIONS = ACCESS_ACCESS_TYPE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE__NAME = ACCESS_ACCESS_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE__COMMENT = ACCESS_ACCESS_TYPE__COMMENT;

	/**
	 * The number of structural features of the '<em>Call Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE_FEATURE_COUNT = ACCESS_ACCESS_TYPE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE___GET_ANNOTATION__STRING = ACCESS_ACCESS_TYPE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Call Access Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACCESS_TYPE_OPERATION_COUNT = ACCESS_ACCESS_TYPE_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.PatternEnvironment <em>Pattern Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern Environment</em>'.
	 * @see de.upb.pose.specification.access.PatternEnvironment
	 * @generated
	 */
	EClass getPatternEnvironment();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.AccessRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see de.upb.pose.specification.access.AccessRule
	 * @generated
	 */
	EClass getAccessRule();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.access.AccessRule#getAccessedElement <em>Accessed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Accessed Element</em>'.
	 * @see de.upb.pose.specification.access.AccessRule#getAccessedElement()
	 * @see #getAccessRule()
	 * @generated
	 */
	EReference getAccessRule_AccessedElement();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.access.AccessRule#getAccessor <em>Accessor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Accessor</em>'.
	 * @see de.upb.pose.specification.access.AccessRule#getAccessor()
	 * @see #getAccessRule()
	 * @generated
	 */
	EReference getAccessRule_Accessor();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.pose.specification.access.AccessRule#isForbidden <em>Forbidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Forbidden</em>'.
	 * @see de.upb.pose.specification.access.AccessRule#isForbidden()
	 * @see #getAccessRule()
	 * @generated
	 */
	EAttribute getAccessRule_Forbidden();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.access.AccessRule#getAccessType <em>Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Access Type</em>'.
	 * @see de.upb.pose.specification.access.AccessRule#getAccessType()
	 * @see #getAccessRule()
	 * @generated
	 */
	EReference getAccessRule_AccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.Accessable <em>Accessable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Accessable</em>'.
	 * @see de.upb.pose.specification.access.Accessable
	 * @generated
	 */
	EClass getAccessable();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.access.Accessable#getAccessingRules <em>Accessing Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Accessing Rules</em>'.
	 * @see de.upb.pose.specification.access.Accessable#getAccessingRules()
	 * @see #getAccessable()
	 * @generated
	 */
	EReference getAccessable_AccessingRules();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.AccessType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see de.upb.pose.specification.access.AccessType
	 * @generated
	 */
	EClass getAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.AnyAccessType <em>Any Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Any Access Type</em>'.
	 * @see de.upb.pose.specification.access.AnyAccessType
	 * @generated
	 */
	EClass getAnyAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.ReferAccessType <em>Refer Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Refer Access Type</em>'.
	 * @see de.upb.pose.specification.access.ReferAccessType
	 * @generated
	 */
	EClass getReferAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.AccessAccessType <em>Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Type</em>'.
	 * @see de.upb.pose.specification.access.AccessAccessType
	 * @generated
	 */
	EClass getAccessAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.SpecializeAccessType <em>Specialize Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Specialize Access Type</em>'.
	 * @see de.upb.pose.specification.access.SpecializeAccessType
	 * @generated
	 */
	EClass getSpecializeAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.InstantiateAccessType <em>Instantiate Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instantiate Access Type</em>'.
	 * @see de.upb.pose.specification.access.InstantiateAccessType
	 * @generated
	 */
	EClass getInstantiateAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.ReadAccessType <em>Read Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Read Access Type</em>'.
	 * @see de.upb.pose.specification.access.ReadAccessType
	 * @generated
	 */
	EClass getReadAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.WriteAccessType <em>Write Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Write Access Type</em>'.
	 * @see de.upb.pose.specification.access.WriteAccessType
	 * @generated
	 */
	EClass getWriteAccessType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.access.CallAccessType <em>Call Access Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Access Type</em>'.
	 * @see de.upb.pose.specification.access.CallAccessType
	 * @generated
	 */
	EClass getCallAccessType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AccessFactory getAccessFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.PatternEnvironmentImpl <em>Pattern Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.PatternEnvironmentImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getPatternEnvironment()
		 * @generated
		 */
		EClass PATTERN_ENVIRONMENT = eINSTANCE.getPatternEnvironment();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.AccessRuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.AccessRuleImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessRule()
		 * @generated
		 */
		EClass ACCESS_RULE = eINSTANCE.getAccessRule();

		/**
		 * The meta object literal for the '<em><b>Accessed Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_RULE__ACCESSED_ELEMENT = eINSTANCE.getAccessRule_AccessedElement();

		/**
		 * The meta object literal for the '<em><b>Accessor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_RULE__ACCESSOR = eINSTANCE.getAccessRule_Accessor();

		/**
		 * The meta object literal for the '<em><b>Forbidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_RULE__FORBIDDEN = eINSTANCE.getAccessRule_Forbidden();

		/**
		 * The meta object literal for the '<em><b>Access Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESS_RULE__ACCESS_TYPE = eINSTANCE.getAccessRule_AccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.Accessable <em>Accessable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.Accessable
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessable()
		 * @generated
		 */
		EClass ACCESSABLE = eINSTANCE.getAccessable();

		/**
		 * The meta object literal for the '<em><b>Accessing Rules</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCESSABLE__ACCESSING_RULES = eINSTANCE.getAccessable_AccessingRules();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.AccessTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.AccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessType()
		 * @generated
		 */
		EClass ACCESS_TYPE = eINSTANCE.getAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.AnyAccessTypeImpl <em>Any Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.AnyAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAnyAccessType()
		 * @generated
		 */
		EClass ANY_ACCESS_TYPE = eINSTANCE.getAnyAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.ReferAccessTypeImpl <em>Refer Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.ReferAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getReferAccessType()
		 * @generated
		 */
		EClass REFER_ACCESS_TYPE = eINSTANCE.getReferAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.AccessAccessTypeImpl <em>Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.AccessAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getAccessAccessType()
		 * @generated
		 */
		EClass ACCESS_ACCESS_TYPE = eINSTANCE.getAccessAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.SpecializeAccessTypeImpl <em>Specialize Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.SpecializeAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getSpecializeAccessType()
		 * @generated
		 */
		EClass SPECIALIZE_ACCESS_TYPE = eINSTANCE.getSpecializeAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.InstantiateAccessTypeImpl <em>Instantiate Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.InstantiateAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getInstantiateAccessType()
		 * @generated
		 */
		EClass INSTANTIATE_ACCESS_TYPE = eINSTANCE.getInstantiateAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.ReadAccessTypeImpl <em>Read Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.ReadAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getReadAccessType()
		 * @generated
		 */
		EClass READ_ACCESS_TYPE = eINSTANCE.getReadAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.WriteAccessTypeImpl <em>Write Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.WriteAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getWriteAccessType()
		 * @generated
		 */
		EClass WRITE_ACCESS_TYPE = eINSTANCE.getWriteAccessType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.access.impl.CallAccessTypeImpl <em>Call Access Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.access.impl.CallAccessTypeImpl
		 * @see de.upb.pose.specification.access.impl.AccessPackageImpl#getCallAccessType()
		 * @generated
		 */
		EClass CALL_ACCESS_TYPE = eINSTANCE.getCallAccessType();

	}

} //AccessPackage
