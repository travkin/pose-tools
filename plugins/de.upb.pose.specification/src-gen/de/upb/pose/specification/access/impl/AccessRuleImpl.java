/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.impl.PatternElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.access.impl.AccessRuleImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.impl.AccessRuleImpl#getAccessedElement <em>Accessed Element</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.impl.AccessRuleImpl#getAccessor <em>Accessor</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.impl.AccessRuleImpl#isForbidden <em>Forbidden</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.impl.AccessRuleImpl#getAccessType <em>Access Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AccessRuleImpl extends PatternElementImpl implements AccessRule {
	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = ""; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccessedElement() <em>Accessed Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessedElement()
	 * @generated
	 * @ordered
	 */
	protected Accessable accessedElement;

	/**
	 * The cached value of the '{@link #getAccessor() <em>Accessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessor()
	 * @generated
	 * @ordered
	 */
	protected PatternElement accessor;

	/**
	 * The default value of the '{@link #isForbidden() <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForbidden()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FORBIDDEN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isForbidden() <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForbidden()
	 * @generated
	 * @ordered
	 */
	protected boolean forbidden = FORBIDDEN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccessType() <em>Access Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessType()
	 * @generated
	 * @ordered
	 */
	protected AccessType accessType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AccessPackage.Literals.ACCESS_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AccessPackage.ACCESS_RULE__COMMENT, oldComment,
					comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accessable getAccessedElement() {
		if (accessedElement != null && accessedElement.eIsProxy()) {
			InternalEObject oldAccessedElement = (InternalEObject) accessedElement;
			accessedElement = (Accessable) eResolveProxy(oldAccessedElement);
			if (accessedElement != oldAccessedElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT, oldAccessedElement, accessedElement));
			}
		}
		return accessedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Accessable basicGetAccessedElement() {
		return accessedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessedElement(Accessable newAccessedElement, NotificationChain msgs) {
		Accessable oldAccessedElement = accessedElement;
		accessedElement = newAccessedElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT, oldAccessedElement, newAccessedElement);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessedElement(Accessable newAccessedElement) {
		if (newAccessedElement != accessedElement) {
			NotificationChain msgs = null;
			if (accessedElement != null)
				msgs = ((InternalEObject) accessedElement).eInverseRemove(this,
						AccessPackage.ACCESSABLE__ACCESSING_RULES, Accessable.class, msgs);
			if (newAccessedElement != null)
				msgs = ((InternalEObject) newAccessedElement).eInverseAdd(this,
						AccessPackage.ACCESSABLE__ACCESSING_RULES, Accessable.class, msgs);
			msgs = basicSetAccessedElement(newAccessedElement, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT,
					newAccessedElement, newAccessedElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternElement getAccessor() {
		if (accessor != null && accessor.eIsProxy()) {
			InternalEObject oldAccessor = (InternalEObject) accessor;
			accessor = (PatternElement) eResolveProxy(oldAccessor);
			if (accessor != oldAccessor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AccessPackage.ACCESS_RULE__ACCESSOR,
							oldAccessor, accessor));
			}
		}
		return accessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternElement basicGetAccessor() {
		return accessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessor(PatternElement newAccessor, NotificationChain msgs) {
		PatternElement oldAccessor = accessor;
		accessor = newAccessor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AccessPackage.ACCESS_RULE__ACCESSOR, oldAccessor, newAccessor);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessor(PatternElement newAccessor) {
		if (newAccessor != accessor) {
			NotificationChain msgs = null;
			if (accessor != null)
				msgs = ((InternalEObject) accessor).eInverseRemove(this,
						SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES, PatternElement.class, msgs);
			if (newAccessor != null)
				msgs = ((InternalEObject) newAccessor).eInverseAdd(this,
						SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES, PatternElement.class, msgs);
			msgs = basicSetAccessor(newAccessor, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AccessPackage.ACCESS_RULE__ACCESSOR, newAccessor,
					newAccessor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isForbidden() {
		return forbidden;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForbidden(boolean newForbidden) {
		boolean oldForbidden = forbidden;
		forbidden = newForbidden;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AccessPackage.ACCESS_RULE__FORBIDDEN, oldForbidden,
					forbidden));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessType getAccessType() {
		if (accessType != null && accessType.eIsProxy()) {
			InternalEObject oldAccessType = (InternalEObject) accessType;
			accessType = (AccessType) eResolveProxy(oldAccessType);
			if (accessType != oldAccessType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AccessPackage.ACCESS_RULE__ACCESS_TYPE,
							oldAccessType, accessType));
			}
		}
		return accessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessType basicGetAccessType() {
		return accessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessType(AccessType newAccessType) {
		AccessType oldAccessType = accessType;
		accessType = newAccessType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AccessPackage.ACCESS_RULE__ACCESS_TYPE,
					oldAccessType, accessType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			if (accessedElement != null)
				msgs = ((InternalEObject) accessedElement).eInverseRemove(this,
						AccessPackage.ACCESSABLE__ACCESSING_RULES, Accessable.class, msgs);
			return basicSetAccessedElement((Accessable) otherEnd, msgs);
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			if (accessor != null)
				msgs = ((InternalEObject) accessor).eInverseRemove(this,
						SpecificationPackage.PATTERN_ELEMENT__ACCESSOR_RULES, PatternElement.class, msgs);
			return basicSetAccessor((PatternElement) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			return basicSetAccessedElement(null, msgs);
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			return basicSetAccessor(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__COMMENT:
			return getComment();
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			if (resolve)
				return getAccessedElement();
			return basicGetAccessedElement();
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			if (resolve)
				return getAccessor();
			return basicGetAccessor();
		case AccessPackage.ACCESS_RULE__FORBIDDEN:
			return isForbidden();
		case AccessPackage.ACCESS_RULE__ACCESS_TYPE:
			if (resolve)
				return getAccessType();
			return basicGetAccessType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__COMMENT:
			setComment((String) newValue);
			return;
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			setAccessedElement((Accessable) newValue);
			return;
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			setAccessor((PatternElement) newValue);
			return;
		case AccessPackage.ACCESS_RULE__FORBIDDEN:
			setForbidden((Boolean) newValue);
			return;
		case AccessPackage.ACCESS_RULE__ACCESS_TYPE:
			setAccessType((AccessType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__COMMENT:
			setComment(COMMENT_EDEFAULT);
			return;
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			setAccessedElement((Accessable) null);
			return;
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			setAccessor((PatternElement) null);
			return;
		case AccessPackage.ACCESS_RULE__FORBIDDEN:
			setForbidden(FORBIDDEN_EDEFAULT);
			return;
		case AccessPackage.ACCESS_RULE__ACCESS_TYPE:
			setAccessType((AccessType) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AccessPackage.ACCESS_RULE__COMMENT:
			return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
		case AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT:
			return accessedElement != null;
		case AccessPackage.ACCESS_RULE__ACCESSOR:
			return accessor != null;
		case AccessPackage.ACCESS_RULE__FORBIDDEN:
			return forbidden != FORBIDDEN_EDEFAULT;
		case AccessPackage.ACCESS_RULE__ACCESS_TYPE:
			return accessType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (derivedFeatureID) {
			case AccessPackage.ACCESS_RULE__COMMENT:
				return CorePackage.COMMENTABLE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Commentable.class) {
			switch (baseFeatureID) {
			case CorePackage.COMMENTABLE__COMMENT:
				return AccessPackage.ACCESS_RULE__COMMENT;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (comment: "); //$NON-NLS-1$
		result.append(comment);
		result.append(", forbidden: "); //$NON-NLS-1$
		result.append(forbidden);
		result.append(')');
		return result.toString();
	}

} //AccessRuleImpl
