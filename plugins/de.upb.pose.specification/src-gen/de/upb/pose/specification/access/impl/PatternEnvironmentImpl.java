/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.impl.PatternElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.access.impl.PatternEnvironmentImpl#getAccessingRules <em>Accessing Rules</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PatternEnvironmentImpl extends PatternElementImpl implements PatternEnvironment {
	/**
	 * The cached value of the '{@link #getAccessingRules() <em>Accessing Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessingRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessingRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternEnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AccessPackage.Literals.PATTERN_ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessingRules() {
		if (accessingRules == null) {
			accessingRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT);
		}
		return accessingRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessingRules()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			return ((InternalEList<?>) getAccessingRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			return getAccessingRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			getAccessingRules().clear();
			getAccessingRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			getAccessingRules().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
			return accessingRules != null && !accessingRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (derivedFeatureID) {
			case AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES:
				return AccessPackage.ACCESSABLE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (baseFeatureID) {
			case AccessPackage.ACCESSABLE__ACCESSING_RULES:
				return AccessPackage.PATTERN_ENVIRONMENT__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //PatternEnvironmentImpl
