/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Specialize Access Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a specialization of a design element, e.g. a type is a subtype of another type or an operation overrides another operation or implements an operation defined in an interface.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.access.AccessPackage#getSpecializeAccessType()
 * @generated
 */
public interface SpecializeAccessType extends ReferAccessType {
} // SpecializeAccessType
