/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import de.upb.pose.core.Commentable;
import de.upb.pose.specification.PatternElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a constraint concerning the coupling of software design elements. An access rule specifies that a certain design element has access to (is coupled with) a certain design element or is not allowed to do so.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.access.AccessRule#getAccessedElement <em>Accessed Element</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.AccessRule#getAccessor <em>Accessor</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.AccessRule#isForbidden <em>Forbidden</em>}</li>
 *   <li>{@link de.upb.pose.specification.access.AccessRule#getAccessType <em>Access Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.access.AccessPackage#getAccessRule()
 * @generated
 */
public interface AccessRule extends PatternElement, Commentable {
	/**
	 * Returns the value of the '<em><b>Accessed Element</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.access.Accessable#getAccessingRules <em>Accessing Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The accessable element which this rule constraints.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Accessed Element</em>' reference.
	 * @see #setAccessedElement(Accessable)
	 * @see de.upb.pose.specification.access.AccessPackage#getAccessRule_AccessedElement()
	 * @see de.upb.pose.specification.access.Accessable#getAccessingRules
	 * @generated
	 */
	Accessable getAccessedElement();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.access.AccessRule#getAccessedElement <em>Accessed Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessed Element</em>' reference.
	 * @see #getAccessedElement()
	 * @generated
	 */
	void setAccessedElement(Accessable value);

	/**
	 * Returns the value of the '<em><b>Accessor</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.PatternElement#getAccessorRules <em>Accessor Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The pattern element for which this access rule is used - the accessor.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Accessor</em>' reference.
	 * @see #setAccessor(PatternElement)
	 * @see de.upb.pose.specification.access.AccessPackage#getAccessRule_Accessor()
	 * @see de.upb.pose.specification.PatternElement#getAccessorRules
	 * @generated
	 */
	PatternElement getAccessor();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.access.AccessRule#getAccessor <em>Accessor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessor</em>' reference.
	 * @see #getAccessor()
	 * @generated
	 */
	void setAccessor(PatternElement value);

	/**
	 * Returns the value of the '<em><b>Forbidden</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forbidden</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forbidden</em>' attribute.
	 * @see #setForbidden(boolean)
	 * @see de.upb.pose.specification.access.AccessPackage#getAccessRule_Forbidden()
	 * @generated
	 */
	boolean isForbidden();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.access.AccessRule#isForbidden <em>Forbidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Forbidden</em>' attribute.
	 * @see #isForbidden()
	 * @generated
	 */
	void setForbidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Access Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Type</em>' reference.
	 * @see #setAccessType(AccessType)
	 * @see de.upb.pose.specification.access.AccessPackage#getAccessRule_AccessType()
	 * @generated
	 */
	AccessType getAccessType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.access.AccessRule#getAccessType <em>Access Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Type</em>' reference.
	 * @see #getAccessType()
	 * @generated
	 */
	void setAccessType(AccessType value);

} // AccessRule
