/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.pose.specification.access.AccessAccessType;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AnyAccessType;
import de.upb.pose.specification.access.CallAccessType;
import de.upb.pose.specification.access.InstantiateAccessType;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.access.ReadAccessType;
import de.upb.pose.specification.access.ReferAccessType;
import de.upb.pose.specification.access.SpecializeAccessType;
import de.upb.pose.specification.access.WriteAccessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AccessFactoryImpl extends EFactoryImpl implements AccessFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AccessFactory init() {
		try {
			AccessFactory theAccessFactory = (AccessFactory) EPackage.Registry.INSTANCE
					.getEFactory(AccessPackage.eNS_URI);
			if (theAccessFactory != null) {
				return theAccessFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AccessFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case AccessPackage.PATTERN_ENVIRONMENT:
			return createPatternEnvironment();
		case AccessPackage.ACCESS_RULE:
			return createAccessRule();
		case AccessPackage.ANY_ACCESS_TYPE:
			return createAnyAccessType();
		case AccessPackage.REFER_ACCESS_TYPE:
			return createReferAccessType();
		case AccessPackage.ACCESS_ACCESS_TYPE:
			return createAccessAccessType();
		case AccessPackage.SPECIALIZE_ACCESS_TYPE:
			return createSpecializeAccessType();
		case AccessPackage.INSTANTIATE_ACCESS_TYPE:
			return createInstantiateAccessType();
		case AccessPackage.READ_ACCESS_TYPE:
			return createReadAccessType();
		case AccessPackage.WRITE_ACCESS_TYPE:
			return createWriteAccessType();
		case AccessPackage.CALL_ACCESS_TYPE:
			return createCallAccessType();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PatternEnvironment createPatternEnvironment() {
		PatternEnvironmentImpl patternEnvironment = new PatternEnvironmentImpl();
		return patternEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessRule createAccessRule() {
		AccessRuleImpl accessRule = new AccessRuleImpl();
		return accessRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnyAccessType createAnyAccessType() {
		AnyAccessTypeImpl anyAccessType = new AnyAccessTypeImpl();
		return anyAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferAccessType createReferAccessType() {
		ReferAccessTypeImpl referAccessType = new ReferAccessTypeImpl();
		return referAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessAccessType createAccessAccessType() {
		AccessAccessTypeImpl accessAccessType = new AccessAccessTypeImpl();
		return accessAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecializeAccessType createSpecializeAccessType() {
		SpecializeAccessTypeImpl specializeAccessType = new SpecializeAccessTypeImpl();
		return specializeAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstantiateAccessType createInstantiateAccessType() {
		InstantiateAccessTypeImpl instantiateAccessType = new InstantiateAccessTypeImpl();
		return instantiateAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadAccessType createReadAccessType() {
		ReadAccessTypeImpl readAccessType = new ReadAccessTypeImpl();
		return readAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriteAccessType createWriteAccessType() {
		WriteAccessTypeImpl writeAccessType = new WriteAccessTypeImpl();
		return writeAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAccessType createCallAccessType() {
		CallAccessTypeImpl callAccessType = new CallAccessTypeImpl();
		return callAccessType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessPackage getAccessPackage() {
		return (AccessPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AccessPackage getPackage() {
		return AccessPackage.eINSTANCE;
	}

} //AccessFactoryImpl
