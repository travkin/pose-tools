/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.pose.specification.access.AccessPackage
 * @generated
 */
public interface AccessFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AccessFactory eINSTANCE = de.upb.pose.specification.access.impl.AccessFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Pattern Environment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pattern Environment</em>'.
	 * @generated
	 */
	PatternEnvironment createPatternEnvironment();

	/**
	 * Returns a new object of class '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule</em>'.
	 * @generated
	 */
	AccessRule createAccessRule();

	/**
	 * Returns a new object of class '<em>Any Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Any Access Type</em>'.
	 * @generated
	 */
	AnyAccessType createAnyAccessType();

	/**
	 * Returns a new object of class '<em>Refer Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Refer Access Type</em>'.
	 * @generated
	 */
	ReferAccessType createReferAccessType();

	/**
	 * Returns a new object of class '<em>Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Type</em>'.
	 * @generated
	 */
	AccessAccessType createAccessAccessType();

	/**
	 * Returns a new object of class '<em>Specialize Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Specialize Access Type</em>'.
	 * @generated
	 */
	SpecializeAccessType createSpecializeAccessType();

	/**
	 * Returns a new object of class '<em>Instantiate Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instantiate Access Type</em>'.
	 * @generated
	 */
	InstantiateAccessType createInstantiateAccessType();

	/**
	 * Returns a new object of class '<em>Read Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Read Access Type</em>'.
	 * @generated
	 */
	ReadAccessType createReadAccessType();

	/**
	 * Returns a new object of class '<em>Write Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Write Access Type</em>'.
	 * @generated
	 */
	WriteAccessType createWriteAccessType();

	/**
	 * Returns a new object of class '<em>Call Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Access Type</em>'.
	 * @generated
	 */
	CallAccessType createCallAccessType();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AccessPackage getAccessPackage();

} //AccessFactory
