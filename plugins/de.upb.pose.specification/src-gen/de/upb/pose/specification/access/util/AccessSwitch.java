/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Identifier;
import de.upb.pose.core.Named;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.access.AccessAccessType;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.access.AnyAccessType;
import de.upb.pose.specification.access.CallAccessType;
import de.upb.pose.specification.access.InstantiateAccessType;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.access.ReadAccessType;
import de.upb.pose.specification.access.ReferAccessType;
import de.upb.pose.specification.access.SpecializeAccessType;
import de.upb.pose.specification.access.WriteAccessType;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.pose.specification.access.AccessPackage
 * @generated
 */
public class AccessSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AccessPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessSwitch() {
		if (modelPackage == null) {
			modelPackage = AccessPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case AccessPackage.PATTERN_ENVIRONMENT: {
			PatternEnvironment patternEnvironment = (PatternEnvironment) theEObject;
			T result = casePatternEnvironment(patternEnvironment);
			if (result == null)
				result = casePatternElement(patternEnvironment);
			if (result == null)
				result = caseAccessable(patternEnvironment);
			if (result == null)
				result = caseNamed(patternEnvironment);
			if (result == null)
				result = caseIdentifier(patternEnvironment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.ACCESS_RULE: {
			AccessRule accessRule = (AccessRule) theEObject;
			T result = caseAccessRule(accessRule);
			if (result == null)
				result = casePatternElement(accessRule);
			if (result == null)
				result = caseCommentable(accessRule);
			if (result == null)
				result = caseNamed(accessRule);
			if (result == null)
				result = caseIdentifier(accessRule);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.ACCESSABLE: {
			Accessable accessable = (Accessable) theEObject;
			T result = caseAccessable(accessable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.ACCESS_TYPE: {
			AccessType accessType = (AccessType) theEObject;
			T result = caseAccessType(accessType);
			if (result == null)
				result = caseNamed(accessType);
			if (result == null)
				result = caseCommentable(accessType);
			if (result == null)
				result = caseIdentifier(accessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.ANY_ACCESS_TYPE: {
			AnyAccessType anyAccessType = (AnyAccessType) theEObject;
			T result = caseAnyAccessType(anyAccessType);
			if (result == null)
				result = caseAccessType(anyAccessType);
			if (result == null)
				result = caseNamed(anyAccessType);
			if (result == null)
				result = caseCommentable(anyAccessType);
			if (result == null)
				result = caseIdentifier(anyAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.REFER_ACCESS_TYPE: {
			ReferAccessType referAccessType = (ReferAccessType) theEObject;
			T result = caseReferAccessType(referAccessType);
			if (result == null)
				result = caseAnyAccessType(referAccessType);
			if (result == null)
				result = caseAccessType(referAccessType);
			if (result == null)
				result = caseNamed(referAccessType);
			if (result == null)
				result = caseCommentable(referAccessType);
			if (result == null)
				result = caseIdentifier(referAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.ACCESS_ACCESS_TYPE: {
			AccessAccessType accessAccessType = (AccessAccessType) theEObject;
			T result = caseAccessAccessType(accessAccessType);
			if (result == null)
				result = caseAnyAccessType(accessAccessType);
			if (result == null)
				result = caseAccessType(accessAccessType);
			if (result == null)
				result = caseNamed(accessAccessType);
			if (result == null)
				result = caseCommentable(accessAccessType);
			if (result == null)
				result = caseIdentifier(accessAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.SPECIALIZE_ACCESS_TYPE: {
			SpecializeAccessType specializeAccessType = (SpecializeAccessType) theEObject;
			T result = caseSpecializeAccessType(specializeAccessType);
			if (result == null)
				result = caseReferAccessType(specializeAccessType);
			if (result == null)
				result = caseAnyAccessType(specializeAccessType);
			if (result == null)
				result = caseAccessType(specializeAccessType);
			if (result == null)
				result = caseNamed(specializeAccessType);
			if (result == null)
				result = caseCommentable(specializeAccessType);
			if (result == null)
				result = caseIdentifier(specializeAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.INSTANTIATE_ACCESS_TYPE: {
			InstantiateAccessType instantiateAccessType = (InstantiateAccessType) theEObject;
			T result = caseInstantiateAccessType(instantiateAccessType);
			if (result == null)
				result = caseReferAccessType(instantiateAccessType);
			if (result == null)
				result = caseAnyAccessType(instantiateAccessType);
			if (result == null)
				result = caseAccessType(instantiateAccessType);
			if (result == null)
				result = caseNamed(instantiateAccessType);
			if (result == null)
				result = caseCommentable(instantiateAccessType);
			if (result == null)
				result = caseIdentifier(instantiateAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.READ_ACCESS_TYPE: {
			ReadAccessType readAccessType = (ReadAccessType) theEObject;
			T result = caseReadAccessType(readAccessType);
			if (result == null)
				result = caseAccessAccessType(readAccessType);
			if (result == null)
				result = caseAnyAccessType(readAccessType);
			if (result == null)
				result = caseAccessType(readAccessType);
			if (result == null)
				result = caseNamed(readAccessType);
			if (result == null)
				result = caseCommentable(readAccessType);
			if (result == null)
				result = caseIdentifier(readAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.WRITE_ACCESS_TYPE: {
			WriteAccessType writeAccessType = (WriteAccessType) theEObject;
			T result = caseWriteAccessType(writeAccessType);
			if (result == null)
				result = caseAccessAccessType(writeAccessType);
			if (result == null)
				result = caseAnyAccessType(writeAccessType);
			if (result == null)
				result = caseAccessType(writeAccessType);
			if (result == null)
				result = caseNamed(writeAccessType);
			if (result == null)
				result = caseCommentable(writeAccessType);
			if (result == null)
				result = caseIdentifier(writeAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AccessPackage.CALL_ACCESS_TYPE: {
			CallAccessType callAccessType = (CallAccessType) theEObject;
			T result = caseCallAccessType(callAccessType);
			if (result == null)
				result = caseAccessAccessType(callAccessType);
			if (result == null)
				result = caseAnyAccessType(callAccessType);
			if (result == null)
				result = caseAccessType(callAccessType);
			if (result == null)
				result = caseNamed(callAccessType);
			if (result == null)
				result = caseCommentable(callAccessType);
			if (result == null)
				result = caseIdentifier(callAccessType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern Environment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern Environment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePatternEnvironment(PatternEnvironment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessRule(AccessRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Accessable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Accessable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessable(Accessable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessType(AccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Any Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Any Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnyAccessType(AnyAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Refer Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Refer Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferAccessType(ReferAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessAccessType(AccessAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Specialize Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Specialize Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSpecializeAccessType(SpecializeAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instantiate Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instantiate Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstantiateAccessType(InstantiateAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Read Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Read Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadAccessType(ReadAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Write Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Write Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWriteAccessType(WriteAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Access Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Access Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallAccessType(CallAccessType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifier(Identifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePatternElement(PatternElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Commentable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Commentable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommentable(Commentable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AccessSwitch
