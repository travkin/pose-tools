/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessAccessType;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.access.AnyAccessType;
import de.upb.pose.specification.access.CallAccessType;
import de.upb.pose.specification.access.InstantiateAccessType;
import de.upb.pose.specification.access.PatternEnvironment;
import de.upb.pose.specification.access.ReadAccessType;
import de.upb.pose.specification.access.ReferAccessType;
import de.upb.pose.specification.access.SpecializeAccessType;
import de.upb.pose.specification.access.WriteAccessType;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.impl.ActionsPackageImpl;
import de.upb.pose.specification.impl.SpecificationPackageImpl;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AccessPackageImpl extends EPackageImpl implements AccessPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternEnvironmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass anyAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accessAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass specializeAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass instantiateAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass writeAccessTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callAccessTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.specification.access.AccessPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AccessPackageImpl() {
		super(eNS_URI, AccessFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AccessPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AccessPackage init() {
		if (isInited)
			return (AccessPackage) EPackage.Registry.INSTANCE.getEPackage(AccessPackage.eNS_URI);

		// Obtain or create and register package
		AccessPackageImpl theAccessPackage = (AccessPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AccessPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new AccessPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SpecificationPackageImpl theSpecificationPackage = (SpecificationPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) instanceof SpecificationPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) : SpecificationPackage.eINSTANCE);
		ActionsPackageImpl theActionsPackage = (ActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) instanceof ActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) : ActionsPackage.eINSTANCE);
		SubsystemsPackageImpl theSubsystemsPackage = (SubsystemsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) instanceof SubsystemsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) : SubsystemsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theAccessPackage.createPackageContents();
		theSpecificationPackage.createPackageContents();
		theActionsPackage.createPackageContents();
		theSubsystemsPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theAccessPackage.initializePackageContents();
		theSpecificationPackage.initializePackageContents();
		theActionsPackage.initializePackageContents();
		theSubsystemsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAccessPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AccessPackage.eNS_URI, theAccessPackage);
		return theAccessPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternEnvironment() {
		return patternEnvironmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessRule() {
		return accessRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessRule_AccessedElement() {
		return (EReference) accessRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessRule_Accessor() {
		return (EReference) accessRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccessRule_Forbidden() {
		return (EAttribute) accessRuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessRule_AccessType() {
		return (EReference) accessRuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessable() {
		return accessableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccessable_AccessingRules() {
		return (EReference) accessableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessType() {
		return accessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnyAccessType() {
		return anyAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReferAccessType() {
		return referAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccessAccessType() {
		return accessAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpecializeAccessType() {
		return specializeAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInstantiateAccessType() {
		return instantiateAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReadAccessType() {
		return readAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWriteAccessType() {
		return writeAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallAccessType() {
		return callAccessTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessFactory getAccessFactory() {
		return (AccessFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		patternEnvironmentEClass = createEClass(PATTERN_ENVIRONMENT);

		accessRuleEClass = createEClass(ACCESS_RULE);
		createEReference(accessRuleEClass, ACCESS_RULE__ACCESSED_ELEMENT);
		createEReference(accessRuleEClass, ACCESS_RULE__ACCESSOR);
		createEAttribute(accessRuleEClass, ACCESS_RULE__FORBIDDEN);
		createEReference(accessRuleEClass, ACCESS_RULE__ACCESS_TYPE);

		accessableEClass = createEClass(ACCESSABLE);
		createEReference(accessableEClass, ACCESSABLE__ACCESSING_RULES);

		accessTypeEClass = createEClass(ACCESS_TYPE);

		anyAccessTypeEClass = createEClass(ANY_ACCESS_TYPE);

		referAccessTypeEClass = createEClass(REFER_ACCESS_TYPE);

		accessAccessTypeEClass = createEClass(ACCESS_ACCESS_TYPE);

		specializeAccessTypeEClass = createEClass(SPECIALIZE_ACCESS_TYPE);

		instantiateAccessTypeEClass = createEClass(INSTANTIATE_ACCESS_TYPE);

		readAccessTypeEClass = createEClass(READ_ACCESS_TYPE);

		writeAccessTypeEClass = createEClass(WRITE_ACCESS_TYPE);

		callAccessTypeEClass = createEClass(CALL_ACCESS_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationPackage theSpecificationPackage = (SpecificationPackage) EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI);
		CorePackage theCorePackage = (CorePackage) EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		patternEnvironmentEClass.getESuperTypes().add(theSpecificationPackage.getPatternElement());
		patternEnvironmentEClass.getESuperTypes().add(this.getAccessable());
		accessRuleEClass.getESuperTypes().add(theSpecificationPackage.getPatternElement());
		accessRuleEClass.getESuperTypes().add(theCorePackage.getCommentable());
		accessTypeEClass.getESuperTypes().add(theCorePackage.getNamed());
		accessTypeEClass.getESuperTypes().add(theCorePackage.getCommentable());
		anyAccessTypeEClass.getESuperTypes().add(this.getAccessType());
		referAccessTypeEClass.getESuperTypes().add(this.getAnyAccessType());
		accessAccessTypeEClass.getESuperTypes().add(this.getAnyAccessType());
		specializeAccessTypeEClass.getESuperTypes().add(this.getReferAccessType());
		instantiateAccessTypeEClass.getESuperTypes().add(this.getReferAccessType());
		readAccessTypeEClass.getESuperTypes().add(this.getAccessAccessType());
		writeAccessTypeEClass.getESuperTypes().add(this.getAccessAccessType());
		callAccessTypeEClass.getESuperTypes().add(this.getAccessAccessType());

		// Initialize classes, features, and operations; add parameters
		initEClass(patternEnvironmentEClass, PatternEnvironment.class,
				"PatternEnvironment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(accessRuleEClass, AccessRule.class,
				"AccessRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getAccessRule_AccessedElement(),
				this.getAccessable(),
				this.getAccessable_AccessingRules(),
				"accessedElement", null, 1, 1, AccessRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getAccessRule_Accessor(),
				theSpecificationPackage.getPatternElement(),
				theSpecificationPackage.getPatternElement_AccessorRules(),
				"accessor", null, 1, 1, AccessRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getAccessRule_Forbidden(),
				ecorePackage.getEBoolean(),
				"forbidden", "false", 1, 1, AccessRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(
				getAccessRule_AccessType(),
				this.getAccessType(),
				null,
				"accessType", null, 1, 1, AccessRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(accessableEClass, Accessable.class,
				"Accessable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getAccessable_AccessingRules(),
				this.getAccessRule(),
				this.getAccessRule_AccessedElement(),
				"accessingRules", null, 0, -1, Accessable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(accessTypeEClass, AccessType.class,
				"AccessType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(anyAccessTypeEClass, AnyAccessType.class,
				"AnyAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(referAccessTypeEClass, ReferAccessType.class,
				"ReferAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(accessAccessTypeEClass, AccessAccessType.class,
				"AccessAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(specializeAccessTypeEClass, SpecializeAccessType.class,
				"SpecializeAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(instantiateAccessTypeEClass, InstantiateAccessType.class,
				"InstantiateAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(readAccessTypeEClass, ReadAccessType.class,
				"ReadAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(writeAccessTypeEClass, WriteAccessType.class,
				"WriteAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(callAccessTypeEClass, CallAccessType.class,
				"CallAccessType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] {
				"documentation", "This package contains all elements that are related to access constraining." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				patternEnvironmentEClass,
				source,
				new String[] {
						"documentation", "This represents the environment of a pattern implementation, i.e. the software system where a pattern is applied without its parts playing any role in the specified pattern." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				accessRuleEClass,
				source,
				new String[] {
						"documentation", "This represents a constraint concerning the coupling of software design elements. An access rule specifies that a certain design element has access to (is coupled with) a certain design element or is not allowed to do so." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getAccessRule_AccessedElement(), source, new String[] {
				"documentation", "The accessable element which this rule constraints." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getAccessRule_Accessor(), source, new String[] {
				"documentation", "The pattern element for which this access rule is used - the accessor." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				accessableEClass,
				source,
				new String[] {
						"documentation", "This is the interface for all elements that could be accessed according to an access rule." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(accessTypeEClass, source, new String[] {
				"documentation", "This is the base class for all types of access used in access rules." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				anyAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents any kind of access to (or coupling with) a design element, e.g. if a type uses another type and thus has a compilation dependence." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				referAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents any kind of reference to a design element, e.g. a type uses another type as parameter type in one of its operations or is a subtype of another type." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				accessAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents any kind of access to a design element, e.g. read, write, or call access to  an operation, a reference, or an attribute." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				specializeAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents a specialization of a design element, e.g. a type is a subtype of another type or an operation overrides another operation or implements an operation defined in an interface." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				instantiateAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents an instantiation of a design element, e.g. a new object of a certain type is created, i.e. a type is instantiated." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				readAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents a read access to a design element, e.g. reading access to a reference or attribute." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				writeAccessTypeEClass,
				source,
				new String[] {
						"documentation", "This represents a write access to a design element, e.g. reading access to a reference or attribute." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(callAccessTypeEClass, source, new String[] {
				"documentation", "This represents a call access to a design element, e.g. calling an operation." //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

} //AccessPackageImpl
