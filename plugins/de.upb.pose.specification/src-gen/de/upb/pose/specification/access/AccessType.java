/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Named;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the base class for all types of access used in access rules.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.access.AccessPackage#getAccessType()
 * @generated
 */
public interface AccessType extends Named, Commentable {
} // AccessType
