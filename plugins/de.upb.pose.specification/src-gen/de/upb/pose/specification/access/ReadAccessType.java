/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Read Access Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a read access to a design element, e.g. reading access to a reference or attribute.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.access.AccessPackage#getReadAccessType()
 * @generated
 */
public interface ReadAccessType extends AccessAccessType {
} // ReadAccessType
