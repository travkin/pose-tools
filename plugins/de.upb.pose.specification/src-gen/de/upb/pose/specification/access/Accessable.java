/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Accessable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is the interface for all elements that could be accessed according to an access rule.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.access.Accessable#getAccessingRules <em>Accessing Rules</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.access.AccessPackage#getAccessable()
 * @generated
 */
public interface Accessable extends EObject {
	/**
	 * Returns the value of the '<em><b>Accessing Rules</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.access.AccessRule}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.access.AccessRule#getAccessedElement <em>Accessed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accessing Rules</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accessing Rules</em>' reference list.
	 * @see de.upb.pose.specification.access.AccessPackage#getAccessable_AccessingRules()
	 * @see de.upb.pose.specification.access.AccessRule#getAccessedElement
	 * @generated
	 */
	EList<AccessRule> getAccessingRules();

} // Accessable
