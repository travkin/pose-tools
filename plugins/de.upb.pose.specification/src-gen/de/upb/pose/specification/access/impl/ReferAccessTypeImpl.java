/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access.impl;

import org.eclipse.emf.ecore.EClass;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.ReferAccessType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Refer Access Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ReferAccessTypeImpl extends AnyAccessTypeImpl implements ReferAccessType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferAccessTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AccessPackage.Literals.REFER_ACCESS_TYPE;
	}

} //ReferAccessTypeImpl
