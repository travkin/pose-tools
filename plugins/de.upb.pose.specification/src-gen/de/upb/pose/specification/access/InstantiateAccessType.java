/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instantiate Access Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents an instantiation of a design element, e.g. a new object of a certain type is created, i.e. a type is instantiated.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.access.AccessPackage#getInstantiateAccessType()
 * @generated
 */
public interface InstantiateAccessType extends ReferAccessType {
} // InstantiateAccessType
