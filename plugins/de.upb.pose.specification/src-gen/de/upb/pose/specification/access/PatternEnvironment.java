/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.access;

import de.upb.pose.specification.PatternElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pattern Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents the environment of a pattern implementation, i.e. the software system where a pattern is applied without its parts playing any role in the specified pattern.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.access.AccessPackage#getPatternEnvironment()
 * @generated
 */
public interface PatternEnvironment extends PatternElement, Accessable {
} // PatternEnvironment
