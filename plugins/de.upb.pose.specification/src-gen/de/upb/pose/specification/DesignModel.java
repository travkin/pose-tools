/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Represents a design model that can contain several design elements. A design model represents the root element of a software model where a pattern is to be applied.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.DesignModel#getDesignElements <em>Design Elements</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.SpecificationPackage#getDesignModel()
 * @generated
 */
public interface DesignModel extends PatternElement {
	/**
	 * Returns the value of the '<em><b>Design Elements</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.DesignElement}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.DesignElement#getDesignModel <em>Design Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The elements that are contained in this design model.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Design Elements</em>' containment reference list.
	 * @see de.upb.pose.specification.SpecificationPackage#getDesignModel_DesignElements()
	 * @see de.upb.pose.specification.DesignElement#getDesignModel
	 * @generated
	 */
	EList<DesignElement> getDesignElements();

} // DesignModel
