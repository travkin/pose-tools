/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.subsystems;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Nothing has been documented yet.
 * <!-- end-model-doc -->
 * @see de.upb.pose.specification.subsystems.SubsystemsFactory
 * @generated
 */
public interface SubsystemsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "subsystems"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni-paderborn.de/pose/specification/subsystems"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "subsystems"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SubsystemsPackage eINSTANCE = de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.subsystems.impl.SystemImpl <em>System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.subsystems.impl.SystemImpl
	 * @see de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl#getSystem()
	 * @generated
	 */
	int SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__ID = SpecificationPackage.DESIGN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__ANNOTATIONS = SpecificationPackage.DESIGN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__NAME = SpecificationPackage.DESIGN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__SPECIFICATION = SpecificationPackage.DESIGN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__CAN_BE_GENERATED = SpecificationPackage.DESIGN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__ACCESSOR_RULES = SpecificationPackage.DESIGN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__CONTAINING_SETS = SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__DESIGN_MODEL = SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__TASKS = SpecificationPackage.DESIGN_ELEMENT__TASKS;

	/**
	 * The number of structural features of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_COUNT = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM___GET_ANNOTATION__STRING = SpecificationPackage.DESIGN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_OPERATION_COUNT = SpecificationPackage.DESIGN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl <em>Subsystem</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.subsystems.impl.SubsystemImpl
	 * @see de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl#getSubsystem()
	 * @generated
	 */
	int SUBSYSTEM = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__ID = SYSTEM__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__ANNOTATIONS = SYSTEM__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__NAME = SYSTEM__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__SPECIFICATION = SYSTEM__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__CAN_BE_GENERATED = SYSTEM__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__ACCESSOR_RULES = SYSTEM__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__CONTAINING_SETS = SYSTEM__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__DESIGN_MODEL = SYSTEM__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__TASKS = SYSTEM__TASKS;

	/**
	 * The feature id for the '<em><b>Accessing Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__ACCESSING_RULES = SYSTEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Contained Subsystems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__CONTAINED_SUBSYSTEMS = SYSTEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Subsystem</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__PARENT_SUBSYSTEM = SYSTEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contained Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM__CONTAINED_TYPES = SYSTEM_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Subsystem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM_FEATURE_COUNT = SYSTEM_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM___GET_ANNOTATION__STRING = SYSTEM___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Subsystem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSYSTEM_OPERATION_COUNT = SYSTEM_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.subsystems.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System</em>'.
	 * @see de.upb.pose.specification.subsystems.System
	 * @generated
	 */
	EClass getSystem();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.subsystems.Subsystem <em>Subsystem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subsystem</em>'.
	 * @see de.upb.pose.specification.subsystems.Subsystem
	 * @generated
	 */
	EClass getSubsystem();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.subsystems.Subsystem#getContainedSubsystems <em>Contained Subsystems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contained Subsystems</em>'.
	 * @see de.upb.pose.specification.subsystems.Subsystem#getContainedSubsystems()
	 * @see #getSubsystem()
	 * @generated
	 */
	EReference getSubsystem_ContainedSubsystems();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem <em>Parent Subsystem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Subsystem</em>'.
	 * @see de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem()
	 * @see #getSubsystem()
	 * @generated
	 */
	EReference getSubsystem_ParentSubsystem();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.pose.specification.subsystems.Subsystem#getContainedTypes <em>Contained Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contained Types</em>'.
	 * @see de.upb.pose.specification.subsystems.Subsystem#getContainedTypes()
	 * @see #getSubsystem()
	 * @generated
	 */
	EReference getSubsystem_ContainedTypes();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SubsystemsFactory getSubsystemsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.subsystems.impl.SystemImpl <em>System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.subsystems.impl.SystemImpl
		 * @see de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl#getSystem()
		 * @generated
		 */
		EClass SYSTEM = eINSTANCE.getSystem();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl <em>Subsystem</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.subsystems.impl.SubsystemImpl
		 * @see de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl#getSubsystem()
		 * @generated
		 */
		EClass SUBSYSTEM = eINSTANCE.getSubsystem();

		/**
		 * The meta object literal for the '<em><b>Contained Subsystems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSYSTEM__CONTAINED_SUBSYSTEMS = eINSTANCE.getSubsystem_ContainedSubsystems();

		/**
		 * The meta object literal for the '<em><b>Parent Subsystem</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSYSTEM__PARENT_SUBSYSTEM = eINSTANCE.getSubsystem_ParentSubsystem();

		/**
		 * The meta object literal for the '<em><b>Contained Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSYSTEM__CONTAINED_TYPES = eINSTANCE.getSubsystem_ContainedTypes();

	}

} //SubsystemsPackage
