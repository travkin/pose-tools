/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.subsystems.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.impl.AccessPackageImpl;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.impl.ActionsPackageImpl;
import de.upb.pose.specification.impl.SpecificationPackageImpl;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.subsystems.SubsystemsFactory;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SubsystemsPackageImpl extends EPackageImpl implements SubsystemsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass subsystemEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SubsystemsPackageImpl() {
		super(eNS_URI, SubsystemsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SubsystemsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SubsystemsPackage init() {
		if (isInited)
			return (SubsystemsPackage) EPackage.Registry.INSTANCE.getEPackage(SubsystemsPackage.eNS_URI);

		// Obtain or create and register package
		SubsystemsPackageImpl theSubsystemsPackage = (SubsystemsPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SubsystemsPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new SubsystemsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SpecificationPackageImpl theSpecificationPackage = (SpecificationPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) instanceof SpecificationPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) : SpecificationPackage.eINSTANCE);
		AccessPackageImpl theAccessPackage = (AccessPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) instanceof AccessPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) : AccessPackage.eINSTANCE);
		ActionsPackageImpl theActionsPackage = (ActionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) instanceof ActionsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(ActionsPackage.eNS_URI) : ActionsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theSubsystemsPackage.createPackageContents();
		theSpecificationPackage.createPackageContents();
		theAccessPackage.createPackageContents();
		theActionsPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theSubsystemsPackage.initializePackageContents();
		theSpecificationPackage.initializePackageContents();
		theAccessPackage.initializePackageContents();
		theActionsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSubsystemsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SubsystemsPackage.eNS_URI, theSubsystemsPackage);
		return theSubsystemsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystem() {
		return systemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSubsystem() {
		return subsystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsystem_ContainedSubsystems() {
		return (EReference) subsystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsystem_ParentSubsystem() {
		return (EReference) subsystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSubsystem_ContainedTypes() {
		return (EReference) subsystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubsystemsFactory getSubsystemsFactory() {
		return (SubsystemsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		systemEClass = createEClass(SYSTEM);

		subsystemEClass = createEClass(SUBSYSTEM);
		createEReference(subsystemEClass, SUBSYSTEM__CONTAINED_SUBSYSTEMS);
		createEReference(subsystemEClass, SUBSYSTEM__PARENT_SUBSYSTEM);
		createEReference(subsystemEClass, SUBSYSTEM__CONTAINED_TYPES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationPackage theSpecificationPackage = (SpecificationPackage) EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI);
		AccessPackage theAccessPackage = (AccessPackage) EPackage.Registry.INSTANCE.getEPackage(AccessPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		systemEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		subsystemEClass.getESuperTypes().add(this.getSystem());
		subsystemEClass.getESuperTypes().add(theAccessPackage.getAccessable());

		// Initialize classes, features, and operations; add parameters
		initEClass(systemEClass, de.upb.pose.specification.subsystems.System.class,
				"System", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(subsystemEClass, Subsystem.class,
				"Subsystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSubsystem_ContainedSubsystems(),
				this.getSubsystem(),
				this.getSubsystem_ParentSubsystem(),
				"containedSubsystems", null, 0, -1, Subsystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSubsystem_ParentSubsystem(),
				this.getSubsystem(),
				this.getSubsystem_ContainedSubsystems(),
				"parentSubsystem", null, 1, 1, Subsystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSubsystem_ContainedTypes(),
				theTypesPackage.getType(),
				null,
				"containedTypes", null, 0, -1, Subsystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "documentation", "Nothing has been documented yet." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(systemEClass, source, new String[] {
				"documentation", "This represents a system in a software design." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(subsystemEClass, source, new String[] {
				"documentation", "This represents a part of a software system which is defined by a set of types." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSubsystem_ContainedSubsystems(), source, new String[] {
				"documentation", "Nothing has been documented yet." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSubsystem_ParentSubsystem(), source, new String[] {
				"documentation", "Nothing has been documented yet." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSubsystem_ContainedTypes(), source, new String[] {
				"documentation", "Nothing has been documented yet." //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

} //SubsystemsPackageImpl
