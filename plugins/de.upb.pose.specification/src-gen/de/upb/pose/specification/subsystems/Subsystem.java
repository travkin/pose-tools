/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.subsystems;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subsystem</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a part of a software system which is defined by a set of types.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.subsystems.Subsystem#getContainedSubsystems <em>Contained Subsystems</em>}</li>
 *   <li>{@link de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem <em>Parent Subsystem</em>}</li>
 *   <li>{@link de.upb.pose.specification.subsystems.Subsystem#getContainedTypes <em>Contained Types</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#getSubsystem()
 * @generated
 */
public interface Subsystem extends de.upb.pose.specification.subsystems.System, Accessable {
	/**
	 * Returns the value of the '<em><b>Contained Subsystems</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.subsystems.Subsystem}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem <em>Parent Subsystem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Nothing has been documented yet.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Contained Subsystems</em>' containment reference list.
	 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#getSubsystem_ContainedSubsystems()
	 * @see de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem
	 * @generated
	 */
	EList<Subsystem> getContainedSubsystems();

	/**
	 * Returns the value of the '<em><b>Parent Subsystem</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.subsystems.Subsystem#getContainedSubsystems <em>Contained Subsystems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Nothing has been documented yet.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parent Subsystem</em>' container reference.
	 * @see #setParentSubsystem(Subsystem)
	 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#getSubsystem_ParentSubsystem()
	 * @see de.upb.pose.specification.subsystems.Subsystem#getContainedSubsystems
	 * @generated
	 */
	Subsystem getParentSubsystem();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.subsystems.Subsystem#getParentSubsystem <em>Parent Subsystem</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Subsystem</em>' container reference.
	 * @see #getParentSubsystem()
	 * @generated
	 */
	void setParentSubsystem(Subsystem value);

	/**
	 * Returns the value of the '<em><b>Contained Types</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.specification.types.Type}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Nothing has been documented yet.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Contained Types</em>' reference list.
	 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#getSubsystem_ContainedTypes()
	 * @generated
	 */
	EList<Type> getContainedTypes();

} // Subsystem
