/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.subsystems.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.subsystems.Subsystem;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.types.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Subsystem</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl#getAccessingRules <em>Accessing Rules</em>}</li>
 *   <li>{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl#getContainedSubsystems <em>Contained Subsystems</em>}</li>
 *   <li>{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl#getParentSubsystem <em>Parent Subsystem</em>}</li>
 *   <li>{@link de.upb.pose.specification.subsystems.impl.SubsystemImpl#getContainedTypes <em>Contained Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SubsystemImpl extends SystemImpl implements Subsystem {
	/**
	 * The cached value of the '{@link #getAccessingRules() <em>Accessing Rules</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessingRules()
	 * @generated
	 * @ordered
	 */
	protected EList<AccessRule> accessingRules;

	/**
	 * The cached value of the '{@link #getContainedSubsystems() <em>Contained Subsystems</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedSubsystems()
	 * @generated
	 * @ordered
	 */
	protected EList<Subsystem> containedSubsystems;

	/**
	 * The cached value of the '{@link #getContainedTypes() <em>Contained Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> containedTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubsystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SubsystemsPackage.Literals.SUBSYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessRule> getAccessingRules() {
		if (accessingRules == null) {
			accessingRules = new EObjectWithInverseResolvingEList<AccessRule>(AccessRule.class, this,
					SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES, AccessPackage.ACCESS_RULE__ACCESSED_ELEMENT);
		}
		return accessingRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Subsystem> getContainedSubsystems() {
		if (containedSubsystems == null) {
			containedSubsystems = new EObjectContainmentWithInverseEList<Subsystem>(Subsystem.class, this,
					SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS, SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM);
		}
		return containedSubsystems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Subsystem getParentSubsystem() {
		if (eContainerFeatureID() != SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM)
			return null;
		return (Subsystem) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentSubsystem(Subsystem newParentSubsystem, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newParentSubsystem, SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentSubsystem(Subsystem newParentSubsystem) {
		if (newParentSubsystem != eInternalContainer()
				|| (eContainerFeatureID() != SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM && newParentSubsystem != null)) {
			if (EcoreUtil.isAncestor(this, newParentSubsystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentSubsystem != null)
				msgs = ((InternalEObject) newParentSubsystem).eInverseAdd(this,
						SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS, Subsystem.class, msgs);
			msgs = basicSetParentSubsystem(newParentSubsystem, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM,
					newParentSubsystem, newParentSubsystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getContainedTypes() {
		if (containedTypes == null) {
			containedTypes = new EObjectResolvingEList<Type>(Type.class, this,
					SubsystemsPackage.SUBSYSTEM__CONTAINED_TYPES);
		}
		return containedTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAccessingRules()).basicAdd(otherEnd, msgs);
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContainedSubsystems()).basicAdd(otherEnd,
					msgs);
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetParentSubsystem((Subsystem) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			return ((InternalEList<?>) getAccessingRules()).basicRemove(otherEnd, msgs);
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			return ((InternalEList<?>) getContainedSubsystems()).basicRemove(otherEnd, msgs);
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			return basicSetParentSubsystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			return eInternalContainer().eInverseRemove(this, SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS,
					Subsystem.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			return getAccessingRules();
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			return getContainedSubsystems();
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			return getParentSubsystem();
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_TYPES:
			return getContainedTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			getAccessingRules().clear();
			getAccessingRules().addAll((Collection<? extends AccessRule>) newValue);
			return;
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			getContainedSubsystems().clear();
			getContainedSubsystems().addAll((Collection<? extends Subsystem>) newValue);
			return;
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			setParentSubsystem((Subsystem) newValue);
			return;
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_TYPES:
			getContainedTypes().clear();
			getContainedTypes().addAll((Collection<? extends Type>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			getAccessingRules().clear();
			return;
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			getContainedSubsystems().clear();
			return;
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			setParentSubsystem((Subsystem) null);
			return;
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_TYPES:
			getContainedTypes().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
			return accessingRules != null && !accessingRules.isEmpty();
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_SUBSYSTEMS:
			return containedSubsystems != null && !containedSubsystems.isEmpty();
		case SubsystemsPackage.SUBSYSTEM__PARENT_SUBSYSTEM:
			return getParentSubsystem() != null;
		case SubsystemsPackage.SUBSYSTEM__CONTAINED_TYPES:
			return containedTypes != null && !containedTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (derivedFeatureID) {
			case SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES:
				return AccessPackage.ACCESSABLE__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Accessable.class) {
			switch (baseFeatureID) {
			case AccessPackage.ACCESSABLE__ACCESSING_RULES:
				return SubsystemsPackage.SUBSYSTEM__ACCESSING_RULES;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //SubsystemImpl
