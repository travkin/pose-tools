/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.subsystems;

import de.upb.pose.specification.DesignElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This represents a system in a software design.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.subsystems.SubsystemsPackage#getSystem()
 * @generated
 */
public interface System extends DesignElement {
} // System
