/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Access Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that accesses a variable.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.VariableAccessAction#getAccessedVariable <em>Accessed Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.VariableAccessAction#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getVariableAccessAction()
 * @generated
 */
public interface VariableAccessAction extends Action {
	/**
	 * Returns the value of the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The variable that is accessed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Accessed Variable</em>' reference.
	 * @see #setAccessedVariable(Variable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getVariableAccessAction_AccessedVariable()
	 * @generated
	 */
	Variable getAccessedVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.VariableAccessAction#getAccessedVariable <em>Accessed Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accessed Variable</em>' reference.
	 * @see #getAccessedVariable()
	 * @generated
	 */
	void setAccessedVariable(Variable value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Variable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getVariableAccessAction_Target()
	 * @generated
	 */
	Variable getTarget();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.VariableAccessAction#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Variable value);

} // VariableAccessAction
