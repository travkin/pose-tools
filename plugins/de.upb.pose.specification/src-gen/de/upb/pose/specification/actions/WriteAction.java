/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Write Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that writes the value of a variable to another one.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.WriteAction#getAssignedVariable <em>Assigned Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getWriteAction()
 * @generated
 */
public interface WriteAction extends VariableAccessAction {
	/**
	 * Returns the value of the '<em><b>Assigned Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The variable which will be assigned to the accessed variable of this write action.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Assigned Variable</em>' reference.
	 * @see #setAssignedVariable(Variable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getWriteAction_AssignedVariable()
	 * @generated
	 */
	Variable getAssignedVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.WriteAction#getAssignedVariable <em>Assigned Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned Variable</em>' reference.
	 * @see #getAssignedVariable()
	 * @generated
	 */
	void setAssignedVariable(Variable value);

} // WriteAction
