/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.ecore.EClass;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.DelegateAction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Delegate Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DelegateActionImpl extends RedirectActionImpl implements DelegateAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DelegateActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.DELEGATE_ACTION;
	}

} //DelegateActionImpl
