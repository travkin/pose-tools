/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

import de.upb.pose.specification.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that creates an instance of a type. The new instance is stored as result in the result variable.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.CreateAction#getInstantiatedType <em>Instantiated Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getCreateAction()
 * @generated
 */
public interface CreateAction extends ResultAction {
	/**
	 * Returns the value of the '<em><b>Instantiated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The type that is instantiated by this action.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Instantiated Type</em>' reference.
	 * @see #setInstantiatedType(Type)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getCreateAction_InstantiatedType()
	 * @generated
	 */
	Type getInstantiatedType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.CreateAction#getInstantiatedType <em>Instantiated Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instantiated Type</em>' reference.
	 * @see #getInstantiatedType()
	 * @generated
	 */
	void setInstantiatedType(Type value);

} // CreateAction
