/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import de.upb.pose.specification.SpecificationPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * This package contains all elements that are related to pattern actions/behavior.
 * <!-- end-model-doc -->
 * @see de.upb.pose.specification.actions.ActionsFactory
 * @generated
 */
public interface ActionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "actions"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uni-paderborn.de/pose/specification/actions"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "actions"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ActionsPackage eINSTANCE = de.upb.pose.specification.actions.impl.ActionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 0;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ID = SpecificationPackage.DESIGN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ANNOTATIONS = SpecificationPackage.DESIGN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__NAME = SpecificationPackage.DESIGN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__SPECIFICATION = SpecificationPackage.DESIGN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__CAN_BE_GENERATED = SpecificationPackage.DESIGN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACCESSOR_RULES = SpecificationPackage.DESIGN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__CONTAINING_SETS = SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESIGN_MODEL = SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TASKS = SpecificationPackage.DESIGN_ELEMENT__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__OPERATION = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___GET_ANNOTATION__STRING = SpecificationPackage.DESIGN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = SpecificationPackage.DESIGN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ResultActionImpl <em>Result Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ResultActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getResultAction()
	 * @generated
	 */
	int RESULT_ACTION = 1;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__ID = ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__ANNOTATIONS = ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__NAME = ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__SPECIFICATION = ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__CAN_BE_GENERATED = ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__ACCESSOR_RULES = ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__CONTAINING_SETS = ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__DESIGN_MODEL = ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__TASKS = ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__OPERATION = ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION__RESULT_VARIABLE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Result Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION___GET_ANNOTATION__STRING = ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Result Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.CreateActionImpl <em>Create Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.CreateActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getCreateAction()
	 * @generated
	 */
	int CREATE_ACTION = 2;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__ID = RESULT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__ANNOTATIONS = RESULT_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__NAME = RESULT_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__SPECIFICATION = RESULT_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__CAN_BE_GENERATED = RESULT_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__ACCESSOR_RULES = RESULT_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__CONTAINING_SETS = RESULT_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__DESIGN_MODEL = RESULT_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__TASKS = RESULT_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__OPERATION = RESULT_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__RESULT_VARIABLE = RESULT_ACTION__RESULT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Instantiated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION__INSTANTIATED_TYPE = RESULT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Create Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION_FEATURE_COUNT = RESULT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION___GET_ANNOTATION__STRING = RESULT_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Create Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ACTION_OPERATION_COUNT = RESULT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.CallActionImpl <em>Call Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.CallActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getCallAction()
	 * @generated
	 */
	int CALL_ACTION = 3;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__ID = RESULT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__ANNOTATIONS = RESULT_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__NAME = RESULT_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__SPECIFICATION = RESULT_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__CAN_BE_GENERATED = RESULT_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__ACCESSOR_RULES = RESULT_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__CONTAINING_SETS = RESULT_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__DESIGN_MODEL = RESULT_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__TASKS = RESULT_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__OPERATION = RESULT_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__RESULT_VARIABLE = RESULT_ACTION__RESULT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__ASSIGNMENTS = RESULT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Called Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__CALLED_OPERATION = RESULT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__TARGET = RESULT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>For All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__FOR_ALL = RESULT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_FEATURE_COUNT = RESULT_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION___GET_ANNOTATION__STRING = RESULT_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_OPERATION_COUNT = RESULT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.RedirectActionImpl <em>Redirect Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.RedirectActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getRedirectAction()
	 * @generated
	 */
	int REDIRECT_ACTION = 16;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__ID = CALL_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__ANNOTATIONS = CALL_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__NAME = CALL_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__SPECIFICATION = CALL_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__CAN_BE_GENERATED = CALL_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__ACCESSOR_RULES = CALL_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__CONTAINING_SETS = CALL_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__DESIGN_MODEL = CALL_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__TASKS = CALL_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__OPERATION = CALL_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__RESULT_VARIABLE = CALL_ACTION__RESULT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__ASSIGNMENTS = CALL_ACTION__ASSIGNMENTS;

	/**
	 * The feature id for the '<em><b>Called Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__CALLED_OPERATION = CALL_ACTION__CALLED_OPERATION;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__TARGET = CALL_ACTION__TARGET;

	/**
	 * The feature id for the '<em><b>For All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION__FOR_ALL = CALL_ACTION__FOR_ALL;

	/**
	 * The number of structural features of the '<em>Redirect Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION_FEATURE_COUNT = CALL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION___GET_ANNOTATION__STRING = CALL_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Redirect Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDIRECT_ACTION_OPERATION_COUNT = CALL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.DelegateActionImpl <em>Delegate Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.DelegateActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getDelegateAction()
	 * @generated
	 */
	int DELEGATE_ACTION = 4;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__ID = REDIRECT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__ANNOTATIONS = REDIRECT_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__NAME = REDIRECT_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__SPECIFICATION = REDIRECT_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__CAN_BE_GENERATED = REDIRECT_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__ACCESSOR_RULES = REDIRECT_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__CONTAINING_SETS = REDIRECT_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__DESIGN_MODEL = REDIRECT_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__TASKS = REDIRECT_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__OPERATION = REDIRECT_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__RESULT_VARIABLE = REDIRECT_ACTION__RESULT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Assignments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__ASSIGNMENTS = REDIRECT_ACTION__ASSIGNMENTS;

	/**
	 * The feature id for the '<em><b>Called Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__CALLED_OPERATION = REDIRECT_ACTION__CALLED_OPERATION;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__TARGET = REDIRECT_ACTION__TARGET;

	/**
	 * The feature id for the '<em><b>For All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION__FOR_ALL = REDIRECT_ACTION__FOR_ALL;

	/**
	 * The number of structural features of the '<em>Delegate Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION_FEATURE_COUNT = REDIRECT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION___GET_ANNOTATION__STRING = REDIRECT_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Delegate Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELEGATE_ACTION_OPERATION_COUNT = REDIRECT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.VariableAccessActionImpl <em>Variable Access Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.VariableAccessActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getVariableAccessAction()
	 * @generated
	 */
	int VARIABLE_ACCESS_ACTION = 5;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__ID = ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__ANNOTATIONS = ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__NAME = ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__SPECIFICATION = ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__CAN_BE_GENERATED = ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__ACCESSOR_RULES = ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__CONTAINING_SETS = ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__DESIGN_MODEL = ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__TASKS = ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__OPERATION = ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION__TARGET = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Variable Access Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION___GET_ANNOTATION__STRING = ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Variable Access Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_ACCESS_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.WriteActionImpl <em>Write Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.WriteActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getWriteAction()
	 * @generated
	 */
	int WRITE_ACTION = 6;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__ID = VARIABLE_ACCESS_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__ANNOTATIONS = VARIABLE_ACCESS_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__NAME = VARIABLE_ACCESS_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__SPECIFICATION = VARIABLE_ACCESS_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__CAN_BE_GENERATED = VARIABLE_ACCESS_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__ACCESSOR_RULES = VARIABLE_ACCESS_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__CONTAINING_SETS = VARIABLE_ACCESS_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__DESIGN_MODEL = VARIABLE_ACCESS_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__TASKS = VARIABLE_ACCESS_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__OPERATION = VARIABLE_ACCESS_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__ACCESSED_VARIABLE = VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__TARGET = VARIABLE_ACCESS_ACTION__TARGET;

	/**
	 * The feature id for the '<em><b>Assigned Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION__ASSIGNED_VARIABLE = VARIABLE_ACCESS_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Write Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION_FEATURE_COUNT = VARIABLE_ACCESS_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION___GET_ANNOTATION__STRING = VARIABLE_ACCESS_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Write Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WRITE_ACTION_OPERATION_COUNT = VARIABLE_ACCESS_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.DeleteActionImpl <em>Delete Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.DeleteActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getDeleteAction()
	 * @generated
	 */
	int DELETE_ACTION = 7;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__ID = WRITE_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__ANNOTATIONS = WRITE_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__NAME = WRITE_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__SPECIFICATION = WRITE_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__CAN_BE_GENERATED = WRITE_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__ACCESSOR_RULES = WRITE_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__CONTAINING_SETS = WRITE_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__DESIGN_MODEL = WRITE_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__TASKS = WRITE_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__OPERATION = WRITE_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__ACCESSED_VARIABLE = WRITE_ACTION__ACCESSED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__TARGET = WRITE_ACTION__TARGET;

	/**
	 * The feature id for the '<em><b>Assigned Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION__ASSIGNED_VARIABLE = WRITE_ACTION__ASSIGNED_VARIABLE;

	/**
	 * The number of structural features of the '<em>Delete Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION_FEATURE_COUNT = WRITE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION___GET_ANNOTATION__STRING = WRITE_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Delete Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELETE_ACTION_OPERATION_COUNT = WRITE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ReadActionImpl <em>Read Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ReadActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getReadAction()
	 * @generated
	 */
	int READ_ACTION = 8;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__ID = VARIABLE_ACCESS_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__ANNOTATIONS = VARIABLE_ACCESS_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__NAME = VARIABLE_ACCESS_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__SPECIFICATION = VARIABLE_ACCESS_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__CAN_BE_GENERATED = VARIABLE_ACCESS_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__ACCESSOR_RULES = VARIABLE_ACCESS_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__CONTAINING_SETS = VARIABLE_ACCESS_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__DESIGN_MODEL = VARIABLE_ACCESS_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__TASKS = VARIABLE_ACCESS_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__OPERATION = VARIABLE_ACCESS_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__ACCESSED_VARIABLE = VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION__TARGET = VARIABLE_ACCESS_ACTION__TARGET;

	/**
	 * The number of structural features of the '<em>Read Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION_FEATURE_COUNT = VARIABLE_ACCESS_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION___GET_ANNOTATION__STRING = VARIABLE_ACCESS_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Read Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READ_ACTION_OPERATION_COUNT = VARIABLE_ACCESS_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ReturnActionImpl <em>Return Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ReturnActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getReturnAction()
	 * @generated
	 */
	int RETURN_ACTION = 9;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__ID = READ_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__ANNOTATIONS = READ_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__NAME = READ_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__SPECIFICATION = READ_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__CAN_BE_GENERATED = READ_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__ACCESSOR_RULES = READ_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__CONTAINING_SETS = READ_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__DESIGN_MODEL = READ_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__TASKS = READ_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__OPERATION = READ_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Accessed Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__ACCESSED_VARIABLE = READ_ACTION__ACCESSED_VARIABLE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION__TARGET = READ_ACTION__TARGET;

	/**
	 * The number of structural features of the '<em>Return Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION_FEATURE_COUNT = READ_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION___GET_ANNOTATION__STRING = READ_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Return Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_ACTION_OPERATION_COUNT = READ_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ParameterAssignmentImpl <em>Parameter Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ParameterAssignmentImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getParameterAssignment()
	 * @generated
	 */
	int PARAMETER_ASSIGNMENT = 10;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__ID = SpecificationPackage.DESIGN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__ANNOTATIONS = SpecificationPackage.DESIGN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__NAME = SpecificationPackage.DESIGN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__SPECIFICATION = SpecificationPackage.DESIGN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__CAN_BE_GENERATED = SpecificationPackage.DESIGN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__ACCESSOR_RULES = SpecificationPackage.DESIGN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__CONTAINING_SETS = SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__DESIGN_MODEL = SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__TASKS = SpecificationPackage.DESIGN_ELEMENT__TASKS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__ACTION = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assigned Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT__PARAMETER = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Parameter Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT_FEATURE_COUNT = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT___GET_ANNOTATION__STRING = SpecificationPackage.DESIGN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Parameter Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ASSIGNMENT_OPERATION_COUNT = SpecificationPackage.DESIGN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.Variable <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.Variable
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 11;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__ID = SpecificationPackage.DESIGN_ELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__ANNOTATIONS = SpecificationPackage.DESIGN_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = SpecificationPackage.DESIGN_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__SPECIFICATION = SpecificationPackage.DESIGN_ELEMENT__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__CAN_BE_GENERATED = SpecificationPackage.DESIGN_ELEMENT__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__ACCESSOR_RULES = SpecificationPackage.DESIGN_ELEMENT__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__CONTAINING_SETS = SpecificationPackage.DESIGN_ELEMENT__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__DESIGN_MODEL = SpecificationPackage.DESIGN_ELEMENT__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__TASKS = SpecificationPackage.DESIGN_ELEMENT__TASKS;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = SpecificationPackage.DESIGN_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___GET_ANNOTATION__STRING = SpecificationPackage.DESIGN_ELEMENT___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OPERATION_COUNT = SpecificationPackage.DESIGN_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.SelfVariableImpl <em>Self Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.SelfVariableImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getSelfVariable()
	 * @generated
	 */
	int SELF_VARIABLE = 12;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__ANNOTATIONS = VARIABLE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__SPECIFICATION = VARIABLE__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__CAN_BE_GENERATED = VARIABLE__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__ACCESSOR_RULES = VARIABLE__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__CONTAINING_SETS = VARIABLE__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__DESIGN_MODEL = VARIABLE__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__TASKS = VARIABLE__TASKS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE__TYPE = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Self Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE___GET_ANNOTATION__STRING = VARIABLE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Self Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ResultVariableImpl <em>Result Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ResultVariableImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getResultVariable()
	 * @generated
	 */
	int RESULT_VARIABLE = 13;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__ANNOTATIONS = VARIABLE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__SPECIFICATION = VARIABLE__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__CAN_BE_GENERATED = VARIABLE__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__ACCESSOR_RULES = VARIABLE__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__CONTAINING_SETS = VARIABLE__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__DESIGN_MODEL = VARIABLE__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__TASKS = VARIABLE__TASKS;

	/**
	 * The feature id for the '<em><b>Action</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE__ACTION = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Result Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE___GET_ANNOTATION__STRING = VARIABLE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Result Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.NullVariableImpl <em>Null Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.NullVariableImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getNullVariable()
	 * @generated
	 */
	int NULL_VARIABLE = 14;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__ANNOTATIONS = VARIABLE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__SPECIFICATION = VARIABLE__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__CAN_BE_GENERATED = VARIABLE__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__ACCESSOR_RULES = VARIABLE__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__CONTAINING_SETS = VARIABLE__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__DESIGN_MODEL = VARIABLE__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE__TASKS = VARIABLE__TASKS;

	/**
	 * The number of structural features of the '<em>Null Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE___GET_ANNOTATION__STRING = VARIABLE___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Null Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NULL_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.pose.specification.actions.impl.ProduceActionImpl <em>Produce Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.pose.specification.actions.impl.ProduceActionImpl
	 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getProduceAction()
	 * @generated
	 */
	int PRODUCE_ACTION = 15;

	/**
	 * The feature id for the '<em><b>ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__ID = CREATE_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__ANNOTATIONS = CREATE_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__NAME = CREATE_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__SPECIFICATION = CREATE_ACTION__SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Can Be Generated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__CAN_BE_GENERATED = CREATE_ACTION__CAN_BE_GENERATED;

	/**
	 * The feature id for the '<em><b>Accessor Rules</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__ACCESSOR_RULES = CREATE_ACTION__ACCESSOR_RULES;

	/**
	 * The feature id for the '<em><b>Containing Sets</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__CONTAINING_SETS = CREATE_ACTION__CONTAINING_SETS;

	/**
	 * The feature id for the '<em><b>Design Model</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__DESIGN_MODEL = CREATE_ACTION__DESIGN_MODEL;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__TASKS = CREATE_ACTION__TASKS;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__OPERATION = CREATE_ACTION__OPERATION;

	/**
	 * The feature id for the '<em><b>Result Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__RESULT_VARIABLE = CREATE_ACTION__RESULT_VARIABLE;

	/**
	 * The feature id for the '<em><b>Instantiated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION__INSTANTIATED_TYPE = CREATE_ACTION__INSTANTIATED_TYPE;

	/**
	 * The number of structural features of the '<em>Produce Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION_FEATURE_COUNT = CREATE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Annotation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION___GET_ANNOTATION__STRING = CREATE_ACTION___GET_ANNOTATION__STRING;

	/**
	 * The number of operations of the '<em>Produce Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRODUCE_ACTION_OPERATION_COUNT = CREATE_ACTION_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see de.upb.pose.specification.actions.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.actions.Action#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Operation</em>'.
	 * @see de.upb.pose.specification.actions.Action#getOperation()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Operation();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ResultAction <em>Result Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Result Action</em>'.
	 * @see de.upb.pose.specification.actions.ResultAction
	 * @generated
	 */
	EClass getResultAction();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.pose.specification.actions.ResultAction#getResultVariable <em>Result Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Result Variable</em>'.
	 * @see de.upb.pose.specification.actions.ResultAction#getResultVariable()
	 * @see #getResultAction()
	 * @generated
	 */
	EReference getResultAction_ResultVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.CreateAction <em>Create Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Action</em>'.
	 * @see de.upb.pose.specification.actions.CreateAction
	 * @generated
	 */
	EClass getCreateAction();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.CreateAction#getInstantiatedType <em>Instantiated Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Instantiated Type</em>'.
	 * @see de.upb.pose.specification.actions.CreateAction#getInstantiatedType()
	 * @see #getCreateAction()
	 * @generated
	 */
	EReference getCreateAction_InstantiatedType();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Action</em>'.
	 * @see de.upb.pose.specification.actions.CallAction
	 * @generated
	 */
	EClass getCallAction();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.pose.specification.actions.CallAction#getAssignments <em>Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assignments</em>'.
	 * @see de.upb.pose.specification.actions.CallAction#getAssignments()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_Assignments();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.CallAction#getCalledOperation <em>Called Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Called Operation</em>'.
	 * @see de.upb.pose.specification.actions.CallAction#getCalledOperation()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_CalledOperation();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.CallAction#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.upb.pose.specification.actions.CallAction#getTarget()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_Target();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.pose.specification.actions.CallAction#isForAll <em>For All</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>For All</em>'.
	 * @see de.upb.pose.specification.actions.CallAction#isForAll()
	 * @see #getCallAction()
	 * @generated
	 */
	EAttribute getCallAction_ForAll();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.DelegateAction <em>Delegate Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delegate Action</em>'.
	 * @see de.upb.pose.specification.actions.DelegateAction
	 * @generated
	 */
	EClass getDelegateAction();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.VariableAccessAction <em>Variable Access Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Access Action</em>'.
	 * @see de.upb.pose.specification.actions.VariableAccessAction
	 * @generated
	 */
	EClass getVariableAccessAction();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.VariableAccessAction#getAccessedVariable <em>Accessed Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Accessed Variable</em>'.
	 * @see de.upb.pose.specification.actions.VariableAccessAction#getAccessedVariable()
	 * @see #getVariableAccessAction()
	 * @generated
	 */
	EReference getVariableAccessAction_AccessedVariable();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.VariableAccessAction#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.upb.pose.specification.actions.VariableAccessAction#getTarget()
	 * @see #getVariableAccessAction()
	 * @generated
	 */
	EReference getVariableAccessAction_Target();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.WriteAction <em>Write Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Write Action</em>'.
	 * @see de.upb.pose.specification.actions.WriteAction
	 * @generated
	 */
	EClass getWriteAction();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.WriteAction#getAssignedVariable <em>Assigned Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assigned Variable</em>'.
	 * @see de.upb.pose.specification.actions.WriteAction#getAssignedVariable()
	 * @see #getWriteAction()
	 * @generated
	 */
	EReference getWriteAction_AssignedVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.DeleteAction <em>Delete Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delete Action</em>'.
	 * @see de.upb.pose.specification.actions.DeleteAction
	 * @generated
	 */
	EClass getDeleteAction();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ReadAction <em>Read Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Read Action</em>'.
	 * @see de.upb.pose.specification.actions.ReadAction
	 * @generated
	 */
	EClass getReadAction();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ReturnAction <em>Return Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Action</em>'.
	 * @see de.upb.pose.specification.actions.ReturnAction
	 * @generated
	 */
	EClass getReturnAction();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ParameterAssignment <em>Parameter Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Assignment</em>'.
	 * @see de.upb.pose.specification.actions.ParameterAssignment
	 * @generated
	 */
	EClass getParameterAssignment();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.actions.ParameterAssignment#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Action</em>'.
	 * @see de.upb.pose.specification.actions.ParameterAssignment#getAction()
	 * @see #getParameterAssignment()
	 * @generated
	 */
	EReference getParameterAssignment_Action();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.ParameterAssignment#getAssignedVariable <em>Assigned Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assigned Variable</em>'.
	 * @see de.upb.pose.specification.actions.ParameterAssignment#getAssignedVariable()
	 * @see #getParameterAssignment()
	 * @generated
	 */
	EReference getParameterAssignment_AssignedVariable();

	/**
	 * Returns the meta object for the reference '{@link de.upb.pose.specification.actions.ParameterAssignment#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see de.upb.pose.specification.actions.ParameterAssignment#getParameter()
	 * @see #getParameterAssignment()
	 * @generated
	 */
	EReference getParameterAssignment_Parameter();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see de.upb.pose.specification.actions.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.SelfVariable <em>Self Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Self Variable</em>'.
	 * @see de.upb.pose.specification.actions.SelfVariable
	 * @generated
	 */
	EClass getSelfVariable();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.actions.SelfVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Type</em>'.
	 * @see de.upb.pose.specification.actions.SelfVariable#getType()
	 * @see #getSelfVariable()
	 * @generated
	 */
	EReference getSelfVariable_Type();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ResultVariable <em>Result Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Result Variable</em>'.
	 * @see de.upb.pose.specification.actions.ResultVariable
	 * @generated
	 */
	EClass getResultVariable();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.pose.specification.actions.ResultVariable#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Action</em>'.
	 * @see de.upb.pose.specification.actions.ResultVariable#getAction()
	 * @see #getResultVariable()
	 * @generated
	 */
	EReference getResultVariable_Action();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.NullVariable <em>Null Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Null Variable</em>'.
	 * @see de.upb.pose.specification.actions.NullVariable
	 * @generated
	 */
	EClass getNullVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.ProduceAction <em>Produce Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Produce Action</em>'.
	 * @see de.upb.pose.specification.actions.ProduceAction
	 * @generated
	 */
	EClass getProduceAction();

	/**
	 * Returns the meta object for class '{@link de.upb.pose.specification.actions.RedirectAction <em>Redirect Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Redirect Action</em>'.
	 * @see de.upb.pose.specification.actions.RedirectAction
	 * @generated
	 */
	EClass getRedirectAction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ActionsFactory getActionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__OPERATION = eINSTANCE.getAction_Operation();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ResultActionImpl <em>Result Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ResultActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getResultAction()
		 * @generated
		 */
		EClass RESULT_ACTION = eINSTANCE.getResultAction();

		/**
		 * The meta object literal for the '<em><b>Result Variable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESULT_ACTION__RESULT_VARIABLE = eINSTANCE.getResultAction_ResultVariable();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.CreateActionImpl <em>Create Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.CreateActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getCreateAction()
		 * @generated
		 */
		EClass CREATE_ACTION = eINSTANCE.getCreateAction();

		/**
		 * The meta object literal for the '<em><b>Instantiated Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CREATE_ACTION__INSTANTIATED_TYPE = eINSTANCE.getCreateAction_InstantiatedType();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.CallActionImpl <em>Call Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.CallActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getCallAction()
		 * @generated
		 */
		EClass CALL_ACTION = eINSTANCE.getCallAction();

		/**
		 * The meta object literal for the '<em><b>Assignments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__ASSIGNMENTS = eINSTANCE.getCallAction_Assignments();

		/**
		 * The meta object literal for the '<em><b>Called Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__CALLED_OPERATION = eINSTANCE.getCallAction_CalledOperation();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__TARGET = eINSTANCE.getCallAction_Target();

		/**
		 * The meta object literal for the '<em><b>For All</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_ACTION__FOR_ALL = eINSTANCE.getCallAction_ForAll();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.DelegateActionImpl <em>Delegate Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.DelegateActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getDelegateAction()
		 * @generated
		 */
		EClass DELEGATE_ACTION = eINSTANCE.getDelegateAction();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.VariableAccessActionImpl <em>Variable Access Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.VariableAccessActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getVariableAccessAction()
		 * @generated
		 */
		EClass VARIABLE_ACCESS_ACTION = eINSTANCE.getVariableAccessAction();

		/**
		 * The meta object literal for the '<em><b>Accessed Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE = eINSTANCE.getVariableAccessAction_AccessedVariable();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_ACCESS_ACTION__TARGET = eINSTANCE.getVariableAccessAction_Target();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.WriteActionImpl <em>Write Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.WriteActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getWriteAction()
		 * @generated
		 */
		EClass WRITE_ACTION = eINSTANCE.getWriteAction();

		/**
		 * The meta object literal for the '<em><b>Assigned Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WRITE_ACTION__ASSIGNED_VARIABLE = eINSTANCE.getWriteAction_AssignedVariable();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.DeleteActionImpl <em>Delete Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.DeleteActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getDeleteAction()
		 * @generated
		 */
		EClass DELETE_ACTION = eINSTANCE.getDeleteAction();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ReadActionImpl <em>Read Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ReadActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getReadAction()
		 * @generated
		 */
		EClass READ_ACTION = eINSTANCE.getReadAction();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ReturnActionImpl <em>Return Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ReturnActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getReturnAction()
		 * @generated
		 */
		EClass RETURN_ACTION = eINSTANCE.getReturnAction();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ParameterAssignmentImpl <em>Parameter Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ParameterAssignmentImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getParameterAssignment()
		 * @generated
		 */
		EClass PARAMETER_ASSIGNMENT = eINSTANCE.getParameterAssignment();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_ASSIGNMENT__ACTION = eINSTANCE.getParameterAssignment_Action();

		/**
		 * The meta object literal for the '<em><b>Assigned Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE = eINSTANCE.getParameterAssignment_AssignedVariable();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_ASSIGNMENT__PARAMETER = eINSTANCE.getParameterAssignment_Parameter();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.Variable <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.Variable
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.SelfVariableImpl <em>Self Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.SelfVariableImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getSelfVariable()
		 * @generated
		 */
		EClass SELF_VARIABLE = eINSTANCE.getSelfVariable();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELF_VARIABLE__TYPE = eINSTANCE.getSelfVariable_Type();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ResultVariableImpl <em>Result Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ResultVariableImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getResultVariable()
		 * @generated
		 */
		EClass RESULT_VARIABLE = eINSTANCE.getResultVariable();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESULT_VARIABLE__ACTION = eINSTANCE.getResultVariable_Action();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.NullVariableImpl <em>Null Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.NullVariableImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getNullVariable()
		 * @generated
		 */
		EClass NULL_VARIABLE = eINSTANCE.getNullVariable();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.ProduceActionImpl <em>Produce Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.ProduceActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getProduceAction()
		 * @generated
		 */
		EClass PRODUCE_ACTION = eINSTANCE.getProduceAction();

		/**
		 * The meta object literal for the '{@link de.upb.pose.specification.actions.impl.RedirectActionImpl <em>Redirect Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.pose.specification.actions.impl.RedirectActionImpl
		 * @see de.upb.pose.specification.actions.impl.ActionsPackageImpl#getRedirectAction()
		 * @generated
		 */
		EClass REDIRECT_ACTION = eINSTANCE.getRedirectAction();

	}

} //ActionsPackage
