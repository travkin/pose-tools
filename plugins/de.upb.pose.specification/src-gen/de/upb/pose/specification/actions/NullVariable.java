/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Null Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents a null variable, i.e. an empty/undefined value or reference.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getNullVariable()
 * @generated
 */
public interface NullVariable extends Variable {
} // NullVariable
