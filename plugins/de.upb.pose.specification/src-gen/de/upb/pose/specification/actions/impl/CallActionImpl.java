/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Operation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.impl.CallActionImpl#getAssignments <em>Assignments</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.CallActionImpl#getCalledOperation <em>Called Operation</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.CallActionImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.CallActionImpl#isForAll <em>For All</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallActionImpl extends ResultActionImpl implements CallAction {
	/**
	 * The cached value of the '{@link #getAssignments() <em>Assignments</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignments()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterAssignment> assignments;

	/**
	 * The cached value of the '{@link #getCalledOperation() <em>Called Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalledOperation()
	 * @generated
	 * @ordered
	 */
	protected Operation calledOperation;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected Variable target;

	/**
	 * The default value of the '{@link #isForAll() <em>For All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForAll()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FOR_ALL_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isForAll() <em>For All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isForAll()
	 * @generated
	 * @ordered
	 */
	protected boolean forAll = FOR_ALL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.CALL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterAssignment> getAssignments() {
		if (assignments == null) {
			assignments = new EObjectContainmentWithInverseEList<ParameterAssignment>(ParameterAssignment.class, this,
					ActionsPackage.CALL_ACTION__ASSIGNMENTS, ActionsPackage.PARAMETER_ASSIGNMENT__ACTION);
		}
		return assignments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getCalledOperation() {
		if (calledOperation != null && calledOperation.eIsProxy()) {
			InternalEObject oldCalledOperation = (InternalEObject) calledOperation;
			calledOperation = (Operation) eResolveProxy(oldCalledOperation);
			if (calledOperation != oldCalledOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.CALL_ACTION__CALLED_OPERATION, oldCalledOperation, calledOperation));
			}
		}
		return calledOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetCalledOperation() {
		return calledOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCalledOperation(Operation newCalledOperation) {
		Operation oldCalledOperation = calledOperation;
		calledOperation = newCalledOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.CALL_ACTION__CALLED_OPERATION,
					oldCalledOperation, calledOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (Variable) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ActionsPackage.CALL_ACTION__TARGET,
							oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(Variable newTarget) {
		Variable oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.CALL_ACTION__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isForAll() {
		return forAll;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForAll(boolean newForAll) {
		boolean oldForAll = forAll;
		forAll = newForAll;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.CALL_ACTION__FOR_ALL, oldForAll,
					forAll));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getAssignments()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			return ((InternalEList<?>) getAssignments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			return getAssignments();
		case ActionsPackage.CALL_ACTION__CALLED_OPERATION:
			if (resolve)
				return getCalledOperation();
			return basicGetCalledOperation();
		case ActionsPackage.CALL_ACTION__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		case ActionsPackage.CALL_ACTION__FOR_ALL:
			return isForAll();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			getAssignments().clear();
			getAssignments().addAll((Collection<? extends ParameterAssignment>) newValue);
			return;
		case ActionsPackage.CALL_ACTION__CALLED_OPERATION:
			setCalledOperation((Operation) newValue);
			return;
		case ActionsPackage.CALL_ACTION__TARGET:
			setTarget((Variable) newValue);
			return;
		case ActionsPackage.CALL_ACTION__FOR_ALL:
			setForAll((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			getAssignments().clear();
			return;
		case ActionsPackage.CALL_ACTION__CALLED_OPERATION:
			setCalledOperation((Operation) null);
			return;
		case ActionsPackage.CALL_ACTION__TARGET:
			setTarget((Variable) null);
			return;
		case ActionsPackage.CALL_ACTION__FOR_ALL:
			setForAll(FOR_ALL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ActionsPackage.CALL_ACTION__ASSIGNMENTS:
			return assignments != null && !assignments.isEmpty();
		case ActionsPackage.CALL_ACTION__CALLED_OPERATION:
			return calledOperation != null;
		case ActionsPackage.CALL_ACTION__TARGET:
			return target != null;
		case ActionsPackage.CALL_ACTION__FOR_ALL:
			return forAll != FOR_ALL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (forAll: "); //$NON-NLS-1$
		result.append(forAll);
		result.append(')');
		return result.toString();
	}

} //CallActionImpl
