/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Produce Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This action is a special CreateAction which additionally returns its result as the return value of an operation.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getProduceAction()
 * @generated
 */
public interface ProduceAction extends CreateAction {
} // ProduceAction
