/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.actions.DeleteAction;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.WriteAction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActionsFactoryImpl extends EFactoryImpl implements ActionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ActionsFactory init() {
		try {
			ActionsFactory theActionsFactory = (ActionsFactory) EPackage.Registry.INSTANCE
					.getEFactory(ActionsPackage.eNS_URI);
			if (theActionsFactory != null) {
				return theActionsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ActionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ActionsPackage.CREATE_ACTION:
			return createCreateAction();
		case ActionsPackage.CALL_ACTION:
			return createCallAction();
		case ActionsPackage.DELEGATE_ACTION:
			return createDelegateAction();
		case ActionsPackage.WRITE_ACTION:
			return createWriteAction();
		case ActionsPackage.DELETE_ACTION:
			return createDeleteAction();
		case ActionsPackage.READ_ACTION:
			return createReadAction();
		case ActionsPackage.RETURN_ACTION:
			return createReturnAction();
		case ActionsPackage.PARAMETER_ASSIGNMENT:
			return createParameterAssignment();
		case ActionsPackage.SELF_VARIABLE:
			return createSelfVariable();
		case ActionsPackage.RESULT_VARIABLE:
			return createResultVariable();
		case ActionsPackage.NULL_VARIABLE:
			return createNullVariable();
		case ActionsPackage.PRODUCE_ACTION:
			return createProduceAction();
		case ActionsPackage.REDIRECT_ACTION:
			return createRedirectAction();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateAction createCreateAction() {
		CreateActionImpl createAction = new CreateActionImpl();
		return createAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction createCallAction() {
		CallActionImpl callAction = new CallActionImpl();
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelegateAction createDelegateAction() {
		DelegateActionImpl delegateAction = new DelegateActionImpl();
		return delegateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriteAction createWriteAction() {
		WriteActionImpl writeAction = new WriteActionImpl();
		return writeAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeleteAction createDeleteAction() {
		DeleteActionImpl deleteAction = new DeleteActionImpl();
		return deleteAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadAction createReadAction() {
		ReadActionImpl readAction = new ReadActionImpl();
		return readAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnAction createReturnAction() {
		ReturnActionImpl returnAction = new ReturnActionImpl();
		return returnAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterAssignment createParameterAssignment() {
		ParameterAssignmentImpl parameterAssignment = new ParameterAssignmentImpl();
		return parameterAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelfVariable createSelfVariable() {
		SelfVariableImpl selfVariable = new SelfVariableImpl();
		return selfVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultVariable createResultVariable() {
		ResultVariableImpl resultVariable = new ResultVariableImpl();
		return resultVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullVariable createNullVariable() {
		NullVariableImpl nullVariable = new NullVariableImpl();
		return nullVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProduceAction createProduceAction() {
		ProduceActionImpl produceAction = new ProduceActionImpl();
		return produceAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RedirectAction createRedirectAction() {
		RedirectActionImpl redirectAction = new RedirectActionImpl();
		return redirectAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionsPackage getActionsPackage() {
		return (ActionsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ActionsPackage getPackage() {
		return ActionsPackage.eINSTANCE;
	}

} //ActionsFactoryImpl
