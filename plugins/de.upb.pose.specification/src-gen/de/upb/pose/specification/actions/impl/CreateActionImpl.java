/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.types.Type;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.impl.CreateActionImpl#getInstantiatedType <em>Instantiated Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CreateActionImpl extends ResultActionImpl implements CreateAction {
	/**
	 * The cached value of the '{@link #getInstantiatedType() <em>Instantiated Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantiatedType()
	 * @generated
	 * @ordered
	 */
	protected Type instantiatedType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreateActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.CREATE_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getInstantiatedType() {
		if (instantiatedType != null && instantiatedType.eIsProxy()) {
			InternalEObject oldInstantiatedType = (InternalEObject) instantiatedType;
			instantiatedType = (Type) eResolveProxy(oldInstantiatedType);
			if (instantiatedType != oldInstantiatedType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE, oldInstantiatedType, instantiatedType));
			}
		}
		return instantiatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type basicGetInstantiatedType() {
		return instantiatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstantiatedType(Type newInstantiatedType) {
		Type oldInstantiatedType = instantiatedType;
		instantiatedType = newInstantiatedType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE,
					oldInstantiatedType, instantiatedType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE:
			if (resolve)
				return getInstantiatedType();
			return basicGetInstantiatedType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE:
			setInstantiatedType((Type) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE:
			setInstantiatedType((Type) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ActionsPackage.CREATE_ACTION__INSTANTIATED_TYPE:
			return instantiatedType != null;
		}
		return super.eIsSet(featureID);
	}

} //CreateActionImpl
