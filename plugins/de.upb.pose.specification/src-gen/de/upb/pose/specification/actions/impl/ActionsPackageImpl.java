/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.upb.pose.core.CorePackage;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.impl.AccessPackageImpl;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.actions.DeleteAction;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.actions.WriteAction;
import de.upb.pose.specification.impl.SpecificationPackageImpl;
import de.upb.pose.specification.subsystems.SubsystemsPackage;
import de.upb.pose.specification.subsystems.impl.SubsystemsPackageImpl;
import de.upb.pose.specification.types.TypesPackage;
import de.upb.pose.specification.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ActionsPackageImpl extends EPackageImpl implements ActionsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delegateActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableAccessActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass writeActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deleteActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass readActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass returnActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass selfVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nullVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass produceActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass redirectActionEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.upb.pose.specification.actions.ActionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ActionsPackageImpl() {
		super(eNS_URI, ActionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ActionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ActionsPackage init() {
		if (isInited)
			return (ActionsPackage) EPackage.Registry.INSTANCE.getEPackage(ActionsPackage.eNS_URI);

		// Obtain or create and register package
		ActionsPackageImpl theActionsPackage = (ActionsPackageImpl) (EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ActionsPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new ActionsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		CorePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		SpecificationPackageImpl theSpecificationPackage = (SpecificationPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) instanceof SpecificationPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI) : SpecificationPackage.eINSTANCE);
		AccessPackageImpl theAccessPackage = (AccessPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) instanceof AccessPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(AccessPackage.eNS_URI) : AccessPackage.eINSTANCE);
		SubsystemsPackageImpl theSubsystemsPackage = (SubsystemsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) instanceof SubsystemsPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(SubsystemsPackage.eNS_URI) : SubsystemsPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theActionsPackage.createPackageContents();
		theSpecificationPackage.createPackageContents();
		theAccessPackage.createPackageContents();
		theSubsystemsPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theActionsPackage.initializePackageContents();
		theSpecificationPackage.initializePackageContents();
		theAccessPackage.initializePackageContents();
		theSubsystemsPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theActionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ActionsPackage.eNS_URI, theActionsPackage);
		return theActionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAction_Operation() {
		return (EReference) actionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResultAction() {
		return resultActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultAction_ResultVariable() {
		return (EReference) resultActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateAction() {
		return createActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCreateAction_InstantiatedType() {
		return (EReference) createActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallAction() {
		return callActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallAction_Assignments() {
		return (EReference) callActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallAction_CalledOperation() {
		return (EReference) callActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallAction_Target() {
		return (EReference) callActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCallAction_ForAll() {
		return (EAttribute) callActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDelegateAction() {
		return delegateActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableAccessAction() {
		return variableAccessActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableAccessAction_AccessedVariable() {
		return (EReference) variableAccessActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableAccessAction_Target() {
		return (EReference) variableAccessActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWriteAction() {
		return writeActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWriteAction_AssignedVariable() {
		return (EReference) writeActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeleteAction() {
		return deleteActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReadAction() {
		return readActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReturnAction() {
		return returnActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterAssignment() {
		return parameterAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterAssignment_Action() {
		return (EReference) parameterAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterAssignment_AssignedVariable() {
		return (EReference) parameterAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterAssignment_Parameter() {
		return (EReference) parameterAssignmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariable() {
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSelfVariable() {
		return selfVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSelfVariable_Type() {
		return (EReference) selfVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResultVariable() {
		return resultVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResultVariable_Action() {
		return (EReference) resultVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNullVariable() {
		return nullVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProduceAction() {
		return produceActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRedirectAction() {
		return redirectActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionsFactory getActionsFactory() {
		return (ActionsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		actionEClass = createEClass(ACTION);
		createEReference(actionEClass, ACTION__OPERATION);

		resultActionEClass = createEClass(RESULT_ACTION);
		createEReference(resultActionEClass, RESULT_ACTION__RESULT_VARIABLE);

		createActionEClass = createEClass(CREATE_ACTION);
		createEReference(createActionEClass, CREATE_ACTION__INSTANTIATED_TYPE);

		callActionEClass = createEClass(CALL_ACTION);
		createEReference(callActionEClass, CALL_ACTION__ASSIGNMENTS);
		createEReference(callActionEClass, CALL_ACTION__CALLED_OPERATION);
		createEReference(callActionEClass, CALL_ACTION__TARGET);
		createEAttribute(callActionEClass, CALL_ACTION__FOR_ALL);

		delegateActionEClass = createEClass(DELEGATE_ACTION);

		variableAccessActionEClass = createEClass(VARIABLE_ACCESS_ACTION);
		createEReference(variableAccessActionEClass, VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE);
		createEReference(variableAccessActionEClass, VARIABLE_ACCESS_ACTION__TARGET);

		writeActionEClass = createEClass(WRITE_ACTION);
		createEReference(writeActionEClass, WRITE_ACTION__ASSIGNED_VARIABLE);

		deleteActionEClass = createEClass(DELETE_ACTION);

		readActionEClass = createEClass(READ_ACTION);

		returnActionEClass = createEClass(RETURN_ACTION);

		parameterAssignmentEClass = createEClass(PARAMETER_ASSIGNMENT);
		createEReference(parameterAssignmentEClass, PARAMETER_ASSIGNMENT__ACTION);
		createEReference(parameterAssignmentEClass, PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE);
		createEReference(parameterAssignmentEClass, PARAMETER_ASSIGNMENT__PARAMETER);

		variableEClass = createEClass(VARIABLE);

		selfVariableEClass = createEClass(SELF_VARIABLE);
		createEReference(selfVariableEClass, SELF_VARIABLE__TYPE);

		resultVariableEClass = createEClass(RESULT_VARIABLE);
		createEReference(resultVariableEClass, RESULT_VARIABLE__ACTION);

		nullVariableEClass = createEClass(NULL_VARIABLE);

		produceActionEClass = createEClass(PRODUCE_ACTION);

		redirectActionEClass = createEClass(REDIRECT_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SpecificationPackage theSpecificationPackage = (SpecificationPackage) EPackage.Registry.INSTANCE
				.getEPackage(SpecificationPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage) EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		actionEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		resultActionEClass.getESuperTypes().add(this.getAction());
		createActionEClass.getESuperTypes().add(this.getResultAction());
		callActionEClass.getESuperTypes().add(this.getResultAction());
		delegateActionEClass.getESuperTypes().add(this.getRedirectAction());
		variableAccessActionEClass.getESuperTypes().add(this.getAction());
		writeActionEClass.getESuperTypes().add(this.getVariableAccessAction());
		deleteActionEClass.getESuperTypes().add(this.getWriteAction());
		readActionEClass.getESuperTypes().add(this.getVariableAccessAction());
		returnActionEClass.getESuperTypes().add(this.getReadAction());
		parameterAssignmentEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		variableEClass.getESuperTypes().add(theSpecificationPackage.getDesignElement());
		selfVariableEClass.getESuperTypes().add(this.getVariable());
		resultVariableEClass.getESuperTypes().add(this.getVariable());
		nullVariableEClass.getESuperTypes().add(this.getVariable());
		produceActionEClass.getESuperTypes().add(this.getCreateAction());
		redirectActionEClass.getESuperTypes().add(this.getCallAction());

		// Initialize classes, features, and operations; add parameters
		initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getAction_Operation(),
				theTypesPackage.getOperation(),
				theTypesPackage.getOperation_Actions(),
				"operation", null, 1, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(resultActionEClass, ResultAction.class,
				"ResultAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getResultAction_ResultVariable(),
				this.getResultVariable(),
				this.getResultVariable_Action(),
				"resultVariable", null, 0, 1, ResultAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(createActionEClass, CreateAction.class,
				"CreateAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getCreateAction_InstantiatedType(),
				theTypesPackage.getType(),
				null,
				"instantiatedType", null, 1, 1, CreateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(callActionEClass, CallAction.class,
				"CallAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getCallAction_Assignments(),
				this.getParameterAssignment(),
				this.getParameterAssignment_Action(),
				"assignments", null, 0, -1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getCallAction_CalledOperation(),
				theTypesPackage.getOperation(),
				null,
				"calledOperation", null, 1, 1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getCallAction_Target(),
				this.getVariable(),
				null,
				"target", null, 1, 1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getCallAction_ForAll(),
				ecorePackage.getEBoolean(),
				"forAll", "true", 1, 1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$

		initEClass(delegateActionEClass, DelegateAction.class,
				"DelegateAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(variableAccessActionEClass, VariableAccessAction.class,
				"VariableAccessAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getVariableAccessAction_AccessedVariable(),
				this.getVariable(),
				null,
				"accessedVariable", null, 1, 1, VariableAccessAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getVariableAccessAction_Target(),
				this.getVariable(),
				null,
				"target", null, 1, 1, VariableAccessAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(writeActionEClass, WriteAction.class,
				"WriteAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getWriteAction_AssignedVariable(),
				this.getVariable(),
				null,
				"assignedVariable", null, 0, 1, WriteAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(deleteActionEClass, DeleteAction.class,
				"DeleteAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(readActionEClass, ReadAction.class,
				"ReadAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(returnActionEClass, ReturnAction.class,
				"ReturnAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(parameterAssignmentEClass, ParameterAssignment.class,
				"ParameterAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getParameterAssignment_Action(),
				this.getCallAction(),
				this.getCallAction_Assignments(),
				"action", null, 0, 1, ParameterAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getParameterAssignment_AssignedVariable(),
				this.getVariable(),
				null,
				"assignedVariable", null, 0, 1, ParameterAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getParameterAssignment_Parameter(),
				theTypesPackage.getParameter(),
				null,
				"parameter", null, 1, 1, ParameterAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(variableEClass, Variable.class, "Variable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(selfVariableEClass, SelfVariable.class,
				"SelfVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getSelfVariable_Type(),
				theTypesPackage.getType(),
				theTypesPackage.getType_SelfVariable(),
				"type", null, 1, 1, SelfVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(resultVariableEClass, ResultVariable.class,
				"ResultVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getResultVariable_Action(),
				this.getResultAction(),
				this.getResultAction_ResultVariable(),
				"action", null, 1, 1, ResultVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(nullVariableEClass, NullVariable.class,
				"NullVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(produceActionEClass, ProduceAction.class,
				"ProduceAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(redirectActionEClass, RedirectAction.class,
				"RedirectAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] {
				"documentation", "This package contains all elements that are related to pattern actions/behavior." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(actionEClass, source, new String[] { "documentation", "This is the base class for all actions." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getAction_Operation(), source, new String[] {
				"documentation", "The operation to which this action belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(resultActionEClass, source, new String[] {
				"documentation", "This class represents an action that can have a result." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getResultAction_ResultVariable(), source, new String[] {
				"documentation", "The variable that is returned by this action." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				createActionEClass,
				source,
				new String[] {
						"documentation", "This class represents an action that creates an instance of a type. The new instance is stored as result in the result variable." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getCreateAction_InstantiatedType(), source, new String[] {
				"documentation", "The type that is instantiated by this action." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				callActionEClass,
				source,
				new String[] {
						"documentation", "This class represents an action that calls an operation. on a certain object or a set of objects. These objects are determined by specification of a target variable that, e.g., could be a reference to a set of objects or a parameter of the calling operation. In case of more than one target object the forAll attribute determines whether the call is to be performed on all or only one of the objects. The return value of the call is stored in the result variable. If the called operation has parameters, the arguments of the call can be explicitly determined by parameter assignments." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getCallAction_Assignments(), source, new String[] {
				"documentation", "The parameter assignments for the called operation." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCallAction_CalledOperation(), source, new String[] {
				"documentation", "The operation that is called by the action." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getCallAction_Target(), source, new String[] {
				"documentation", "The target variable on which the call will be made." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				delegateActionEClass,
				source,
				new String[] {
						"documentation", "This represents a special RedirectionAction, a delegation of a call to another object and its operation. A delegation is always performed on only one target object and returns the return value of the called operation as its own return value. In addition, it has all the properties of a redirection." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(variableAccessActionEClass, source, new String[] {
				"documentation", "This class represents an action that accesses a variable." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getVariableAccessAction_AccessedVariable(), source, new String[] {
				"documentation", "The variable that is accessed." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(writeActionEClass, source, new String[] {
				"documentation", "This class represents an action that writes the value of a variable to another one." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getWriteAction_AssignedVariable(), source, new String[] {
				"documentation", "The variable which will be assigned to the accessed variable of this write action." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(deleteActionEClass, source, new String[] {
				"documentation", "This class represents an action that deletes the value of the accessed variable." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(readActionEClass, source, new String[] {
				"documentation", "This class represents an action that reads the value of the accessed variable" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				returnActionEClass,
				source,
				new String[] {
						"documentation", "This class represents an action that reads and returns the value of the accessed variable as the return value of the parent operation." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(parameterAssignmentEClass, source, new String[] {
				"documentation", "This class represents the assignment of a variable to a parameter." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getParameterAssignment_Action(), source, new String[] {
				"documentation", "The call action to which this assignment belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getParameterAssignment_AssignedVariable(), source, new String[] {
				"documentation", "The variable which is assigned to the parameter." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getParameterAssignment_Parameter(), source, new String[] {
				"documentation", "The parameter to which the variable value will be assigned." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(variableEClass, source, new String[] { "documentation", "An interface for all variables." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				selfVariableEClass,
				source,
				new String[] {
						"documentation", "This variable represents the self reference of a type (similar to the keyword \'this\' in Java)." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(getSelfVariable_Type(), source, new String[] {
				"documentation", "The type to which this self variable belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(resultVariableEClass, source, new String[] {
				"documentation", "This class represents a result variable holding the result of an action execution." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getResultVariable_Action(), source, new String[] {
				"documentation", "The action to which this variable belongs." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(nullVariableEClass, source, new String[] {
				"documentation", "This class represents a null variable, i.e. an empty/undefined value or reference." //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(
				produceActionEClass,
				source,
				new String[] {
						"documentation", "This action is a special CreateAction which additionally returns its result as the return value of an operation." //$NON-NLS-1$ //$NON-NLS-2$
				});
		addAnnotation(
				redirectActionEClass,
				source,
				new String[] {
						"documentation", "This represents a special CallAction, a redirection of a call to another object and its operation. A redirection always takes all its arguments and uses them as arguments of the called operation." //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore"; //$NON-NLS-1$	
		addAnnotation(this, source, new String[] { "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL", //$NON-NLS-1$ //$NON-NLS-2$
				"validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

} //ActionsPackageImpl
