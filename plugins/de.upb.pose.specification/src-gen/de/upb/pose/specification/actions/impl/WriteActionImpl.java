/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.WriteAction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Write Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.impl.WriteActionImpl#getAssignedVariable <em>Assigned Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class WriteActionImpl extends VariableAccessActionImpl implements WriteAction {
	/**
	 * The cached value of the '{@link #getAssignedVariable() <em>Assigned Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable assignedVariable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WriteActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.WRITE_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getAssignedVariable() {
		if (assignedVariable != null && assignedVariable.eIsProxy()) {
			InternalEObject oldAssignedVariable = (InternalEObject) assignedVariable;
			assignedVariable = (Variable) eResolveProxy(oldAssignedVariable);
			if (assignedVariable != oldAssignedVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE, oldAssignedVariable, assignedVariable));
			}
		}
		return assignedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetAssignedVariable() {
		return assignedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignedVariable(Variable newAssignedVariable) {
		Variable oldAssignedVariable = assignedVariable;
		assignedVariable = newAssignedVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE,
					oldAssignedVariable, assignedVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE:
			if (resolve)
				return getAssignedVariable();
			return basicGetAssignedVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE:
			setAssignedVariable((Variable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE:
			setAssignedVariable((Variable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ActionsPackage.WRITE_ACTION__ASSIGNED_VARIABLE:
			return assignedVariable != null;
		}
		return super.eIsSet(featureID);
	}

} //WriteActionImpl
