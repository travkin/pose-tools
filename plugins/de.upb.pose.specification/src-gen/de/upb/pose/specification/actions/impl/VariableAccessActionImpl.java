/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Access Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.impl.VariableAccessActionImpl#getAccessedVariable <em>Accessed Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.VariableAccessActionImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class VariableAccessActionImpl extends ActionImpl implements VariableAccessAction {
	/**
	 * The cached value of the '{@link #getAccessedVariable() <em>Accessed Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessedVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable accessedVariable;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected Variable target;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableAccessActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.VARIABLE_ACCESS_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getAccessedVariable() {
		if (accessedVariable != null && accessedVariable.eIsProxy()) {
			InternalEObject oldAccessedVariable = (InternalEObject) accessedVariable;
			accessedVariable = (Variable) eResolveProxy(oldAccessedVariable);
			if (accessedVariable != oldAccessedVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE, oldAccessedVariable,
							accessedVariable));
			}
		}
		return accessedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetAccessedVariable() {
		return accessedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessedVariable(Variable newAccessedVariable) {
		Variable oldAccessedVariable = accessedVariable;
		accessedVariable = newAccessedVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE, oldAccessedVariable, accessedVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject) target;
			target = (Variable) eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(Variable newTarget) {
		Variable oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET,
					oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE:
			if (resolve)
				return getAccessedVariable();
			return basicGetAccessedVariable();
		case ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET:
			if (resolve)
				return getTarget();
			return basicGetTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE:
			setAccessedVariable((Variable) newValue);
			return;
		case ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET:
			setTarget((Variable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE:
			setAccessedVariable((Variable) null);
			return;
		case ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET:
			setTarget((Variable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ActionsPackage.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE:
			return accessedVariable != null;
		case ActionsPackage.VARIABLE_ACCESS_ACTION__TARGET:
			return target != null;
		}
		return super.eIsSet(featureID);
	}

} //VariableAccessActionImpl
