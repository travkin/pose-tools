/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.specification.types.Operation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that calls an operation. on a certain object or a set of objects. These objects are determined by specification of a target variable that, e.g., could be a reference to a set of objects or a parameter of the calling operation. In case of more than one target object the forAll attribute determines whether the call is to be performed on all or only one of the objects. The return value of the call is stored in the result variable. If the called operation has parameters, the arguments of the call can be explicitly determined by parameter assignments.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.CallAction#getAssignments <em>Assignments</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.CallAction#getCalledOperation <em>Called Operation</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.CallAction#getTarget <em>Target</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.CallAction#isForAll <em>For All</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getCallAction()
 * @generated
 */
public interface CallAction extends ResultAction {
	/**
	 * Returns the value of the '<em><b>Assignments</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.specification.actions.ParameterAssignment}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.ParameterAssignment#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parameter assignments for the called operation.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Assignments</em>' containment reference list.
	 * @see de.upb.pose.specification.actions.ActionsPackage#getCallAction_Assignments()
	 * @see de.upb.pose.specification.actions.ParameterAssignment#getAction
	 * @generated
	 */
	EList<ParameterAssignment> getAssignments();

	/**
	 * Returns the value of the '<em><b>Called Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The operation that is called by the action.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Called Operation</em>' reference.
	 * @see #setCalledOperation(Operation)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getCallAction_CalledOperation()
	 * @generated
	 */
	Operation getCalledOperation();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.CallAction#getCalledOperation <em>Called Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Called Operation</em>' reference.
	 * @see #getCalledOperation()
	 * @generated
	 */
	void setCalledOperation(Operation value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The target variable on which the call will be made.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Variable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getCallAction_Target()
	 * @generated
	 */
	Variable getTarget();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.CallAction#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Variable value);

	/**
	 * Returns the value of the '<em><b>For All</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For All</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For All</em>' attribute.
	 * @see #setForAll(boolean)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getCallAction_ForAll()
	 * @generated
	 */
	boolean isForAll();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.CallAction#isForAll <em>For All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For All</em>' attribute.
	 * @see #isForAll()
	 * @generated
	 */
	void setForAll(boolean value);

} // CallAction
