/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.Identifier;
import de.upb.pose.core.Named;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.actions.DeleteAction;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.actions.WriteAction;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.upb.pose.specification.actions.ActionsPackage
 * @generated
 */
public class ActionsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ActionsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ActionsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionsSwitch<Adapter> modelSwitch = new ActionsSwitch<Adapter>() {
		@Override
		public Adapter caseAction(Action object) {
			return createActionAdapter();
		}

		@Override
		public Adapter caseResultAction(ResultAction object) {
			return createResultActionAdapter();
		}

		@Override
		public Adapter caseCreateAction(CreateAction object) {
			return createCreateActionAdapter();
		}

		@Override
		public Adapter caseCallAction(CallAction object) {
			return createCallActionAdapter();
		}

		@Override
		public Adapter caseDelegateAction(DelegateAction object) {
			return createDelegateActionAdapter();
		}

		@Override
		public Adapter caseVariableAccessAction(VariableAccessAction object) {
			return createVariableAccessActionAdapter();
		}

		@Override
		public Adapter caseWriteAction(WriteAction object) {
			return createWriteActionAdapter();
		}

		@Override
		public Adapter caseDeleteAction(DeleteAction object) {
			return createDeleteActionAdapter();
		}

		@Override
		public Adapter caseReadAction(ReadAction object) {
			return createReadActionAdapter();
		}

		@Override
		public Adapter caseReturnAction(ReturnAction object) {
			return createReturnActionAdapter();
		}

		@Override
		public Adapter caseParameterAssignment(ParameterAssignment object) {
			return createParameterAssignmentAdapter();
		}

		@Override
		public Adapter caseVariable(Variable object) {
			return createVariableAdapter();
		}

		@Override
		public Adapter caseSelfVariable(SelfVariable object) {
			return createSelfVariableAdapter();
		}

		@Override
		public Adapter caseResultVariable(ResultVariable object) {
			return createResultVariableAdapter();
		}

		@Override
		public Adapter caseNullVariable(NullVariable object) {
			return createNullVariableAdapter();
		}

		@Override
		public Adapter caseProduceAction(ProduceAction object) {
			return createProduceActionAdapter();
		}

		@Override
		public Adapter caseRedirectAction(RedirectAction object) {
			return createRedirectActionAdapter();
		}

		@Override
		public Adapter caseIdentifier(Identifier object) {
			return createIdentifierAdapter();
		}

		@Override
		public Adapter caseNamed(Named object) {
			return createNamedAdapter();
		}

		@Override
		public Adapter casePatternElement(PatternElement object) {
			return createPatternElementAdapter();
		}

		@Override
		public Adapter caseSetFragmentElement(SetFragmentElement object) {
			return createSetFragmentElementAdapter();
		}

		@Override
		public Adapter caseDesignElement(DesignElement object) {
			return createDesignElementAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ResultAction <em>Result Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ResultAction
	 * @generated
	 */
	public Adapter createResultActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.CreateAction <em>Create Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.CreateAction
	 * @generated
	 */
	public Adapter createCreateActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.CallAction
	 * @generated
	 */
	public Adapter createCallActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.DelegateAction <em>Delegate Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.DelegateAction
	 * @generated
	 */
	public Adapter createDelegateActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.VariableAccessAction <em>Variable Access Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.VariableAccessAction
	 * @generated
	 */
	public Adapter createVariableAccessActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.WriteAction <em>Write Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.WriteAction
	 * @generated
	 */
	public Adapter createWriteActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.DeleteAction <em>Delete Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.DeleteAction
	 * @generated
	 */
	public Adapter createDeleteActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ReadAction <em>Read Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ReadAction
	 * @generated
	 */
	public Adapter createReadActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ReturnAction <em>Return Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ReturnAction
	 * @generated
	 */
	public Adapter createReturnActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ParameterAssignment <em>Parameter Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ParameterAssignment
	 * @generated
	 */
	public Adapter createParameterAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.Variable
	 * @generated
	 */
	public Adapter createVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.SelfVariable <em>Self Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.SelfVariable
	 * @generated
	 */
	public Adapter createSelfVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ResultVariable <em>Result Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ResultVariable
	 * @generated
	 */
	public Adapter createResultVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.NullVariable <em>Null Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.NullVariable
	 * @generated
	 */
	public Adapter createNullVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.ProduceAction <em>Produce Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.ProduceAction
	 * @generated
	 */
	public Adapter createProduceActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.actions.RedirectAction <em>Redirect Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.actions.RedirectAction
	 * @generated
	 */
	public Adapter createRedirectActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.SetFragmentElement <em>Set Fragment Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.SetFragmentElement
	 * @generated
	 */
	public Adapter createSetFragmentElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.core.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.core.Identifier
	 * @generated
	 */
	public Adapter createIdentifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.core.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.core.Named
	 * @generated
	 */
	public Adapter createNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.PatternElement <em>Pattern Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.PatternElement
	 * @generated
	 */
	public Adapter createPatternElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.upb.pose.specification.DesignElement <em>Design Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.upb.pose.specification.DesignElement
	 * @generated
	 */
	public Adapter createDesignElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ActionsAdapterFactory
