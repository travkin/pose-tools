/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.impl.DesignElementImpl;
import de.upb.pose.specification.types.Parameter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.impl.ParameterAssignmentImpl#getAction <em>Action</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.ParameterAssignmentImpl#getAssignedVariable <em>Assigned Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.impl.ParameterAssignmentImpl#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ParameterAssignmentImpl extends DesignElementImpl implements ParameterAssignment {
	/**
	 * The cached value of the '{@link #getAssignedVariable() <em>Assigned Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedVariable()
	 * @generated
	 * @ordered
	 */
	protected Variable assignedVariable;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected Parameter parameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ActionsPackage.Literals.PARAMETER_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction getAction() {
		if (eContainerFeatureID() != ActionsPackage.PARAMETER_ASSIGNMENT__ACTION)
			return null;
		return (CallAction) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAction(CallAction newAction, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newAction, ActionsPackage.PARAMETER_ASSIGNMENT__ACTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(CallAction newAction) {
		if (newAction != eInternalContainer()
				|| (eContainerFeatureID() != ActionsPackage.PARAMETER_ASSIGNMENT__ACTION && newAction != null)) {
			if (EcoreUtil.isAncestor(this, newAction))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString()); //$NON-NLS-1$
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAction != null)
				msgs = ((InternalEObject) newAction).eInverseAdd(this, ActionsPackage.CALL_ACTION__ASSIGNMENTS,
						CallAction.class, msgs);
			msgs = basicSetAction(newAction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.PARAMETER_ASSIGNMENT__ACTION,
					newAction, newAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable getAssignedVariable() {
		if (assignedVariable != null && assignedVariable.eIsProxy()) {
			InternalEObject oldAssignedVariable = (InternalEObject) assignedVariable;
			assignedVariable = (Variable) eResolveProxy(oldAssignedVariable);
			if (assignedVariable != oldAssignedVariable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE, oldAssignedVariable,
							assignedVariable));
			}
		}
		return assignedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable basicGetAssignedVariable() {
		return assignedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignedVariable(Variable newAssignedVariable) {
		Variable oldAssignedVariable = assignedVariable;
		assignedVariable = newAssignedVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE, oldAssignedVariable, assignedVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter getParameter() {
		if (parameter != null && parameter.eIsProxy()) {
			InternalEObject oldParameter = (InternalEObject) parameter;
			parameter = (Parameter) eResolveProxy(oldParameter);
			if (parameter != oldParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER, oldParameter, parameter));
			}
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter basicGetParameter() {
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameter(Parameter newParameter) {
		Parameter oldParameter = parameter;
		parameter = newParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER,
					oldParameter, parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetAction((CallAction) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			return basicSetAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			return eInternalContainer().eInverseRemove(this, ActionsPackage.CALL_ACTION__ASSIGNMENTS, CallAction.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			return getAction();
		case ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE:
			if (resolve)
				return getAssignedVariable();
			return basicGetAssignedVariable();
		case ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER:
			if (resolve)
				return getParameter();
			return basicGetParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			setAction((CallAction) newValue);
			return;
		case ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE:
			setAssignedVariable((Variable) newValue);
			return;
		case ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER:
			setParameter((Parameter) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			setAction((CallAction) null);
			return;
		case ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE:
			setAssignedVariable((Variable) null);
			return;
		case ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER:
			setParameter((Parameter) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ActionsPackage.PARAMETER_ASSIGNMENT__ACTION:
			return getAction() != null;
		case ActionsPackage.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE:
			return assignedVariable != null;
		case ActionsPackage.PARAMETER_ASSIGNMENT__PARAMETER:
			return parameter != null;
		}
		return super.eIsSet(featureID);
	}

} //ParameterAssignmentImpl
