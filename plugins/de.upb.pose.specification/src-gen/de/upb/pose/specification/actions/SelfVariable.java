/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

import de.upb.pose.specification.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Self Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This variable represents the self reference of a type (similar to the keyword 'this' in Java).
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.SelfVariable#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getSelfVariable()
 * @generated
 */
public interface SelfVariable extends Variable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.types.Type#getSelfVariable <em>Self Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The type to which this self variable belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Type</em>' container reference.
	 * @see #setType(Type)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getSelfVariable_Type()
	 * @see de.upb.pose.specification.types.Type#getSelfVariable
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.SelfVariable#getType <em>Type</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' container reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // SelfVariable
