/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Result Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents a result variable holding the result of an action execution.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.ResultVariable#getAction <em>Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getResultVariable()
 * @generated
 */
public interface ResultVariable extends Variable {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.ResultAction#getResultVariable <em>Result Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The action to which this variable belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Action</em>' container reference.
	 * @see #setAction(ResultAction)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getResultVariable_Action()
	 * @see de.upb.pose.specification.actions.ResultAction#getResultVariable
	 * @generated
	 */
	ResultAction getAction();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.ResultVariable#getAction <em>Action</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' container reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(ResultAction value);

} // ResultVariable
