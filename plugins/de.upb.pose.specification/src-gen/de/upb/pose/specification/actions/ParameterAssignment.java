/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.types.Parameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents the assignment of a variable to a parameter.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.ParameterAssignment#getAction <em>Action</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.ParameterAssignment#getAssignedVariable <em>Assigned Variable</em>}</li>
 *   <li>{@link de.upb.pose.specification.actions.ParameterAssignment#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getParameterAssignment()
 * @generated
 */
public interface ParameterAssignment extends DesignElement {
	/**
	 * Returns the value of the '<em><b>Action</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.CallAction#getAssignments <em>Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The call action to which this assignment belongs.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Action</em>' container reference.
	 * @see #setAction(CallAction)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getParameterAssignment_Action()
	 * @see de.upb.pose.specification.actions.CallAction#getAssignments
	 * @generated
	 */
	CallAction getAction();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.ParameterAssignment#getAction <em>Action</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' container reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(CallAction value);

	/**
	 * Returns the value of the '<em><b>Assigned Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The variable which is assigned to the parameter.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Assigned Variable</em>' reference.
	 * @see #setAssignedVariable(Variable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getParameterAssignment_AssignedVariable()
	 * @generated
	 */
	Variable getAssignedVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.ParameterAssignment#getAssignedVariable <em>Assigned Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigned Variable</em>' reference.
	 * @see #getAssignedVariable()
	 * @generated
	 */
	void setAssignedVariable(Variable value);

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The parameter to which the variable value will be assigned.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(Parameter)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getParameterAssignment_Parameter()
	 * @generated
	 */
	Parameter getParameter();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.ParameterAssignment#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(Parameter value);

} // ParameterAssignment
