/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.upb.pose.core.Identifier;
import de.upb.pose.core.Named;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.DelegateAction;
import de.upb.pose.specification.actions.DeleteAction;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ProduceAction;
import de.upb.pose.specification.actions.ReadAction;
import de.upb.pose.specification.actions.RedirectAction;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.actions.WriteAction;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.pose.specification.actions.ActionsPackage
 * @generated
 */
public class ActionsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ActionsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionsSwitch() {
		if (modelPackage == null) {
			modelPackage = ActionsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case ActionsPackage.ACTION: {
			Action action = (Action) theEObject;
			T result = caseAction(action);
			if (result == null)
				result = caseDesignElement(action);
			if (result == null)
				result = casePatternElement(action);
			if (result == null)
				result = caseSetFragmentElement(action);
			if (result == null)
				result = caseNamed(action);
			if (result == null)
				result = caseIdentifier(action);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.RESULT_ACTION: {
			ResultAction resultAction = (ResultAction) theEObject;
			T result = caseResultAction(resultAction);
			if (result == null)
				result = caseAction(resultAction);
			if (result == null)
				result = caseDesignElement(resultAction);
			if (result == null)
				result = casePatternElement(resultAction);
			if (result == null)
				result = caseSetFragmentElement(resultAction);
			if (result == null)
				result = caseNamed(resultAction);
			if (result == null)
				result = caseIdentifier(resultAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.CREATE_ACTION: {
			CreateAction createAction = (CreateAction) theEObject;
			T result = caseCreateAction(createAction);
			if (result == null)
				result = caseResultAction(createAction);
			if (result == null)
				result = caseAction(createAction);
			if (result == null)
				result = caseDesignElement(createAction);
			if (result == null)
				result = casePatternElement(createAction);
			if (result == null)
				result = caseSetFragmentElement(createAction);
			if (result == null)
				result = caseNamed(createAction);
			if (result == null)
				result = caseIdentifier(createAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.CALL_ACTION: {
			CallAction callAction = (CallAction) theEObject;
			T result = caseCallAction(callAction);
			if (result == null)
				result = caseResultAction(callAction);
			if (result == null)
				result = caseAction(callAction);
			if (result == null)
				result = caseDesignElement(callAction);
			if (result == null)
				result = casePatternElement(callAction);
			if (result == null)
				result = caseSetFragmentElement(callAction);
			if (result == null)
				result = caseNamed(callAction);
			if (result == null)
				result = caseIdentifier(callAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.DELEGATE_ACTION: {
			DelegateAction delegateAction = (DelegateAction) theEObject;
			T result = caseDelegateAction(delegateAction);
			if (result == null)
				result = caseRedirectAction(delegateAction);
			if (result == null)
				result = caseCallAction(delegateAction);
			if (result == null)
				result = caseResultAction(delegateAction);
			if (result == null)
				result = caseAction(delegateAction);
			if (result == null)
				result = caseDesignElement(delegateAction);
			if (result == null)
				result = casePatternElement(delegateAction);
			if (result == null)
				result = caseSetFragmentElement(delegateAction);
			if (result == null)
				result = caseNamed(delegateAction);
			if (result == null)
				result = caseIdentifier(delegateAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.VARIABLE_ACCESS_ACTION: {
			VariableAccessAction variableAccessAction = (VariableAccessAction) theEObject;
			T result = caseVariableAccessAction(variableAccessAction);
			if (result == null)
				result = caseAction(variableAccessAction);
			if (result == null)
				result = caseDesignElement(variableAccessAction);
			if (result == null)
				result = casePatternElement(variableAccessAction);
			if (result == null)
				result = caseSetFragmentElement(variableAccessAction);
			if (result == null)
				result = caseNamed(variableAccessAction);
			if (result == null)
				result = caseIdentifier(variableAccessAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.WRITE_ACTION: {
			WriteAction writeAction = (WriteAction) theEObject;
			T result = caseWriteAction(writeAction);
			if (result == null)
				result = caseVariableAccessAction(writeAction);
			if (result == null)
				result = caseAction(writeAction);
			if (result == null)
				result = caseDesignElement(writeAction);
			if (result == null)
				result = casePatternElement(writeAction);
			if (result == null)
				result = caseSetFragmentElement(writeAction);
			if (result == null)
				result = caseNamed(writeAction);
			if (result == null)
				result = caseIdentifier(writeAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.DELETE_ACTION: {
			DeleteAction deleteAction = (DeleteAction) theEObject;
			T result = caseDeleteAction(deleteAction);
			if (result == null)
				result = caseWriteAction(deleteAction);
			if (result == null)
				result = caseVariableAccessAction(deleteAction);
			if (result == null)
				result = caseAction(deleteAction);
			if (result == null)
				result = caseDesignElement(deleteAction);
			if (result == null)
				result = casePatternElement(deleteAction);
			if (result == null)
				result = caseSetFragmentElement(deleteAction);
			if (result == null)
				result = caseNamed(deleteAction);
			if (result == null)
				result = caseIdentifier(deleteAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.READ_ACTION: {
			ReadAction readAction = (ReadAction) theEObject;
			T result = caseReadAction(readAction);
			if (result == null)
				result = caseVariableAccessAction(readAction);
			if (result == null)
				result = caseAction(readAction);
			if (result == null)
				result = caseDesignElement(readAction);
			if (result == null)
				result = casePatternElement(readAction);
			if (result == null)
				result = caseSetFragmentElement(readAction);
			if (result == null)
				result = caseNamed(readAction);
			if (result == null)
				result = caseIdentifier(readAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.RETURN_ACTION: {
			ReturnAction returnAction = (ReturnAction) theEObject;
			T result = caseReturnAction(returnAction);
			if (result == null)
				result = caseReadAction(returnAction);
			if (result == null)
				result = caseVariableAccessAction(returnAction);
			if (result == null)
				result = caseAction(returnAction);
			if (result == null)
				result = caseDesignElement(returnAction);
			if (result == null)
				result = casePatternElement(returnAction);
			if (result == null)
				result = caseSetFragmentElement(returnAction);
			if (result == null)
				result = caseNamed(returnAction);
			if (result == null)
				result = caseIdentifier(returnAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.PARAMETER_ASSIGNMENT: {
			ParameterAssignment parameterAssignment = (ParameterAssignment) theEObject;
			T result = caseParameterAssignment(parameterAssignment);
			if (result == null)
				result = caseDesignElement(parameterAssignment);
			if (result == null)
				result = casePatternElement(parameterAssignment);
			if (result == null)
				result = caseSetFragmentElement(parameterAssignment);
			if (result == null)
				result = caseNamed(parameterAssignment);
			if (result == null)
				result = caseIdentifier(parameterAssignment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.VARIABLE: {
			Variable variable = (Variable) theEObject;
			T result = caseVariable(variable);
			if (result == null)
				result = caseDesignElement(variable);
			if (result == null)
				result = casePatternElement(variable);
			if (result == null)
				result = caseSetFragmentElement(variable);
			if (result == null)
				result = caseNamed(variable);
			if (result == null)
				result = caseIdentifier(variable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.SELF_VARIABLE: {
			SelfVariable selfVariable = (SelfVariable) theEObject;
			T result = caseSelfVariable(selfVariable);
			if (result == null)
				result = caseVariable(selfVariable);
			if (result == null)
				result = caseDesignElement(selfVariable);
			if (result == null)
				result = casePatternElement(selfVariable);
			if (result == null)
				result = caseSetFragmentElement(selfVariable);
			if (result == null)
				result = caseNamed(selfVariable);
			if (result == null)
				result = caseIdentifier(selfVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.RESULT_VARIABLE: {
			ResultVariable resultVariable = (ResultVariable) theEObject;
			T result = caseResultVariable(resultVariable);
			if (result == null)
				result = caseVariable(resultVariable);
			if (result == null)
				result = caseDesignElement(resultVariable);
			if (result == null)
				result = casePatternElement(resultVariable);
			if (result == null)
				result = caseSetFragmentElement(resultVariable);
			if (result == null)
				result = caseNamed(resultVariable);
			if (result == null)
				result = caseIdentifier(resultVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.NULL_VARIABLE: {
			NullVariable nullVariable = (NullVariable) theEObject;
			T result = caseNullVariable(nullVariable);
			if (result == null)
				result = caseVariable(nullVariable);
			if (result == null)
				result = caseDesignElement(nullVariable);
			if (result == null)
				result = casePatternElement(nullVariable);
			if (result == null)
				result = caseSetFragmentElement(nullVariable);
			if (result == null)
				result = caseNamed(nullVariable);
			if (result == null)
				result = caseIdentifier(nullVariable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.PRODUCE_ACTION: {
			ProduceAction produceAction = (ProduceAction) theEObject;
			T result = caseProduceAction(produceAction);
			if (result == null)
				result = caseCreateAction(produceAction);
			if (result == null)
				result = caseResultAction(produceAction);
			if (result == null)
				result = caseAction(produceAction);
			if (result == null)
				result = caseDesignElement(produceAction);
			if (result == null)
				result = casePatternElement(produceAction);
			if (result == null)
				result = caseSetFragmentElement(produceAction);
			if (result == null)
				result = caseNamed(produceAction);
			if (result == null)
				result = caseIdentifier(produceAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ActionsPackage.REDIRECT_ACTION: {
			RedirectAction redirectAction = (RedirectAction) theEObject;
			T result = caseRedirectAction(redirectAction);
			if (result == null)
				result = caseCallAction(redirectAction);
			if (result == null)
				result = caseResultAction(redirectAction);
			if (result == null)
				result = caseAction(redirectAction);
			if (result == null)
				result = caseDesignElement(redirectAction);
			if (result == null)
				result = casePatternElement(redirectAction);
			if (result == null)
				result = caseSetFragmentElement(redirectAction);
			if (result == null)
				result = caseNamed(redirectAction);
			if (result == null)
				result = caseIdentifier(redirectAction);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Result Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Result Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResultAction(ResultAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateAction(CreateAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallAction(CallAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delegate Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delegate Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelegateAction(DelegateAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Access Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Access Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableAccessAction(VariableAccessAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Write Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Write Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWriteAction(WriteAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delete Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delete Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeleteAction(DeleteAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Read Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Read Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadAction(ReadAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnAction(ReturnAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterAssignment(ParameterAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariable(Variable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Self Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Self Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelfVariable(SelfVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Result Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Result Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResultVariable(ResultVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullVariable(NullVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Produce Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Produce Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProduceAction(ProduceAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Redirect Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Redirect Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRedirectAction(RedirectAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Fragment Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Fragment Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetFragmentElement(SetFragmentElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifier(Identifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePatternElement(PatternElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignElement(DesignElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ActionsSwitch
