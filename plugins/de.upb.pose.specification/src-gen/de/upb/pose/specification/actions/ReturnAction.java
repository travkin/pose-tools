/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that reads and returns the value of the accessed variable as the return value of the parent operation.
 * <!-- end-model-doc -->
 *
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getReturnAction()
 * @generated
 */
public interface ReturnAction extends ReadAction {
} // ReturnAction
