/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.actions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Result Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This class represents an action that can have a result.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.specification.actions.ResultAction#getResultVariable <em>Result Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.specification.actions.ActionsPackage#getResultAction()
 * @generated
 */
public interface ResultAction extends Action {
	/**
	 * Returns the value of the '<em><b>Result Variable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.specification.actions.ResultVariable#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The variable that is returned by this action.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Result Variable</em>' containment reference.
	 * @see #setResultVariable(ResultVariable)
	 * @see de.upb.pose.specification.actions.ActionsPackage#getResultAction_ResultVariable()
	 * @see de.upb.pose.specification.actions.ResultVariable#getAction
	 * @generated
	 */
	ResultVariable getResultVariable();

	/**
	 * Sets the value of the '{@link de.upb.pose.specification.actions.ResultAction#getResultVariable <em>Result Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Variable</em>' containment reference.
	 * @see #getResultVariable()
	 * @generated
	 */
	void setResultVariable(ResultVariable value);

} // ResultAction
