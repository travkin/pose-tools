/**
 * <copyright>
 * 	Copyright 2012 by University of Paderborn and others. All rights reserved. This program and its materials are
 * 	made available under the terms of the Eclipse Public License v1.0 which is referenced in this distribution.
 * 
 * 	Contributors:
 * 		Dietrich Travkin <travkin@uni-paderborn.de> - Initial code
 * 
 * </copyright>
 */
package de.upb.pose.specification.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import de.upb.pose.core.Commentable;
import de.upb.pose.core.Identifier;
import de.upb.pose.core.Named;
import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignModel;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationPackage;
import de.upb.pose.specification.TaskDescription;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.upb.pose.specification.SpecificationPackage
 * @generated
 */
public class SpecificationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SpecificationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpecificationSwitch() {
		if (modelPackage == null) {
			modelPackage = SpecificationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case SpecificationPackage.DESIGN_PATTERN_CATALOG: {
			DesignPatternCatalog designPatternCatalog = (DesignPatternCatalog) theEObject;
			T result = caseDesignPatternCatalog(designPatternCatalog);
			if (result == null)
				result = caseNamed(designPatternCatalog);
			if (result == null)
				result = caseCommentable(designPatternCatalog);
			if (result == null)
				result = caseIdentifier(designPatternCatalog);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.CATEGORY: {
			Category category = (Category) theEObject;
			T result = caseCategory(category);
			if (result == null)
				result = caseNamed(category);
			if (result == null)
				result = caseCommentable(category);
			if (result == null)
				result = caseIdentifier(category);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.DESIGN_PATTERN: {
			DesignPattern designPattern = (DesignPattern) theEObject;
			T result = caseDesignPattern(designPattern);
			if (result == null)
				result = caseNamed(designPattern);
			if (result == null)
				result = caseCommentable(designPattern);
			if (result == null)
				result = caseIdentifier(designPattern);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.PATTERN_SPECIFICATION: {
			PatternSpecification patternSpecification = (PatternSpecification) theEObject;
			T result = casePatternSpecification(patternSpecification);
			if (result == null)
				result = caseNamed(patternSpecification);
			if (result == null)
				result = caseCommentable(patternSpecification);
			if (result == null)
				result = caseIdentifier(patternSpecification);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.PATTERN_ELEMENT: {
			PatternElement patternElement = (PatternElement) theEObject;
			T result = casePatternElement(patternElement);
			if (result == null)
				result = caseNamed(patternElement);
			if (result == null)
				result = caseIdentifier(patternElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.TASK_DESCRIPTION: {
			TaskDescription taskDescription = (TaskDescription) theEObject;
			T result = caseTaskDescription(taskDescription);
			if (result == null)
				result = casePatternElement(taskDescription);
			if (result == null)
				result = caseCommentable(taskDescription);
			if (result == null)
				result = caseSetFragmentElement(taskDescription);
			if (result == null)
				result = caseNamed(taskDescription);
			if (result == null)
				result = caseIdentifier(taskDescription);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.DESIGN_ELEMENT: {
			DesignElement designElement = (DesignElement) theEObject;
			T result = caseDesignElement(designElement);
			if (result == null)
				result = casePatternElement(designElement);
			if (result == null)
				result = caseSetFragmentElement(designElement);
			if (result == null)
				result = caseNamed(designElement);
			if (result == null)
				result = caseIdentifier(designElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.SET_FRAGMENT: {
			SetFragment setFragment = (SetFragment) theEObject;
			T result = caseSetFragment(setFragment);
			if (result == null)
				result = casePatternElement(setFragment);
			if (result == null)
				result = caseSetFragmentElement(setFragment);
			if (result == null)
				result = caseNamed(setFragment);
			if (result == null)
				result = caseIdentifier(setFragment);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.DESIGN_MODEL: {
			DesignModel designModel = (DesignModel) theEObject;
			T result = caseDesignModel(designModel);
			if (result == null)
				result = casePatternElement(designModel);
			if (result == null)
				result = caseNamed(designModel);
			if (result == null)
				result = caseIdentifier(designModel);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case SpecificationPackage.SET_FRAGMENT_ELEMENT: {
			SetFragmentElement setFragmentElement = (SetFragmentElement) theEObject;
			T result = caseSetFragmentElement(setFragmentElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern Catalog</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern Catalog</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPatternCatalog(DesignPatternCatalog object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Category</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Category</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCategory(Category object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPattern(DesignPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePatternSpecification(PatternSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pattern Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePatternElement(PatternElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskDescription(TaskDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignElement(DesignElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Fragment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetFragment(SetFragment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignModel(DesignModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Fragment Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Fragment Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetFragmentElement(SetFragmentElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifier(Identifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Commentable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Commentable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommentable(Commentable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SpecificationSwitch
