package de.upb.pose.specification;

import de.upb.pose.core.ui.runtime.ActivatorImpl;
import de.upb.pose.core.ui.runtime.IActivator;

public final class Activator extends ActivatorImpl {
	private static IActivator instance;

	public static IActivator get() {
		return Activator.instance;
	}

	@Override
	protected void dispose() {
		Activator.instance = null;
	}

	@Override
	protected void initialize() {
		Activator.instance = this;

		// add images
		for (String path : SpecificationImages.getPaths()) {
			addImage(path);
		}
	}
}
