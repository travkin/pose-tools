/**
 * 
 */
package de.upb.pose.specification.validation;


import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core._DEPRECIATED.validation.IValidationObjectProvider;
import de.upb.pose.specification.CatalogLoader;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.util.PamelaUtil;


/**
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class CatalogProvider implements IValidationObjectProvider
{

   private final IResource resource;


   public CatalogProvider(IResource resource)
   {
      super();
      this.resource = resource;
   }


   /**
    * @see de.upb.pose.core._DEPRECIATED.validation.IValidationObjectProvider#getValidationObjects()
    */
   @Override
   public List<? extends EObject> getValidationObjects()
   {
      CatalogLoader loader = new CatalogLoader();
      DesignPatternCatalog catalog = loader.load(resource);

      return PamelaUtil.getPatternSpecifications(catalog);
   }


}
