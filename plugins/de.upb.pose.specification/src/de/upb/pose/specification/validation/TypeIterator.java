/**
 * 
 */
package de.upb.pose.specification.validation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.util.CrossReferenceUtil;

/**
 * An {@link Iterator} for {@link Type}s used in a DepthFirstSearch. Iterates over the neighbours of the given
 * {@link Type}.
 * 
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class TypeIterator implements Iterator<Type> {

	/** Internal iterator **/
	private final Iterator<Type> iterator;

	/** Store visited objects during the search for neighbours to prevent infinite loops **/
	private final Set<Object> visited = new HashSet<Object>();

	/** Implementation of a {@link Set} that ignores {@code null} when added. **/
	private class IgnoreNullHashSet<T> extends HashSet<T> {
		private static final long serialVersionUID = -8440083190794628169L;

		/**
		 * @see java.util.HashSet#add(java.lang.Object)
		 */
		@Override
		public boolean add(T e) {
			if (e != null)
				return super.add(e);

			return false;
		}
	}

	public TypeIterator(Type type) {
		super();

		// create the internal iterator
		this.iterator = createIterator(type);
	}

	private final Iterator<Type> createIterator(Type type) {
		// get the neighbours of the given type
		Collection<Type> neighbourhood = new IgnoreNullHashSet<Type>();
		addNeighbours(type, neighbourhood);

		// return the iterator
		return neighbourhood.iterator();
	}

	private void addNeighbours(Type type, Collection<Type> neighbourhood) {
		// Need not check or add to visited!

		// Direct
		neighbourhood.add(type.getSuperType());
		neighbourhood.addAll(type.getSubTypes());

		for (Reference reference : type.getReferences()) {
			addNeighbours(reference, neighbourhood);
		}

		for (Operation operation : type.getOperations()) {
			addNeighbours(operation, neighbourhood);
		}

		// CrossReference
		for (EObject operation : CrossReferenceUtil.getReturnTypeOperations(type)) {
			addNeighbours((Operation) operation, neighbourhood);
		}

		for (EObject action : CrossReferenceUtil.getCreateActions(type)) {
			addNeighbours((CreateAction) action, neighbourhood);
		}

		for (EObject parameter : CrossReferenceUtil.getTypeParameters(type)) {
			addNeighbours((Parameter) parameter, neighbourhood);
		}

		for (EObject reference : CrossReferenceUtil.getReference(type)) {
			addNeighbours((Reference) reference, neighbourhood);
		}

		// parent
		// no
	}

	private void addNeighbours(Reference reference, Collection<Type> neighbourhood) {
		if (!visited.contains(reference)) {
			visited.add(reference);

			// Direct
			neighbourhood.add(reference.getType());

			// CrossReference
			for (EObject action : CrossReferenceUtil.getAccessActions(reference)) {
				addNeighbours((VariableAccessAction) action, neighbourhood);
			}

			// parent
			neighbourhood.add(reference.getParentType());
		}
	}

	private void addNeighbours(VariableAccessAction action, Collection<Type> neighbourhood) {
		if (!visited.contains(action)) {
			visited.add(action);

			// Direct
			Variable variable = action.getAccessedVariable();
			if (variable instanceof Reference) {
				addNeighbours((Reference) variable, neighbourhood);
			} else {
				System.out
						.println("TypeIterator#addNeighbours(VariableAccessAction, Collection<Type>) - the variable is not a reference...");
			}

			// CrossReference
			// no

			// parent
			addNeighbours(action.getOperation(), neighbourhood);
		}
	}

	private void addNeighbours(Operation operation, Collection<Type> neighbourhood) {
		if (!visited.contains(operation)) {
			visited.add(operation);

			// Direct
			neighbourhood.add(operation.getType());

			for (Parameter param : operation.getParameters()) {
				addNeighbours(param, neighbourhood);
			}

			for (Action action : operation.getActions()) {
				addNeighbours(action, neighbourhood);
			}

			// CrossReference
			for (EObject action : CrossReferenceUtil.getCallActions(operation)) {
				addNeighbours((CallAction) action, neighbourhood);
			}

			// parent
			neighbourhood.add(operation.getParentType());
		}
	}

	private void addNeighbours(Action action, Collection<Type> neighbourhood) {
		if (action instanceof CreateAction)
			addNeighbours((CreateAction) action, neighbourhood);
		else if (action instanceof CallAction)
			addNeighbours((CallAction) action, neighbourhood);
	}

	private void addNeighbours(Parameter parameter, Collection<Type> neighbourhood) {
		if (!visited.contains(parameter)) {
			visited.add(parameter);

			// Direct
			neighbourhood.add(parameter.getType());

			// CrossReference
			// no

			// parent
			addNeighbours(parameter.getOperation(), neighbourhood);
		}
	}

	private void addNeighbours(CreateAction action, Collection<Type> neighbourhood) {
		if (!visited.contains(action)) {
			visited.add(action);

			// Direct
			neighbourhood.add(action.getInstantiatedType());

			// CrossReference
			// no

			// parent
			addNeighbours(action.getOperation(), neighbourhood);
		}
	}

	private void addNeighbours(CallAction action, Collection<Type> neighbourhood) {
		if (!visited.contains(action)) {
			visited.add(action);

			// Direct
			addNeighbours(action.getCalledOperation(), neighbourhood);

			// CrossReference
			// no

			// parent
			addNeighbours(action.getOperation(), neighbourhood);
		}
	}

	/**
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	/**
	 * @see java.util.Iterator#next()
	 */
	@Override
	public Type next() {
		return iterator.next();
	}

	/**
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		// not used
	}
}
