/**
 * 
 */
package de.upb.pose.specification.validation;


import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core._DEPRECIATED.validation.IValidator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.util.PamelaUtil;


/**
 * An {@link IValidator} that validates that the used {@link Type}s are suitable in a specific
 * context.
 * 
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class TypeValidator implements IValidator
{

   public static final String DIAGNOSTIC_SOURCE = "de.upb.pose.metamodel.custom.validation.TypeValidator";


   /**
    * @see de.upb.pose.core._DEPRECIATED.validation.IValidator#validate(org.eclipse.emf.ecore.EObject,
    *      org.eclipse.core.runtime.IProgressMonitor)
    */
   @Override
   public Diagnostic validate(EObject validationObject, IProgressMonitor progressMonitor)
   {
      progressMonitor.beginTask("Validating", 1);

      // set TaskName
      progressMonitor.setTaskName("Validating suitable types " + getObjectLabel(validationObject));

      BasicDiagnostic diagnostic = createDefaultDiagnostic(validationObject);

      if (validationObject instanceof PatternSpecification)
      {
         PatternSpecification patternSpecification = (PatternSpecification) validationObject;
         for (DesignElement element : PamelaUtil.getAllElements(patternSpecification))
         {
            check(element, diagnostic);
         }
      }

      return diagnostic;
   }


   private BasicDiagnostic createDefaultDiagnostic(EObject eObject)
   {
      return new BasicDiagnostic(DIAGNOSTIC_SOURCE, 0, "Diagnosis of " + getObjectLabel(eObject),
            new Object[] { eObject });
   }


   private void check(DesignElement element, BasicDiagnostic diagnostic)
   {
      if (element instanceof Operation)
         checkOperation((Operation) element, diagnostic);

      if (element instanceof ParameterAssignment)
         checkParameterAssignment((ParameterAssignment) element, diagnostic);

      if (element instanceof ReturnAction)
         checkReturnAction((ReturnAction) element, diagnostic);
   }


   private void checkOperation(Operation operation, BasicDiagnostic diagnostic)
   {
      if (operation.getOverrides() != null)
         check(operation, operation.getParentType(), operation.getOverrides().getParentType(), diagnostic);
   }


   private void checkParameterAssignment(ParameterAssignment parameterAssignment, BasicDiagnostic diagnostic)
   {
      Type typeOfAssignedVariable = PamelaUtil.getType(parameterAssignment.getAssignedVariable());
      check(parameterAssignment, typeOfAssignedVariable, parameterAssignment.getParameter().getType(), diagnostic);
   }


   private void checkReturnAction(ReturnAction returnAction, BasicDiagnostic diagnostic)
   {
      Type typeOfReturnedVariable = PamelaUtil.getType(returnAction.getAccessedVariable());
      check(returnAction, typeOfReturnedVariable, returnAction.getOperation().getType(), diagnostic);
   }


   private void check(DesignElement context, Type subtype, Type supertype, BasicDiagnostic diagnostic)
   {
      if (!PamelaUtil.isSubtype(subtype, supertype))
         diagnostic.add(createDiagnostic(context, subtype, supertype));
   }


   private Diagnostic createDiagnostic(DesignElement context, Type subtype, Type supertype)
   {
      // an indicator of the severity of the problem.
      int severity = Diagnostic.ERROR;
      // the unique identifier of the source.
      String source = DIAGNOSTIC_SOURCE;
      // the source-specific identity code.
      int code = 0;
      // a string describing the problem
      String message = "The 'SuitableType' constraint is violated on '" + getObjectLabel(context) + "'. The type '"
            + getObjectLabel(subtype) + "' is no subtype of '" + getObjectLabel(supertype) + "'.";
      // the data associated with the diagnostic
      Object[] data = new Object[] { context };

      return new BasicDiagnostic(severity, source, code, message, data);
   }


   public String getObjectLabel(EObject eObject)
   {
      if (eObject instanceof DesignElement)
         return ((DesignElement) eObject).getName();
      else if (eObject == null)
         return "null";

      return eObject.toString();
   }
}
