/**
 * 
 */
package de.upb.pose.specification.validation;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Simple implementation of a DepthFirstSearch
 * 
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class DepthFirstSearch<T>
{

   /** Factory that is able to create iterators of the neighbours for a given object **/
   public static interface IteratorFactory<T>
   {
      Iterator<T> create(T object);
   }

   private final IteratorFactory<T> iteratorFactory;

   /** Visited nodes **/
   private final Set<T> visited;


   public DepthFirstSearch(IteratorFactory<T> iteratorFactory)
   {
      super();

      this.iteratorFactory = iteratorFactory;

      this.visited = new HashSet<T>();
   }


   /**
    * Performs a DepthFirstSearch on the given {@code root}
    * 
    * @param root
    */
   public void perform(T root)
   {
      dfsRecursive(root);
   }


   /**
    * Returns all visited nodes
    * 
    * @return
    */
   public Set<T> getVisited()
   {
      return this.visited;
   }


   private void dfsRecursive(T node)
   {
      // visit the node
      visited.add(node);

      // visit his neighbours
      Iterator<T> it = iteratorFactory.create(node);
      while (it.hasNext())
      {
         T expanded = it.next();
         if (!visited.contains(expanded))
         {
            dfsRecursive(expanded);
         }
      }
   }

}
