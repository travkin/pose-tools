/**
 * 
 */
package de.upb.pose.specification.validation;


import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core._DEPRECIATED.validation.IValidator;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.validation.DepthFirstSearch.IteratorFactory;


/**
 * A {@link IValidator} that validates whether the {@link Type}s of a {@link PatternSpecification}
 * are connected to each other.
 * 
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class ConnectivityValidator implements IValidator
{

   public static final String DIAGNOSTIC_SOURCE = "de.upb.pose.metamodel.custom.validation.ConnectivityValidator";

   private final int severity;


   private class TypeIteratorFactory implements IteratorFactory<Type>
   {

      /**
       * @see de.upb.pose.specification.validation.DepthFirstSearch.IteratorFactory#create(java.lang.Object)
       */
      @Override
      public Iterator<Type> create(Type object)
      {
         return new TypeIterator(object);
      }

   }


   /**
    * 
    * @param severity Specifies how to declare connectivity problems. Use {@link Diagnostic} for
    *           configuration, e.g. {@link Diagnostic#ERROR} or {@link Diagnostic#WARNING}.
    */
   public ConnectivityValidator(int severity)
   {
      super();
      this.severity = severity;
   }


   /**
    * @see de.upb.pose.core._DEPRECIATED.validation.IValidator#validate(org.eclipse.emf.ecore.EObject,
    *      org.eclipse.core.runtime.IProgressMonitor)
    */
   @Override
   public Diagnostic validate(EObject validationObject, IProgressMonitor progressMonitor)
   {
      progressMonitor.beginTask("Validating", 1);

      // set TaskName
      progressMonitor.setTaskName("Validating connectivity of " + getObjectLabel(validationObject));

      BasicDiagnostic diagnostic = createDefaultDiagnostic(validationObject);

      if (validationObject instanceof PatternSpecification)
      {
         Set<Type> disconnectedTypes = getAllTypes((PatternSpecification) validationObject);

         if (!disconnectedTypes.isEmpty())
         {
            // perform a DFS
            DepthFirstSearch<Type> dfs = new DepthFirstSearch<Type>(new TypeIteratorFactory());
            dfs.perform(disconnectedTypes.iterator().next());

            // remove visited from all types
            disconnectedTypes.removeAll(dfs.getVisited());

            // remaining types not visited during DFS -> connectivity problem
            if (!disconnectedTypes.isEmpty())
            {
               Diagnostic newDiagnostic = createDiagnostic(disconnectedTypes);
               diagnostic.add(newDiagnostic);
            }
         }
      }

      return diagnostic;
   }

   private BasicDiagnostic createDefaultDiagnostic(EObject eObject)
   {
      return new BasicDiagnostic(DIAGNOSTIC_SOURCE, 0, "Diagnosis of " + getObjectLabel(eObject),
            new Object[] { eObject });
   }


   public String getObjectLabel(EObject eObject)
   {
      return eObject.toString();
   }


   private Set<Type> getAllTypes(PatternSpecification patternSpecification)
   {
      Set<Type> allTypes = new HashSet<Type>();

      for (PatternElement item : patternSpecification.getPatternElements())
      {
         if (item instanceof Type)
            allTypes.add((Type) item);
      }

      return allTypes;
   }


   private Diagnostic createDiagnostic(Collection<? extends EObject> disconnectedObjects)
   {
      // an indicator of the severity of the problem.
      int severity = this.severity;
      // the unique identifier of the source.
      String source = DIAGNOSTIC_SOURCE;
      // the source-specific identity code.
      int code = 0;
      // a string describing the problem
      String message = "The specification is not connected.";
      // the data associated with the diagnostic
      Object[] data = disconnectedObjects.toArray();

      return new BasicDiagnostic(severity, source, code, message, data);
   }
}
