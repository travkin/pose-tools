/**
 * 
 */
package de.upb.pose.specification.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.specification.Category;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.access.AccessFactory;
import de.upb.pose.specification.access.AccessType;
import de.upb.pose.specification.actions.ActionsFactory;
import de.upb.pose.specification.actions.NullVariable;
import de.upb.pose.specification.types.AbstractionType;
import de.upb.pose.specification.types.DataType;
import de.upb.pose.specification.types.PrimitiveType;
import de.upb.pose.specification.types.TypesFactory;

/**
 * @author Dietrich Travkin
 */
public class SpecificationUtil {
	
	public static List<PatternElement> collectAllPatternElements(PatternSpecification specification)
	{
		Assert.isNotNull(specification);
		
		ArrayList<PatternElement> collection = new ArrayList<PatternElement>();
		TreeIterator<Object> iterator = EcoreUtil.getAllContents(specification, true);
		while (iterator.hasNext())
		{
			Object obj = iterator.next();
			
			Assert.isTrue(obj instanceof PatternElement);
			
			collection.add((PatternElement) obj);
		}
		return collection;
	}
	
	/**
	 * Checks if the given pattern role (pattern specification element) is in a set fragment.
	 * 
	 * @param patternElement
	 * @return <code>true</code> if given pattern role (pattern specification element) is in a set fragment,
	 *         <code>false</code> otherwise.
	 */
	public static boolean inASetFragment(PatternElement patternElement) {
		if (patternElement instanceof SetFragmentElement) {
			SetFragmentElement setElement = (SetFragmentElement) patternElement;
			return (!setElement.getContainingSets().isEmpty());
		}
		return false;
	}

	/**
	 * Determines if the two given pattern roles (pattern specification elements) are in the same set fragment area.
	 * They are in the same set fragment area if (a) both are in no set fragment at all or (b) both are in exactly the
	 * same set fragments, e.g. in the same intersection of two set fragments or in the same non-intersecting area of a
	 * set fragment.
	 * 
	 * @param patternElement1 a pattern role (pattern specification element)
	 * @param patternElement2 another pattern role (pattern specification element)
	 * @return <code>true</code> if the two pattern role are in the same set fragment area, <code>false</code>
	 *         otherwise.
	 */
	public static boolean inTheSameSetFragmentArea(PatternElement patternElement1, PatternElement patternElement2) {
		if (!inASetFragment(patternElement1) && !inASetFragment(patternElement2)) {
			return true;
		} else if (inASetFragment(patternElement1) && !inASetFragment(patternElement2)) {
			return false;
		} else if (!inASetFragment(patternElement1) && inASetFragment(patternElement2)) {
			return false;
		} else // both are in set fragments
		{
			SetFragmentElement setElement1 = (SetFragmentElement) patternElement1;
			SetFragmentElement setElement2 = (SetFragmentElement) patternElement2;
			if (setElement1.getContainingSets().size() != setElement2.getContainingSets().size()) {
				return false;
			} else {
				for (SetFragment setFragment : setElement1.getContainingSets()) {
					if (!setElement2.getContainingSets().contains(setFragment)) {
						return false;
					}
				}
				for (SetFragment setFragment : setElement2.getContainingSets()) {
					if (!setElement1.getContainingSets().contains(setFragment)) {
						return false;
					}
				}
				return true;
			}
		}
	}

	/**
	 * Determines if the two given pattern roles (pattern specification elements) are in disjoint set fragments, i.e.
	 * (a) they are in no set fragments at all, (b) only one of them is in a set fragment, (c) both are in set fragments
	 * that do not intersect with each other.
	 * 
	 * @param patternElement1 a pattern role (pattern specification element)
	 * @param patternElement2 another pattern role (pattern specification element)
	 * @return <code>true</code> if the two pattern role are in disjoint set fragments, <code>false</code> otherwise.
	 */
	public static boolean inDisjointSetFragments(PatternElement patternElement1, PatternElement patternElement2) {
		if (inASetFragment(patternElement1) && inASetFragment(patternElement2)) // both are in a set fragment
		{
			SetFragmentElement setElement1 = (SetFragmentElement) patternElement1;
			SetFragmentElement setElement2 = (SetFragmentElement) patternElement2;

			for (SetFragment setFragment : setElement1.getContainingSets()) {
				List<SetFragment> setFragmentsContainingSetElement2 = collectAllDirectlyAndIndirecltyContainingSetFragments(setElement2);
				if (setFragmentsContainingSetElement2.contains(setFragment)) {
					return false;
				}
			}
			for (SetFragment setFragment : setElement2.getContainingSets()) {
				List<SetFragment> setFragmentsContainingSetElement1 = collectAllDirectlyAndIndirecltyContainingSetFragments(setElement1);
				if (setFragmentsContainingSetElement1.contains(setFragment)) {
					return false;
				}
			}
			return true;
		} else // one or both are not in a set fragment
		{
			return true;
		}
	}

	/**
	 * Recursively collects all set fragments that directly or indirectly contain the given set element.
	 * 
	 * @param setElement
	 * @return all set fragments that directly or indirectly contain the given set element
	 */
	public static List<SetFragment> collectAllDirectlyAndIndirecltyContainingSetFragments(SetFragmentElement setElement) {
		ArrayList<SetFragment> containingsSetFragments = new ArrayList<SetFragment>();
		addContainingSetFragments(setElement, containingsSetFragments);
		return containingsSetFragments;
	}

	private static void addContainingSetFragments(SetFragmentElement setElement, ArrayList<SetFragment> setFragments) {
		for (SetFragment setFragment : setElement.getContainingSets()) {
			setFragments.add(setFragment);
			addContainingSetFragments(setFragment, setFragments);
		}
	}

	/**
	 * Determines whether pattern role 1 (pattern specification element 1) is in a set fragment area that completely
	 * contains the set fragment area of pattern role 2 (pattern specification element 2) as direct child area (no
	 * indirect containment, e.g. set fragment in set fragment in set fragment).
	 * 
	 * @param patternElement1 a pattern role (pattern specification element)
	 * @param patternElement2 another pattern role (pattern specification element
	 * @return <code>true</code> if the pattern role 2 is in a set fragment area that is completely contained in the set
	 *         fragment area of pattern role 1, <code>false</code> otherwise.
	 */
	public static boolean setFragmentAreaOfElement1DirectlyContainsSetFragmentAreaOfElement2(
			PatternElement patternElement1, PatternElement patternElement2) {
		if (inASetFragment(patternElement1) && inASetFragment(patternElement2) // both are in set fragments
				&& !inTheSameSetFragmentArea(patternElement1, patternElement2)) // the set fragment areas are not the
																				// same
		{
			SetFragmentElement setElement1 = (SetFragmentElement) patternElement1;
			SetFragmentElement setElement2 = (SetFragmentElement) patternElement2;

			boolean completelyContainsSetFragments = true;
			for (SetFragment setFragment : setElement1.getContainingSets()) {
				// set fragment completely contains the set fragments of set element 2
				if (!directlyContainsAllSetFragments(setFragment, setElement2.getContainingSets())) {
					completelyContainsSetFragments = false;
					break;
				}
			}

			if (completelyContainsSetFragments) {
				return true;
			} else {
				// check if set fragments intersect and set fragment area of set element 2 is completely contained
				// in the set fragment area of set element 1
				if (setElement1.getContainingSets().size() == 1 && setElement2.getContainingSets().size() == 2) {
					SetFragment parentFragment = setElement1.getContainingSets().get(0);
					if (setElement2.getContainingSets().contains(parentFragment)) {
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}

	private static boolean directlyContainsAllSetFragments(SetFragment setFragment, List<SetFragment> setFragments) {
		for (SetFragment someSetFragment : setFragments) {
			if (!setFragment.getContainedElements().contains(someSetFragment)) {
				return false;
			}
		}
		return true;
	}

	public static boolean directlyOrIndirectlyContaints(SetFragment parentSetFragment, SetFragment childSetFragment) {
		if (parentSetFragment.getContainedElements().contains(childSetFragment)) {
			return true;
		} else {
			for (SetFragmentElement setElement : parentSetFragment.getContainedElements()) {
				if (setElement instanceof SetFragment) {
					SetFragment tempParent = (SetFragment) setElement;
					if (directlyOrIndirectlyContaints(tempParent, childSetFragment)) {
						return true;
					}
				}
			}
			return false;
		}
	}

	/**
	 * Determines whether pattern role 1 (pattern specification element 1) is in a set fragment that directly or
	 * indirectly and completely contains the set fragment of pattern role 2 (pattern specification element 2).
	 * 
	 * @param patternElement1 a pattern role (pattern specification element)
	 * @param patternElement2 another pattern role (pattern specification element
	 * @return <code>true</code> if the pattern role 1 is in a set fragment that completely contains the set fragment of
	 *         pattern role 2, <code>false</code> otherwise.
	 */
	public static boolean setFragmentsOfElement1ContainSetFragmentsOfElement2(PatternElement patternElement1,
			PatternElement patternElement2) {
		if (inASetFragment(patternElement1) && inASetFragment(patternElement2)) {
			SetFragmentElement setElement1 = (SetFragmentElement) patternElement1;
			SetFragmentElement setElement2 = (SetFragmentElement) patternElement2;

			for (SetFragment parentSetFragment : setElement1.getContainingSets()) {
				for (SetFragment childSetFragment : setElement2.getContainingSets()) {
					if (!directlyOrIndirectlyContaints(parentSetFragment, childSetFragment)) {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}

	public static List<SetFragmentElement> getRootSetElements(PatternSpecification specification) {
		List<SetFragmentElement> elements = new ArrayList<SetFragmentElement>();
		for (PatternElement element : specification.getPatternElements()) {
			if (element instanceof SetFragmentElement && ((SetFragmentElement) element).getContainingSets().isEmpty()) {
				elements.add((SetFragmentElement) element);
			}
		}
		return elements;
	}

	public static List<Category> getRootCategories(DesignPatternCatalog element) {
		List<Category> elements = new ArrayList<Category>();
		for (Category category : element.getCategories()) {
			if (category.getParent() == null) {
				elements.add(category);
			}
		}
		return elements;
	}

	public static List<DesignPattern> getRootPatterns(DesignPatternCatalog element) {
		List<DesignPattern> elements = new ArrayList<DesignPattern>();
		for (DesignPattern pattern : element.getPatterns()) {
			if (pattern.getCategories().isEmpty()) {
				elements.add(pattern);
			}
		}
		return elements;
	}

	public static DesignPatternCatalog getCatalog(Category element) {
		DesignPatternCatalog catalog = element.getCatalog();
		if (catalog == null) {
			return getCatalog(element.getParent());
		}
		return catalog;
	}

	public static DesignPatternCatalog getCatalog(PatternSpecification element) {
		return element.getPattern().getCatalog();
	}

	public static PatternSpecification getSpecification(EObject element) {
		Assert.isNotNull(element);

		EObject container = element;
		while (container != null) {
			if (container instanceof PatternSpecification) {
				return (PatternSpecification) container;
			}
			container = container.eContainer();
		}

		throw new RuntimeException();
	}
	
	public static DesignPatternCatalog createEmptyInitializedCatalog() {
		// create catalog
		DesignPatternCatalog catalog = SpecificationFactory.eINSTANCE.createDesignPatternCatalog();
		catalog.setName("Design Patterns Catalog");

		// add singleton elements
		createPrimitiveTypes(catalog);
		createAccessTypes(catalog);
		createNullVariable(catalog);

		return catalog;
	}
	
	/**
	 * Creates all primitive types and adds them to the given catalog.
	 * @param catalog the pattern catalog where the new types are to be added to
	 */
	public static void createPrimitiveTypes(DesignPatternCatalog catalog)
	{
		if (catalog.getPrimitiveTypes().isEmpty())
		{
			PrimitiveType ptype = createPrimitiveType(catalog);
			ptype.setName("boolean");
			ptype.setType(DataType.BOOLEAN);
			ptype = createPrimitiveType(catalog);
			ptype.setName("integer");
			ptype.setType(DataType.INTEGER);
			ptype = createPrimitiveType(catalog);
			ptype.setName("real");
			ptype.setType(DataType.REAL);
			ptype = createPrimitiveType(catalog);
			ptype.setName("string");
			ptype.setType(DataType.STRING);
		}
	}
	
	private static PrimitiveType createPrimitiveType(DesignPatternCatalog catalog)
	{
		PrimitiveType ptype = TypesFactory.eINSTANCE.createPrimitiveType();
		ptype.setID(EcoreUtil.generateUUID());
		ptype.setAbstraction(AbstractionType.CONCRETE);
		ptype.setCanBeGenerated(true);
		catalog.getPrimitiveTypes().add(ptype);
		return ptype;
	}
	
	/**
	 * Creates all access types and adds them to the given catalog.
	 * @param catalog the pattern catalog where the new types are to be added to
	 */
	public static void createAccessTypes(DesignPatternCatalog catalog)
	{
		if (catalog.getAccessTypes().isEmpty())
		{
			AccessType atype = AccessFactory.eINSTANCE.createAnyAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("use");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createReferAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("point to");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createSpecializeAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("specialize");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createAccessAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("access"); // "instantiate, call, read, or write access"
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createInstantiateAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("instantiate");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createCallAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("call");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createReadAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("read");
			catalog.getAccessTypes().add(atype);
			
			atype = AccessFactory.eINSTANCE.createWriteAccessType();
			atype.setID(EcoreUtil.generateUUID());
			atype.setName("write");
			catalog.getAccessTypes().add(atype);
		}
	}
	
	/**
	 * Creates the unique null variable and adds it to the given catalog.
	 * @param catalog the pattern catalog where the variable is to be added to
	 */
	public static void createNullVariable(DesignPatternCatalog catalog)
	{
		if (catalog.getNullVariable() == null) {
			NullVariable nullVariable = ActionsFactory.eINSTANCE.createNullVariable();
			nullVariable.setID(EcoreUtil.generateUUID());
			nullVariable.setName("null");
			nullVariable.setCanBeGenerated(true);
			catalog.setNullVariable(nullVariable);
		}
	}
	
	public static String getAccessTypeLabel(AccessType accessType)
	{
		String label = accessType.getName();
		if (label == null || label.length() == 0)
		{
			label = "No Label [" + accessType.eClass().getName() +"]";
		}
		return label;
	}
	
	public static String getPrimitiveTypeLabel(PrimitiveType primitiveType)
	{
		String label = primitiveType.getName();
		if (label == null || label.length() == 0)
		{
			label = "No Label [" + primitiveType.eClass().getName() +"]";
		}
		return label;
	}
}
