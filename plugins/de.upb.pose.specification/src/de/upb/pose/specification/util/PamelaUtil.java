package de.upb.pose.specification.util;


import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core.Named;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ResultVariable;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;


public class PamelaUtil
{
   /**
    * A policy for the selection of pattern elements.
    * 
    * @author Maik
    * @author Last editor: $Author$
    * @version $Revision$ $Date$
    * 
    */
   public static abstract class ElementSelectionPolicy
   {
      /**
       * Specifies, which elements have to be selected.
       * 
       * @param element An {@link DesignElement}.
       * @return True, if the element should be selected.
       */
      public abstract boolean isSelected(DesignElement element);
   }


   /**
    * Visits all elements of the {@link PatternSpecification} and
    * 
    * @param patternSpec The {@link PatternSpecification}
    * @param selectionStrategy The {@link ElementSelectionPolicy}
    * @return
    */
   public static List<DesignElement> selectElements(PatternSpecification patternSpec,
         ElementSelectionPolicy elementSelectionPolicy)
   {
      List<DesignElement> selectedElements = new Vector<DesignElement>();
      for (DesignElement element : getAllElements(patternSpec))
      {
         if (elementSelectionPolicy.isSelected(element))
         {
            selectedElements.add(element);
         }
      }
      return selectedElements;
   }


   /**
    * 
    * 
    * @param patternSpec
    * @return
    */
   public static List<DesignElement> getAllElements(PatternSpecification patternSpec)
   {
      List<DesignElement> allElements = new Vector<DesignElement>();

      TreeIterator<EObject> contentTreeIterator = patternSpec.eAllContents();
      while (contentTreeIterator.hasNext())
      {
         EObject eObject = contentTreeIterator.next();
         if (eObject instanceof DesignElement)
         {
            DesignElement element = (DesignElement) eObject;
            allElements.add(element);
         }
      }
      return allElements;
   }


   /**
    * Returns the {@link Type} of the given {@link Variable}
    * 
    * @param variable
    * @return
    */
   public static Type getType(Variable variable)
   {
      if (variable instanceof Reference)
         return ((Reference) variable).getType();
      else if (variable instanceof Parameter)
         return ((Parameter) variable).getType();
      else if (variable instanceof ResultVariable)
         return getType((ResultVariable) variable);
      else if (variable instanceof SelfVariable)
         return ((SelfVariable) variable).getType();
      else if (variable == null)
         return null;

      throw new IllegalArgumentException("Unknown Type for Variable " + variable);
   }


   /**
    * Returns the {@link Type} of the given {@link ResultVariable}
    * 
    * @param resultVariable
    * @return
    */
   public static Type getType(ResultVariable resultVariable)
   {
      if (resultVariable.getAction() instanceof CreateAction)
         return ((CreateAction) resultVariable.getAction()).getInstantiatedType();
      else if (resultVariable.getAction() instanceof CallAction)
         return ((CallAction) resultVariable.getAction()).getCalledOperation().getType();

      throw new IllegalArgumentException("Unknown parentAction of ResultVariable " + resultVariable);
   }


   public static String getLongDisplayName(PatternSpecification patternVariant)
   {
      return patternVariant.getPattern().getName() + ": " + patternVariant.getName();
   }


   public static String getShortDisplayName(PatternSpecification patternVariant)
   {
      return patternVariant.getPattern().getName();
   }

   /**
    * Get the child items of a pattern composite, which is a {@code PatternSpecification} or a
    * {@code Set}. For {@code PatternSpecification}, only those items are returned which are not
    * contained in a set.
    * 
    * @param patternComposite The containing pattern composite.
    * @return A list of the contained child items.
    */
   public static List<SetFragmentElement> getChildItems(Named patternComposite)
   {
      List<SetFragmentElement> items = null;
      if (patternComposite instanceof PatternSpecification)
      {
         items = new Vector<SetFragmentElement>();
         PatternSpecification patternSpec = (PatternSpecification) patternComposite;
         for (PatternElement specItem : patternSpec.getPatternElements())
         {
        	 if(specItem instanceof SetFragmentElement) {
            // add only those items which have no other parents
            SetFragmentElement setItem = (SetFragmentElement) specItem;
            if (setItem.getContainingSets().isEmpty())
            {
               items.add(setItem);
            }else {
				System.out.println("PamelaUtil#getChildItems(): could not add item " + specItem);
			}}
         }
      }
      else if (patternComposite instanceof SetFragment)
      {
         SetFragment set = (SetFragment) patternComposite;
         items = set.getContainedElements();
      }
      if (items == null)
      {
         items = new Vector<SetFragmentElement>();
      }
      return items;
   }


   public static String getName(Named patternComposite)
   {

      if (patternComposite instanceof PatternSpecification)
      {
         return "Root";
      }
      else if (patternComposite instanceof SetFragment)
      {
         SetFragment set = (SetFragment) patternComposite;
         return set.getName();
      }
      else
      {
         return "Unknown";
      }

   }


   /**
    * Returns the {@link PatternSpecification} for the given {@code object}.
    * 
    * @param object The {@link EObject} of interest
    * @return The {@link PatternSpecification} for the given {@code object}
    */
   public static PatternSpecification getPatternSpecification(EObject object)
   {
      EObject container = object;
      while (container != null)
      {
         if (container instanceof PatternSpecification)
            return (PatternSpecification) container;
         else
            container = container.eContainer();
      }

      throw new IllegalStateException("The EObject " + object + " is not contained in a patternSpecification.");
   }


   public static Reference getOppositeReference(Reference reference)
   {
      Reference sourceReference = reference.getSourceReference();
      if (sourceReference != null)
      {
         return sourceReference;
      }
      return reference.getTargetReference();
   }


   /**
    * Returns {@code true} if the given {@code subtype} is a subtype of the given {@code supertype}
    * 
    * @param subtype
    * @param supertype
    * @return
    */
   public static boolean isSubtype(Type subtype, Type supertype)
   {
      Type type = subtype;
      while (type != supertype && type != null)
      {
         type = type.getSuperType();
      }

      return type == supertype;
   }


   public static List<PatternSpecification> getPatternSpecifications(DesignPatternCatalog patternCatalog)
   {
      List<PatternSpecification> patternSpecifications = new LinkedList<PatternSpecification>();

      for (DesignPattern pattern : patternCatalog.getPatterns())
      {
         for (PatternSpecification patternVariant : pattern.getSpecifications())
         {
            patternSpecifications.add(patternVariant);
         }
      }

      return patternSpecifications;
   }


   public static boolean isBidirectional(Reference reference)
   {
      if (reference.getSourceReference() != null)
      {
         return true;
      }
      if (reference.getTargetReference() != null)
      {
         return true;
      }
      return false;
   }

}
