/**
 * 
 */
package de.upb.pose.specification.util;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;

import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ResultAction;
import de.upb.pose.specification.actions.SelfVariable;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.types.Feature;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class VisibleVariablesCollector {

	private static final VisibleVariablesCollector instance = new VisibleVariablesCollector();
	
	private VisibleVariablesCollector() { }
	
	public static VisibleVariablesCollector get() {
		return instance;
	}
	
	private Set<Variable> createEmtpySet() {
		return new HashSet<Variable>();
	}
	
	
	public Set<Variable> findVariablesVisibleFor(PatternSpecification specification) {
		Set<Variable> variables = createEmtpySet();
		addVariablesVisibleFor(specification, variables);
		return variables;
	}
	
	public Set<Variable> findVariablesVisibleFor(Type type) {
		Set<Variable> variables = createEmtpySet();
		addVariablesVisibleFor(type, variables);
		return variables;
	}
	
	public Set<Variable> findVariablesVisibleFor(Operation operation) {
		Set<Variable> variables = createEmtpySet();
		addVariablesVisibleFor(operation, variables);
		return variables;
	}
	
	public Set<Variable> findVariablesVisibleFor(Action action) {
		Set<Variable> variables = createEmtpySet();
		addVariablesVisibleFor(action, variables);
		return variables;
	}
	
	private void addVariablesVisibleFor(PatternSpecification specification, Set<Variable> variables) {
		// null variable
		DesignPatternCatalog catalog = specification.getPattern().getCatalog();
		if (catalog.getNullVariable() != null) {
			variables.add(catalog.getNullVariable());
		}
	}
	
	private void addVariablesVisibleFor(Type type, Set<Variable> variables) {
		// self variable
		if (type.getSelfVariable() != null && !containsSelfVariable(variables)) {
			variables.add(type.getSelfVariable());
		}
		
		// features
		for (Feature feature: type.getFeatures()) {
			addVariableIfNoContainedVariableHasSameName(feature, variables);
		}
		
		// recursively collect other visible variables
		if (type.getSuperType() != null) {
			addVariablesVisibleFor(type.getSuperType(), variables);
		} else {
			addVariablesVisibleFor(type.getSpecification(), variables);
		}
	}
	
	private boolean containsSelfVariable(Set<Variable> variables) {
		for (Variable variable: variables) {
			if (variable instanceof SelfVariable) {
				return true;
			}
		}
		return false;
	}
	
	private void addVariablesInOperationHierarchyVisibleFor(Operation operation, Set<Variable> variables) {
		// parameters
		for (Parameter param : operation.getParameters()) {
			addVariableIfNoContainedVariableHasSameName(param, variables);
		}
		
		// recursively collect inherited parameters
		if (operation.getOverrides() != null) {
			addVariablesInOperationHierarchyVisibleFor(operation.getOverrides(), variables);
		}
	}
	
	private void addVariablesVisibleFor(Operation operation, Set<Variable> variables) {
		// parameters
		addVariablesInOperationHierarchyVisibleFor(operation, variables);
		
		// recursively collect other visible variables
		addVariablesVisibleFor(operation.getParentType(), variables);
	}
	
	private void addVariablesVisibleFor(Action action, Set<Variable> variables) {
		// result variable
		Action predecessorAction = findPredecessor(action);
		if (predecessorAction != null && predecessorAction instanceof ResultAction) {
			ResultAction resultAction = (ResultAction) predecessorAction;
			if (resultAction.getResultVariable() != null) {
				addVariableIfNoContainedVariableHasSameName(resultAction.getResultVariable(), variables);
			}
		}
		
		// recursively collect other visible variables
		if (predecessorAction != null) {
			addVariablesVisibleFor(predecessorAction, variables);
		} else {
			addVariablesVisibleFor(action.getOperation(), variables);
		}
	}
	
	private Action findPredecessor(Action action) {
		EList<Action> actions = action.getOperation().getActions();
		int index = actions.indexOf(action);
		if (index != -1 && index > 0) {
			return actions.get(index - 1);
		}
		return null;
	}
	
	private void addVariableIfNoContainedVariableHasSameName(Variable variable, Set<Variable> variables) {
		boolean variableWithSameNameExists = false;
		if (variable.getName() != null) {
			for (Variable var: variables) {
				if (variable.getName().equals(var.getName())) {
					variableWithSameNameExists = true;
				}
			}
		}
		
		if (!variableWithSameNameExists) {
			variables.add(variable);
		}
	}
}
