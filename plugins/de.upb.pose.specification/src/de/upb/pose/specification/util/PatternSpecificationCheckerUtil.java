package de.upb.pose.specification.util;


import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.types.Type;


public class PatternSpecificationCheckerUtil
{

   public static List<EObject> check(PatternSpecification patternSpecification)
   {
      List<EObject> erroneousObjects = new LinkedList<EObject>();

      for (DesignElement element : PamelaUtil.getAllElements(patternSpecification))
      {
         // check type
         if (element instanceof Type)
         {
            Type type = (Type) element;

            if (isTypeErroneous(type))
               erroneousObjects.add(type);
         }
      }

      return erroneousObjects;
   }


   private static boolean isTypeErroneous(Type type)
   {
      return type.getSubTypes().isEmpty() && type.getSuperType() == null
            && CrossReferenceUtil.getReference(type).isEmpty() && type.getReferences().isEmpty();
   }

}
