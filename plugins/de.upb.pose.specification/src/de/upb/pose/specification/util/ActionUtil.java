/**
 * 
 */
package de.upb.pose.specification.util;


import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.types.Operation;


/**
 * Contains some helpful methods on {@link Action}s
 * 
 * @author andreb
 * @author Last editor: $Author$
 * @version $Revision$ $Date$
 * 
 */
public class ActionUtil
{

   /**
    * Returns the first {@link Action} of the given {@link Operation}, if any.
    * 
    * @param operation
    * @return
    */
   public static Action getFirstAction(Operation operation)
   {
      if (hasFirstAction(operation))
      {
         return operation.getActions().get(0);
      }

      return null;
   }


   /**
    * Returns {@code true} if the given {@link Operation} has a first {@link Action}.
    * 
    * @param operation
    * @return
    */
   public static boolean hasFirstAction(Operation operation)
   {
      return !operation.getActions().isEmpty();
   }


   /**
    * Returns {@code true} if the given {@link Action} has a predecessor {@link Action} in his
    * parent {@link Operation}.
    * 
    * @param action
    * @return
    */
   public static boolean hasPredecessorAction(Action action)
   {
      return !isFirstAction(action);
   }


   /**
    * Returns {@code true} if the given {@link Action} is the first Action of his parent
    * {@link Operation}.
    * 
    * @param action
    * @return
    */
   public static boolean isFirstAction(Action action)
   {
      Operation operation = action.getOperation();
      int index = operation.getActions().indexOf(action);

      return index == 0;
   }


   /**
    * Returns {@code true} if the given {@link Action} has a successor {@link Action} in his parent
    * {@link Operation}.
    * 
    * @param action
    * @return
    */
   public static boolean hasSuccessorAction(Action action)
   {
      Operation operation = action.getOperation();
      int index = operation.getActions().indexOf(action);

      return index >= 0 && index + 1 < operation.getActions().size();
   }


   /**
    * Returns the successor {@link Action} of the given {@link Action} from his parent
    * {@link Operation}, if any.
    * 
    * @param action
    * @return
    */
   public static Action getSuccessorAction(Action action)
   {
      Operation operation = action.getOperation();
      int index = operation.getActions().indexOf(action);

      if (index >= 0 && index + 1 < operation.getActions().size())
      {
         return operation.getActions().get(index + 1);
      }

      return null;
   }


   /**
    * Returns the predecessor {@link Action} of the given {@link Action} from his parent
    * {@link Operation}, if any.
    * 
    * @param action
    * @return
    */
   public static Action getPredecessorAction(Action action)
   {
      Operation operation = action.getOperation();
      int index = operation.getActions().indexOf(action);

      if (index >= 1)
      {
         return operation.getActions().get(index - 1);
      }

      return null;
   }


   /**
    * Reorders the {@link Action}s of an {@link Operation} so that the Action {@code before} is
    * directly after the Action {@code after}.
    * 
    * <p>
    * Before call of {@link ActionUtil#reorderActions(Action, Action)}
    * </p>
    * <ul>
    * <li>...</li>
    * <li>after</li>
    * <li>...</li>
    * <li>before</li>
    * <li>...</li>
    * </ul>
    * 
    * <p>
    * After call of {@link ActionUtil#reorderActions(Action, Action)}
    * </p>
    * <ul>
    * <li>...</li>
    * <li>before</li>
    * <li>after</li>
    * <li>...</li>
    * </ul>
    * 
    * 
    * @param before
    * @param after
    */
   public static void reorderActions(Action before, Action after)
   {
      // the parent operartion
      Operation operation = before.getOperation();

      // move on top if one action equals null
      if (after == null || before == null)
      {
         operation.getActions().move(0, before);
      }
      else
      {
         // get move index
         int indexOfAfter = operation.getActions().indexOf(after);
         int indexOfBefore = operation.getActions().indexOf(before);
         int moveToIndex = indexOfAfter;
         if (indexOfBefore > indexOfAfter)
            moveToIndex++;

         // move
         if (moveToIndex < operation.getActions().size())
         {
            operation.getActions().move(moveToIndex, before);
         }
         else
         {
            operation.getActions().move(operation.getActions().size() - 1, before);
         }
      }
   }
}
