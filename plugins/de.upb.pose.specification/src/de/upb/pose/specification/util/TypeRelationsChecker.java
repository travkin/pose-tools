/**
 * 
 */
package de.upb.pose.specification.util;

import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 */
public class TypeRelationsChecker {

	public static boolean isEqualOrSubtypeOf(Type subtype, Type supertype) {
		if (subtype.equals(supertype)) {
			return true;
		}
		
		while (subtype.getSuperType() != null) {
			Type type = subtype.getSuperType();
			return isEqualOrSubtypeOf(type, supertype);
		}
		return false;
	}
	
}
