package de.upb.pose.specification.util;

import org.eclipse.gef.dnd.SimpleObjectTransfer;

public class PatternVariantObjectTransfer extends SimpleObjectTransfer {
	private static PatternVariantObjectTransfer instance;

	private final String[] names;

	private final int[] ids;

	public PatternVariantObjectTransfer() {
		StringBuilder builder = new StringBuilder();

		builder.append(PatternVariantObjectTransfer.class.getCanonicalName());
		builder.append(System.currentTimeMillis());
		builder.append(hashCode());

		names = new String[] { builder.toString() };
		ids = new int[] { registerType(builder.toString()) };
	}

	public static PatternVariantObjectTransfer getInstance() {
		if (instance == null) {
			instance = new PatternVariantObjectTransfer();
		}
		return instance;
	}

	@Override
	protected int[] getTypeIds() {
		return ids;
	}

	@Override
	protected String[] getTypeNames() {
		return names;
	}
}
