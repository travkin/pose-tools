package de.upb.pose.specification.util;

import org.eclipse.emf.ecore.EClass;

import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.actions.Action;
import de.upb.pose.specification.actions.ActionsPackage;

public final class SpecificationTextUtil {
	
	private static final String NULL = "null"; //$NON-NLS-1$

	private SpecificationTextUtil() {
		// hide constructor
	}

	public static String get(PatternSpecification element) {
		if (element != null && element.getPattern() != null) {
			StringBuilder builder = new StringBuilder();

			// add pattern name
			builder.append(element.getPattern().getName());

			// add specification name
			if (element.getPattern().getSpecifications().size() > 1) {
				builder.append(':');
				builder.append(' ');
				builder.append(element.getName());
			}

			return builder.toString();
		}
		return NULL;
	}
	
	public static String getName(Action action) {
		if (action != null) {
			EClass actionClass = action.eClass();
			
			switch (actionClass.getClassifierID()) {
			
			case ActionsPackage.CALL_ACTION:
				return "call";
			case ActionsPackage.REDIRECT_ACTION:
				return "redirect";
			case ActionsPackage.DELEGATE_ACTION:
				return "delegate";	
				
			case ActionsPackage.CREATE_ACTION:
				return "create";
			case ActionsPackage.PRODUCE_ACTION:
				return "produce";
			
			case ActionsPackage.READ_ACTION:
				return "read";
			case ActionsPackage.RETURN_ACTION:
				return "return";
			case ActionsPackage.WRITE_ACTION:
				return "write";
			case ActionsPackage.DELETE_ACTION:
				return "delete";
			
			default:
				return "UNKNOWN";
			}
		}
		return null;
	} 
}
