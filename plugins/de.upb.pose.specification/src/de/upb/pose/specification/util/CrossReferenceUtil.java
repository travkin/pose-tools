package de.upb.pose.specification.util;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;

import de.upb.pose.specification.access.AccessPackage;
import de.upb.pose.specification.access.AccessRule;
import de.upb.pose.specification.access.Accessable;
import de.upb.pose.specification.actions.ActionsPackage;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.actions.CreateAction;
import de.upb.pose.specification.actions.ParameterAssignment;
import de.upb.pose.specification.actions.ReturnAction;
import de.upb.pose.specification.actions.Variable;
import de.upb.pose.specification.actions.VariableAccessAction;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Parameter;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;
import de.upb.pose.specification.types.TypesPackage;


public class CrossReferenceUtil
{

   /**
    * Returns the {@link CreateAction}s for a {@link Type}.
    * 
    * <pre>
    * +--------------+ instantiationType            +--------------+
    * |     Type     |<-----------------------------| CreateAction |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link CreateAction}s
    */
   public static List<EObject> getCreateActions(Type type)
   {
      List<EObject> actions = findCrossReferences(type, type.getSpecification(),
            ActionsPackage.Literals.CREATE_ACTION__INSTANTIATED_TYPE);
      return actions;
   }


   /**
    * Returns the {@link CallAction}s for a {@link Variable}.
    * 
    * <pre>
    * +--------------+ target                       +--------------+
    * |   Variable   |<-----------------------------|  CallAction  |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param variable The {@link Variable} of interest
    * @return List of {@link CreateAction}s
    */
   public static List<EObject> getCallActions(Variable variable)
   {
      List<EObject> actions = findCrossReferences(variable,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(variable)),
            ActionsPackage.Literals.CALL_ACTION__TARGET);
      return actions;
   }


   /**
    * Returns the {@link CallAction}s for a {@link Operation}.
    * 
    * <pre>
    * +--------------+ calledOperation              +--------------+
    * |  Operation   |<-----------------------------|  CallAction  |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link CreateAction}s
    */
   public static List<EObject> getCallActions(Operation operation)
   {
      List<EObject> actions = findCrossReferences(operation,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(operation)),
            ActionsPackage.Literals.CALL_ACTION__CALLED_OPERATION);
      return actions;
   }


   /**
    * Returns the {@link ReturnAction}s for a {@link Variable}.
    * 
    * <pre>
    * +--------------+ returnedVariable             +--------------+
    * |   Variable   |<-----------------------------| ReturnAction |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link CreateAction}s
    */
   public static List<EObject> getReturnActions(Variable variable)
   {
      List<EObject> actions = findCrossReferences(variable,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(variable)),
            ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE);
      return actions;
   }


   /**
    * Returns the {@link VariableAccessAction}s for a {@link Reference}.
    * 
    * <pre>
    * +--------------+ accessedReference            +--------------+
    * |  Reference   |<-----------------------------| AccessAction |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link CreateAction}s
    */
   public static List<EObject> getAccessActions(Reference reference)
   {
      List<EObject> actions = findCrossReferences(reference,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(reference)),
            ActionsPackage.Literals.VARIABLE_ACCESS_ACTION__ACCESSED_VARIABLE);
      return actions;
   }


   /**
    * Returns the {@link AccessRule}s for a {@link Accessable}.
    * 
    * <pre>
    * +--------------+ accessedElement              +--------------+
    * |  Accessable  |<-----------------------------| AccessRule   |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Accessable} of interest
    * @return List of {@link AccessRule}s
    */
   public static List<EObject> getAccessRules(Accessable accessable)
   {
      List<EObject> actions = findCrossReferences(accessable,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(accessable)),
            AccessPackage.Literals.ACCESS_RULE__ACCESSED_ELEMENT);
      return actions;
   }


   /**
    * Returns the {@link Reference} for which the {@link Type} is the referenced type.
    * 
    * <pre>
    * +--------------+ type                         +--------------+
    * |  Type        |<-----------------------------| Feature      |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link Reference}s
    */
   public static List<EObject> getReference(Type type)
   {
      List<EObject> actions = findCrossReferences(type,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(type)),
            TypesPackage.Literals.FEATURE__TYPE);
      return actions;
   }


   /**
    * Returns the {@link ParameterAssignment}s for a {@link Parameter}.
    * 
    * <pre>
    * +--------------+ targetParameter              +------------------------+
    * |  Parameter   |<-----------------------------| ParameterAssignment    |
    * +--------------+                              +------------------------+
    * </pre>
    * 
    * @param type The {@link Parameter} of interest
    * @return List of {@link ParameterAssignment}s
    */
   public static List<EObject> getParameterAssignments(Parameter parameter)
   {
      List<EObject> list = findCrossReferences(parameter,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(parameter)),
            ActionsPackage.Literals.PARAMETER_ASSIGNMENT__PARAMETER);
      return list;
   }


   /**
    * Returns the {@link ParameterAssignment}s for a {@link Variable}.
    * 
    * <pre>
    * +--------------+ assignedVariable             +------------------------+
    * |   Variable   |<-----------------------------| ParameterAssignment    |
    * +--------------+                              +------------------------+
    * </pre>
    * 
    * @param type The {@link Variable} of interest
    * @return List of {@link ParameterAssignment}s
    */
   public static List<EObject> getParameterAssignments(Variable variable)
   {
      List<EObject> list = findCrossReferences(variable,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(variable)),
            ActionsPackage.Literals.PARAMETER_ASSIGNMENT__ASSIGNED_VARIABLE);
      return list;
   }


   /**
    * Returns the {@code returnType} {@link Operation}s for a {@link Type}.
    * 
    * <pre>
    * +--------------+ returnType                   +--------------+
    * |     Type     |<-----------------------------|  Operation   |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link Operation}s
    */
   public static List<EObject> getReturnTypeOperations(Type type)
   {
      List<EObject> list = findCrossReferences(type,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(type)),
            TypesPackage.Literals.OPERATION__TYPE);
      return list;
   }


   /**
    * Returns the {@code type} {@link Parameter}s for a {@link Type}.
    * 
    * <pre>
    * +--------------+ type                         +--------------+
    * |     Type     |<-----------------------------|  Parameter   |
    * +--------------+                              +--------------+
    * </pre>
    * 
    * @param type The {@link Type} of interest
    * @return List of {@link Parameter}s
    */
   public static List<EObject> getTypeParameters(Type type)
   {
      List<EObject> list = findCrossReferences(type,
            PamelaUtil.getPatternSpecification(PamelaUtil.getPatternSpecification(type)),
            TypesPackage.Literals.PARAMETER__TYPE);
      return list;
   }


   private static List<EObject> findCrossReferences(EObject object, EObject diag, EReference eReference)
   {
      ArrayList<EObject> list = new ArrayList<EObject>();
      Collection<Setting> result = EcoreUtil.UsageCrossReferencer.find(object, diag);
      Iterator<Setting> iter = result.iterator();
      while (iter.hasNext())
      {
         EStructuralFeature.Setting element = iter.next();
         if (element.getEStructuralFeature() == eReference)
         {
            list.add(element.getEObject());
         }
      }
      return list;
   }
}
