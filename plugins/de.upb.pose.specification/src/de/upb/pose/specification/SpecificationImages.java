package de.upb.pose.specification;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import de.upb.pose.core.ui.runtime.ImagesImpl;

public final class SpecificationImages extends ImagesImpl {
	private static Map<EClass, String> map;

	public static Image get(Object element) {
		return Activator.get().getImage(getKey(element));
	}

	public static ImageDescriptor getDescriptor(Object element) {
		return Activator.get().getImageDescriptor(getKey(element));
	}

	public static String getKey(Object element) {
		if (element instanceof EClass) {
			return getMap().get(element);
		}
		if (element instanceof EObject) {
			return getKey(((EObject) element).eClass());
		}
		return null;
	}

	public static Collection<String> getPaths() {
		return getMap().values();
	}

	private static Map<EClass, String> getMap() {
		if (map == null) {
			map = new LinkedHashMap<EClass, String>();

			initialize(map, SpecificationPackage.eINSTANCE);
		}
		return map;
	}
}
