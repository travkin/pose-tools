package de.upb.pose.specification;


import org.eclipse.core.resources.IResource;

import de.upb.pose.core._DEPRECIATED.loader.SingleEmfLoader;


public class PamelaLoader
{

   private final SingleEmfLoader<PatternSpecification> loader;


   public PamelaLoader()
   {
      loader = new SingleEmfLoader<PatternSpecification>(new PamelaToXmlStrategy());
   }


   public PatternSpecification load(IResource resource)
   {
      try
      {
         return loader.load(resource, PatternSpecification.class);
      }
      catch (Exception e)
      {
         return SpecificationFactory.eINSTANCE.createPatternSpecification();
      }
   }

   // nicht unterst�tzt, da beim speichern auch das visuelle modell gespeichert
   // werden muss!
   // public void save(PatternSpecification patternSpecification, IResource
   // resource) throws CoreException
   // {
   // loader.save(patternSpecification, resource);
   // }
}
