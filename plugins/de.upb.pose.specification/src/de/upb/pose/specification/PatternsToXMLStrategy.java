package de.upb.pose.specification;

import de.upb.pose.core._DEPRECIATED.loader.IInitablePackage;
import de.upb.pose.core._DEPRECIATED.loader.ModelToXmlResourceStrategy;

public class PatternsToXMLStrategy extends ModelToXmlResourceStrategy {
	public PatternsToXMLStrategy() {
		super("ctlg", new IInitablePackage() {

			@Override
			public void init() {
				SpecificationPackage.eINSTANCE.getEFactoryInstance();
			}
		});

	}

}
