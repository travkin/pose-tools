package de.upb.pose.specification;

import java.util.Collection;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.ecore.EObject;

import de.upb.pose.core._DEPRECIATED.loader.CombinedEmfLoader;
import de.upb.pose.core._DEPRECIATED.loader.IInitablePackage;
import de.upb.pose.core._DEPRECIATED.loader.ILoadListener;
import de.upb.pose.core._DEPRECIATED.loader.ISaveProvider;
import de.upb.pose.core._DEPRECIATED.loader.ListUtil;
import de.upb.pose.core._DEPRECIATED.loader.LoadException;
import de.upb.pose.core._DEPRECIATED.loader.ModelToXmlResourceStrategy;
import de.upb.pose.core._DEPRECIATED.loader.UnknownElementException;

public class CatalogLoader {

	private final CombinedEmfLoader loader;

	public CatalogLoader() {
		loader = new CombinedEmfLoader(new ModelToXmlResourceStrategy("ctlg", new IInitablePackage() {
			@Override
			public void init() {
				SpecificationPackage.eINSTANCE.getEFactoryInstance();
			}
		}));
	}

	public DesignPatternCatalog load(IResource resource) {
		try {
			Collection<EObject> loaded = loader.loadAll(resource);
			return ListUtil.getObjectFromList(DesignPatternCatalog.class, loaded);
		} catch (LoadException e) {
			e.printStackTrace();

			return SpecificationFactory.eINSTANCE.createDesignPatternCatalog();
		} catch (UnknownElementException e) {
			e.printStackTrace();

			return SpecificationFactory.eINSTANCE.createDesignPatternCatalog();
		}
	}

	public void save(DesignPatternCatalog patternCatalog, IResource resource) throws LoadException {
		loader.save(resource, patternCatalog);
	}

	public void addSaveProvider(ISaveProvider provider) {
		loader.addSaveProvider(provider);
	}

	public void removeSaveProvider(ISaveProvider provider) {
		loader.removeSaveProvider(provider);
	}

	public void addLoadListener(ILoadListener listener) {
		loader.addLoadListener(listener);
	}

	public void removeLoadListener(ILoadListener listener) {
		loader.removeLoadListener(listener);
	}
}
