package de.upb.pose.specification;

import org.eclipse.swt.graphics.Color;

public final class SpecificationColors {
	public static final String BACKGROUND_DEFAULT = "background"; //$NON-NLS-1$
	public static final String BACKGROUND_NON_GENERABLE = "background_notgenerable"; //$NON-NLS-1$
	public static final String HIGHLIGHT_COLOR = "highlight"; //$NON-NLS-1$
	public static final String SECONDARY_HIGHLIGHT_COLOR = "highlight_secondary"; //$NON-NLS-1$

	public static Color get(String key) {
		return Activator.get().getColor(key);
	}
}
