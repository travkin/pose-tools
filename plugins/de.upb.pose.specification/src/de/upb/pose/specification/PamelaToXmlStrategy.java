package de.upb.pose.specification;

import de.upb.pose.core._DEPRECIATED.loader.IInitablePackage;
import de.upb.pose.core._DEPRECIATED.loader.ModelToXmlResourceStrategy;

public class PamelaToXmlStrategy extends ModelToXmlResourceStrategy {

	public PamelaToXmlStrategy() {
		super("pamela", new IInitablePackage() {
			@Override
			public void init() {
				SpecificationPackage.eINSTANCE.getEFactoryInstance();
			}
		});
	}

}
