/**
 * 
 */
package de.upb.pose.specification;

/**
 * @author Dietrich Travkin
 */
public final class PatternSpecificationConstants
{
	public static final String MODEL_FILE_EXTENSION = "patterns"; //$NON-NLS-1$
	public static final String DIAGRAMS_FILE_EXTENSION = "patterns_diagrams"; //$NON-NLS-1$
}
