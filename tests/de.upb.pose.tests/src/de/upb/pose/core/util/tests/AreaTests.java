package de.upb.pose.core.util.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.core.util.Area;

public class AreaTests {
	private Random random;

	@Test
	public void contains_false_x() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());

		Area a = new Area(x, y, w, h);
		Area b = new Area(x - 1, y, w, h);

		assertFalse(Area.contains(a, b));
	}

	@Test
	public void contains_true_same() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());

		Area a = new Area(x, y, w, h);
		Area b = new Area(x, y, w, h);

		assertTrue(Area.contains(a, b));
	}

	@Test
	public void equals_false_height() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		int w1 = Math.abs(random.nextInt());
		int h1 = Math.abs(random.nextInt());

		Area a = new Area(x1, y1, w1, h1);

		int h2 = random.nextInt();
		while (h2 == h1) {
			h2 = random.nextInt();
		}

		Area b = new Area(x1, y1, w1, h2);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_width() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		int w1 = Math.abs(random.nextInt());
		int h1 = Math.abs(random.nextInt());

		Area a = new Area(x1, y1, w1, h1);

		int w2 = random.nextInt();
		while (w2 == w1) {
			w2 = random.nextInt();
		}

		Area b = new Area(x1, y1, w2, h1);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_x() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		int w1 = Math.abs(random.nextInt());
		int h1 = Math.abs(random.nextInt());

		Area a = new Area(x1, y1, w1, h1);

		int x2 = random.nextInt();
		while (x2 == x1) {
			x2 = random.nextInt();
		}

		Area b = new Area(x2, y1, w1, h1);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_y() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		int w1 = Math.abs(random.nextInt());
		int h1 = Math.abs(random.nextInt());

		Area a = new Area(x1, y1, w1, h1);

		int y2 = random.nextInt();
		while (y2 == y1) {
			y2 = random.nextInt();
		}

		Area b = new Area(x1, y2, w1, h1);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_true() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());

		Area a = new Area(x, y, w, h);
		Area b = new Area(x, y, w, h);

		assertTrue(a.equals(b));
	}

	@Test
	public void hashCode_same() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());

		Area a = new Area(x, y, w, h);
		Area b = new Area(x, y, w, h);

		assertTrue(a.hashCode() == b.hashCode());
	}

	@Test
	public void hashCode_stable() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());

		Area a = new Area(x, y, w, h);

		assertTrue(a.hashCode() == a.hashCode());
	}

	@Test
	public void merge_height() {
		Area a = new Area(0, 0, 100, 200);
		Area b = new Area(0, 0, 100, 210);

		Area expected = new Area(0, 0, 100, 210);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_null_a() {
		Area a = null;

		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());
		Area b = new Area(x, y, w, h);

		Area expected = new Area(x, y, w, h);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_null_b() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());
		Area a = new Area(x, y, w, h);

		Area b = null;

		Area expected = new Area(x, y, w, h);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_same() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());
		Area a = new Area(x, y, w, h);

		Area b = new Area(x, y, w, h);

		Area expected = new Area(x, y, w, h);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_stable() {
		int x = random.nextInt();
		int y = random.nextInt();
		int w = Math.abs(random.nextInt());
		int h = Math.abs(random.nextInt());
		Area a = new Area(x, y, w, h);

		Area expected = new Area(x, y, w, h);

		assertEquals(expected, Area.merge(a, a));
	}

	@Test
	public void merge_width() {
		Area a = new Area(0, 0, 100, 200);
		Area b = new Area(0, 0, 110, 200);

		Area expected = new Area(0, 0, 110, 200);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_x() {
		Area a = new Area(0, 0, 100, 200);
		Area b = new Area(10, 0, 100, 200);

		Area expected = new Area(0, 0, 110, 200);

		assertEquals(expected, Area.merge(a, b));
	}

	@Test
	public void merge_y() {
		Area a = new Area(0, 0, 100, 200);
		Area b = new Area(0, 10, 100, 200);

		Area expected = new Area(0, 0, 100, 210);

		assertEquals(expected, Area.merge(a, b));
	}

	@Before
	public void start() {
		random = new Random();
	}

	@After
	public void stop() {
		random = null;
	}
}
