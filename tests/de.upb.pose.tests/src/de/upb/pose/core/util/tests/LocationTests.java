package de.upb.pose.core.util.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.core.util.Location;

public class LocationTests {
	private Random random;

	@Test
	public void equals_false_null() {
		int x = random.nextInt();
		int y = random.nextInt();

		Location a = new Location(x, y);
		Location b = null;

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_x() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		Location a = new Location(x1, y1);

		int x2 = random.nextInt();
		while (x2 == x1) {
			x2 = random.nextInt();
		}
		Location b = new Location(x2, y1);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_xy() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		Location a = new Location(x1, y1);

		int x2 = random.nextInt();
		while (x2 == x1) {
			x2 = random.nextInt();
		}
		int y2 = random.nextInt();
		while (y2 == y1) {
			y2 = random.nextInt();
		}
		Location b = new Location(x2, y2);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_false_y() {
		int x1 = random.nextInt();
		int y1 = random.nextInt();
		Location a = new Location(x1, y1);

		int y2 = random.nextInt();
		while (y2 == y1) {
			y2 = random.nextInt();
		}
		Location b = new Location(y1, y2);

		assertFalse(a.equals(b));
	}

	@Test
	public void equals_true() {
		int x = random.nextInt();
		int y = random.nextInt();

		Location a = new Location(x, y);
		Location b = new Location(x, y);

		assertTrue(a.equals(b));
	}

	@Test
	public void hashCode_same() {
		int x = random.nextInt();
		int y = random.nextInt();

		Location a = new Location(x, y);
		Location b = new Location(x, y);

		assertTrue(a.hashCode() == b.hashCode());
	}

	@Test
	public void hashCode_stable() {
		int x = random.nextInt();
		int y = random.nextInt();

		Location a = new Location(x, y);

		assertTrue(a.hashCode() == a.hashCode());
	}

	@Before
	public void start() {
		random = new Random();
	}

	@After
	public void stop() {
		random = null;
	}
}
