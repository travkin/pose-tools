package de.upb.pose.specification;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.util.SpecificationUtil;

/**
 * @author Dietrich Travkin
 *
 */
public class SpecificationUtilTest extends AbstractPatternApplicationTest
{
	@Test
	public void collectingAllPatternSpecificationElements()
	{
		AppliedPattern pattern = getAppliedPattern();
		
		Assert.assertNotNull(pattern);
		
		PatternSpecification specification = pattern.getPatternSpecification();
		
		Assert.assertNotNull(specification);
		Assert.assertFalse(specification.eIsProxy());
		
		List<PatternElement> patternElements = SpecificationUtil.collectAllPatternElements(specification);
		
		Assert.assertFalse(patternElements.isEmpty());
		Assert.assertEquals(15, patternElements.size());
	}
}
