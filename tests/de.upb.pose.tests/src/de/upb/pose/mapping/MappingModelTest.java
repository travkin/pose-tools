/**
 * 
 */
package de.upb.pose.mapping;

import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Assert;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.SetFragmentElement;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.types.Operation;

/**
 * @author Dietrich Travkin
 *
 */
public class MappingModelTest extends AbstractPatternApplicationTest
{
	
	@Test
	public void checkMappingModelBeforeUnfolding()
	{
		PatternApplications patternApplications = getPatternApplications(); 
		Assert.assertNotNull(patternApplications);
		
		Assert.assertFalse(patternApplications.getApplications().isEmpty());
		
		// ensure this is the right applied pattern
		AppliedPattern pattern = getAppliedPattern();
		
		Assert.assertNotNull(pattern);
		
		// for each design element, there must be at least one role binding
		TreeIterator<Object> iterator = EcoreUtil.getAllContents(pattern.getPatternSpecification(), true);
		while (iterator.hasNext())
		{
			Object obj = iterator.next();
			
			Assert.assertTrue(obj instanceof PatternElement);
			
			if (obj instanceof DesignElement)
			{
				List<RoleBinding> roleBindings = RoleMapper.findAllRoleBindingsFor((DesignElement) obj, pattern);
				
				Assert.assertFalse("No role bindings for a pattern specification element! Type: " + ((DesignElement) obj).eClass().getName()
						+ " name: " + ((DesignElement) obj).getName(),
						roleBindings.isEmpty());
			}
		}

		for (PatternElement patternElement: pattern.getPatternSpecification().getPatternElements())
		{
			if (patternElement instanceof DesignElement)
			{
				List<RoleBinding> roleBindings = RoleMapper.findAllRoleBindingsFor((DesignElement) patternElement, pattern);
				
				Assert.assertFalse(roleBindings.isEmpty());
			}
		}
		
		// for each set fragment, there must be a set binding and at least one set element binding
		for (PatternElement patternElement: pattern.getPatternSpecification().getPatternElements())
		{
			if (patternElement instanceof SetFragment)
			{
				SetFragmentBinding setBinding = SetFragmentBindingCreator.findSetFragmentBindingFor((SetFragment) patternElement, pattern);
				
				Assert.assertNotNull(setBinding);
				
				Assert.assertTrue(setBinding.getSetFragmentInstances().size() > 0);
				
				// for each set element binding, all contained pattern roles have to be included at least once
				for (SetFragmentInstance seb: setBinding.getSetFragmentInstances())
				{
					for (SetFragmentElement setElement: setBinding.getSetFragment().getContainedElements())
					{
						if (setElement instanceof DesignElement)
						{
							List<RoleBinding> allRoleBindings = RoleMapper.findAllRoleBindingsFor((DesignElement) setElement, pattern);
							List<RoleBinding> roleBindingsInSet = findRoleBindingsIn(seb, allRoleBindings);
							
							Assert.assertTrue("A specification element of type "
									+ setElement.eClass().getInstanceClassName()
									+ " has no role binding in the set element binding \""
									+ seb.getName() + "\".",
									roleBindingsInSet.size() >= 1);
						}
					}
				}
			}
		}
		
		// each role binding for the concreteAlgorithm operation is contained in two set element bindings for the set fragments "operations" and "strategies"
		for (PatternElement patternElement: pattern.getPatternSpecification().getPatternElements())
		{
			if (patternElement instanceof Operation && ((Operation) patternElement).getName().equals("concreteAlgorithm"))
			{
				List<RoleBinding> roleBindings = RoleMapper.findAllRoleBindingsFor((Operation) patternElement, pattern);
				
				Assert.assertFalse(roleBindings.isEmpty());
				
				for (RoleBinding roleBinding: roleBindings)
				{
					Assert.assertEquals(2, roleBinding.getContainingSetFragmentInstances().size());
					
					SetFragmentInstance seb1, seb2;
					seb1 = roleBinding.getContainingSetFragmentInstances().get(0);
					seb2 = roleBinding.getContainingSetFragmentInstances().get(1);
					
					Assert.assertNotSame(seb1, seb2);
					
					Assert.assertTrue(
							(seb1.getParentSetFragmentBinding().getSetFragment().getName().equals("operations")
							|| seb2.getParentSetFragmentBinding().getSetFragment().getName().equals("operations"))
							&&
							(seb1.getParentSetFragmentBinding().getSetFragment().getName().equals("strategies")
							|| seb2.getParentSetFragmentBinding().getSetFragment().getName().equals("strategies")));
				}
			}
		}
	}
}
