package de.upb.pose;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	AllNonPluginTests.class,
	AllPluginTests.class
})

public class AllTests {

}
