/**
 * 
 */
package de.upb.pose;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.upb.pose.mapping.MappingModelTest;
import de.upb.pose.patternapplication.ApplyPatternTest;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelElementCreatorTest;
import de.upb.pose.patternapplication.applicationmodel.ComplexUnfoldingTest;
import de.upb.pose.patternapplication.applicationmodel.UnfoldSetFragmentsTest;
import de.upb.pose.specification.SpecificationUtilTest;

/**
 * @author Dietrich Travkin
 *
 */

@RunWith(Suite.class)
@SuiteClasses({
	SpecificationUtilTest.class,
	MappingModelTest.class,
	UnfoldSetFragmentsTest.class,
	ApplicationModelElementCreatorTest.class,
	ApplyPatternTest.class,
	ComplexUnfoldingTest.class
	//ApplyQVTOTransformationTest.class
})

public class AllPluginTests
{

}
