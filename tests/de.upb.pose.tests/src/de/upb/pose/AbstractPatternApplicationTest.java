/**
 * 
 */
package de.upb.pose;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.After;
import org.junit.Before;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentInstance;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractPatternApplicationTest
{
	private PatternApplications patternApplications = null;
	private Resource resource = null;
	private ResourceSet allResources = null;
	
	public static final String APPLIED_PATTERN_NAME_STRATEGY = "Strategy pattern application for layout algorithms";
	
	@Before
	public void loadModels()
	{
		allResources = new ResourceSetImpl();
		resource = allResources.getResource(getMappingResourceURI(), true);
	    patternApplications = (PatternApplications) resource.getContents().get(0);
	}
	
	@After
	public void unloadModels()
	{
		EcoreUtil.remove(patternApplications);
		patternApplications = null;
		resource = null;
	}
	
	protected URI getMappingResourceURI()
	{
		return URI.createPlatformPluginURI("de.upb.pose.tests/models/design_before_strategy_application/editor.mappings", true);
	}
	
	protected Resource getMappingModelResource()
	{
		return this.resource;
	}
	
	protected ResourceSet getAllResources()
	{
		return this.allResources;
	}
	
	protected PatternApplications getPatternApplications()
	{
		return this.patternApplications;
	}
	
	protected AppliedPattern getAppliedPattern()
	{
		return findAppliedPattern(getAppliedPatternName());
	}
	
	protected String getAppliedPatternName()
	{
		return APPLIED_PATTERN_NAME_STRATEGY;
	}
	
	protected AppliedPattern findAppliedPattern(String patternApplicationName)
	{
		for (AppliedPattern pattern: patternApplications.getApplications())
		{
			if (pattern.getName().equals(patternApplicationName))
			{
				return pattern;
			}
		}
		return null;
	}
	
	protected List<RoleBinding> findRoleBindingsIn(SetFragmentInstance seb, List<RoleBinding> roleBindings)
	{
		ArrayList<RoleBinding> result = new ArrayList<RoleBinding>();
		for (RoleBinding roleBinding: roleBindings)
		{
			if (roleBinding.getContainingSetFragmentInstances().contains(seb))
			{
				result.add(roleBinding);
			}
		}
		return result;
	}
	
	protected int getNumberOfSetElementBindings(AppliedPattern pattern)
	{
		ArrayList<SetFragmentInstance> all = new ArrayList<SetFragmentInstance>();
		for (SetFragmentBinding binding: pattern.getSetFragmentBindings())
		{
			for (SetFragmentInstance b: binding.getSetFragmentInstances())
			{
				if (!all.contains(b))
				{
					all.add(b);
				}
			}
		}
		return all.size();
	}
}
