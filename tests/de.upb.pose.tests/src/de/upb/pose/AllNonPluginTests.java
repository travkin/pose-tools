/**
 * 
 */
package de.upb.pose;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.upb.pose.core.util.tests.AreaTests;
import de.upb.pose.core.util.tests.LocationTests;

/**
 * @author Dietrich Travkin
 *
 */

@RunWith(Suite.class)
@SuiteClasses({
	LocationTests.class,
	AreaTests.class
})

public class AllNonPluginTests
{

}
