/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.BindingCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelCreator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 *
 */
public class ApplicationModelElementCreatorTest extends AbstractPatternApplicationTest
{	
	private ApplicationModelCreator modelCreator = null;
	
	@Before
	public void createApplicationModel()
	{
		modelCreator = ApplicationModelCreator.get();
		BindingCreator.createMissingBindings(getAppliedPattern());
		ApplicationModelCreator.createInitialApplicationModelFromMapping(getAppliedPattern());
	}
	
	@Test
	public void ensureCopyWithoutContainments()
	{
		AppliedPattern pattern = getAppliedPattern();
		
		for (RoleBinding binding: pattern.getRoleBindings())
		{
			if (binding.getRole() instanceof Type)
			{
				Type type = (Type) binding.getRole();
				
				int childrenCount = type.eContents().size();
				
				if (childrenCount > 0)
				{
					DesignElement copy = modelCreator.getApplicationModelElementCreator().createApplicationModelElement(binding);
					
					Assert.assertNotNull(copy);
					Assert.assertTrue(copy instanceof Type);
					
					Type typeCopy = (Type) copy;
					
					Assert.assertEquals(0, typeCopy.eContents().size());
				}
			}
		}
	}
}
