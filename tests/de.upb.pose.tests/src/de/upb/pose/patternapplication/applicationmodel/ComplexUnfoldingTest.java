/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.AbstractPoseTest;
import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.mapping.PatternApplications2EcoreConnector;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.mapping.RoleMapper;
import de.upb.pose.mapping.SetFragmentBinding;
import de.upb.pose.mapping.SetFragmentBindingCreator;
import de.upb.pose.mapping.SetFragmentInstance;
import de.upb.pose.patternapplication.PatternApplicationCreator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.DesignPattern;
import de.upb.pose.specification.DesignPatternCatalog;
import de.upb.pose.specification.PatternElement;
import de.upb.pose.specification.PatternSpecification;
import de.upb.pose.specification.SetFragment;
import de.upb.pose.specification.SpecificationFactory;
import de.upb.pose.specification.util.SpecificationUtil;
import de.upb.pose.tests.graph.GraphFactory;
import de.upb.pose.tests.graph.Node;

/**
 * Tests based on a separate graph meta-model (instead of ecore meta-model where edges, i.e. references,
 * can only connect exactly 2 nodes, i.e. classes, with each other, not arbitrarily many nodes with another node;
 * taken from de.upb.pose.tests.graph plug-in)
 * to test the complex set fragment constellations described in my PhD thesis
 * (Chapter "Formale Definition der Semantik von Set Fragments", tag: secMSSetFragmentsFormal), Fig. 4.53, p. 87
 * 
 * @author Dietrich Travkin
 */
public class ComplexUnfoldingTest extends AbstractPoseTest {

	private static final String RESOURCE_URI_ECORE = "models/design_before_strategy_application/diagrams/petrinet.ecorediag";
	
	private PatternApplications patternApplications = null;
	private PatternSpecification patternSpecification = null;

	/**
	 * @see de.upb.pose.AbstractPoseTest#getPluginRelativePathsToResourcesToBeLoaded()
	 */
	@Override
	protected String[] getPluginRelativePathsToResourcesToBeLoaded() {
		return new String[] { RESOURCE_URI_ECORE };
	}

	@Override
	@Before
	public void setUp() {
		// load resources
		super.setUp();
		
		// create mapping resource and pattern applications root
		Resource ecoreResource = findEcoreResource();
		this.patternSpecification = this.createPatternSpecification(ecoreResource);
		this.patternApplications = this.createPatternApplications(ecoreResource);
		
		assertNotNull(this.patternApplications);
		assertNotNull(this.patternSpecification);
	}

	@Override
	@After
	public void tearDown() {
		// unload resources
		super.tearDown();
		
		this.patternApplications = null;
		this.patternSpecification = null;
	}
	
	private Resource findEcoreResource() {
		List<Resource> ecoreFiles = this.findAllEcoreModelResources();
		Resource petrinetResource = null;
		for (Resource resource: ecoreFiles) {
			String fileName = resource.getURI().trimFileExtension().lastSegment();
			if ("petrinet".equals(fileName)) {
				petrinetResource = resource;
				break;
			}
		}
		Assert.assertNotNull(petrinetResource);
		
		Resource ecoreResource = ecoreFiles.get(0);
		return ecoreResource;
	}
	
	private PatternApplications createPatternApplications(Resource ecoreResource) {
		URI mappingUri = ecoreResource.getURI().trimFileExtension().trimSegments(1)
				.appendSegment("mapping").appendFileExtension(MAPPING_MODEL_FILE_EXTENSION);
		Resource mappingResource = this.getResourceSet().createResource(mappingUri);
		
		PatternApplications patternApplications = PatternApplications2EcoreConnector.createPatternApplicationsForEcoreFile(ecoreResource);
		mappingResource.getContents().add(patternApplications);
		
		return patternApplications;
	}
	
	private PatternSpecification createPatternSpecification(Resource ecoreResource) {
		URI specificationsUri = ecoreResource.getURI().trimFileExtension().trimSegments(1)
				.appendSegment("patterns").appendFileExtension(SPECIFICATION_MODEL_FILE_EXTENSION);
		Resource specificationsResource = this.getResourceSet().createResource(specificationsUri);
		
		PatternSpecification specification = createPatternSpecificationInNewCatalog();
		DesignPatternCatalog catalog = specification.getPattern().getCatalog();
		if (!specificationsResource.getContents().contains(catalog)) {
			specificationsResource.getContents().add(catalog);
		}
		return specification;
	}
	
	private PatternSpecification createPatternSpecificationInNewCatalog() {
		DesignPatternCatalog catalog = SpecificationUtil.createEmptyInitializedCatalog();
		
		DesignPattern pattern = SpecificationFactory.eINSTANCE.createDesignPattern();
		pattern.setName("Test Pattern");
		catalog.getPatterns().add(pattern);
		
		PatternSpecification specification = SpecificationFactory.eINSTANCE.createPatternSpecification();
		specification.setName("Specification with complex set fragment structure");
		pattern.getSpecifications().add(specification);
		
		Node t, u, v, w, x, y, z;
		SetFragment a, b, c, d;
		
		t = GraphFactory.eINSTANCE.createNode();
		u = GraphFactory.eINSTANCE.createNode();
		v = GraphFactory.eINSTANCE.createNode();
		w = GraphFactory.eINSTANCE.createNode();
		x = GraphFactory.eINSTANCE.createNode();
		y = GraphFactory.eINSTANCE.createNode();
		z = GraphFactory.eINSTANCE.createNode();

		a = SpecificationFactory.eINSTANCE.createSetFragment();
		b = SpecificationFactory.eINSTANCE.createSetFragment();
		c = SpecificationFactory.eINSTANCE.createSetFragment();
		d = SpecificationFactory.eINSTANCE.createSetFragment();
		
		t.setName("t");
		u.setName("u");
		v.setName("v");
		w.setName("w");
		x.setName("x");
		y.setName("y");
		z.setName("z");
		
		a.setName("a");
		b.setName("b");
		c.setName("c");
		d.setName("d");
		
		specification.getPatternElements().add(t);
		specification.getPatternElements().add(u);
		specification.getPatternElements().add(v);
		specification.getPatternElements().add(w);
		specification.getPatternElements().add(x);
		specification.getPatternElements().add(y);
		specification.getPatternElements().add(z);
		
		specification.getPatternElements().add(a);
		specification.getPatternElements().add(b);
		specification.getPatternElements().add(c);
		specification.getPatternElements().add(d);
		
		t.getEdgesTo().add(v);
		v.getEdgesTo().add(y);
		y.getEdgesTo().add(z);
		u.getEdgesTo().add(w);
		x.getEdgesTo().add(t);
		x.getEdgesTo().add(v);
		x.getEdgesTo().add(w);
		z.getEdgesTo().add(x);
		w.getEdgesTo().add(z);
		w.getEdgesTo().add(y);
		
		c.getContainingSets().add(a);
		c.getContainingSets().add(b);
		a.getContainingSets().add(d);
		b.getContainingSets().add(d);
		
		v.getContainingSets().add(d);
		y.getContainingSets().add(b);
		x.getContainingSets().add(a);
		z.getContainingSets().add(a);
		z.getContainingSets().add(b);
		w.getContainingSets().add(c);
		
		return specification;
	}
	
	@Test
	public void initializationOfBindingsSucceful() {
		// create pattern application and role bindings
		AppliedPattern patternApplication = PatternApplicationCreator.createPatternApplicationFor(
				this.patternSpecification, this.patternApplications);
		assertNotNull("Creation of pattern application and role bindings failed.", patternApplication);
		
		assertInitializationRoleBindingsArePresent(patternApplication);
	}
	
	@Test
	public void initializationOfApplicationModelSucceful() {
		// create pattern application and role bindings
		AppliedPattern patternApplication = PatternApplicationCreator.createPatternApplicationFor(
				this.patternSpecification, this.patternApplications);
		assertNotNull(patternApplication);

		// create pattern application model and tasks (unfolding initialization)
		ApplicationModel applicationModel = ApplicationModelCreator
				.createInitialApplicationModelFromMapping(patternApplication);
		assertInitializationSubgraphIsPresent(applicationModel);
		
		// no additional nodes created
		int numberOfNodesInApplicationModel = 0;
		for (DesignElement element: applicationModel.getDesignElements()) {
			if (element instanceof Node) {
				numberOfNodesInApplicationModel++;
			}
		}
		assertEquals(7, numberOfNodesInApplicationModel);
		
		// no additional set fragment bindings created
		assertEquals(4, patternApplication.getSetFragmentBindings().size());
		
		// no additional set fragment instances created
		SetFragmentBinding a, b, c, d;
		a = findSetFragmentBinding("a", patternApplication);
		b = findSetFragmentBinding("b", patternApplication);
		c = findSetFragmentBinding("c", patternApplication);
		d = findSetFragmentBinding("d", patternApplication);
		
		assertNotNull(a);
		assertEquals(1, a.getSetFragmentInstances().size());
		assertNotNull(b);
		assertEquals(1, b.getSetFragmentInstances().size());
		assertNotNull(c);
		assertEquals(1, c.getSetFragmentInstances().size());
		assertNotNull(d);
		assertEquals(1, d.getSetFragmentInstances().size());
	}

	@Test
	public void addSetFragmentInstanceCorrectInTestCase1() {
		// initialize application model (as tested in {@link #initializationSucceful()})
		AppliedPattern patternApplication = PatternApplicationCreator.createPatternApplicationFor(
				this.patternSpecification, this.patternApplications);
		ApplicationModel applicationModel = ApplicationModelCreator
				.createInitialApplicationModelFromMapping(patternApplication);
		assertInitializationSubgraphIsPresent(applicationModel);
		
		
		// ####### step 1 #######
		// add new set fragment instance by copying set fragment instance a0
		SetFragmentBinding setFragmentBindingA = findSetFragmentBinding("a", patternApplication);
		assertNotNull(setFragmentBindingA);
		assertEquals(1, setFragmentBindingA.getSetFragmentInstances().size());
		
		SetFragmentInstance a0 = setFragmentBindingA.getSetFragmentInstances().get(0);
		assertNotNull(a0);
		
		SetFragmentInstance a1 = ApplicationModelCreator.addSetFragmentInstance(a0);
		assertNotNull(a1);
		
		assertInitializationSubgraphIsPresent(applicationModel);
		
		assertEquals(1, findNodes("t", applicationModel).size());
		assertEquals(1, findNodes("u", applicationModel).size());
		assertEquals(1, findNodes("v", applicationModel).size());
		assertEquals(2, findNodes("w", applicationModel).size());
		assertEquals(2, findNodes("x", applicationModel).size());
		assertEquals(1, findNodes("y", applicationModel).size());
		assertEquals(2, findNodes("z", applicationModel).size());
		
		@SuppressWarnings("unused")
		Node t, u, v, w, x, y, z, z_a, x_a, w_a;
		t = findNodes("t", applicationModel).get(0);
		u = findNodes("u", applicationModel).get(0);
		v = findNodes("v", applicationModel).get(0);
		w = findNodes("w", applicationModel).get(0);
		x = findNodes("x", applicationModel).get(0);
		y = findNodes("y", applicationModel).get(0);
		z = findNodes("z", applicationModel).get(0);
		
		// assert subgraph of set fragment instance a1 is present including incoming & outgoing edges
		z_a = findNodes("z", applicationModel).get(1);
		x_a = findNodes("x", applicationModel).get(1);
		w_a = findNodes("w", applicationModel).get(1);
		
		assertTrue(isNodeLinkedToNode(z_a, x_a));
		assertTrue(isNodeLinkedToNode(w_a, z_a));
		assertTrue(isNodeLinkedToNode(x_a, w_a));
		assertTrue(isNodeLinkedToNode(x_a, t));
		assertTrue(isNodeLinkedToNode(x_a, v));
		assertTrue(isNodeLinkedToNode(y, z_a));
		assertTrue(isNodeLinkedToNode(w_a, y));
		assertTrue(isNodeLinkedToNode(u, w_a));
		
		
		// ####### step 2 #######
		// add new set fragment instance by copying set fragment instance b0
		SetFragmentBinding setFragmentBindingB = findSetFragmentBinding("b", patternApplication);
		assertNotNull(setFragmentBindingB);
		assertEquals(1, setFragmentBindingB.getSetFragmentInstances().size());
		
		SetFragmentInstance b0 = setFragmentBindingB.getSetFragmentInstances().get(0);
		assertNotNull(b0);
		
		// add operation
		SetFragmentInstance b1 = ApplicationModelCreator.addSetFragmentInstance(b0);
		assertNotNull(b1);
		
		// assert all expected set fragment instances and role bindings are there
		assertEquals(1, findRoleBindingsForNode("t", patternApplication).size());
		assertEquals(1, findRoleBindingsForNode("u", patternApplication).size());
		assertEquals(1, findRoleBindingsForNode("v", patternApplication).size());
		assertEquals(4, findRoleBindingsForNode("w", patternApplication).size());
		assertEquals(2, findRoleBindingsForNode("x", patternApplication).size());
		assertEquals(2, findRoleBindingsForNode("y", patternApplication).size());
		assertEquals(4, findRoleBindingsForNode("z", patternApplication).size());
		
		assertEquals(2, setFragmentBindingA.getSetFragmentInstances().size());
		assertEquals(2, setFragmentBindingB.getSetFragmentInstances().size());
		SetFragmentBinding setFragmentBindingC = findSetFragmentBinding("c", patternApplication);
		assertNotNull(setFragmentBindingC);
		assertEquals(4, setFragmentBindingC.getSetFragmentInstances().size());
		SetFragmentBinding setFragmentBindingD = findSetFragmentBinding("d", patternApplication);
		assertNotNull(setFragmentBindingD);
		assertEquals(1, setFragmentBindingD.getSetFragmentInstances().size());
		

		// assert all edges are still there and those of set fragment instance b1 (incl. incoming & outgoing), too
		
		// "black nodes and edges"
		assertInitializationSubgraphIsPresent(applicationModel);
		
		assertEquals(1, findNodes("t", applicationModel).size());
		assertEquals(1, findNodes("u", applicationModel).size());
		assertEquals(1, findNodes("v", applicationModel).size());
		assertEquals(4, findNodes("w", applicationModel).size());
		assertEquals(2, findNodes("x", applicationModel).size());
		assertEquals(2, findNodes("y", applicationModel).size());
		assertEquals(4, findNodes("z", applicationModel).size());
		
		Node z_b, y_b, w_b, z_a_b, w_a_b;
		y_b = findNodes("y", applicationModel).get(1);
		
		z_b = findNodes("z", applicationModel).get(2);
		z_a_b = findNodes("z", applicationModel).get(3);
		w_b = findNodes("w", applicationModel).get(2);
		w_a_b = findNodes("w", applicationModel).get(3);
		
		if (a0.getContainedRoleBindings().contains(z_a_b)) {
			Node tempNode = z_b;
			z_b = z_a_b;
			z_a_b = tempNode;
		}
		
		if (a0.getContainedRoleBindings().contains(w_a_b)) {
			Node tempNode = w_b;
			w_b = w_a_b;
			w_a_b = tempNode;
		}
		
		// "blue nodes and edges"
		assertTrue(isNodeLinkedToNode(z_a, x_a));
		assertTrue(isNodeLinkedToNode(w_a, z_a));
		assertTrue(isNodeLinkedToNode(x_a, w_a));
		assertTrue(isNodeLinkedToNode(x_a, t));
		assertTrue(isNodeLinkedToNode(x_a, v));
		assertTrue(isNodeLinkedToNode(y, z_a));
		assertTrue(isNodeLinkedToNode(w_a, y));
		assertTrue(isNodeLinkedToNode(u, w_a));
		
		// "green nodes and edges"
		assertTrue(isNodeLinkedToNode(y_b, z_b));
		assertTrue(isNodeLinkedToNode(w_b, y_b));
		assertTrue(isNodeLinkedToNode(w_b, z_b));
		assertTrue(isNodeLinkedToNode(v, y_b));
		assertTrue(isNodeLinkedToNode(z_b, x));
		assertTrue(isNodeLinkedToNode(u, w_b));
		assertTrue(isNodeLinkedToNode(x, w_b));
		
		// "red nodes and edges"
		assertTrue(isNodeLinkedToNode(w_a_b, z_a_b));
		assertTrue(isNodeLinkedToNode(y_b, z_a_b));
		assertTrue(isNodeLinkedToNode(z_a_b, x_a));
		assertTrue(isNodeLinkedToNode(u, w_a_b));
		assertTrue(isNodeLinkedToNode(w_a_b, y_b));
		assertTrue(isNodeLinkedToNode(x_a, w_a_b));
		
		
		// assert set fragment instance containments are correct
		SetFragmentInstance d0 = setFragmentBindingD.getSetFragmentInstances().get(0);
		assertTrue(d0.getContainingSetFragmentInstances().isEmpty());
		for (SetFragmentInstance c: setFragmentBindingC.getSetFragmentInstances()) {
			assertTrue(c.getContainedSetFragmentInstances().isEmpty());
		}
		assertTrue(d0.getContainedSetFragmentInstances().contains(a0));
		assertTrue(d0.getContainedSetFragmentInstances().contains(a1));
		assertTrue(d0.getContainedSetFragmentInstances().contains(b0));
		assertTrue(d0.getContainedSetFragmentInstances().contains(b1));
		SetFragmentInstance c0, c0_1, c0_2, c0_3;
		RoleBinding r_w, r_w_b, r_w_a, r_w_a_b;
		r_w = findRoleBindingForNode(w, patternApplication);
		r_w_a = findRoleBindingForNode(w_a, patternApplication);
		r_w_b = findRoleBindingForNode(w_b, patternApplication);
		r_w_a_b = findRoleBindingForNode(w_a_b, patternApplication);
		assertEquals(1, r_w.getContainingSetFragmentInstances().size());
		c0 = r_w.getContainingSetFragmentInstances().get(0);
		assertEquals(1, r_w_a.getContainingSetFragmentInstances().size());
		c0_1 = r_w_a.getContainingSetFragmentInstances().get(0);
		assertEquals(1, r_w_b.getContainingSetFragmentInstances().size());
		c0_2 = r_w_b.getContainingSetFragmentInstances().get(0);
		assertEquals(1, r_w_a_b.getContainingSetFragmentInstances().size());
		c0_3 = r_w_a_b.getContainingSetFragmentInstances().get(0);
		assertEquals(2, c0.getContainingSetFragmentInstances().size());
		assertTrue(a0.getContainedSetFragmentInstances().contains(c0));
		assertTrue(b0.getContainedSetFragmentInstances().contains(c0));
		assertEquals(2, c0_1.getContainingSetFragmentInstances().size());
		assertTrue(a1.getContainedSetFragmentInstances().contains(c0_1));
		assertTrue(b0.getContainedSetFragmentInstances().contains(c0_1));
		assertEquals(2, c0_2.getContainingSetFragmentInstances().size());
		assertTrue(a0.getContainedSetFragmentInstances().contains(c0_2));
		assertTrue(b1.getContainedSetFragmentInstances().contains(c0_2));
		assertEquals(2, c0_3.getContainingSetFragmentInstances().size());
		assertTrue(a1.getContainedSetFragmentInstances().contains(c0_3));
		assertTrue(b1.getContainedSetFragmentInstances().contains(c0_3));
	}
	
	private void assertInitializationRoleBindingsArePresent(AppliedPattern patternApplication) {
		// check role binding availability
		assertTrue(findRoleBindingsForNode("t", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("u", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("v", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("w", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("x", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("y", patternApplication).size() >= 1);
		assertTrue(findRoleBindingsForNode("z", patternApplication).size() >= 1);
		
		RoleBinding r_t, r_u, r_v, r_w, r_x, r_y, r_z;
		r_t = findRoleBindingsForNode("t", patternApplication).get(0);
		r_u = findRoleBindingsForNode("u", patternApplication).get(0);
		r_v = findRoleBindingsForNode("v", patternApplication).get(0);
		r_w = findRoleBindingsForNode("w", patternApplication).get(0);
		r_x = findRoleBindingsForNode("x", patternApplication).get(0);
		r_y = findRoleBindingsForNode("y", patternApplication).get(0);
		r_z = findRoleBindingsForNode("z", patternApplication).get(0);
		
		// check set fragment binding availability
		SetFragmentBinding a, b, c, d;
		a = findSetFragmentBinding("a", patternApplication);
		b = findSetFragmentBinding("b", patternApplication);
		c = findSetFragmentBinding("c", patternApplication);
		d = findSetFragmentBinding("d", patternApplication);
		
		// check set fragment instance availability
		assertTrue(a.getSetFragmentInstances().size() >= 1);
		assertTrue(b.getSetFragmentInstances().size() >= 1);
		assertTrue(c.getSetFragmentInstances().size() >= 1);
		assertTrue(d.getSetFragmentInstances().size() >= 1);
		
		SetFragmentInstance a0, b0, c0, d0;
		a0 = a.getSetFragmentInstances().get(0);
		b0 = b.getSetFragmentInstances().get(0);
		c0 = c.getSetFragmentInstances().get(0);
		d0 = d.getSetFragmentInstances().get(0);
		
		// check role bindings' containments
		assertTrue(r_t.getContainingSetFragmentInstances().isEmpty());
		
		assertEquals(1, r_v.getContainingSetFragmentInstances().size());
		assertTrue(r_v.getContainingSetFragmentInstances().contains(d0));
		
		assertEquals(1, r_y.getContainingSetFragmentInstances().size());
		assertTrue(r_y.getContainingSetFragmentInstances().contains(b0));
		
		assertTrue(r_u.getContainingSetFragmentInstances().isEmpty());
		
		assertEquals(1, r_x.getContainingSetFragmentInstances().size());
		assertTrue(r_x.getContainingSetFragmentInstances().contains(a0));
		
		assertEquals(2, r_z.getContainingSetFragmentInstances().size());
		assertTrue(r_z.getContainingSetFragmentInstances().contains(a0));
		assertTrue(r_z.getContainingSetFragmentInstances().contains(b0));
		
		assertEquals(1, r_w.getContainingSetFragmentInstances().size());
		assertTrue(r_w.getContainingSetFragmentInstances().contains(c0));
		
		// check set fragment instances' containments
		assertTrue(c0.getContainedSetFragmentInstances().isEmpty());
		assertTrue(a0.getContainedSetFragmentInstances().contains(c0));
		assertTrue(b0.getContainedSetFragmentInstances().contains(c0));
		assertTrue(d0.getContainedSetFragmentInstances().contains(a0));
		assertTrue(d0.getContainedSetFragmentInstances().contains(b0));
	}
	
	private void assertInitializationSubgraphIsPresent(ApplicationModel applicationModel) {
		assertNotNull("Initial unfolding (creation of an initial application model) failed.", applicationModel);
		
		assertInitializationRoleBindingsArePresent(applicationModel.getAppliedPattern());
		
		// check node availability
		assertTrue(findNodes("t", applicationModel).size() >= 1);
		assertTrue(findNodes("u", applicationModel).size() >= 1);
		assertTrue(findNodes("v", applicationModel).size() >= 1);
		assertTrue(findNodes("w", applicationModel).size() >= 1);
		assertTrue(findNodes("x", applicationModel).size() >= 1);
		assertTrue(findNodes("y", applicationModel).size() >= 1);
		assertTrue(findNodes("z", applicationModel).size() >= 1);
		
		Node t, u, v, w, x, y, z;
		t = findNodes("t", applicationModel).get(0);
		u = findNodes("u", applicationModel).get(0);
		v = findNodes("v", applicationModel).get(0);
		w = findNodes("w", applicationModel).get(0);
		x = findNodes("x", applicationModel).get(0);
		y = findNodes("y", applicationModel).get(0);
		z = findNodes("z", applicationModel).get(0);
		
		assertNotNull(t);
		assertNotNull(u);
		assertNotNull(v);
		assertNotNull(w);
		assertNotNull(x);
		assertNotNull(y);
		assertNotNull(z);
		
		// check edge availability
		assertTrue(isNodeLinkedToNode(t, v));
		assertTrue(isNodeLinkedToNode(v, y));
		assertTrue(isNodeLinkedToNode(y, z));
		assertTrue(isNodeLinkedToNode(u, w));
		assertTrue(isNodeLinkedToNode(z, x));
		assertTrue(isNodeLinkedToNode(x, t));
		assertTrue(isNodeLinkedToNode(x, v));
		assertTrue(isNodeLinkedToNode(x, w));
		assertTrue(isNodeLinkedToNode(w, z));
		assertTrue(isNodeLinkedToNode(w, y));
	}
	
	private List<Node> findNodes(String nodeName, ApplicationModel applicationModel) {
		assertNotNull(nodeName);
		assertNotNull(applicationModel);
		
		List<Node> nodesFound = new ArrayList<Node>(); 
		for (DesignElement element: applicationModel.getDesignElements()) {
			if (element instanceof Node) {
				Node node = (Node) element;
				
				if (nodeName.equals(node.getName())) {
					nodesFound.add(node);
				}
			}
		}
		return nodesFound;
	}
	
	private boolean isNodeLinkedToNode(Node source, Node target) {
		assertNotNull(source);
		assertNotNull(target);
		
		return source.getEdgesTo().contains(target);
	}
	
	private SetFragment findSetFragment(String setFragmentName, PatternSpecification specification) {
		assertNotNull(setFragmentName);
		assertNotNull(specification);
		
		for (PatternElement element: specification.getPatternElements()) {
			if (element instanceof SetFragment) {
				SetFragment setFragment = (SetFragment) element;
				
				if (setFragmentName.equals(setFragment.getName())) {
					return setFragment;
				}
			}
		}
		return null;
	}
	
	private Node findNode(String nodeName, PatternSpecification specification) {
		assertNotNull(nodeName);
		assertNotNull(specification);
		
		for (PatternElement element: specification.getPatternElements()) {
			if (element instanceof Node) {
				Node node = (Node) element;
				
				if (nodeName.equals(node.getName())) {
					return node;
				}
			}
		}
		return null;
	}
	
	private List<RoleBinding> findRoleBindingsForNode(String nodeName, AppliedPattern patternApplication) {
		assertNotNull(nodeName);
		assertNotNull(patternApplication);
		
		Node nodeInPatternSpecification = findNode(nodeName, patternApplication.getPatternSpecification());
		assertNotNull(nodeInPatternSpecification);
		
		return RoleMapper.findAllRoleBindingsFor(nodeInPatternSpecification, patternApplication);
	}
	
	private RoleBinding findRoleBindingForNode(Node applicationModelNode, AppliedPattern patternApplication) {
		assertNotNull(applicationModelNode);
		assertNotNull(patternApplication);
		
		List<RoleBinding> roleBindings = findRoleBindingsForNode(applicationModelNode.getName(), patternApplication);
		for (RoleBinding roleBinding: roleBindings) {
			if (applicationModelNode.equals(roleBinding.getApplicationModelElement())) {
				return roleBinding;
			}
		}
		return null;
	}
	
	private SetFragmentBinding findSetFragmentBinding(String setFragmentName, AppliedPattern patternApplication) {
		assertNotNull(setFragmentName);
		assertNotNull(patternApplication);
		
		SetFragment setFragment = findSetFragment(setFragmentName, patternApplication.getPatternSpecification());
		assertNotNull(setFragment);
		
		SetFragmentBinding setFragmentBinding = SetFragmentBindingCreator.findSetFragmentBindingFor(setFragment, patternApplication);

		return setFragmentBinding;
	}
	
}
