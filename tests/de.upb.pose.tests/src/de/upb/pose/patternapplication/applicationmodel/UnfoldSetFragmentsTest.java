/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;
import de.upb.pose.mapping.ApplicationModel;
import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.BindingCreator;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelCreator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.actions.CallAction;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 *
 */
public class UnfoldSetFragmentsTest extends AbstractPatternApplicationTest
{
	@Before
	public void completeMapping()
	{
		BindingCreator.createMissingBindings(getAppliedPattern());
	}
	
	@Test
	public void validRoleBindingsAfterUnfolding()
	{
		AppliedPattern pattern = getAppliedPattern();
		int numberOfRoleBindingsBeforeUnfolding = pattern.getRoleBindings().size();
		
		ApplicationModelCreator.createInitialApplicationModelFromMapping(pattern);

		// ensure no additional role bindings were created
		Assert.assertEquals(numberOfRoleBindingsBeforeUnfolding, pattern.getRoleBindings().size());
		
		Assert.assertEquals(3, getNumberOfSetElementBindings(pattern));
	}
	
	@Test
	public void applicationModelElementsForEachRoleBinding()
	{
		AppliedPattern pattern = getAppliedPattern();
		
		Assert.assertNotNull(ApplicationModelCreator.createInitialApplicationModelFromMapping(pattern));
		
		// for each role binding there is exactly one design element in the application model
		for (RoleBinding roleBinding : pattern.getRoleBindings())
		{
			Assert.assertNotNull(roleBinding.getApplicationModelElement());
		}
	}
	
	@Test
	public void correctAttributeValuesInApplicationModelElements()
	{
		AppliedPattern pattern = getAppliedPattern();
		ApplicationModelCreator.createInitialApplicationModelFromMapping(pattern);
		
		for (RoleBinding roleBinding : pattern.getRoleBindings())
		{
			DesignElement applicationModelElement = roleBinding.getApplicationModelElement();

			// different ids
			Assert.assertFalse(roleBinding.getRole().getID().equals(applicationModelElement.getID()));
			
			// valid names
			if (roleBinding.getModelElements().isEmpty()
					&& (roleBinding.getNewElementName() == null || roleBinding.getNewElementName().equals("")))
			{
				Assert.assertTrue("Application model element has not the name of its pattern role.",
						applicationModelElement.getName().startsWith(roleBinding.getRole().getName()));
			}
			else if (roleBinding.getModelElements().isEmpty()
					&& roleBinding.getNewElementName() != null && !roleBinding.getNewElementName().equals(""))
			{
				Assert.assertEquals("Application model element has not the new element name stored in its role binding.",
						roleBinding.getNewElementName(), applicationModelElement.getName());
			}
			else if (!roleBinding.getModelElements().isEmpty())
			{
				EObject mappedEcoreElement = roleBinding.getModelElements().get(0);
				
				Assert.assertNotNull(mappedEcoreElement);
				Assert.assertTrue(mappedEcoreElement instanceof ENamedElement);
				
				ENamedElement modelElment = (ENamedElement) mappedEcoreElement;
				
				Assert.assertEquals("Application model element has not the name of its mapped model element.",
						modelElment.getName(), applicationModelElement.getName());
			}
		}
	}
	
	@Test
	public void relationsInApplicationModelAreIsomorphicToSpecification()
	{
		AppliedPattern pattern = getAppliedPattern();
		ApplicationModelCreator.createInitialApplicationModelFromMapping(pattern);
		
		for (RoleBinding roleBinding : pattern.getRoleBindings())
		{
			testTypesInApplicationModel(roleBinding, pattern);
			testOperationsInApplicationModel(roleBinding, pattern);
			testCallActionsInApplicationModel(roleBinding, pattern);
			testReferencesInApplicationModel(roleBinding, pattern);
		}
	}
	
	private void testTypesInApplicationModel(RoleBinding roleBinding, AppliedPattern pattern)
	{
		if (roleBinding.getRole() instanceof Type)
		{
			Type patternSpecificationElement = (Type) roleBinding.getRole();
			
			Assert.assertNotNull(roleBinding.getApplicationModelElement());
			Assert.assertTrue(roleBinding.getApplicationModelElement() instanceof Type);
			
			Type applicationModelElement = (Type) roleBinding.getApplicationModelElement();
			
			if (patternSpecificationElement.getSuperType() != null)
			{
				checkMappedApplictionModelElement(patternSpecificationElement.getSuperType(), applicationModelElement.getSuperType(), pattern);
			}
		}
	}
	
	private void testOperationsInApplicationModel(RoleBinding roleBinding, AppliedPattern pattern)
	{
		if (roleBinding.getRole() instanceof Operation)
		{
			Operation patternSpecificationElement = (Operation) roleBinding.getRole();
			
			Assert.assertNotNull(roleBinding.getApplicationModelElement());
			Assert.assertTrue(roleBinding.getApplicationModelElement() instanceof Operation);
			
			Operation applicationModelElement = (Operation) roleBinding.getApplicationModelElement();
			
			checkMappedApplictionModelElement(patternSpecificationElement.getParentType(), applicationModelElement.getParentType(), pattern);
			
			if (patternSpecificationElement.getOverrides() != null)
			{
				checkMappedApplictionModelElement(patternSpecificationElement.getOverrides(), applicationModelElement.getOverrides(), pattern);
			}
		}
	}
	
	private void testCallActionsInApplicationModel(RoleBinding roleBinding, AppliedPattern pattern)
	{
		if (roleBinding.getRole() instanceof CallAction)
		{
			CallAction patternSpecificationElement = (CallAction) roleBinding.getRole();
			
			Assert.assertNotNull(roleBinding.getApplicationModelElement());
			Assert.assertTrue(roleBinding.getApplicationModelElement() instanceof CallAction);
			
			CallAction applicationModelElement = (CallAction) roleBinding.getApplicationModelElement();
			
			checkMappedApplictionModelElement(patternSpecificationElement.getOperation(), applicationModelElement.getOperation(), pattern);
			checkMappedApplictionModelElement(patternSpecificationElement.getCalledOperation(), applicationModelElement.getCalledOperation(), pattern);
			
			if (patternSpecificationElement.getTarget() != null)
			{
				checkMappedApplictionModelElement(patternSpecificationElement.getTarget(), applicationModelElement.getTarget(), pattern);
			}
		}
	}
	
	private void testReferencesInApplicationModel(RoleBinding roleBinding, AppliedPattern pattern)
	{
		if (roleBinding.getRole() instanceof Reference)
		{
			Reference patternSpecificationElement = (Reference) roleBinding.getRole();
			
			Assert.assertNotNull(roleBinding.getApplicationModelElement());
			Assert.assertTrue(roleBinding.getApplicationModelElement() instanceof Reference);
			
			Reference applicationModelElement = (Reference) roleBinding.getApplicationModelElement();
			
			checkMappedApplictionModelElement(patternSpecificationElement.getParentType(), applicationModelElement.getParentType(), pattern);
			checkMappedApplictionModelElement(patternSpecificationElement.getType(), applicationModelElement.getType(), pattern);
		}
	}
	
	private void checkMappedApplictionModelElement(DesignElement patternSpecificationElement, DesignElement applicationModelElement, AppliedPattern pattern)
	{
		Assert.assertNotNull(patternSpecificationElement);
		Assert.assertNotNull(applicationModelElement);
		Assert.assertNotNull(pattern);
		
		Assert.assertNotSame(patternSpecificationElement, applicationModelElement);
		
		List<DesignElement> allApplicationModelDesignElements = collectAllApplicationModelDesignElements(pattern.getApplicationModel()); 
		Assert.assertTrue(allApplicationModelDesignElements.contains(applicationModelElement));
	}
	
	private static List<DesignElement> collectAllApplicationModelDesignElements(ApplicationModel applicationModel) {
		Assert.assertNotNull(applicationModel);

		ArrayList<DesignElement> collection = new ArrayList<DesignElement>();

		TreeIterator<Object> iterator = EcoreUtil.getAllContents(applicationModel, true);
		while (iterator.hasNext()) {
			Object obj = iterator.next();

			if (obj instanceof DesignElement) {
				collection.add((DesignElement) obj);
			}
		}
		return collection;
	}
}
