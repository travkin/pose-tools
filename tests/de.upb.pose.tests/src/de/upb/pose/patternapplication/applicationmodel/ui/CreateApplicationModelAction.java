package de.upb.pose.patternapplication.applicationmodel.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import de.upb.pose.patternapplication.applicationmodel.CreateApplicationModel;

public class CreateApplicationModelAction implements IObjectActionDelegate
{
	private IWorkbenchPart targetPart;
	private URI domainModelURI;

	@Override
	public void run(IAction action)
	{
		CreateApplicationModel.deriveApplicationModelAndSave(domainModelURI);
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection)
	{
		domainModelURI = null;
		action.setEnabled(false);
		if (selection instanceof IStructuredSelection == false || selection.isEmpty()) {
			return;
		}
		IFile file = (IFile) ((IStructuredSelection) selection).getFirstElement();
		domainModelURI = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		action.setEnabled(true);
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart)
	{
		this.targetPart = targetPart;
	}

}
