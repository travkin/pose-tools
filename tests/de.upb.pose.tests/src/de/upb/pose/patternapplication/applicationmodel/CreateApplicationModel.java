/**
 * 
 */
package de.upb.pose.patternapplication.applicationmodel;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import de.upb.pose.mapping.AppliedPattern;
import de.upb.pose.mapping.BindingCreator;
import de.upb.pose.mapping.MappingPackage;
import de.upb.pose.mapping.PatternApplications;
import de.upb.pose.patternapplication.applicationmodel.ApplicationModelCreator;

/**
 * @author Dietrich Travkin
 *
 */
public class CreateApplicationModel
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		init();
		
		ResourceSet resources = new ResourceSetImpl();
		Resource resource = resources.getResource(getMappingResourceURI(), true);
		EcoreUtil.resolveAll(resources);
		
		PatternApplications patternApplications = (PatternApplications) resource.getContents().get(0);
		AppliedPattern appliedPattern = patternApplications.getApplications().get(0);
		
		BindingCreator.createMissingBindings(appliedPattern);
		ApplicationModelCreator.createInitialApplicationModelFromMapping(appliedPattern);
		
		copyResource(resource);
	}
	
	public static Resource deriveApplicationModelAndSave(URI inputMappingModel)
	{
		init();
		
		ResourceSet resources = new ResourceSetImpl();
		Resource resource = resources.getResource(inputMappingModel, true);
		EcoreUtil.resolveAll(resources);
		
		PatternApplications patternApplications = (PatternApplications) resource.getContents().get(0);
		AppliedPattern appliedPattern = patternApplications.getApplications().get(0);
		
		BindingCreator.createMissingBindings(appliedPattern);
		ApplicationModelCreator.createInitialApplicationModelFromMapping(appliedPattern);
		
		return copyResource(resource);
	}

	private static void init()
	{
		// initialize the model
		MappingPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Register the XMI resource factory for the .mappings extension
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("mappings", new XMIResourceFactoryImpl());
		m.put("ecore", new XMIResourceFactoryImpl());
		m.put("patterns", new XMIResourceFactoryImpl());
	}
	
	private static URI getMappingResourceURI()
	{
		//return URI.createFileURI("models/design_before_strategy_application/editor.mappings");
		return URI.createPlatformResourceURI("de.upb.pose.tests/models/design_before_strategy_application/editor.mappings",true);
	}
	
	private static Resource copyResource(Resource mappingModel)
	{
		Collection<? extends EObject> copies = EcoreUtil.copyAll(mappingModel.getContents());
		
		URI sourceURI = mappingModel.getURI();
		
		String lastSegment = sourceURI.lastSegment();
		String fileExtension = sourceURI.fileExtension();
		URI newURI = sourceURI.trimSegments(1);
		String file = lastSegment.substring(0, lastSegment.indexOf("." + fileExtension)) + "-with-application-model." + fileExtension;
		newURI = newURI.appendSegment(file);
		Resource newResource = mappingModel.getResourceSet().createResource(newURI);
		newResource.getContents().addAll(copies);
		
		try
		{
			newResource.save(Collections.EMPTY_MAP);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return newResource;
	}
}
