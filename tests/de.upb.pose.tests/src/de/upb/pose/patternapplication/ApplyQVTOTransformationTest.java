/**
 * 
 */
package de.upb.pose.patternapplication;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;

/**
 * @author Dietrich Travkin
 *
 */
public class ApplyQVTOTransformationTest extends AbstractPatternApplicationTest
{
	private QVTOPatternApplicator executor;
	private Resource ecoreModel;
	
//	@Before
//	public void completeMapping()
//	{
//		new BindingCreator().createMissingBindings(getAppliedPattern());
//		ApplicationModelCreator.deriveApplicationModelFromMapping(getAppliedPattern());
//	}
	
	/* (non-Javadoc)
	 * @see de.upb.pose.patternapplication.AbstractPatternApplicationTest#getMappingResourceURI()
	 */
	@Override
	protected URI getMappingResourceURI()
	{
		return URI.createPlatformPluginURI("de.upb.pose.tests/models/design_before_strategy_application/editor-with-application-model.mappings", true);
	}
	
	@Before
	public void prepareExecutor()
	{
		executor = new QVTOPatternApplicator();
		ResourceSet rSet = this.getAllResources();
		ecoreModel = rSet.getResource(
	    		URI.createPlatformPluginURI("de.upb.pose.tests/models/design_before_strategy_application/editor.ecore", true), true);
	}
	
	@After
	public void removeExecutor()
	{
		executor = null;
		ecoreModel = null;
	}
	
	@Test
	public void testTransformation()
	{
		Assert.assertNotNull(executor);
		Assert.assertNotNull(ecoreModel);
		Assert.assertFalse(ecoreModel.getContents().isEmpty());
		
		executor.execute(ecoreModel, getAppliedPattern());
		
		// TODO test the result
	}

}
