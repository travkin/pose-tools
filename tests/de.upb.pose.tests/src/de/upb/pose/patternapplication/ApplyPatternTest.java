/**
 * 
 */
package de.upb.pose.patternapplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.junit.Test;

import de.upb.pose.AbstractPatternApplicationTest;
import de.upb.pose.mapping.RoleBinding;
import de.upb.pose.patternapplication.translation.ApplicationModel2EcoreAndSDsTranslator;
import de.upb.pose.specification.DesignElement;
import de.upb.pose.specification.types.Operation;
import de.upb.pose.specification.types.Reference;
import de.upb.pose.specification.types.Type;

/**
 * @author Dietrich Travkin
 *
 */
public class ApplyPatternTest extends AbstractPatternApplicationTest
{
	/* (non-Javadoc)
	 * @see de.upb.pose.patternapplication.AbstractPatternApplicationTest#getMappingResourceURI()
	 */
	@Override
	protected URI getMappingResourceURI()
	{
		return URI.createPlatformPluginURI("de.upb.pose.tests/models/design_before_strategy_application/editor-with-application-model.mappings", true);
	}
	
	@Test
	public void testTransformation()
	{
		assertNotNull(getAppliedPattern());
		
		ApplicationModel2EcoreAndSDsTranslator.performPatternApplication(getAppliedPattern(), getAllResources());
		
		// ensure the model root is set
		EObject root = getAppliedPattern().getParent().getDesignModelRoot();
		assertNotNull(root);
		
		// check translations/mappings
		for (RoleBinding binding: getAppliedPattern().getRoleBindings())
		{
			// check types
			if (binding.getApplicationModelElement() instanceof Type)
			{
				Type type = (Type) binding.getApplicationModelElement();
				
				assertFalse(binding.getModelElements().isEmpty());
				assertTrue(binding.getModelElements().get(0) instanceof EClass);
				
				EClass mappedClass = (EClass) binding.getModelElements().get(0);
				assertNotNull(mappedClass.getEPackage());
				
				// check names
				assertEquals(type.getName(), mappedClass.getName());
				
				// check super type relations
				if (type.getSuperType() != null)
				{
					EClass superClass = (EClass) findRoleBinding(type.getSuperType()).getModelElements().get(0);
					
					assertTrue(mappedClass.getESuperTypes().contains(superClass));
				}
			}
			// check operations
			else if (binding.getApplicationModelElement() instanceof Operation)
			{
				Operation operation = (Operation) binding.getApplicationModelElement();
				
				assertFalse(binding.getModelElements().isEmpty());
				assertTrue(binding.getModelElements().get(0) instanceof EOperation);
				
				EOperation mappedOperation = (EOperation) binding.getModelElements().get(0);
				assertNotNull(mappedOperation.getEContainingClass());
				
				// check names
				if (operation.getOverrides() == null)
				{
					assertEquals(operation.getName(), mappedOperation.getName());
				}
				
				// check return type
				if (operation.getType() != null)
				{
					EClass returnType = (EClass) findRoleBinding(operation.getType()).getModelElements().get(0);
					
					assertNotNull(returnType);
					assertEquals(returnType, mappedOperation.getEType());
				}
				
			}
			// check references
			else if (binding.getApplicationModelElement() instanceof Reference)
			{
				Reference reference = (Reference) binding.getApplicationModelElement();

				assertFalse(binding.getModelElements().isEmpty());
				assertTrue(binding.getModelElements().get(0) instanceof EReference);

				EReference mappedReference = (EReference) binding.getModelElements().get(0);
				assertNotNull(mappedReference.getEContainingClass());

				// check names
				assertEquals(reference.getName(), mappedReference.getName());

				// check referenced type
				EClass refType = (EClass) findRoleBinding(reference.getType()).getModelElements().get(0);

				assertNotNull(refType);
				assertEquals(refType, mappedReference.getEType());
			}
		}
	}
	
	private RoleBinding findRoleBinding(DesignElement applicationModelElement)
	{
		for (RoleBinding binding: getAppliedPattern().getRoleBindings())
		{
			if (binding.getApplicationModelElement() == applicationModelElement)
			{
				return binding;
			}
		}
		return null;
	}
}
