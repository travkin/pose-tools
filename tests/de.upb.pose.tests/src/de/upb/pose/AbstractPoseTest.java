/**
 * 
 */
package de.upb.pose;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.After;
import org.junit.Before;
import org.storydriven.storydiagrams.StoryDiagramsEcoreConnector;
import org.storydriven.storydiagrams.diagram.custom.util.StoryDiagramsModelDiagramConnector;

import de.upb.pose.mapping.MappingConstants;
import de.upb.pose.specification.PatternSpecificationConstants;

/**
 * @author Dietrich Travkin
 */
public abstract class AbstractPoseTest {

	private static final String TESTS_PLUGIN_NAME = "de.upb.pose.tests";
	
	public static final String ECORE_MODEL_FILE_EXTENSION = "ecore";
	public static final String ECORE_DIAGRAMS_FILE_EXTENSION = "ecorediag";
	
	public static final String SPECIFICATION_MODEL_FILE_EXTENSION = PatternSpecificationConstants.MODEL_FILE_EXTENSION;
	public static final String SPECIFICATION_DIAGRAMS_FILE_EXTENSION = PatternSpecificationConstants.DIAGRAMS_FILE_EXTENSION;
	
	public static final String MAPPING_MODEL_FILE_EXTENSION = MappingConstants.MODEL_FILE_EXTENSION;
	public static final String MAPPING_DIAGRAMS_FILE_EXTENSION = MappingConstants.DIAGRAMS_FILE_EXTENSION;
	
	public static final String STORY_DIAGRAM_MODEL_FILE_EXTENSION = StoryDiagramsEcoreConnector.SD_FILE_EXTENSION;
	public static final String STORY_DIAGRAM_DIAGRAMS_FILE_EXTENSION = StoryDiagramsModelDiagramConnector.SD_DIAGRAM_FILE_EXTENSION;
	
	private ResourceSet resourceSet = null;
	
	@Before
	public void setUp() {
		this.resourceSet = new ResourceSetImpl();
		
		for (String path: getPluginRelativePathsToResourcesToBeLoaded()) {
			assertNotNull(path);
			assertFalse(path.isEmpty());
			
			Resource resource = loadResource(path);
			assertNotNull(resource);
		}
		EcoreUtil.resolveAll(this.resourceSet);
	}
	
	@After
	public void tearDown() {
		if (this.resourceSet != null) {
			for (Resource resource : this.resourceSet.getResources()) {
				if (resource.isLoaded()) {
					resource.unload();
				}
			}

			this.resourceSet = null;
		}
	}
	
	protected abstract String[] getPluginRelativePathsToResourcesToBeLoaded();
	
	private URI createResourceUri(String pluginRelativePathToResource) {
		URI uri = URI.createPlatformPluginURI(TESTS_PLUGIN_NAME + "/" + pluginRelativePathToResource, true);
		return uri;
	}
	
	protected Resource loadResource(String pluginRelativePathToResource) {
		assertNotNull(this.resourceSet);
		
		assertNotNull(pluginRelativePathToResource);
		assertFalse(pluginRelativePathToResource.isEmpty());
		
		URI resourceUri = createResourceUri(pluginRelativePathToResource);
		assertNotNull(resourceUri);
		
		Resource resource = this.resourceSet.getResource(resourceUri, true);
		assertNotNull(resource);
		assertTrue("Resource is not loaded.", resource.isLoaded());
		
		return resource;
	}
	
	protected ResourceSet getResourceSet() {
		return this.resourceSet;
	}
	
	private List<Resource> findAllResourcesWithFileExtension(String extension) {
		assertNotNull(extension);
		
		if (this.resourceSet == null
				|| this.resourceSet.getResources().isEmpty()) {
			return Collections.emptyList();
		}
		
		List<Resource> resources = new LinkedList<Resource>();
		for (Resource resource: this.resourceSet.getResources()) {
			if (resource.getURI() != null) {
				String fileExtension = resource.getURI().fileExtension();
				if (extension.equals(fileExtension)) {
					resources.add(resource);
				}
			}
		}
		return resources;
	}
	
	protected List<Resource> findAllEcoreModelResources() {
		return findAllResourcesWithFileExtension(ECORE_MODEL_FILE_EXTENSION);
	}
	
	protected List<Resource> findAllSpecificationModelResources() {
		return findAllResourcesWithFileExtension(SPECIFICATION_MODEL_FILE_EXTENSION);
	}
	
	protected List<Resource> findAllMappingModelResources() {
		return findAllResourcesWithFileExtension(MAPPING_MODEL_FILE_EXTENSION);
	}
	
	protected List<Resource> findAllStoryDiagramModelResources() {
		return findAllResourcesWithFileExtension(STORY_DIAGRAM_MODEL_FILE_EXTENSION);
	}
	
}
