/**
 */
package de.upb.pose.tests.graph;

import de.upb.pose.specification.DesignElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.pose.tests.graph.Node#getEdgesTo <em>Edges To</em>}</li>
 *   <li>{@link de.upb.pose.tests.graph.Node#getParent <em>Parent</em>}</li>
 *   <li>{@link de.upb.pose.tests.graph.Node#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.pose.tests.graph.GraphPackage#getNode()
 * @model
 * @generated
 */
public interface Node extends DesignElement {
	/**
	 * Returns the value of the '<em><b>Edges To</b></em>' reference list.
	 * The list contents are of type {@link de.upb.pose.tests.graph.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edges To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edges To</em>' reference list.
	 * @see de.upb.pose.tests.graph.GraphPackage#getNode_EdgesTo()
	 * @model
	 * @generated
	 */
	EList<Node> getEdgesTo();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.tests.graph.Node#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Node)
	 * @see de.upb.pose.tests.graph.GraphPackage#getNode_Parent()
	 * @see de.upb.pose.tests.graph.Node#getChildren
	 * @model opposite="children" transient="false"
	 * @generated
	 */
	Node getParent();

	/**
	 * Sets the value of the '{@link de.upb.pose.tests.graph.Node#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Node value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.pose.tests.graph.Node}.
	 * It is bidirectional and its opposite is '{@link de.upb.pose.tests.graph.Node#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see de.upb.pose.tests.graph.GraphPackage#getNode_Children()
	 * @see de.upb.pose.tests.graph.Node#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<Node> getChildren();

} // Node
